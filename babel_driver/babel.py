from pynq import Overlay
from pynq import DefaultHierarchy
import pynq.lib.dma
from pynq import allocate
import numpy as np

class UDPDriver(DefaultHierarchy):
    def __init__(self, description):
        super().__init__(description=description)
        self.udp_stack.protocol_V = 0x11
        self.udp_stack.udp_0.reg_listen_port_V = 54001
    
    def read_data(self, length):
        """
        Read data and metadata from the UDP module.

        All data is guaranteed to be for this IP address/the open port except for malicious packets.
        """
        with allocate(shape=(length), dtype=np.uint8) as data, allocate(shape=(22), dtype=np.uint8) as meta:
            self.udp_data.recvchannel.transfer(data)
            self.udp_meta.recvchannel.transfer(meta)
            self.udp_data.recvchannel.wait()
            self.udp_meta.recvchannel.wait()
            self.udp_data.recvchannel.stop()
            self.udp_meta.recvchannel.stop()
            result_data = data.copy()
            result_meta = data.copy()
        return result_data, result_meta

    def write_data(self, data, ip_address, port, myport):
        with allocate(shape=(len(data)), dtype=np.uint8) as data_buf, allocate(shape=(22), dtype=np.uint8) as meta_buf:
            for i, d in enumerate(data):
                data_buf[i] = d
            for i, b in enumerate(ip_address.to_bytes(4,'big')):
                meta_buf[i] = b
            meta_buf[4] = meta_buf[0]
            meta_buf[5] = meta_buf[1] 
            meta_buf[6] = meta_buf[2]
            meta_buf[7] = meta_buf[3]
            meta_buf[8] = meta_buf[0]
            meta_buf[9] = meta_buf[1] 
            meta_buf[10] = meta_buf[2]
            meta_buf[11] = meta_buf[3]
            meta_buf[12] = meta_buf[0]
            meta_buf[13] = meta_buf[1] 
            meta_buf[14] = meta_buf[2]
            meta_buf[15] = meta_buf[3]
            port_bytes = port.to_bytes(2, 'big')
            meta_buf[16] = port_bytes[0]
            meta_buf[17] = port_bytes[1]
            port_bytes = myport.to_bytes(2, 'big')
            meta_buf[18] = port_bytes[0]
            meta_buf[19] = port_bytes[1]
            length_bytes = len(data).to_bytes(2, 'big')
            meta_buf[20] = length_bytes[0]
            meta_buf[21] = length_bytes[1]

            self.udp_data.sendchannel.transfer(meta_buf)
            self.udp_meta.sendchannel.transfer(data_buf)
            self.udp_meta.sendchannel.wait()
            self.udp_data.sendchannel.wait()
            self.udp_data.sendchannel.stop()
            self.udp_meta.sendchannel.stop()

    def set_listen_port(self, port):
        self.udp_stack.udp_0.reg_listen_port_V = port


    @staticmethod
    def checkhierarchy(description):
        if 'udp_data' in description['ip'] and 'udp_meta' in description['ip']:
            return True
        return False



class TCPDriver(DefaultHierarchy):
    """
    Still a work in progress, no current TCP support.
    """
    def __init__(self, description):
        super().__init__(description=description)
    
    def read_data(self, length=1024):
        pass

    def write_data(self, data):
        pass


    @staticmethod
    def checkhierarchy(description):
        if 'tcp_meta' in description['ip'] and 'tcp_data' in description['ip']:
            return True
        return False 


class BabelOverlay(Overlay):
    """
    Control parameters for the whole overlay, including partial reconfig.
    """
    def __init__(self, bitfile, **kwargs):
        super().__init__(bitfile, **kwargs)
        self.UDP.udp_stack.ipv4_0.local_ipv4_address_V = 0xDEADBEEF
        self.UDP.udp_stack.udp_0.reg_ip_address_V_1 = 0xDEADBEEF
        self.UDP.udp_stack.udp_0.reg_ip_address_V_2 = 0xDEADBEEF
        self.UDP.udp_stack.udp_0.reg_ip_address_V_3 = 0xDEADBEEF
        self.UDP.udp_stack.udp_0.reg_ip_address_V_4 = 0xDEADBEEF
        self.network_stack.arp_server_subnet_0.myIpAddress_V = 0xDEADBEEF
        self.network_stack.ip_handler_0.myIpAddress_V = 0xDEADBEEF
        self.network_stack.arp_server_subnet_0.myMacAddress_V_1 = 0xEEEEEEEE
        self.network_stack.arp_server_subnet_0.myMacAddress_V_2 = 0xEEEEEEEE
        self.network_stack.mac_ip_encode_0.myMacAddress_V_1 = 0xEEEEEEEE
        self.network_stack.mac_ip_encode_0.myMacAddress_V_2 = 0xEEEEEEEE
        self.network_stack.mac_ip_encode_0.regSubNetMask_V= 0x00FFFFFF
        self.network_stack.mac_ip_encode_0.regDefaultGateway_V= 0xDEADBEEF

    def setIP(self, ip):
        self.UDP.udp_stack.ipv4_0.local_ipv4_address_V = ip
        self.UDP.udp_stack.udp_0.reg_ip_address_V_1 = ip
        self.UDP.udp_stack.udp_0.reg_ip_address_V_2 = ip
        self.UDP.udp_stack.udp_0.reg_ip_address_V_3 = ip
        self.UDP.udp_stack.udp_0.reg_ip_address_V_4 = ip
        self.network_stack.arp_server_subnet_0.myIpAddress_V = ip
        self.network_stack.ip_handler_0.myIpAddress_V = ip

    def setMAC(self, mac):
        self.network_stack.arp_server_subnet_0.myMacAddress_V_1 = mac[0]
        self.network_stack.arp_server_subnet_0.myMacAddress_V_2 = mac[1]
        self.network_stack.mac_ip_encode_0.myMacAddress_V_1 = mac[0]
        self.network_stack.mac_ip_encode_0.myMacAddress_V_2 = mac[1]


class NetworkDriver(DefaultHierarchy):
    """
    Send data out and receive as an ethernet interface. Temporary until ethernet bypass works.
    Permanant function is to set parameters in the network_stack hierarchy
    """
    def __init__(self, description):
        super().__init__(description=description)
    
    def read_data(self, length):
        with allocate(shape=(length), dtype=np.uint8) as data:
            self.ethernet_dma.recvchannel.transfer(data)
            self.ethernet_dma.recvchannel.wait()
            self.ethernet_dma.recvchannel.stop()
            result_data = data.copy()
        return result_data

    def write_data(self, data):
        with allocate(shape=(len(data)), dtype=np.uint8) as data:
            self.ethernet_dma.sendchannel.transfer(data)
            self.ethernet_dma.sendchannel.wait()
            self.ethernet_dma.sendchannel.stop()

    def set_subnet_mask(self, mask):
        self.mac_ip_encode_0.regSubNetMask_V = mask
    
    def set_gateway(self, mask):
        self.mac_ip_encode_0.regDefaultGateway_V= mask

    @staticmethod
    def checkhierarchy(description):
        if 'ethernet_dma' in description['ip'] and 'arp_server_subnet_0' in description['ip']:
            return True
        return False