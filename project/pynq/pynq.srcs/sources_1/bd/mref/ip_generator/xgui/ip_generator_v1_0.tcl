# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "DHCP_EN" -parent ${Page_0}
  ipgui::add_param $IPINST -name "IPV6_ADDRESS" -parent ${Page_0}
  ipgui::add_param $IPINST -name "IP_DEFAULT_GATEWAY" -parent ${Page_0}
  ipgui::add_param $IPINST -name "IP_SUBNET_MASK" -parent ${Page_0}
  ipgui::add_param $IPINST -name "MAC_ADDRESS" -parent ${Page_0}
  ipgui::add_param $IPINST -name "NET_BANDWIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "ROCE_EN" -parent ${Page_0}
  ipgui::add_param $IPINST -name "RX_DDR_BYPASS_EN" -parent ${Page_0}
  ipgui::add_param $IPINST -name "TCP_EN" -parent ${Page_0}
  ipgui::add_param $IPINST -name "UDP_EN" -parent ${Page_0}
  ipgui::add_param $IPINST -name "WIDTH" -parent ${Page_0}


}

proc update_PARAM_VALUE.DHCP_EN { PARAM_VALUE.DHCP_EN } {
	# Procedure called to update DHCP_EN when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DHCP_EN { PARAM_VALUE.DHCP_EN } {
	# Procedure called to validate DHCP_EN
	return true
}

proc update_PARAM_VALUE.IPV6_ADDRESS { PARAM_VALUE.IPV6_ADDRESS } {
	# Procedure called to update IPV6_ADDRESS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.IPV6_ADDRESS { PARAM_VALUE.IPV6_ADDRESS } {
	# Procedure called to validate IPV6_ADDRESS
	return true
}

proc update_PARAM_VALUE.IP_DEFAULT_GATEWAY { PARAM_VALUE.IP_DEFAULT_GATEWAY } {
	# Procedure called to update IP_DEFAULT_GATEWAY when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.IP_DEFAULT_GATEWAY { PARAM_VALUE.IP_DEFAULT_GATEWAY } {
	# Procedure called to validate IP_DEFAULT_GATEWAY
	return true
}

proc update_PARAM_VALUE.IP_SUBNET_MASK { PARAM_VALUE.IP_SUBNET_MASK } {
	# Procedure called to update IP_SUBNET_MASK when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.IP_SUBNET_MASK { PARAM_VALUE.IP_SUBNET_MASK } {
	# Procedure called to validate IP_SUBNET_MASK
	return true
}

proc update_PARAM_VALUE.MAC_ADDRESS { PARAM_VALUE.MAC_ADDRESS } {
	# Procedure called to update MAC_ADDRESS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.MAC_ADDRESS { PARAM_VALUE.MAC_ADDRESS } {
	# Procedure called to validate MAC_ADDRESS
	return true
}

proc update_PARAM_VALUE.NET_BANDWIDTH { PARAM_VALUE.NET_BANDWIDTH } {
	# Procedure called to update NET_BANDWIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.NET_BANDWIDTH { PARAM_VALUE.NET_BANDWIDTH } {
	# Procedure called to validate NET_BANDWIDTH
	return true
}

proc update_PARAM_VALUE.ROCE_EN { PARAM_VALUE.ROCE_EN } {
	# Procedure called to update ROCE_EN when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.ROCE_EN { PARAM_VALUE.ROCE_EN } {
	# Procedure called to validate ROCE_EN
	return true
}

proc update_PARAM_VALUE.RX_DDR_BYPASS_EN { PARAM_VALUE.RX_DDR_BYPASS_EN } {
	# Procedure called to update RX_DDR_BYPASS_EN when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.RX_DDR_BYPASS_EN { PARAM_VALUE.RX_DDR_BYPASS_EN } {
	# Procedure called to validate RX_DDR_BYPASS_EN
	return true
}

proc update_PARAM_VALUE.TCP_EN { PARAM_VALUE.TCP_EN } {
	# Procedure called to update TCP_EN when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.TCP_EN { PARAM_VALUE.TCP_EN } {
	# Procedure called to validate TCP_EN
	return true
}

proc update_PARAM_VALUE.UDP_EN { PARAM_VALUE.UDP_EN } {
	# Procedure called to update UDP_EN when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.UDP_EN { PARAM_VALUE.UDP_EN } {
	# Procedure called to validate UDP_EN
	return true
}

proc update_PARAM_VALUE.WIDTH { PARAM_VALUE.WIDTH } {
	# Procedure called to update WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.WIDTH { PARAM_VALUE.WIDTH } {
	# Procedure called to validate WIDTH
	return true
}


proc update_MODELPARAM_VALUE.NET_BANDWIDTH { MODELPARAM_VALUE.NET_BANDWIDTH PARAM_VALUE.NET_BANDWIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.NET_BANDWIDTH}] ${MODELPARAM_VALUE.NET_BANDWIDTH}
}

proc update_MODELPARAM_VALUE.WIDTH { MODELPARAM_VALUE.WIDTH PARAM_VALUE.WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.WIDTH}] ${MODELPARAM_VALUE.WIDTH}
}

proc update_MODELPARAM_VALUE.MAC_ADDRESS { MODELPARAM_VALUE.MAC_ADDRESS PARAM_VALUE.MAC_ADDRESS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.MAC_ADDRESS}] ${MODELPARAM_VALUE.MAC_ADDRESS}
}

proc update_MODELPARAM_VALUE.IPV6_ADDRESS { MODELPARAM_VALUE.IPV6_ADDRESS PARAM_VALUE.IPV6_ADDRESS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.IPV6_ADDRESS}] ${MODELPARAM_VALUE.IPV6_ADDRESS}
}

proc update_MODELPARAM_VALUE.IP_SUBNET_MASK { MODELPARAM_VALUE.IP_SUBNET_MASK PARAM_VALUE.IP_SUBNET_MASK } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.IP_SUBNET_MASK}] ${MODELPARAM_VALUE.IP_SUBNET_MASK}
}

proc update_MODELPARAM_VALUE.IP_DEFAULT_GATEWAY { MODELPARAM_VALUE.IP_DEFAULT_GATEWAY PARAM_VALUE.IP_DEFAULT_GATEWAY } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.IP_DEFAULT_GATEWAY}] ${MODELPARAM_VALUE.IP_DEFAULT_GATEWAY}
}

proc update_MODELPARAM_VALUE.DHCP_EN { MODELPARAM_VALUE.DHCP_EN PARAM_VALUE.DHCP_EN } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DHCP_EN}] ${MODELPARAM_VALUE.DHCP_EN}
}

proc update_MODELPARAM_VALUE.TCP_EN { MODELPARAM_VALUE.TCP_EN PARAM_VALUE.TCP_EN } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.TCP_EN}] ${MODELPARAM_VALUE.TCP_EN}
}

proc update_MODELPARAM_VALUE.RX_DDR_BYPASS_EN { MODELPARAM_VALUE.RX_DDR_BYPASS_EN PARAM_VALUE.RX_DDR_BYPASS_EN } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.RX_DDR_BYPASS_EN}] ${MODELPARAM_VALUE.RX_DDR_BYPASS_EN}
}

proc update_MODELPARAM_VALUE.UDP_EN { MODELPARAM_VALUE.UDP_EN PARAM_VALUE.UDP_EN } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.UDP_EN}] ${MODELPARAM_VALUE.UDP_EN}
}

proc update_MODELPARAM_VALUE.ROCE_EN { MODELPARAM_VALUE.ROCE_EN PARAM_VALUE.ROCE_EN } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.ROCE_EN}] ${MODELPARAM_VALUE.ROCE_EN}
}

