`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 13.12.2020 10:09:23
// Design Name: 
// Module Name: ip_generator
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ip_generator#(
    parameter NET_BANDWIDTH = 10,
    parameter WIDTH = 64,
    parameter MAC_ADDRESS = 48'hE59D02350A00, // LSB first, 00:0A:35:02:9D:E5
    parameter IPV6_ADDRESS= 128'hE59D_02FF_FF35_0A02_0000_0000_0000_80FE, //LSB first: FE80_0000_0000_0000_020A_35FF_FF02_9DE5,
    parameter IP_SUBNET_MASK = 32'h00FFFFFF,
    parameter IP_DEFAULT_GATEWAY = 32'h00000000,
    parameter DHCP_EN   = 0,
    parameter TCP_EN = 0,
    parameter RX_DDR_BYPASS_EN = 0,
    parameter UDP_EN = 0,
    parameter ROCE_EN = 0
)(
    input wire          net_clk,
    input wire          net_aresetn,
    input wire set_ip_addr_valid,
    input wire [31:0] set_ip_addr_data,

    input wire set_board_number_valid,
    input wire[3:0] set_board_number_data,

    output wire[31:0]  dhcp_ip_address,
    output wire        dhcp_ip_address_en,
    output reg[47:0]   mie_mac_address,
    output reg[47:0]   arp_mac_address,
    output reg[47:0]   ipv6_mac_address,
    output reg[31:0]   iph_ip_address,
    output reg[31:0]   arp_ip_address,
    output reg[31:0]   toe_ip_address,
    output reg[31:0]   ip_subnet_mask,
    output reg[31:0]   ip_default_gateway,
    output reg[127:0] link_local_ipv6_address,
    output reg [31:0] local_ip_address,
    output reg[3:0] board_number
    );


always @(posedge net_clk) begin
    if (~net_aresetn) begin
        local_ip_address <= 32'hD1D4010B;
        board_number <= 0;
    end
    else begin
        if (set_ip_addr_valid) begin
            local_ip_address[7:0] <= set_ip_addr_data[31:24];
            local_ip_address[15:8] <= set_ip_addr_data[23:16];
            local_ip_address[23:16] <= set_ip_addr_data[15:8];
            local_ip_address[31:24] <= set_ip_addr_data[7:0];
        end
        if (set_board_number_valid) begin
            board_number <= set_board_number_data;
        end
    end
end
    
always @(posedge net_clk)
begin
    if (net_aresetn == 0) begin
        mie_mac_address <= 48'h000000000000;
        arp_mac_address <= 48'h000000000000;
        ipv6_mac_address <= 48'h000000000000;
        iph_ip_address <= 32'h00000000;
        arp_ip_address <= 32'h00000000;
        toe_ip_address <= 32'h00000000;
        ip_subnet_mask <= 32'h00000000;
        ip_default_gateway <= 32'h00000000;
        link_local_ipv6_address <= 0;
    end
    else begin
        mie_mac_address <= {MAC_ADDRESS[47:44], (MAC_ADDRESS[43:40]+board_number), MAC_ADDRESS[39:0]};
        arp_mac_address <= {MAC_ADDRESS[47:44], (MAC_ADDRESS[43:40]+board_number), MAC_ADDRESS[39:0]};
        ipv6_mac_address <= {MAC_ADDRESS[47:44], (MAC_ADDRESS[43:40]+board_number), MAC_ADDRESS[39:0]};
        //link_local_ipv6_address[127:80] <= ipv6_mac_address;
        //link_local_ipv6_address[15:0] <= 16'h80fe; // fe80
        //link_local_ipv6_address[79:16] <= 64'h0000_0000_0000_0000;
        link_local_ipv6_address <= {IPV6_ADDRESS[127:120]+board_number, IPV6_ADDRESS[119:0]};
        if (DHCP_EN == 1) begin
            if (dhcp_ip_address_en == 1'b1) begin
                iph_ip_address <= dhcp_ip_address;
                arp_ip_address <= dhcp_ip_address;
                toe_ip_address <= dhcp_ip_address;
            end
        end
        else begin
            iph_ip_address <= local_ip_address;
            arp_ip_address <= local_ip_address;
            toe_ip_address <= local_ip_address;
            ip_subnet_mask <= IP_SUBNET_MASK;
            ip_default_gateway <= {local_ip_address[31:28], 8'h01, local_ip_address[23:0]};
        end
    end
end
endmodule
