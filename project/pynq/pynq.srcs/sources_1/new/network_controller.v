/*
 * Copyright (c) 2019, Systems Group, ETH Zurich
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software
 * without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
`timescale 1ns / 1ps
`default_nettype none

module network_controller
(
    // user clk
    input wire          net_clk,
    input wire          net_aresetn,
    
    //Control interface
    input  wire  [31:0] axil_awaddr,
    input  wire         axil_awvalid,
    output reg          axil_awready,
    input  wire  [31:0] axil_wdata  ,
    input  wire   [3:0] axil_wstrb  ,
    input  wire         axil_wvalid ,
    output reg          axil_wready ,
    output reg   [1:0]  axil_bresp  ,
    output reg          axil_bvalid ,
    input  wire         axil_bready ,
    input  wire  [31:0] axil_araddr ,
    input  wire         axil_arvalid,
    output reg          axil_arready,
    output reg  [31:0]  axil_rdata  ,
    output reg   [1:0]  axil_rresp  ,
    output reg          axil_rvalid ,
    input  wire         axil_rready ,

    output reg          m_axis_host_arp_lookup_request_TVALID,
    input wire          m_axis_host_arp_lookup_request_TREADY,
    output reg[31:0]    m_axis_host_arp_lookup_request_TDATA,
    input wire          s_axis_host_arp_lookup_reply_TVALID,
    output reg          s_axis_host_arp_lookup_reply_TREADY,
    input wire[55:0]    s_axis_host_arp_lookup_reply_TDATA,

//all counter deleted

    output reg         set_ip_addr_valid,
    output reg[31:0]   set_ip_addr_data,
    
    output reg          set_board_number_valid,
    output reg[3:0]     set_board_number_data


);

localparam AXI_RESP_OK = 2'b00;
localparam AXI_RESP_SLVERR = 2'b10;

//TODO clean up and use enum

//WRITE states
localparam WRITE_IDLE = 0;
localparam WRITE_DATA = 1;
localparam WRITE_RESPONSE = 2;
localparam WRITE_CONTEXT = 3;
localparam WRITE_CONN = 4;
//localparam WRITE_TLB = 5;
localparam WRITE_POST = 6;
//localparam WRITE_PART_CMD = 7;
//localparam WRITE_PART_MAP = 8;
//localparam RESET_RX_PART = 9;
localparam WRITE_ARP_LOOKUP = 10;
localparam WRITE_PC_META = 11;

//READ states
localparam READ_IDLE = 0;
//localparam READ_DATA = 1;
localparam READ_RESPONSE = 1;
localparam READ_RESPONSE2 = 2;
localparam WAIT_BRAM = 3;

//ADDRESES
localparam GPIO_REG_CTX         = 8'h00;
localparam GPIO_REG_CONN        = 8'h01;
//localparam GPIO_REG_TLB         = 8'h02;
localparam GPIO_REG_POST        = 8'h03;
//localparam GPIO_REG_PART_CMD    = 8'h04;
//localparam GPIO_REG_PART_MAP    = 8'h05;
//localparam GPIO_REG_PART_RESET  = 8'h06;
localparam GPIO_REG_BOARDNUM    = 8'h07;
localparam GPIO_REG_IPADDR      = 8'h08;
localparam GPIO_REG_ARP         = 8'h09;
//localparam GPIO_REG_DMA_READS   = 8'h0A;
//localparam GPIO_REG_DMA_WRITES  = 8'h0B;
localparam GPIO_REG_DEBUG       = 8'h0C;
//localparam GPIO_REG_DEBUG2      = 8'h0D;
localparam GPIO_REG_CMDS        = 8'h0E;
localparam GPIO_REG_PC_META     = 8'h0F;
localparam GPIO_REG_PART_TUPLES = 8'h10;

//localparam GPIO_REG_CRC_DROPS   = 8'h11;

localparam DIRECTION_RX = 0;
localparam DIRECTION_TX = 1;

localparam NUM_DEBUG_REGS = 25;

localparam DEBUG_CRC_DROPS  = 8'h00;
localparam DEBUG_PSN_DROPS  = 8'h01;
localparam DEBUG_RX_WORDS   = 8'h02;
localparam DEBUG_RX_PKGS    = 8'h03;
localparam DEBUG_TX_WORDS   = 8'h04;
localparam DEBUG_TX_PKGS    = 8'h05;
localparam DEBUG_ARP_RX_PKG = 8'h06;
localparam DEBUG_ARP_TX_PKG = 8'h07;
localparam DEBUG_ARP_REQ_PKG = 8'h08;
localparam DEBUG_ARP_RSP_PKG = 8'h09;
localparam DEBUG_ICMP_RX_PKG = 8'h0A;
localparam DEBUG_ICMP_TX_PKG = 8'h0B;
localparam DEBUG_TCP_RX_PKG = 8'h0C;
localparam DEBUG_TCP_TX_PKG = 8'h0D;

localparam DEBUG_ROCE_RX_PKG = 8'h10;
localparam DEBUG_ROCE_TX_PKG = 8'h11;
localparam DEBUG_ROCE_DATA_RX_WORDS  = 8'h12;
localparam DEBUG_ROCE_DATA_RX_PKGS   = 8'h13;
localparam DEBUG_ROCE_DATA_HOST_TX_WORDS  = 8'h14;
localparam DEBUG_ROCE_DATA_HOST_TX_PKGS   = 8'h15;
localparam DEBUG_ROCE_DATA_ROLE_TX_WORDS  = 8'h16;
localparam DEBUG_ROCE_DATA_ROLE_TX_PKGS   = 8'h17;
localparam DEBUG_NET_DOWN       = 8'h18;


//AXI_MM releated codes deleted
  
//Fifo for tx metadata, deleted

// ACTUAL LOGIC

reg[7:0] writeState;
reg[7:0] readState;

reg[31:0] writeAddr;
reg[31:0] readAddr;

reg[7:0]  word_counter;

reg[7:0] mmwriteState;
reg[63:0]mmwriteAddr;

//handle writes
//handle writes
always @(posedge net_clk)
begin
    if (~net_aresetn) begin
        axil_awready <= 1'b0;
        axil_wready <= 1'b0;
        axil_bvalid <= 1'b0;
        
        word_counter <= 0;
        set_ip_addr_valid <= 1'b0;
        set_board_number_valid <= 1'b0;
        
        writeState <= WRITE_IDLE;
    end
    else begin
        case (writeState)
            WRITE_IDLE: begin
                axil_awready <= 1'b1;
                axil_wready <= 1'b0;
                axil_bvalid <= 1'b0;
                
                m_axis_host_arp_lookup_request_TVALID <= 1'b0;
                s_axis_host_arp_lookup_reply_TREADY <= 1'b1;
                
                writeAddr <= (axil_awaddr[11:0] >> 5);
                if (axil_awvalid && axil_awready) begin
                    axil_awready <= 1'b0;
                    axil_wready <= 1'b1;
                    writeState <= WRITE_DATA;
                end
            end //WRITE_IDLE
            WRITE_DATA: begin
                axil_wready <= 1'b1;
                if (axil_wvalid && axil_wready) begin
                    axil_wready <= 0;
                    axil_bvalid <= 1'b1;
                    axil_bresp <= AXI_RESP_OK;
                    writeState <= WRITE_RESPONSE;
                    case (writeAddr)
                        GPIO_REG_IPADDR: begin
                            set_ip_addr_valid <= 1'b1;
                            set_ip_addr_data <= axil_wdata[31:0];
                            axil_bvalid <= 1'b1;
                            axil_bresp <= AXI_RESP_OK;
                            writeState <= WRITE_RESPONSE;
                        end
                        GPIO_REG_BOARDNUM: begin
                            set_board_number_valid <= 1'b1;
                            set_board_number_data <= axil_wdata[3:0];
                            axil_bvalid <= 1'b1;
                            axil_bresp <= AXI_RESP_OK;
                            writeState <= WRITE_RESPONSE;
                        end
                        GPIO_REG_ARP: begin
                            m_axis_host_arp_lookup_request_TVALID <= 1'b1;
                            m_axis_host_arp_lookup_request_TDATA[7:0] <= axil_wdata[31:24];
                            m_axis_host_arp_lookup_request_TDATA[15:8] <= axil_wdata[23:16];
                            m_axis_host_arp_lookup_request_TDATA[23:16] <= axil_wdata[15:8];
                            m_axis_host_arp_lookup_request_TDATA[31:24] <= axil_wdata[7:0];
                            axil_bvalid <= 1'b0;
                            writeState <= WRITE_ARP_LOOKUP;
                        end
                    endcase
                end
            end //WRITE_DATA
            WRITE_RESPONSE: begin
                axil_bvalid <= 1'b1;
                if (axil_bvalid && axil_bready) begin
                    axil_bvalid <= 1'b0;
                    writeState <= WRITE_IDLE;
                end
            end//WRITE_RESPONSE

            WRITE_ARP_LOOKUP: begin
                m_axis_host_arp_lookup_request_TVALID <= 1'b1;
                if (m_axis_host_arp_lookup_request_TVALID && m_axis_host_arp_lookup_request_TREADY) begin
                    axil_bvalid <= 1'b1;
                    axil_bresp <= AXI_RESP_OK;
                    m_axis_host_arp_lookup_request_TVALID <= 1'b0;
                    writeState <= WRITE_RESPONSE;
                end
            end
        endcase
    end
end

endmodule
`default_nettype wire
