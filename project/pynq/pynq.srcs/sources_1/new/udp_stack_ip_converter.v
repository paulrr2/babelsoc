`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 13.12.2020 07:06:51
// Design Name: 
// Module Name: udp_stack_ip_converter
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module udp_stack_ip_converter(
input wire [31:0] local_ip_address,
output wire [127:0] reg_ip_address_V
    );
    
assign reg_ip_address_V = {local_ip_address,local_ip_address,local_ip_address,local_ip_address};
endmodule
