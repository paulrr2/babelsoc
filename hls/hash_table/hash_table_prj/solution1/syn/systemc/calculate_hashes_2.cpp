#include "calculate_hashes.h"
#include "AESL_pkg.h"

using namespace std;

namespace ap_rtl {

void calculate_hashes::thread_ap_clk_no_reset_() {
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_CS_fsm = ap_ST_fsm_state1;
    } else {
        ap_CS_fsm = ap_NS_fsm.read();
    }
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state1.read()) && esl_seteq<1,1,1>(ap_start.read(), ap_const_logic_1))) {
        p_Result_0_10_reg_8560 = key_V.read().range(11, 11);
        p_Result_0_11_reg_8573 = key_V.read().range(12, 12);
        p_Result_0_12_reg_8586 = key_V.read().range(13, 13);
        p_Result_0_13_reg_8598 = key_V.read().range(14, 14);
        p_Result_0_14_reg_8611 = key_V.read().range(15, 15);
        p_Result_0_15_reg_8624 = key_V.read().range(16, 16);
        p_Result_0_16_reg_8637 = key_V.read().range(17, 17);
        p_Result_0_17_reg_8650 = key_V.read().range(18, 18);
        p_Result_0_18_reg_8663 = key_V.read().range(19, 19);
        p_Result_0_19_reg_8676 = key_V.read().range(20, 20);
        p_Result_0_1_reg_8431 = key_V.read().range(1, 1);
        p_Result_0_20_reg_8689 = key_V.read().range(21, 21);
        p_Result_0_21_reg_8702 = key_V.read().range(22, 22);
        p_Result_0_22_reg_8715 = key_V.read().range(23, 23);
        p_Result_0_23_reg_8728 = key_V.read().range(24, 24);
        p_Result_0_24_reg_8741 = key_V.read().range(25, 25);
        p_Result_0_25_reg_8754 = key_V.read().range(26, 26);
        p_Result_0_26_reg_8766 = key_V.read().range(27, 27);
        p_Result_0_27_reg_8778 = key_V.read().range(28, 28);
        p_Result_0_28_reg_8791 = key_V.read().range(29, 29);
        p_Result_0_29_reg_8803 = key_V.read().range(30, 30);
        p_Result_0_2_reg_8443 = key_V.read().range(2, 2);
        p_Result_0_30_reg_8816 = key_V.read().range(31, 31);
        p_Result_0_31_reg_8829 = key_V.read().range(32, 32);
        p_Result_0_32_reg_8841 = key_V.read().range(33, 33);
        p_Result_0_33_reg_8851 = key_V.read().range(34, 34);
        p_Result_0_34_reg_8861 = key_V.read().range(35, 35);
        p_Result_0_35_reg_8870 = key_V.read().range(36, 36);
        p_Result_0_36_reg_8881 = key_V.read().range(37, 37);
        p_Result_0_37_reg_8894 = key_V.read().range(38, 38);
        p_Result_0_38_reg_8906 = key_V.read().range(39, 39);
        p_Result_0_39_reg_8919 = key_V.read().range(40, 40);
        p_Result_0_3_reg_8456 = key_V.read().range(3, 3);
        p_Result_0_40_reg_8932 = key_V.read().range(41, 41);
        p_Result_0_41_reg_8945 = key_V.read().range(42, 42);
        p_Result_0_42_reg_8958 = key_V.read().range(43, 43);
        p_Result_0_43_reg_8971 = key_V.read().range(44, 44);
        p_Result_0_44_reg_8984 = key_V.read().range(45, 45);
        p_Result_0_45_reg_8997 = key_V.read().range(46, 46);
        p_Result_0_46_reg_9010 = key_V.read().range(47, 47);
        p_Result_0_47_reg_9023 = key_V.read().range(48, 48);
        p_Result_0_48_reg_9036 = key_V.read().range(49, 49);
        p_Result_0_49_reg_9049 = key_V.read().range(50, 50);
        p_Result_0_4_reg_8469 = key_V.read().range(4, 4);
        p_Result_0_50_reg_9062 = key_V.read().range(51, 51);
        p_Result_0_51_reg_9075 = key_V.read().range(52, 52);
        p_Result_0_52_reg_9087 = key_V.read().range(53, 53);
        p_Result_0_53_reg_9100 = key_V.read().range(54, 54);
        p_Result_0_54_reg_9113 = key_V.read().range(55, 55);
        p_Result_0_55_reg_9126 = key_V.read().range(56, 56);
        p_Result_0_56_reg_9139 = key_V.read().range(57, 57);
        p_Result_0_57_reg_9152 = key_V.read().range(58, 58);
        p_Result_0_58_reg_9164 = key_V.read().range(59, 59);
        p_Result_0_59_reg_9177 = key_V.read().range(60, 60);
        p_Result_0_5_reg_8482 = key_V.read().range(5, 5);
        p_Result_0_60_reg_9190 = key_V.read().range(61, 61);
        p_Result_0_61_reg_9203 = key_V.read().range(62, 62);
        p_Result_0_62_reg_9216 = key_V.read().range(63, 63);
        p_Result_0_6_reg_8495 = key_V.read().range(6, 6);
        p_Result_0_7_reg_8508 = key_V.read().range(7, 7);
        p_Result_0_8_reg_8521 = key_V.read().range(8, 8);
        p_Result_0_9_reg_8534 = key_V.read().range(9, 9);
        p_Result_0_s_reg_8547 = key_V.read().range(10, 10);
        p_Result_s_reg_8418 = key_V.read().range(0, 0);
        xor_ln1724_154_reg_9234 = xor_ln1724_154_fu_988_p2.read();
        xor_ln1724_403_reg_9239 = xor_ln1724_403_fu_1042_p2.read();
        xor_ln1724_93_reg_9229 = xor_ln1724_93_fu_916_p2.read();
    }
}

void calculate_hashes::thread_ap_NS_fsm() {
    switch (ap_CS_fsm.read().to_uint64()) {
        case 1 : 
            if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state1.read()) && esl_seteq<1,1,1>(ap_start.read(), ap_const_logic_1))) {
                ap_NS_fsm = ap_ST_fsm_state2;
            } else {
                ap_NS_fsm = ap_ST_fsm_state1;
            }
            break;
        case 2 : 
            ap_NS_fsm = ap_ST_fsm_state1;
            break;
        default : 
            ap_NS_fsm = "XX";
            break;
    }
}

}

