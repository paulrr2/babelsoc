
open_project hash_table_prj

open_solution "solution1"
set_part {xc7z020clg400-1}
create_clock -period 6.4 -name default

set_top hash_table_top

add_files /home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/hash_table/hash_table.cpp -cflags "-std=c++11 -I/home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/build_pynq/hls/hash_table"


add_files -tb /home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/hash_table/test_hash_table.cpp -cflags "-std=c++11 -I/home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/build_pynq/hls/hash_table"


#Check which command
set command [lindex $argv 2]

if {$command == "synthesis"} {
   csynth_design
} elseif {$command == "csim"} {
   csim_design
} elseif {$command == "ip"} {
   export_design -format ip_catalog -ipname "hash_table" -display_name "Hash Table (cuckoo)" -description "" -vendor "ethz.systems.fpga" -version "1.0"
} elseif {$command == "installip"} {
   file mkdir /home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/iprepo
   file delete -force /home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/iprepo/hash_table
   file copy -force /home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/build_pynq/hls/hash_table/hash_table_prj/solution1/impl/ip /home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/iprepo/hash_table/
} else {
   puts "No valid command specified. Use vivado_hls -f make.tcl <synthesis|csim|ip> ."
}


exit
