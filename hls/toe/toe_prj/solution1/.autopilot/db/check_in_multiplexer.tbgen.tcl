set moduleName check_in_multiplexer
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {check_in_multiplexer}
set C_modelType { void 0 }
set C_modelArgList {
	{ rxEng2portTable_chec_1 int 16 regular {fifo 0 volatile } {global 0}  }
	{ pt_portCheckListenin_1 int 15 regular {fifo 1 volatile } {global 1}  }
	{ pt_dstFifo_V int 1 regular {fifo 1 volatile } {global 1}  }
	{ pt_portCheckUsed_req_1 int 15 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "rxEng2portTable_chec_1", "interface" : "fifo", "bitwidth" : 16, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "pt_portCheckListenin_1", "interface" : "fifo", "bitwidth" : 15, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "pt_dstFifo_V", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "pt_portCheckUsed_req_1", "interface" : "fifo", "bitwidth" : 15, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 19
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ rxEng2portTable_chec_1_dout sc_in sc_lv 16 signal 0 } 
	{ rxEng2portTable_chec_1_empty_n sc_in sc_logic 1 signal 0 } 
	{ rxEng2portTable_chec_1_read sc_out sc_logic 1 signal 0 } 
	{ pt_portCheckListenin_1_din sc_out sc_lv 15 signal 1 } 
	{ pt_portCheckListenin_1_full_n sc_in sc_logic 1 signal 1 } 
	{ pt_portCheckListenin_1_write sc_out sc_logic 1 signal 1 } 
	{ pt_dstFifo_V_din sc_out sc_lv 1 signal 2 } 
	{ pt_dstFifo_V_full_n sc_in sc_logic 1 signal 2 } 
	{ pt_dstFifo_V_write sc_out sc_logic 1 signal 2 } 
	{ pt_portCheckUsed_req_1_din sc_out sc_lv 15 signal 3 } 
	{ pt_portCheckUsed_req_1_full_n sc_in sc_logic 1 signal 3 } 
	{ pt_portCheckUsed_req_1_write sc_out sc_logic 1 signal 3 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "rxEng2portTable_chec_1_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "rxEng2portTable_chec_1", "role": "dout" }} , 
 	{ "name": "rxEng2portTable_chec_1_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng2portTable_chec_1", "role": "empty_n" }} , 
 	{ "name": "rxEng2portTable_chec_1_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng2portTable_chec_1", "role": "read" }} , 
 	{ "name": "pt_portCheckListenin_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":15, "type": "signal", "bundle":{"name": "pt_portCheckListenin_1", "role": "din" }} , 
 	{ "name": "pt_portCheckListenin_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "pt_portCheckListenin_1", "role": "full_n" }} , 
 	{ "name": "pt_portCheckListenin_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "pt_portCheckListenin_1", "role": "write" }} , 
 	{ "name": "pt_dstFifo_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "pt_dstFifo_V", "role": "din" }} , 
 	{ "name": "pt_dstFifo_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "pt_dstFifo_V", "role": "full_n" }} , 
 	{ "name": "pt_dstFifo_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "pt_dstFifo_V", "role": "write" }} , 
 	{ "name": "pt_portCheckUsed_req_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":15, "type": "signal", "bundle":{"name": "pt_portCheckUsed_req_1", "role": "din" }} , 
 	{ "name": "pt_portCheckUsed_req_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "pt_portCheckUsed_req_1", "role": "full_n" }} , 
 	{ "name": "pt_portCheckUsed_req_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "pt_portCheckUsed_req_1", "role": "write" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "check_in_multiplexer",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "rxEng2portTable_chec_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng2portTable_chec_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "pt_portCheckListenin_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "pt_portCheckListenin_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "pt_dstFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "pt_dstFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "pt_portCheckUsed_req_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "pt_portCheckUsed_req_1_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	check_in_multiplexer {
		rxEng2portTable_chec_1 {Type I LastRead 0 FirstWrite -1}
		pt_portCheckListenin_1 {Type O LastRead -1 FirstWrite 1}
		pt_dstFifo_V {Type O LastRead -1 FirstWrite 1}
		pt_portCheckUsed_req_1 {Type O LastRead -1 FirstWrite 1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "1", "Max" : "1"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	rxEng2portTable_chec_1 { ap_fifo {  { rxEng2portTable_chec_1_dout fifo_data 0 16 }  { rxEng2portTable_chec_1_empty_n fifo_status 0 1 }  { rxEng2portTable_chec_1_read fifo_update 1 1 } } }
	pt_portCheckListenin_1 { ap_fifo {  { pt_portCheckListenin_1_din fifo_data 1 15 }  { pt_portCheckListenin_1_full_n fifo_status 0 1 }  { pt_portCheckListenin_1_write fifo_update 1 1 } } }
	pt_dstFifo_V { ap_fifo {  { pt_dstFifo_V_din fifo_data 1 1 }  { pt_dstFifo_V_full_n fifo_status 0 1 }  { pt_dstFifo_V_write fifo_update 1 1 } } }
	pt_portCheckUsed_req_1 { ap_fifo {  { pt_portCheckUsed_req_1_din fifo_data 1 15 }  { pt_portCheckUsed_req_1_full_n fifo_status 0 1 }  { pt_portCheckUsed_req_1_write fifo_update 1 1 } } }
}
