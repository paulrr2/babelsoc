set moduleName updateRequestSender
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {updateRequestSender}
set C_modelType { void 0 }
set C_modelArgList {
	{ sessionUpdate_req_V int 88 regular {axi_s 1 volatile  { sessionUpdate_req_V Data } }  }
	{ regSessionCount_V int 16 regular {pointer 1}  }
	{ sessionInsert_req_V_4 int 1 regular {fifo 0 volatile } {global 0}  }
	{ sessionInsert_req_V_1 int 32 regular {fifo 0 volatile } {global 0}  }
	{ sessionInsert_req_V_6 int 16 regular {fifo 0 volatile } {global 0}  }
	{ sessionInsert_req_V_3 int 16 regular {fifo 0 volatile } {global 0}  }
	{ sessionInsert_req_V_s int 16 regular {fifo 0 volatile } {global 0}  }
	{ sessionInsert_req_V_5 int 1 regular {fifo 0 volatile } {global 0}  }
	{ sessionDelete_req_V_4 int 1 regular {fifo 0 volatile } {global 0}  }
	{ sessionDelete_req_V_1 int 32 regular {fifo 0 volatile } {global 0}  }
	{ sessionDelete_req_V_6 int 16 regular {fifo 0 volatile } {global 0}  }
	{ sessionDelete_req_V_3 int 16 regular {fifo 0 volatile } {global 0}  }
	{ sessionDelete_req_V_s int 16 regular {fifo 0 volatile } {global 0}  }
	{ sessionDelete_req_V_5 int 1 regular {fifo 0 volatile } {global 0}  }
	{ slc_sessionIdFinFifo_1 int 14 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "sessionUpdate_req_V", "interface" : "axis", "bitwidth" : 88, "direction" : "WRITEONLY"} , 
 	{ "Name" : "regSessionCount_V", "interface" : "wire", "bitwidth" : 16, "direction" : "WRITEONLY"} , 
 	{ "Name" : "sessionInsert_req_V_4", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "sessionInsert_req_V_1", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "sessionInsert_req_V_6", "interface" : "fifo", "bitwidth" : 16, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "sessionInsert_req_V_3", "interface" : "fifo", "bitwidth" : 16, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "sessionInsert_req_V_s", "interface" : "fifo", "bitwidth" : 16, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "sessionInsert_req_V_5", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "sessionDelete_req_V_4", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "sessionDelete_req_V_1", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "sessionDelete_req_V_6", "interface" : "fifo", "bitwidth" : 16, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "sessionDelete_req_V_3", "interface" : "fifo", "bitwidth" : 16, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "sessionDelete_req_V_s", "interface" : "fifo", "bitwidth" : 16, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "sessionDelete_req_V_5", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "slc_sessionIdFinFifo_1", "interface" : "fifo", "bitwidth" : 14, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 51
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ sessionInsert_req_V_4_dout sc_in sc_lv 1 signal 2 } 
	{ sessionInsert_req_V_4_empty_n sc_in sc_logic 1 signal 2 } 
	{ sessionInsert_req_V_4_read sc_out sc_logic 1 signal 2 } 
	{ sessionInsert_req_V_1_dout sc_in sc_lv 32 signal 3 } 
	{ sessionInsert_req_V_1_empty_n sc_in sc_logic 1 signal 3 } 
	{ sessionInsert_req_V_1_read sc_out sc_logic 1 signal 3 } 
	{ sessionInsert_req_V_6_dout sc_in sc_lv 16 signal 4 } 
	{ sessionInsert_req_V_6_empty_n sc_in sc_logic 1 signal 4 } 
	{ sessionInsert_req_V_6_read sc_out sc_logic 1 signal 4 } 
	{ sessionInsert_req_V_3_dout sc_in sc_lv 16 signal 5 } 
	{ sessionInsert_req_V_3_empty_n sc_in sc_logic 1 signal 5 } 
	{ sessionInsert_req_V_3_read sc_out sc_logic 1 signal 5 } 
	{ sessionInsert_req_V_s_dout sc_in sc_lv 16 signal 6 } 
	{ sessionInsert_req_V_s_empty_n sc_in sc_logic 1 signal 6 } 
	{ sessionInsert_req_V_s_read sc_out sc_logic 1 signal 6 } 
	{ sessionInsert_req_V_5_dout sc_in sc_lv 1 signal 7 } 
	{ sessionInsert_req_V_5_empty_n sc_in sc_logic 1 signal 7 } 
	{ sessionInsert_req_V_5_read sc_out sc_logic 1 signal 7 } 
	{ sessionDelete_req_V_4_dout sc_in sc_lv 1 signal 8 } 
	{ sessionDelete_req_V_4_empty_n sc_in sc_logic 1 signal 8 } 
	{ sessionDelete_req_V_4_read sc_out sc_logic 1 signal 8 } 
	{ sessionDelete_req_V_1_dout sc_in sc_lv 32 signal 9 } 
	{ sessionDelete_req_V_1_empty_n sc_in sc_logic 1 signal 9 } 
	{ sessionDelete_req_V_1_read sc_out sc_logic 1 signal 9 } 
	{ sessionDelete_req_V_6_dout sc_in sc_lv 16 signal 10 } 
	{ sessionDelete_req_V_6_empty_n sc_in sc_logic 1 signal 10 } 
	{ sessionDelete_req_V_6_read sc_out sc_logic 1 signal 10 } 
	{ sessionDelete_req_V_3_dout sc_in sc_lv 16 signal 11 } 
	{ sessionDelete_req_V_3_empty_n sc_in sc_logic 1 signal 11 } 
	{ sessionDelete_req_V_3_read sc_out sc_logic 1 signal 11 } 
	{ sessionDelete_req_V_s_dout sc_in sc_lv 16 signal 12 } 
	{ sessionDelete_req_V_s_empty_n sc_in sc_logic 1 signal 12 } 
	{ sessionDelete_req_V_s_read sc_out sc_logic 1 signal 12 } 
	{ sessionDelete_req_V_5_dout sc_in sc_lv 1 signal 13 } 
	{ sessionDelete_req_V_5_empty_n sc_in sc_logic 1 signal 13 } 
	{ sessionDelete_req_V_5_read sc_out sc_logic 1 signal 13 } 
	{ slc_sessionIdFinFifo_1_din sc_out sc_lv 14 signal 14 } 
	{ slc_sessionIdFinFifo_1_full_n sc_in sc_logic 1 signal 14 } 
	{ slc_sessionIdFinFifo_1_write sc_out sc_logic 1 signal 14 } 
	{ sessionUpdate_req_V_TREADY sc_in sc_logic 1 outacc 0 } 
	{ sessionUpdate_req_V_TDATA sc_out sc_lv 88 signal 0 } 
	{ sessionUpdate_req_V_TVALID sc_out sc_logic 1 outvld 0 } 
	{ regSessionCount_V sc_out sc_lv 16 signal 1 } 
	{ regSessionCount_V_ap_vld sc_out sc_logic 1 outvld 1 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "sessionInsert_req_V_4_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionInsert_req_V_4", "role": "dout" }} , 
 	{ "name": "sessionInsert_req_V_4_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionInsert_req_V_4", "role": "empty_n" }} , 
 	{ "name": "sessionInsert_req_V_4_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionInsert_req_V_4", "role": "read" }} , 
 	{ "name": "sessionInsert_req_V_1_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "sessionInsert_req_V_1", "role": "dout" }} , 
 	{ "name": "sessionInsert_req_V_1_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionInsert_req_V_1", "role": "empty_n" }} , 
 	{ "name": "sessionInsert_req_V_1_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionInsert_req_V_1", "role": "read" }} , 
 	{ "name": "sessionInsert_req_V_6_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "sessionInsert_req_V_6", "role": "dout" }} , 
 	{ "name": "sessionInsert_req_V_6_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionInsert_req_V_6", "role": "empty_n" }} , 
 	{ "name": "sessionInsert_req_V_6_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionInsert_req_V_6", "role": "read" }} , 
 	{ "name": "sessionInsert_req_V_3_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "sessionInsert_req_V_3", "role": "dout" }} , 
 	{ "name": "sessionInsert_req_V_3_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionInsert_req_V_3", "role": "empty_n" }} , 
 	{ "name": "sessionInsert_req_V_3_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionInsert_req_V_3", "role": "read" }} , 
 	{ "name": "sessionInsert_req_V_s_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "sessionInsert_req_V_s", "role": "dout" }} , 
 	{ "name": "sessionInsert_req_V_s_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionInsert_req_V_s", "role": "empty_n" }} , 
 	{ "name": "sessionInsert_req_V_s_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionInsert_req_V_s", "role": "read" }} , 
 	{ "name": "sessionInsert_req_V_5_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionInsert_req_V_5", "role": "dout" }} , 
 	{ "name": "sessionInsert_req_V_5_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionInsert_req_V_5", "role": "empty_n" }} , 
 	{ "name": "sessionInsert_req_V_5_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionInsert_req_V_5", "role": "read" }} , 
 	{ "name": "sessionDelete_req_V_4_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionDelete_req_V_4", "role": "dout" }} , 
 	{ "name": "sessionDelete_req_V_4_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionDelete_req_V_4", "role": "empty_n" }} , 
 	{ "name": "sessionDelete_req_V_4_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionDelete_req_V_4", "role": "read" }} , 
 	{ "name": "sessionDelete_req_V_1_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "sessionDelete_req_V_1", "role": "dout" }} , 
 	{ "name": "sessionDelete_req_V_1_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionDelete_req_V_1", "role": "empty_n" }} , 
 	{ "name": "sessionDelete_req_V_1_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionDelete_req_V_1", "role": "read" }} , 
 	{ "name": "sessionDelete_req_V_6_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "sessionDelete_req_V_6", "role": "dout" }} , 
 	{ "name": "sessionDelete_req_V_6_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionDelete_req_V_6", "role": "empty_n" }} , 
 	{ "name": "sessionDelete_req_V_6_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionDelete_req_V_6", "role": "read" }} , 
 	{ "name": "sessionDelete_req_V_3_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "sessionDelete_req_V_3", "role": "dout" }} , 
 	{ "name": "sessionDelete_req_V_3_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionDelete_req_V_3", "role": "empty_n" }} , 
 	{ "name": "sessionDelete_req_V_3_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionDelete_req_V_3", "role": "read" }} , 
 	{ "name": "sessionDelete_req_V_s_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "sessionDelete_req_V_s", "role": "dout" }} , 
 	{ "name": "sessionDelete_req_V_s_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionDelete_req_V_s", "role": "empty_n" }} , 
 	{ "name": "sessionDelete_req_V_s_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionDelete_req_V_s", "role": "read" }} , 
 	{ "name": "sessionDelete_req_V_5_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionDelete_req_V_5", "role": "dout" }} , 
 	{ "name": "sessionDelete_req_V_5_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionDelete_req_V_5", "role": "empty_n" }} , 
 	{ "name": "sessionDelete_req_V_5_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionDelete_req_V_5", "role": "read" }} , 
 	{ "name": "slc_sessionIdFinFifo_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":14, "type": "signal", "bundle":{"name": "slc_sessionIdFinFifo_1", "role": "din" }} , 
 	{ "name": "slc_sessionIdFinFifo_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionIdFinFifo_1", "role": "full_n" }} , 
 	{ "name": "slc_sessionIdFinFifo_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionIdFinFifo_1", "role": "write" }} , 
 	{ "name": "sessionUpdate_req_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "sessionUpdate_req_V", "role": "TREADY" }} , 
 	{ "name": "sessionUpdate_req_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":88, "type": "signal", "bundle":{"name": "sessionUpdate_req_V", "role": "TDATA" }} , 
 	{ "name": "sessionUpdate_req_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "sessionUpdate_req_V", "role": "TVALID" }} , 
 	{ "name": "regSessionCount_V", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "regSessionCount_V", "role": "default" }} , 
 	{ "name": "regSessionCount_V_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "regSessionCount_V", "role": "ap_vld" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1"],
		"CDFG" : "updateRequestSender",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "3", "EstimateLatencyMin" : "3", "EstimateLatencyMax" : "3",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "sessionUpdate_req_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "sessionUpdate_req_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regSessionCount_V", "Type" : "Vld", "Direction" : "O"},
			{"Name" : "sessionInsert_req_V_4", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "sessionInsert_req_V_4_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "sessionInsert_req_V_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "sessionInsert_req_V_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "sessionInsert_req_V_6", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "sessionInsert_req_V_6_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "sessionInsert_req_V_3", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "sessionInsert_req_V_3_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "sessionInsert_req_V_s", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "sessionInsert_req_V_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "sessionInsert_req_V_5", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "sessionInsert_req_V_5_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "usedSessionIDs_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "sessionDelete_req_V_4", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "sessionDelete_req_V_4_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "sessionDelete_req_V_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "sessionDelete_req_V_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "sessionDelete_req_V_6", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "sessionDelete_req_V_6_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "sessionDelete_req_V_3", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "sessionDelete_req_V_3_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "sessionDelete_req_V_s", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "sessionDelete_req_V_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "sessionDelete_req_V_5", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "sessionDelete_req_V_5_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "slc_sessionIdFinFifo_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "slc_sessionIdFinFifo_1_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_sessionUpdate_req_V_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	updateRequestSender {
		sessionUpdate_req_V {Type O LastRead -1 FirstWrite 2}
		regSessionCount_V {Type O LastRead -1 FirstWrite 3}
		sessionInsert_req_V_4 {Type I LastRead 0 FirstWrite -1}
		sessionInsert_req_V_1 {Type I LastRead 0 FirstWrite -1}
		sessionInsert_req_V_6 {Type I LastRead 0 FirstWrite -1}
		sessionInsert_req_V_3 {Type I LastRead 0 FirstWrite -1}
		sessionInsert_req_V_s {Type I LastRead 0 FirstWrite -1}
		sessionInsert_req_V_5 {Type I LastRead 0 FirstWrite -1}
		usedSessionIDs_V {Type IO LastRead -1 FirstWrite -1}
		sessionDelete_req_V_4 {Type I LastRead 1 FirstWrite -1}
		sessionDelete_req_V_1 {Type I LastRead 1 FirstWrite -1}
		sessionDelete_req_V_6 {Type I LastRead 1 FirstWrite -1}
		sessionDelete_req_V_3 {Type I LastRead 1 FirstWrite -1}
		sessionDelete_req_V_s {Type I LastRead 1 FirstWrite -1}
		sessionDelete_req_V_5 {Type I LastRead 1 FirstWrite -1}
		slc_sessionIdFinFifo_1 {Type O LastRead -1 FirstWrite 2}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "3", "Max" : "3"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	sessionUpdate_req_V { axis {  { sessionUpdate_req_V_TREADY out_acc 0 1 }  { sessionUpdate_req_V_TDATA out_data 1 88 }  { sessionUpdate_req_V_TVALID out_vld 1 1 } } }
	regSessionCount_V { ap_vld {  { regSessionCount_V out_data 1 16 }  { regSessionCount_V_ap_vld out_vld 1 1 } } }
	sessionInsert_req_V_4 { ap_fifo {  { sessionInsert_req_V_4_dout fifo_data 0 1 }  { sessionInsert_req_V_4_empty_n fifo_status 0 1 }  { sessionInsert_req_V_4_read fifo_update 1 1 } } }
	sessionInsert_req_V_1 { ap_fifo {  { sessionInsert_req_V_1_dout fifo_data 0 32 }  { sessionInsert_req_V_1_empty_n fifo_status 0 1 }  { sessionInsert_req_V_1_read fifo_update 1 1 } } }
	sessionInsert_req_V_6 { ap_fifo {  { sessionInsert_req_V_6_dout fifo_data 0 16 }  { sessionInsert_req_V_6_empty_n fifo_status 0 1 }  { sessionInsert_req_V_6_read fifo_update 1 1 } } }
	sessionInsert_req_V_3 { ap_fifo {  { sessionInsert_req_V_3_dout fifo_data 0 16 }  { sessionInsert_req_V_3_empty_n fifo_status 0 1 }  { sessionInsert_req_V_3_read fifo_update 1 1 } } }
	sessionInsert_req_V_s { ap_fifo {  { sessionInsert_req_V_s_dout fifo_data 0 16 }  { sessionInsert_req_V_s_empty_n fifo_status 0 1 }  { sessionInsert_req_V_s_read fifo_update 1 1 } } }
	sessionInsert_req_V_5 { ap_fifo {  { sessionInsert_req_V_5_dout fifo_data 0 1 }  { sessionInsert_req_V_5_empty_n fifo_status 0 1 }  { sessionInsert_req_V_5_read fifo_update 1 1 } } }
	sessionDelete_req_V_4 { ap_fifo {  { sessionDelete_req_V_4_dout fifo_data 0 1 }  { sessionDelete_req_V_4_empty_n fifo_status 0 1 }  { sessionDelete_req_V_4_read fifo_update 1 1 } } }
	sessionDelete_req_V_1 { ap_fifo {  { sessionDelete_req_V_1_dout fifo_data 0 32 }  { sessionDelete_req_V_1_empty_n fifo_status 0 1 }  { sessionDelete_req_V_1_read fifo_update 1 1 } } }
	sessionDelete_req_V_6 { ap_fifo {  { sessionDelete_req_V_6_dout fifo_data 0 16 }  { sessionDelete_req_V_6_empty_n fifo_status 0 1 }  { sessionDelete_req_V_6_read fifo_update 1 1 } } }
	sessionDelete_req_V_3 { ap_fifo {  { sessionDelete_req_V_3_dout fifo_data 0 16 }  { sessionDelete_req_V_3_empty_n fifo_status 0 1 }  { sessionDelete_req_V_3_read fifo_update 1 1 } } }
	sessionDelete_req_V_s { ap_fifo {  { sessionDelete_req_V_s_dout fifo_data 0 16 }  { sessionDelete_req_V_s_empty_n fifo_status 0 1 }  { sessionDelete_req_V_s_read fifo_update 1 1 } } }
	sessionDelete_req_V_5 { ap_fifo {  { sessionDelete_req_V_5_dout fifo_data 0 1 }  { sessionDelete_req_V_5_empty_n fifo_status 0 1 }  { sessionDelete_req_V_5_read fifo_update 1 1 } } }
	slc_sessionIdFinFifo_1 { ap_fifo {  { slc_sessionIdFinFifo_1_din fifo_data 1 14 }  { slc_sessionIdFinFifo_1_full_n fifo_status 0 1 }  { slc_sessionIdFinFifo_1_write fifo_update 1 1 } } }
}
