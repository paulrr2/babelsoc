set moduleName tasi_pkg_pusher_64_s
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {tasi_pkg_pusher<64>}
set C_modelType { void 0 }
set C_modelArgList {
	{ txBufferWriteCmd_V int 72 regular {axi_s 1 volatile  { txBufferWriteCmd_V Data } }  }
	{ txBufferWriteData_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_txwrite_data Data } }  }
	{ txBufferWriteData_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_txwrite_data Keep } }  }
	{ txBufferWriteData_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_txwrite_data Last } }  }
	{ tasi_meta2pkgPushCmd_1 int 72 regular {fifo 0 volatile } {global 0}  }
	{ tasi_dataFifo_V int 73 regular {fifo 0 volatile } {global 0}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "txBufferWriteCmd_V", "interface" : "axis", "bitwidth" : 72, "direction" : "WRITEONLY"} , 
 	{ "Name" : "txBufferWriteData_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY"} , 
 	{ "Name" : "txBufferWriteData_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY"} , 
 	{ "Name" : "txBufferWriteData_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY"} , 
 	{ "Name" : "tasi_meta2pkgPushCmd_1", "interface" : "fifo", "bitwidth" : 72, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "tasi_dataFifo_V", "interface" : "fifo", "bitwidth" : 73, "direction" : "READONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 21
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ tasi_dataFifo_V_dout sc_in sc_lv 73 signal 5 } 
	{ tasi_dataFifo_V_empty_n sc_in sc_logic 1 signal 5 } 
	{ tasi_dataFifo_V_read sc_out sc_logic 1 signal 5 } 
	{ tasi_meta2pkgPushCmd_1_dout sc_in sc_lv 72 signal 4 } 
	{ tasi_meta2pkgPushCmd_1_empty_n sc_in sc_logic 1 signal 4 } 
	{ tasi_meta2pkgPushCmd_1_read sc_out sc_logic 1 signal 4 } 
	{ m_axis_txwrite_data_TREADY sc_in sc_logic 1 outacc 3 } 
	{ txBufferWriteCmd_V_TREADY sc_in sc_logic 1 outacc 0 } 
	{ txBufferWriteCmd_V_TDATA sc_out sc_lv 72 signal 0 } 
	{ txBufferWriteCmd_V_TVALID sc_out sc_logic 1 outvld 0 } 
	{ m_axis_txwrite_data_TDATA sc_out sc_lv 64 signal 1 } 
	{ m_axis_txwrite_data_TVALID sc_out sc_logic 1 outvld 3 } 
	{ m_axis_txwrite_data_TKEEP sc_out sc_lv 8 signal 2 } 
	{ m_axis_txwrite_data_TLAST sc_out sc_lv 1 signal 3 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "tasi_dataFifo_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":73, "type": "signal", "bundle":{"name": "tasi_dataFifo_V", "role": "dout" }} , 
 	{ "name": "tasi_dataFifo_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tasi_dataFifo_V", "role": "empty_n" }} , 
 	{ "name": "tasi_dataFifo_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tasi_dataFifo_V", "role": "read" }} , 
 	{ "name": "tasi_meta2pkgPushCmd_1_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":72, "type": "signal", "bundle":{"name": "tasi_meta2pkgPushCmd_1", "role": "dout" }} , 
 	{ "name": "tasi_meta2pkgPushCmd_1_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tasi_meta2pkgPushCmd_1", "role": "empty_n" }} , 
 	{ "name": "tasi_meta2pkgPushCmd_1_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tasi_meta2pkgPushCmd_1", "role": "read" }} , 
 	{ "name": "m_axis_txwrite_data_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "txBufferWriteData_V_last_V", "role": "default" }} , 
 	{ "name": "txBufferWriteCmd_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "txBufferWriteCmd_V", "role": "TREADY" }} , 
 	{ "name": "txBufferWriteCmd_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":72, "type": "signal", "bundle":{"name": "txBufferWriteCmd_V", "role": "TDATA" }} , 
 	{ "name": "txBufferWriteCmd_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "txBufferWriteCmd_V", "role": "TVALID" }} , 
 	{ "name": "m_axis_txwrite_data_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "txBufferWriteData_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_txwrite_data_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "txBufferWriteData_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_txwrite_data_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "txBufferWriteData_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_txwrite_data_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "txBufferWriteData_V_last_V", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2", "3", "4", "5"],
		"CDFG" : "tasi_pkg_pusher_64_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "3", "EstimateLatencyMin" : "3", "EstimateLatencyMax" : "3",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "txBufferWriteCmd_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "txBufferWriteCmd_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txBufferWriteData_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_txwrite_data_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txBufferWriteData_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "txBufferWriteData_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "tasiPkgPushState", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cmd_bbt_V_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cmd_type_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cmd_dsa_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cmd_eof_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cmd_drr_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cmd_saddr_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cmd_tag_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cmd_rsvd_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "lengthFirstPkg_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "remainingLength_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "offset_V_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "prevWord_data_V_7", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "prevWord_keep_V_8", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tasi_meta2pkgPushCmd_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "tasi_meta2pkgPushCmd_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tasi_dataFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "tasi_dataFifo_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.toe_top_mux_646_6DeQ_U386", "Parent" : "0"},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_txBufferWriteCmd_V_U", "Parent" : "0"},
	{"ID" : "3", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_txBufferWriteData_V_data_V_U", "Parent" : "0"},
	{"ID" : "4", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_txBufferWriteData_V_keep_V_U", "Parent" : "0"},
	{"ID" : "5", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_txBufferWriteData_V_last_V_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	tasi_pkg_pusher_64_s {
		txBufferWriteCmd_V {Type O LastRead -1 FirstWrite 2}
		txBufferWriteData_V_data_V {Type O LastRead -1 FirstWrite 2}
		txBufferWriteData_V_keep_V {Type O LastRead -1 FirstWrite 2}
		txBufferWriteData_V_last_V {Type O LastRead -1 FirstWrite 2}
		tasiPkgPushState {Type IO LastRead -1 FirstWrite -1}
		cmd_bbt_V_1 {Type IO LastRead -1 FirstWrite -1}
		cmd_type_V {Type IO LastRead -1 FirstWrite -1}
		cmd_dsa_V {Type IO LastRead -1 FirstWrite -1}
		cmd_eof_V {Type IO LastRead -1 FirstWrite -1}
		cmd_drr_V {Type IO LastRead -1 FirstWrite -1}
		cmd_saddr_V {Type IO LastRead -1 FirstWrite -1}
		cmd_tag_V {Type IO LastRead -1 FirstWrite -1}
		cmd_rsvd_V {Type IO LastRead -1 FirstWrite -1}
		lengthFirstPkg_V {Type IO LastRead -1 FirstWrite -1}
		remainingLength_V {Type IO LastRead -1 FirstWrite -1}
		offset_V_1 {Type IO LastRead -1 FirstWrite -1}
		prevWord_data_V_7 {Type IO LastRead -1 FirstWrite -1}
		prevWord_keep_V_8 {Type IO LastRead -1 FirstWrite -1}
		tasi_meta2pkgPushCmd_1 {Type I LastRead 0 FirstWrite -1}
		tasi_dataFifo_V {Type I LastRead 0 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "3", "Max" : "3"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	txBufferWriteCmd_V { axis {  { txBufferWriteCmd_V_TREADY out_acc 0 1 }  { txBufferWriteCmd_V_TDATA out_data 1 72 }  { txBufferWriteCmd_V_TVALID out_vld 1 1 } } }
	txBufferWriteData_V_data_V { axis {  { m_axis_txwrite_data_TDATA out_data 1 64 } } }
	txBufferWriteData_V_keep_V { axis {  { m_axis_txwrite_data_TKEEP out_data 1 8 } } }
	txBufferWriteData_V_last_V { axis {  { m_axis_txwrite_data_TREADY out_acc 0 1 }  { m_axis_txwrite_data_TVALID out_vld 1 1 }  { m_axis_txwrite_data_TLAST out_data 1 1 } } }
	tasi_meta2pkgPushCmd_1 { ap_fifo {  { tasi_meta2pkgPushCmd_1_dout fifo_data 0 72 }  { tasi_meta2pkgPushCmd_1_empty_n fifo_status 0 1 }  { tasi_meta2pkgPushCmd_1_read fifo_update 1 1 } } }
	tasi_dataFifo_V { ap_fifo {  { tasi_dataFifo_V_dout fifo_data 0 73 }  { tasi_dataFifo_V_empty_n fifo_status 0 1 }  { tasi_dataFifo_V_read fifo_update 1 1 } } }
}
