set moduleName retransmit_timer
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {retransmit_timer}
set C_modelType { void 0 }
set C_modelArgList {
	{ rxEng2timer_clearRet_1 int 17 regular {fifo 0 volatile } {global 0}  }
	{ txEng2timer_setRetra_1 int 19 regular {fifo 0 volatile } {global 0}  }
	{ rtTimer2eventEng_set_1 int 56 regular {fifo 1 volatile } {global 1}  }
	{ rtTimer2stateTable_r_1 int 16 regular {fifo 1 volatile } {global 1}  }
	{ timer2txApp_notifica_1 int 17 regular {fifo 1 volatile } {global 1}  }
	{ timer2rxApp_notifica_1 int 81 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "rxEng2timer_clearRet_1", "interface" : "fifo", "bitwidth" : 17, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "txEng2timer_setRetra_1", "interface" : "fifo", "bitwidth" : 19, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "rtTimer2eventEng_set_1", "interface" : "fifo", "bitwidth" : 56, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rtTimer2stateTable_r_1", "interface" : "fifo", "bitwidth" : 16, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "timer2txApp_notifica_1", "interface" : "fifo", "bitwidth" : 17, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "timer2rxApp_notifica_1", "interface" : "fifo", "bitwidth" : 81, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 25
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ rxEng2timer_clearRet_1_dout sc_in sc_lv 17 signal 0 } 
	{ rxEng2timer_clearRet_1_empty_n sc_in sc_logic 1 signal 0 } 
	{ rxEng2timer_clearRet_1_read sc_out sc_logic 1 signal 0 } 
	{ txEng2timer_setRetra_1_dout sc_in sc_lv 19 signal 1 } 
	{ txEng2timer_setRetra_1_empty_n sc_in sc_logic 1 signal 1 } 
	{ txEng2timer_setRetra_1_read sc_out sc_logic 1 signal 1 } 
	{ rtTimer2eventEng_set_1_din sc_out sc_lv 56 signal 2 } 
	{ rtTimer2eventEng_set_1_full_n sc_in sc_logic 1 signal 2 } 
	{ rtTimer2eventEng_set_1_write sc_out sc_logic 1 signal 2 } 
	{ rtTimer2stateTable_r_1_din sc_out sc_lv 16 signal 3 } 
	{ rtTimer2stateTable_r_1_full_n sc_in sc_logic 1 signal 3 } 
	{ rtTimer2stateTable_r_1_write sc_out sc_logic 1 signal 3 } 
	{ timer2rxApp_notifica_1_din sc_out sc_lv 81 signal 5 } 
	{ timer2rxApp_notifica_1_full_n sc_in sc_logic 1 signal 5 } 
	{ timer2rxApp_notifica_1_write sc_out sc_logic 1 signal 5 } 
	{ timer2txApp_notifica_1_din sc_out sc_lv 17 signal 4 } 
	{ timer2txApp_notifica_1_full_n sc_in sc_logic 1 signal 4 } 
	{ timer2txApp_notifica_1_write sc_out sc_logic 1 signal 4 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "rxEng2timer_clearRet_1_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":17, "type": "signal", "bundle":{"name": "rxEng2timer_clearRet_1", "role": "dout" }} , 
 	{ "name": "rxEng2timer_clearRet_1_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng2timer_clearRet_1", "role": "empty_n" }} , 
 	{ "name": "rxEng2timer_clearRet_1_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng2timer_clearRet_1", "role": "read" }} , 
 	{ "name": "txEng2timer_setRetra_1_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":19, "type": "signal", "bundle":{"name": "txEng2timer_setRetra_1", "role": "dout" }} , 
 	{ "name": "txEng2timer_setRetra_1_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng2timer_setRetra_1", "role": "empty_n" }} , 
 	{ "name": "txEng2timer_setRetra_1_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng2timer_setRetra_1", "role": "read" }} , 
 	{ "name": "rtTimer2eventEng_set_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":56, "type": "signal", "bundle":{"name": "rtTimer2eventEng_set_1", "role": "din" }} , 
 	{ "name": "rtTimer2eventEng_set_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rtTimer2eventEng_set_1", "role": "full_n" }} , 
 	{ "name": "rtTimer2eventEng_set_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rtTimer2eventEng_set_1", "role": "write" }} , 
 	{ "name": "rtTimer2stateTable_r_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "rtTimer2stateTable_r_1", "role": "din" }} , 
 	{ "name": "rtTimer2stateTable_r_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rtTimer2stateTable_r_1", "role": "full_n" }} , 
 	{ "name": "rtTimer2stateTable_r_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rtTimer2stateTable_r_1", "role": "write" }} , 
 	{ "name": "timer2rxApp_notifica_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":81, "type": "signal", "bundle":{"name": "timer2rxApp_notifica_1", "role": "din" }} , 
 	{ "name": "timer2rxApp_notifica_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "timer2rxApp_notifica_1", "role": "full_n" }} , 
 	{ "name": "timer2rxApp_notifica_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "timer2rxApp_notifica_1", "role": "write" }} , 
 	{ "name": "timer2txApp_notifica_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":17, "type": "signal", "bundle":{"name": "timer2txApp_notifica_1", "role": "din" }} , 
 	{ "name": "timer2txApp_notifica_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "timer2txApp_notifica_1", "role": "full_n" }} , 
 	{ "name": "timer2txApp_notifica_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "timer2txApp_notifica_1", "role": "write" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1"],
		"CDFG" : "retransmit_timer",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "4", "EstimateLatencyMin" : "4", "EstimateLatencyMax" : "4",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"DependenceCheck" : [
			{"FromInitialState" : "ap_enable_state2_pp0_iter1_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter1", "FromInitialOperation" : "ap_enable_operation_52", "FromInitialSV" : "1", "FromFinalState" : "ap_enable_state3_pp0_iter2_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter2", "FromFinalOperation" : "ap_enable_operation_59", "FromFinalSV" : "2", "FromAddress" : "retransmitTimerTable_address0", "FromType" : "R", "ToInitialState" : "ap_enable_state4_pp0_iter3_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter3", "ToInitialNextIteration" : "ap_enable_reg_pp0_iter4", "ToInitialOperation" : "ap_enable_operation_97", "ToInitialSV" : "3", "ToFinalState" : "ap_enable_state4_pp0_iter3_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter3", "ToFinalOperation" : "ap_enable_operation_97", "ToFinalSV" : "3", "ToAddress" : "retransmitTimerTable_address1", "ToType" : "W", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "10", "II" : "1", "Pragma" : "(/home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/toe/retransmit_timer/retransmit_timer.cpp:68:1)", "Type" : "WAR"},
			{"FromInitialState" : "ap_enable_state2_pp0_iter1_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter1", "FromInitialOperation" : "ap_enable_operation_52", "FromInitialSV" : "1", "FromFinalState" : "ap_enable_state3_pp0_iter2_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter2", "FromFinalOperation" : "ap_enable_operation_59", "FromFinalSV" : "2", "FromAddress" : "retransmitTimerTable_address0", "FromType" : "R", "ToInitialState" : "ap_enable_state4_pp0_iter3_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter3", "ToInitialNextIteration" : "ap_enable_reg_pp0_iter4", "ToInitialOperation" : "ap_enable_operation_99", "ToInitialSV" : "3", "ToFinalState" : "ap_enable_state4_pp0_iter3_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter3", "ToFinalOperation" : "ap_enable_operation_99", "ToFinalSV" : "3", "ToAddress" : "retransmitTimerTable_address1", "ToType" : "W", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "10", "II" : "1", "Pragma" : "(/home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/toe/retransmit_timer/retransmit_timer.cpp:68:1)", "Type" : "WAR"},
			{"FromInitialState" : "ap_enable_state2_pp0_iter1_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter1", "FromInitialOperation" : "ap_enable_operation_54", "FromInitialSV" : "1", "FromFinalState" : "ap_enable_state3_pp0_iter2_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter2", "FromFinalOperation" : "ap_enable_operation_75", "FromFinalSV" : "2", "FromAddress" : "retransmitTimerTable_address0", "FromType" : "R", "ToInitialState" : "ap_enable_state4_pp0_iter3_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter3", "ToInitialNextIteration" : "ap_enable_reg_pp0_iter4", "ToInitialOperation" : "ap_enable_operation_97", "ToInitialSV" : "3", "ToFinalState" : "ap_enable_state4_pp0_iter3_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter3", "ToFinalOperation" : "ap_enable_operation_97", "ToFinalSV" : "3", "ToAddress" : "retransmitTimerTable_address1", "ToType" : "W", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "10", "II" : "1", "Pragma" : "(/home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/toe/retransmit_timer/retransmit_timer.cpp:68:1)", "Type" : "WAR"},
			{"FromInitialState" : "ap_enable_state2_pp0_iter1_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter1", "FromInitialOperation" : "ap_enable_operation_54", "FromInitialSV" : "1", "FromFinalState" : "ap_enable_state3_pp0_iter2_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter2", "FromFinalOperation" : "ap_enable_operation_75", "FromFinalSV" : "2", "FromAddress" : "retransmitTimerTable_address0", "FromType" : "R", "ToInitialState" : "ap_enable_state4_pp0_iter3_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter3", "ToInitialNextIteration" : "ap_enable_reg_pp0_iter4", "ToInitialOperation" : "ap_enable_operation_99", "ToInitialSV" : "3", "ToFinalState" : "ap_enable_state4_pp0_iter3_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter3", "ToFinalOperation" : "ap_enable_operation_99", "ToFinalSV" : "3", "ToAddress" : "retransmitTimerTable_address1", "ToType" : "W", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "10", "II" : "1", "Pragma" : "(/home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/toe/retransmit_timer/retransmit_timer.cpp:68:1)", "Type" : "WAR"},
			{"FromInitialState" : "ap_enable_state4_pp0_iter3_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter3", "FromInitialOperation" : "ap_enable_operation_97", "FromInitialSV" : "3", "FromFinalState" : "ap_enable_state4_pp0_iter3_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter3", "FromFinalOperation" : "ap_enable_operation_97", "FromFinalSV" : "3", "FromAddress" : "retransmitTimerTable_address1", "FromType" : "W", "ToInitialState" : "ap_enable_state2_pp0_iter1_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter1", "ToInitialNextIteration" : "ap_enable_reg_pp0_iter2", "ToInitialOperation" : "ap_enable_operation_52", "ToInitialSV" : "1", "ToFinalState" : "ap_enable_state3_pp0_iter2_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter2", "ToFinalOperation" : "ap_enable_operation_59", "ToFinalSV" : "2", "ToAddress" : "retransmitTimerTable_address0", "ToType" : "R", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "10", "II" : "1", "Pragma" : "(/home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/toe/retransmit_timer/retransmit_timer.cpp:68:1)", "Type" : "RAW", "StateEnableSignalListForFifoShift" : ["ap_enable_state2_pp0_iter1_stage0", "ap_enable_state3_pp0_iter2_stage0", "ap_enable_state4_pp0_iter3_stage0"]},
			{"FromInitialState" : "ap_enable_state4_pp0_iter3_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter3", "FromInitialOperation" : "ap_enable_operation_97", "FromInitialSV" : "3", "FromFinalState" : "ap_enable_state4_pp0_iter3_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter3", "FromFinalOperation" : "ap_enable_operation_97", "FromFinalSV" : "3", "FromAddress" : "retransmitTimerTable_address1", "FromType" : "W", "ToInitialState" : "ap_enable_state2_pp0_iter1_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter1", "ToInitialNextIteration" : "ap_enable_reg_pp0_iter2", "ToInitialOperation" : "ap_enable_operation_54", "ToInitialSV" : "1", "ToFinalState" : "ap_enable_state3_pp0_iter2_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter2", "ToFinalOperation" : "ap_enable_operation_75", "ToFinalSV" : "2", "ToAddress" : "retransmitTimerTable_address0", "ToType" : "R", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "10", "II" : "1", "Pragma" : "(/home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/toe/retransmit_timer/retransmit_timer.cpp:68:1)", "Type" : "RAW", "StateEnableSignalListForFifoShift" : ["ap_enable_state2_pp0_iter1_stage0", "ap_enable_state3_pp0_iter2_stage0", "ap_enable_state4_pp0_iter3_stage0"]},
			{"FromInitialState" : "ap_enable_state4_pp0_iter3_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter3", "FromInitialOperation" : "ap_enable_operation_97", "FromInitialSV" : "3", "FromFinalState" : "ap_enable_state4_pp0_iter3_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter3", "FromFinalOperation" : "ap_enable_operation_97", "FromFinalSV" : "3", "FromAddress" : "retransmitTimerTable_address1", "FromType" : "W", "ToInitialState" : "ap_enable_state4_pp0_iter3_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter3", "ToInitialNextIteration" : "ap_enable_reg_pp0_iter4", "ToInitialOperation" : "ap_enable_operation_99", "ToInitialSV" : "3", "ToFinalState" : "ap_enable_state4_pp0_iter3_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter3", "ToFinalOperation" : "ap_enable_operation_99", "ToFinalSV" : "3", "ToAddress" : "retransmitTimerTable_address1", "ToType" : "W", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "10", "II" : "1", "Pragma" : "(/home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/toe/retransmit_timer/retransmit_timer.cpp:68:1)", "Type" : "WAW"},
			{"FromInitialState" : "ap_enable_state4_pp0_iter3_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter3", "FromInitialOperation" : "ap_enable_operation_99", "FromInitialSV" : "3", "FromFinalState" : "ap_enable_state4_pp0_iter3_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter3", "FromFinalOperation" : "ap_enable_operation_99", "FromFinalSV" : "3", "FromAddress" : "retransmitTimerTable_address1", "FromType" : "W", "ToInitialState" : "ap_enable_state2_pp0_iter1_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter1", "ToInitialNextIteration" : "ap_enable_reg_pp0_iter2", "ToInitialOperation" : "ap_enable_operation_52", "ToInitialSV" : "1", "ToFinalState" : "ap_enable_state3_pp0_iter2_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter2", "ToFinalOperation" : "ap_enable_operation_59", "ToFinalSV" : "2", "ToAddress" : "retransmitTimerTable_address0", "ToType" : "R", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "10", "II" : "1", "Pragma" : "(/home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/toe/retransmit_timer/retransmit_timer.cpp:68:1)", "Type" : "RAW", "StateEnableSignalListForFifoShift" : ["ap_enable_state2_pp0_iter1_stage0", "ap_enable_state3_pp0_iter2_stage0", "ap_enable_state4_pp0_iter3_stage0"]},
			{"FromInitialState" : "ap_enable_state4_pp0_iter3_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter3", "FromInitialOperation" : "ap_enable_operation_99", "FromInitialSV" : "3", "FromFinalState" : "ap_enable_state4_pp0_iter3_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter3", "FromFinalOperation" : "ap_enable_operation_99", "FromFinalSV" : "3", "FromAddress" : "retransmitTimerTable_address1", "FromType" : "W", "ToInitialState" : "ap_enable_state2_pp0_iter1_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter1", "ToInitialNextIteration" : "ap_enable_reg_pp0_iter2", "ToInitialOperation" : "ap_enable_operation_54", "ToInitialSV" : "1", "ToFinalState" : "ap_enable_state3_pp0_iter2_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter2", "ToFinalOperation" : "ap_enable_operation_75", "ToFinalSV" : "2", "ToAddress" : "retransmitTimerTable_address0", "ToType" : "R", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "10", "II" : "1", "Pragma" : "(/home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/toe/retransmit_timer/retransmit_timer.cpp:68:1)", "Type" : "RAW", "StateEnableSignalListForFifoShift" : ["ap_enable_state2_pp0_iter1_stage0", "ap_enable_state3_pp0_iter2_stage0", "ap_enable_state4_pp0_iter3_stage0"]},
			{"FromInitialState" : "ap_enable_state4_pp0_iter3_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter3", "FromInitialOperation" : "ap_enable_operation_99", "FromInitialSV" : "3", "FromFinalState" : "ap_enable_state4_pp0_iter3_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter3", "FromFinalOperation" : "ap_enable_operation_99", "FromFinalSV" : "3", "FromAddress" : "retransmitTimerTable_address1", "FromType" : "W", "ToInitialState" : "ap_enable_state4_pp0_iter3_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter3", "ToInitialNextIteration" : "ap_enable_reg_pp0_iter4", "ToInitialOperation" : "ap_enable_operation_97", "ToInitialSV" : "3", "ToFinalState" : "ap_enable_state4_pp0_iter3_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter3", "ToFinalOperation" : "ap_enable_operation_97", "ToFinalSV" : "3", "ToAddress" : "retransmitTimerTable_address1", "ToType" : "W", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "10", "II" : "1", "Pragma" : "(/home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/toe/retransmit_timer/retransmit_timer.cpp:68:1)", "Type" : "WAW"}],
		"Port" : [
			{"Name" : "rt_waitForWrite", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "rt_update_sessionID_s", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "rt_prevPosition_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "rt_update_stop", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "retransmitTimerTable", "Type" : "Memory", "Direction" : "IO"},
			{"Name" : "rxEng2timer_clearRet_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng2timer_clearRet_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rt_position_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "txEng2timer_setRetra_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txEng2timer_setRetra_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rtTimer2eventEng_set_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rtTimer2eventEng_set_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rtTimer2stateTable_r_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rtTimer2stateTable_r_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "timer2txApp_notifica_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "timer2txApp_notifica_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "timer2rxApp_notifica_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "timer2rxApp_notifica_1_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.retransmitTimerTable_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	retransmit_timer {
		rt_waitForWrite {Type IO LastRead -1 FirstWrite -1}
		rt_update_sessionID_s {Type IO LastRead -1 FirstWrite -1}
		rt_prevPosition_V {Type IO LastRead -1 FirstWrite -1}
		rt_update_stop {Type IO LastRead -1 FirstWrite -1}
		retransmitTimerTable {Type IO LastRead -1 FirstWrite -1}
		rxEng2timer_clearRet_1 {Type I LastRead 0 FirstWrite -1}
		rt_position_V {Type IO LastRead -1 FirstWrite -1}
		txEng2timer_setRetra_1 {Type I LastRead 0 FirstWrite -1}
		rtTimer2eventEng_set_1 {Type O LastRead 3 FirstWrite 3}
		rtTimer2stateTable_r_1 {Type O LastRead -1 FirstWrite 4}
		timer2txApp_notifica_1 {Type O LastRead -1 FirstWrite 4}
		timer2rxApp_notifica_1 {Type O LastRead -1 FirstWrite 4}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "4", "Max" : "4"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	rxEng2timer_clearRet_1 { ap_fifo {  { rxEng2timer_clearRet_1_dout fifo_data 0 17 }  { rxEng2timer_clearRet_1_empty_n fifo_status 0 1 }  { rxEng2timer_clearRet_1_read fifo_update 1 1 } } }
	txEng2timer_setRetra_1 { ap_fifo {  { txEng2timer_setRetra_1_dout fifo_data 0 19 }  { txEng2timer_setRetra_1_empty_n fifo_status 0 1 }  { txEng2timer_setRetra_1_read fifo_update 1 1 } } }
	rtTimer2eventEng_set_1 { ap_fifo {  { rtTimer2eventEng_set_1_din fifo_data 1 56 }  { rtTimer2eventEng_set_1_full_n fifo_status 0 1 }  { rtTimer2eventEng_set_1_write fifo_update 1 1 } } }
	rtTimer2stateTable_r_1 { ap_fifo {  { rtTimer2stateTable_r_1_din fifo_data 1 16 }  { rtTimer2stateTable_r_1_full_n fifo_status 0 1 }  { rtTimer2stateTable_r_1_write fifo_update 1 1 } } }
	timer2txApp_notifica_1 { ap_fifo {  { timer2txApp_notifica_1_din fifo_data 1 17 }  { timer2txApp_notifica_1_full_n fifo_status 0 1 }  { timer2txApp_notifica_1_write fifo_update 1 1 } } }
	timer2rxApp_notifica_1 { ap_fifo {  { timer2rxApp_notifica_1_din fifo_data 1 81 }  { timer2rxApp_notifica_1_full_n fifo_status 0 1 }  { timer2rxApp_notifica_1_write fifo_update 1 1 } } }
}
