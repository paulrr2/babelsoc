# This script segment is generated automatically by AutoPilot

# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# Native AXIS:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::native_axis_add] == "::AESL_LIB_XILADAPTER::native_axis_add"} {
eval "::AESL_LIB_XILADAPTER::native_axis_add { \
    id 395 \
    name appOpenConnReq_V \
    reset_level 1 \
    sync_rst true \
    corename {} \
    metadata {  } \
    op interface \
    ports { appOpenConnReq_V_TVALID { I 1 bit } appOpenConnReq_V_TDATA { I 48 vector } appOpenConnReq_V_TREADY { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'appOpenConnReq_V'"
}
}


# Native AXIS:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::native_axis_add] == "::AESL_LIB_XILADAPTER::native_axis_add"} {
eval "::AESL_LIB_XILADAPTER::native_axis_add { \
    id 396 \
    name closeConnReq_V_V \
    reset_level 1 \
    sync_rst true \
    corename {} \
    metadata {  } \
    op interface \
    ports { closeConnReq_V_V_TVALID { I 1 bit } closeConnReq_V_V_TDATA { I 16 vector } closeConnReq_V_V_TREADY { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'closeConnReq_V_V'"
}
}


# Native AXIS:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::native_axis_add] == "::AESL_LIB_XILADAPTER::native_axis_add"} {
eval "::AESL_LIB_XILADAPTER::native_axis_add { \
    id 397 \
    name appOpenConnRsp_V \
    reset_level 1 \
    sync_rst true \
    corename {} \
    metadata {  } \
    op interface \
    ports { appOpenConnRsp_V_TREADY { I 1 bit } appOpenConnRsp_V_TDATA { O 24 vector } appOpenConnRsp_V_TVALID { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'appOpenConnRsp_V'"
}
}


# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 398 \
    name myIpAddress_V \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_myIpAddress_V \
    op interface \
    ports { myIpAddress_V { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 399 \
    name portTable2txApp_port_1 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_portTable2txApp_port_1 \
    op interface \
    ports { portTable2txApp_port_1_dout { I 16 vector } portTable2txApp_port_1_empty_n { I 1 bit } portTable2txApp_port_1_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 400 \
    name txApp2sLookup_req_V \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_txApp2sLookup_req_V \
    op interface \
    ports { txApp2sLookup_req_V_din { O 96 vector } txApp2sLookup_req_V_full_n { I 1 bit } txApp2sLookup_req_V_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 401 \
    name sLookup2txApp_rsp_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_sLookup2txApp_rsp_V \
    op interface \
    ports { sLookup2txApp_rsp_V_dout { I 17 vector } sLookup2txApp_rsp_V_empty_n { I 1 bit } sLookup2txApp_rsp_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 402 \
    name txApp2eventEng_merge_1 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_txApp2eventEng_merge_1 \
    op interface \
    ports { txApp2eventEng_merge_1_din { O 56 vector } txApp2eventEng_merge_1_full_n { I 1 bit } txApp2eventEng_merge_1_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 403 \
    name txApp2stateTable_upd_1 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_txApp2stateTable_upd_1 \
    op interface \
    ports { txApp2stateTable_upd_1_din { O 21 vector } txApp2stateTable_upd_1_full_n { I 1 bit } txApp2stateTable_upd_1_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 404 \
    name conEstablishedFifo_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_conEstablishedFifo_V \
    op interface \
    ports { conEstablishedFifo_V_dout { I 17 vector } conEstablishedFifo_V_empty_n { I 1 bit } conEstablishedFifo_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 405 \
    name timer2txApp_notifica_1 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_timer2txApp_notifica_1 \
    op interface \
    ports { timer2txApp_notifica_1_dout { I 17 vector } timer2txApp_notifica_1_empty_n { I 1 bit } timer2txApp_notifica_1_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 406 \
    name stateTable2txApp_upd_1 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_stateTable2txApp_upd_1 \
    op interface \
    ports { stateTable2txApp_upd_1_dout { I 4 vector } stateTable2txApp_upd_1_empty_n { I 1 bit } stateTable2txApp_upd_1_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_start { I 1 bit } ap_ready { O 1 bit } ap_done { O 1 bit } ap_idle { O 1 bit } ap_continue { I 1 bit } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


# RegSlice definition:
set ID 407
set RegSliceName regslice_core
set RegSliceInstName regslice_core_U
set CoreName ap_simcore_regslice_core
if {${::AESL::PGuard_rtl_comp_handler}} {
	::AP::rtl_comp_handler $RegSliceName
}


if {${::AESL::PGuard_autocg_gen} && ${::AESL::PGuard_autocg_ipmgen}} {
if {[info proc ::AESL_LIB_VIRTEX::xil_gen_regSlice] == "::AESL_LIB_VIRTEX::xil_gen_regSlice"} {
eval "::AESL_LIB_VIRTEX::xil_gen_regSlice { \
    name ${RegSliceName} \
}"
} else {
puts "@W \[IMPL-107\] Cannot find ::AESL_LIB_VIRTEX::xil_gen_regSlice, check your platform lib"
}
}


