set moduleName rx_app_stream_if
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {rx_app_stream_if}
set C_modelType { void 0 }
set C_modelArgList {
	{ appRxDataReq_V int 32 regular {axi_s 0 volatile  { appRxDataReq_V Data } }  }
	{ appRxDataRspMetadata_V_V int 16 regular {axi_s 1 volatile  { appRxDataRspMetadata_V_V Data } }  }
	{ rxApp2rxSar_upd_req_s_19 int 35 regular {fifo 1 volatile } {global 1}  }
	{ rxSar2rxApp_upd_rsp_s_16 int 35 regular {fifo 0 volatile } {global 0}  }
	{ rxBufferReadCmd_V_V int 1 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "appRxDataReq_V", "interface" : "axis", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "appRxDataRspMetadata_V_V", "interface" : "axis", "bitwidth" : 16, "direction" : "WRITEONLY"} , 
 	{ "Name" : "rxApp2rxSar_upd_req_s_19", "interface" : "fifo", "bitwidth" : 35, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rxSar2rxApp_upd_rsp_s_16", "interface" : "fifo", "bitwidth" : 35, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "rxBufferReadCmd_V_V", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 22
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ appRxDataReq_V_TVALID sc_in sc_logic 1 invld 0 } 
	{ rxSar2rxApp_upd_rsp_s_16_dout sc_in sc_lv 35 signal 3 } 
	{ rxSar2rxApp_upd_rsp_s_16_empty_n sc_in sc_logic 1 signal 3 } 
	{ rxSar2rxApp_upd_rsp_s_16_read sc_out sc_logic 1 signal 3 } 
	{ rxApp2rxSar_upd_req_s_19_din sc_out sc_lv 35 signal 2 } 
	{ rxApp2rxSar_upd_req_s_19_full_n sc_in sc_logic 1 signal 2 } 
	{ rxApp2rxSar_upd_req_s_19_write sc_out sc_logic 1 signal 2 } 
	{ rxBufferReadCmd_V_V_din sc_out sc_lv 1 signal 4 } 
	{ rxBufferReadCmd_V_V_full_n sc_in sc_logic 1 signal 4 } 
	{ rxBufferReadCmd_V_V_write sc_out sc_logic 1 signal 4 } 
	{ appRxDataRspMetadata_V_V_TREADY sc_in sc_logic 1 outacc 1 } 
	{ appRxDataReq_V_TDATA sc_in sc_lv 32 signal 0 } 
	{ appRxDataReq_V_TREADY sc_out sc_logic 1 inacc 0 } 
	{ appRxDataRspMetadata_V_V_TDATA sc_out sc_lv 16 signal 1 } 
	{ appRxDataRspMetadata_V_V_TVALID sc_out sc_logic 1 outvld 1 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "appRxDataReq_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "appRxDataReq_V", "role": "TVALID" }} , 
 	{ "name": "rxSar2rxApp_upd_rsp_s_16_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":35, "type": "signal", "bundle":{"name": "rxSar2rxApp_upd_rsp_s_16", "role": "dout" }} , 
 	{ "name": "rxSar2rxApp_upd_rsp_s_16_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxSar2rxApp_upd_rsp_s_16", "role": "empty_n" }} , 
 	{ "name": "rxSar2rxApp_upd_rsp_s_16_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxSar2rxApp_upd_rsp_s_16", "role": "read" }} , 
 	{ "name": "rxApp2rxSar_upd_req_s_19_din", "direction": "out", "datatype": "sc_lv", "bitwidth":35, "type": "signal", "bundle":{"name": "rxApp2rxSar_upd_req_s_19", "role": "din" }} , 
 	{ "name": "rxApp2rxSar_upd_req_s_19_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxApp2rxSar_upd_req_s_19", "role": "full_n" }} , 
 	{ "name": "rxApp2rxSar_upd_req_s_19_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxApp2rxSar_upd_req_s_19", "role": "write" }} , 
 	{ "name": "rxBufferReadCmd_V_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "rxBufferReadCmd_V_V", "role": "din" }} , 
 	{ "name": "rxBufferReadCmd_V_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxBufferReadCmd_V_V", "role": "full_n" }} , 
 	{ "name": "rxBufferReadCmd_V_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxBufferReadCmd_V_V", "role": "write" }} , 
 	{ "name": "appRxDataRspMetadata_V_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "appRxDataRspMetadata_V_V", "role": "TREADY" }} , 
 	{ "name": "appRxDataReq_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "appRxDataReq_V", "role": "TDATA" }} , 
 	{ "name": "appRxDataReq_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "appRxDataReq_V", "role": "TREADY" }} , 
 	{ "name": "appRxDataRspMetadata_V_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "appRxDataRspMetadata_V_V", "role": "TDATA" }} , 
 	{ "name": "appRxDataRspMetadata_V_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "appRxDataRspMetadata_V_V", "role": "TVALID" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1"],
		"CDFG" : "rx_app_stream_if",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "appRxDataReq_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "appRxDataReq_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "appRxDataRspMetadata_V_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "appRxDataRspMetadata_V_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rasi_fsmState_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "rxApp2rxSar_upd_req_s_19", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxApp2rxSar_upd_req_s_19_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rasi_readLength_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "rxSar2rxApp_upd_rsp_s_16", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxSar2rxApp_upd_rsp_s_16_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxBufferReadCmd_V_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxBufferReadCmd_V_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_appRxDataRspMetadata_V_V_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	rx_app_stream_if {
		appRxDataReq_V {Type I LastRead 0 FirstWrite -1}
		appRxDataRspMetadata_V_V {Type O LastRead -1 FirstWrite 1}
		rasi_fsmState_V {Type IO LastRead -1 FirstWrite -1}
		rxApp2rxSar_upd_req_s_19 {Type O LastRead -1 FirstWrite 1}
		rasi_readLength_V {Type IO LastRead -1 FirstWrite -1}
		rxSar2rxApp_upd_rsp_s_16 {Type I LastRead 0 FirstWrite -1}
		rxBufferReadCmd_V_V {Type O LastRead -1 FirstWrite 1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "2", "Max" : "2"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	appRxDataReq_V { axis {  { appRxDataReq_V_TVALID in_vld 0 1 }  { appRxDataReq_V_TDATA in_data 0 32 }  { appRxDataReq_V_TREADY in_acc 1 1 } } }
	appRxDataRspMetadata_V_V { axis {  { appRxDataRspMetadata_V_V_TREADY out_acc 0 1 }  { appRxDataRspMetadata_V_V_TDATA out_data 1 16 }  { appRxDataRspMetadata_V_V_TVALID out_vld 1 1 } } }
	rxApp2rxSar_upd_req_s_19 { ap_fifo {  { rxApp2rxSar_upd_req_s_19_din fifo_data 1 35 }  { rxApp2rxSar_upd_req_s_19_full_n fifo_status 0 1 }  { rxApp2rxSar_upd_req_s_19_write fifo_update 1 1 } } }
	rxSar2rxApp_upd_rsp_s_16 { ap_fifo {  { rxSar2rxApp_upd_rsp_s_16_dout fifo_data 0 35 }  { rxSar2rxApp_upd_rsp_s_16_empty_n fifo_status 0 1 }  { rxSar2rxApp_upd_rsp_s_16_read fifo_update 1 1 } } }
	rxBufferReadCmd_V_V { ap_fifo {  { rxBufferReadCmd_V_V_din fifo_data 1 1 }  { rxBufferReadCmd_V_V_full_n fifo_status 0 1 }  { rxBufferReadCmd_V_V_write fifo_update 1 1 } } }
}
