set moduleName processPseudoHeader
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {processPseudoHeader}
set C_modelType { void 0 }
set C_modelArgList {
	{ rxEng_dataBuffer2_V int 73 regular {fifo 0 volatile } {global 0}  }
	{ rxEng_checksumValidF_1 int 1 regular {fifo 0 volatile } {global 0}  }
	{ rxEng_dataBuffer3a_V int 73 regular {fifo 1 volatile } {global 1}  }
	{ rxEng_headerMetaFifo_20 int 32 regular {fifo 1 volatile } {global 1}  }
	{ rxEng_headerMetaFifo_12 int 32 regular {fifo 1 volatile } {global 1}  }
	{ rxEng_headerMetaFifo_23 int 16 regular {fifo 1 volatile } {global 1}  }
	{ rxEng_headerMetaFifo_22 int 4 regular {fifo 1 volatile } {global 1}  }
	{ rxEng_headerMetaFifo_18 int 16 regular {fifo 1 volatile } {global 1}  }
	{ rxEng_headerMetaFifo_10 int 1 regular {fifo 1 volatile } {global 1}  }
	{ rxEng_headerMetaFifo_19 int 1 regular {fifo 1 volatile } {global 1}  }
	{ rxEng_headerMetaFifo_21 int 1 regular {fifo 1 volatile } {global 1}  }
	{ rxEng_headerMetaFifo_16 int 1 regular {fifo 1 volatile } {global 1}  }
	{ rxEng_headerMetaFifo_14 int 4 regular {fifo 1 volatile } {global 1}  }
	{ rxEng2portTable_chec_1 int 16 regular {fifo 1 volatile } {global 1}  }
	{ rxEng_tupleBuffer_V int 96 regular {fifo 1 volatile } {global 1}  }
	{ rxEng_optionalFields_4 int 4 regular {fifo 1 volatile } {global 1}  }
	{ rxEng_optionalFields_5 int 1 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "rxEng_dataBuffer2_V", "interface" : "fifo", "bitwidth" : 73, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_checksumValidF_1", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_dataBuffer3a_V", "interface" : "fifo", "bitwidth" : 73, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_headerMetaFifo_20", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_headerMetaFifo_12", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_headerMetaFifo_23", "interface" : "fifo", "bitwidth" : 16, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_headerMetaFifo_22", "interface" : "fifo", "bitwidth" : 4, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_headerMetaFifo_18", "interface" : "fifo", "bitwidth" : 16, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_headerMetaFifo_10", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_headerMetaFifo_19", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_headerMetaFifo_21", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_headerMetaFifo_16", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_headerMetaFifo_14", "interface" : "fifo", "bitwidth" : 4, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rxEng2portTable_chec_1", "interface" : "fifo", "bitwidth" : 16, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_tupleBuffer_V", "interface" : "fifo", "bitwidth" : 96, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_optionalFields_4", "interface" : "fifo", "bitwidth" : 4, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_optionalFields_5", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 58
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ rxEng_dataBuffer2_V_dout sc_in sc_lv 73 signal 0 } 
	{ rxEng_dataBuffer2_V_empty_n sc_in sc_logic 1 signal 0 } 
	{ rxEng_dataBuffer2_V_read sc_out sc_logic 1 signal 0 } 
	{ rxEng_checksumValidF_1_dout sc_in sc_lv 1 signal 1 } 
	{ rxEng_checksumValidF_1_empty_n sc_in sc_logic 1 signal 1 } 
	{ rxEng_checksumValidF_1_read sc_out sc_logic 1 signal 1 } 
	{ rxEng_dataBuffer3a_V_din sc_out sc_lv 73 signal 2 } 
	{ rxEng_dataBuffer3a_V_full_n sc_in sc_logic 1 signal 2 } 
	{ rxEng_dataBuffer3a_V_write sc_out sc_logic 1 signal 2 } 
	{ rxEng_headerMetaFifo_20_din sc_out sc_lv 32 signal 3 } 
	{ rxEng_headerMetaFifo_20_full_n sc_in sc_logic 1 signal 3 } 
	{ rxEng_headerMetaFifo_20_write sc_out sc_logic 1 signal 3 } 
	{ rxEng_headerMetaFifo_12_din sc_out sc_lv 32 signal 4 } 
	{ rxEng_headerMetaFifo_12_full_n sc_in sc_logic 1 signal 4 } 
	{ rxEng_headerMetaFifo_12_write sc_out sc_logic 1 signal 4 } 
	{ rxEng_headerMetaFifo_23_din sc_out sc_lv 16 signal 5 } 
	{ rxEng_headerMetaFifo_23_full_n sc_in sc_logic 1 signal 5 } 
	{ rxEng_headerMetaFifo_23_write sc_out sc_logic 1 signal 5 } 
	{ rxEng_headerMetaFifo_22_din sc_out sc_lv 4 signal 6 } 
	{ rxEng_headerMetaFifo_22_full_n sc_in sc_logic 1 signal 6 } 
	{ rxEng_headerMetaFifo_22_write sc_out sc_logic 1 signal 6 } 
	{ rxEng_headerMetaFifo_18_din sc_out sc_lv 16 signal 7 } 
	{ rxEng_headerMetaFifo_18_full_n sc_in sc_logic 1 signal 7 } 
	{ rxEng_headerMetaFifo_18_write sc_out sc_logic 1 signal 7 } 
	{ rxEng_headerMetaFifo_10_din sc_out sc_lv 1 signal 8 } 
	{ rxEng_headerMetaFifo_10_full_n sc_in sc_logic 1 signal 8 } 
	{ rxEng_headerMetaFifo_10_write sc_out sc_logic 1 signal 8 } 
	{ rxEng_headerMetaFifo_19_din sc_out sc_lv 1 signal 9 } 
	{ rxEng_headerMetaFifo_19_full_n sc_in sc_logic 1 signal 9 } 
	{ rxEng_headerMetaFifo_19_write sc_out sc_logic 1 signal 9 } 
	{ rxEng_headerMetaFifo_21_din sc_out sc_lv 1 signal 10 } 
	{ rxEng_headerMetaFifo_21_full_n sc_in sc_logic 1 signal 10 } 
	{ rxEng_headerMetaFifo_21_write sc_out sc_logic 1 signal 10 } 
	{ rxEng_headerMetaFifo_16_din sc_out sc_lv 1 signal 11 } 
	{ rxEng_headerMetaFifo_16_full_n sc_in sc_logic 1 signal 11 } 
	{ rxEng_headerMetaFifo_16_write sc_out sc_logic 1 signal 11 } 
	{ rxEng_headerMetaFifo_14_din sc_out sc_lv 4 signal 12 } 
	{ rxEng_headerMetaFifo_14_full_n sc_in sc_logic 1 signal 12 } 
	{ rxEng_headerMetaFifo_14_write sc_out sc_logic 1 signal 12 } 
	{ rxEng2portTable_chec_1_din sc_out sc_lv 16 signal 13 } 
	{ rxEng2portTable_chec_1_full_n sc_in sc_logic 1 signal 13 } 
	{ rxEng2portTable_chec_1_write sc_out sc_logic 1 signal 13 } 
	{ rxEng_tupleBuffer_V_din sc_out sc_lv 96 signal 14 } 
	{ rxEng_tupleBuffer_V_full_n sc_in sc_logic 1 signal 14 } 
	{ rxEng_tupleBuffer_V_write sc_out sc_logic 1 signal 14 } 
	{ rxEng_optionalFields_4_din sc_out sc_lv 4 signal 15 } 
	{ rxEng_optionalFields_4_full_n sc_in sc_logic 1 signal 15 } 
	{ rxEng_optionalFields_4_write sc_out sc_logic 1 signal 15 } 
	{ rxEng_optionalFields_5_din sc_out sc_lv 1 signal 16 } 
	{ rxEng_optionalFields_5_full_n sc_in sc_logic 1 signal 16 } 
	{ rxEng_optionalFields_5_write sc_out sc_logic 1 signal 16 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "rxEng_dataBuffer2_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":73, "type": "signal", "bundle":{"name": "rxEng_dataBuffer2_V", "role": "dout" }} , 
 	{ "name": "rxEng_dataBuffer2_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_dataBuffer2_V", "role": "empty_n" }} , 
 	{ "name": "rxEng_dataBuffer2_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_dataBuffer2_V", "role": "read" }} , 
 	{ "name": "rxEng_checksumValidF_1_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_checksumValidF_1", "role": "dout" }} , 
 	{ "name": "rxEng_checksumValidF_1_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_checksumValidF_1", "role": "empty_n" }} , 
 	{ "name": "rxEng_checksumValidF_1_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_checksumValidF_1", "role": "read" }} , 
 	{ "name": "rxEng_dataBuffer3a_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":73, "type": "signal", "bundle":{"name": "rxEng_dataBuffer3a_V", "role": "din" }} , 
 	{ "name": "rxEng_dataBuffer3a_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_dataBuffer3a_V", "role": "full_n" }} , 
 	{ "name": "rxEng_dataBuffer3a_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_dataBuffer3a_V", "role": "write" }} , 
 	{ "name": "rxEng_headerMetaFifo_20_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_20", "role": "din" }} , 
 	{ "name": "rxEng_headerMetaFifo_20_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_20", "role": "full_n" }} , 
 	{ "name": "rxEng_headerMetaFifo_20_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_20", "role": "write" }} , 
 	{ "name": "rxEng_headerMetaFifo_12_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_12", "role": "din" }} , 
 	{ "name": "rxEng_headerMetaFifo_12_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_12", "role": "full_n" }} , 
 	{ "name": "rxEng_headerMetaFifo_12_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_12", "role": "write" }} , 
 	{ "name": "rxEng_headerMetaFifo_23_din", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_23", "role": "din" }} , 
 	{ "name": "rxEng_headerMetaFifo_23_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_23", "role": "full_n" }} , 
 	{ "name": "rxEng_headerMetaFifo_23_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_23", "role": "write" }} , 
 	{ "name": "rxEng_headerMetaFifo_22_din", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_22", "role": "din" }} , 
 	{ "name": "rxEng_headerMetaFifo_22_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_22", "role": "full_n" }} , 
 	{ "name": "rxEng_headerMetaFifo_22_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_22", "role": "write" }} , 
 	{ "name": "rxEng_headerMetaFifo_18_din", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_18", "role": "din" }} , 
 	{ "name": "rxEng_headerMetaFifo_18_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_18", "role": "full_n" }} , 
 	{ "name": "rxEng_headerMetaFifo_18_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_18", "role": "write" }} , 
 	{ "name": "rxEng_headerMetaFifo_10_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_10", "role": "din" }} , 
 	{ "name": "rxEng_headerMetaFifo_10_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_10", "role": "full_n" }} , 
 	{ "name": "rxEng_headerMetaFifo_10_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_10", "role": "write" }} , 
 	{ "name": "rxEng_headerMetaFifo_19_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_19", "role": "din" }} , 
 	{ "name": "rxEng_headerMetaFifo_19_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_19", "role": "full_n" }} , 
 	{ "name": "rxEng_headerMetaFifo_19_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_19", "role": "write" }} , 
 	{ "name": "rxEng_headerMetaFifo_21_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_21", "role": "din" }} , 
 	{ "name": "rxEng_headerMetaFifo_21_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_21", "role": "full_n" }} , 
 	{ "name": "rxEng_headerMetaFifo_21_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_21", "role": "write" }} , 
 	{ "name": "rxEng_headerMetaFifo_16_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_16", "role": "din" }} , 
 	{ "name": "rxEng_headerMetaFifo_16_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_16", "role": "full_n" }} , 
 	{ "name": "rxEng_headerMetaFifo_16_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_16", "role": "write" }} , 
 	{ "name": "rxEng_headerMetaFifo_14_din", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_14", "role": "din" }} , 
 	{ "name": "rxEng_headerMetaFifo_14_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_14", "role": "full_n" }} , 
 	{ "name": "rxEng_headerMetaFifo_14_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_14", "role": "write" }} , 
 	{ "name": "rxEng2portTable_chec_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "rxEng2portTable_chec_1", "role": "din" }} , 
 	{ "name": "rxEng2portTable_chec_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng2portTable_chec_1", "role": "full_n" }} , 
 	{ "name": "rxEng2portTable_chec_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng2portTable_chec_1", "role": "write" }} , 
 	{ "name": "rxEng_tupleBuffer_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":96, "type": "signal", "bundle":{"name": "rxEng_tupleBuffer_V", "role": "din" }} , 
 	{ "name": "rxEng_tupleBuffer_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_tupleBuffer_V", "role": "full_n" }} , 
 	{ "name": "rxEng_tupleBuffer_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_tupleBuffer_V", "role": "write" }} , 
 	{ "name": "rxEng_optionalFields_4_din", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "rxEng_optionalFields_4", "role": "din" }} , 
 	{ "name": "rxEng_optionalFields_4_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_optionalFields_4", "role": "full_n" }} , 
 	{ "name": "rxEng_optionalFields_4_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_optionalFields_4", "role": "write" }} , 
 	{ "name": "rxEng_optionalFields_5_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_optionalFields_5", "role": "din" }} , 
 	{ "name": "rxEng_optionalFields_5_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_optionalFields_5", "role": "full_n" }} , 
 	{ "name": "rxEng_optionalFields_5_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_optionalFields_5", "role": "write" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "processPseudoHeader",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "5", "EstimateLatencyMin" : "5", "EstimateLatencyMax" : "5",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "rxEng_dataBuffer2_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_dataBuffer2_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "firstWord_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "rxEng_checksumValidF_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_checksumValidF_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "header_ready_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_idx_6", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_header_V_5", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "pkgValid", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "metaWritten_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "rxEng_dataBuffer3a_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_dataBuffer3a_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng_headerMetaFifo_20", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_headerMetaFifo_20_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng_headerMetaFifo_12", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_headerMetaFifo_12_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng_headerMetaFifo_23", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_headerMetaFifo_23_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng_headerMetaFifo_22", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_headerMetaFifo_22_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng_headerMetaFifo_18", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_headerMetaFifo_18_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng_headerMetaFifo_10", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_headerMetaFifo_10_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng_headerMetaFifo_19", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_headerMetaFifo_19_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng_headerMetaFifo_21", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_headerMetaFifo_21_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng_headerMetaFifo_16", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_headerMetaFifo_16_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng_headerMetaFifo_14", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_headerMetaFifo_14_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng2portTable_chec_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng2portTable_chec_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng_tupleBuffer_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_tupleBuffer_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng_optionalFields_4", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_optionalFields_4_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng_optionalFields_5", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_optionalFields_5_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	processPseudoHeader {
		rxEng_dataBuffer2_V {Type I LastRead 0 FirstWrite -1}
		firstWord_1 {Type IO LastRead -1 FirstWrite -1}
		rxEng_checksumValidF_1 {Type I LastRead 0 FirstWrite -1}
		header_ready_1 {Type IO LastRead -1 FirstWrite -1}
		header_idx_6 {Type IO LastRead -1 FirstWrite -1}
		header_header_V_5 {Type IO LastRead -1 FirstWrite -1}
		pkgValid {Type IO LastRead -1 FirstWrite -1}
		metaWritten_1 {Type IO LastRead -1 FirstWrite -1}
		rxEng_dataBuffer3a_V {Type O LastRead -1 FirstWrite 2}
		rxEng_headerMetaFifo_20 {Type O LastRead -1 FirstWrite 4}
		rxEng_headerMetaFifo_12 {Type O LastRead -1 FirstWrite 4}
		rxEng_headerMetaFifo_23 {Type O LastRead -1 FirstWrite 4}
		rxEng_headerMetaFifo_22 {Type O LastRead -1 FirstWrite 4}
		rxEng_headerMetaFifo_18 {Type O LastRead -1 FirstWrite 4}
		rxEng_headerMetaFifo_10 {Type O LastRead -1 FirstWrite 4}
		rxEng_headerMetaFifo_19 {Type O LastRead -1 FirstWrite 4}
		rxEng_headerMetaFifo_21 {Type O LastRead -1 FirstWrite 4}
		rxEng_headerMetaFifo_16 {Type O LastRead -1 FirstWrite 4}
		rxEng_headerMetaFifo_14 {Type O LastRead -1 FirstWrite 4}
		rxEng2portTable_chec_1 {Type O LastRead -1 FirstWrite 4}
		rxEng_tupleBuffer_V {Type O LastRead -1 FirstWrite 4}
		rxEng_optionalFields_4 {Type O LastRead -1 FirstWrite 5}
		rxEng_optionalFields_5 {Type O LastRead -1 FirstWrite 5}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "5", "Max" : "5"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	rxEng_dataBuffer2_V { ap_fifo {  { rxEng_dataBuffer2_V_dout fifo_data 0 73 }  { rxEng_dataBuffer2_V_empty_n fifo_status 0 1 }  { rxEng_dataBuffer2_V_read fifo_update 1 1 } } }
	rxEng_checksumValidF_1 { ap_fifo {  { rxEng_checksumValidF_1_dout fifo_data 0 1 }  { rxEng_checksumValidF_1_empty_n fifo_status 0 1 }  { rxEng_checksumValidF_1_read fifo_update 1 1 } } }
	rxEng_dataBuffer3a_V { ap_fifo {  { rxEng_dataBuffer3a_V_din fifo_data 1 73 }  { rxEng_dataBuffer3a_V_full_n fifo_status 0 1 }  { rxEng_dataBuffer3a_V_write fifo_update 1 1 } } }
	rxEng_headerMetaFifo_20 { ap_fifo {  { rxEng_headerMetaFifo_20_din fifo_data 1 32 }  { rxEng_headerMetaFifo_20_full_n fifo_status 0 1 }  { rxEng_headerMetaFifo_20_write fifo_update 1 1 } } }
	rxEng_headerMetaFifo_12 { ap_fifo {  { rxEng_headerMetaFifo_12_din fifo_data 1 32 }  { rxEng_headerMetaFifo_12_full_n fifo_status 0 1 }  { rxEng_headerMetaFifo_12_write fifo_update 1 1 } } }
	rxEng_headerMetaFifo_23 { ap_fifo {  { rxEng_headerMetaFifo_23_din fifo_data 1 16 }  { rxEng_headerMetaFifo_23_full_n fifo_status 0 1 }  { rxEng_headerMetaFifo_23_write fifo_update 1 1 } } }
	rxEng_headerMetaFifo_22 { ap_fifo {  { rxEng_headerMetaFifo_22_din fifo_data 1 4 }  { rxEng_headerMetaFifo_22_full_n fifo_status 0 1 }  { rxEng_headerMetaFifo_22_write fifo_update 1 1 } } }
	rxEng_headerMetaFifo_18 { ap_fifo {  { rxEng_headerMetaFifo_18_din fifo_data 1 16 }  { rxEng_headerMetaFifo_18_full_n fifo_status 0 1 }  { rxEng_headerMetaFifo_18_write fifo_update 1 1 } } }
	rxEng_headerMetaFifo_10 { ap_fifo {  { rxEng_headerMetaFifo_10_din fifo_data 1 1 }  { rxEng_headerMetaFifo_10_full_n fifo_status 0 1 }  { rxEng_headerMetaFifo_10_write fifo_update 1 1 } } }
	rxEng_headerMetaFifo_19 { ap_fifo {  { rxEng_headerMetaFifo_19_din fifo_data 1 1 }  { rxEng_headerMetaFifo_19_full_n fifo_status 0 1 }  { rxEng_headerMetaFifo_19_write fifo_update 1 1 } } }
	rxEng_headerMetaFifo_21 { ap_fifo {  { rxEng_headerMetaFifo_21_din fifo_data 1 1 }  { rxEng_headerMetaFifo_21_full_n fifo_status 0 1 }  { rxEng_headerMetaFifo_21_write fifo_update 1 1 } } }
	rxEng_headerMetaFifo_16 { ap_fifo {  { rxEng_headerMetaFifo_16_din fifo_data 1 1 }  { rxEng_headerMetaFifo_16_full_n fifo_status 0 1 }  { rxEng_headerMetaFifo_16_write fifo_update 1 1 } } }
	rxEng_headerMetaFifo_14 { ap_fifo {  { rxEng_headerMetaFifo_14_din fifo_data 1 4 }  { rxEng_headerMetaFifo_14_full_n fifo_status 0 1 }  { rxEng_headerMetaFifo_14_write fifo_update 1 1 } } }
	rxEng2portTable_chec_1 { ap_fifo {  { rxEng2portTable_chec_1_din fifo_data 1 16 }  { rxEng2portTable_chec_1_full_n fifo_status 0 1 }  { rxEng2portTable_chec_1_write fifo_update 1 1 } } }
	rxEng_tupleBuffer_V { ap_fifo {  { rxEng_tupleBuffer_V_din fifo_data 1 96 }  { rxEng_tupleBuffer_V_full_n fifo_status 0 1 }  { rxEng_tupleBuffer_V_write fifo_update 1 1 } } }
	rxEng_optionalFields_4 { ap_fifo {  { rxEng_optionalFields_4_din fifo_data 1 4 }  { rxEng_optionalFields_4_full_n fifo_status 0 1 }  { rxEng_optionalFields_4_write fifo_update 1 1 } } }
	rxEng_optionalFields_5 { ap_fifo {  { rxEng_optionalFields_5_din fifo_data 1 1 }  { rxEng_optionalFields_5_full_n fifo_status 0 1 }  { rxEng_optionalFields_5_write fifo_update 1 1 } } }
}
