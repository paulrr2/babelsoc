set moduleName duplicate_stream
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {duplicate_stream}
set C_modelType { void 0 }
set C_modelArgList {
	{ in_V_data_V int 64 regular {axi_s 0 volatile  { s_axis_tx_data_req Data } }  }
	{ in_V_keep_V int 8 regular {axi_s 0 volatile  { s_axis_tx_data_req Keep } }  }
	{ in_V_last_V int 1 regular {axi_s 0 volatile  { s_axis_tx_data_req Last } }  }
	{ tasi_dataFifo_V int 73 regular {fifo 1 volatile } {global 1}  }
	{ txApp2txEng_data_str_3 int 64 regular {fifo 1 volatile } {global 1}  }
	{ txApp2txEng_data_str_5 int 8 regular {fifo 1 volatile } {global 1}  }
	{ txApp2txEng_data_str_6 int 1 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "in_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "READONLY"} , 
 	{ "Name" : "in_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "in_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY"} , 
 	{ "Name" : "tasi_dataFifo_V", "interface" : "fifo", "bitwidth" : 73, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "txApp2txEng_data_str_3", "interface" : "fifo", "bitwidth" : 64, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "txApp2txEng_data_str_5", "interface" : "fifo", "bitwidth" : 8, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "txApp2txEng_data_str_6", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 24
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ s_axis_tx_data_req_TVALID sc_in sc_logic 1 invld 0 } 
	{ tasi_dataFifo_V_din sc_out sc_lv 73 signal 3 } 
	{ tasi_dataFifo_V_full_n sc_in sc_logic 1 signal 3 } 
	{ tasi_dataFifo_V_write sc_out sc_logic 1 signal 3 } 
	{ txApp2txEng_data_str_3_din sc_out sc_lv 64 signal 4 } 
	{ txApp2txEng_data_str_3_full_n sc_in sc_logic 1 signal 4 } 
	{ txApp2txEng_data_str_3_write sc_out sc_logic 1 signal 4 } 
	{ txApp2txEng_data_str_5_din sc_out sc_lv 8 signal 5 } 
	{ txApp2txEng_data_str_5_full_n sc_in sc_logic 1 signal 5 } 
	{ txApp2txEng_data_str_5_write sc_out sc_logic 1 signal 5 } 
	{ txApp2txEng_data_str_6_din sc_out sc_lv 1 signal 6 } 
	{ txApp2txEng_data_str_6_full_n sc_in sc_logic 1 signal 6 } 
	{ txApp2txEng_data_str_6_write sc_out sc_logic 1 signal 6 } 
	{ s_axis_tx_data_req_TDATA sc_in sc_lv 64 signal 0 } 
	{ s_axis_tx_data_req_TREADY sc_out sc_logic 1 inacc 2 } 
	{ s_axis_tx_data_req_TKEEP sc_in sc_lv 8 signal 1 } 
	{ s_axis_tx_data_req_TLAST sc_in sc_lv 1 signal 2 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "s_axis_tx_data_req_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "in_V_data_V", "role": "ta_req_TVALID" }} , 
 	{ "name": "tasi_dataFifo_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":73, "type": "signal", "bundle":{"name": "tasi_dataFifo_V", "role": "din" }} , 
 	{ "name": "tasi_dataFifo_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tasi_dataFifo_V", "role": "full_n" }} , 
 	{ "name": "tasi_dataFifo_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tasi_dataFifo_V", "role": "write" }} , 
 	{ "name": "txApp2txEng_data_str_3_din", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "txApp2txEng_data_str_3", "role": "din" }} , 
 	{ "name": "txApp2txEng_data_str_3_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txApp2txEng_data_str_3", "role": "full_n" }} , 
 	{ "name": "txApp2txEng_data_str_3_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txApp2txEng_data_str_3", "role": "write" }} , 
 	{ "name": "txApp2txEng_data_str_5_din", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "txApp2txEng_data_str_5", "role": "din" }} , 
 	{ "name": "txApp2txEng_data_str_5_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txApp2txEng_data_str_5", "role": "full_n" }} , 
 	{ "name": "txApp2txEng_data_str_5_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txApp2txEng_data_str_5", "role": "write" }} , 
 	{ "name": "txApp2txEng_data_str_6_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "txApp2txEng_data_str_6", "role": "din" }} , 
 	{ "name": "txApp2txEng_data_str_6_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txApp2txEng_data_str_6", "role": "full_n" }} , 
 	{ "name": "txApp2txEng_data_str_6_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txApp2txEng_data_str_6", "role": "write" }} , 
 	{ "name": "s_axis_tx_data_req_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "in_V_data_V", "role": "ta_req_TDATA" }} , 
 	{ "name": "s_axis_tx_data_req_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "in_V_last_V", "role": "ta_req_TREADY" }} , 
 	{ "name": "s_axis_tx_data_req_TKEEP", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "in_V_keep_V", "role": "ta_req_TKEEP" }} , 
 	{ "name": "s_axis_tx_data_req_TLAST", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "in_V_last_V", "role": "ta_req_TLAST" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "duplicate_stream",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "in_V_data_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "s_axis_tx_data_req_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "in_V_keep_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "in_V_last_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "tasi_dataFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "tasi_dataFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txApp2txEng_data_str_3", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txApp2txEng_data_str_3_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txApp2txEng_data_str_5", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txApp2txEng_data_str_5_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txApp2txEng_data_str_6", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txApp2txEng_data_str_6_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	duplicate_stream {
		in_V_data_V {Type I LastRead 0 FirstWrite -1}
		in_V_keep_V {Type I LastRead 0 FirstWrite -1}
		in_V_last_V {Type I LastRead 0 FirstWrite -1}
		tasi_dataFifo_V {Type O LastRead -1 FirstWrite 1}
		txApp2txEng_data_str_3 {Type O LastRead -1 FirstWrite 1}
		txApp2txEng_data_str_5 {Type O LastRead -1 FirstWrite 1}
		txApp2txEng_data_str_6 {Type O LastRead -1 FirstWrite 1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "1", "Max" : "1"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	in_V_data_V { axis {  { s_axis_tx_data_req_TVALID in_vld 0 1 }  { s_axis_tx_data_req_TDATA in_data 0 64 } } }
	in_V_keep_V { axis {  { s_axis_tx_data_req_TKEEP in_data 0 8 } } }
	in_V_last_V { axis {  { s_axis_tx_data_req_TREADY in_acc 1 1 }  { s_axis_tx_data_req_TLAST in_data 0 1 } } }
	tasi_dataFifo_V { ap_fifo {  { tasi_dataFifo_V_din fifo_data 1 73 }  { tasi_dataFifo_V_full_n fifo_status 0 1 }  { tasi_dataFifo_V_write fifo_update 1 1 } } }
	txApp2txEng_data_str_3 { ap_fifo {  { txApp2txEng_data_str_3_din fifo_data 1 64 }  { txApp2txEng_data_str_3_full_n fifo_status 0 1 }  { txApp2txEng_data_str_3_write fifo_update 1 1 } } }
	txApp2txEng_data_str_5 { ap_fifo {  { txApp2txEng_data_str_5_din fifo_data 1 8 }  { txApp2txEng_data_str_5_full_n fifo_status 0 1 }  { txApp2txEng_data_str_5_write fifo_update 1 1 } } }
	txApp2txEng_data_str_6 { ap_fifo {  { txApp2txEng_data_str_6_din fifo_data 1 1 }  { txApp2txEng_data_str_6_full_n fifo_status 0 1 }  { txApp2txEng_data_str_6_write fifo_update 1 1 } } }
}
