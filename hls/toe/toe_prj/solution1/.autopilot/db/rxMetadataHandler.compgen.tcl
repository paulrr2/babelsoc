# This script segment is generated automatically by AutoPilot

# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 244 \
    name rxEng_metaDataFifo_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_metaDataFifo_V \
    op interface \
    ports { rxEng_metaDataFifo_V_dout { I 108 vector } rxEng_metaDataFifo_V_empty_n { I 1 bit } rxEng_metaDataFifo_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 245 \
    name portTable2rxEng_chec_1 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_portTable2rxEng_chec_1 \
    op interface \
    ports { portTable2rxEng_chec_1_dout { I 1 vector } portTable2rxEng_chec_1_empty_n { I 1 bit } portTable2rxEng_chec_1_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 246 \
    name rxEng_tupleBuffer_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_tupleBuffer_V \
    op interface \
    ports { rxEng_tupleBuffer_V_dout { I 96 vector } rxEng_tupleBuffer_V_empty_n { I 1 bit } rxEng_tupleBuffer_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 247 \
    name rxEng_metaHandlerEve_1 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_metaHandlerEve_1 \
    op interface \
    ports { rxEng_metaHandlerEve_1_din { O 152 vector } rxEng_metaHandlerEve_1_full_n { I 1 bit } rxEng_metaHandlerEve_1_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 248 \
    name rxEng_metaHandlerDro_1 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_metaHandlerDro_1 \
    op interface \
    ports { rxEng_metaHandlerDro_1_din { O 1 vector } rxEng_metaHandlerDro_1_full_n { I 1 bit } rxEng_metaHandlerDro_1_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 249 \
    name rxEng2sLookup_req_V \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng2sLookup_req_V \
    op interface \
    ports { rxEng2sLookup_req_V_din { O 97 vector } rxEng2sLookup_req_V_full_n { I 1 bit } rxEng2sLookup_req_V_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 250 \
    name sLookup2rxEng_rsp_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_sLookup2rxEng_rsp_V \
    op interface \
    ports { sLookup2rxEng_rsp_V_dout { I 17 vector } sLookup2rxEng_rsp_V_empty_n { I 1 bit } sLookup2rxEng_rsp_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 251 \
    name rxEng_fsmMetaDataFif_1 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_fsmMetaDataFif_1 \
    op interface \
    ports { rxEng_fsmMetaDataFif_1_din { O 172 vector } rxEng_fsmMetaDataFif_1_full_n { I 1 bit } rxEng_fsmMetaDataFif_1_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_start { I 1 bit } ap_ready { O 1 bit } ap_done { O 1 bit } ap_idle { O 1 bit } ap_continue { I 1 bit } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


