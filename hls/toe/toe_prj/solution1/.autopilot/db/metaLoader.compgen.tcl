# This script segment is generated automatically by AutoPilot

# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 278 \
    name eventEng2txEng_event_1 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_eventEng2txEng_event_1 \
    op interface \
    ports { eventEng2txEng_event_1_dout { I 152 vector } eventEng2txEng_event_1_empty_n { I 1 bit } eventEng2txEng_event_1_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 279 \
    name txEngFifoReadCount_V \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_txEngFifoReadCount_V \
    op interface \
    ports { txEngFifoReadCount_V_din { O 1 vector } txEngFifoReadCount_V_full_n { I 1 bit } txEngFifoReadCount_V_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 280 \
    name txEng2rxSar_req_V_V \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_txEng2rxSar_req_V_V \
    op interface \
    ports { txEng2rxSar_req_V_V_din { O 16 vector } txEng2rxSar_req_V_V_full_n { I 1 bit } txEng2rxSar_req_V_V_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 281 \
    name txEng2txSar_upd_req_s_10 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_txEng2txSar_upd_req_s_10 \
    op interface \
    ports { txEng2txSar_upd_req_s_10_din { O 53 vector } txEng2txSar_upd_req_s_10_full_n { I 1 bit } txEng2txSar_upd_req_s_10_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 282 \
    name rxSar2txEng_rsp_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_rxSar2txEng_rsp_V \
    op interface \
    ports { rxSar2txEng_rsp_V_dout { I 70 vector } rxSar2txEng_rsp_V_empty_n { I 1 bit } rxSar2txEng_rsp_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 283 \
    name txSar2txEng_upd_rsp_s_0 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_txSar2txEng_upd_rsp_s_0 \
    op interface \
    ports { txSar2txEng_upd_rsp_s_0_dout { I 124 vector } txSar2txEng_upd_rsp_s_0_empty_n { I 1 bit } txSar2txEng_upd_rsp_s_0_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 284 \
    name txEng2timer_setProbe_1 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_txEng2timer_setProbe_1 \
    op interface \
    ports { txEng2timer_setProbe_1_din { O 16 vector } txEng2timer_setProbe_1_full_n { I 1 bit } txEng2timer_setProbe_1_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 285 \
    name txEng_ipMetaFifo_V_V \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_txEng_ipMetaFifo_V_V \
    op interface \
    ports { txEng_ipMetaFifo_V_V_din { O 16 vector } txEng_ipMetaFifo_V_V_full_n { I 1 bit } txEng_ipMetaFifo_V_V_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 286 \
    name txEng_tcpMetaFifo_V \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_txEng_tcpMetaFifo_V \
    op interface \
    ports { txEng_tcpMetaFifo_V_din { O 104 vector } txEng_tcpMetaFifo_V_full_n { I 1 bit } txEng_tcpMetaFifo_V_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 287 \
    name txEng_isLookUpFifo_V \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_txEng_isLookUpFifo_V \
    op interface \
    ports { txEng_isLookUpFifo_V_din { O 1 vector } txEng_isLookUpFifo_V_full_n { I 1 bit } txEng_isLookUpFifo_V_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 288 \
    name txEng_isDDRbypass_V \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_txEng_isDDRbypass_V \
    op interface \
    ports { txEng_isDDRbypass_V_din { O 1 vector } txEng_isDDRbypass_V_full_n { I 1 bit } txEng_isDDRbypass_V_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 289 \
    name txEng2sLookup_rev_re_1 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_txEng2sLookup_rev_re_1 \
    op interface \
    ports { txEng2sLookup_rev_re_1_din { O 16 vector } txEng2sLookup_rev_re_1_full_n { I 1 bit } txEng2sLookup_rev_re_1_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 290 \
    name txEng2timer_setRetra_1 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_txEng2timer_setRetra_1 \
    op interface \
    ports { txEng2timer_setRetra_1_din { O 19 vector } txEng2timer_setRetra_1_full_n { I 1 bit } txEng2timer_setRetra_1_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 291 \
    name txMetaloader2memAcce_1 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_txMetaloader2memAcce_1 \
    op interface \
    ports { txMetaloader2memAcce_1_din { O 72 vector } txMetaloader2memAcce_1_full_n { I 1 bit } txMetaloader2memAcce_1_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 292 \
    name txEng_tupleShortCutF_1 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_txEng_tupleShortCutF_1 \
    op interface \
    ports { txEng_tupleShortCutF_1_din { O 96 vector } txEng_tupleShortCutF_1_full_n { I 1 bit } txEng_tupleShortCutF_1_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_start { I 1 bit } ap_ready { O 1 bit } ap_done { O 1 bit } ap_idle { O 1 bit } ap_continue { I 1 bit } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


