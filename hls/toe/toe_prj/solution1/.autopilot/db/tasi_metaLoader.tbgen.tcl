set moduleName tasi_metaLoader
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {tasi_metaLoader}
set C_modelType { void 0 }
set C_modelArgList {
	{ appTxDataReqMetaData_V int 32 regular {axi_s 0 volatile  { appTxDataReqMetaData_V Data } }  }
	{ appTxDataRsp_V int 64 regular {axi_s 1 volatile  { appTxDataRsp_V Data } }  }
	{ txApp2stateTable_req_1 int 16 regular {fifo 1 volatile } {global 1}  }
	{ txApp2txSar_upd_req_s_11 int 35 regular {fifo 1 volatile } {global 1}  }
	{ txSar2txApp_upd_rsp_s_1 int 70 regular {fifo 0 volatile } {global 0}  }
	{ stateTable2txApp_rsp_1 int 4 regular {fifo 0 volatile } {global 0}  }
	{ tasi_meta2pkgPushCmd_1 int 72 regular {fifo 1 volatile } {global 1}  }
	{ txAppStream2event_me_1 int 56 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "appTxDataReqMetaData_V", "interface" : "axis", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "appTxDataRsp_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY"} , 
 	{ "Name" : "txApp2stateTable_req_1", "interface" : "fifo", "bitwidth" : 16, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "txApp2txSar_upd_req_s_11", "interface" : "fifo", "bitwidth" : 35, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "txSar2txApp_upd_rsp_s_1", "interface" : "fifo", "bitwidth" : 70, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "stateTable2txApp_rsp_1", "interface" : "fifo", "bitwidth" : 4, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "tasi_meta2pkgPushCmd_1", "interface" : "fifo", "bitwidth" : 72, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "txAppStream2event_me_1", "interface" : "fifo", "bitwidth" : 56, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 31
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ appTxDataReqMetaData_V_TVALID sc_in sc_logic 1 invld 0 } 
	{ stateTable2txApp_rsp_1_dout sc_in sc_lv 4 signal 5 } 
	{ stateTable2txApp_rsp_1_empty_n sc_in sc_logic 1 signal 5 } 
	{ stateTable2txApp_rsp_1_read sc_out sc_logic 1 signal 5 } 
	{ txSar2txApp_upd_rsp_s_1_dout sc_in sc_lv 70 signal 4 } 
	{ txSar2txApp_upd_rsp_s_1_empty_n sc_in sc_logic 1 signal 4 } 
	{ txSar2txApp_upd_rsp_s_1_read sc_out sc_logic 1 signal 4 } 
	{ txApp2stateTable_req_1_din sc_out sc_lv 16 signal 2 } 
	{ txApp2stateTable_req_1_full_n sc_in sc_logic 1 signal 2 } 
	{ txApp2stateTable_req_1_write sc_out sc_logic 1 signal 2 } 
	{ txApp2txSar_upd_req_s_11_din sc_out sc_lv 35 signal 3 } 
	{ txApp2txSar_upd_req_s_11_full_n sc_in sc_logic 1 signal 3 } 
	{ txApp2txSar_upd_req_s_11_write sc_out sc_logic 1 signal 3 } 
	{ tasi_meta2pkgPushCmd_1_din sc_out sc_lv 72 signal 6 } 
	{ tasi_meta2pkgPushCmd_1_full_n sc_in sc_logic 1 signal 6 } 
	{ tasi_meta2pkgPushCmd_1_write sc_out sc_logic 1 signal 6 } 
	{ txAppStream2event_me_1_din sc_out sc_lv 56 signal 7 } 
	{ txAppStream2event_me_1_full_n sc_in sc_logic 1 signal 7 } 
	{ txAppStream2event_me_1_write sc_out sc_logic 1 signal 7 } 
	{ appTxDataRsp_V_TREADY sc_in sc_logic 1 outacc 1 } 
	{ appTxDataReqMetaData_V_TDATA sc_in sc_lv 32 signal 0 } 
	{ appTxDataReqMetaData_V_TREADY sc_out sc_logic 1 inacc 0 } 
	{ appTxDataRsp_V_TDATA sc_out sc_lv 64 signal 1 } 
	{ appTxDataRsp_V_TVALID sc_out sc_logic 1 outvld 1 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "appTxDataReqMetaData_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "appTxDataReqMetaData_V", "role": "TVALID" }} , 
 	{ "name": "stateTable2txApp_rsp_1_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "stateTable2txApp_rsp_1", "role": "dout" }} , 
 	{ "name": "stateTable2txApp_rsp_1_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "stateTable2txApp_rsp_1", "role": "empty_n" }} , 
 	{ "name": "stateTable2txApp_rsp_1_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "stateTable2txApp_rsp_1", "role": "read" }} , 
 	{ "name": "txSar2txApp_upd_rsp_s_1_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":70, "type": "signal", "bundle":{"name": "txSar2txApp_upd_rsp_s_1", "role": "dout" }} , 
 	{ "name": "txSar2txApp_upd_rsp_s_1_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txSar2txApp_upd_rsp_s_1", "role": "empty_n" }} , 
 	{ "name": "txSar2txApp_upd_rsp_s_1_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txSar2txApp_upd_rsp_s_1", "role": "read" }} , 
 	{ "name": "txApp2stateTable_req_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "txApp2stateTable_req_1", "role": "din" }} , 
 	{ "name": "txApp2stateTable_req_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txApp2stateTable_req_1", "role": "full_n" }} , 
 	{ "name": "txApp2stateTable_req_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txApp2stateTable_req_1", "role": "write" }} , 
 	{ "name": "txApp2txSar_upd_req_s_11_din", "direction": "out", "datatype": "sc_lv", "bitwidth":35, "type": "signal", "bundle":{"name": "txApp2txSar_upd_req_s_11", "role": "din" }} , 
 	{ "name": "txApp2txSar_upd_req_s_11_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txApp2txSar_upd_req_s_11", "role": "full_n" }} , 
 	{ "name": "txApp2txSar_upd_req_s_11_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txApp2txSar_upd_req_s_11", "role": "write" }} , 
 	{ "name": "tasi_meta2pkgPushCmd_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":72, "type": "signal", "bundle":{"name": "tasi_meta2pkgPushCmd_1", "role": "din" }} , 
 	{ "name": "tasi_meta2pkgPushCmd_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tasi_meta2pkgPushCmd_1", "role": "full_n" }} , 
 	{ "name": "tasi_meta2pkgPushCmd_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tasi_meta2pkgPushCmd_1", "role": "write" }} , 
 	{ "name": "txAppStream2event_me_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":56, "type": "signal", "bundle":{"name": "txAppStream2event_me_1", "role": "din" }} , 
 	{ "name": "txAppStream2event_me_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txAppStream2event_me_1", "role": "full_n" }} , 
 	{ "name": "txAppStream2event_me_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txAppStream2event_me_1", "role": "write" }} , 
 	{ "name": "appTxDataRsp_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "appTxDataRsp_V", "role": "TREADY" }} , 
 	{ "name": "appTxDataReqMetaData_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "appTxDataReqMetaData_V", "role": "TDATA" }} , 
 	{ "name": "appTxDataReqMetaData_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "appTxDataReqMetaData_V", "role": "TREADY" }} , 
 	{ "name": "appTxDataRsp_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "appTxDataRsp_V", "role": "TDATA" }} , 
 	{ "name": "appTxDataRsp_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "appTxDataRsp_V", "role": "TVALID" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1"],
		"CDFG" : "tasi_metaLoader",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "4", "EstimateLatencyMin" : "4", "EstimateLatencyMax" : "4",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "appTxDataReqMetaData_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "appTxDataReqMetaData_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "appTxDataRsp_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "appTxDataRsp_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tai_state", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tasi_writeMeta_sessi", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tasi_writeMeta_lengt", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "txApp2stateTable_req_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txApp2stateTable_req_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txApp2txSar_upd_req_s_11", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txApp2txSar_upd_req_s_11_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txSar2txApp_upd_rsp_s_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txSar2txApp_upd_rsp_s_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "stateTable2txApp_rsp_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "stateTable2txApp_rsp_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tasi_meta2pkgPushCmd_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "tasi_meta2pkgPushCmd_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txAppStream2event_me_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txAppStream2event_me_1_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_appTxDataRsp_V_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	tasi_metaLoader {
		appTxDataReqMetaData_V {Type I LastRead 0 FirstWrite -1}
		appTxDataRsp_V {Type O LastRead -1 FirstWrite 3}
		tai_state {Type IO LastRead -1 FirstWrite -1}
		tasi_writeMeta_sessi {Type IO LastRead -1 FirstWrite -1}
		tasi_writeMeta_lengt {Type IO LastRead -1 FirstWrite -1}
		txApp2stateTable_req_1 {Type O LastRead -1 FirstWrite 3}
		txApp2txSar_upd_req_s_11 {Type O LastRead -1 FirstWrite 3}
		txSar2txApp_upd_rsp_s_1 {Type I LastRead 0 FirstWrite -1}
		stateTable2txApp_rsp_1 {Type I LastRead 0 FirstWrite -1}
		tasi_meta2pkgPushCmd_1 {Type O LastRead -1 FirstWrite 3}
		txAppStream2event_me_1 {Type O LastRead -1 FirstWrite 3}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "4", "Max" : "4"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	appTxDataReqMetaData_V { axis {  { appTxDataReqMetaData_V_TVALID in_vld 0 1 }  { appTxDataReqMetaData_V_TDATA in_data 0 32 }  { appTxDataReqMetaData_V_TREADY in_acc 1 1 } } }
	appTxDataRsp_V { axis {  { appTxDataRsp_V_TREADY out_acc 0 1 }  { appTxDataRsp_V_TDATA out_data 1 64 }  { appTxDataRsp_V_TVALID out_vld 1 1 } } }
	txApp2stateTable_req_1 { ap_fifo {  { txApp2stateTable_req_1_din fifo_data 1 16 }  { txApp2stateTable_req_1_full_n fifo_status 0 1 }  { txApp2stateTable_req_1_write fifo_update 1 1 } } }
	txApp2txSar_upd_req_s_11 { ap_fifo {  { txApp2txSar_upd_req_s_11_din fifo_data 1 35 }  { txApp2txSar_upd_req_s_11_full_n fifo_status 0 1 }  { txApp2txSar_upd_req_s_11_write fifo_update 1 1 } } }
	txSar2txApp_upd_rsp_s_1 { ap_fifo {  { txSar2txApp_upd_rsp_s_1_dout fifo_data 0 70 }  { txSar2txApp_upd_rsp_s_1_empty_n fifo_status 0 1 }  { txSar2txApp_upd_rsp_s_1_read fifo_update 1 1 } } }
	stateTable2txApp_rsp_1 { ap_fifo {  { stateTable2txApp_rsp_1_dout fifo_data 0 4 }  { stateTable2txApp_rsp_1_empty_n fifo_status 0 1 }  { stateTable2txApp_rsp_1_read fifo_update 1 1 } } }
	tasi_meta2pkgPushCmd_1 { ap_fifo {  { tasi_meta2pkgPushCmd_1_din fifo_data 1 72 }  { tasi_meta2pkgPushCmd_1_full_n fifo_status 0 1 }  { tasi_meta2pkgPushCmd_1_write fifo_update 1 1 } } }
	txAppStream2event_me_1 { ap_fifo {  { txAppStream2event_me_1_din fifo_data 1 56 }  { txAppStream2event_me_1_full_n fifo_status 0 1 }  { txAppStream2event_me_1_write fifo_update 1 1 } } }
}
