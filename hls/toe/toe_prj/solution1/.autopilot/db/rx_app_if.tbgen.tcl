set moduleName rx_app_if
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {rx_app_if}
set C_modelType { void 0 }
set C_modelArgList {
	{ appListenPortReq_V_V int 16 regular {axi_s 0 volatile  { appListenPortReq_V_V Data } }  }
	{ appListenPortRsp_V int 8 regular {axi_s 1 volatile  { appListenPortRsp_V Data } }  }
	{ rxApp2portTable_list_1 int 16 regular {fifo 1 volatile } {global 1}  }
	{ portTable2rxApp_list_1 int 1 regular {fifo 0 volatile } {global 0}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "appListenPortReq_V_V", "interface" : "axis", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "appListenPortRsp_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY"} , 
 	{ "Name" : "rxApp2portTable_list_1", "interface" : "fifo", "bitwidth" : 16, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "portTable2rxApp_list_1", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 19
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ appListenPortReq_V_V_TVALID sc_in sc_logic 1 invld 0 } 
	{ portTable2rxApp_list_1_dout sc_in sc_lv 1 signal 3 } 
	{ portTable2rxApp_list_1_empty_n sc_in sc_logic 1 signal 3 } 
	{ portTable2rxApp_list_1_read sc_out sc_logic 1 signal 3 } 
	{ rxApp2portTable_list_1_din sc_out sc_lv 16 signal 2 } 
	{ rxApp2portTable_list_1_full_n sc_in sc_logic 1 signal 2 } 
	{ rxApp2portTable_list_1_write sc_out sc_logic 1 signal 2 } 
	{ appListenPortRsp_V_TREADY sc_in sc_logic 1 outacc 1 } 
	{ appListenPortReq_V_V_TDATA sc_in sc_lv 16 signal 0 } 
	{ appListenPortReq_V_V_TREADY sc_out sc_logic 1 inacc 0 } 
	{ appListenPortRsp_V_TDATA sc_out sc_lv 8 signal 1 } 
	{ appListenPortRsp_V_TVALID sc_out sc_logic 1 outvld 1 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "appListenPortReq_V_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "appListenPortReq_V_V", "role": "TVALID" }} , 
 	{ "name": "portTable2rxApp_list_1_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "portTable2rxApp_list_1", "role": "dout" }} , 
 	{ "name": "portTable2rxApp_list_1_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "portTable2rxApp_list_1", "role": "empty_n" }} , 
 	{ "name": "portTable2rxApp_list_1_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "portTable2rxApp_list_1", "role": "read" }} , 
 	{ "name": "rxApp2portTable_list_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "rxApp2portTable_list_1", "role": "din" }} , 
 	{ "name": "rxApp2portTable_list_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxApp2portTable_list_1", "role": "full_n" }} , 
 	{ "name": "rxApp2portTable_list_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxApp2portTable_list_1", "role": "write" }} , 
 	{ "name": "appListenPortRsp_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "appListenPortRsp_V", "role": "TREADY" }} , 
 	{ "name": "appListenPortReq_V_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "appListenPortReq_V_V", "role": "TDATA" }} , 
 	{ "name": "appListenPortReq_V_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "appListenPortReq_V_V", "role": "TREADY" }} , 
 	{ "name": "appListenPortRsp_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "appListenPortRsp_V", "role": "TDATA" }} , 
 	{ "name": "appListenPortRsp_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "appListenPortRsp_V", "role": "TVALID" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1"],
		"CDFG" : "rx_app_if",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "appListenPortReq_V_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "appListenPortReq_V_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "appListenPortRsp_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "appListenPortRsp_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rai_wait", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "rxApp2portTable_list_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxApp2portTable_list_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "portTable2rxApp_list_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "portTable2rxApp_list_1_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_appListenPortRsp_V_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	rx_app_if {
		appListenPortReq_V_V {Type I LastRead 0 FirstWrite -1}
		appListenPortRsp_V {Type O LastRead -1 FirstWrite 1}
		rai_wait {Type IO LastRead -1 FirstWrite -1}
		rxApp2portTable_list_1 {Type O LastRead -1 FirstWrite 1}
		portTable2rxApp_list_1 {Type I LastRead 0 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "2", "Max" : "2"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	appListenPortReq_V_V { axis {  { appListenPortReq_V_V_TVALID in_vld 0 1 }  { appListenPortReq_V_V_TDATA in_data 0 16 }  { appListenPortReq_V_V_TREADY in_acc 1 1 } } }
	appListenPortRsp_V { axis {  { appListenPortRsp_V_TREADY out_acc 0 1 }  { appListenPortRsp_V_TDATA out_data 1 8 }  { appListenPortRsp_V_TVALID out_vld 1 1 } } }
	rxApp2portTable_list_1 { ap_fifo {  { rxApp2portTable_list_1_din fifo_data 1 16 }  { rxApp2portTable_list_1_full_n fifo_status 0 1 }  { rxApp2portTable_list_1_write fifo_update 1 1 } } }
	portTable2rxApp_list_1 { ap_fifo {  { portTable2rxApp_list_1_dout fifo_data 0 1 }  { portTable2rxApp_list_1_empty_n fifo_status 0 1 }  { portTable2rxApp_list_1_read fifo_update 1 1 } } }
}
