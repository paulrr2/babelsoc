set moduleName read_data_arbiter
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {read_data_arbiter}
set C_modelType { void 0 }
set C_modelArgList {
	{ txEng_isDDRbypass_V int 1 regular {fifo 0 volatile } {global 0}  }
	{ txBufferReadDataStit_1 int 73 regular {fifo 0 volatile } {global 0}  }
	{ txEng_tcpPkgBuffer0_s_9 int 73 regular {fifo 1 volatile } {global 1}  }
	{ txApp2txEng_data_str_3 int 64 regular {fifo 0 volatile } {global 0}  }
	{ txApp2txEng_data_str_5 int 8 regular {fifo 0 volatile } {global 0}  }
	{ txApp2txEng_data_str_6 int 1 regular {fifo 0 volatile } {global 0}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "txEng_isDDRbypass_V", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "txBufferReadDataStit_1", "interface" : "fifo", "bitwidth" : 73, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "txEng_tcpPkgBuffer0_s_9", "interface" : "fifo", "bitwidth" : 73, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "txApp2txEng_data_str_3", "interface" : "fifo", "bitwidth" : 64, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "txApp2txEng_data_str_5", "interface" : "fifo", "bitwidth" : 8, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "txApp2txEng_data_str_6", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 25
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ txApp2txEng_data_str_3_dout sc_in sc_lv 64 signal 3 } 
	{ txApp2txEng_data_str_3_empty_n sc_in sc_logic 1 signal 3 } 
	{ txApp2txEng_data_str_3_read sc_out sc_logic 1 signal 3 } 
	{ txApp2txEng_data_str_5_dout sc_in sc_lv 8 signal 4 } 
	{ txApp2txEng_data_str_5_empty_n sc_in sc_logic 1 signal 4 } 
	{ txApp2txEng_data_str_5_read sc_out sc_logic 1 signal 4 } 
	{ txApp2txEng_data_str_6_dout sc_in sc_lv 1 signal 5 } 
	{ txApp2txEng_data_str_6_empty_n sc_in sc_logic 1 signal 5 } 
	{ txApp2txEng_data_str_6_read sc_out sc_logic 1 signal 5 } 
	{ txBufferReadDataStit_1_dout sc_in sc_lv 73 signal 1 } 
	{ txBufferReadDataStit_1_empty_n sc_in sc_logic 1 signal 1 } 
	{ txBufferReadDataStit_1_read sc_out sc_logic 1 signal 1 } 
	{ txEng_isDDRbypass_V_dout sc_in sc_lv 1 signal 0 } 
	{ txEng_isDDRbypass_V_empty_n sc_in sc_logic 1 signal 0 } 
	{ txEng_isDDRbypass_V_read sc_out sc_logic 1 signal 0 } 
	{ txEng_tcpPkgBuffer0_s_9_din sc_out sc_lv 73 signal 2 } 
	{ txEng_tcpPkgBuffer0_s_9_full_n sc_in sc_logic 1 signal 2 } 
	{ txEng_tcpPkgBuffer0_s_9_write sc_out sc_logic 1 signal 2 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "txApp2txEng_data_str_3_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "txApp2txEng_data_str_3", "role": "dout" }} , 
 	{ "name": "txApp2txEng_data_str_3_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txApp2txEng_data_str_3", "role": "empty_n" }} , 
 	{ "name": "txApp2txEng_data_str_3_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txApp2txEng_data_str_3", "role": "read" }} , 
 	{ "name": "txApp2txEng_data_str_5_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "txApp2txEng_data_str_5", "role": "dout" }} , 
 	{ "name": "txApp2txEng_data_str_5_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txApp2txEng_data_str_5", "role": "empty_n" }} , 
 	{ "name": "txApp2txEng_data_str_5_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txApp2txEng_data_str_5", "role": "read" }} , 
 	{ "name": "txApp2txEng_data_str_6_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "txApp2txEng_data_str_6", "role": "dout" }} , 
 	{ "name": "txApp2txEng_data_str_6_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txApp2txEng_data_str_6", "role": "empty_n" }} , 
 	{ "name": "txApp2txEng_data_str_6_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txApp2txEng_data_str_6", "role": "read" }} , 
 	{ "name": "txBufferReadDataStit_1_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":73, "type": "signal", "bundle":{"name": "txBufferReadDataStit_1", "role": "dout" }} , 
 	{ "name": "txBufferReadDataStit_1_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txBufferReadDataStit_1", "role": "empty_n" }} , 
 	{ "name": "txBufferReadDataStit_1_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txBufferReadDataStit_1", "role": "read" }} , 
 	{ "name": "txEng_isDDRbypass_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng_isDDRbypass_V", "role": "dout" }} , 
 	{ "name": "txEng_isDDRbypass_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng_isDDRbypass_V", "role": "empty_n" }} , 
 	{ "name": "txEng_isDDRbypass_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng_isDDRbypass_V", "role": "read" }} , 
 	{ "name": "txEng_tcpPkgBuffer0_s_9_din", "direction": "out", "datatype": "sc_lv", "bitwidth":73, "type": "signal", "bundle":{"name": "txEng_tcpPkgBuffer0_s_9", "role": "din" }} , 
 	{ "name": "txEng_tcpPkgBuffer0_s_9_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng_tcpPkgBuffer0_s_9", "role": "full_n" }} , 
 	{ "name": "txEng_tcpPkgBuffer0_s_9_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng_tcpPkgBuffer0_s_9", "role": "write" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "read_data_arbiter",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "tps_state_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "txEng_isDDRbypass_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txEng_isDDRbypass_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txBufferReadDataStit_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txBufferReadDataStit_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txEng_tcpPkgBuffer0_s_9", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txEng_tcpPkgBuffer0_s_9_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txApp2txEng_data_str_3", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txApp2txEng_data_str_3_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txApp2txEng_data_str_5", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txApp2txEng_data_str_5_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txApp2txEng_data_str_6", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txApp2txEng_data_str_6_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	read_data_arbiter {
		tps_state_V {Type IO LastRead -1 FirstWrite -1}
		txEng_isDDRbypass_V {Type I LastRead 0 FirstWrite -1}
		txBufferReadDataStit_1 {Type I LastRead 0 FirstWrite -1}
		txEng_tcpPkgBuffer0_s_9 {Type O LastRead -1 FirstWrite 1}
		txApp2txEng_data_str_3 {Type I LastRead 0 FirstWrite -1}
		txApp2txEng_data_str_5 {Type I LastRead 0 FirstWrite -1}
		txApp2txEng_data_str_6 {Type I LastRead 0 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "1", "Max" : "1"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	txEng_isDDRbypass_V { ap_fifo {  { txEng_isDDRbypass_V_dout fifo_data 0 1 }  { txEng_isDDRbypass_V_empty_n fifo_status 0 1 }  { txEng_isDDRbypass_V_read fifo_update 1 1 } } }
	txBufferReadDataStit_1 { ap_fifo {  { txBufferReadDataStit_1_dout fifo_data 0 73 }  { txBufferReadDataStit_1_empty_n fifo_status 0 1 }  { txBufferReadDataStit_1_read fifo_update 1 1 } } }
	txEng_tcpPkgBuffer0_s_9 { ap_fifo {  { txEng_tcpPkgBuffer0_s_9_din fifo_data 1 73 }  { txEng_tcpPkgBuffer0_s_9_full_n fifo_status 0 1 }  { txEng_tcpPkgBuffer0_s_9_write fifo_update 1 1 } } }
	txApp2txEng_data_str_3 { ap_fifo {  { txApp2txEng_data_str_3_dout fifo_data 0 64 }  { txApp2txEng_data_str_3_empty_n fifo_status 0 1 }  { txApp2txEng_data_str_3_read fifo_update 1 1 } } }
	txApp2txEng_data_str_5 { ap_fifo {  { txApp2txEng_data_str_5_dout fifo_data 0 8 }  { txApp2txEng_data_str_5_empty_n fifo_status 0 1 }  { txApp2txEng_data_str_5_read fifo_update 1 1 } } }
	txApp2txEng_data_str_6 { ap_fifo {  { txApp2txEng_data_str_6_dout fifo_data 0 1 }  { txApp2txEng_data_str_6_empty_n fifo_status 0 1 }  { txApp2txEng_data_str_6_read fifo_update 1 1 } } }
}
