# This script segment is generated automatically by AutoPilot

# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 232 \
    name rxEng_headerMetaFifo_20 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_headerMetaFifo_20 \
    op interface \
    ports { rxEng_headerMetaFifo_20_dout { I 32 vector } rxEng_headerMetaFifo_20_empty_n { I 1 bit } rxEng_headerMetaFifo_20_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 233 \
    name rxEng_headerMetaFifo_12 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_headerMetaFifo_12 \
    op interface \
    ports { rxEng_headerMetaFifo_12_dout { I 32 vector } rxEng_headerMetaFifo_12_empty_n { I 1 bit } rxEng_headerMetaFifo_12_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 234 \
    name rxEng_headerMetaFifo_23 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_headerMetaFifo_23 \
    op interface \
    ports { rxEng_headerMetaFifo_23_dout { I 16 vector } rxEng_headerMetaFifo_23_empty_n { I 1 bit } rxEng_headerMetaFifo_23_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 235 \
    name rxEng_headerMetaFifo_22 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_headerMetaFifo_22 \
    op interface \
    ports { rxEng_headerMetaFifo_22_dout { I 4 vector } rxEng_headerMetaFifo_22_empty_n { I 1 bit } rxEng_headerMetaFifo_22_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 236 \
    name rxEng_headerMetaFifo_18 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_headerMetaFifo_18 \
    op interface \
    ports { rxEng_headerMetaFifo_18_dout { I 16 vector } rxEng_headerMetaFifo_18_empty_n { I 1 bit } rxEng_headerMetaFifo_18_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 237 \
    name rxEng_headerMetaFifo_10 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_headerMetaFifo_10 \
    op interface \
    ports { rxEng_headerMetaFifo_10_dout { I 1 vector } rxEng_headerMetaFifo_10_empty_n { I 1 bit } rxEng_headerMetaFifo_10_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 238 \
    name rxEng_headerMetaFifo_19 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_headerMetaFifo_19 \
    op interface \
    ports { rxEng_headerMetaFifo_19_dout { I 1 vector } rxEng_headerMetaFifo_19_empty_n { I 1 bit } rxEng_headerMetaFifo_19_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 239 \
    name rxEng_headerMetaFifo_21 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_headerMetaFifo_21 \
    op interface \
    ports { rxEng_headerMetaFifo_21_dout { I 1 vector } rxEng_headerMetaFifo_21_empty_n { I 1 bit } rxEng_headerMetaFifo_21_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 240 \
    name rxEng_headerMetaFifo_16 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_headerMetaFifo_16 \
    op interface \
    ports { rxEng_headerMetaFifo_16_dout { I 1 vector } rxEng_headerMetaFifo_16_empty_n { I 1 bit } rxEng_headerMetaFifo_16_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 241 \
    name rxEng_headerMetaFifo_14 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_headerMetaFifo_14 \
    op interface \
    ports { rxEng_headerMetaFifo_14_dout { I 4 vector } rxEng_headerMetaFifo_14_empty_n { I 1 bit } rxEng_headerMetaFifo_14_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 242 \
    name rxEng_metaDataFifo_V \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_metaDataFifo_V \
    op interface \
    ports { rxEng_metaDataFifo_V_din { O 108 vector } rxEng_metaDataFifo_V_full_n { I 1 bit } rxEng_metaDataFifo_V_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 243 \
    name rxEng_winScaleFifo_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_winScaleFifo_V \
    op interface \
    ports { rxEng_winScaleFifo_V_dout { I 4 vector } rxEng_winScaleFifo_V_empty_n { I 1 bit } rxEng_winScaleFifo_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_start { I 1 bit } ap_ready { O 1 bit } ap_done { O 1 bit } ap_idle { O 1 bit } ap_continue { I 1 bit } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


