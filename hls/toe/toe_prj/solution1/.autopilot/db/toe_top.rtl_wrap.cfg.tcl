set lang "C++"
set moduleName "toe_top"
set moduleIsExternC "0"
set rawDecl ""
set globalVariable ""
set PortList ""
set PortName0 "s_axis_tcp_data"
set BitWidth0 "128"
set ArrayOpt0 ""
set Const0 "0"
set Volatile0 "0"
set Pointer0 "2"
set Reference0 "1"
set Dims0 [list 0]
set Interface0 [list AP_STREAM 0] 
set structMem0 ""
set PortName00 "data"
set BitWidth00 "64"
set ArrayOpt00 ""
set Const00 "0"
set Volatile00 "0"
set Pointer00 "0"
set Reference00 "0"
set Dims00 [list 0]
set Interface00 "wire"
set DataType00 "[list ap_uint 64 ]"
set Port00 [list $PortName00 $Interface00 $DataType00 $Pointer00 $Dims00 $Const00 $Volatile00 $ArrayOpt00]
lappend structMem0 $Port00
set PortName01 "keep"
set BitWidth01 "8"
set ArrayOpt01 ""
set Const01 "0"
set Volatile01 "0"
set Pointer01 "0"
set Reference01 "0"
set Dims01 [list 0]
set Interface01 "wire"
set DataType01 "[list ap_uint 8 ]"
set Port01 [list $PortName01 $Interface01 $DataType01 $Pointer01 $Dims01 $Const01 $Volatile01 $ArrayOpt01]
lappend structMem0 $Port01
set PortName02 "last"
set BitWidth02 "8"
set ArrayOpt02 ""
set Const02 "0"
set Volatile02 "0"
set Pointer02 "0"
set Reference02 "0"
set Dims02 [list 0]
set Interface02 "wire"
set DataType02 "[list ap_uint 1 ]"
set Port02 [list $PortName02 $Interface02 $DataType02 $Pointer02 $Dims02 $Const02 $Volatile02 $ArrayOpt02]
lappend structMem0 $Port02
set DataType0tp0 "int"
set structParameter0 [list [list $DataType0tp0 D] ]
set structArgument0 [list 64 ]
set NameSpace0 [list ]
set structIsPacked0 "0"
set DataType0 [list "net_axis<64>" "struct net_axis" $structMem0 1 0 $structParameter0 $structArgument0 $NameSpace0 $structIsPacked0]
set Port0 [list $PortName0 $Interface0 $DataType0 $Pointer0 $Dims0 $Const0 $Volatile0 $ArrayOpt0]
lappend PortList $Port0
set PortName1 "s_axis_txwrite_sts"
set BitWidth1 "40"
set ArrayOpt1 ""
set Const1 "0"
set Volatile1 "0"
set Pointer1 "2"
set Reference1 "1"
set Dims1 [list 0]
set Interface1 [list AP_STREAM 0] 
set structMem1 ""
set PortName10 "tag"
set BitWidth10 "8"
set ArrayOpt10 ""
set Const10 "0"
set Volatile10 "0"
set Pointer10 "0"
set Reference10 "0"
set Dims10 [list 0]
set Interface10 "wire"
set DataType10 "[list ap_uint 4 ]"
set Port10 [list $PortName10 $Interface10 $DataType10 $Pointer10 $Dims10 $Const10 $Volatile10 $ArrayOpt10]
lappend structMem1 $Port10
set PortName11 "interr"
set BitWidth11 "8"
set ArrayOpt11 ""
set Const11 "0"
set Volatile11 "0"
set Pointer11 "0"
set Reference11 "0"
set Dims11 [list 0]
set Interface11 "wire"
set DataType11 "[list ap_uint 1 ]"
set Port11 [list $PortName11 $Interface11 $DataType11 $Pointer11 $Dims11 $Const11 $Volatile11 $ArrayOpt11]
lappend structMem1 $Port11
set PortName12 "decerr"
set BitWidth12 "8"
set ArrayOpt12 ""
set Const12 "0"
set Volatile12 "0"
set Pointer12 "0"
set Reference12 "0"
set Dims12 [list 0]
set Interface12 "wire"
set DataType12 "[list ap_uint 1 ]"
set Port12 [list $PortName12 $Interface12 $DataType12 $Pointer12 $Dims12 $Const12 $Volatile12 $ArrayOpt12]
lappend structMem1 $Port12
set PortName13 "slverr"
set BitWidth13 "8"
set ArrayOpt13 ""
set Const13 "0"
set Volatile13 "0"
set Pointer13 "0"
set Reference13 "0"
set Dims13 [list 0]
set Interface13 "wire"
set DataType13 "[list ap_uint 1 ]"
set Port13 [list $PortName13 $Interface13 $DataType13 $Pointer13 $Dims13 $Const13 $Volatile13 $ArrayOpt13]
lappend structMem1 $Port13
set PortName14 "okay"
set BitWidth14 "8"
set ArrayOpt14 ""
set Const14 "0"
set Volatile14 "0"
set Pointer14 "0"
set Reference14 "0"
set Dims14 [list 0]
set Interface14 "wire"
set DataType14 "[list ap_uint 1 ]"
set Port14 [list $PortName14 $Interface14 $DataType14 $Pointer14 $Dims14 $Const14 $Volatile14 $ArrayOpt14]
lappend structMem1 $Port14
set structParameter1 [list ]
set structArgument1 [list ]
set NameSpace1 [list ]
set structIsPacked1 "1"
set DataType1 [list "mmStatus" "struct mmStatus" $structMem1 1 0 $structParameter1 $structArgument1 $NameSpace1 $structIsPacked1]
set Port1 [list $PortName1 $Interface1 $DataType1 $Pointer1 $Dims1 $Const1 $Volatile1 $ArrayOpt1]
lappend PortList $Port1
set PortName2 "s_axis_rxread_data"
set BitWidth2 "128"
set ArrayOpt2 ""
set Const2 "0"
set Volatile2 "0"
set Pointer2 "2"
set Reference2 "1"
set Dims2 [list 0]
set Interface2 [list AP_STREAM 0] 
set structMem2 ""
set PortName20 "data"
set BitWidth20 "64"
set ArrayOpt20 ""
set Const20 "0"
set Volatile20 "0"
set Pointer20 "0"
set Reference20 "0"
set Dims20 [list 0]
set Interface20 "wire"
set DataType20 "[list ap_uint 64 ]"
set Port20 [list $PortName20 $Interface20 $DataType20 $Pointer20 $Dims20 $Const20 $Volatile20 $ArrayOpt20]
lappend structMem2 $Port20
set PortName21 "keep"
set BitWidth21 "8"
set ArrayOpt21 ""
set Const21 "0"
set Volatile21 "0"
set Pointer21 "0"
set Reference21 "0"
set Dims21 [list 0]
set Interface21 "wire"
set DataType21 "[list ap_uint 8 ]"
set Port21 [list $PortName21 $Interface21 $DataType21 $Pointer21 $Dims21 $Const21 $Volatile21 $ArrayOpt21]
lappend structMem2 $Port21
set PortName22 "last"
set BitWidth22 "8"
set ArrayOpt22 ""
set Const22 "0"
set Volatile22 "0"
set Pointer22 "0"
set Reference22 "0"
set Dims22 [list 0]
set Interface22 "wire"
set DataType22 "[list ap_uint 1 ]"
set Port22 [list $PortName22 $Interface22 $DataType22 $Pointer22 $Dims22 $Const22 $Volatile22 $ArrayOpt22]
lappend structMem2 $Port22
set DataType2tp0 "int"
set structParameter2 [list [list $DataType2tp0 D] ]
set structArgument2 [list 64 ]
set NameSpace2 [list ]
set structIsPacked2 "0"
set DataType2 [list "net_axis<64>" "struct net_axis" $structMem2 1 0 $structParameter2 $structArgument2 $NameSpace2 $structIsPacked2]
set Port2 [list $PortName2 $Interface2 $DataType2 $Pointer2 $Dims2 $Const2 $Volatile2 $ArrayOpt2]
lappend PortList $Port2
set PortName3 "s_axis_txread_data"
set BitWidth3 "128"
set ArrayOpt3 ""
set Const3 "0"
set Volatile3 "0"
set Pointer3 "2"
set Reference3 "1"
set Dims3 [list 0]
set Interface3 [list AP_STREAM 0] 
set structMem3 ""
set PortName30 "data"
set BitWidth30 "64"
set ArrayOpt30 ""
set Const30 "0"
set Volatile30 "0"
set Pointer30 "0"
set Reference30 "0"
set Dims30 [list 0]
set Interface30 "wire"
set DataType30 "[list ap_uint 64 ]"
set Port30 [list $PortName30 $Interface30 $DataType30 $Pointer30 $Dims30 $Const30 $Volatile30 $ArrayOpt30]
lappend structMem3 $Port30
set PortName31 "keep"
set BitWidth31 "8"
set ArrayOpt31 ""
set Const31 "0"
set Volatile31 "0"
set Pointer31 "0"
set Reference31 "0"
set Dims31 [list 0]
set Interface31 "wire"
set DataType31 "[list ap_uint 8 ]"
set Port31 [list $PortName31 $Interface31 $DataType31 $Pointer31 $Dims31 $Const31 $Volatile31 $ArrayOpt31]
lappend structMem3 $Port31
set PortName32 "last"
set BitWidth32 "8"
set ArrayOpt32 ""
set Const32 "0"
set Volatile32 "0"
set Pointer32 "0"
set Reference32 "0"
set Dims32 [list 0]
set Interface32 "wire"
set DataType32 "[list ap_uint 1 ]"
set Port32 [list $PortName32 $Interface32 $DataType32 $Pointer32 $Dims32 $Const32 $Volatile32 $ArrayOpt32]
lappend structMem3 $Port32
set DataType3tp0 "int"
set structParameter3 [list [list $DataType3tp0 D] ]
set structArgument3 [list 64 ]
set NameSpace3 [list ]
set structIsPacked3 "0"
set DataType3 [list "net_axis<64>" "struct net_axis" $structMem3 1 0 $structParameter3 $structArgument3 $NameSpace3 $structIsPacked3]
set Port3 [list $PortName3 $Interface3 $DataType3 $Pointer3 $Dims3 $Const3 $Volatile3 $ArrayOpt3]
lappend PortList $Port3
set PortName4 "m_axis_tcp_data"
set BitWidth4 "128"
set ArrayOpt4 ""
set Const4 "0"
set Volatile4 "0"
set Pointer4 "2"
set Reference4 "1"
set Dims4 [list 0]
set Interface4 [list AP_STREAM 0] 
set structMem4 ""
set PortName40 "data"
set BitWidth40 "64"
set ArrayOpt40 ""
set Const40 "0"
set Volatile40 "0"
set Pointer40 "0"
set Reference40 "0"
set Dims40 [list 0]
set Interface40 "wire"
set DataType40 "[list ap_uint 64 ]"
set Port40 [list $PortName40 $Interface40 $DataType40 $Pointer40 $Dims40 $Const40 $Volatile40 $ArrayOpt40]
lappend structMem4 $Port40
set PortName41 "keep"
set BitWidth41 "8"
set ArrayOpt41 ""
set Const41 "0"
set Volatile41 "0"
set Pointer41 "0"
set Reference41 "0"
set Dims41 [list 0]
set Interface41 "wire"
set DataType41 "[list ap_uint 8 ]"
set Port41 [list $PortName41 $Interface41 $DataType41 $Pointer41 $Dims41 $Const41 $Volatile41 $ArrayOpt41]
lappend structMem4 $Port41
set PortName42 "last"
set BitWidth42 "8"
set ArrayOpt42 ""
set Const42 "0"
set Volatile42 "0"
set Pointer42 "0"
set Reference42 "0"
set Dims42 [list 0]
set Interface42 "wire"
set DataType42 "[list ap_uint 1 ]"
set Port42 [list $PortName42 $Interface42 $DataType42 $Pointer42 $Dims42 $Const42 $Volatile42 $ArrayOpt42]
lappend structMem4 $Port42
set DataType4tp0 "int"
set structParameter4 [list [list $DataType4tp0 D] ]
set structArgument4 [list 64 ]
set NameSpace4 [list ]
set structIsPacked4 "0"
set DataType4 [list "net_axis<64>" "struct net_axis" $structMem4 1 0 $structParameter4 $structArgument4 $NameSpace4 $structIsPacked4]
set Port4 [list $PortName4 $Interface4 $DataType4 $Pointer4 $Dims4 $Const4 $Volatile4 $ArrayOpt4]
lappend PortList $Port4
set PortName5 "m_axis_txwrite_cmd"
set BitWidth5 "128"
set ArrayOpt5 ""
set Const5 "0"
set Volatile5 "0"
set Pointer5 "2"
set Reference5 "1"
set Dims5 [list 0]
set Interface5 [list AP_STREAM 0] 
set structMem5 ""
set PortName50 "bbt"
set BitWidth50 "32"
set ArrayOpt50 ""
set Const50 "0"
set Volatile50 "0"
set Pointer50 "0"
set Reference50 "0"
set Dims50 [list 0]
set Interface50 "wire"
set DataType50 "[list ap_uint 23 ]"
set Port50 [list $PortName50 $Interface50 $DataType50 $Pointer50 $Dims50 $Const50 $Volatile50 $ArrayOpt50]
lappend structMem5 $Port50
set PortName51 "type"
set BitWidth51 "8"
set ArrayOpt51 ""
set Const51 "0"
set Volatile51 "0"
set Pointer51 "0"
set Reference51 "0"
set Dims51 [list 0]
set Interface51 "wire"
set DataType51 "[list ap_uint 1 ]"
set Port51 [list $PortName51 $Interface51 $DataType51 $Pointer51 $Dims51 $Const51 $Volatile51 $ArrayOpt51]
lappend structMem5 $Port51
set PortName52 "dsa"
set BitWidth52 "8"
set ArrayOpt52 ""
set Const52 "0"
set Volatile52 "0"
set Pointer52 "0"
set Reference52 "0"
set Dims52 [list 0]
set Interface52 "wire"
set DataType52 "[list ap_uint 6 ]"
set Port52 [list $PortName52 $Interface52 $DataType52 $Pointer52 $Dims52 $Const52 $Volatile52 $ArrayOpt52]
lappend structMem5 $Port52
set PortName53 "eof"
set BitWidth53 "8"
set ArrayOpt53 ""
set Const53 "0"
set Volatile53 "0"
set Pointer53 "0"
set Reference53 "0"
set Dims53 [list 0]
set Interface53 "wire"
set DataType53 "[list ap_uint 1 ]"
set Port53 [list $PortName53 $Interface53 $DataType53 $Pointer53 $Dims53 $Const53 $Volatile53 $ArrayOpt53]
lappend structMem5 $Port53
set PortName54 "drr"
set BitWidth54 "8"
set ArrayOpt54 ""
set Const54 "0"
set Volatile54 "0"
set Pointer54 "0"
set Reference54 "0"
set Dims54 [list 0]
set Interface54 "wire"
set DataType54 "[list ap_uint 1 ]"
set Port54 [list $PortName54 $Interface54 $DataType54 $Pointer54 $Dims54 $Const54 $Volatile54 $ArrayOpt54]
lappend structMem5 $Port54
set PortName55 "saddr"
set BitWidth55 "32"
set ArrayOpt55 ""
set Const55 "0"
set Volatile55 "0"
set Pointer55 "0"
set Reference55 "0"
set Dims55 [list 0]
set Interface55 "wire"
set DataType55 "[list ap_uint 32 ]"
set Port55 [list $PortName55 $Interface55 $DataType55 $Pointer55 $Dims55 $Const55 $Volatile55 $ArrayOpt55]
lappend structMem5 $Port55
set PortName56 "tag"
set BitWidth56 "8"
set ArrayOpt56 ""
set Const56 "0"
set Volatile56 "0"
set Pointer56 "0"
set Reference56 "0"
set Dims56 [list 0]
set Interface56 "wire"
set DataType56 "[list ap_uint 4 ]"
set Port56 [list $PortName56 $Interface56 $DataType56 $Pointer56 $Dims56 $Const56 $Volatile56 $ArrayOpt56]
lappend structMem5 $Port56
set PortName57 "rsvd"
set BitWidth57 "8"
set ArrayOpt57 ""
set Const57 "0"
set Volatile57 "0"
set Pointer57 "0"
set Reference57 "0"
set Dims57 [list 0]
set Interface57 "wire"
set DataType57 "[list ap_uint 4 ]"
set Port57 [list $PortName57 $Interface57 $DataType57 $Pointer57 $Dims57 $Const57 $Volatile57 $ArrayOpt57]
lappend structMem5 $Port57
set structParameter5 [list ]
set structArgument5 [list ]
set NameSpace5 [list ]
set structIsPacked5 "0"
set DataType5 [list "mmCmd" "struct mmCmd" $structMem5 1 0 $structParameter5 $structArgument5 $NameSpace5 $structIsPacked5]
set Port5 [list $PortName5 $Interface5 $DataType5 $Pointer5 $Dims5 $Const5 $Volatile5 $ArrayOpt5]
lappend PortList $Port5
set PortName6 "m_axis_txread_cmd"
set BitWidth6 "128"
set ArrayOpt6 ""
set Const6 "0"
set Volatile6 "0"
set Pointer6 "2"
set Reference6 "1"
set Dims6 [list 0]
set Interface6 [list AP_STREAM 0] 
set structMem6 ""
set PortName60 "bbt"
set BitWidth60 "32"
set ArrayOpt60 ""
set Const60 "0"
set Volatile60 "0"
set Pointer60 "0"
set Reference60 "0"
set Dims60 [list 0]
set Interface60 "wire"
set DataType60 "[list ap_uint 23 ]"
set Port60 [list $PortName60 $Interface60 $DataType60 $Pointer60 $Dims60 $Const60 $Volatile60 $ArrayOpt60]
lappend structMem6 $Port60
set PortName61 "type"
set BitWidth61 "8"
set ArrayOpt61 ""
set Const61 "0"
set Volatile61 "0"
set Pointer61 "0"
set Reference61 "0"
set Dims61 [list 0]
set Interface61 "wire"
set DataType61 "[list ap_uint 1 ]"
set Port61 [list $PortName61 $Interface61 $DataType61 $Pointer61 $Dims61 $Const61 $Volatile61 $ArrayOpt61]
lappend structMem6 $Port61
set PortName62 "dsa"
set BitWidth62 "8"
set ArrayOpt62 ""
set Const62 "0"
set Volatile62 "0"
set Pointer62 "0"
set Reference62 "0"
set Dims62 [list 0]
set Interface62 "wire"
set DataType62 "[list ap_uint 6 ]"
set Port62 [list $PortName62 $Interface62 $DataType62 $Pointer62 $Dims62 $Const62 $Volatile62 $ArrayOpt62]
lappend structMem6 $Port62
set PortName63 "eof"
set BitWidth63 "8"
set ArrayOpt63 ""
set Const63 "0"
set Volatile63 "0"
set Pointer63 "0"
set Reference63 "0"
set Dims63 [list 0]
set Interface63 "wire"
set DataType63 "[list ap_uint 1 ]"
set Port63 [list $PortName63 $Interface63 $DataType63 $Pointer63 $Dims63 $Const63 $Volatile63 $ArrayOpt63]
lappend structMem6 $Port63
set PortName64 "drr"
set BitWidth64 "8"
set ArrayOpt64 ""
set Const64 "0"
set Volatile64 "0"
set Pointer64 "0"
set Reference64 "0"
set Dims64 [list 0]
set Interface64 "wire"
set DataType64 "[list ap_uint 1 ]"
set Port64 [list $PortName64 $Interface64 $DataType64 $Pointer64 $Dims64 $Const64 $Volatile64 $ArrayOpt64]
lappend structMem6 $Port64
set PortName65 "saddr"
set BitWidth65 "32"
set ArrayOpt65 ""
set Const65 "0"
set Volatile65 "0"
set Pointer65 "0"
set Reference65 "0"
set Dims65 [list 0]
set Interface65 "wire"
set DataType65 "[list ap_uint 32 ]"
set Port65 [list $PortName65 $Interface65 $DataType65 $Pointer65 $Dims65 $Const65 $Volatile65 $ArrayOpt65]
lappend structMem6 $Port65
set PortName66 "tag"
set BitWidth66 "8"
set ArrayOpt66 ""
set Const66 "0"
set Volatile66 "0"
set Pointer66 "0"
set Reference66 "0"
set Dims66 [list 0]
set Interface66 "wire"
set DataType66 "[list ap_uint 4 ]"
set Port66 [list $PortName66 $Interface66 $DataType66 $Pointer66 $Dims66 $Const66 $Volatile66 $ArrayOpt66]
lappend structMem6 $Port66
set PortName67 "rsvd"
set BitWidth67 "8"
set ArrayOpt67 ""
set Const67 "0"
set Volatile67 "0"
set Pointer67 "0"
set Reference67 "0"
set Dims67 [list 0]
set Interface67 "wire"
set DataType67 "[list ap_uint 4 ]"
set Port67 [list $PortName67 $Interface67 $DataType67 $Pointer67 $Dims67 $Const67 $Volatile67 $ArrayOpt67]
lappend structMem6 $Port67
set structParameter6 [list ]
set structArgument6 [list ]
set NameSpace6 [list ]
set structIsPacked6 "0"
set DataType6 [list "mmCmd" "struct mmCmd" $structMem6 1 0 $structParameter6 $structArgument6 $NameSpace6 $structIsPacked6]
set Port6 [list $PortName6 $Interface6 $DataType6 $Pointer6 $Dims6 $Const6 $Volatile6 $ArrayOpt6]
lappend PortList $Port6
set PortName7 "m_axis_rxwrite_data"
set BitWidth7 "128"
set ArrayOpt7 ""
set Const7 "0"
set Volatile7 "0"
set Pointer7 "2"
set Reference7 "1"
set Dims7 [list 0]
set Interface7 [list AP_STREAM 0] 
set structMem7 ""
set PortName70 "data"
set BitWidth70 "64"
set ArrayOpt70 ""
set Const70 "0"
set Volatile70 "0"
set Pointer70 "0"
set Reference70 "0"
set Dims70 [list 0]
set Interface70 "wire"
set DataType70 "[list ap_uint 64 ]"
set Port70 [list $PortName70 $Interface70 $DataType70 $Pointer70 $Dims70 $Const70 $Volatile70 $ArrayOpt70]
lappend structMem7 $Port70
set PortName71 "keep"
set BitWidth71 "8"
set ArrayOpt71 ""
set Const71 "0"
set Volatile71 "0"
set Pointer71 "0"
set Reference71 "0"
set Dims71 [list 0]
set Interface71 "wire"
set DataType71 "[list ap_uint 8 ]"
set Port71 [list $PortName71 $Interface71 $DataType71 $Pointer71 $Dims71 $Const71 $Volatile71 $ArrayOpt71]
lappend structMem7 $Port71
set PortName72 "last"
set BitWidth72 "8"
set ArrayOpt72 ""
set Const72 "0"
set Volatile72 "0"
set Pointer72 "0"
set Reference72 "0"
set Dims72 [list 0]
set Interface72 "wire"
set DataType72 "[list ap_uint 1 ]"
set Port72 [list $PortName72 $Interface72 $DataType72 $Pointer72 $Dims72 $Const72 $Volatile72 $ArrayOpt72]
lappend structMem7 $Port72
set DataType7tp0 "int"
set structParameter7 [list [list $DataType7tp0 D] ]
set structArgument7 [list 64 ]
set NameSpace7 [list ]
set structIsPacked7 "0"
set DataType7 [list "net_axis<64>" "struct net_axis" $structMem7 1 0 $structParameter7 $structArgument7 $NameSpace7 $structIsPacked7]
set Port7 [list $PortName7 $Interface7 $DataType7 $Pointer7 $Dims7 $Const7 $Volatile7 $ArrayOpt7]
lappend PortList $Port7
set PortName8 "m_axis_txwrite_data"
set BitWidth8 "128"
set ArrayOpt8 ""
set Const8 "0"
set Volatile8 "0"
set Pointer8 "2"
set Reference8 "1"
set Dims8 [list 0]
set Interface8 [list AP_STREAM 0] 
set structMem8 ""
set PortName80 "data"
set BitWidth80 "64"
set ArrayOpt80 ""
set Const80 "0"
set Volatile80 "0"
set Pointer80 "0"
set Reference80 "0"
set Dims80 [list 0]
set Interface80 "wire"
set DataType80 "[list ap_uint 64 ]"
set Port80 [list $PortName80 $Interface80 $DataType80 $Pointer80 $Dims80 $Const80 $Volatile80 $ArrayOpt80]
lappend structMem8 $Port80
set PortName81 "keep"
set BitWidth81 "8"
set ArrayOpt81 ""
set Const81 "0"
set Volatile81 "0"
set Pointer81 "0"
set Reference81 "0"
set Dims81 [list 0]
set Interface81 "wire"
set DataType81 "[list ap_uint 8 ]"
set Port81 [list $PortName81 $Interface81 $DataType81 $Pointer81 $Dims81 $Const81 $Volatile81 $ArrayOpt81]
lappend structMem8 $Port81
set PortName82 "last"
set BitWidth82 "8"
set ArrayOpt82 ""
set Const82 "0"
set Volatile82 "0"
set Pointer82 "0"
set Reference82 "0"
set Dims82 [list 0]
set Interface82 "wire"
set DataType82 "[list ap_uint 1 ]"
set Port82 [list $PortName82 $Interface82 $DataType82 $Pointer82 $Dims82 $Const82 $Volatile82 $ArrayOpt82]
lappend structMem8 $Port82
set DataType8tp0 "int"
set structParameter8 [list [list $DataType8tp0 D] ]
set structArgument8 [list 64 ]
set NameSpace8 [list ]
set structIsPacked8 "0"
set DataType8 [list "net_axis<64>" "struct net_axis" $structMem8 1 0 $structParameter8 $structArgument8 $NameSpace8 $structIsPacked8]
set Port8 [list $PortName8 $Interface8 $DataType8 $Pointer8 $Dims8 $Const8 $Volatile8 $ArrayOpt8]
lappend PortList $Port8
set PortName9 "s_axis_session_lup_rsp"
set BitWidth9 "96"
set ArrayOpt9 ""
set Const9 "0"
set Volatile9 "0"
set Pointer9 "2"
set Reference9 "1"
set Dims9 [list 0]
set Interface9 [list AP_STREAM 0] 
set structMem9 ""
set PortName90 "key"
set BitWidth90 "64"
set ArrayOpt90 ""
set Const90 "0"
set Volatile90 "0"
set Pointer90 "0"
set Reference90 "0"
set Dims90 [list 0]
set Interface90 "wire"
set structMem90 ""
set PortName900 "theirIp"
set BitWidth900 "32"
set ArrayOpt900 ""
set Const900 "0"
set Volatile900 "0"
set Pointer900 "0"
set Reference900 "0"
set Dims900 [list 0]
set Interface900 "wire"
set DataType900 "[list ap_uint 32 ]"
set Port900 [list $PortName900 $Interface900 $DataType900 $Pointer900 $Dims900 $Const900 $Volatile900 $ArrayOpt900]
lappend structMem90 $Port900
set PortName901 "myPort"
set BitWidth901 "16"
set ArrayOpt901 ""
set Const901 "0"
set Volatile901 "0"
set Pointer901 "0"
set Reference901 "0"
set Dims901 [list 0]
set Interface901 "wire"
set DataType901 "[list ap_uint 16 ]"
set Port901 [list $PortName901 $Interface901 $DataType901 $Pointer901 $Dims901 $Const901 $Volatile901 $ArrayOpt901]
lappend structMem90 $Port901
set PortName902 "theirPort"
set BitWidth902 "16"
set ArrayOpt902 ""
set Const902 "0"
set Volatile902 "0"
set Pointer902 "0"
set Reference902 "0"
set Dims902 [list 0]
set Interface902 "wire"
set DataType902 "[list ap_uint 16 ]"
set Port902 [list $PortName902 $Interface902 $DataType902 $Pointer902 $Dims902 $Const902 $Volatile902 $ArrayOpt902]
lappend structMem90 $Port902
set structParameter90 [list ]
set structArgument90 [list ]
set NameSpace90 [list ]
set structIsPacked90 "0"
set DataType90 [list "threeTupleInternal" "struct threeTupleInternal" $structMem90 1 0 $structParameter90 $structArgument90 $NameSpace90 $structIsPacked90]
set Port90 [list $PortName90 $Interface90 $DataType90 $Pointer90 $Dims90 $Const90 $Volatile90 $ArrayOpt90]
lappend structMem9 $Port90
set PortName91 "sessionID"
set BitWidth91 "16"
set ArrayOpt91 ""
set Const91 "0"
set Volatile91 "0"
set Pointer91 "0"
set Reference91 "0"
set Dims91 [list 0]
set Interface91 "wire"
set DataType91 "[list ap_uint 16 ]"
set Port91 [list $PortName91 $Interface91 $DataType91 $Pointer91 $Dims91 $Const91 $Volatile91 $ArrayOpt91]
lappend structMem9 $Port91
set PortName92 "hit"
set BitWidth92 "8"
set ArrayOpt92 ""
set Const92 "0"
set Volatile92 "0"
set Pointer92 "0"
set Reference92 "0"
set Dims92 [list 0]
set Interface92 "wire"
set DataType92 "bool"
set Port92 [list $PortName92 $Interface92 $DataType92 $Pointer92 $Dims92 $Const92 $Volatile92 $ArrayOpt92]
lappend structMem9 $Port92
set PortName93 "source"
set BitWidth93 "1"
set ArrayOpt93 ""
set Const93 "0"
set Volatile93 "0"
set Pointer93 "0"
set Reference93 "0"
set Dims93 [list 0]
set Interface93 "wire"
set DataType93 "[list lookupSource enum 1]"
set Port93 [list $PortName93 $Interface93 $DataType93 $Pointer93 $Dims93 $Const93 $Volatile93 $ArrayOpt93]
lappend structMem9 $Port93
set structParameter9 [list ]
set structArgument9 [list ]
set NameSpace9 [list ]
set structIsPacked9 "0"
set DataType9 [list "rtlSessionLookupReply" "struct rtlSessionLookupReply" $structMem9 1 0 $structParameter9 $structArgument9 $NameSpace9 $structIsPacked9]
set Port9 [list $PortName9 $Interface9 $DataType9 $Pointer9 $Dims9 $Const9 $Volatile9 $ArrayOpt9]
lappend PortList $Port9
set PortName10 "s_axis_session_upd_rsp"
set BitWidth10 "128"
set ArrayOpt10 ""
set Const10 "0"
set Volatile10 "0"
set Pointer10 "2"
set Reference10 "1"
set Dims10 [list 0]
set Interface10 [list AP_STREAM 0] 
set structMem10 ""
set PortName100 "op"
set BitWidth100 "1"
set ArrayOpt100 ""
set Const100 "0"
set Volatile100 "0"
set Pointer100 "0"
set Reference100 "0"
set Dims100 [list 0]
set Interface100 "wire"
set DataType100 "[list lookupOp enum 1]"
set Port100 [list $PortName100 $Interface100 $DataType100 $Pointer100 $Dims100 $Const100 $Volatile100 $ArrayOpt100]
lappend structMem10 $Port100
set PortName101 "key"
set BitWidth101 "64"
set ArrayOpt101 ""
set Const101 "0"
set Volatile101 "0"
set Pointer101 "0"
set Reference101 "0"
set Dims101 [list 0]
set Interface101 "wire"
set structMem101 ""
set PortName1010 "theirIp"
set BitWidth1010 "32"
set ArrayOpt1010 ""
set Const1010 "0"
set Volatile1010 "0"
set Pointer1010 "0"
set Reference1010 "0"
set Dims1010 [list 0]
set Interface1010 "wire"
set DataType1010 "[list ap_uint 32 ]"
set Port1010 [list $PortName1010 $Interface1010 $DataType1010 $Pointer1010 $Dims1010 $Const1010 $Volatile1010 $ArrayOpt1010]
lappend structMem101 $Port1010
set PortName1011 "myPort"
set BitWidth1011 "16"
set ArrayOpt1011 ""
set Const1011 "0"
set Volatile1011 "0"
set Pointer1011 "0"
set Reference1011 "0"
set Dims1011 [list 0]
set Interface1011 "wire"
set DataType1011 "[list ap_uint 16 ]"
set Port1011 [list $PortName1011 $Interface1011 $DataType1011 $Pointer1011 $Dims1011 $Const1011 $Volatile1011 $ArrayOpt1011]
lappend structMem101 $Port1011
set PortName1012 "theirPort"
set BitWidth1012 "16"
set ArrayOpt1012 ""
set Const1012 "0"
set Volatile1012 "0"
set Pointer1012 "0"
set Reference1012 "0"
set Dims1012 [list 0]
set Interface1012 "wire"
set DataType1012 "[list ap_uint 16 ]"
set Port1012 [list $PortName1012 $Interface1012 $DataType1012 $Pointer1012 $Dims1012 $Const1012 $Volatile1012 $ArrayOpt1012]
lappend structMem101 $Port1012
set structParameter101 [list ]
set structArgument101 [list ]
set NameSpace101 [list ]
set structIsPacked101 "0"
set DataType101 [list "threeTupleInternal" "struct threeTupleInternal" $structMem101 1 0 $structParameter101 $structArgument101 $NameSpace101 $structIsPacked101]
set Port101 [list $PortName101 $Interface101 $DataType101 $Pointer101 $Dims101 $Const101 $Volatile101 $ArrayOpt101]
lappend structMem10 $Port101
set PortName102 "sessionID"
set BitWidth102 "16"
set ArrayOpt102 ""
set Const102 "0"
set Volatile102 "0"
set Pointer102 "0"
set Reference102 "0"
set Dims102 [list 0]
set Interface102 "wire"
set DataType102 "[list ap_uint 16 ]"
set Port102 [list $PortName102 $Interface102 $DataType102 $Pointer102 $Dims102 $Const102 $Volatile102 $ArrayOpt102]
lappend structMem10 $Port102
set PortName103 "success"
set BitWidth103 "8"
set ArrayOpt103 ""
set Const103 "0"
set Volatile103 "0"
set Pointer103 "0"
set Reference103 "0"
set Dims103 [list 0]
set Interface103 "wire"
set DataType103 "bool"
set Port103 [list $PortName103 $Interface103 $DataType103 $Pointer103 $Dims103 $Const103 $Volatile103 $ArrayOpt103]
lappend structMem10 $Port103
set PortName104 "source"
set BitWidth104 "1"
set ArrayOpt104 ""
set Const104 "0"
set Volatile104 "0"
set Pointer104 "0"
set Reference104 "0"
set Dims104 [list 0]
set Interface104 "wire"
set DataType104 "[list lookupSource enum 1]"
set Port104 [list $PortName104 $Interface104 $DataType104 $Pointer104 $Dims104 $Const104 $Volatile104 $ArrayOpt104]
lappend structMem10 $Port104
set structParameter10 [list ]
set structArgument10 [list ]
set NameSpace10 [list ]
set structIsPacked10 "0"
set DataType10 [list "rtlSessionUpdateReply" "struct rtlSessionUpdateReply" $structMem10 1 0 $structParameter10 $structArgument10 $NameSpace10 $structIsPacked10]
set Port10 [list $PortName10 $Interface10 $DataType10 $Pointer10 $Dims10 $Const10 $Volatile10 $ArrayOpt10]
lappend PortList $Port10
set PortName11 "m_axis_session_lup_req"
set BitWidth11 "96"
set ArrayOpt11 ""
set Const11 "0"
set Volatile11 "0"
set Pointer11 "2"
set Reference11 "1"
set Dims11 [list 0]
set Interface11 [list AP_STREAM 0] 
set structMem11 ""
set PortName110 "key"
set BitWidth110 "64"
set ArrayOpt110 ""
set Const110 "0"
set Volatile110 "0"
set Pointer110 "0"
set Reference110 "0"
set Dims110 [list 0]
set Interface110 "wire"
set structMem110 ""
set PortName1100 "theirIp"
set BitWidth1100 "32"
set ArrayOpt1100 ""
set Const1100 "0"
set Volatile1100 "0"
set Pointer1100 "0"
set Reference1100 "0"
set Dims1100 [list 0]
set Interface1100 "wire"
set DataType1100 "[list ap_uint 32 ]"
set Port1100 [list $PortName1100 $Interface1100 $DataType1100 $Pointer1100 $Dims1100 $Const1100 $Volatile1100 $ArrayOpt1100]
lappend structMem110 $Port1100
set PortName1101 "myPort"
set BitWidth1101 "16"
set ArrayOpt1101 ""
set Const1101 "0"
set Volatile1101 "0"
set Pointer1101 "0"
set Reference1101 "0"
set Dims1101 [list 0]
set Interface1101 "wire"
set DataType1101 "[list ap_uint 16 ]"
set Port1101 [list $PortName1101 $Interface1101 $DataType1101 $Pointer1101 $Dims1101 $Const1101 $Volatile1101 $ArrayOpt1101]
lappend structMem110 $Port1101
set PortName1102 "theirPort"
set BitWidth1102 "16"
set ArrayOpt1102 ""
set Const1102 "0"
set Volatile1102 "0"
set Pointer1102 "0"
set Reference1102 "0"
set Dims1102 [list 0]
set Interface1102 "wire"
set DataType1102 "[list ap_uint 16 ]"
set Port1102 [list $PortName1102 $Interface1102 $DataType1102 $Pointer1102 $Dims1102 $Const1102 $Volatile1102 $ArrayOpt1102]
lappend structMem110 $Port1102
set structParameter110 [list ]
set structArgument110 [list ]
set NameSpace110 [list ]
set structIsPacked110 "0"
set DataType110 [list "threeTupleInternal" "struct threeTupleInternal" $structMem110 1 0 $structParameter110 $structArgument110 $NameSpace110 $structIsPacked110]
set Port110 [list $PortName110 $Interface110 $DataType110 $Pointer110 $Dims110 $Const110 $Volatile110 $ArrayOpt110]
lappend structMem11 $Port110
set PortName111 "source"
set BitWidth111 "1"
set ArrayOpt111 ""
set Const111 "0"
set Volatile111 "0"
set Pointer111 "0"
set Reference111 "0"
set Dims111 [list 0]
set Interface111 "wire"
set DataType111 "[list lookupSource enum 1]"
set Port111 [list $PortName111 $Interface111 $DataType111 $Pointer111 $Dims111 $Const111 $Volatile111 $ArrayOpt111]
lappend structMem11 $Port111
set structParameter11 [list ]
set structArgument11 [list ]
set NameSpace11 [list ]
set structIsPacked11 "0"
set DataType11 [list "rtlSessionLookupRequest" "struct rtlSessionLookupRequest" $structMem11 1 0 $structParameter11 $structArgument11 $NameSpace11 $structIsPacked11]
set Port11 [list $PortName11 $Interface11 $DataType11 $Pointer11 $Dims11 $Const11 $Volatile11 $ArrayOpt11]
lappend PortList $Port11
set PortName12 "m_axis_session_upd_req"
set BitWidth12 "128"
set ArrayOpt12 ""
set Const12 "0"
set Volatile12 "0"
set Pointer12 "2"
set Reference12 "1"
set Dims12 [list 0]
set Interface12 [list AP_STREAM 0] 
set structMem12 ""
set PortName120 "op"
set BitWidth120 "1"
set ArrayOpt120 ""
set Const120 "0"
set Volatile120 "0"
set Pointer120 "0"
set Reference120 "0"
set Dims120 [list 0]
set Interface120 "wire"
set DataType120 "[list lookupOp enum 1]"
set Port120 [list $PortName120 $Interface120 $DataType120 $Pointer120 $Dims120 $Const120 $Volatile120 $ArrayOpt120]
lappend structMem12 $Port120
set PortName121 "key"
set BitWidth121 "64"
set ArrayOpt121 ""
set Const121 "0"
set Volatile121 "0"
set Pointer121 "0"
set Reference121 "0"
set Dims121 [list 0]
set Interface121 "wire"
set structMem121 ""
set PortName1210 "theirIp"
set BitWidth1210 "32"
set ArrayOpt1210 ""
set Const1210 "0"
set Volatile1210 "0"
set Pointer1210 "0"
set Reference1210 "0"
set Dims1210 [list 0]
set Interface1210 "wire"
set DataType1210 "[list ap_uint 32 ]"
set Port1210 [list $PortName1210 $Interface1210 $DataType1210 $Pointer1210 $Dims1210 $Const1210 $Volatile1210 $ArrayOpt1210]
lappend structMem121 $Port1210
set PortName1211 "myPort"
set BitWidth1211 "16"
set ArrayOpt1211 ""
set Const1211 "0"
set Volatile1211 "0"
set Pointer1211 "0"
set Reference1211 "0"
set Dims1211 [list 0]
set Interface1211 "wire"
set DataType1211 "[list ap_uint 16 ]"
set Port1211 [list $PortName1211 $Interface1211 $DataType1211 $Pointer1211 $Dims1211 $Const1211 $Volatile1211 $ArrayOpt1211]
lappend structMem121 $Port1211
set PortName1212 "theirPort"
set BitWidth1212 "16"
set ArrayOpt1212 ""
set Const1212 "0"
set Volatile1212 "0"
set Pointer1212 "0"
set Reference1212 "0"
set Dims1212 [list 0]
set Interface1212 "wire"
set DataType1212 "[list ap_uint 16 ]"
set Port1212 [list $PortName1212 $Interface1212 $DataType1212 $Pointer1212 $Dims1212 $Const1212 $Volatile1212 $ArrayOpt1212]
lappend structMem121 $Port1212
set structParameter121 [list ]
set structArgument121 [list ]
set NameSpace121 [list ]
set structIsPacked121 "0"
set DataType121 [list "threeTupleInternal" "struct threeTupleInternal" $structMem121 1 0 $structParameter121 $structArgument121 $NameSpace121 $structIsPacked121]
set Port121 [list $PortName121 $Interface121 $DataType121 $Pointer121 $Dims121 $Const121 $Volatile121 $ArrayOpt121]
lappend structMem12 $Port121
set PortName122 "value"
set BitWidth122 "16"
set ArrayOpt122 ""
set Const122 "0"
set Volatile122 "0"
set Pointer122 "0"
set Reference122 "0"
set Dims122 [list 0]
set Interface122 "wire"
set DataType122 "[list ap_uint 16 ]"
set Port122 [list $PortName122 $Interface122 $DataType122 $Pointer122 $Dims122 $Const122 $Volatile122 $ArrayOpt122]
lappend structMem12 $Port122
set PortName123 "source"
set BitWidth123 "1"
set ArrayOpt123 ""
set Const123 "0"
set Volatile123 "0"
set Pointer123 "0"
set Reference123 "0"
set Dims123 [list 0]
set Interface123 "wire"
set DataType123 "[list lookupSource enum 1]"
set Port123 [list $PortName123 $Interface123 $DataType123 $Pointer123 $Dims123 $Const123 $Volatile123 $ArrayOpt123]
lappend structMem12 $Port123
set structParameter12 [list ]
set structArgument12 [list ]
set NameSpace12 [list ]
set structIsPacked12 "0"
set DataType12 [list "rtlSessionUpdateRequest" "struct rtlSessionUpdateRequest" $structMem12 1 0 $structParameter12 $structArgument12 $NameSpace12 $structIsPacked12]
set Port12 [list $PortName12 $Interface12 $DataType12 $Pointer12 $Dims12 $Const12 $Volatile12 $ArrayOpt12]
lappend PortList $Port12
set PortName13 "s_axis_listen_port_req"
set BitWidth13 "16"
set ArrayOpt13 ""
set Const13 "0"
set Volatile13 "0"
set Pointer13 "2"
set Reference13 "1"
set Dims13 [list 0]
set Interface13 [list AP_STREAM 0] 
set DataType13 "[list ap_uint 16 ]"
set Port13 [list $PortName13 $Interface13 $DataType13 $Pointer13 $Dims13 $Const13 $Volatile13 $ArrayOpt13]
lappend PortList $Port13
set PortName14 "s_axis_rx_data_req"
set BitWidth14 "32"
set ArrayOpt14 ""
set Const14 "0"
set Volatile14 "0"
set Pointer14 "2"
set Reference14 "1"
set Dims14 [list 0]
set Interface14 [list AP_STREAM 0] 
set structMem14 ""
set PortName140 "sessionID"
set BitWidth140 "16"
set ArrayOpt140 ""
set Const140 "0"
set Volatile140 "0"
set Pointer140 "0"
set Reference140 "0"
set Dims140 [list 0]
set Interface140 "wire"
set DataType140 "[list ap_uint 16 ]"
set Port140 [list $PortName140 $Interface140 $DataType140 $Pointer140 $Dims140 $Const140 $Volatile140 $ArrayOpt140]
lappend structMem14 $Port140
set PortName141 "length"
set BitWidth141 "16"
set ArrayOpt141 ""
set Const141 "0"
set Volatile141 "0"
set Pointer141 "0"
set Reference141 "0"
set Dims141 [list 0]
set Interface141 "wire"
set DataType141 "[list ap_uint 16 ]"
set Port141 [list $PortName141 $Interface141 $DataType141 $Pointer141 $Dims141 $Const141 $Volatile141 $ArrayOpt141]
lappend structMem14 $Port141
set structParameter14 [list ]
set structArgument14 [list ]
set NameSpace14 [list ]
set structIsPacked14 "0"
set DataType14 [list "appReadRequest" "struct appReadRequest" $structMem14 1 0 $structParameter14 $structArgument14 $NameSpace14 $structIsPacked14]
set Port14 [list $PortName14 $Interface14 $DataType14 $Pointer14 $Dims14 $Const14 $Volatile14 $ArrayOpt14]
lappend PortList $Port14
set PortName15 "s_axis_open_conn_req"
set BitWidth15 "64"
set ArrayOpt15 ""
set Const15 "0"
set Volatile15 "0"
set Pointer15 "2"
set Reference15 "1"
set Dims15 [list 0]
set Interface15 [list AP_STREAM 0] 
set structMem15 ""
set PortName150 "ip_address"
set BitWidth150 "32"
set ArrayOpt150 ""
set Const150 "0"
set Volatile150 "0"
set Pointer150 "0"
set Reference150 "0"
set Dims150 [list 0]
set Interface150 "wire"
set DataType150 "[list ap_uint 32 ]"
set Port150 [list $PortName150 $Interface150 $DataType150 $Pointer150 $Dims150 $Const150 $Volatile150 $ArrayOpt150]
lappend structMem15 $Port150
set PortName151 "ip_port"
set BitWidth151 "16"
set ArrayOpt151 ""
set Const151 "0"
set Volatile151 "0"
set Pointer151 "0"
set Reference151 "0"
set Dims151 [list 0]
set Interface151 "wire"
set DataType151 "[list ap_uint 16 ]"
set Port151 [list $PortName151 $Interface151 $DataType151 $Pointer151 $Dims151 $Const151 $Volatile151 $ArrayOpt151]
lappend structMem15 $Port151
set structParameter15 [list ]
set structArgument15 [list ]
set NameSpace15 [list ]
set structIsPacked15 "0"
set DataType15 [list "ipTuple" "struct ipTuple" $structMem15 1 0 $structParameter15 $structArgument15 $NameSpace15 $structIsPacked15]
set Port15 [list $PortName15 $Interface15 $DataType15 $Pointer15 $Dims15 $Const15 $Volatile15 $ArrayOpt15]
lappend PortList $Port15
set PortName16 "s_axis_close_conn_req"
set BitWidth16 "16"
set ArrayOpt16 ""
set Const16 "0"
set Volatile16 "0"
set Pointer16 "2"
set Reference16 "1"
set Dims16 [list 0]
set Interface16 [list AP_STREAM 0] 
set DataType16 "[list ap_uint 16 ]"
set Port16 [list $PortName16 $Interface16 $DataType16 $Pointer16 $Dims16 $Const16 $Volatile16 $ArrayOpt16]
lappend PortList $Port16
set PortName17 "s_axis_tx_data_req_metadata"
set BitWidth17 "32"
set ArrayOpt17 ""
set Const17 "0"
set Volatile17 "0"
set Pointer17 "2"
set Reference17 "1"
set Dims17 [list 0]
set Interface17 [list AP_STREAM 0] 
set structMem17 ""
set PortName170 "sessionID"
set BitWidth170 "16"
set ArrayOpt170 ""
set Const170 "0"
set Volatile170 "0"
set Pointer170 "0"
set Reference170 "0"
set Dims170 [list 0]
set Interface170 "wire"
set DataType170 "[list ap_uint 16 ]"
set Port170 [list $PortName170 $Interface170 $DataType170 $Pointer170 $Dims170 $Const170 $Volatile170 $ArrayOpt170]
lappend structMem17 $Port170
set PortName171 "length"
set BitWidth171 "16"
set ArrayOpt171 ""
set Const171 "0"
set Volatile171 "0"
set Pointer171 "0"
set Reference171 "0"
set Dims171 [list 0]
set Interface171 "wire"
set DataType171 "[list ap_uint 16 ]"
set Port171 [list $PortName171 $Interface171 $DataType171 $Pointer171 $Dims171 $Const171 $Volatile171 $ArrayOpt171]
lappend structMem17 $Port171
set structParameter17 [list ]
set structArgument17 [list ]
set NameSpace17 [list ]
set structIsPacked17 "0"
set DataType17 [list "appTxMeta" "struct appTxMeta" $structMem17 1 0 $structParameter17 $structArgument17 $NameSpace17 $structIsPacked17]
set Port17 [list $PortName17 $Interface17 $DataType17 $Pointer17 $Dims17 $Const17 $Volatile17 $ArrayOpt17]
lappend PortList $Port17
set PortName18 "s_axis_tx_data_req"
set BitWidth18 "128"
set ArrayOpt18 ""
set Const18 "0"
set Volatile18 "0"
set Pointer18 "2"
set Reference18 "1"
set Dims18 [list 0]
set Interface18 [list AP_STREAM 0] 
set structMem18 ""
set PortName180 "data"
set BitWidth180 "64"
set ArrayOpt180 ""
set Const180 "0"
set Volatile180 "0"
set Pointer180 "0"
set Reference180 "0"
set Dims180 [list 0]
set Interface180 "wire"
set DataType180 "[list ap_uint 64 ]"
set Port180 [list $PortName180 $Interface180 $DataType180 $Pointer180 $Dims180 $Const180 $Volatile180 $ArrayOpt180]
lappend structMem18 $Port180
set PortName181 "keep"
set BitWidth181 "8"
set ArrayOpt181 ""
set Const181 "0"
set Volatile181 "0"
set Pointer181 "0"
set Reference181 "0"
set Dims181 [list 0]
set Interface181 "wire"
set DataType181 "[list ap_uint 8 ]"
set Port181 [list $PortName181 $Interface181 $DataType181 $Pointer181 $Dims181 $Const181 $Volatile181 $ArrayOpt181]
lappend structMem18 $Port181
set PortName182 "last"
set BitWidth182 "8"
set ArrayOpt182 ""
set Const182 "0"
set Volatile182 "0"
set Pointer182 "0"
set Reference182 "0"
set Dims182 [list 0]
set Interface182 "wire"
set DataType182 "[list ap_uint 1 ]"
set Port182 [list $PortName182 $Interface182 $DataType182 $Pointer182 $Dims182 $Const182 $Volatile182 $ArrayOpt182]
lappend structMem18 $Port182
set DataType18tp0 "int"
set structParameter18 [list [list $DataType18tp0 D] ]
set structArgument18 [list 64 ]
set NameSpace18 [list ]
set structIsPacked18 "0"
set DataType18 [list "net_axis<64>" "struct net_axis" $structMem18 1 0 $structParameter18 $structArgument18 $NameSpace18 $structIsPacked18]
set Port18 [list $PortName18 $Interface18 $DataType18 $Pointer18 $Dims18 $Const18 $Volatile18 $ArrayOpt18]
lappend PortList $Port18
set PortName19 "m_axis_listen_port_rsp"
set BitWidth19 "8"
set ArrayOpt19 ""
set Const19 "0"
set Volatile19 "0"
set Pointer19 "2"
set Reference19 "1"
set Dims19 [list 0]
set Interface19 [list AP_STREAM 0] 
set DataType19 "bool"
set Port19 [list $PortName19 $Interface19 $DataType19 $Pointer19 $Dims19 $Const19 $Volatile19 $ArrayOpt19]
lappend PortList $Port19
set PortName20 "m_axis_notification"
set BitWidth20 "96"
set ArrayOpt20 ""
set Const20 "0"
set Volatile20 "0"
set Pointer20 "2"
set Reference20 "1"
set Dims20 [list 0]
set Interface20 [list AP_STREAM 0] 
set structMem20 ""
set PortName200 "sessionID"
set BitWidth200 "16"
set ArrayOpt200 ""
set Const200 "0"
set Volatile200 "0"
set Pointer200 "0"
set Reference200 "0"
set Dims200 [list 0]
set Interface200 "wire"
set DataType200 "[list ap_uint 16 ]"
set Port200 [list $PortName200 $Interface200 $DataType200 $Pointer200 $Dims200 $Const200 $Volatile200 $ArrayOpt200]
lappend structMem20 $Port200
set PortName201 "length"
set BitWidth201 "16"
set ArrayOpt201 ""
set Const201 "0"
set Volatile201 "0"
set Pointer201 "0"
set Reference201 "0"
set Dims201 [list 0]
set Interface201 "wire"
set DataType201 "[list ap_uint 16 ]"
set Port201 [list $PortName201 $Interface201 $DataType201 $Pointer201 $Dims201 $Const201 $Volatile201 $ArrayOpt201]
lappend structMem20 $Port201
set PortName202 "ipAddress"
set BitWidth202 "32"
set ArrayOpt202 ""
set Const202 "0"
set Volatile202 "0"
set Pointer202 "0"
set Reference202 "0"
set Dims202 [list 0]
set Interface202 "wire"
set DataType202 "[list ap_uint 32 ]"
set Port202 [list $PortName202 $Interface202 $DataType202 $Pointer202 $Dims202 $Const202 $Volatile202 $ArrayOpt202]
lappend structMem20 $Port202
set PortName203 "dstPort"
set BitWidth203 "16"
set ArrayOpt203 ""
set Const203 "0"
set Volatile203 "0"
set Pointer203 "0"
set Reference203 "0"
set Dims203 [list 0]
set Interface203 "wire"
set DataType203 "[list ap_uint 16 ]"
set Port203 [list $PortName203 $Interface203 $DataType203 $Pointer203 $Dims203 $Const203 $Volatile203 $ArrayOpt203]
lappend structMem20 $Port203
set PortName204 "closed"
set BitWidth204 "8"
set ArrayOpt204 ""
set Const204 "0"
set Volatile204 "0"
set Pointer204 "0"
set Reference204 "0"
set Dims204 [list 0]
set Interface204 "wire"
set DataType204 "bool"
set Port204 [list $PortName204 $Interface204 $DataType204 $Pointer204 $Dims204 $Const204 $Volatile204 $ArrayOpt204]
lappend structMem20 $Port204
set structParameter20 [list ]
set structArgument20 [list ]
set NameSpace20 [list ]
set structIsPacked20 "0"
set DataType20 [list "appNotification" "struct appNotification" $structMem20 1 0 $structParameter20 $structArgument20 $NameSpace20 $structIsPacked20]
set Port20 [list $PortName20 $Interface20 $DataType20 $Pointer20 $Dims20 $Const20 $Volatile20 $ArrayOpt20]
lappend PortList $Port20
set PortName21 "m_axis_rx_data_rsp_metadata"
set BitWidth21 "16"
set ArrayOpt21 ""
set Const21 "0"
set Volatile21 "0"
set Pointer21 "2"
set Reference21 "1"
set Dims21 [list 0]
set Interface21 [list AP_STREAM 0] 
set DataType21 "[list ap_uint 16 ]"
set Port21 [list $PortName21 $Interface21 $DataType21 $Pointer21 $Dims21 $Const21 $Volatile21 $ArrayOpt21]
lappend PortList $Port21
set PortName22 "m_axis_rx_data_rsp"
set BitWidth22 "128"
set ArrayOpt22 ""
set Const22 "0"
set Volatile22 "0"
set Pointer22 "2"
set Reference22 "1"
set Dims22 [list 0]
set Interface22 [list AP_STREAM 0] 
set structMem22 ""
set PortName220 "data"
set BitWidth220 "64"
set ArrayOpt220 ""
set Const220 "0"
set Volatile220 "0"
set Pointer220 "0"
set Reference220 "0"
set Dims220 [list 0]
set Interface220 "wire"
set DataType220 "[list ap_uint 64 ]"
set Port220 [list $PortName220 $Interface220 $DataType220 $Pointer220 $Dims220 $Const220 $Volatile220 $ArrayOpt220]
lappend structMem22 $Port220
set PortName221 "keep"
set BitWidth221 "8"
set ArrayOpt221 ""
set Const221 "0"
set Volatile221 "0"
set Pointer221 "0"
set Reference221 "0"
set Dims221 [list 0]
set Interface221 "wire"
set DataType221 "[list ap_uint 8 ]"
set Port221 [list $PortName221 $Interface221 $DataType221 $Pointer221 $Dims221 $Const221 $Volatile221 $ArrayOpt221]
lappend structMem22 $Port221
set PortName222 "last"
set BitWidth222 "8"
set ArrayOpt222 ""
set Const222 "0"
set Volatile222 "0"
set Pointer222 "0"
set Reference222 "0"
set Dims222 [list 0]
set Interface222 "wire"
set DataType222 "[list ap_uint 1 ]"
set Port222 [list $PortName222 $Interface222 $DataType222 $Pointer222 $Dims222 $Const222 $Volatile222 $ArrayOpt222]
lappend structMem22 $Port222
set DataType22tp0 "int"
set structParameter22 [list [list $DataType22tp0 D] ]
set structArgument22 [list 64 ]
set NameSpace22 [list ]
set structIsPacked22 "0"
set DataType22 [list "net_axis<64>" "struct net_axis" $structMem22 1 0 $structParameter22 $structArgument22 $NameSpace22 $structIsPacked22]
set Port22 [list $PortName22 $Interface22 $DataType22 $Pointer22 $Dims22 $Const22 $Volatile22 $ArrayOpt22]
lappend PortList $Port22
set PortName23 "m_axis_open_conn_rsp"
set BitWidth23 "32"
set ArrayOpt23 ""
set Const23 "0"
set Volatile23 "0"
set Pointer23 "2"
set Reference23 "1"
set Dims23 [list 0]
set Interface23 [list AP_STREAM 0] 
set structMem23 ""
set PortName230 "sessionID"
set BitWidth230 "16"
set ArrayOpt230 ""
set Const230 "0"
set Volatile230 "0"
set Pointer230 "0"
set Reference230 "0"
set Dims230 [list 0]
set Interface230 "wire"
set DataType230 "[list ap_uint 16 ]"
set Port230 [list $PortName230 $Interface230 $DataType230 $Pointer230 $Dims230 $Const230 $Volatile230 $ArrayOpt230]
lappend structMem23 $Port230
set PortName231 "success"
set BitWidth231 "8"
set ArrayOpt231 ""
set Const231 "0"
set Volatile231 "0"
set Pointer231 "0"
set Reference231 "0"
set Dims231 [list 0]
set Interface231 "wire"
set DataType231 "bool"
set Port231 [list $PortName231 $Interface231 $DataType231 $Pointer231 $Dims231 $Const231 $Volatile231 $ArrayOpt231]
lappend structMem23 $Port231
set structParameter23 [list ]
set structArgument23 [list ]
set NameSpace23 [list ]
set structIsPacked23 "0"
set DataType23 [list "openStatus" "struct openStatus" $structMem23 1 0 $structParameter23 $structArgument23 $NameSpace23 $structIsPacked23]
set Port23 [list $PortName23 $Interface23 $DataType23 $Pointer23 $Dims23 $Const23 $Volatile23 $ArrayOpt23]
lappend PortList $Port23
set PortName24 "m_axis_tx_data_rsp"
set BitWidth24 "96"
set ArrayOpt24 ""
set Const24 "0"
set Volatile24 "0"
set Pointer24 "2"
set Reference24 "1"
set Dims24 [list 0]
set Interface24 [list AP_STREAM 0] 
set structMem24 ""
set PortName240 "sessionID"
set BitWidth240 "16"
set ArrayOpt240 ""
set Const240 "0"
set Volatile240 "0"
set Pointer240 "0"
set Reference240 "0"
set Dims240 [list 0]
set Interface240 "wire"
set DataType240 "[list ap_uint 16 ]"
set Port240 [list $PortName240 $Interface240 $DataType240 $Pointer240 $Dims240 $Const240 $Volatile240 $ArrayOpt240]
lappend structMem24 $Port240
set PortName241 "length"
set BitWidth241 "16"
set ArrayOpt241 ""
set Const241 "0"
set Volatile241 "0"
set Pointer241 "0"
set Reference241 "0"
set Dims241 [list 0]
set Interface241 "wire"
set DataType241 "[list ap_uint 16 ]"
set Port241 [list $PortName241 $Interface241 $DataType241 $Pointer241 $Dims241 $Const241 $Volatile241 $ArrayOpt241]
lappend structMem24 $Port241
set PortName242 "remaining_space"
set BitWidth242 "32"
set ArrayOpt242 ""
set Const242 "0"
set Volatile242 "0"
set Pointer242 "0"
set Reference242 "0"
set Dims242 [list 0]
set Interface242 "wire"
set DataType242 "[list ap_uint 30 ]"
set Port242 [list $PortName242 $Interface242 $DataType242 $Pointer242 $Dims242 $Const242 $Volatile242 $ArrayOpt242]
lappend structMem24 $Port242
set PortName243 "error"
set BitWidth243 "8"
set ArrayOpt243 ""
set Const243 "0"
set Volatile243 "0"
set Pointer243 "0"
set Reference243 "0"
set Dims243 [list 0]
set Interface243 "wire"
set DataType243 "[list ap_uint 2 ]"
set Port243 [list $PortName243 $Interface243 $DataType243 $Pointer243 $Dims243 $Const243 $Volatile243 $ArrayOpt243]
lappend structMem24 $Port243
set structParameter24 [list ]
set structArgument24 [list ]
set NameSpace24 [list ]
set structIsPacked24 "0"
set DataType24 [list "appTxRsp" "struct appTxRsp" $structMem24 1 0 $structParameter24 $structArgument24 $NameSpace24 $structIsPacked24]
set Port24 [list $PortName24 $Interface24 $DataType24 $Pointer24 $Dims24 $Const24 $Volatile24 $ArrayOpt24]
lappend PortList $Port24
set PortName25 "axis_data_count"
set BitWidth25 "16"
set ArrayOpt25 ""
set Const25 "0"
set Volatile25 "0"
set Pointer25 "0"
set Reference25 "0"
set Dims25 [list 0]
set Interface25 "wire"
set DataType25 "[list ap_uint 16 ]"
set Port25 [list $PortName25 $Interface25 $DataType25 $Pointer25 $Dims25 $Const25 $Volatile25 $ArrayOpt25]
lappend PortList $Port25
set PortName26 "axis_max_data_count"
set BitWidth26 "16"
set ArrayOpt26 ""
set Const26 "0"
set Volatile26 "0"
set Pointer26 "0"
set Reference26 "0"
set Dims26 [list 0]
set Interface26 "wire"
set DataType26 "[list ap_uint 16 ]"
set Port26 [list $PortName26 $Interface26 $DataType26 $Pointer26 $Dims26 $Const26 $Volatile26 $ArrayOpt26]
lappend PortList $Port26
set PortName27 "myIpAddress"
set BitWidth27 "32"
set ArrayOpt27 ""
set Const27 "0"
set Volatile27 "0"
set Pointer27 "0"
set Reference27 "0"
set Dims27 [list 0]
set Interface27 "wire"
set DataType27 "[list ap_uint 32 ]"
set Port27 [list $PortName27 $Interface27 $DataType27 $Pointer27 $Dims27 $Const27 $Volatile27 $ArrayOpt27]
lappend PortList $Port27
set PortName28 "regSessionCount"
set BitWidth28 "16"
set ArrayOpt28 ""
set Const28 "0"
set Volatile28 "0"
set Pointer28 "2"
set Reference28 "1"
set Dims28 [list 0]
set Interface28 "wire"
set DataType28 "[list ap_uint 16 ]"
set Port28 [list $PortName28 $Interface28 $DataType28 $Pointer28 $Dims28 $Const28 $Volatile28 $ArrayOpt28]
lappend PortList $Port28
set globalAPint "" 
set returnAPInt "" 
set hasCPPAPInt 1 
set argAPInt "" 
set hasCPPAPFix 0 
set hasSCFix 0 
set hasCBool 0 
set hasCPPComplex 0 
set isTemplateTop 0
set hasHalf 0 
set dataPackList ""
set module [list $moduleName $PortList $rawDecl $argAPInt $returnAPInt $dataPackList]
