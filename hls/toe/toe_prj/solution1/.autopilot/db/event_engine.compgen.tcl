# This script segment is generated automatically by AutoPilot

# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 149 \
    name rxEng2eventEng_setEv_1 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng2eventEng_setEv_1 \
    op interface \
    ports { rxEng2eventEng_setEv_1_dout { I 152 vector } rxEng2eventEng_setEv_1_empty_n { I 1 bit } rxEng2eventEng_setEv_1_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 150 \
    name eventEng2ackDelay_ev_1 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_eventEng2ackDelay_ev_1 \
    op interface \
    ports { eventEng2ackDelay_ev_1_din { O 152 vector } eventEng2ackDelay_ev_1_full_n { I 1 bit } eventEng2ackDelay_ev_1_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 151 \
    name timer2eventEng_setEv_1 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_timer2eventEng_setEv_1 \
    op interface \
    ports { timer2eventEng_setEv_1_dout { I 56 vector } timer2eventEng_setEv_1_empty_n { I 1 bit } timer2eventEng_setEv_1_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 152 \
    name txApp2eventEng_setEv_1 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_txApp2eventEng_setEv_1 \
    op interface \
    ports { txApp2eventEng_setEv_1_dout { I 56 vector } txApp2eventEng_setEv_1_empty_n { I 1 bit } txApp2eventEng_setEv_1_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 153 \
    name ackDelayFifoReadCoun_1 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_ackDelayFifoReadCoun_1 \
    op interface \
    ports { ackDelayFifoReadCoun_1_dout { I 1 vector } ackDelayFifoReadCoun_1_empty_n { I 1 bit } ackDelayFifoReadCoun_1_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 154 \
    name ackDelayFifoWriteCou_1 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_ackDelayFifoWriteCou_1 \
    op interface \
    ports { ackDelayFifoWriteCou_1_dout { I 1 vector } ackDelayFifoWriteCou_1_empty_n { I 1 bit } ackDelayFifoWriteCou_1_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 155 \
    name txEngFifoReadCount_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_txEngFifoReadCount_V \
    op interface \
    ports { txEngFifoReadCount_V_dout { I 1 vector } txEngFifoReadCount_V_empty_n { I 1 bit } txEngFifoReadCount_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_start { I 1 bit } ap_ready { O 1 bit } ap_done { O 1 bit } ap_idle { O 1 bit } ap_continue { I 1 bit } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


