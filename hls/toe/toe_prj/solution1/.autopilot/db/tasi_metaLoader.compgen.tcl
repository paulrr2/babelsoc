# This script segment is generated automatically by AutoPilot

# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# Native AXIS:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::native_axis_add] == "::AESL_LIB_XILADAPTER::native_axis_add"} {
eval "::AESL_LIB_XILADAPTER::native_axis_add { \
    id 370 \
    name appTxDataReqMetaData_V \
    reset_level 1 \
    sync_rst true \
    corename {} \
    metadata {  } \
    op interface \
    ports { appTxDataReqMetaData_V_TVALID { I 1 bit } appTxDataReqMetaData_V_TDATA { I 32 vector } appTxDataReqMetaData_V_TREADY { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'appTxDataReqMetaData_V'"
}
}


# Native AXIS:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::native_axis_add] == "::AESL_LIB_XILADAPTER::native_axis_add"} {
eval "::AESL_LIB_XILADAPTER::native_axis_add { \
    id 371 \
    name appTxDataRsp_V \
    reset_level 1 \
    sync_rst true \
    corename {} \
    metadata {  } \
    op interface \
    ports { appTxDataRsp_V_TREADY { I 1 bit } appTxDataRsp_V_TDATA { O 64 vector } appTxDataRsp_V_TVALID { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'appTxDataRsp_V'"
}
}


# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 372 \
    name txApp2stateTable_req_1 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_txApp2stateTable_req_1 \
    op interface \
    ports { txApp2stateTable_req_1_din { O 16 vector } txApp2stateTable_req_1_full_n { I 1 bit } txApp2stateTable_req_1_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 373 \
    name txApp2txSar_upd_req_s_11 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_txApp2txSar_upd_req_s_11 \
    op interface \
    ports { txApp2txSar_upd_req_s_11_din { O 35 vector } txApp2txSar_upd_req_s_11_full_n { I 1 bit } txApp2txSar_upd_req_s_11_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 374 \
    name txSar2txApp_upd_rsp_s_1 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_txSar2txApp_upd_rsp_s_1 \
    op interface \
    ports { txSar2txApp_upd_rsp_s_1_dout { I 70 vector } txSar2txApp_upd_rsp_s_1_empty_n { I 1 bit } txSar2txApp_upd_rsp_s_1_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 375 \
    name stateTable2txApp_rsp_1 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_stateTable2txApp_rsp_1 \
    op interface \
    ports { stateTable2txApp_rsp_1_dout { I 4 vector } stateTable2txApp_rsp_1_empty_n { I 1 bit } stateTable2txApp_rsp_1_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 376 \
    name tasi_meta2pkgPushCmd_1 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_tasi_meta2pkgPushCmd_1 \
    op interface \
    ports { tasi_meta2pkgPushCmd_1_din { O 72 vector } tasi_meta2pkgPushCmd_1_full_n { I 1 bit } tasi_meta2pkgPushCmd_1_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 377 \
    name txAppStream2event_me_1 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_txAppStream2event_me_1 \
    op interface \
    ports { txAppStream2event_me_1_din { O 56 vector } txAppStream2event_me_1_full_n { I 1 bit } txAppStream2event_me_1_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_start { I 1 bit } ap_ready { O 1 bit } ap_done { O 1 bit } ap_idle { O 1 bit } ap_continue { I 1 bit } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


# RegSlice definition:
set ID 378
set RegSliceName regslice_core
set RegSliceInstName regslice_core_U
set CoreName ap_simcore_regslice_core
if {${::AESL::PGuard_rtl_comp_handler}} {
	::AP::rtl_comp_handler $RegSliceName
}


if {${::AESL::PGuard_autocg_gen} && ${::AESL::PGuard_autocg_ipmgen}} {
if {[info proc ::AESL_LIB_VIRTEX::xil_gen_regSlice] == "::AESL_LIB_VIRTEX::xil_gen_regSlice"} {
eval "::AESL_LIB_VIRTEX::xil_gen_regSlice { \
    name ${RegSliceName} \
}"
} else {
puts "@W \[IMPL-107\] Cannot find ::AESL_LIB_VIRTEX::xil_gen_regSlice, check your platform lib"
}
}


