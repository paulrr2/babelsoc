set moduleName rxAppMemDataRead_64_s
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {rxAppMemDataRead<64>}
set C_modelType { void 0 }
set C_modelArgList {
	{ rxBufferReadData_V_data_V int 64 regular {axi_s 0 volatile  { s_axis_rxread_data Data } }  }
	{ rxBufferReadData_V_keep_V int 8 regular {axi_s 0 volatile  { s_axis_rxread_data Keep } }  }
	{ rxBufferReadData_V_last_V int 1 regular {axi_s 0 volatile  { s_axis_rxread_data Last } }  }
	{ rxDataRsp_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_rx_data_rsp Data } }  }
	{ rxDataRsp_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_rx_data_rsp Keep } }  }
	{ rxDataRsp_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_rx_data_rsp Last } }  }
	{ rxBufferReadCmd_V_V int 1 regular {fifo 0 volatile } {global 0}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "rxBufferReadData_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "READONLY"} , 
 	{ "Name" : "rxBufferReadData_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "rxBufferReadData_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY"} , 
 	{ "Name" : "rxDataRsp_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY"} , 
 	{ "Name" : "rxDataRsp_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY"} , 
 	{ "Name" : "rxDataRsp_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY"} , 
 	{ "Name" : "rxBufferReadCmd_V_V", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 20
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ rxBufferReadCmd_V_V_dout sc_in sc_lv 1 signal 6 } 
	{ rxBufferReadCmd_V_V_empty_n sc_in sc_logic 1 signal 6 } 
	{ rxBufferReadCmd_V_V_read sc_out sc_logic 1 signal 6 } 
	{ s_axis_rxread_data_TVALID sc_in sc_logic 1 invld 0 } 
	{ m_axis_rx_data_rsp_TREADY sc_in sc_logic 1 outacc 5 } 
	{ s_axis_rxread_data_TDATA sc_in sc_lv 64 signal 0 } 
	{ s_axis_rxread_data_TREADY sc_out sc_logic 1 inacc 2 } 
	{ s_axis_rxread_data_TKEEP sc_in sc_lv 8 signal 1 } 
	{ s_axis_rxread_data_TLAST sc_in sc_lv 1 signal 2 } 
	{ m_axis_rx_data_rsp_TDATA sc_out sc_lv 64 signal 3 } 
	{ m_axis_rx_data_rsp_TVALID sc_out sc_logic 1 outvld 5 } 
	{ m_axis_rx_data_rsp_TKEEP sc_out sc_lv 8 signal 4 } 
	{ m_axis_rx_data_rsp_TLAST sc_out sc_lv 1 signal 5 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "rxBufferReadCmd_V_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "rxBufferReadCmd_V_V", "role": "dout" }} , 
 	{ "name": "rxBufferReadCmd_V_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxBufferReadCmd_V_V", "role": "empty_n" }} , 
 	{ "name": "rxBufferReadCmd_V_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxBufferReadCmd_V_V", "role": "read" }} , 
 	{ "name": "s_axis_rxread_data_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "rxBufferReadData_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_rx_data_rsp_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "rxDataRsp_V_last_V", "role": "TREADY" }} , 
 	{ "name": "s_axis_rxread_data_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "rxBufferReadData_V_data_V", "role": "default" }} , 
 	{ "name": "s_axis_rxread_data_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "rxBufferReadData_V_last_V", "role": "default" }} , 
 	{ "name": "s_axis_rxread_data_TKEEP", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "rxBufferReadData_V_keep_V", "role": "default" }} , 
 	{ "name": "s_axis_rxread_data_TLAST", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "rxBufferReadData_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_rx_data_rsp_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "rxDataRsp_V_data_V", "role": "TDATA" }} , 
 	{ "name": "m_axis_rx_data_rsp_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "rxDataRsp_V_last_V", "role": "TVALID" }} , 
 	{ "name": "m_axis_rx_data_rsp_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "rxDataRsp_V_keep_V", "role": "TKEEP" }} , 
 	{ "name": "m_axis_rx_data_rsp_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "rxDataRsp_V_last_V", "role": "TLAST" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2", "3"],
		"CDFG" : "rxAppMemDataRead_64_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "rxBufferReadData_V_data_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "s_axis_rxread_data_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxBufferReadData_V_keep_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "rxBufferReadData_V_last_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "rxDataRsp_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_rx_data_rsp_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxDataRsp_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "rxDataRsp_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "ramdr_fsmState_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "rxBufferReadCmd_V_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxBufferReadCmd_V_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_rxDataRsp_V_data_V_U", "Parent" : "0"},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_rxDataRsp_V_keep_V_U", "Parent" : "0"},
	{"ID" : "3", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_rxDataRsp_V_last_V_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	rxAppMemDataRead_64_s {
		rxBufferReadData_V_data_V {Type I LastRead 0 FirstWrite -1}
		rxBufferReadData_V_keep_V {Type I LastRead 0 FirstWrite -1}
		rxBufferReadData_V_last_V {Type I LastRead 0 FirstWrite -1}
		rxDataRsp_V_data_V {Type O LastRead -1 FirstWrite 1}
		rxDataRsp_V_keep_V {Type O LastRead -1 FirstWrite 1}
		rxDataRsp_V_last_V {Type O LastRead -1 FirstWrite 1}
		ramdr_fsmState_V {Type IO LastRead -1 FirstWrite -1}
		rxBufferReadCmd_V_V {Type I LastRead 0 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "2", "Max" : "2"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	rxBufferReadData_V_data_V { axis {  { s_axis_rxread_data_TVALID in_vld 0 1 }  { s_axis_rxread_data_TDATA in_data 0 64 } } }
	rxBufferReadData_V_keep_V { axis {  { s_axis_rxread_data_TKEEP in_data 0 8 } } }
	rxBufferReadData_V_last_V { axis {  { s_axis_rxread_data_TREADY in_acc 1 1 }  { s_axis_rxread_data_TLAST in_data 0 1 } } }
	rxDataRsp_V_data_V { axis {  { m_axis_rx_data_rsp_TDATA out_data 1 64 } } }
	rxDataRsp_V_keep_V { axis {  { m_axis_rx_data_rsp_TKEEP out_data 1 8 } } }
	rxDataRsp_V_last_V { axis {  { m_axis_rx_data_rsp_TREADY out_acc 0 1 }  { m_axis_rx_data_rsp_TVALID out_vld 1 1 }  { m_axis_rx_data_rsp_TLAST out_data 1 1 } } }
	rxBufferReadCmd_V_V { ap_fifo {  { rxBufferReadCmd_V_V_dout fifo_data 0 1 }  { rxBufferReadCmd_V_V_empty_n fifo_status 0 1 }  { rxBufferReadCmd_V_V_read fifo_update 1 1 } } }
}
