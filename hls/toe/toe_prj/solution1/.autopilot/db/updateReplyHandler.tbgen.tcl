set moduleName updateReplyHandler
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {updateReplyHandler}
set C_modelType { void 0 }
set C_modelArgList {
	{ sessionUpdate_rsp_V int 88 regular {axi_s 0 volatile  { sessionUpdate_rsp_V Data } }  }
	{ slc_sessionInsert_rs_13 int 1 regular {fifo 1 volatile } {global 1}  }
	{ slc_sessionInsert_rs_9 int 32 regular {fifo 1 volatile } {global 1}  }
	{ slc_sessionInsert_rs_8 int 16 regular {fifo 1 volatile } {global 1}  }
	{ slc_sessionInsert_rs_11 int 16 regular {fifo 1 volatile } {global 1}  }
	{ slc_sessionInsert_rs_14 int 16 regular {fifo 1 volatile } {global 1}  }
	{ slc_sessionInsert_rs_7 int 1 regular {fifo 1 volatile } {global 1}  }
	{ slc_sessionInsert_rs_15 int 1 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "sessionUpdate_rsp_V", "interface" : "axis", "bitwidth" : 88, "direction" : "READONLY"} , 
 	{ "Name" : "slc_sessionInsert_rs_13", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "slc_sessionInsert_rs_9", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "slc_sessionInsert_rs_8", "interface" : "fifo", "bitwidth" : 16, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "slc_sessionInsert_rs_11", "interface" : "fifo", "bitwidth" : 16, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "slc_sessionInsert_rs_14", "interface" : "fifo", "bitwidth" : 16, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "slc_sessionInsert_rs_7", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "slc_sessionInsert_rs_15", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 31
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ sessionUpdate_rsp_V_TVALID sc_in sc_logic 1 invld 0 } 
	{ slc_sessionInsert_rs_13_din sc_out sc_lv 1 signal 1 } 
	{ slc_sessionInsert_rs_13_full_n sc_in sc_logic 1 signal 1 } 
	{ slc_sessionInsert_rs_13_write sc_out sc_logic 1 signal 1 } 
	{ slc_sessionInsert_rs_9_din sc_out sc_lv 32 signal 2 } 
	{ slc_sessionInsert_rs_9_full_n sc_in sc_logic 1 signal 2 } 
	{ slc_sessionInsert_rs_9_write sc_out sc_logic 1 signal 2 } 
	{ slc_sessionInsert_rs_8_din sc_out sc_lv 16 signal 3 } 
	{ slc_sessionInsert_rs_8_full_n sc_in sc_logic 1 signal 3 } 
	{ slc_sessionInsert_rs_8_write sc_out sc_logic 1 signal 3 } 
	{ slc_sessionInsert_rs_11_din sc_out sc_lv 16 signal 4 } 
	{ slc_sessionInsert_rs_11_full_n sc_in sc_logic 1 signal 4 } 
	{ slc_sessionInsert_rs_11_write sc_out sc_logic 1 signal 4 } 
	{ slc_sessionInsert_rs_14_din sc_out sc_lv 16 signal 5 } 
	{ slc_sessionInsert_rs_14_full_n sc_in sc_logic 1 signal 5 } 
	{ slc_sessionInsert_rs_14_write sc_out sc_logic 1 signal 5 } 
	{ slc_sessionInsert_rs_7_din sc_out sc_lv 1 signal 6 } 
	{ slc_sessionInsert_rs_7_full_n sc_in sc_logic 1 signal 6 } 
	{ slc_sessionInsert_rs_7_write sc_out sc_logic 1 signal 6 } 
	{ slc_sessionInsert_rs_15_din sc_out sc_lv 1 signal 7 } 
	{ slc_sessionInsert_rs_15_full_n sc_in sc_logic 1 signal 7 } 
	{ slc_sessionInsert_rs_15_write sc_out sc_logic 1 signal 7 } 
	{ sessionUpdate_rsp_V_TDATA sc_in sc_lv 88 signal 0 } 
	{ sessionUpdate_rsp_V_TREADY sc_out sc_logic 1 inacc 0 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "sessionUpdate_rsp_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "sessionUpdate_rsp_V", "role": "TVALID" }} , 
 	{ "name": "slc_sessionInsert_rs_13_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_13", "role": "din" }} , 
 	{ "name": "slc_sessionInsert_rs_13_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_13", "role": "full_n" }} , 
 	{ "name": "slc_sessionInsert_rs_13_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_13", "role": "write" }} , 
 	{ "name": "slc_sessionInsert_rs_9_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_9", "role": "din" }} , 
 	{ "name": "slc_sessionInsert_rs_9_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_9", "role": "full_n" }} , 
 	{ "name": "slc_sessionInsert_rs_9_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_9", "role": "write" }} , 
 	{ "name": "slc_sessionInsert_rs_8_din", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_8", "role": "din" }} , 
 	{ "name": "slc_sessionInsert_rs_8_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_8", "role": "full_n" }} , 
 	{ "name": "slc_sessionInsert_rs_8_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_8", "role": "write" }} , 
 	{ "name": "slc_sessionInsert_rs_11_din", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_11", "role": "din" }} , 
 	{ "name": "slc_sessionInsert_rs_11_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_11", "role": "full_n" }} , 
 	{ "name": "slc_sessionInsert_rs_11_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_11", "role": "write" }} , 
 	{ "name": "slc_sessionInsert_rs_14_din", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_14", "role": "din" }} , 
 	{ "name": "slc_sessionInsert_rs_14_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_14", "role": "full_n" }} , 
 	{ "name": "slc_sessionInsert_rs_14_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_14", "role": "write" }} , 
 	{ "name": "slc_sessionInsert_rs_7_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_7", "role": "din" }} , 
 	{ "name": "slc_sessionInsert_rs_7_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_7", "role": "full_n" }} , 
 	{ "name": "slc_sessionInsert_rs_7_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_7", "role": "write" }} , 
 	{ "name": "slc_sessionInsert_rs_15_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_15", "role": "din" }} , 
 	{ "name": "slc_sessionInsert_rs_15_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_15", "role": "full_n" }} , 
 	{ "name": "slc_sessionInsert_rs_15_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_15", "role": "write" }} , 
 	{ "name": "sessionUpdate_rsp_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":88, "type": "signal", "bundle":{"name": "sessionUpdate_rsp_V", "role": "TDATA" }} , 
 	{ "name": "sessionUpdate_rsp_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "sessionUpdate_rsp_V", "role": "TREADY" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "updateReplyHandler",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "sessionUpdate_rsp_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "sessionUpdate_rsp_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "slc_sessionInsert_rs_13", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "slc_sessionInsert_rs_13_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "slc_sessionInsert_rs_9", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "slc_sessionInsert_rs_9_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "slc_sessionInsert_rs_8", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "slc_sessionInsert_rs_8_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "slc_sessionInsert_rs_11", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "slc_sessionInsert_rs_11_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "slc_sessionInsert_rs_14", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "slc_sessionInsert_rs_14_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "slc_sessionInsert_rs_7", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "slc_sessionInsert_rs_7_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "slc_sessionInsert_rs_15", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "slc_sessionInsert_rs_15_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	updateReplyHandler {
		sessionUpdate_rsp_V {Type I LastRead 0 FirstWrite -1}
		slc_sessionInsert_rs_13 {Type O LastRead -1 FirstWrite 1}
		slc_sessionInsert_rs_9 {Type O LastRead -1 FirstWrite 1}
		slc_sessionInsert_rs_8 {Type O LastRead -1 FirstWrite 1}
		slc_sessionInsert_rs_11 {Type O LastRead -1 FirstWrite 1}
		slc_sessionInsert_rs_14 {Type O LastRead -1 FirstWrite 1}
		slc_sessionInsert_rs_7 {Type O LastRead -1 FirstWrite 1}
		slc_sessionInsert_rs_15 {Type O LastRead -1 FirstWrite 1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "1", "Max" : "1"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	sessionUpdate_rsp_V { axis {  { sessionUpdate_rsp_V_TVALID in_vld 0 1 }  { sessionUpdate_rsp_V_TDATA in_data 0 88 }  { sessionUpdate_rsp_V_TREADY in_acc 1 1 } } }
	slc_sessionInsert_rs_13 { ap_fifo {  { slc_sessionInsert_rs_13_din fifo_data 1 1 }  { slc_sessionInsert_rs_13_full_n fifo_status 0 1 }  { slc_sessionInsert_rs_13_write fifo_update 1 1 } } }
	slc_sessionInsert_rs_9 { ap_fifo {  { slc_sessionInsert_rs_9_din fifo_data 1 32 }  { slc_sessionInsert_rs_9_full_n fifo_status 0 1 }  { slc_sessionInsert_rs_9_write fifo_update 1 1 } } }
	slc_sessionInsert_rs_8 { ap_fifo {  { slc_sessionInsert_rs_8_din fifo_data 1 16 }  { slc_sessionInsert_rs_8_full_n fifo_status 0 1 }  { slc_sessionInsert_rs_8_write fifo_update 1 1 } } }
	slc_sessionInsert_rs_11 { ap_fifo {  { slc_sessionInsert_rs_11_din fifo_data 1 16 }  { slc_sessionInsert_rs_11_full_n fifo_status 0 1 }  { slc_sessionInsert_rs_11_write fifo_update 1 1 } } }
	slc_sessionInsert_rs_14 { ap_fifo {  { slc_sessionInsert_rs_14_din fifo_data 1 16 }  { slc_sessionInsert_rs_14_full_n fifo_status 0 1 }  { slc_sessionInsert_rs_14_write fifo_update 1 1 } } }
	slc_sessionInsert_rs_7 { ap_fifo {  { slc_sessionInsert_rs_7_din fifo_data 1 1 }  { slc_sessionInsert_rs_7_full_n fifo_status 0 1 }  { slc_sessionInsert_rs_7_write fifo_update 1 1 } } }
	slc_sessionInsert_rs_15 { ap_fifo {  { slc_sessionInsert_rs_15_din fifo_data 1 1 }  { slc_sessionInsert_rs_15_full_n fifo_status 0 1 }  { slc_sessionInsert_rs_15_write fifo_update 1 1 } } }
}
