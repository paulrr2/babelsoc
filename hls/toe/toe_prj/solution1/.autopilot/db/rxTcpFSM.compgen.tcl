# This script segment is generated automatically by AutoPilot

# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 252 \
    name rxbuffer_data_count_V \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_rxbuffer_data_count_V \
    op interface \
    ports { rxbuffer_data_count_V { I 16 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 253 \
    name rxbuffer_max_data_count_V \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_rxbuffer_max_data_count_V \
    op interface \
    ports { rxbuffer_max_data_count_V { I 16 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 254 \
    name rxEng_fsmMetaDataFif_1 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_fsmMetaDataFif_1 \
    op interface \
    ports { rxEng_fsmMetaDataFif_1_dout { I 172 vector } rxEng_fsmMetaDataFif_1_empty_n { I 1 bit } rxEng_fsmMetaDataFif_1_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 255 \
    name rxEng2stateTable_upd_1 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng2stateTable_upd_1 \
    op interface \
    ports { rxEng2stateTable_upd_1_din { O 21 vector } rxEng2stateTable_upd_1_full_n { I 1 bit } rxEng2stateTable_upd_1_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 256 \
    name rxEng2rxSar_upd_req_s_18 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng2rxSar_upd_req_s_18 \
    op interface \
    ports { rxEng2rxSar_upd_req_s_18_din { O 119 vector } rxEng2rxSar_upd_req_s_18_full_n { I 1 bit } rxEng2rxSar_upd_req_s_18_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 257 \
    name rxEng2txSar_upd_req_s_17 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng2txSar_upd_req_s_17 \
    op interface \
    ports { rxEng2txSar_upd_req_s_17_din { O 91 vector } rxEng2txSar_upd_req_s_17_full_n { I 1 bit } rxEng2txSar_upd_req_s_17_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 258 \
    name stateTable2rxEng_upd_1 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_stateTable2rxEng_upd_1 \
    op interface \
    ports { stateTable2rxEng_upd_1_dout { I 4 vector } stateTable2rxEng_upd_1_empty_n { I 1 bit } stateTable2rxEng_upd_1_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 259 \
    name rxSar2rxEng_upd_rsp_s_15 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_rxSar2rxEng_upd_rsp_s_15 \
    op interface \
    ports { rxSar2rxEng_upd_rsp_s_15_dout { I 119 vector } rxSar2rxEng_upd_rsp_s_15_empty_n { I 1 bit } rxSar2rxEng_upd_rsp_s_15_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 260 \
    name txSar2rxEng_upd_rsp_s_2 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_txSar2rxEng_upd_rsp_s_2 \
    op interface \
    ports { txSar2rxEng_upd_rsp_s_2_dout { I 103 vector } txSar2rxEng_upd_rsp_s_2_empty_n { I 1 bit } txSar2rxEng_upd_rsp_s_2_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 261 \
    name rxEng2timer_clearRet_1 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng2timer_clearRet_1 \
    op interface \
    ports { rxEng2timer_clearRet_1_din { O 17 vector } rxEng2timer_clearRet_1_full_n { I 1 bit } rxEng2timer_clearRet_1_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 262 \
    name rxEng2timer_clearPro_1 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng2timer_clearPro_1 \
    op interface \
    ports { rxEng2timer_clearPro_1_din { O 16 vector } rxEng2timer_clearPro_1_full_n { I 1 bit } rxEng2timer_clearPro_1_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 263 \
    name rxEng2rxApp_notifica_1 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng2rxApp_notifica_1 \
    op interface \
    ports { rxEng2rxApp_notifica_1_din { O 81 vector } rxEng2rxApp_notifica_1_full_n { I 1 bit } rxEng2rxApp_notifica_1_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 264 \
    name rxEng_fsmDropFifo_V \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_fsmDropFifo_V \
    op interface \
    ports { rxEng_fsmDropFifo_V_din { O 1 vector } rxEng_fsmDropFifo_V_full_n { I 1 bit } rxEng_fsmDropFifo_V_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 265 \
    name rxEng_fsmEventFifo_V \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_fsmEventFifo_V \
    op interface \
    ports { rxEng_fsmEventFifo_V_din { O 56 vector } rxEng_fsmEventFifo_V_full_n { I 1 bit } rxEng_fsmEventFifo_V_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 266 \
    name rxEng2timer_setClose_1 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng2timer_setClose_1 \
    op interface \
    ports { rxEng2timer_setClose_1_din { O 16 vector } rxEng2timer_setClose_1_full_n { I 1 bit } rxEng2timer_setClose_1_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 267 \
    name conEstablishedFifo_V \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_conEstablishedFifo_V \
    op interface \
    ports { conEstablishedFifo_V_din { O 17 vector } conEstablishedFifo_V_full_n { I 1 bit } conEstablishedFifo_V_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_start { I 1 bit } ap_ready { O 1 bit } ap_done { O 1 bit } ap_idle { O 1 bit } ap_continue { I 1 bit } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


