# This script segment is generated automatically by AutoPilot

# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# Native AXIS:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::native_axis_add] == "::AESL_LIB_XILADAPTER::native_axis_add"} {
eval "::AESL_LIB_XILADAPTER::native_axis_add { \
    id 36 \
    name sessionUpdate_req_V \
    reset_level 1 \
    sync_rst true \
    corename {} \
    metadata {  } \
    op interface \
    ports { sessionUpdate_req_V_TREADY { I 1 bit } sessionUpdate_req_V_TDATA { O 88 vector } sessionUpdate_req_V_TVALID { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'sessionUpdate_req_V'"
}
}


# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 37 \
    name regSessionCount_V \
    type other \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_regSessionCount_V \
    op interface \
    ports { regSessionCount_V { O 16 vector } regSessionCount_V_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 38 \
    name sessionInsert_req_V_4 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_sessionInsert_req_V_4 \
    op interface \
    ports { sessionInsert_req_V_4_dout { I 1 vector } sessionInsert_req_V_4_empty_n { I 1 bit } sessionInsert_req_V_4_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 39 \
    name sessionInsert_req_V_1 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_sessionInsert_req_V_1 \
    op interface \
    ports { sessionInsert_req_V_1_dout { I 32 vector } sessionInsert_req_V_1_empty_n { I 1 bit } sessionInsert_req_V_1_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 40 \
    name sessionInsert_req_V_6 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_sessionInsert_req_V_6 \
    op interface \
    ports { sessionInsert_req_V_6_dout { I 16 vector } sessionInsert_req_V_6_empty_n { I 1 bit } sessionInsert_req_V_6_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 41 \
    name sessionInsert_req_V_3 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_sessionInsert_req_V_3 \
    op interface \
    ports { sessionInsert_req_V_3_dout { I 16 vector } sessionInsert_req_V_3_empty_n { I 1 bit } sessionInsert_req_V_3_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 42 \
    name sessionInsert_req_V_s \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_sessionInsert_req_V_s \
    op interface \
    ports { sessionInsert_req_V_s_dout { I 16 vector } sessionInsert_req_V_s_empty_n { I 1 bit } sessionInsert_req_V_s_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 43 \
    name sessionInsert_req_V_5 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_sessionInsert_req_V_5 \
    op interface \
    ports { sessionInsert_req_V_5_dout { I 1 vector } sessionInsert_req_V_5_empty_n { I 1 bit } sessionInsert_req_V_5_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 44 \
    name sessionDelete_req_V_4 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_sessionDelete_req_V_4 \
    op interface \
    ports { sessionDelete_req_V_4_dout { I 1 vector } sessionDelete_req_V_4_empty_n { I 1 bit } sessionDelete_req_V_4_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 45 \
    name sessionDelete_req_V_1 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_sessionDelete_req_V_1 \
    op interface \
    ports { sessionDelete_req_V_1_dout { I 32 vector } sessionDelete_req_V_1_empty_n { I 1 bit } sessionDelete_req_V_1_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 46 \
    name sessionDelete_req_V_6 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_sessionDelete_req_V_6 \
    op interface \
    ports { sessionDelete_req_V_6_dout { I 16 vector } sessionDelete_req_V_6_empty_n { I 1 bit } sessionDelete_req_V_6_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 47 \
    name sessionDelete_req_V_3 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_sessionDelete_req_V_3 \
    op interface \
    ports { sessionDelete_req_V_3_dout { I 16 vector } sessionDelete_req_V_3_empty_n { I 1 bit } sessionDelete_req_V_3_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 48 \
    name sessionDelete_req_V_s \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_sessionDelete_req_V_s \
    op interface \
    ports { sessionDelete_req_V_s_dout { I 16 vector } sessionDelete_req_V_s_empty_n { I 1 bit } sessionDelete_req_V_s_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 49 \
    name sessionDelete_req_V_5 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_sessionDelete_req_V_5 \
    op interface \
    ports { sessionDelete_req_V_5_dout { I 1 vector } sessionDelete_req_V_5_empty_n { I 1 bit } sessionDelete_req_V_5_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 50 \
    name slc_sessionIdFinFifo_1 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_slc_sessionIdFinFifo_1 \
    op interface \
    ports { slc_sessionIdFinFifo_1_din { O 14 vector } slc_sessionIdFinFifo_1_full_n { I 1 bit } slc_sessionIdFinFifo_1_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_start { I 1 bit } ap_ready { O 1 bit } ap_done { O 1 bit } ap_idle { O 1 bit } ap_continue { I 1 bit } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


# RegSlice definition:
set ID 51
set RegSliceName regslice_core
set RegSliceInstName regslice_core_U
set CoreName ap_simcore_regslice_core
if {${::AESL::PGuard_rtl_comp_handler}} {
	::AP::rtl_comp_handler $RegSliceName
}


if {${::AESL::PGuard_autocg_gen} && ${::AESL::PGuard_autocg_ipmgen}} {
if {[info proc ::AESL_LIB_VIRTEX::xil_gen_regSlice] == "::AESL_LIB_VIRTEX::xil_gen_regSlice"} {
eval "::AESL_LIB_VIRTEX::xil_gen_regSlice { \
    name ${RegSliceName} \
}"
} else {
puts "@W \[IMPL-107\] Cannot find ::AESL_LIB_VIRTEX::xil_gen_regSlice, check your platform lib"
}
}


