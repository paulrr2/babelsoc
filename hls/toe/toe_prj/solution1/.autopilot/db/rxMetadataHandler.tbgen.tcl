set moduleName rxMetadataHandler
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {rxMetadataHandler}
set C_modelType { void 0 }
set C_modelArgList {
	{ rxEng_metaDataFifo_V int 108 regular {fifo 0 volatile } {global 0}  }
	{ portTable2rxEng_chec_1 int 1 regular {fifo 0 volatile } {global 0}  }
	{ rxEng_tupleBuffer_V int 96 regular {fifo 0 volatile } {global 0}  }
	{ rxEng_metaHandlerEve_1 int 152 regular {fifo 1 volatile } {global 1}  }
	{ rxEng_metaHandlerDro_1 int 1 regular {fifo 1 volatile } {global 1}  }
	{ rxEng2sLookup_req_V int 97 regular {fifo 1 volatile } {global 1}  }
	{ sLookup2rxEng_rsp_V int 17 regular {fifo 0 volatile } {global 0}  }
	{ rxEng_fsmMetaDataFif_1 int 172 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "rxEng_metaDataFifo_V", "interface" : "fifo", "bitwidth" : 108, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "portTable2rxEng_chec_1", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_tupleBuffer_V", "interface" : "fifo", "bitwidth" : 96, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_metaHandlerEve_1", "interface" : "fifo", "bitwidth" : 152, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_metaHandlerDro_1", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rxEng2sLookup_req_V", "interface" : "fifo", "bitwidth" : 97, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "sLookup2rxEng_rsp_V", "interface" : "fifo", "bitwidth" : 17, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_fsmMetaDataFif_1", "interface" : "fifo", "bitwidth" : 172, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 31
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ rxEng_metaDataFifo_V_dout sc_in sc_lv 108 signal 0 } 
	{ rxEng_metaDataFifo_V_empty_n sc_in sc_logic 1 signal 0 } 
	{ rxEng_metaDataFifo_V_read sc_out sc_logic 1 signal 0 } 
	{ portTable2rxEng_chec_1_dout sc_in sc_lv 1 signal 1 } 
	{ portTable2rxEng_chec_1_empty_n sc_in sc_logic 1 signal 1 } 
	{ portTable2rxEng_chec_1_read sc_out sc_logic 1 signal 1 } 
	{ rxEng_tupleBuffer_V_dout sc_in sc_lv 96 signal 2 } 
	{ rxEng_tupleBuffer_V_empty_n sc_in sc_logic 1 signal 2 } 
	{ rxEng_tupleBuffer_V_read sc_out sc_logic 1 signal 2 } 
	{ sLookup2rxEng_rsp_V_dout sc_in sc_lv 17 signal 6 } 
	{ sLookup2rxEng_rsp_V_empty_n sc_in sc_logic 1 signal 6 } 
	{ sLookup2rxEng_rsp_V_read sc_out sc_logic 1 signal 6 } 
	{ rxEng_metaHandlerEve_1_din sc_out sc_lv 152 signal 3 } 
	{ rxEng_metaHandlerEve_1_full_n sc_in sc_logic 1 signal 3 } 
	{ rxEng_metaHandlerEve_1_write sc_out sc_logic 1 signal 3 } 
	{ rxEng_metaHandlerDro_1_din sc_out sc_lv 1 signal 4 } 
	{ rxEng_metaHandlerDro_1_full_n sc_in sc_logic 1 signal 4 } 
	{ rxEng_metaHandlerDro_1_write sc_out sc_logic 1 signal 4 } 
	{ rxEng2sLookup_req_V_din sc_out sc_lv 97 signal 5 } 
	{ rxEng2sLookup_req_V_full_n sc_in sc_logic 1 signal 5 } 
	{ rxEng2sLookup_req_V_write sc_out sc_logic 1 signal 5 } 
	{ rxEng_fsmMetaDataFif_1_din sc_out sc_lv 172 signal 7 } 
	{ rxEng_fsmMetaDataFif_1_full_n sc_in sc_logic 1 signal 7 } 
	{ rxEng_fsmMetaDataFif_1_write sc_out sc_logic 1 signal 7 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "rxEng_metaDataFifo_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":108, "type": "signal", "bundle":{"name": "rxEng_metaDataFifo_V", "role": "dout" }} , 
 	{ "name": "rxEng_metaDataFifo_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_metaDataFifo_V", "role": "empty_n" }} , 
 	{ "name": "rxEng_metaDataFifo_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_metaDataFifo_V", "role": "read" }} , 
 	{ "name": "portTable2rxEng_chec_1_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "portTable2rxEng_chec_1", "role": "dout" }} , 
 	{ "name": "portTable2rxEng_chec_1_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "portTable2rxEng_chec_1", "role": "empty_n" }} , 
 	{ "name": "portTable2rxEng_chec_1_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "portTable2rxEng_chec_1", "role": "read" }} , 
 	{ "name": "rxEng_tupleBuffer_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":96, "type": "signal", "bundle":{"name": "rxEng_tupleBuffer_V", "role": "dout" }} , 
 	{ "name": "rxEng_tupleBuffer_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_tupleBuffer_V", "role": "empty_n" }} , 
 	{ "name": "rxEng_tupleBuffer_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_tupleBuffer_V", "role": "read" }} , 
 	{ "name": "sLookup2rxEng_rsp_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":17, "type": "signal", "bundle":{"name": "sLookup2rxEng_rsp_V", "role": "dout" }} , 
 	{ "name": "sLookup2rxEng_rsp_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sLookup2rxEng_rsp_V", "role": "empty_n" }} , 
 	{ "name": "sLookup2rxEng_rsp_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sLookup2rxEng_rsp_V", "role": "read" }} , 
 	{ "name": "rxEng_metaHandlerEve_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":152, "type": "signal", "bundle":{"name": "rxEng_metaHandlerEve_1", "role": "din" }} , 
 	{ "name": "rxEng_metaHandlerEve_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_metaHandlerEve_1", "role": "full_n" }} , 
 	{ "name": "rxEng_metaHandlerEve_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_metaHandlerEve_1", "role": "write" }} , 
 	{ "name": "rxEng_metaHandlerDro_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_metaHandlerDro_1", "role": "din" }} , 
 	{ "name": "rxEng_metaHandlerDro_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_metaHandlerDro_1", "role": "full_n" }} , 
 	{ "name": "rxEng_metaHandlerDro_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_metaHandlerDro_1", "role": "write" }} , 
 	{ "name": "rxEng2sLookup_req_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":97, "type": "signal", "bundle":{"name": "rxEng2sLookup_req_V", "role": "din" }} , 
 	{ "name": "rxEng2sLookup_req_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng2sLookup_req_V", "role": "full_n" }} , 
 	{ "name": "rxEng2sLookup_req_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng2sLookup_req_V", "role": "write" }} , 
 	{ "name": "rxEng_fsmMetaDataFif_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":172, "type": "signal", "bundle":{"name": "rxEng_fsmMetaDataFif_1", "role": "din" }} , 
 	{ "name": "rxEng_fsmMetaDataFif_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_fsmMetaDataFif_1", "role": "full_n" }} , 
 	{ "name": "rxEng_fsmMetaDataFif_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_fsmMetaDataFif_1", "role": "write" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "rxMetadataHandler",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "mh_state", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "mh_meta_length_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "rxEng_metaDataFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_metaDataFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "portTable2rxEng_chec_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "portTable2rxEng_chec_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng_tupleBuffer_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_tupleBuffer_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "mh_meta_seqNumb_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "mh_meta_ackNumb_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "mh_meta_winSize_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "mh_meta_winScale_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "mh_meta_ack_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "mh_meta_rst_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "mh_meta_syn_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "mh_meta_fin_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "mh_meta_dataOffset_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "mh_srcIpAddress_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "mh_dstIpPort_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "rxEng_metaHandlerEve_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_metaHandlerEve_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng_metaHandlerDro_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_metaHandlerDro_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng2sLookup_req_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng2sLookup_req_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "sLookup2rxEng_rsp_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "sLookup2rxEng_rsp_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng_fsmMetaDataFif_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_fsmMetaDataFif_1_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	rxMetadataHandler {
		mh_state {Type IO LastRead -1 FirstWrite -1}
		mh_meta_length_V {Type IO LastRead -1 FirstWrite -1}
		rxEng_metaDataFifo_V {Type I LastRead 0 FirstWrite -1}
		portTable2rxEng_chec_1 {Type I LastRead 0 FirstWrite -1}
		rxEng_tupleBuffer_V {Type I LastRead 0 FirstWrite -1}
		mh_meta_seqNumb_V {Type IO LastRead -1 FirstWrite -1}
		mh_meta_ackNumb_V {Type IO LastRead -1 FirstWrite -1}
		mh_meta_winSize_V {Type IO LastRead -1 FirstWrite -1}
		mh_meta_winScale_V {Type IO LastRead -1 FirstWrite -1}
		mh_meta_ack_V {Type IO LastRead -1 FirstWrite -1}
		mh_meta_rst_V {Type IO LastRead -1 FirstWrite -1}
		mh_meta_syn_V {Type IO LastRead -1 FirstWrite -1}
		mh_meta_fin_V {Type IO LastRead -1 FirstWrite -1}
		mh_meta_dataOffset_V {Type IO LastRead -1 FirstWrite -1}
		mh_srcIpAddress_V {Type IO LastRead -1 FirstWrite -1}
		mh_dstIpPort_V {Type IO LastRead -1 FirstWrite -1}
		rxEng_metaHandlerEve_1 {Type O LastRead -1 FirstWrite 1}
		rxEng_metaHandlerDro_1 {Type O LastRead -1 FirstWrite 1}
		rxEng2sLookup_req_V {Type O LastRead -1 FirstWrite 1}
		sLookup2rxEng_rsp_V {Type I LastRead 0 FirstWrite -1}
		rxEng_fsmMetaDataFif_1 {Type O LastRead -1 FirstWrite 1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "1", "Max" : "1"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	rxEng_metaDataFifo_V { ap_fifo {  { rxEng_metaDataFifo_V_dout fifo_data 0 108 }  { rxEng_metaDataFifo_V_empty_n fifo_status 0 1 }  { rxEng_metaDataFifo_V_read fifo_update 1 1 } } }
	portTable2rxEng_chec_1 { ap_fifo {  { portTable2rxEng_chec_1_dout fifo_data 0 1 }  { portTable2rxEng_chec_1_empty_n fifo_status 0 1 }  { portTable2rxEng_chec_1_read fifo_update 1 1 } } }
	rxEng_tupleBuffer_V { ap_fifo {  { rxEng_tupleBuffer_V_dout fifo_data 0 96 }  { rxEng_tupleBuffer_V_empty_n fifo_status 0 1 }  { rxEng_tupleBuffer_V_read fifo_update 1 1 } } }
	rxEng_metaHandlerEve_1 { ap_fifo {  { rxEng_metaHandlerEve_1_din fifo_data 1 152 }  { rxEng_metaHandlerEve_1_full_n fifo_status 0 1 }  { rxEng_metaHandlerEve_1_write fifo_update 1 1 } } }
	rxEng_metaHandlerDro_1 { ap_fifo {  { rxEng_metaHandlerDro_1_din fifo_data 1 1 }  { rxEng_metaHandlerDro_1_full_n fifo_status 0 1 }  { rxEng_metaHandlerDro_1_write fifo_update 1 1 } } }
	rxEng2sLookup_req_V { ap_fifo {  { rxEng2sLookup_req_V_din fifo_data 1 97 }  { rxEng2sLookup_req_V_full_n fifo_status 0 1 }  { rxEng2sLookup_req_V_write fifo_update 1 1 } } }
	sLookup2rxEng_rsp_V { ap_fifo {  { sLookup2rxEng_rsp_V_dout fifo_data 0 17 }  { sLookup2rxEng_rsp_V_empty_n fifo_status 0 1 }  { sLookup2rxEng_rsp_V_read fifo_update 1 1 } } }
	rxEng_fsmMetaDataFif_1 { ap_fifo {  { rxEng_fsmMetaDataFif_1_din fifo_data 1 172 }  { rxEng_fsmMetaDataFif_1_full_n fifo_status 0 1 }  { rxEng_fsmMetaDataFif_1_write fifo_update 1 1 } } }
}
