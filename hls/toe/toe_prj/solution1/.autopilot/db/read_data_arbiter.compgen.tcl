# This script segment is generated automatically by AutoPilot

# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 307 \
    name txEng_isDDRbypass_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_txEng_isDDRbypass_V \
    op interface \
    ports { txEng_isDDRbypass_V_dout { I 1 vector } txEng_isDDRbypass_V_empty_n { I 1 bit } txEng_isDDRbypass_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 308 \
    name txBufferReadDataStit_1 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_txBufferReadDataStit_1 \
    op interface \
    ports { txBufferReadDataStit_1_dout { I 73 vector } txBufferReadDataStit_1_empty_n { I 1 bit } txBufferReadDataStit_1_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 309 \
    name txEng_tcpPkgBuffer0_s_9 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_txEng_tcpPkgBuffer0_s_9 \
    op interface \
    ports { txEng_tcpPkgBuffer0_s_9_din { O 73 vector } txEng_tcpPkgBuffer0_s_9_full_n { I 1 bit } txEng_tcpPkgBuffer0_s_9_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 310 \
    name txApp2txEng_data_str_3 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_txApp2txEng_data_str_3 \
    op interface \
    ports { txApp2txEng_data_str_3_dout { I 64 vector } txApp2txEng_data_str_3_empty_n { I 1 bit } txApp2txEng_data_str_3_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 311 \
    name txApp2txEng_data_str_5 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_txApp2txEng_data_str_5 \
    op interface \
    ports { txApp2txEng_data_str_5_dout { I 8 vector } txApp2txEng_data_str_5_empty_n { I 1 bit } txApp2txEng_data_str_5_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 312 \
    name txApp2txEng_data_str_6 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_txApp2txEng_data_str_6 \
    op interface \
    ports { txApp2txEng_data_str_6_dout { I 1 vector } txApp2txEng_data_str_6_empty_n { I 1 bit } txApp2txEng_data_str_6_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_start { I 1 bit } ap_ready { O 1 bit } ap_done { O 1 bit } ap_idle { O 1 bit } ap_continue { I 1 bit } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


