set moduleName tupleSplitter
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {tupleSplitter}
set C_modelType { void 0 }
set C_modelArgList {
	{ txEng_isLookUpFifo_V int 1 regular {fifo 0 volatile } {global 0}  }
	{ sLookup2txEng_rev_rs_1 int 96 regular {fifo 0 volatile } {global 0}  }
	{ txEng_ipTupleFifo_V int 64 regular {fifo 1 volatile } {global 1}  }
	{ txEng_tcpTupleFifo_V int 96 regular {fifo 1 volatile } {global 1}  }
	{ txEng_tupleShortCutF_1 int 96 regular {fifo 0 volatile } {global 0}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "txEng_isLookUpFifo_V", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "sLookup2txEng_rev_rs_1", "interface" : "fifo", "bitwidth" : 96, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "txEng_ipTupleFifo_V", "interface" : "fifo", "bitwidth" : 64, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "txEng_tcpTupleFifo_V", "interface" : "fifo", "bitwidth" : 96, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "txEng_tupleShortCutF_1", "interface" : "fifo", "bitwidth" : 96, "direction" : "READONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 22
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ txEng_tupleShortCutF_1_dout sc_in sc_lv 96 signal 4 } 
	{ txEng_tupleShortCutF_1_empty_n sc_in sc_logic 1 signal 4 } 
	{ txEng_tupleShortCutF_1_read sc_out sc_logic 1 signal 4 } 
	{ sLookup2txEng_rev_rs_1_dout sc_in sc_lv 96 signal 1 } 
	{ sLookup2txEng_rev_rs_1_empty_n sc_in sc_logic 1 signal 1 } 
	{ sLookup2txEng_rev_rs_1_read sc_out sc_logic 1 signal 1 } 
	{ txEng_isLookUpFifo_V_dout sc_in sc_lv 1 signal 0 } 
	{ txEng_isLookUpFifo_V_empty_n sc_in sc_logic 1 signal 0 } 
	{ txEng_isLookUpFifo_V_read sc_out sc_logic 1 signal 0 } 
	{ txEng_ipTupleFifo_V_din sc_out sc_lv 64 signal 2 } 
	{ txEng_ipTupleFifo_V_full_n sc_in sc_logic 1 signal 2 } 
	{ txEng_ipTupleFifo_V_write sc_out sc_logic 1 signal 2 } 
	{ txEng_tcpTupleFifo_V_din sc_out sc_lv 96 signal 3 } 
	{ txEng_tcpTupleFifo_V_full_n sc_in sc_logic 1 signal 3 } 
	{ txEng_tcpTupleFifo_V_write sc_out sc_logic 1 signal 3 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "txEng_tupleShortCutF_1_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":96, "type": "signal", "bundle":{"name": "txEng_tupleShortCutF_1", "role": "dout" }} , 
 	{ "name": "txEng_tupleShortCutF_1_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng_tupleShortCutF_1", "role": "empty_n" }} , 
 	{ "name": "txEng_tupleShortCutF_1_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng_tupleShortCutF_1", "role": "read" }} , 
 	{ "name": "sLookup2txEng_rev_rs_1_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":96, "type": "signal", "bundle":{"name": "sLookup2txEng_rev_rs_1", "role": "dout" }} , 
 	{ "name": "sLookup2txEng_rev_rs_1_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sLookup2txEng_rev_rs_1", "role": "empty_n" }} , 
 	{ "name": "sLookup2txEng_rev_rs_1_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sLookup2txEng_rev_rs_1", "role": "read" }} , 
 	{ "name": "txEng_isLookUpFifo_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng_isLookUpFifo_V", "role": "dout" }} , 
 	{ "name": "txEng_isLookUpFifo_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng_isLookUpFifo_V", "role": "empty_n" }} , 
 	{ "name": "txEng_isLookUpFifo_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng_isLookUpFifo_V", "role": "read" }} , 
 	{ "name": "txEng_ipTupleFifo_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "txEng_ipTupleFifo_V", "role": "din" }} , 
 	{ "name": "txEng_ipTupleFifo_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng_ipTupleFifo_V", "role": "full_n" }} , 
 	{ "name": "txEng_ipTupleFifo_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng_ipTupleFifo_V", "role": "write" }} , 
 	{ "name": "txEng_tcpTupleFifo_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":96, "type": "signal", "bundle":{"name": "txEng_tcpTupleFifo_V", "role": "din" }} , 
 	{ "name": "txEng_tcpTupleFifo_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng_tcpTupleFifo_V", "role": "full_n" }} , 
 	{ "name": "txEng_tcpTupleFifo_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng_tcpTupleFifo_V", "role": "write" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "tupleSplitter",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "ts_getMeta", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ts_isLookUp", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "txEng_isLookUpFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txEng_isLookUpFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "sLookup2txEng_rev_rs_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "sLookup2txEng_rev_rs_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txEng_ipTupleFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txEng_ipTupleFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txEng_tcpTupleFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txEng_tcpTupleFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txEng_tupleShortCutF_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txEng_tupleShortCutF_1_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	tupleSplitter {
		ts_getMeta {Type IO LastRead -1 FirstWrite -1}
		ts_isLookUp {Type IO LastRead -1 FirstWrite -1}
		txEng_isLookUpFifo_V {Type I LastRead 0 FirstWrite -1}
		sLookup2txEng_rev_rs_1 {Type I LastRead 0 FirstWrite -1}
		txEng_ipTupleFifo_V {Type O LastRead -1 FirstWrite 1}
		txEng_tcpTupleFifo_V {Type O LastRead -1 FirstWrite 1}
		txEng_tupleShortCutF_1 {Type I LastRead 0 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "1", "Max" : "1"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	txEng_isLookUpFifo_V { ap_fifo {  { txEng_isLookUpFifo_V_dout fifo_data 0 1 }  { txEng_isLookUpFifo_V_empty_n fifo_status 0 1 }  { txEng_isLookUpFifo_V_read fifo_update 1 1 } } }
	sLookup2txEng_rev_rs_1 { ap_fifo {  { sLookup2txEng_rev_rs_1_dout fifo_data 0 96 }  { sLookup2txEng_rev_rs_1_empty_n fifo_status 0 1 }  { sLookup2txEng_rev_rs_1_read fifo_update 1 1 } } }
	txEng_ipTupleFifo_V { ap_fifo {  { txEng_ipTupleFifo_V_din fifo_data 1 64 }  { txEng_ipTupleFifo_V_full_n fifo_status 0 1 }  { txEng_ipTupleFifo_V_write fifo_update 1 1 } } }
	txEng_tcpTupleFifo_V { ap_fifo {  { txEng_tcpTupleFifo_V_din fifo_data 1 96 }  { txEng_tcpTupleFifo_V_full_n fifo_status 0 1 }  { txEng_tcpTupleFifo_V_write fifo_update 1 1 } } }
	txEng_tupleShortCutF_1 { ap_fifo {  { txEng_tupleShortCutF_1_dout fifo_data 0 96 }  { txEng_tupleShortCutF_1_empty_n fifo_status 0 1 }  { txEng_tupleShortCutF_1_read fifo_update 1 1 } } }
}
