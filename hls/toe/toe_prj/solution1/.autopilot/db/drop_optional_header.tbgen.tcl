set moduleName drop_optional_header
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {drop_optional_header}
set C_modelType { void 0 }
set C_modelArgList {
	{ rxEng_optionalFields_4 int 4 regular {fifo 0 volatile } {global 0}  }
	{ rxEng_optionalFields_5 int 1 regular {fifo 0 volatile } {global 0}  }
	{ rxEng_dataBuffer3b_V int 73 regular {fifo 0 volatile } {global 0}  }
	{ rxEng_dataBuffer3_V int 73 regular {fifo 1 volatile } {global 1}  }
	{ rxEng_dataOffsetFifo_1 int 4 regular {fifo 1 volatile } {global 1}  }
	{ rxEng_optionalFields_2 int 320 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "rxEng_optionalFields_4", "interface" : "fifo", "bitwidth" : 4, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_optionalFields_5", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_dataBuffer3b_V", "interface" : "fifo", "bitwidth" : 73, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_dataBuffer3_V", "interface" : "fifo", "bitwidth" : 73, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_dataOffsetFifo_1", "interface" : "fifo", "bitwidth" : 4, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_optionalFields_2", "interface" : "fifo", "bitwidth" : 320, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 25
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ rxEng_dataBuffer3b_V_dout sc_in sc_lv 73 signal 2 } 
	{ rxEng_dataBuffer3b_V_empty_n sc_in sc_logic 1 signal 2 } 
	{ rxEng_dataBuffer3b_V_read sc_out sc_logic 1 signal 2 } 
	{ rxEng_optionalFields_4_dout sc_in sc_lv 4 signal 0 } 
	{ rxEng_optionalFields_4_empty_n sc_in sc_logic 1 signal 0 } 
	{ rxEng_optionalFields_4_read sc_out sc_logic 1 signal 0 } 
	{ rxEng_optionalFields_5_dout sc_in sc_lv 1 signal 1 } 
	{ rxEng_optionalFields_5_empty_n sc_in sc_logic 1 signal 1 } 
	{ rxEng_optionalFields_5_read sc_out sc_logic 1 signal 1 } 
	{ rxEng_dataOffsetFifo_1_din sc_out sc_lv 4 signal 4 } 
	{ rxEng_dataOffsetFifo_1_full_n sc_in sc_logic 1 signal 4 } 
	{ rxEng_dataOffsetFifo_1_write sc_out sc_logic 1 signal 4 } 
	{ rxEng_dataBuffer3_V_din sc_out sc_lv 73 signal 3 } 
	{ rxEng_dataBuffer3_V_full_n sc_in sc_logic 1 signal 3 } 
	{ rxEng_dataBuffer3_V_write sc_out sc_logic 1 signal 3 } 
	{ rxEng_optionalFields_2_din sc_out sc_lv 320 signal 5 } 
	{ rxEng_optionalFields_2_full_n sc_in sc_logic 1 signal 5 } 
	{ rxEng_optionalFields_2_write sc_out sc_logic 1 signal 5 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "rxEng_dataBuffer3b_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":73, "type": "signal", "bundle":{"name": "rxEng_dataBuffer3b_V", "role": "dout" }} , 
 	{ "name": "rxEng_dataBuffer3b_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_dataBuffer3b_V", "role": "empty_n" }} , 
 	{ "name": "rxEng_dataBuffer3b_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_dataBuffer3b_V", "role": "read" }} , 
 	{ "name": "rxEng_optionalFields_4_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "rxEng_optionalFields_4", "role": "dout" }} , 
 	{ "name": "rxEng_optionalFields_4_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_optionalFields_4", "role": "empty_n" }} , 
 	{ "name": "rxEng_optionalFields_4_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_optionalFields_4", "role": "read" }} , 
 	{ "name": "rxEng_optionalFields_5_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_optionalFields_5", "role": "dout" }} , 
 	{ "name": "rxEng_optionalFields_5_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_optionalFields_5", "role": "empty_n" }} , 
 	{ "name": "rxEng_optionalFields_5_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_optionalFields_5", "role": "read" }} , 
 	{ "name": "rxEng_dataOffsetFifo_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "rxEng_dataOffsetFifo_1", "role": "din" }} , 
 	{ "name": "rxEng_dataOffsetFifo_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_dataOffsetFifo_1", "role": "full_n" }} , 
 	{ "name": "rxEng_dataOffsetFifo_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_dataOffsetFifo_1", "role": "write" }} , 
 	{ "name": "rxEng_dataBuffer3_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":73, "type": "signal", "bundle":{"name": "rxEng_dataBuffer3_V", "role": "din" }} , 
 	{ "name": "rxEng_dataBuffer3_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_dataBuffer3_V", "role": "full_n" }} , 
 	{ "name": "rxEng_dataBuffer3_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_dataBuffer3_V", "role": "write" }} , 
 	{ "name": "rxEng_optionalFields_2_din", "direction": "out", "datatype": "sc_lv", "bitwidth":320, "type": "signal", "bundle":{"name": "rxEng_optionalFields_2", "role": "din" }} , 
 	{ "name": "rxEng_optionalFields_2_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_optionalFields_2", "role": "full_n" }} , 
 	{ "name": "rxEng_optionalFields_2_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_optionalFields_2", "role": "write" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "drop_optional_header",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "4", "EstimateLatencyMin" : "4", "EstimateLatencyMax" : "4",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "state_V_3", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "optionalHeader_ready", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "optionalHeader_idx", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "dataOffset_V_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "optionalHeader_heade", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "parseHeader", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "prevWord_data_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "prevWord_keep_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "headerWritten", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "rxEng_optionalFields_4", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_optionalFields_4_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng_optionalFields_5", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_optionalFields_5_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng_dataBuffer3b_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_dataBuffer3b_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng_dataBuffer3_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_dataBuffer3_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng_dataOffsetFifo_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_dataOffsetFifo_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng_optionalFields_2", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_optionalFields_2_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	drop_optional_header {
		state_V_3 {Type IO LastRead -1 FirstWrite -1}
		optionalHeader_ready {Type IO LastRead -1 FirstWrite -1}
		optionalHeader_idx {Type IO LastRead -1 FirstWrite -1}
		dataOffset_V_1 {Type IO LastRead -1 FirstWrite -1}
		optionalHeader_heade {Type IO LastRead -1 FirstWrite -1}
		parseHeader {Type IO LastRead -1 FirstWrite -1}
		prevWord_data_V {Type IO LastRead -1 FirstWrite -1}
		prevWord_keep_V {Type IO LastRead -1 FirstWrite -1}
		headerWritten {Type IO LastRead -1 FirstWrite -1}
		rxEng_optionalFields_4 {Type I LastRead 0 FirstWrite -1}
		rxEng_optionalFields_5 {Type I LastRead 0 FirstWrite -1}
		rxEng_dataBuffer3b_V {Type I LastRead 0 FirstWrite -1}
		rxEng_dataBuffer3_V {Type O LastRead -1 FirstWrite 4}
		rxEng_dataOffsetFifo_1 {Type O LastRead -1 FirstWrite 1}
		rxEng_optionalFields_2 {Type O LastRead -1 FirstWrite 4}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "4", "Max" : "4"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	rxEng_optionalFields_4 { ap_fifo {  { rxEng_optionalFields_4_dout fifo_data 0 4 }  { rxEng_optionalFields_4_empty_n fifo_status 0 1 }  { rxEng_optionalFields_4_read fifo_update 1 1 } } }
	rxEng_optionalFields_5 { ap_fifo {  { rxEng_optionalFields_5_dout fifo_data 0 1 }  { rxEng_optionalFields_5_empty_n fifo_status 0 1 }  { rxEng_optionalFields_5_read fifo_update 1 1 } } }
	rxEng_dataBuffer3b_V { ap_fifo {  { rxEng_dataBuffer3b_V_dout fifo_data 0 73 }  { rxEng_dataBuffer3b_V_empty_n fifo_status 0 1 }  { rxEng_dataBuffer3b_V_read fifo_update 1 1 } } }
	rxEng_dataBuffer3_V { ap_fifo {  { rxEng_dataBuffer3_V_din fifo_data 1 73 }  { rxEng_dataBuffer3_V_full_n fifo_status 0 1 }  { rxEng_dataBuffer3_V_write fifo_update 1 1 } } }
	rxEng_dataOffsetFifo_1 { ap_fifo {  { rxEng_dataOffsetFifo_1_din fifo_data 1 4 }  { rxEng_dataOffsetFifo_1_full_n fifo_status 0 1 }  { rxEng_dataOffsetFifo_1_write fifo_update 1 1 } } }
	rxEng_optionalFields_2 { ap_fifo {  { rxEng_optionalFields_2_din fifo_data 1 320 }  { rxEng_optionalFields_2_full_n fifo_status 0 1 }  { rxEng_optionalFields_2_write fifo_update 1 1 } } }
}
