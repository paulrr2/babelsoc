# This script segment is generated automatically by AutoPilot

# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# Native AXIS:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::native_axis_add] == "::AESL_LIB_XILADAPTER::native_axis_add"} {
eval "::AESL_LIB_XILADAPTER::native_axis_add { \
    id 52 \
    name sessionUpdate_rsp_V \
    reset_level 1 \
    sync_rst true \
    corename {} \
    metadata {  } \
    op interface \
    ports { sessionUpdate_rsp_V_TVALID { I 1 bit } sessionUpdate_rsp_V_TDATA { I 88 vector } sessionUpdate_rsp_V_TREADY { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'sessionUpdate_rsp_V'"
}
}


# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 53 \
    name slc_sessionInsert_rs_13 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_slc_sessionInsert_rs_13 \
    op interface \
    ports { slc_sessionInsert_rs_13_din { O 1 vector } slc_sessionInsert_rs_13_full_n { I 1 bit } slc_sessionInsert_rs_13_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 54 \
    name slc_sessionInsert_rs_9 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_slc_sessionInsert_rs_9 \
    op interface \
    ports { slc_sessionInsert_rs_9_din { O 32 vector } slc_sessionInsert_rs_9_full_n { I 1 bit } slc_sessionInsert_rs_9_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 55 \
    name slc_sessionInsert_rs_8 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_slc_sessionInsert_rs_8 \
    op interface \
    ports { slc_sessionInsert_rs_8_din { O 16 vector } slc_sessionInsert_rs_8_full_n { I 1 bit } slc_sessionInsert_rs_8_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 56 \
    name slc_sessionInsert_rs_11 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_slc_sessionInsert_rs_11 \
    op interface \
    ports { slc_sessionInsert_rs_11_din { O 16 vector } slc_sessionInsert_rs_11_full_n { I 1 bit } slc_sessionInsert_rs_11_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 57 \
    name slc_sessionInsert_rs_14 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_slc_sessionInsert_rs_14 \
    op interface \
    ports { slc_sessionInsert_rs_14_din { O 16 vector } slc_sessionInsert_rs_14_full_n { I 1 bit } slc_sessionInsert_rs_14_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 58 \
    name slc_sessionInsert_rs_7 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_slc_sessionInsert_rs_7 \
    op interface \
    ports { slc_sessionInsert_rs_7_din { O 1 vector } slc_sessionInsert_rs_7_full_n { I 1 bit } slc_sessionInsert_rs_7_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 59 \
    name slc_sessionInsert_rs_15 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_slc_sessionInsert_rs_15 \
    op interface \
    ports { slc_sessionInsert_rs_15_din { O 1 vector } slc_sessionInsert_rs_15_full_n { I 1 bit } slc_sessionInsert_rs_15_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_start { I 1 bit } ap_ready { O 1 bit } ap_done { O 1 bit } ap_idle { O 1 bit } ap_continue { I 1 bit } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


