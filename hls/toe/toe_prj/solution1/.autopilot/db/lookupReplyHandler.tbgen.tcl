set moduleName lookupReplyHandler
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {lookupReplyHandler}
set C_modelType { void 0 }
set C_modelArgList {
	{ sessionLookup_rsp_V int 88 regular {axi_s 0 volatile  { sessionLookup_rsp_V Data } }  }
	{ sessionLookup_req_V int 72 regular {axi_s 1 volatile  { sessionLookup_req_V Data } }  }
	{ txApp2sLookup_req_V int 96 regular {fifo 0 volatile } {global 0}  }
	{ rxEng2sLookup_req_V int 97 regular {fifo 0 volatile } {global 0}  }
	{ slc_sessionIdFreeLis_1 int 14 regular {fifo 0 volatile } {global 0}  }
	{ sessionInsert_req_V_4 int 1 regular {fifo 1 volatile } {global 1}  }
	{ sessionInsert_req_V_1 int 32 regular {fifo 1 volatile } {global 1}  }
	{ sessionInsert_req_V_6 int 16 regular {fifo 1 volatile } {global 1}  }
	{ sessionInsert_req_V_3 int 16 regular {fifo 1 volatile } {global 1}  }
	{ sessionInsert_req_V_s int 16 regular {fifo 1 volatile } {global 1}  }
	{ sessionInsert_req_V_5 int 1 regular {fifo 1 volatile } {global 1}  }
	{ sLookup2rxEng_rsp_V int 17 regular {fifo 1 volatile } {global 1}  }
	{ sLookup2txApp_rsp_V int 17 regular {fifo 1 volatile } {global 1}  }
	{ slc_sessionInsert_rs_13 int 1 regular {fifo 0 volatile } {global 0}  }
	{ slc_sessionInsert_rs_9 int 32 regular {fifo 0 volatile } {global 0}  }
	{ slc_sessionInsert_rs_8 int 16 regular {fifo 0 volatile } {global 0}  }
	{ slc_sessionInsert_rs_11 int 16 regular {fifo 0 volatile } {global 0}  }
	{ slc_sessionInsert_rs_14 int 16 regular {fifo 0 volatile } {global 0}  }
	{ slc_sessionInsert_rs_7 int 1 regular {fifo 0 volatile } {global 0}  }
	{ slc_sessionInsert_rs_15 int 1 regular {fifo 0 volatile } {global 0}  }
	{ reverseLupInsertFifo_8 int 16 regular {fifo 1 volatile } {global 1}  }
	{ reverseLupInsertFifo_6 int 32 regular {fifo 1 volatile } {global 1}  }
	{ reverseLupInsertFifo_4 int 16 regular {fifo 1 volatile } {global 1}  }
	{ reverseLupInsertFifo_7 int 16 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "sessionLookup_rsp_V", "interface" : "axis", "bitwidth" : 88, "direction" : "READONLY"} , 
 	{ "Name" : "sessionLookup_req_V", "interface" : "axis", "bitwidth" : 72, "direction" : "WRITEONLY"} , 
 	{ "Name" : "txApp2sLookup_req_V", "interface" : "fifo", "bitwidth" : 96, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "rxEng2sLookup_req_V", "interface" : "fifo", "bitwidth" : 97, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "slc_sessionIdFreeLis_1", "interface" : "fifo", "bitwidth" : 14, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "sessionInsert_req_V_4", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "sessionInsert_req_V_1", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "sessionInsert_req_V_6", "interface" : "fifo", "bitwidth" : 16, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "sessionInsert_req_V_3", "interface" : "fifo", "bitwidth" : 16, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "sessionInsert_req_V_s", "interface" : "fifo", "bitwidth" : 16, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "sessionInsert_req_V_5", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "sLookup2rxEng_rsp_V", "interface" : "fifo", "bitwidth" : 17, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "sLookup2txApp_rsp_V", "interface" : "fifo", "bitwidth" : 17, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "slc_sessionInsert_rs_13", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "slc_sessionInsert_rs_9", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "slc_sessionInsert_rs_8", "interface" : "fifo", "bitwidth" : 16, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "slc_sessionInsert_rs_11", "interface" : "fifo", "bitwidth" : 16, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "slc_sessionInsert_rs_14", "interface" : "fifo", "bitwidth" : 16, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "slc_sessionInsert_rs_7", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "slc_sessionInsert_rs_15", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "reverseLupInsertFifo_8", "interface" : "fifo", "bitwidth" : 16, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "reverseLupInsertFifo_6", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "reverseLupInsertFifo_4", "interface" : "fifo", "bitwidth" : 16, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "reverseLupInsertFifo_7", "interface" : "fifo", "bitwidth" : 16, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 79
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ slc_sessionInsert_rs_13_dout sc_in sc_lv 1 signal 13 } 
	{ slc_sessionInsert_rs_13_empty_n sc_in sc_logic 1 signal 13 } 
	{ slc_sessionInsert_rs_13_read sc_out sc_logic 1 signal 13 } 
	{ slc_sessionInsert_rs_9_dout sc_in sc_lv 32 signal 14 } 
	{ slc_sessionInsert_rs_9_empty_n sc_in sc_logic 1 signal 14 } 
	{ slc_sessionInsert_rs_9_read sc_out sc_logic 1 signal 14 } 
	{ slc_sessionInsert_rs_8_dout sc_in sc_lv 16 signal 15 } 
	{ slc_sessionInsert_rs_8_empty_n sc_in sc_logic 1 signal 15 } 
	{ slc_sessionInsert_rs_8_read sc_out sc_logic 1 signal 15 } 
	{ slc_sessionInsert_rs_11_dout sc_in sc_lv 16 signal 16 } 
	{ slc_sessionInsert_rs_11_empty_n sc_in sc_logic 1 signal 16 } 
	{ slc_sessionInsert_rs_11_read sc_out sc_logic 1 signal 16 } 
	{ slc_sessionInsert_rs_14_dout sc_in sc_lv 16 signal 17 } 
	{ slc_sessionInsert_rs_14_empty_n sc_in sc_logic 1 signal 17 } 
	{ slc_sessionInsert_rs_14_read sc_out sc_logic 1 signal 17 } 
	{ slc_sessionInsert_rs_7_dout sc_in sc_lv 1 signal 18 } 
	{ slc_sessionInsert_rs_7_empty_n sc_in sc_logic 1 signal 18 } 
	{ slc_sessionInsert_rs_7_read sc_out sc_logic 1 signal 18 } 
	{ slc_sessionInsert_rs_15_dout sc_in sc_lv 1 signal 19 } 
	{ slc_sessionInsert_rs_15_empty_n sc_in sc_logic 1 signal 19 } 
	{ slc_sessionInsert_rs_15_read sc_out sc_logic 1 signal 19 } 
	{ sessionLookup_rsp_V_TVALID sc_in sc_logic 1 invld 0 } 
	{ slc_sessionIdFreeLis_1_dout sc_in sc_lv 14 signal 4 } 
	{ slc_sessionIdFreeLis_1_empty_n sc_in sc_logic 1 signal 4 } 
	{ slc_sessionIdFreeLis_1_read sc_out sc_logic 1 signal 4 } 
	{ rxEng2sLookup_req_V_dout sc_in sc_lv 97 signal 3 } 
	{ rxEng2sLookup_req_V_empty_n sc_in sc_logic 1 signal 3 } 
	{ rxEng2sLookup_req_V_read sc_out sc_logic 1 signal 3 } 
	{ txApp2sLookup_req_V_dout sc_in sc_lv 96 signal 2 } 
	{ txApp2sLookup_req_V_empty_n sc_in sc_logic 1 signal 2 } 
	{ txApp2sLookup_req_V_read sc_out sc_logic 1 signal 2 } 
	{ sLookup2rxEng_rsp_V_din sc_out sc_lv 17 signal 11 } 
	{ sLookup2rxEng_rsp_V_full_n sc_in sc_logic 1 signal 11 } 
	{ sLookup2rxEng_rsp_V_write sc_out sc_logic 1 signal 11 } 
	{ sLookup2txApp_rsp_V_din sc_out sc_lv 17 signal 12 } 
	{ sLookup2txApp_rsp_V_full_n sc_in sc_logic 1 signal 12 } 
	{ sLookup2txApp_rsp_V_write sc_out sc_logic 1 signal 12 } 
	{ reverseLupInsertFifo_8_din sc_out sc_lv 16 signal 20 } 
	{ reverseLupInsertFifo_8_full_n sc_in sc_logic 1 signal 20 } 
	{ reverseLupInsertFifo_8_write sc_out sc_logic 1 signal 20 } 
	{ reverseLupInsertFifo_6_din sc_out sc_lv 32 signal 21 } 
	{ reverseLupInsertFifo_6_full_n sc_in sc_logic 1 signal 21 } 
	{ reverseLupInsertFifo_6_write sc_out sc_logic 1 signal 21 } 
	{ reverseLupInsertFifo_4_din sc_out sc_lv 16 signal 22 } 
	{ reverseLupInsertFifo_4_full_n sc_in sc_logic 1 signal 22 } 
	{ reverseLupInsertFifo_4_write sc_out sc_logic 1 signal 22 } 
	{ reverseLupInsertFifo_7_din sc_out sc_lv 16 signal 23 } 
	{ reverseLupInsertFifo_7_full_n sc_in sc_logic 1 signal 23 } 
	{ reverseLupInsertFifo_7_write sc_out sc_logic 1 signal 23 } 
	{ sessionInsert_req_V_4_din sc_out sc_lv 1 signal 5 } 
	{ sessionInsert_req_V_4_full_n sc_in sc_logic 1 signal 5 } 
	{ sessionInsert_req_V_4_write sc_out sc_logic 1 signal 5 } 
	{ sessionInsert_req_V_1_din sc_out sc_lv 32 signal 6 } 
	{ sessionInsert_req_V_1_full_n sc_in sc_logic 1 signal 6 } 
	{ sessionInsert_req_V_1_write sc_out sc_logic 1 signal 6 } 
	{ sessionInsert_req_V_6_din sc_out sc_lv 16 signal 7 } 
	{ sessionInsert_req_V_6_full_n sc_in sc_logic 1 signal 7 } 
	{ sessionInsert_req_V_6_write sc_out sc_logic 1 signal 7 } 
	{ sessionInsert_req_V_3_din sc_out sc_lv 16 signal 8 } 
	{ sessionInsert_req_V_3_full_n sc_in sc_logic 1 signal 8 } 
	{ sessionInsert_req_V_3_write sc_out sc_logic 1 signal 8 } 
	{ sessionInsert_req_V_s_din sc_out sc_lv 16 signal 9 } 
	{ sessionInsert_req_V_s_full_n sc_in sc_logic 1 signal 9 } 
	{ sessionInsert_req_V_s_write sc_out sc_logic 1 signal 9 } 
	{ sessionInsert_req_V_5_din sc_out sc_lv 1 signal 10 } 
	{ sessionInsert_req_V_5_full_n sc_in sc_logic 1 signal 10 } 
	{ sessionInsert_req_V_5_write sc_out sc_logic 1 signal 10 } 
	{ sessionLookup_req_V_TREADY sc_in sc_logic 1 outacc 1 } 
	{ sessionLookup_rsp_V_TDATA sc_in sc_lv 88 signal 0 } 
	{ sessionLookup_rsp_V_TREADY sc_out sc_logic 1 inacc 0 } 
	{ sessionLookup_req_V_TDATA sc_out sc_lv 72 signal 1 } 
	{ sessionLookup_req_V_TVALID sc_out sc_logic 1 outvld 1 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "slc_sessionInsert_rs_13_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_13", "role": "dout" }} , 
 	{ "name": "slc_sessionInsert_rs_13_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_13", "role": "empty_n" }} , 
 	{ "name": "slc_sessionInsert_rs_13_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_13", "role": "read" }} , 
 	{ "name": "slc_sessionInsert_rs_9_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_9", "role": "dout" }} , 
 	{ "name": "slc_sessionInsert_rs_9_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_9", "role": "empty_n" }} , 
 	{ "name": "slc_sessionInsert_rs_9_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_9", "role": "read" }} , 
 	{ "name": "slc_sessionInsert_rs_8_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_8", "role": "dout" }} , 
 	{ "name": "slc_sessionInsert_rs_8_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_8", "role": "empty_n" }} , 
 	{ "name": "slc_sessionInsert_rs_8_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_8", "role": "read" }} , 
 	{ "name": "slc_sessionInsert_rs_11_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_11", "role": "dout" }} , 
 	{ "name": "slc_sessionInsert_rs_11_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_11", "role": "empty_n" }} , 
 	{ "name": "slc_sessionInsert_rs_11_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_11", "role": "read" }} , 
 	{ "name": "slc_sessionInsert_rs_14_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_14", "role": "dout" }} , 
 	{ "name": "slc_sessionInsert_rs_14_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_14", "role": "empty_n" }} , 
 	{ "name": "slc_sessionInsert_rs_14_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_14", "role": "read" }} , 
 	{ "name": "slc_sessionInsert_rs_7_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_7", "role": "dout" }} , 
 	{ "name": "slc_sessionInsert_rs_7_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_7", "role": "empty_n" }} , 
 	{ "name": "slc_sessionInsert_rs_7_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_7", "role": "read" }} , 
 	{ "name": "slc_sessionInsert_rs_15_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_15", "role": "dout" }} , 
 	{ "name": "slc_sessionInsert_rs_15_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_15", "role": "empty_n" }} , 
 	{ "name": "slc_sessionInsert_rs_15_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionInsert_rs_15", "role": "read" }} , 
 	{ "name": "sessionLookup_rsp_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "sessionLookup_rsp_V", "role": "TVALID" }} , 
 	{ "name": "slc_sessionIdFreeLis_1_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":14, "type": "signal", "bundle":{"name": "slc_sessionIdFreeLis_1", "role": "dout" }} , 
 	{ "name": "slc_sessionIdFreeLis_1_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionIdFreeLis_1", "role": "empty_n" }} , 
 	{ "name": "slc_sessionIdFreeLis_1_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "slc_sessionIdFreeLis_1", "role": "read" }} , 
 	{ "name": "rxEng2sLookup_req_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":97, "type": "signal", "bundle":{"name": "rxEng2sLookup_req_V", "role": "dout" }} , 
 	{ "name": "rxEng2sLookup_req_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng2sLookup_req_V", "role": "empty_n" }} , 
 	{ "name": "rxEng2sLookup_req_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng2sLookup_req_V", "role": "read" }} , 
 	{ "name": "txApp2sLookup_req_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":96, "type": "signal", "bundle":{"name": "txApp2sLookup_req_V", "role": "dout" }} , 
 	{ "name": "txApp2sLookup_req_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txApp2sLookup_req_V", "role": "empty_n" }} , 
 	{ "name": "txApp2sLookup_req_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txApp2sLookup_req_V", "role": "read" }} , 
 	{ "name": "sLookup2rxEng_rsp_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":17, "type": "signal", "bundle":{"name": "sLookup2rxEng_rsp_V", "role": "din" }} , 
 	{ "name": "sLookup2rxEng_rsp_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sLookup2rxEng_rsp_V", "role": "full_n" }} , 
 	{ "name": "sLookup2rxEng_rsp_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sLookup2rxEng_rsp_V", "role": "write" }} , 
 	{ "name": "sLookup2txApp_rsp_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":17, "type": "signal", "bundle":{"name": "sLookup2txApp_rsp_V", "role": "din" }} , 
 	{ "name": "sLookup2txApp_rsp_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sLookup2txApp_rsp_V", "role": "full_n" }} , 
 	{ "name": "sLookup2txApp_rsp_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sLookup2txApp_rsp_V", "role": "write" }} , 
 	{ "name": "reverseLupInsertFifo_8_din", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "reverseLupInsertFifo_8", "role": "din" }} , 
 	{ "name": "reverseLupInsertFifo_8_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "reverseLupInsertFifo_8", "role": "full_n" }} , 
 	{ "name": "reverseLupInsertFifo_8_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "reverseLupInsertFifo_8", "role": "write" }} , 
 	{ "name": "reverseLupInsertFifo_6_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "reverseLupInsertFifo_6", "role": "din" }} , 
 	{ "name": "reverseLupInsertFifo_6_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "reverseLupInsertFifo_6", "role": "full_n" }} , 
 	{ "name": "reverseLupInsertFifo_6_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "reverseLupInsertFifo_6", "role": "write" }} , 
 	{ "name": "reverseLupInsertFifo_4_din", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "reverseLupInsertFifo_4", "role": "din" }} , 
 	{ "name": "reverseLupInsertFifo_4_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "reverseLupInsertFifo_4", "role": "full_n" }} , 
 	{ "name": "reverseLupInsertFifo_4_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "reverseLupInsertFifo_4", "role": "write" }} , 
 	{ "name": "reverseLupInsertFifo_7_din", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "reverseLupInsertFifo_7", "role": "din" }} , 
 	{ "name": "reverseLupInsertFifo_7_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "reverseLupInsertFifo_7", "role": "full_n" }} , 
 	{ "name": "reverseLupInsertFifo_7_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "reverseLupInsertFifo_7", "role": "write" }} , 
 	{ "name": "sessionInsert_req_V_4_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionInsert_req_V_4", "role": "din" }} , 
 	{ "name": "sessionInsert_req_V_4_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionInsert_req_V_4", "role": "full_n" }} , 
 	{ "name": "sessionInsert_req_V_4_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionInsert_req_V_4", "role": "write" }} , 
 	{ "name": "sessionInsert_req_V_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "sessionInsert_req_V_1", "role": "din" }} , 
 	{ "name": "sessionInsert_req_V_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionInsert_req_V_1", "role": "full_n" }} , 
 	{ "name": "sessionInsert_req_V_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionInsert_req_V_1", "role": "write" }} , 
 	{ "name": "sessionInsert_req_V_6_din", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "sessionInsert_req_V_6", "role": "din" }} , 
 	{ "name": "sessionInsert_req_V_6_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionInsert_req_V_6", "role": "full_n" }} , 
 	{ "name": "sessionInsert_req_V_6_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionInsert_req_V_6", "role": "write" }} , 
 	{ "name": "sessionInsert_req_V_3_din", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "sessionInsert_req_V_3", "role": "din" }} , 
 	{ "name": "sessionInsert_req_V_3_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionInsert_req_V_3", "role": "full_n" }} , 
 	{ "name": "sessionInsert_req_V_3_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionInsert_req_V_3", "role": "write" }} , 
 	{ "name": "sessionInsert_req_V_s_din", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "sessionInsert_req_V_s", "role": "din" }} , 
 	{ "name": "sessionInsert_req_V_s_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionInsert_req_V_s", "role": "full_n" }} , 
 	{ "name": "sessionInsert_req_V_s_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionInsert_req_V_s", "role": "write" }} , 
 	{ "name": "sessionInsert_req_V_5_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionInsert_req_V_5", "role": "din" }} , 
 	{ "name": "sessionInsert_req_V_5_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionInsert_req_V_5", "role": "full_n" }} , 
 	{ "name": "sessionInsert_req_V_5_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sessionInsert_req_V_5", "role": "write" }} , 
 	{ "name": "sessionLookup_req_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "sessionLookup_req_V", "role": "TREADY" }} , 
 	{ "name": "sessionLookup_rsp_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":88, "type": "signal", "bundle":{"name": "sessionLookup_rsp_V", "role": "TDATA" }} , 
 	{ "name": "sessionLookup_rsp_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "sessionLookup_rsp_V", "role": "TREADY" }} , 
 	{ "name": "sessionLookup_req_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":72, "type": "signal", "bundle":{"name": "sessionLookup_req_V", "role": "TDATA" }} , 
 	{ "name": "sessionLookup_req_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "sessionLookup_req_V", "role": "TVALID" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2", "3", "4", "5", "6", "7", "8", "9"],
		"CDFG" : "lookupReplyHandler",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "sessionLookup_rsp_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "sessionLookup_rsp_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "sessionLookup_req_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "sessionLookup_req_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "slc_fsmState", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "txApp2sLookup_req_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txApp2sLookup_req_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "slc_queryCache_V_tup_2", "Type" : "Fifo", "Direction" : "IO"},
			{"Name" : "slc_queryCache_V_tup_1", "Type" : "Fifo", "Direction" : "IO"},
			{"Name" : "slc_queryCache_V_tup", "Type" : "Fifo", "Direction" : "IO"},
			{"Name" : "slc_queryCache_V_all", "Type" : "Fifo", "Direction" : "IO"},
			{"Name" : "slc_queryCache_V_sou", "Type" : "Fifo", "Direction" : "IO"},
			{"Name" : "rxEng2sLookup_req_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng2sLookup_req_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "slc_sessionIdFreeLis_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "slc_sessionIdFreeLis_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "sessionInsert_req_V_4", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "sessionInsert_req_V_4_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "sessionInsert_req_V_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "sessionInsert_req_V_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "sessionInsert_req_V_6", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "sessionInsert_req_V_6_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "sessionInsert_req_V_3", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "sessionInsert_req_V_3_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "sessionInsert_req_V_s", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "sessionInsert_req_V_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "sessionInsert_req_V_5", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "sessionInsert_req_V_5_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "slc_insertTuples_V_t_1", "Type" : "Fifo", "Direction" : "IO"},
			{"Name" : "slc_insertTuples_V_m", "Type" : "Fifo", "Direction" : "IO"},
			{"Name" : "slc_insertTuples_V_t", "Type" : "Fifo", "Direction" : "IO"},
			{"Name" : "sLookup2rxEng_rsp_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "sLookup2rxEng_rsp_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "sLookup2txApp_rsp_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "sLookup2txApp_rsp_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "slc_sessionInsert_rs_13", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "slc_sessionInsert_rs_13_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "slc_sessionInsert_rs_9", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "slc_sessionInsert_rs_9_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "slc_sessionInsert_rs_8", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "slc_sessionInsert_rs_8_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "slc_sessionInsert_rs_11", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "slc_sessionInsert_rs_11_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "slc_sessionInsert_rs_14", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "slc_sessionInsert_rs_14_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "slc_sessionInsert_rs_7", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "slc_sessionInsert_rs_7_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "slc_sessionInsert_rs_15", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "slc_sessionInsert_rs_15_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "reverseLupInsertFifo_8", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "reverseLupInsertFifo_8_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "reverseLupInsertFifo_6", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "reverseLupInsertFifo_6_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "reverseLupInsertFifo_4", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "reverseLupInsertFifo_4_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "reverseLupInsertFifo_7", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "reverseLupInsertFifo_7_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.slc_insertTuples_V_m_fifo_U", "Parent" : "0"},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.slc_insertTuples_V_t_1_fifo_U", "Parent" : "0"},
	{"ID" : "3", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.slc_insertTuples_V_t_fifo_U", "Parent" : "0"},
	{"ID" : "4", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.slc_queryCache_V_all_fifo_U", "Parent" : "0"},
	{"ID" : "5", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.slc_queryCache_V_sou_fifo_U", "Parent" : "0"},
	{"ID" : "6", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.slc_queryCache_V_tup_1_fifo_U", "Parent" : "0"},
	{"ID" : "7", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.slc_queryCache_V_tup_2_fifo_U", "Parent" : "0"},
	{"ID" : "8", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.slc_queryCache_V_tup_fifo_U", "Parent" : "0"},
	{"ID" : "9", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_sessionLookup_req_V_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	lookupReplyHandler {
		sessionLookup_rsp_V {Type I LastRead 0 FirstWrite -1}
		sessionLookup_req_V {Type O LastRead -1 FirstWrite 1}
		slc_fsmState {Type IO LastRead -1 FirstWrite -1}
		txApp2sLookup_req_V {Type I LastRead 0 FirstWrite -1}
		slc_queryCache_V_tup_2 {Type IO LastRead -1 FirstWrite -1}
		slc_queryCache_V_tup_1 {Type IO LastRead -1 FirstWrite -1}
		slc_queryCache_V_tup {Type IO LastRead -1 FirstWrite -1}
		slc_queryCache_V_all {Type IO LastRead -1 FirstWrite -1}
		slc_queryCache_V_sou {Type IO LastRead -1 FirstWrite -1}
		rxEng2sLookup_req_V {Type I LastRead 0 FirstWrite -1}
		slc_sessionIdFreeLis_1 {Type I LastRead 0 FirstWrite -1}
		sessionInsert_req_V_4 {Type O LastRead -1 FirstWrite 1}
		sessionInsert_req_V_1 {Type O LastRead -1 FirstWrite 1}
		sessionInsert_req_V_6 {Type O LastRead -1 FirstWrite 1}
		sessionInsert_req_V_3 {Type O LastRead -1 FirstWrite 1}
		sessionInsert_req_V_s {Type O LastRead -1 FirstWrite 1}
		sessionInsert_req_V_5 {Type O LastRead -1 FirstWrite 1}
		slc_insertTuples_V_t_1 {Type IO LastRead -1 FirstWrite -1}
		slc_insertTuples_V_m {Type IO LastRead -1 FirstWrite -1}
		slc_insertTuples_V_t {Type IO LastRead -1 FirstWrite -1}
		sLookup2rxEng_rsp_V {Type O LastRead -1 FirstWrite 1}
		sLookup2txApp_rsp_V {Type O LastRead -1 FirstWrite 1}
		slc_sessionInsert_rs_13 {Type I LastRead 0 FirstWrite -1}
		slc_sessionInsert_rs_9 {Type I LastRead 0 FirstWrite -1}
		slc_sessionInsert_rs_8 {Type I LastRead 0 FirstWrite -1}
		slc_sessionInsert_rs_11 {Type I LastRead 0 FirstWrite -1}
		slc_sessionInsert_rs_14 {Type I LastRead 0 FirstWrite -1}
		slc_sessionInsert_rs_7 {Type I LastRead 0 FirstWrite -1}
		slc_sessionInsert_rs_15 {Type I LastRead 0 FirstWrite -1}
		reverseLupInsertFifo_8 {Type O LastRead -1 FirstWrite 1}
		reverseLupInsertFifo_6 {Type O LastRead -1 FirstWrite 1}
		reverseLupInsertFifo_4 {Type O LastRead -1 FirstWrite 1}
		reverseLupInsertFifo_7 {Type O LastRead -1 FirstWrite 1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "2", "Max" : "2"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	sessionLookup_rsp_V { axis {  { sessionLookup_rsp_V_TVALID in_vld 0 1 }  { sessionLookup_rsp_V_TDATA in_data 0 88 }  { sessionLookup_rsp_V_TREADY in_acc 1 1 } } }
	sessionLookup_req_V { axis {  { sessionLookup_req_V_TREADY out_acc 0 1 }  { sessionLookup_req_V_TDATA out_data 1 72 }  { sessionLookup_req_V_TVALID out_vld 1 1 } } }
	txApp2sLookup_req_V { ap_fifo {  { txApp2sLookup_req_V_dout fifo_data 0 96 }  { txApp2sLookup_req_V_empty_n fifo_status 0 1 }  { txApp2sLookup_req_V_read fifo_update 1 1 } } }
	rxEng2sLookup_req_V { ap_fifo {  { rxEng2sLookup_req_V_dout fifo_data 0 97 }  { rxEng2sLookup_req_V_empty_n fifo_status 0 1 }  { rxEng2sLookup_req_V_read fifo_update 1 1 } } }
	slc_sessionIdFreeLis_1 { ap_fifo {  { slc_sessionIdFreeLis_1_dout fifo_data 0 14 }  { slc_sessionIdFreeLis_1_empty_n fifo_status 0 1 }  { slc_sessionIdFreeLis_1_read fifo_update 1 1 } } }
	sessionInsert_req_V_4 { ap_fifo {  { sessionInsert_req_V_4_din fifo_data 1 1 }  { sessionInsert_req_V_4_full_n fifo_status 0 1 }  { sessionInsert_req_V_4_write fifo_update 1 1 } } }
	sessionInsert_req_V_1 { ap_fifo {  { sessionInsert_req_V_1_din fifo_data 1 32 }  { sessionInsert_req_V_1_full_n fifo_status 0 1 }  { sessionInsert_req_V_1_write fifo_update 1 1 } } }
	sessionInsert_req_V_6 { ap_fifo {  { sessionInsert_req_V_6_din fifo_data 1 16 }  { sessionInsert_req_V_6_full_n fifo_status 0 1 }  { sessionInsert_req_V_6_write fifo_update 1 1 } } }
	sessionInsert_req_V_3 { ap_fifo {  { sessionInsert_req_V_3_din fifo_data 1 16 }  { sessionInsert_req_V_3_full_n fifo_status 0 1 }  { sessionInsert_req_V_3_write fifo_update 1 1 } } }
	sessionInsert_req_V_s { ap_fifo {  { sessionInsert_req_V_s_din fifo_data 1 16 }  { sessionInsert_req_V_s_full_n fifo_status 0 1 }  { sessionInsert_req_V_s_write fifo_update 1 1 } } }
	sessionInsert_req_V_5 { ap_fifo {  { sessionInsert_req_V_5_din fifo_data 1 1 }  { sessionInsert_req_V_5_full_n fifo_status 0 1 }  { sessionInsert_req_V_5_write fifo_update 1 1 } } }
	sLookup2rxEng_rsp_V { ap_fifo {  { sLookup2rxEng_rsp_V_din fifo_data 1 17 }  { sLookup2rxEng_rsp_V_full_n fifo_status 0 1 }  { sLookup2rxEng_rsp_V_write fifo_update 1 1 } } }
	sLookup2txApp_rsp_V { ap_fifo {  { sLookup2txApp_rsp_V_din fifo_data 1 17 }  { sLookup2txApp_rsp_V_full_n fifo_status 0 1 }  { sLookup2txApp_rsp_V_write fifo_update 1 1 } } }
	slc_sessionInsert_rs_13 { ap_fifo {  { slc_sessionInsert_rs_13_dout fifo_data 0 1 }  { slc_sessionInsert_rs_13_empty_n fifo_status 0 1 }  { slc_sessionInsert_rs_13_read fifo_update 1 1 } } }
	slc_sessionInsert_rs_9 { ap_fifo {  { slc_sessionInsert_rs_9_dout fifo_data 0 32 }  { slc_sessionInsert_rs_9_empty_n fifo_status 0 1 }  { slc_sessionInsert_rs_9_read fifo_update 1 1 } } }
	slc_sessionInsert_rs_8 { ap_fifo {  { slc_sessionInsert_rs_8_dout fifo_data 0 16 }  { slc_sessionInsert_rs_8_empty_n fifo_status 0 1 }  { slc_sessionInsert_rs_8_read fifo_update 1 1 } } }
	slc_sessionInsert_rs_11 { ap_fifo {  { slc_sessionInsert_rs_11_dout fifo_data 0 16 }  { slc_sessionInsert_rs_11_empty_n fifo_status 0 1 }  { slc_sessionInsert_rs_11_read fifo_update 1 1 } } }
	slc_sessionInsert_rs_14 { ap_fifo {  { slc_sessionInsert_rs_14_dout fifo_data 0 16 }  { slc_sessionInsert_rs_14_empty_n fifo_status 0 1 }  { slc_sessionInsert_rs_14_read fifo_update 1 1 } } }
	slc_sessionInsert_rs_7 { ap_fifo {  { slc_sessionInsert_rs_7_dout fifo_data 0 1 }  { slc_sessionInsert_rs_7_empty_n fifo_status 0 1 }  { slc_sessionInsert_rs_7_read fifo_update 1 1 } } }
	slc_sessionInsert_rs_15 { ap_fifo {  { slc_sessionInsert_rs_15_dout fifo_data 0 1 }  { slc_sessionInsert_rs_15_empty_n fifo_status 0 1 }  { slc_sessionInsert_rs_15_read fifo_update 1 1 } } }
	reverseLupInsertFifo_8 { ap_fifo {  { reverseLupInsertFifo_8_din fifo_data 1 16 }  { reverseLupInsertFifo_8_full_n fifo_status 0 1 }  { reverseLupInsertFifo_8_write fifo_update 1 1 } } }
	reverseLupInsertFifo_6 { ap_fifo {  { reverseLupInsertFifo_6_din fifo_data 1 32 }  { reverseLupInsertFifo_6_full_n fifo_status 0 1 }  { reverseLupInsertFifo_6_write fifo_update 1 1 } } }
	reverseLupInsertFifo_4 { ap_fifo {  { reverseLupInsertFifo_4_din fifo_data 1 16 }  { reverseLupInsertFifo_4_full_n fifo_status 0 1 }  { reverseLupInsertFifo_4_write fifo_update 1 1 } } }
	reverseLupInsertFifo_7 { ap_fifo {  { reverseLupInsertFifo_7_din fifo_data 1 16 }  { reverseLupInsertFifo_7_full_n fifo_status 0 1 }  { reverseLupInsertFifo_7_write fifo_update 1 1 } } }
}
