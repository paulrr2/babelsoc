set moduleName read_data_stitching
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {read_data_stitching}
set C_modelType { void 0 }
set C_modelArgList {
	{ readDataIn_V_data_V int 64 regular {axi_s 0 volatile  { s_axis_txread_data Data } }  }
	{ readDataIn_V_keep_V int 8 regular {axi_s 0 volatile  { s_axis_txread_data Keep } }  }
	{ readDataIn_V_last_V int 1 regular {axi_s 0 volatile  { s_axis_txread_data Last } }  }
	{ memAccessBreakdown2t_1 int 1 regular {fifo 0 volatile } {global 0}  }
	{ txBufferReadDataStit_1 int 73 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "readDataIn_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "READONLY"} , 
 	{ "Name" : "readDataIn_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "readDataIn_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY"} , 
 	{ "Name" : "memAccessBreakdown2t_1", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "txBufferReadDataStit_1", "interface" : "fifo", "bitwidth" : 73, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 18
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ s_axis_txread_data_TVALID sc_in sc_logic 1 invld 0 } 
	{ memAccessBreakdown2t_1_dout sc_in sc_lv 1 signal 3 } 
	{ memAccessBreakdown2t_1_empty_n sc_in sc_logic 1 signal 3 } 
	{ memAccessBreakdown2t_1_read sc_out sc_logic 1 signal 3 } 
	{ txBufferReadDataStit_1_din sc_out sc_lv 73 signal 4 } 
	{ txBufferReadDataStit_1_full_n sc_in sc_logic 1 signal 4 } 
	{ txBufferReadDataStit_1_write sc_out sc_logic 1 signal 4 } 
	{ s_axis_txread_data_TDATA sc_in sc_lv 64 signal 0 } 
	{ s_axis_txread_data_TREADY sc_out sc_logic 1 inacc 2 } 
	{ s_axis_txread_data_TKEEP sc_in sc_lv 8 signal 1 } 
	{ s_axis_txread_data_TLAST sc_in sc_lv 1 signal 2 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "s_axis_txread_data_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "readDataIn_V_data_V", "role": "VALID" }} , 
 	{ "name": "memAccessBreakdown2t_1_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "memAccessBreakdown2t_1", "role": "dout" }} , 
 	{ "name": "memAccessBreakdown2t_1_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "memAccessBreakdown2t_1", "role": "empty_n" }} , 
 	{ "name": "memAccessBreakdown2t_1_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "memAccessBreakdown2t_1", "role": "read" }} , 
 	{ "name": "txBufferReadDataStit_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":73, "type": "signal", "bundle":{"name": "txBufferReadDataStit_1", "role": "din" }} , 
 	{ "name": "txBufferReadDataStit_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txBufferReadDataStit_1", "role": "full_n" }} , 
 	{ "name": "txBufferReadDataStit_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txBufferReadDataStit_1", "role": "write" }} , 
 	{ "name": "s_axis_txread_data_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "readDataIn_V_data_V", "role": "DATA" }} , 
 	{ "name": "s_axis_txread_data_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "readDataIn_V_last_V", "role": "READY" }} , 
 	{ "name": "s_axis_txread_data_TKEEP", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "readDataIn_V_keep_V", "role": "KEEP" }} , 
 	{ "name": "s_axis_txread_data_TLAST", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "readDataIn_V_last_V", "role": "LAST" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "read_data_stitching",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "3", "EstimateLatencyMin" : "3", "EstimateLatencyMax" : "3",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "readDataIn_V_data_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "s_axis_txread_data_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "readDataIn_V_keep_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "readDataIn_V_last_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "state", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "pkgNeedsMerge", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "offset_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "prevWord_data_V_9", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "prevWord_keep_V_9", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "memAccessBreakdown2t_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "memAccessBreakdown2t_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txBufferReadDataStit_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txBufferReadDataStit_1_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	read_data_stitching {
		readDataIn_V_data_V {Type I LastRead 0 FirstWrite -1}
		readDataIn_V_keep_V {Type I LastRead 0 FirstWrite -1}
		readDataIn_V_last_V {Type I LastRead 0 FirstWrite -1}
		state {Type IO LastRead -1 FirstWrite -1}
		pkgNeedsMerge {Type IO LastRead -1 FirstWrite -1}
		offset_V {Type IO LastRead -1 FirstWrite -1}
		prevWord_data_V_9 {Type IO LastRead -1 FirstWrite -1}
		prevWord_keep_V_9 {Type IO LastRead -1 FirstWrite -1}
		memAccessBreakdown2t_1 {Type I LastRead 0 FirstWrite -1}
		txBufferReadDataStit_1 {Type O LastRead -1 FirstWrite 3}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "3", "Max" : "3"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	readDataIn_V_data_V { axis {  { s_axis_txread_data_TVALID in_vld 0 1 }  { s_axis_txread_data_TDATA in_data 0 64 } } }
	readDataIn_V_keep_V { axis {  { s_axis_txread_data_TKEEP in_data 0 8 } } }
	readDataIn_V_last_V { axis {  { s_axis_txread_data_TREADY in_acc 1 1 }  { s_axis_txread_data_TLAST in_data 0 1 } } }
	memAccessBreakdown2t_1 { ap_fifo {  { memAccessBreakdown2t_1_dout fifo_data 0 1 }  { memAccessBreakdown2t_1_empty_n fifo_status 0 1 }  { memAccessBreakdown2t_1_read fifo_update 1 1 } } }
	txBufferReadDataStit_1 { ap_fifo {  { txBufferReadDataStit_1_din fifo_data 1 73 }  { txBufferReadDataStit_1_full_n fifo_status 0 1 }  { txBufferReadDataStit_1_write fifo_update 1 1 } } }
}
