# This script segment is generated automatically by AutoPilot

# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 204 \
    name rxEng_dataBuffer2_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_dataBuffer2_V \
    op interface \
    ports { rxEng_dataBuffer2_V_dout { I 73 vector } rxEng_dataBuffer2_V_empty_n { I 1 bit } rxEng_dataBuffer2_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 205 \
    name rxEng_checksumValidF_1 \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_checksumValidF_1 \
    op interface \
    ports { rxEng_checksumValidF_1_dout { I 1 vector } rxEng_checksumValidF_1_empty_n { I 1 bit } rxEng_checksumValidF_1_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 206 \
    name rxEng_dataBuffer3a_V \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_dataBuffer3a_V \
    op interface \
    ports { rxEng_dataBuffer3a_V_din { O 73 vector } rxEng_dataBuffer3a_V_full_n { I 1 bit } rxEng_dataBuffer3a_V_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 207 \
    name rxEng_headerMetaFifo_20 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_headerMetaFifo_20 \
    op interface \
    ports { rxEng_headerMetaFifo_20_din { O 32 vector } rxEng_headerMetaFifo_20_full_n { I 1 bit } rxEng_headerMetaFifo_20_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 208 \
    name rxEng_headerMetaFifo_12 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_headerMetaFifo_12 \
    op interface \
    ports { rxEng_headerMetaFifo_12_din { O 32 vector } rxEng_headerMetaFifo_12_full_n { I 1 bit } rxEng_headerMetaFifo_12_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 209 \
    name rxEng_headerMetaFifo_23 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_headerMetaFifo_23 \
    op interface \
    ports { rxEng_headerMetaFifo_23_din { O 16 vector } rxEng_headerMetaFifo_23_full_n { I 1 bit } rxEng_headerMetaFifo_23_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 210 \
    name rxEng_headerMetaFifo_22 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_headerMetaFifo_22 \
    op interface \
    ports { rxEng_headerMetaFifo_22_din { O 4 vector } rxEng_headerMetaFifo_22_full_n { I 1 bit } rxEng_headerMetaFifo_22_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 211 \
    name rxEng_headerMetaFifo_18 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_headerMetaFifo_18 \
    op interface \
    ports { rxEng_headerMetaFifo_18_din { O 16 vector } rxEng_headerMetaFifo_18_full_n { I 1 bit } rxEng_headerMetaFifo_18_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 212 \
    name rxEng_headerMetaFifo_10 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_headerMetaFifo_10 \
    op interface \
    ports { rxEng_headerMetaFifo_10_din { O 1 vector } rxEng_headerMetaFifo_10_full_n { I 1 bit } rxEng_headerMetaFifo_10_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 213 \
    name rxEng_headerMetaFifo_19 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_headerMetaFifo_19 \
    op interface \
    ports { rxEng_headerMetaFifo_19_din { O 1 vector } rxEng_headerMetaFifo_19_full_n { I 1 bit } rxEng_headerMetaFifo_19_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 214 \
    name rxEng_headerMetaFifo_21 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_headerMetaFifo_21 \
    op interface \
    ports { rxEng_headerMetaFifo_21_din { O 1 vector } rxEng_headerMetaFifo_21_full_n { I 1 bit } rxEng_headerMetaFifo_21_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 215 \
    name rxEng_headerMetaFifo_16 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_headerMetaFifo_16 \
    op interface \
    ports { rxEng_headerMetaFifo_16_din { O 1 vector } rxEng_headerMetaFifo_16_full_n { I 1 bit } rxEng_headerMetaFifo_16_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 216 \
    name rxEng_headerMetaFifo_14 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_headerMetaFifo_14 \
    op interface \
    ports { rxEng_headerMetaFifo_14_din { O 4 vector } rxEng_headerMetaFifo_14_full_n { I 1 bit } rxEng_headerMetaFifo_14_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 217 \
    name rxEng2portTable_chec_1 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng2portTable_chec_1 \
    op interface \
    ports { rxEng2portTable_chec_1_din { O 16 vector } rxEng2portTable_chec_1_full_n { I 1 bit } rxEng2portTable_chec_1_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 218 \
    name rxEng_tupleBuffer_V \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_tupleBuffer_V \
    op interface \
    ports { rxEng_tupleBuffer_V_din { O 96 vector } rxEng_tupleBuffer_V_full_n { I 1 bit } rxEng_tupleBuffer_V_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 219 \
    name rxEng_optionalFields_4 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_optionalFields_4 \
    op interface \
    ports { rxEng_optionalFields_4_din { O 4 vector } rxEng_optionalFields_4_full_n { I 1 bit } rxEng_optionalFields_4_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 220 \
    name rxEng_optionalFields_5 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_rxEng_optionalFields_5 \
    op interface \
    ports { rxEng_optionalFields_5_din { O 1 vector } rxEng_optionalFields_5_full_n { I 1 bit } rxEng_optionalFields_5_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_start { I 1 bit } ap_ready { O 1 bit } ap_done { O 1 bit } ap_idle { O 1 bit } ap_continue { I 1 bit } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


