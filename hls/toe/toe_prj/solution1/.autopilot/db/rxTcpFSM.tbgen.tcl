set moduleName rxTcpFSM
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {rxTcpFSM}
set C_modelType { void 0 }
set C_modelArgList {
	{ rxbuffer_data_count_V int 16 regular {ap_stable 0} }
	{ rxbuffer_max_data_count_V int 16 regular {ap_stable 0} }
	{ rxEng_fsmMetaDataFif_1 int 172 regular {fifo 0 volatile } {global 0}  }
	{ rxEng2stateTable_upd_1 int 21 regular {fifo 1 volatile } {global 1}  }
	{ rxEng2rxSar_upd_req_s_18 int 119 regular {fifo 1 volatile } {global 1}  }
	{ rxEng2txSar_upd_req_s_17 int 91 regular {fifo 1 volatile } {global 1}  }
	{ stateTable2rxEng_upd_1 int 4 regular {fifo 0 volatile } {global 0}  }
	{ rxSar2rxEng_upd_rsp_s_15 int 119 regular {fifo 0 volatile } {global 0}  }
	{ txSar2rxEng_upd_rsp_s_2 int 103 regular {fifo 0 volatile } {global 0}  }
	{ rxEng2timer_clearRet_1 int 17 regular {fifo 1 volatile } {global 1}  }
	{ rxEng2timer_clearPro_1 int 16 regular {fifo 1 volatile } {global 1}  }
	{ rxEng2rxApp_notifica_1 int 81 regular {fifo 1 volatile } {global 1}  }
	{ rxEng_fsmDropFifo_V int 1 regular {fifo 1 volatile } {global 1}  }
	{ rxEng_fsmEventFifo_V int 56 regular {fifo 1 volatile } {global 1}  }
	{ rxEng2timer_setClose_1 int 16 regular {fifo 1 volatile } {global 1}  }
	{ conEstablishedFifo_V int 17 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "rxbuffer_data_count_V", "interface" : "wire", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "rxbuffer_max_data_count_V", "interface" : "wire", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "rxEng_fsmMetaDataFif_1", "interface" : "fifo", "bitwidth" : 172, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "rxEng2stateTable_upd_1", "interface" : "fifo", "bitwidth" : 21, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rxEng2rxSar_upd_req_s_18", "interface" : "fifo", "bitwidth" : 119, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rxEng2txSar_upd_req_s_17", "interface" : "fifo", "bitwidth" : 91, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "stateTable2rxEng_upd_1", "interface" : "fifo", "bitwidth" : 4, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "rxSar2rxEng_upd_rsp_s_15", "interface" : "fifo", "bitwidth" : 119, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "txSar2rxEng_upd_rsp_s_2", "interface" : "fifo", "bitwidth" : 103, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "rxEng2timer_clearRet_1", "interface" : "fifo", "bitwidth" : 17, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rxEng2timer_clearPro_1", "interface" : "fifo", "bitwidth" : 16, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rxEng2rxApp_notifica_1", "interface" : "fifo", "bitwidth" : 81, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_fsmDropFifo_V", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_fsmEventFifo_V", "interface" : "fifo", "bitwidth" : 56, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rxEng2timer_setClose_1", "interface" : "fifo", "bitwidth" : 16, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "conEstablishedFifo_V", "interface" : "fifo", "bitwidth" : 17, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 51
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ rxEng_fsmMetaDataFif_1_dout sc_in sc_lv 172 signal 2 } 
	{ rxEng_fsmMetaDataFif_1_empty_n sc_in sc_logic 1 signal 2 } 
	{ rxEng_fsmMetaDataFif_1_read sc_out sc_logic 1 signal 2 } 
	{ stateTable2rxEng_upd_1_dout sc_in sc_lv 4 signal 6 } 
	{ stateTable2rxEng_upd_1_empty_n sc_in sc_logic 1 signal 6 } 
	{ stateTable2rxEng_upd_1_read sc_out sc_logic 1 signal 6 } 
	{ rxSar2rxEng_upd_rsp_s_15_dout sc_in sc_lv 119 signal 7 } 
	{ rxSar2rxEng_upd_rsp_s_15_empty_n sc_in sc_logic 1 signal 7 } 
	{ rxSar2rxEng_upd_rsp_s_15_read sc_out sc_logic 1 signal 7 } 
	{ txSar2rxEng_upd_rsp_s_2_dout sc_in sc_lv 103 signal 8 } 
	{ txSar2rxEng_upd_rsp_s_2_empty_n sc_in sc_logic 1 signal 8 } 
	{ txSar2rxEng_upd_rsp_s_2_read sc_out sc_logic 1 signal 8 } 
	{ rxEng2stateTable_upd_1_din sc_out sc_lv 21 signal 3 } 
	{ rxEng2stateTable_upd_1_full_n sc_in sc_logic 1 signal 3 } 
	{ rxEng2stateTable_upd_1_write sc_out sc_logic 1 signal 3 } 
	{ rxEng2rxSar_upd_req_s_18_din sc_out sc_lv 119 signal 4 } 
	{ rxEng2rxSar_upd_req_s_18_full_n sc_in sc_logic 1 signal 4 } 
	{ rxEng2rxSar_upd_req_s_18_write sc_out sc_logic 1 signal 4 } 
	{ rxEng2txSar_upd_req_s_17_din sc_out sc_lv 91 signal 5 } 
	{ rxEng2txSar_upd_req_s_17_full_n sc_in sc_logic 1 signal 5 } 
	{ rxEng2txSar_upd_req_s_17_write sc_out sc_logic 1 signal 5 } 
	{ rxEng2timer_clearRet_1_din sc_out sc_lv 17 signal 9 } 
	{ rxEng2timer_clearRet_1_full_n sc_in sc_logic 1 signal 9 } 
	{ rxEng2timer_clearRet_1_write sc_out sc_logic 1 signal 9 } 
	{ rxEng_fsmEventFifo_V_din sc_out sc_lv 56 signal 13 } 
	{ rxEng_fsmEventFifo_V_full_n sc_in sc_logic 1 signal 13 } 
	{ rxEng_fsmEventFifo_V_write sc_out sc_logic 1 signal 13 } 
	{ rxEng_fsmDropFifo_V_din sc_out sc_lv 1 signal 12 } 
	{ rxEng_fsmDropFifo_V_full_n sc_in sc_logic 1 signal 12 } 
	{ rxEng_fsmDropFifo_V_write sc_out sc_logic 1 signal 12 } 
	{ rxEng2timer_clearPro_1_din sc_out sc_lv 16 signal 10 } 
	{ rxEng2timer_clearPro_1_full_n sc_in sc_logic 1 signal 10 } 
	{ rxEng2timer_clearPro_1_write sc_out sc_logic 1 signal 10 } 
	{ rxEng2rxApp_notifica_1_din sc_out sc_lv 81 signal 11 } 
	{ rxEng2rxApp_notifica_1_full_n sc_in sc_logic 1 signal 11 } 
	{ rxEng2rxApp_notifica_1_write sc_out sc_logic 1 signal 11 } 
	{ rxEng2timer_setClose_1_din sc_out sc_lv 16 signal 14 } 
	{ rxEng2timer_setClose_1_full_n sc_in sc_logic 1 signal 14 } 
	{ rxEng2timer_setClose_1_write sc_out sc_logic 1 signal 14 } 
	{ conEstablishedFifo_V_din sc_out sc_lv 17 signal 15 } 
	{ conEstablishedFifo_V_full_n sc_in sc_logic 1 signal 15 } 
	{ conEstablishedFifo_V_write sc_out sc_logic 1 signal 15 } 
	{ rxbuffer_data_count_V sc_in sc_lv 16 signal 0 } 
	{ rxbuffer_max_data_count_V sc_in sc_lv 16 signal 1 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "rxEng_fsmMetaDataFif_1_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":172, "type": "signal", "bundle":{"name": "rxEng_fsmMetaDataFif_1", "role": "dout" }} , 
 	{ "name": "rxEng_fsmMetaDataFif_1_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_fsmMetaDataFif_1", "role": "empty_n" }} , 
 	{ "name": "rxEng_fsmMetaDataFif_1_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_fsmMetaDataFif_1", "role": "read" }} , 
 	{ "name": "stateTable2rxEng_upd_1_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "stateTable2rxEng_upd_1", "role": "dout" }} , 
 	{ "name": "stateTable2rxEng_upd_1_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "stateTable2rxEng_upd_1", "role": "empty_n" }} , 
 	{ "name": "stateTable2rxEng_upd_1_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "stateTable2rxEng_upd_1", "role": "read" }} , 
 	{ "name": "rxSar2rxEng_upd_rsp_s_15_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":119, "type": "signal", "bundle":{"name": "rxSar2rxEng_upd_rsp_s_15", "role": "dout" }} , 
 	{ "name": "rxSar2rxEng_upd_rsp_s_15_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxSar2rxEng_upd_rsp_s_15", "role": "empty_n" }} , 
 	{ "name": "rxSar2rxEng_upd_rsp_s_15_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxSar2rxEng_upd_rsp_s_15", "role": "read" }} , 
 	{ "name": "txSar2rxEng_upd_rsp_s_2_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":103, "type": "signal", "bundle":{"name": "txSar2rxEng_upd_rsp_s_2", "role": "dout" }} , 
 	{ "name": "txSar2rxEng_upd_rsp_s_2_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txSar2rxEng_upd_rsp_s_2", "role": "empty_n" }} , 
 	{ "name": "txSar2rxEng_upd_rsp_s_2_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txSar2rxEng_upd_rsp_s_2", "role": "read" }} , 
 	{ "name": "rxEng2stateTable_upd_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":21, "type": "signal", "bundle":{"name": "rxEng2stateTable_upd_1", "role": "din" }} , 
 	{ "name": "rxEng2stateTable_upd_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng2stateTable_upd_1", "role": "full_n" }} , 
 	{ "name": "rxEng2stateTable_upd_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng2stateTable_upd_1", "role": "write" }} , 
 	{ "name": "rxEng2rxSar_upd_req_s_18_din", "direction": "out", "datatype": "sc_lv", "bitwidth":119, "type": "signal", "bundle":{"name": "rxEng2rxSar_upd_req_s_18", "role": "din" }} , 
 	{ "name": "rxEng2rxSar_upd_req_s_18_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng2rxSar_upd_req_s_18", "role": "full_n" }} , 
 	{ "name": "rxEng2rxSar_upd_req_s_18_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng2rxSar_upd_req_s_18", "role": "write" }} , 
 	{ "name": "rxEng2txSar_upd_req_s_17_din", "direction": "out", "datatype": "sc_lv", "bitwidth":91, "type": "signal", "bundle":{"name": "rxEng2txSar_upd_req_s_17", "role": "din" }} , 
 	{ "name": "rxEng2txSar_upd_req_s_17_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng2txSar_upd_req_s_17", "role": "full_n" }} , 
 	{ "name": "rxEng2txSar_upd_req_s_17_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng2txSar_upd_req_s_17", "role": "write" }} , 
 	{ "name": "rxEng2timer_clearRet_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":17, "type": "signal", "bundle":{"name": "rxEng2timer_clearRet_1", "role": "din" }} , 
 	{ "name": "rxEng2timer_clearRet_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng2timer_clearRet_1", "role": "full_n" }} , 
 	{ "name": "rxEng2timer_clearRet_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng2timer_clearRet_1", "role": "write" }} , 
 	{ "name": "rxEng_fsmEventFifo_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":56, "type": "signal", "bundle":{"name": "rxEng_fsmEventFifo_V", "role": "din" }} , 
 	{ "name": "rxEng_fsmEventFifo_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_fsmEventFifo_V", "role": "full_n" }} , 
 	{ "name": "rxEng_fsmEventFifo_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_fsmEventFifo_V", "role": "write" }} , 
 	{ "name": "rxEng_fsmDropFifo_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_fsmDropFifo_V", "role": "din" }} , 
 	{ "name": "rxEng_fsmDropFifo_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_fsmDropFifo_V", "role": "full_n" }} , 
 	{ "name": "rxEng_fsmDropFifo_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_fsmDropFifo_V", "role": "write" }} , 
 	{ "name": "rxEng2timer_clearPro_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "rxEng2timer_clearPro_1", "role": "din" }} , 
 	{ "name": "rxEng2timer_clearPro_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng2timer_clearPro_1", "role": "full_n" }} , 
 	{ "name": "rxEng2timer_clearPro_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng2timer_clearPro_1", "role": "write" }} , 
 	{ "name": "rxEng2rxApp_notifica_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":81, "type": "signal", "bundle":{"name": "rxEng2rxApp_notifica_1", "role": "din" }} , 
 	{ "name": "rxEng2rxApp_notifica_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng2rxApp_notifica_1", "role": "full_n" }} , 
 	{ "name": "rxEng2rxApp_notifica_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng2rxApp_notifica_1", "role": "write" }} , 
 	{ "name": "rxEng2timer_setClose_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "rxEng2timer_setClose_1", "role": "din" }} , 
 	{ "name": "rxEng2timer_setClose_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng2timer_setClose_1", "role": "full_n" }} , 
 	{ "name": "rxEng2timer_setClose_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng2timer_setClose_1", "role": "write" }} , 
 	{ "name": "conEstablishedFifo_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":17, "type": "signal", "bundle":{"name": "conEstablishedFifo_V", "role": "din" }} , 
 	{ "name": "conEstablishedFifo_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "conEstablishedFifo_V", "role": "full_n" }} , 
 	{ "name": "conEstablishedFifo_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "conEstablishedFifo_V", "role": "write" }} , 
 	{ "name": "rxbuffer_data_count_V", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "rxbuffer_data_count_V", "role": "default" }} , 
 	{ "name": "rxbuffer_max_data_count_V", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "rxbuffer_max_data_count_V", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "rxTcpFSM",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "3", "EstimateLatencyMin" : "3", "EstimateLatencyMax" : "3",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "rxbuffer_data_count_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "rxbuffer_max_data_count_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "fsm_state", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "fsm_meta_sessionID_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "fsm_meta_srcIpAddres", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "fsm_meta_dstIpPort_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "fsm_meta_meta_seqNum", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "fsm_meta_meta_ackNum", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "fsm_meta_meta_winSiz", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "fsm_meta_meta_winSca", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "fsm_meta_meta_length", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "fsm_txSarRequest", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "rxEng_fsmMetaDataFif_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_fsmMetaDataFif_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "fsm_meta_meta_ack_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "fsm_meta_meta_rst_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "fsm_meta_meta_syn_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "fsm_meta_meta_fin_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "rxEng2stateTable_upd_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng2stateTable_upd_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng2rxSar_upd_req_s_18", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng2rxSar_upd_req_s_18_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng2txSar_upd_req_s_17", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng2txSar_upd_req_s_17_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "stateTable2rxEng_upd_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "stateTable2rxEng_upd_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxSar2rxEng_upd_rsp_s_15", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxSar2rxEng_upd_rsp_s_15_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txSar2rxEng_upd_rsp_s_2", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txSar2rxEng_upd_rsp_s_2_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng2timer_clearRet_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng2timer_clearRet_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng2timer_clearPro_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng2timer_clearPro_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng2rxApp_notifica_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng2rxApp_notifica_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng_fsmDropFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_fsmDropFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng_fsmEventFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_fsmEventFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng2timer_setClose_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng2timer_setClose_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "conEstablishedFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "conEstablishedFifo_V_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	rxTcpFSM {
		rxbuffer_data_count_V {Type I LastRead 0 FirstWrite -1}
		rxbuffer_max_data_count_V {Type I LastRead 0 FirstWrite -1}
		fsm_state {Type IO LastRead -1 FirstWrite -1}
		fsm_meta_sessionID_V {Type IO LastRead -1 FirstWrite -1}
		fsm_meta_srcIpAddres {Type IO LastRead -1 FirstWrite -1}
		fsm_meta_dstIpPort_V {Type IO LastRead -1 FirstWrite -1}
		fsm_meta_meta_seqNum {Type IO LastRead -1 FirstWrite -1}
		fsm_meta_meta_ackNum {Type IO LastRead -1 FirstWrite -1}
		fsm_meta_meta_winSiz {Type IO LastRead -1 FirstWrite -1}
		fsm_meta_meta_winSca {Type IO LastRead -1 FirstWrite -1}
		fsm_meta_meta_length {Type IO LastRead -1 FirstWrite -1}
		fsm_txSarRequest {Type IO LastRead -1 FirstWrite -1}
		rxEng_fsmMetaDataFif_1 {Type I LastRead 0 FirstWrite -1}
		fsm_meta_meta_ack_V {Type IO LastRead -1 FirstWrite -1}
		fsm_meta_meta_rst_V {Type IO LastRead -1 FirstWrite -1}
		fsm_meta_meta_syn_V {Type IO LastRead -1 FirstWrite -1}
		fsm_meta_meta_fin_V {Type IO LastRead -1 FirstWrite -1}
		rxEng2stateTable_upd_1 {Type O LastRead -1 FirstWrite 3}
		rxEng2rxSar_upd_req_s_18 {Type O LastRead -1 FirstWrite 3}
		rxEng2txSar_upd_req_s_17 {Type O LastRead -1 FirstWrite 3}
		stateTable2rxEng_upd_1 {Type I LastRead 0 FirstWrite -1}
		rxSar2rxEng_upd_rsp_s_15 {Type I LastRead 0 FirstWrite -1}
		txSar2rxEng_upd_rsp_s_2 {Type I LastRead 0 FirstWrite -1}
		rxEng2timer_clearRet_1 {Type O LastRead -1 FirstWrite 3}
		rxEng2timer_clearPro_1 {Type O LastRead -1 FirstWrite 3}
		rxEng2rxApp_notifica_1 {Type O LastRead -1 FirstWrite 3}
		rxEng_fsmDropFifo_V {Type O LastRead -1 FirstWrite 3}
		rxEng_fsmEventFifo_V {Type O LastRead -1 FirstWrite 3}
		rxEng2timer_setClose_1 {Type O LastRead -1 FirstWrite 3}
		conEstablishedFifo_V {Type O LastRead -1 FirstWrite 3}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "3", "Max" : "3"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	rxbuffer_data_count_V { ap_stable {  { rxbuffer_data_count_V in_data 0 16 } } }
	rxbuffer_max_data_count_V { ap_stable {  { rxbuffer_max_data_count_V in_data 0 16 } } }
	rxEng_fsmMetaDataFif_1 { ap_fifo {  { rxEng_fsmMetaDataFif_1_dout fifo_data 0 172 }  { rxEng_fsmMetaDataFif_1_empty_n fifo_status 0 1 }  { rxEng_fsmMetaDataFif_1_read fifo_update 1 1 } } }
	rxEng2stateTable_upd_1 { ap_fifo {  { rxEng2stateTable_upd_1_din fifo_data 1 21 }  { rxEng2stateTable_upd_1_full_n fifo_status 0 1 }  { rxEng2stateTable_upd_1_write fifo_update 1 1 } } }
	rxEng2rxSar_upd_req_s_18 { ap_fifo {  { rxEng2rxSar_upd_req_s_18_din fifo_data 1 119 }  { rxEng2rxSar_upd_req_s_18_full_n fifo_status 0 1 }  { rxEng2rxSar_upd_req_s_18_write fifo_update 1 1 } } }
	rxEng2txSar_upd_req_s_17 { ap_fifo {  { rxEng2txSar_upd_req_s_17_din fifo_data 1 91 }  { rxEng2txSar_upd_req_s_17_full_n fifo_status 0 1 }  { rxEng2txSar_upd_req_s_17_write fifo_update 1 1 } } }
	stateTable2rxEng_upd_1 { ap_fifo {  { stateTable2rxEng_upd_1_dout fifo_data 0 4 }  { stateTable2rxEng_upd_1_empty_n fifo_status 0 1 }  { stateTable2rxEng_upd_1_read fifo_update 1 1 } } }
	rxSar2rxEng_upd_rsp_s_15 { ap_fifo {  { rxSar2rxEng_upd_rsp_s_15_dout fifo_data 0 119 }  { rxSar2rxEng_upd_rsp_s_15_empty_n fifo_status 0 1 }  { rxSar2rxEng_upd_rsp_s_15_read fifo_update 1 1 } } }
	txSar2rxEng_upd_rsp_s_2 { ap_fifo {  { txSar2rxEng_upd_rsp_s_2_dout fifo_data 0 103 }  { txSar2rxEng_upd_rsp_s_2_empty_n fifo_status 0 1 }  { txSar2rxEng_upd_rsp_s_2_read fifo_update 1 1 } } }
	rxEng2timer_clearRet_1 { ap_fifo {  { rxEng2timer_clearRet_1_din fifo_data 1 17 }  { rxEng2timer_clearRet_1_full_n fifo_status 0 1 }  { rxEng2timer_clearRet_1_write fifo_update 1 1 } } }
	rxEng2timer_clearPro_1 { ap_fifo {  { rxEng2timer_clearPro_1_din fifo_data 1 16 }  { rxEng2timer_clearPro_1_full_n fifo_status 0 1 }  { rxEng2timer_clearPro_1_write fifo_update 1 1 } } }
	rxEng2rxApp_notifica_1 { ap_fifo {  { rxEng2rxApp_notifica_1_din fifo_data 1 81 }  { rxEng2rxApp_notifica_1_full_n fifo_status 0 1 }  { rxEng2rxApp_notifica_1_write fifo_update 1 1 } } }
	rxEng_fsmDropFifo_V { ap_fifo {  { rxEng_fsmDropFifo_V_din fifo_data 1 1 }  { rxEng_fsmDropFifo_V_full_n fifo_status 0 1 }  { rxEng_fsmDropFifo_V_write fifo_update 1 1 } } }
	rxEng_fsmEventFifo_V { ap_fifo {  { rxEng_fsmEventFifo_V_din fifo_data 1 56 }  { rxEng_fsmEventFifo_V_full_n fifo_status 0 1 }  { rxEng_fsmEventFifo_V_write fifo_update 1 1 } } }
	rxEng2timer_setClose_1 { ap_fifo {  { rxEng2timer_setClose_1_din fifo_data 1 16 }  { rxEng2timer_setClose_1_full_n fifo_status 0 1 }  { rxEng2timer_setClose_1_write fifo_update 1 1 } } }
	conEstablishedFifo_V { ap_fifo {  { conEstablishedFifo_V_din fifo_data 1 17 }  { conEstablishedFifo_V_full_n fifo_status 0 1 }  { conEstablishedFifo_V_write fifo_update 1 1 } } }
}
