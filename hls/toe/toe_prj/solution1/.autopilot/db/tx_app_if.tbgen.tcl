set moduleName tx_app_if
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {tx_app_if}
set C_modelType { void 0 }
set C_modelArgList {
	{ appOpenConnReq_V int 48 regular {axi_s 0 volatile  { appOpenConnReq_V Data } }  }
	{ closeConnReq_V_V int 16 regular {axi_s 0 volatile  { closeConnReq_V_V Data } }  }
	{ appOpenConnRsp_V int 24 regular {axi_s 1 volatile  { appOpenConnRsp_V Data } }  }
	{ myIpAddress_V int 32 regular {ap_stable 0} }
	{ portTable2txApp_port_1 int 16 regular {fifo 0 volatile } {global 0}  }
	{ txApp2sLookup_req_V int 96 regular {fifo 1 volatile } {global 1}  }
	{ sLookup2txApp_rsp_V int 17 regular {fifo 0 volatile } {global 0}  }
	{ txApp2eventEng_merge_1 int 56 regular {fifo 1 volatile } {global 1}  }
	{ txApp2stateTable_upd_1 int 21 regular {fifo 1 volatile } {global 1}  }
	{ conEstablishedFifo_V int 17 regular {fifo 0 volatile } {global 0}  }
	{ timer2txApp_notifica_1 int 17 regular {fifo 0 volatile } {global 0}  }
	{ stateTable2txApp_upd_1 int 4 regular {fifo 0 volatile } {global 0}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "appOpenConnReq_V", "interface" : "axis", "bitwidth" : 48, "direction" : "READONLY"} , 
 	{ "Name" : "closeConnReq_V_V", "interface" : "axis", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "appOpenConnRsp_V", "interface" : "axis", "bitwidth" : 24, "direction" : "WRITEONLY"} , 
 	{ "Name" : "myIpAddress_V", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "portTable2txApp_port_1", "interface" : "fifo", "bitwidth" : 16, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "txApp2sLookup_req_V", "interface" : "fifo", "bitwidth" : 96, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "sLookup2txApp_rsp_V", "interface" : "fifo", "bitwidth" : 17, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "txApp2eventEng_merge_1", "interface" : "fifo", "bitwidth" : 56, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "txApp2stateTable_upd_1", "interface" : "fifo", "bitwidth" : 21, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "conEstablishedFifo_V", "interface" : "fifo", "bitwidth" : 17, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "timer2txApp_notifica_1", "interface" : "fifo", "bitwidth" : 17, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "stateTable2txApp_upd_1", "interface" : "fifo", "bitwidth" : 4, "direction" : "READONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 41
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ appOpenConnReq_V_TVALID sc_in sc_logic 1 invld 0 } 
	{ portTable2txApp_port_1_dout sc_in sc_lv 16 signal 4 } 
	{ portTable2txApp_port_1_empty_n sc_in sc_logic 1 signal 4 } 
	{ portTable2txApp_port_1_read sc_out sc_logic 1 signal 4 } 
	{ closeConnReq_V_V_TVALID sc_in sc_logic 1 invld 1 } 
	{ timer2txApp_notifica_1_dout sc_in sc_lv 17 signal 10 } 
	{ timer2txApp_notifica_1_empty_n sc_in sc_logic 1 signal 10 } 
	{ timer2txApp_notifica_1_read sc_out sc_logic 1 signal 10 } 
	{ conEstablishedFifo_V_dout sc_in sc_lv 17 signal 9 } 
	{ conEstablishedFifo_V_empty_n sc_in sc_logic 1 signal 9 } 
	{ conEstablishedFifo_V_read sc_out sc_logic 1 signal 9 } 
	{ sLookup2txApp_rsp_V_dout sc_in sc_lv 17 signal 6 } 
	{ sLookup2txApp_rsp_V_empty_n sc_in sc_logic 1 signal 6 } 
	{ sLookup2txApp_rsp_V_read sc_out sc_logic 1 signal 6 } 
	{ stateTable2txApp_upd_1_dout sc_in sc_lv 4 signal 11 } 
	{ stateTable2txApp_upd_1_empty_n sc_in sc_logic 1 signal 11 } 
	{ stateTable2txApp_upd_1_read sc_out sc_logic 1 signal 11 } 
	{ txApp2sLookup_req_V_din sc_out sc_lv 96 signal 5 } 
	{ txApp2sLookup_req_V_full_n sc_in sc_logic 1 signal 5 } 
	{ txApp2sLookup_req_V_write sc_out sc_logic 1 signal 5 } 
	{ appOpenConnRsp_V_TREADY sc_in sc_logic 1 outacc 2 } 
	{ txApp2stateTable_upd_1_din sc_out sc_lv 21 signal 8 } 
	{ txApp2stateTable_upd_1_full_n sc_in sc_logic 1 signal 8 } 
	{ txApp2stateTable_upd_1_write sc_out sc_logic 1 signal 8 } 
	{ txApp2eventEng_merge_1_din sc_out sc_lv 56 signal 7 } 
	{ txApp2eventEng_merge_1_full_n sc_in sc_logic 1 signal 7 } 
	{ txApp2eventEng_merge_1_write sc_out sc_logic 1 signal 7 } 
	{ appOpenConnReq_V_TDATA sc_in sc_lv 48 signal 0 } 
	{ appOpenConnReq_V_TREADY sc_out sc_logic 1 inacc 0 } 
	{ closeConnReq_V_V_TDATA sc_in sc_lv 16 signal 1 } 
	{ closeConnReq_V_V_TREADY sc_out sc_logic 1 inacc 1 } 
	{ appOpenConnRsp_V_TDATA sc_out sc_lv 24 signal 2 } 
	{ appOpenConnRsp_V_TVALID sc_out sc_logic 1 outvld 2 } 
	{ myIpAddress_V sc_in sc_lv 32 signal 3 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "appOpenConnReq_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "appOpenConnReq_V", "role": "TVALID" }} , 
 	{ "name": "portTable2txApp_port_1_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "portTable2txApp_port_1", "role": "dout" }} , 
 	{ "name": "portTable2txApp_port_1_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "portTable2txApp_port_1", "role": "empty_n" }} , 
 	{ "name": "portTable2txApp_port_1_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "portTable2txApp_port_1", "role": "read" }} , 
 	{ "name": "closeConnReq_V_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "closeConnReq_V_V", "role": "TVALID" }} , 
 	{ "name": "timer2txApp_notifica_1_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":17, "type": "signal", "bundle":{"name": "timer2txApp_notifica_1", "role": "dout" }} , 
 	{ "name": "timer2txApp_notifica_1_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "timer2txApp_notifica_1", "role": "empty_n" }} , 
 	{ "name": "timer2txApp_notifica_1_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "timer2txApp_notifica_1", "role": "read" }} , 
 	{ "name": "conEstablishedFifo_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":17, "type": "signal", "bundle":{"name": "conEstablishedFifo_V", "role": "dout" }} , 
 	{ "name": "conEstablishedFifo_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "conEstablishedFifo_V", "role": "empty_n" }} , 
 	{ "name": "conEstablishedFifo_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "conEstablishedFifo_V", "role": "read" }} , 
 	{ "name": "sLookup2txApp_rsp_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":17, "type": "signal", "bundle":{"name": "sLookup2txApp_rsp_V", "role": "dout" }} , 
 	{ "name": "sLookup2txApp_rsp_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sLookup2txApp_rsp_V", "role": "empty_n" }} , 
 	{ "name": "sLookup2txApp_rsp_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "sLookup2txApp_rsp_V", "role": "read" }} , 
 	{ "name": "stateTable2txApp_upd_1_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "stateTable2txApp_upd_1", "role": "dout" }} , 
 	{ "name": "stateTable2txApp_upd_1_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "stateTable2txApp_upd_1", "role": "empty_n" }} , 
 	{ "name": "stateTable2txApp_upd_1_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "stateTable2txApp_upd_1", "role": "read" }} , 
 	{ "name": "txApp2sLookup_req_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":96, "type": "signal", "bundle":{"name": "txApp2sLookup_req_V", "role": "din" }} , 
 	{ "name": "txApp2sLookup_req_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txApp2sLookup_req_V", "role": "full_n" }} , 
 	{ "name": "txApp2sLookup_req_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txApp2sLookup_req_V", "role": "write" }} , 
 	{ "name": "appOpenConnRsp_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "appOpenConnRsp_V", "role": "TREADY" }} , 
 	{ "name": "txApp2stateTable_upd_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":21, "type": "signal", "bundle":{"name": "txApp2stateTable_upd_1", "role": "din" }} , 
 	{ "name": "txApp2stateTable_upd_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txApp2stateTable_upd_1", "role": "full_n" }} , 
 	{ "name": "txApp2stateTable_upd_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txApp2stateTable_upd_1", "role": "write" }} , 
 	{ "name": "txApp2eventEng_merge_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":56, "type": "signal", "bundle":{"name": "txApp2eventEng_merge_1", "role": "din" }} , 
 	{ "name": "txApp2eventEng_merge_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txApp2eventEng_merge_1", "role": "full_n" }} , 
 	{ "name": "txApp2eventEng_merge_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txApp2eventEng_merge_1", "role": "write" }} , 
 	{ "name": "appOpenConnReq_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":48, "type": "signal", "bundle":{"name": "appOpenConnReq_V", "role": "TDATA" }} , 
 	{ "name": "appOpenConnReq_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "appOpenConnReq_V", "role": "TREADY" }} , 
 	{ "name": "closeConnReq_V_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "closeConnReq_V_V", "role": "TDATA" }} , 
 	{ "name": "closeConnReq_V_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "closeConnReq_V_V", "role": "TREADY" }} , 
 	{ "name": "appOpenConnRsp_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":24, "type": "signal", "bundle":{"name": "appOpenConnRsp_V", "role": "TDATA" }} , 
 	{ "name": "appOpenConnRsp_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "appOpenConnRsp_V", "role": "TVALID" }} , 
 	{ "name": "myIpAddress_V", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "myIpAddress_V", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1"],
		"CDFG" : "tx_app_if",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "appOpenConnReq_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "appOpenConnReq_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "closeConnReq_V_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "closeConnReq_V_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "appOpenConnRsp_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "appOpenConnRsp_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "myIpAddress_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "portTable2txApp_port_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "portTable2txApp_port_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txApp2sLookup_req_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txApp2sLookup_req_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tai_fsmState", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "sLookup2txApp_rsp_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "sLookup2txApp_rsp_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txApp2eventEng_merge_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txApp2eventEng_merge_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txApp2stateTable_upd_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txApp2stateTable_upd_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "conEstablishedFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "conEstablishedFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "timer2txApp_notifica_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "timer2txApp_notifica_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tai_closeSessionID_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "stateTable2txApp_upd_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "stateTable2txApp_upd_1_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_appOpenConnRsp_V_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	tx_app_if {
		appOpenConnReq_V {Type I LastRead 0 FirstWrite -1}
		closeConnReq_V_V {Type I LastRead 0 FirstWrite -1}
		appOpenConnRsp_V {Type O LastRead -1 FirstWrite 1}
		myIpAddress_V {Type I LastRead 0 FirstWrite -1}
		portTable2txApp_port_1 {Type I LastRead 0 FirstWrite -1}
		txApp2sLookup_req_V {Type O LastRead -1 FirstWrite 1}
		tai_fsmState {Type IO LastRead -1 FirstWrite -1}
		sLookup2txApp_rsp_V {Type I LastRead 0 FirstWrite -1}
		txApp2eventEng_merge_1 {Type O LastRead -1 FirstWrite 2}
		txApp2stateTable_upd_1 {Type O LastRead -1 FirstWrite 2}
		conEstablishedFifo_V {Type I LastRead 0 FirstWrite -1}
		timer2txApp_notifica_1 {Type I LastRead 0 FirstWrite -1}
		tai_closeSessionID_V {Type IO LastRead -1 FirstWrite -1}
		stateTable2txApp_upd_1 {Type I LastRead 0 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "2", "Max" : "2"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	appOpenConnReq_V { axis {  { appOpenConnReq_V_TVALID in_vld 0 1 }  { appOpenConnReq_V_TDATA in_data 0 48 }  { appOpenConnReq_V_TREADY in_acc 1 1 } } }
	closeConnReq_V_V { axis {  { closeConnReq_V_V_TVALID in_vld 0 1 }  { closeConnReq_V_V_TDATA in_data 0 16 }  { closeConnReq_V_V_TREADY in_acc 1 1 } } }
	appOpenConnRsp_V { axis {  { appOpenConnRsp_V_TREADY out_acc 0 1 }  { appOpenConnRsp_V_TDATA out_data 1 24 }  { appOpenConnRsp_V_TVALID out_vld 1 1 } } }
	myIpAddress_V { ap_stable {  { myIpAddress_V in_data 0 32 } } }
	portTable2txApp_port_1 { ap_fifo {  { portTable2txApp_port_1_dout fifo_data 0 16 }  { portTable2txApp_port_1_empty_n fifo_status 0 1 }  { portTable2txApp_port_1_read fifo_update 1 1 } } }
	txApp2sLookup_req_V { ap_fifo {  { txApp2sLookup_req_V_din fifo_data 1 96 }  { txApp2sLookup_req_V_full_n fifo_status 0 1 }  { txApp2sLookup_req_V_write fifo_update 1 1 } } }
	sLookup2txApp_rsp_V { ap_fifo {  { sLookup2txApp_rsp_V_dout fifo_data 0 17 }  { sLookup2txApp_rsp_V_empty_n fifo_status 0 1 }  { sLookup2txApp_rsp_V_read fifo_update 1 1 } } }
	txApp2eventEng_merge_1 { ap_fifo {  { txApp2eventEng_merge_1_din fifo_data 1 56 }  { txApp2eventEng_merge_1_full_n fifo_status 0 1 }  { txApp2eventEng_merge_1_write fifo_update 1 1 } } }
	txApp2stateTable_upd_1 { ap_fifo {  { txApp2stateTable_upd_1_din fifo_data 1 21 }  { txApp2stateTable_upd_1_full_n fifo_status 0 1 }  { txApp2stateTable_upd_1_write fifo_update 1 1 } } }
	conEstablishedFifo_V { ap_fifo {  { conEstablishedFifo_V_dout fifo_data 0 17 }  { conEstablishedFifo_V_empty_n fifo_status 0 1 }  { conEstablishedFifo_V_read fifo_update 1 1 } } }
	timer2txApp_notifica_1 { ap_fifo {  { timer2txApp_notifica_1_dout fifo_data 0 17 }  { timer2txApp_notifica_1_empty_n fifo_status 0 1 }  { timer2txApp_notifica_1_read fifo_update 1 1 } } }
	stateTable2txApp_upd_1 { ap_fifo {  { stateTable2txApp_upd_1_dout fifo_data 0 4 }  { stateTable2txApp_upd_1_empty_n fifo_status 0 1 }  { stateTable2txApp_upd_1_read fifo_update 1 1 } } }
}
