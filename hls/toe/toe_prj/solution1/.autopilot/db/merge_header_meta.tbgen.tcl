set moduleName merge_header_meta
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {merge_header_meta}
set C_modelType { void 0 }
set C_modelArgList {
	{ rxEng_headerMetaFifo_20 int 32 regular {fifo 0 volatile } {global 0}  }
	{ rxEng_headerMetaFifo_12 int 32 regular {fifo 0 volatile } {global 0}  }
	{ rxEng_headerMetaFifo_23 int 16 regular {fifo 0 volatile } {global 0}  }
	{ rxEng_headerMetaFifo_22 int 4 regular {fifo 0 volatile } {global 0}  }
	{ rxEng_headerMetaFifo_18 int 16 regular {fifo 0 volatile } {global 0}  }
	{ rxEng_headerMetaFifo_10 int 1 regular {fifo 0 volatile } {global 0}  }
	{ rxEng_headerMetaFifo_19 int 1 regular {fifo 0 volatile } {global 0}  }
	{ rxEng_headerMetaFifo_21 int 1 regular {fifo 0 volatile } {global 0}  }
	{ rxEng_headerMetaFifo_16 int 1 regular {fifo 0 volatile } {global 0}  }
	{ rxEng_headerMetaFifo_14 int 4 regular {fifo 0 volatile } {global 0}  }
	{ rxEng_metaDataFifo_V int 108 regular {fifo 1 volatile } {global 1}  }
	{ rxEng_winScaleFifo_V int 4 regular {fifo 0 volatile } {global 0}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "rxEng_headerMetaFifo_20", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_headerMetaFifo_12", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_headerMetaFifo_23", "interface" : "fifo", "bitwidth" : 16, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_headerMetaFifo_22", "interface" : "fifo", "bitwidth" : 4, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_headerMetaFifo_18", "interface" : "fifo", "bitwidth" : 16, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_headerMetaFifo_10", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_headerMetaFifo_19", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_headerMetaFifo_21", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_headerMetaFifo_16", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_headerMetaFifo_14", "interface" : "fifo", "bitwidth" : 4, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_metaDataFifo_V", "interface" : "fifo", "bitwidth" : 108, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rxEng_winScaleFifo_V", "interface" : "fifo", "bitwidth" : 4, "direction" : "READONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 43
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ rxEng_headerMetaFifo_20_dout sc_in sc_lv 32 signal 0 } 
	{ rxEng_headerMetaFifo_20_empty_n sc_in sc_logic 1 signal 0 } 
	{ rxEng_headerMetaFifo_20_read sc_out sc_logic 1 signal 0 } 
	{ rxEng_headerMetaFifo_12_dout sc_in sc_lv 32 signal 1 } 
	{ rxEng_headerMetaFifo_12_empty_n sc_in sc_logic 1 signal 1 } 
	{ rxEng_headerMetaFifo_12_read sc_out sc_logic 1 signal 1 } 
	{ rxEng_headerMetaFifo_23_dout sc_in sc_lv 16 signal 2 } 
	{ rxEng_headerMetaFifo_23_empty_n sc_in sc_logic 1 signal 2 } 
	{ rxEng_headerMetaFifo_23_read sc_out sc_logic 1 signal 2 } 
	{ rxEng_headerMetaFifo_22_dout sc_in sc_lv 4 signal 3 } 
	{ rxEng_headerMetaFifo_22_empty_n sc_in sc_logic 1 signal 3 } 
	{ rxEng_headerMetaFifo_22_read sc_out sc_logic 1 signal 3 } 
	{ rxEng_headerMetaFifo_18_dout sc_in sc_lv 16 signal 4 } 
	{ rxEng_headerMetaFifo_18_empty_n sc_in sc_logic 1 signal 4 } 
	{ rxEng_headerMetaFifo_18_read sc_out sc_logic 1 signal 4 } 
	{ rxEng_headerMetaFifo_10_dout sc_in sc_lv 1 signal 5 } 
	{ rxEng_headerMetaFifo_10_empty_n sc_in sc_logic 1 signal 5 } 
	{ rxEng_headerMetaFifo_10_read sc_out sc_logic 1 signal 5 } 
	{ rxEng_headerMetaFifo_19_dout sc_in sc_lv 1 signal 6 } 
	{ rxEng_headerMetaFifo_19_empty_n sc_in sc_logic 1 signal 6 } 
	{ rxEng_headerMetaFifo_19_read sc_out sc_logic 1 signal 6 } 
	{ rxEng_headerMetaFifo_21_dout sc_in sc_lv 1 signal 7 } 
	{ rxEng_headerMetaFifo_21_empty_n sc_in sc_logic 1 signal 7 } 
	{ rxEng_headerMetaFifo_21_read sc_out sc_logic 1 signal 7 } 
	{ rxEng_headerMetaFifo_16_dout sc_in sc_lv 1 signal 8 } 
	{ rxEng_headerMetaFifo_16_empty_n sc_in sc_logic 1 signal 8 } 
	{ rxEng_headerMetaFifo_16_read sc_out sc_logic 1 signal 8 } 
	{ rxEng_headerMetaFifo_14_dout sc_in sc_lv 4 signal 9 } 
	{ rxEng_headerMetaFifo_14_empty_n sc_in sc_logic 1 signal 9 } 
	{ rxEng_headerMetaFifo_14_read sc_out sc_logic 1 signal 9 } 
	{ rxEng_winScaleFifo_V_dout sc_in sc_lv 4 signal 11 } 
	{ rxEng_winScaleFifo_V_empty_n sc_in sc_logic 1 signal 11 } 
	{ rxEng_winScaleFifo_V_read sc_out sc_logic 1 signal 11 } 
	{ rxEng_metaDataFifo_V_din sc_out sc_lv 108 signal 10 } 
	{ rxEng_metaDataFifo_V_full_n sc_in sc_logic 1 signal 10 } 
	{ rxEng_metaDataFifo_V_write sc_out sc_logic 1 signal 10 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "rxEng_headerMetaFifo_20_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_20", "role": "dout" }} , 
 	{ "name": "rxEng_headerMetaFifo_20_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_20", "role": "empty_n" }} , 
 	{ "name": "rxEng_headerMetaFifo_20_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_20", "role": "read" }} , 
 	{ "name": "rxEng_headerMetaFifo_12_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_12", "role": "dout" }} , 
 	{ "name": "rxEng_headerMetaFifo_12_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_12", "role": "empty_n" }} , 
 	{ "name": "rxEng_headerMetaFifo_12_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_12", "role": "read" }} , 
 	{ "name": "rxEng_headerMetaFifo_23_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_23", "role": "dout" }} , 
 	{ "name": "rxEng_headerMetaFifo_23_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_23", "role": "empty_n" }} , 
 	{ "name": "rxEng_headerMetaFifo_23_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_23", "role": "read" }} , 
 	{ "name": "rxEng_headerMetaFifo_22_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_22", "role": "dout" }} , 
 	{ "name": "rxEng_headerMetaFifo_22_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_22", "role": "empty_n" }} , 
 	{ "name": "rxEng_headerMetaFifo_22_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_22", "role": "read" }} , 
 	{ "name": "rxEng_headerMetaFifo_18_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_18", "role": "dout" }} , 
 	{ "name": "rxEng_headerMetaFifo_18_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_18", "role": "empty_n" }} , 
 	{ "name": "rxEng_headerMetaFifo_18_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_18", "role": "read" }} , 
 	{ "name": "rxEng_headerMetaFifo_10_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_10", "role": "dout" }} , 
 	{ "name": "rxEng_headerMetaFifo_10_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_10", "role": "empty_n" }} , 
 	{ "name": "rxEng_headerMetaFifo_10_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_10", "role": "read" }} , 
 	{ "name": "rxEng_headerMetaFifo_19_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_19", "role": "dout" }} , 
 	{ "name": "rxEng_headerMetaFifo_19_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_19", "role": "empty_n" }} , 
 	{ "name": "rxEng_headerMetaFifo_19_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_19", "role": "read" }} , 
 	{ "name": "rxEng_headerMetaFifo_21_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_21", "role": "dout" }} , 
 	{ "name": "rxEng_headerMetaFifo_21_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_21", "role": "empty_n" }} , 
 	{ "name": "rxEng_headerMetaFifo_21_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_21", "role": "read" }} , 
 	{ "name": "rxEng_headerMetaFifo_16_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_16", "role": "dout" }} , 
 	{ "name": "rxEng_headerMetaFifo_16_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_16", "role": "empty_n" }} , 
 	{ "name": "rxEng_headerMetaFifo_16_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_16", "role": "read" }} , 
 	{ "name": "rxEng_headerMetaFifo_14_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_14", "role": "dout" }} , 
 	{ "name": "rxEng_headerMetaFifo_14_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_14", "role": "empty_n" }} , 
 	{ "name": "rxEng_headerMetaFifo_14_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_headerMetaFifo_14", "role": "read" }} , 
 	{ "name": "rxEng_winScaleFifo_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "rxEng_winScaleFifo_V", "role": "dout" }} , 
 	{ "name": "rxEng_winScaleFifo_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_winScaleFifo_V", "role": "empty_n" }} , 
 	{ "name": "rxEng_winScaleFifo_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_winScaleFifo_V", "role": "read" }} , 
 	{ "name": "rxEng_metaDataFifo_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":108, "type": "signal", "bundle":{"name": "rxEng_metaDataFifo_V", "role": "din" }} , 
 	{ "name": "rxEng_metaDataFifo_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_metaDataFifo_V", "role": "full_n" }} , 
 	{ "name": "rxEng_metaDataFifo_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxEng_metaDataFifo_V", "role": "write" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "merge_header_meta",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "state_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "rxEng_headerMetaFifo_20", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_headerMetaFifo_20_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng_headerMetaFifo_12", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_headerMetaFifo_12_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng_headerMetaFifo_23", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_headerMetaFifo_23_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng_headerMetaFifo_22", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_headerMetaFifo_22_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng_headerMetaFifo_18", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_headerMetaFifo_18_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng_headerMetaFifo_10", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_headerMetaFifo_10_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng_headerMetaFifo_19", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_headerMetaFifo_19_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng_headerMetaFifo_21", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_headerMetaFifo_21_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng_headerMetaFifo_16", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_headerMetaFifo_16_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng_headerMetaFifo_14", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_headerMetaFifo_14_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "meta_seqNumb_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "meta_ackNumb_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "meta_winSize_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "meta_length_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "meta_ack_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "meta_rst_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "meta_syn_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "meta_fin_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "meta_dataOffset_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "rxEng_metaDataFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_metaDataFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxEng_winScaleFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxEng_winScaleFifo_V_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	merge_header_meta {
		state_V {Type IO LastRead -1 FirstWrite -1}
		rxEng_headerMetaFifo_20 {Type I LastRead 0 FirstWrite -1}
		rxEng_headerMetaFifo_12 {Type I LastRead 0 FirstWrite -1}
		rxEng_headerMetaFifo_23 {Type I LastRead 0 FirstWrite -1}
		rxEng_headerMetaFifo_22 {Type I LastRead 0 FirstWrite -1}
		rxEng_headerMetaFifo_18 {Type I LastRead 0 FirstWrite -1}
		rxEng_headerMetaFifo_10 {Type I LastRead 0 FirstWrite -1}
		rxEng_headerMetaFifo_19 {Type I LastRead 0 FirstWrite -1}
		rxEng_headerMetaFifo_21 {Type I LastRead 0 FirstWrite -1}
		rxEng_headerMetaFifo_16 {Type I LastRead 0 FirstWrite -1}
		rxEng_headerMetaFifo_14 {Type I LastRead 0 FirstWrite -1}
		meta_seqNumb_V {Type IO LastRead -1 FirstWrite -1}
		meta_ackNumb_V {Type IO LastRead -1 FirstWrite -1}
		meta_winSize_V {Type IO LastRead -1 FirstWrite -1}
		meta_length_V {Type IO LastRead -1 FirstWrite -1}
		meta_ack_V {Type IO LastRead -1 FirstWrite -1}
		meta_rst_V {Type IO LastRead -1 FirstWrite -1}
		meta_syn_V {Type IO LastRead -1 FirstWrite -1}
		meta_fin_V {Type IO LastRead -1 FirstWrite -1}
		meta_dataOffset_V {Type IO LastRead -1 FirstWrite -1}
		rxEng_metaDataFifo_V {Type O LastRead -1 FirstWrite 1}
		rxEng_winScaleFifo_V {Type I LastRead 0 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "1", "Max" : "1"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	rxEng_headerMetaFifo_20 { ap_fifo {  { rxEng_headerMetaFifo_20_dout fifo_data 0 32 }  { rxEng_headerMetaFifo_20_empty_n fifo_status 0 1 }  { rxEng_headerMetaFifo_20_read fifo_update 1 1 } } }
	rxEng_headerMetaFifo_12 { ap_fifo {  { rxEng_headerMetaFifo_12_dout fifo_data 0 32 }  { rxEng_headerMetaFifo_12_empty_n fifo_status 0 1 }  { rxEng_headerMetaFifo_12_read fifo_update 1 1 } } }
	rxEng_headerMetaFifo_23 { ap_fifo {  { rxEng_headerMetaFifo_23_dout fifo_data 0 16 }  { rxEng_headerMetaFifo_23_empty_n fifo_status 0 1 }  { rxEng_headerMetaFifo_23_read fifo_update 1 1 } } }
	rxEng_headerMetaFifo_22 { ap_fifo {  { rxEng_headerMetaFifo_22_dout fifo_data 0 4 }  { rxEng_headerMetaFifo_22_empty_n fifo_status 0 1 }  { rxEng_headerMetaFifo_22_read fifo_update 1 1 } } }
	rxEng_headerMetaFifo_18 { ap_fifo {  { rxEng_headerMetaFifo_18_dout fifo_data 0 16 }  { rxEng_headerMetaFifo_18_empty_n fifo_status 0 1 }  { rxEng_headerMetaFifo_18_read fifo_update 1 1 } } }
	rxEng_headerMetaFifo_10 { ap_fifo {  { rxEng_headerMetaFifo_10_dout fifo_data 0 1 }  { rxEng_headerMetaFifo_10_empty_n fifo_status 0 1 }  { rxEng_headerMetaFifo_10_read fifo_update 1 1 } } }
	rxEng_headerMetaFifo_19 { ap_fifo {  { rxEng_headerMetaFifo_19_dout fifo_data 0 1 }  { rxEng_headerMetaFifo_19_empty_n fifo_status 0 1 }  { rxEng_headerMetaFifo_19_read fifo_update 1 1 } } }
	rxEng_headerMetaFifo_21 { ap_fifo {  { rxEng_headerMetaFifo_21_dout fifo_data 0 1 }  { rxEng_headerMetaFifo_21_empty_n fifo_status 0 1 }  { rxEng_headerMetaFifo_21_read fifo_update 1 1 } } }
	rxEng_headerMetaFifo_16 { ap_fifo {  { rxEng_headerMetaFifo_16_dout fifo_data 0 1 }  { rxEng_headerMetaFifo_16_empty_n fifo_status 0 1 }  { rxEng_headerMetaFifo_16_read fifo_update 1 1 } } }
	rxEng_headerMetaFifo_14 { ap_fifo {  { rxEng_headerMetaFifo_14_dout fifo_data 0 4 }  { rxEng_headerMetaFifo_14_empty_n fifo_status 0 1 }  { rxEng_headerMetaFifo_14_read fifo_update 1 1 } } }
	rxEng_metaDataFifo_V { ap_fifo {  { rxEng_metaDataFifo_V_din fifo_data 1 108 }  { rxEng_metaDataFifo_V_full_n fifo_status 0 1 }  { rxEng_metaDataFifo_V_write fifo_update 1 1 } } }
	rxEng_winScaleFifo_V { ap_fifo {  { rxEng_winScaleFifo_V_dout fifo_data 0 4 }  { rxEng_winScaleFifo_V_empty_n fifo_status 0 1 }  { rxEng_winScaleFifo_V_read fifo_update 1 1 } } }
}
