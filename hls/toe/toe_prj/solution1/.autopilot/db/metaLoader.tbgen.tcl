set moduleName metaLoader
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {metaLoader}
set C_modelType { void 0 }
set C_modelArgList {
	{ eventEng2txEng_event_1 int 152 regular {fifo 0 volatile } {global 0}  }
	{ txEngFifoReadCount_V int 1 regular {fifo 1 volatile } {global 1}  }
	{ txEng2rxSar_req_V_V int 16 regular {fifo 1 volatile } {global 1}  }
	{ txEng2txSar_upd_req_s_10 int 53 regular {fifo 1 volatile } {global 1}  }
	{ rxSar2txEng_rsp_V int 70 regular {fifo 0 volatile } {global 0}  }
	{ txSar2txEng_upd_rsp_s_0 int 124 regular {fifo 0 volatile } {global 0}  }
	{ txEng2timer_setProbe_1 int 16 regular {fifo 1 volatile } {global 1}  }
	{ txEng_ipMetaFifo_V_V int 16 regular {fifo 1 volatile } {global 1}  }
	{ txEng_tcpMetaFifo_V int 104 regular {fifo 1 volatile } {global 1}  }
	{ txEng_isLookUpFifo_V int 1 regular {fifo 1 volatile } {global 1}  }
	{ txEng_isDDRbypass_V int 1 regular {fifo 1 volatile } {global 1}  }
	{ txEng2sLookup_rev_re_1 int 16 regular {fifo 1 volatile } {global 1}  }
	{ txEng2timer_setRetra_1 int 19 regular {fifo 1 volatile } {global 1}  }
	{ txMetaloader2memAcce_1 int 72 regular {fifo 1 volatile } {global 1}  }
	{ txEng_tupleShortCutF_1 int 96 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "eventEng2txEng_event_1", "interface" : "fifo", "bitwidth" : 152, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "txEngFifoReadCount_V", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "txEng2rxSar_req_V_V", "interface" : "fifo", "bitwidth" : 16, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "txEng2txSar_upd_req_s_10", "interface" : "fifo", "bitwidth" : 53, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rxSar2txEng_rsp_V", "interface" : "fifo", "bitwidth" : 70, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "txSar2txEng_upd_rsp_s_0", "interface" : "fifo", "bitwidth" : 124, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "txEng2timer_setProbe_1", "interface" : "fifo", "bitwidth" : 16, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "txEng_ipMetaFifo_V_V", "interface" : "fifo", "bitwidth" : 16, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "txEng_tcpMetaFifo_V", "interface" : "fifo", "bitwidth" : 104, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "txEng_isLookUpFifo_V", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "txEng_isDDRbypass_V", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "txEng2sLookup_rev_re_1", "interface" : "fifo", "bitwidth" : 16, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "txEng2timer_setRetra_1", "interface" : "fifo", "bitwidth" : 19, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "txMetaloader2memAcce_1", "interface" : "fifo", "bitwidth" : 72, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "txEng_tupleShortCutF_1", "interface" : "fifo", "bitwidth" : 96, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 52
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ eventEng2txEng_event_1_dout sc_in sc_lv 152 signal 0 } 
	{ eventEng2txEng_event_1_empty_n sc_in sc_logic 1 signal 0 } 
	{ eventEng2txEng_event_1_read sc_out sc_logic 1 signal 0 } 
	{ txSar2txEng_upd_rsp_s_0_dout sc_in sc_lv 124 signal 5 } 
	{ txSar2txEng_upd_rsp_s_0_empty_n sc_in sc_logic 1 signal 5 } 
	{ txSar2txEng_upd_rsp_s_0_read sc_out sc_logic 1 signal 5 } 
	{ rxSar2txEng_rsp_V_dout sc_in sc_lv 70 signal 4 } 
	{ rxSar2txEng_rsp_V_empty_n sc_in sc_logic 1 signal 4 } 
	{ rxSar2txEng_rsp_V_read sc_out sc_logic 1 signal 4 } 
	{ txEngFifoReadCount_V_din sc_out sc_lv 1 signal 1 } 
	{ txEngFifoReadCount_V_full_n sc_in sc_logic 1 signal 1 } 
	{ txEngFifoReadCount_V_write sc_out sc_logic 1 signal 1 } 
	{ txEng2txSar_upd_req_s_10_din sc_out sc_lv 53 signal 3 } 
	{ txEng2txSar_upd_req_s_10_full_n sc_in sc_logic 1 signal 3 } 
	{ txEng2txSar_upd_req_s_10_write sc_out sc_logic 1 signal 3 } 
	{ txEng2rxSar_req_V_V_din sc_out sc_lv 16 signal 2 } 
	{ txEng2rxSar_req_V_V_full_n sc_in sc_logic 1 signal 2 } 
	{ txEng2rxSar_req_V_V_write sc_out sc_logic 1 signal 2 } 
	{ txEng_ipMetaFifo_V_V_din sc_out sc_lv 16 signal 7 } 
	{ txEng_ipMetaFifo_V_V_full_n sc_in sc_logic 1 signal 7 } 
	{ txEng_ipMetaFifo_V_V_write sc_out sc_logic 1 signal 7 } 
	{ txEng_isLookUpFifo_V_din sc_out sc_lv 1 signal 9 } 
	{ txEng_isLookUpFifo_V_full_n sc_in sc_logic 1 signal 9 } 
	{ txEng_isLookUpFifo_V_write sc_out sc_logic 1 signal 9 } 
	{ txEng2sLookup_rev_re_1_din sc_out sc_lv 16 signal 11 } 
	{ txEng2sLookup_rev_re_1_full_n sc_in sc_logic 1 signal 11 } 
	{ txEng2sLookup_rev_re_1_write sc_out sc_logic 1 signal 11 } 
	{ txEng_tcpMetaFifo_V_din sc_out sc_lv 104 signal 8 } 
	{ txEng_tcpMetaFifo_V_full_n sc_in sc_logic 1 signal 8 } 
	{ txEng_tcpMetaFifo_V_write sc_out sc_logic 1 signal 8 } 
	{ txEng_tupleShortCutF_1_din sc_out sc_lv 96 signal 14 } 
	{ txEng_tupleShortCutF_1_full_n sc_in sc_logic 1 signal 14 } 
	{ txEng_tupleShortCutF_1_write sc_out sc_logic 1 signal 14 } 
	{ txEng2timer_setRetra_1_din sc_out sc_lv 19 signal 12 } 
	{ txEng2timer_setRetra_1_full_n sc_in sc_logic 1 signal 12 } 
	{ txEng2timer_setRetra_1_write sc_out sc_logic 1 signal 12 } 
	{ txMetaloader2memAcce_1_din sc_out sc_lv 72 signal 13 } 
	{ txMetaloader2memAcce_1_full_n sc_in sc_logic 1 signal 13 } 
	{ txMetaloader2memAcce_1_write sc_out sc_logic 1 signal 13 } 
	{ txEng_isDDRbypass_V_din sc_out sc_lv 1 signal 10 } 
	{ txEng_isDDRbypass_V_full_n sc_in sc_logic 1 signal 10 } 
	{ txEng_isDDRbypass_V_write sc_out sc_logic 1 signal 10 } 
	{ txEng2timer_setProbe_1_din sc_out sc_lv 16 signal 6 } 
	{ txEng2timer_setProbe_1_full_n sc_in sc_logic 1 signal 6 } 
	{ txEng2timer_setProbe_1_write sc_out sc_logic 1 signal 6 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "eventEng2txEng_event_1_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":152, "type": "signal", "bundle":{"name": "eventEng2txEng_event_1", "role": "dout" }} , 
 	{ "name": "eventEng2txEng_event_1_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "eventEng2txEng_event_1", "role": "empty_n" }} , 
 	{ "name": "eventEng2txEng_event_1_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "eventEng2txEng_event_1", "role": "read" }} , 
 	{ "name": "txSar2txEng_upd_rsp_s_0_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":124, "type": "signal", "bundle":{"name": "txSar2txEng_upd_rsp_s_0", "role": "dout" }} , 
 	{ "name": "txSar2txEng_upd_rsp_s_0_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txSar2txEng_upd_rsp_s_0", "role": "empty_n" }} , 
 	{ "name": "txSar2txEng_upd_rsp_s_0_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txSar2txEng_upd_rsp_s_0", "role": "read" }} , 
 	{ "name": "rxSar2txEng_rsp_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":70, "type": "signal", "bundle":{"name": "rxSar2txEng_rsp_V", "role": "dout" }} , 
 	{ "name": "rxSar2txEng_rsp_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxSar2txEng_rsp_V", "role": "empty_n" }} , 
 	{ "name": "rxSar2txEng_rsp_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rxSar2txEng_rsp_V", "role": "read" }} , 
 	{ "name": "txEngFifoReadCount_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "txEngFifoReadCount_V", "role": "din" }} , 
 	{ "name": "txEngFifoReadCount_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEngFifoReadCount_V", "role": "full_n" }} , 
 	{ "name": "txEngFifoReadCount_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEngFifoReadCount_V", "role": "write" }} , 
 	{ "name": "txEng2txSar_upd_req_s_10_din", "direction": "out", "datatype": "sc_lv", "bitwidth":53, "type": "signal", "bundle":{"name": "txEng2txSar_upd_req_s_10", "role": "din" }} , 
 	{ "name": "txEng2txSar_upd_req_s_10_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng2txSar_upd_req_s_10", "role": "full_n" }} , 
 	{ "name": "txEng2txSar_upd_req_s_10_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng2txSar_upd_req_s_10", "role": "write" }} , 
 	{ "name": "txEng2rxSar_req_V_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "txEng2rxSar_req_V_V", "role": "din" }} , 
 	{ "name": "txEng2rxSar_req_V_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng2rxSar_req_V_V", "role": "full_n" }} , 
 	{ "name": "txEng2rxSar_req_V_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng2rxSar_req_V_V", "role": "write" }} , 
 	{ "name": "txEng_ipMetaFifo_V_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "txEng_ipMetaFifo_V_V", "role": "din" }} , 
 	{ "name": "txEng_ipMetaFifo_V_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng_ipMetaFifo_V_V", "role": "full_n" }} , 
 	{ "name": "txEng_ipMetaFifo_V_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng_ipMetaFifo_V_V", "role": "write" }} , 
 	{ "name": "txEng_isLookUpFifo_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng_isLookUpFifo_V", "role": "din" }} , 
 	{ "name": "txEng_isLookUpFifo_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng_isLookUpFifo_V", "role": "full_n" }} , 
 	{ "name": "txEng_isLookUpFifo_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng_isLookUpFifo_V", "role": "write" }} , 
 	{ "name": "txEng2sLookup_rev_re_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "txEng2sLookup_rev_re_1", "role": "din" }} , 
 	{ "name": "txEng2sLookup_rev_re_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng2sLookup_rev_re_1", "role": "full_n" }} , 
 	{ "name": "txEng2sLookup_rev_re_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng2sLookup_rev_re_1", "role": "write" }} , 
 	{ "name": "txEng_tcpMetaFifo_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":104, "type": "signal", "bundle":{"name": "txEng_tcpMetaFifo_V", "role": "din" }} , 
 	{ "name": "txEng_tcpMetaFifo_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng_tcpMetaFifo_V", "role": "full_n" }} , 
 	{ "name": "txEng_tcpMetaFifo_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng_tcpMetaFifo_V", "role": "write" }} , 
 	{ "name": "txEng_tupleShortCutF_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":96, "type": "signal", "bundle":{"name": "txEng_tupleShortCutF_1", "role": "din" }} , 
 	{ "name": "txEng_tupleShortCutF_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng_tupleShortCutF_1", "role": "full_n" }} , 
 	{ "name": "txEng_tupleShortCutF_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng_tupleShortCutF_1", "role": "write" }} , 
 	{ "name": "txEng2timer_setRetra_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":19, "type": "signal", "bundle":{"name": "txEng2timer_setRetra_1", "role": "din" }} , 
 	{ "name": "txEng2timer_setRetra_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng2timer_setRetra_1", "role": "full_n" }} , 
 	{ "name": "txEng2timer_setRetra_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng2timer_setRetra_1", "role": "write" }} , 
 	{ "name": "txMetaloader2memAcce_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":72, "type": "signal", "bundle":{"name": "txMetaloader2memAcce_1", "role": "din" }} , 
 	{ "name": "txMetaloader2memAcce_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txMetaloader2memAcce_1", "role": "full_n" }} , 
 	{ "name": "txMetaloader2memAcce_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txMetaloader2memAcce_1", "role": "write" }} , 
 	{ "name": "txEng_isDDRbypass_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng_isDDRbypass_V", "role": "din" }} , 
 	{ "name": "txEng_isDDRbypass_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng_isDDRbypass_V", "role": "full_n" }} , 
 	{ "name": "txEng_isDDRbypass_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng_isDDRbypass_V", "role": "write" }} , 
 	{ "name": "txEng2timer_setProbe_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "txEng2timer_setProbe_1", "role": "din" }} , 
 	{ "name": "txEng2timer_setProbe_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng2timer_setProbe_1", "role": "full_n" }} , 
 	{ "name": "txEng2timer_setProbe_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txEng2timer_setProbe_1", "role": "write" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "metaLoader",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "3", "EstimateLatencyMin" : "3", "EstimateLatencyMax" : "3",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "ml_FsmState_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ml_curEvent_sessionI", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ml_curEvent_length_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ml_curEvent_rt_count", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ml_sarLoaded", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ml_randomValue_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ml_segmentCount_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "eventEng2txEng_event_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "eventEng2txEng_event_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ml_curEvent_type", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ml_curEvent_address_s", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ml_curEvent_tuple_sr_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ml_curEvent_tuple_ds_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ml_curEvent_tuple_sr", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ml_curEvent_tuple_ds", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "txEngFifoReadCount_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txEngFifoReadCount_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txEng2rxSar_req_V_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txEng2rxSar_req_V_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txEng2txSar_upd_req_s_10", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txEng2txSar_upd_req_s_10_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxSar_recvd_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "rxSar_windowSize_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "meta_win_shift_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "txSarReg_not_ackd_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "rxSar2txEng_rsp_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rxSar2txEng_rsp_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txSar2txEng_upd_rsp_s_0", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txSar2txEng_upd_rsp_s_0_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txEng2timer_setProbe_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txEng2timer_setProbe_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txEng_ipMetaFifo_V_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txEng_ipMetaFifo_V_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txEng_tcpMetaFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txEng_tcpMetaFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txEng_isLookUpFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txEng_isLookUpFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txEng_isDDRbypass_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txEng_isDDRbypass_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txEng2sLookup_rev_re_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txEng2sLookup_rev_re_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txEng2timer_setRetra_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txEng2timer_setRetra_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txSarReg_ackd_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "txSarReg_usableWindo", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "txSarReg_app_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "txSarReg_usedLength_s", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "txSarReg_finReady", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "txSarReg_finSent", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "txSarReg_win_shift_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "txMetaloader2memAcce_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txMetaloader2memAcce_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txEng_tupleShortCutF_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txEng_tupleShortCutF_1_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	metaLoader {
		ml_FsmState_V {Type IO LastRead -1 FirstWrite -1}
		ml_curEvent_sessionI {Type IO LastRead -1 FirstWrite -1}
		ml_curEvent_length_V {Type IO LastRead -1 FirstWrite -1}
		ml_curEvent_rt_count {Type IO LastRead -1 FirstWrite -1}
		ml_sarLoaded {Type IO LastRead -1 FirstWrite -1}
		ml_randomValue_V {Type IO LastRead -1 FirstWrite -1}
		ml_segmentCount_V {Type IO LastRead -1 FirstWrite -1}
		eventEng2txEng_event_1 {Type I LastRead 1 FirstWrite -1}
		ml_curEvent_type {Type IO LastRead -1 FirstWrite -1}
		ml_curEvent_address_s {Type IO LastRead -1 FirstWrite -1}
		ml_curEvent_tuple_sr_1 {Type IO LastRead -1 FirstWrite -1}
		ml_curEvent_tuple_ds_1 {Type IO LastRead -1 FirstWrite -1}
		ml_curEvent_tuple_sr {Type IO LastRead -1 FirstWrite -1}
		ml_curEvent_tuple_ds {Type IO LastRead -1 FirstWrite -1}
		txEngFifoReadCount_V {Type O LastRead -1 FirstWrite 2}
		txEng2rxSar_req_V_V {Type O LastRead -1 FirstWrite 3}
		txEng2txSar_upd_req_s_10 {Type O LastRead -1 FirstWrite 3}
		rxSar_recvd_V {Type IO LastRead -1 FirstWrite -1}
		rxSar_windowSize_V {Type IO LastRead -1 FirstWrite -1}
		meta_win_shift_V {Type IO LastRead -1 FirstWrite -1}
		txSarReg_not_ackd_V {Type IO LastRead -1 FirstWrite -1}
		rxSar2txEng_rsp_V {Type I LastRead 1 FirstWrite -1}
		txSar2txEng_upd_rsp_s_0 {Type I LastRead 1 FirstWrite -1}
		txEng2timer_setProbe_1 {Type O LastRead -1 FirstWrite 3}
		txEng_ipMetaFifo_V_V {Type O LastRead -1 FirstWrite 3}
		txEng_tcpMetaFifo_V {Type O LastRead -1 FirstWrite 3}
		txEng_isLookUpFifo_V {Type O LastRead -1 FirstWrite 3}
		txEng_isDDRbypass_V {Type O LastRead -1 FirstWrite 3}
		txEng2sLookup_rev_re_1 {Type O LastRead -1 FirstWrite 3}
		txEng2timer_setRetra_1 {Type O LastRead -1 FirstWrite 3}
		txSarReg_ackd_V {Type IO LastRead -1 FirstWrite -1}
		txSarReg_usableWindo {Type IO LastRead -1 FirstWrite -1}
		txSarReg_app_V {Type IO LastRead -1 FirstWrite -1}
		txSarReg_usedLength_s {Type IO LastRead -1 FirstWrite -1}
		txSarReg_finReady {Type IO LastRead -1 FirstWrite -1}
		txSarReg_finSent {Type IO LastRead -1 FirstWrite -1}
		txSarReg_win_shift_V {Type IO LastRead -1 FirstWrite -1}
		txMetaloader2memAcce_1 {Type O LastRead -1 FirstWrite 3}
		txEng_tupleShortCutF_1 {Type O LastRead -1 FirstWrite 3}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "3", "Max" : "3"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	eventEng2txEng_event_1 { ap_fifo {  { eventEng2txEng_event_1_dout fifo_data 0 152 }  { eventEng2txEng_event_1_empty_n fifo_status 0 1 }  { eventEng2txEng_event_1_read fifo_update 1 1 } } }
	txEngFifoReadCount_V { ap_fifo {  { txEngFifoReadCount_V_din fifo_data 1 1 }  { txEngFifoReadCount_V_full_n fifo_status 0 1 }  { txEngFifoReadCount_V_write fifo_update 1 1 } } }
	txEng2rxSar_req_V_V { ap_fifo {  { txEng2rxSar_req_V_V_din fifo_data 1 16 }  { txEng2rxSar_req_V_V_full_n fifo_status 0 1 }  { txEng2rxSar_req_V_V_write fifo_update 1 1 } } }
	txEng2txSar_upd_req_s_10 { ap_fifo {  { txEng2txSar_upd_req_s_10_din fifo_data 1 53 }  { txEng2txSar_upd_req_s_10_full_n fifo_status 0 1 }  { txEng2txSar_upd_req_s_10_write fifo_update 1 1 } } }
	rxSar2txEng_rsp_V { ap_fifo {  { rxSar2txEng_rsp_V_dout fifo_data 0 70 }  { rxSar2txEng_rsp_V_empty_n fifo_status 0 1 }  { rxSar2txEng_rsp_V_read fifo_update 1 1 } } }
	txSar2txEng_upd_rsp_s_0 { ap_fifo {  { txSar2txEng_upd_rsp_s_0_dout fifo_data 0 124 }  { txSar2txEng_upd_rsp_s_0_empty_n fifo_status 0 1 }  { txSar2txEng_upd_rsp_s_0_read fifo_update 1 1 } } }
	txEng2timer_setProbe_1 { ap_fifo {  { txEng2timer_setProbe_1_din fifo_data 1 16 }  { txEng2timer_setProbe_1_full_n fifo_status 0 1 }  { txEng2timer_setProbe_1_write fifo_update 1 1 } } }
	txEng_ipMetaFifo_V_V { ap_fifo {  { txEng_ipMetaFifo_V_V_din fifo_data 1 16 }  { txEng_ipMetaFifo_V_V_full_n fifo_status 0 1 }  { txEng_ipMetaFifo_V_V_write fifo_update 1 1 } } }
	txEng_tcpMetaFifo_V { ap_fifo {  { txEng_tcpMetaFifo_V_din fifo_data 1 104 }  { txEng_tcpMetaFifo_V_full_n fifo_status 0 1 }  { txEng_tcpMetaFifo_V_write fifo_update 1 1 } } }
	txEng_isLookUpFifo_V { ap_fifo {  { txEng_isLookUpFifo_V_din fifo_data 1 1 }  { txEng_isLookUpFifo_V_full_n fifo_status 0 1 }  { txEng_isLookUpFifo_V_write fifo_update 1 1 } } }
	txEng_isDDRbypass_V { ap_fifo {  { txEng_isDDRbypass_V_din fifo_data 1 1 }  { txEng_isDDRbypass_V_full_n fifo_status 0 1 }  { txEng_isDDRbypass_V_write fifo_update 1 1 } } }
	txEng2sLookup_rev_re_1 { ap_fifo {  { txEng2sLookup_rev_re_1_din fifo_data 1 16 }  { txEng2sLookup_rev_re_1_full_n fifo_status 0 1 }  { txEng2sLookup_rev_re_1_write fifo_update 1 1 } } }
	txEng2timer_setRetra_1 { ap_fifo {  { txEng2timer_setRetra_1_din fifo_data 1 19 }  { txEng2timer_setRetra_1_full_n fifo_status 0 1 }  { txEng2timer_setRetra_1_write fifo_update 1 1 } } }
	txMetaloader2memAcce_1 { ap_fifo {  { txMetaloader2memAcce_1_din fifo_data 1 72 }  { txMetaloader2memAcce_1_full_n fifo_status 0 1 }  { txMetaloader2memAcce_1_write fifo_update 1 1 } } }
	txEng_tupleShortCutF_1 { ap_fifo {  { txEng_tupleShortCutF_1_din fifo_data 1 96 }  { txEng_tupleShortCutF_1_full_n fifo_status 0 1 }  { txEng_tupleShortCutF_1_write fifo_update 1 1 } } }
}
