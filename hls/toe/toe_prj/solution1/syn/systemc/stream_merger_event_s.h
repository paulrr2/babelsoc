// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
// Version: 2019.2.1
// Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

#ifndef _stream_merger_event_s_HH_
#define _stream_merger_event_s_HH_

#include "systemc.h"
#include "AESL_pkg.h"


namespace ap_rtl {

struct stream_merger_event_s : public sc_module {
    // Port declarations 16
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_in< sc_logic > ap_start;
    sc_out< sc_logic > ap_done;
    sc_in< sc_logic > ap_continue;
    sc_out< sc_logic > ap_idle;
    sc_out< sc_logic > ap_ready;
    sc_in< sc_lv<56> > rtTimer2eventEng_set_1_dout;
    sc_in< sc_logic > rtTimer2eventEng_set_1_empty_n;
    sc_out< sc_logic > rtTimer2eventEng_set_1_read;
    sc_in< sc_lv<56> > probeTimer2eventEng_1_dout;
    sc_in< sc_logic > probeTimer2eventEng_1_empty_n;
    sc_out< sc_logic > probeTimer2eventEng_1_read;
    sc_out< sc_lv<56> > timer2eventEng_setEv_1_din;
    sc_in< sc_logic > timer2eventEng_setEv_1_full_n;
    sc_out< sc_logic > timer2eventEng_setEv_1_write;


    // Module declarations
    stream_merger_event_s(sc_module_name name);
    SC_HAS_PROCESS(stream_merger_event_s);

    ~stream_merger_event_s();

    sc_trace_file* mVcdFile;

    sc_signal< sc_logic > ap_done_reg;
    sc_signal< sc_lv<1> > ap_CS_fsm;
    sc_signal< sc_logic > ap_CS_fsm_pp0_stage0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter1;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter2;
    sc_signal< sc_logic > ap_idle_pp0;
    sc_signal< sc_lv<1> > tmp_nbreadreq_fu_28_p3;
    sc_signal< bool > ap_block_state1_pp0_stage0_iter0;
    sc_signal< sc_lv<1> > tmp_reg_63;
    sc_signal< sc_lv<1> > tmp_73_nbreadreq_fu_42_p3;
    sc_signal< bool > ap_predicate_op9_read_state2;
    sc_signal< bool > ap_block_state2_pp0_stage0_iter1;
    sc_signal< sc_lv<1> > tmp_reg_63_pp0_iter1_reg;
    sc_signal< sc_lv<1> > tmp_73_reg_72;
    sc_signal< bool > ap_predicate_op14_write_state3;
    sc_signal< bool > ap_block_state3_pp0_stage0_iter2;
    sc_signal< bool > ap_block_pp0_stage0_11001;
    sc_signal< sc_logic > rtTimer2eventEng_set_1_blk_n;
    sc_signal< bool > ap_block_pp0_stage0;
    sc_signal< sc_logic > timer2eventEng_setEv_1_blk_n;
    sc_signal< sc_logic > probeTimer2eventEng_1_blk_n;
    sc_signal< sc_lv<56> > tmp78_reg_67;
    sc_signal< sc_lv<56> > tmp78_reg_67_pp0_iter1_reg;
    sc_signal< sc_lv<56> > tmp_2_reg_76;
    sc_signal< bool > ap_block_pp0_stage0_subdone;
    sc_signal< bool > ap_block_pp0_stage0_01001;
    sc_signal< sc_lv<1> > ap_NS_fsm;
    sc_signal< sc_logic > ap_idle_pp0_0to1;
    sc_signal< sc_logic > ap_reset_idle_pp0;
    sc_signal< sc_logic > ap_enable_pp0;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<1> ap_ST_fsm_pp0_stage0;
    static const sc_lv<32> ap_const_lv32_0;
    static const bool ap_const_boolean_1;
    static const sc_lv<1> ap_const_lv1_1;
    static const sc_lv<1> ap_const_lv1_0;
    static const bool ap_const_boolean_0;
    // Thread declarations
    void thread_ap_clk_no_reset_();
    void thread_ap_CS_fsm_pp0_stage0();
    void thread_ap_block_pp0_stage0();
    void thread_ap_block_pp0_stage0_01001();
    void thread_ap_block_pp0_stage0_11001();
    void thread_ap_block_pp0_stage0_subdone();
    void thread_ap_block_state1_pp0_stage0_iter0();
    void thread_ap_block_state2_pp0_stage0_iter1();
    void thread_ap_block_state3_pp0_stage0_iter2();
    void thread_ap_done();
    void thread_ap_enable_pp0();
    void thread_ap_enable_reg_pp0_iter0();
    void thread_ap_idle();
    void thread_ap_idle_pp0();
    void thread_ap_idle_pp0_0to1();
    void thread_ap_predicate_op14_write_state3();
    void thread_ap_predicate_op9_read_state2();
    void thread_ap_ready();
    void thread_ap_reset_idle_pp0();
    void thread_probeTimer2eventEng_1_blk_n();
    void thread_probeTimer2eventEng_1_read();
    void thread_rtTimer2eventEng_set_1_blk_n();
    void thread_rtTimer2eventEng_set_1_read();
    void thread_timer2eventEng_setEv_1_blk_n();
    void thread_timer2eventEng_setEv_1_din();
    void thread_timer2eventEng_setEv_1_write();
    void thread_tmp_73_nbreadreq_fu_42_p3();
    void thread_tmp_nbreadreq_fu_28_p3();
    void thread_ap_NS_fsm();
};

}

using namespace ap_rtl;

#endif
