// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
// Version: 2019.2.1
// Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

#ifndef _updateRequestSender_HH_
#define _updateRequestSender_HH_

#include "systemc.h"
#include "AESL_pkg.h"


namespace ap_rtl {

struct updateRequestSender : public sc_module {
    // Port declarations 51
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_in< sc_logic > ap_start;
    sc_out< sc_logic > ap_done;
    sc_in< sc_logic > ap_continue;
    sc_out< sc_logic > ap_idle;
    sc_out< sc_logic > ap_ready;
    sc_in< sc_lv<1> > sessionInsert_req_V_4_dout;
    sc_in< sc_logic > sessionInsert_req_V_4_empty_n;
    sc_out< sc_logic > sessionInsert_req_V_4_read;
    sc_in< sc_lv<32> > sessionInsert_req_V_1_dout;
    sc_in< sc_logic > sessionInsert_req_V_1_empty_n;
    sc_out< sc_logic > sessionInsert_req_V_1_read;
    sc_in< sc_lv<16> > sessionInsert_req_V_6_dout;
    sc_in< sc_logic > sessionInsert_req_V_6_empty_n;
    sc_out< sc_logic > sessionInsert_req_V_6_read;
    sc_in< sc_lv<16> > sessionInsert_req_V_3_dout;
    sc_in< sc_logic > sessionInsert_req_V_3_empty_n;
    sc_out< sc_logic > sessionInsert_req_V_3_read;
    sc_in< sc_lv<16> > sessionInsert_req_V_s_dout;
    sc_in< sc_logic > sessionInsert_req_V_s_empty_n;
    sc_out< sc_logic > sessionInsert_req_V_s_read;
    sc_in< sc_lv<1> > sessionInsert_req_V_5_dout;
    sc_in< sc_logic > sessionInsert_req_V_5_empty_n;
    sc_out< sc_logic > sessionInsert_req_V_5_read;
    sc_in< sc_lv<1> > sessionDelete_req_V_4_dout;
    sc_in< sc_logic > sessionDelete_req_V_4_empty_n;
    sc_out< sc_logic > sessionDelete_req_V_4_read;
    sc_in< sc_lv<32> > sessionDelete_req_V_1_dout;
    sc_in< sc_logic > sessionDelete_req_V_1_empty_n;
    sc_out< sc_logic > sessionDelete_req_V_1_read;
    sc_in< sc_lv<16> > sessionDelete_req_V_6_dout;
    sc_in< sc_logic > sessionDelete_req_V_6_empty_n;
    sc_out< sc_logic > sessionDelete_req_V_6_read;
    sc_in< sc_lv<16> > sessionDelete_req_V_3_dout;
    sc_in< sc_logic > sessionDelete_req_V_3_empty_n;
    sc_out< sc_logic > sessionDelete_req_V_3_read;
    sc_in< sc_lv<16> > sessionDelete_req_V_s_dout;
    sc_in< sc_logic > sessionDelete_req_V_s_empty_n;
    sc_out< sc_logic > sessionDelete_req_V_s_read;
    sc_in< sc_lv<1> > sessionDelete_req_V_5_dout;
    sc_in< sc_logic > sessionDelete_req_V_5_empty_n;
    sc_out< sc_logic > sessionDelete_req_V_5_read;
    sc_out< sc_lv<14> > slc_sessionIdFinFifo_1_din;
    sc_in< sc_logic > slc_sessionIdFinFifo_1_full_n;
    sc_out< sc_logic > slc_sessionIdFinFifo_1_write;
    sc_in< sc_logic > sessionUpdate_req_V_TREADY;
    sc_out< sc_lv<88> > sessionUpdate_req_V_TDATA;
    sc_out< sc_logic > sessionUpdate_req_V_TVALID;
    sc_out< sc_lv<16> > regSessionCount_V;
    sc_out< sc_logic > regSessionCount_V_ap_vld;


    // Module declarations
    updateRequestSender(sc_module_name name);
    SC_HAS_PROCESS(updateRequestSender);

    ~updateRequestSender();

    sc_trace_file* mVcdFile;

    regslice_both<88>* regslice_both_sessionUpdate_req_V_U;
    sc_signal< sc_logic > ap_done_reg;
    sc_signal< sc_lv<1> > ap_CS_fsm;
    sc_signal< sc_logic > ap_CS_fsm_pp0_stage0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter1;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter2;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter3;
    sc_signal< sc_logic > ap_idle_pp0;
    sc_signal< sc_logic > io_acc_block_signal_op7;
    sc_signal< sc_lv<1> > tmp_nbreadreq_fu_72_p8;
    sc_signal< bool > ap_block_state1_pp0_stage0_iter0;
    sc_signal< sc_logic > io_acc_block_signal_op16;
    sc_signal< sc_lv<1> > tmp_reg_273;
    sc_signal< sc_lv<1> > tmp_1_nbreadreq_fu_106_p8;
    sc_signal< bool > ap_predicate_op16_read_state2;
    sc_signal< bool > ap_block_state2_pp0_stage0_iter1;
    sc_signal< sc_lv<1> > tmp_reg_273_pp0_iter1_reg;
    sc_signal< sc_lv<1> > tmp_1_reg_307;
    sc_signal< bool > ap_predicate_op27_write_state3;
    sc_signal< bool > ap_block_state3_pp0_stage0_iter2;
    sc_signal< bool > ap_predicate_op26_write_state3;
    sc_signal< bool > ap_block_state3_io;
    sc_signal< sc_logic > regslice_both_sessionUpdate_req_V_U_apdone_blk;
    sc_signal< bool > ap_block_state4_pp0_stage0_iter3;
    sc_signal< sc_lv<1> > tmp_reg_273_pp0_iter2_reg;
    sc_signal< sc_lv<1> > tmp_1_reg_307_pp0_iter2_reg;
    sc_signal< bool > ap_predicate_op47_write_state4;
    sc_signal< bool > ap_block_state4_io;
    sc_signal< bool > ap_block_pp0_stage0_11001;
    sc_signal< sc_lv<16> > usedSessionIDs_V;
    sc_signal< sc_logic > sessionUpdate_req_V_TDATA_blk_n;
    sc_signal< bool > ap_block_pp0_stage0;
    sc_signal< sc_logic > sessionInsert_req_V_4_blk_n;
    sc_signal< sc_logic > sessionInsert_req_V_1_blk_n;
    sc_signal< sc_logic > sessionInsert_req_V_6_blk_n;
    sc_signal< sc_logic > sessionInsert_req_V_3_blk_n;
    sc_signal< sc_logic > sessionInsert_req_V_s_blk_n;
    sc_signal< sc_logic > sessionInsert_req_V_5_blk_n;
    sc_signal< sc_logic > sessionDelete_req_V_4_blk_n;
    sc_signal< sc_logic > sessionDelete_req_V_1_blk_n;
    sc_signal< sc_logic > sessionDelete_req_V_6_blk_n;
    sc_signal< sc_logic > sessionDelete_req_V_3_blk_n;
    sc_signal< sc_logic > sessionDelete_req_V_s_blk_n;
    sc_signal< sc_logic > sessionDelete_req_V_5_blk_n;
    sc_signal< sc_logic > slc_sessionIdFinFifo_1_blk_n;
    sc_signal< sc_lv<1> > tmp_op_reg_277;
    sc_signal< sc_lv<1> > tmp_op_reg_277_pp0_iter1_reg;
    sc_signal< sc_lv<32> > tmp_key_theirIp_V_reg_282;
    sc_signal< sc_lv<32> > tmp_key_theirIp_V_reg_282_pp0_iter1_reg;
    sc_signal< sc_lv<16> > tmp_key_myPort_V_reg_287;
    sc_signal< sc_lv<16> > tmp_key_myPort_V_reg_287_pp0_iter1_reg;
    sc_signal< sc_lv<16> > tmp_key_theirPort_V_reg_292;
    sc_signal< sc_lv<16> > tmp_key_theirPort_V_reg_292_pp0_iter1_reg;
    sc_signal< sc_lv<16> > tmp_value_V_reg_297;
    sc_signal< sc_lv<16> > tmp_value_V_reg_297_pp0_iter1_reg;
    sc_signal< sc_lv<1> > tmp_source_reg_302;
    sc_signal< sc_lv<1> > tmp_source_reg_302_pp0_iter1_reg;
    sc_signal< sc_lv<1> > tmp_op_1_reg_311;
    sc_signal< sc_lv<32> > tmp_key_theirIp_V_1_reg_316;
    sc_signal< sc_lv<16> > tmp_key_myPort_V_1_reg_321;
    sc_signal< sc_lv<16> > tmp_key_theirPort_V_1_reg_326;
    sc_signal< sc_lv<16> > tmp_value_V_1_reg_331;
    sc_signal< sc_lv<1> > tmp_source_1_reg_336;
    sc_signal< sc_lv<14> > tmp_V_fu_209_p1;
    sc_signal< sc_lv<14> > tmp_V_reg_341;
    sc_signal< sc_lv<88> > sext_ln177_1_fu_223_p1;
    sc_signal< sc_lv<88> > sext_ln177_fu_238_p1;
    sc_signal< bool > ap_block_pp0_stage0_subdone;
    sc_signal< sc_lv<16> > add_ln701_fu_247_p2;
    sc_signal< sc_lv<16> > add_ln700_fu_260_p2;
    sc_signal< bool > ap_block_pp0_stage0_01001;
    sc_signal< sc_lv<82> > tmp_11_fu_213_p7;
    sc_signal< sc_lv<82> > tmp13_fu_228_p7;
    sc_signal< sc_lv<1> > ap_NS_fsm;
    sc_signal< sc_logic > ap_idle_pp0_0to2;
    sc_signal< sc_logic > ap_reset_idle_pp0;
    sc_signal< sc_logic > ap_enable_pp0;
    sc_signal< sc_lv<88> > sessionUpdate_req_V_TDATA_int;
    sc_signal< sc_logic > sessionUpdate_req_V_TVALID_int;
    sc_signal< sc_logic > sessionUpdate_req_V_TREADY_int;
    sc_signal< sc_logic > regslice_both_sessionUpdate_req_V_U_vld_out;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<1> ap_ST_fsm_pp0_stage0;
    static const sc_lv<32> ap_const_lv32_0;
    static const bool ap_const_boolean_1;
    static const sc_lv<1> ap_const_lv1_1;
    static const sc_lv<1> ap_const_lv1_0;
    static const bool ap_const_boolean_0;
    static const sc_lv<16> ap_const_lv16_FFFF;
    static const sc_lv<16> ap_const_lv16_1;
    // Thread declarations
    void thread_ap_clk_no_reset_();
    void thread_add_ln700_fu_260_p2();
    void thread_add_ln701_fu_247_p2();
    void thread_ap_CS_fsm_pp0_stage0();
    void thread_ap_block_pp0_stage0();
    void thread_ap_block_pp0_stage0_01001();
    void thread_ap_block_pp0_stage0_11001();
    void thread_ap_block_pp0_stage0_subdone();
    void thread_ap_block_state1_pp0_stage0_iter0();
    void thread_ap_block_state2_pp0_stage0_iter1();
    void thread_ap_block_state3_io();
    void thread_ap_block_state3_pp0_stage0_iter2();
    void thread_ap_block_state4_io();
    void thread_ap_block_state4_pp0_stage0_iter3();
    void thread_ap_done();
    void thread_ap_enable_pp0();
    void thread_ap_enable_reg_pp0_iter0();
    void thread_ap_idle();
    void thread_ap_idle_pp0();
    void thread_ap_idle_pp0_0to2();
    void thread_ap_predicate_op16_read_state2();
    void thread_ap_predicate_op26_write_state3();
    void thread_ap_predicate_op27_write_state3();
    void thread_ap_predicate_op47_write_state4();
    void thread_ap_ready();
    void thread_ap_reset_idle_pp0();
    void thread_io_acc_block_signal_op16();
    void thread_io_acc_block_signal_op7();
    void thread_regSessionCount_V();
    void thread_regSessionCount_V_ap_vld();
    void thread_sessionDelete_req_V_1_blk_n();
    void thread_sessionDelete_req_V_1_read();
    void thread_sessionDelete_req_V_3_blk_n();
    void thread_sessionDelete_req_V_3_read();
    void thread_sessionDelete_req_V_4_blk_n();
    void thread_sessionDelete_req_V_4_read();
    void thread_sessionDelete_req_V_5_blk_n();
    void thread_sessionDelete_req_V_5_read();
    void thread_sessionDelete_req_V_6_blk_n();
    void thread_sessionDelete_req_V_6_read();
    void thread_sessionDelete_req_V_s_blk_n();
    void thread_sessionDelete_req_V_s_read();
    void thread_sessionInsert_req_V_1_blk_n();
    void thread_sessionInsert_req_V_1_read();
    void thread_sessionInsert_req_V_3_blk_n();
    void thread_sessionInsert_req_V_3_read();
    void thread_sessionInsert_req_V_4_blk_n();
    void thread_sessionInsert_req_V_4_read();
    void thread_sessionInsert_req_V_5_blk_n();
    void thread_sessionInsert_req_V_5_read();
    void thread_sessionInsert_req_V_6_blk_n();
    void thread_sessionInsert_req_V_6_read();
    void thread_sessionInsert_req_V_s_blk_n();
    void thread_sessionInsert_req_V_s_read();
    void thread_sessionUpdate_req_V_TDATA_blk_n();
    void thread_sessionUpdate_req_V_TDATA_int();
    void thread_sessionUpdate_req_V_TVALID();
    void thread_sessionUpdate_req_V_TVALID_int();
    void thread_sext_ln177_1_fu_223_p1();
    void thread_sext_ln177_fu_238_p1();
    void thread_slc_sessionIdFinFifo_1_blk_n();
    void thread_slc_sessionIdFinFifo_1_din();
    void thread_slc_sessionIdFinFifo_1_write();
    void thread_tmp13_fu_228_p7();
    void thread_tmp_11_fu_213_p7();
    void thread_tmp_1_nbreadreq_fu_106_p8();
    void thread_tmp_V_fu_209_p1();
    void thread_tmp_nbreadreq_fu_72_p8();
    void thread_ap_NS_fsm();
};

}

using namespace ap_rtl;

#endif
