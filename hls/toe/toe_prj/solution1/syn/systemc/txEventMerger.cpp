// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
// Version: 2019.2.1
// Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

#include "txEventMerger.h"
#include "AESL_pkg.h"

using namespace std;

namespace ap_rtl {

const sc_logic txEventMerger::ap_const_logic_1 = sc_dt::Log_1;
const sc_logic txEventMerger::ap_const_logic_0 = sc_dt::Log_0;
const sc_lv<1> txEventMerger::ap_ST_fsm_pp0_stage0 = "1";
const sc_lv<32> txEventMerger::ap_const_lv32_0 = "00000000000000000000000000000000";
const bool txEventMerger::ap_const_boolean_1 = true;
const sc_lv<1> txEventMerger::ap_const_lv1_1 = "1";
const sc_lv<1> txEventMerger::ap_const_lv1_0 = "0";
const bool txEventMerger::ap_const_boolean_0 = false;
const sc_lv<3> txEventMerger::ap_const_lv3_0 = "000";
const sc_lv<32> txEventMerger::ap_const_lv32_3 = "11";
const sc_lv<32> txEventMerger::ap_const_lv32_37 = "110111";

txEventMerger::txEventMerger(sc_module_name name) : sc_module(name), mVcdFile(0) {

    SC_METHOD(thread_ap_clk_no_reset_);
    dont_initialize();
    sensitive << ( ap_clk.pos() );

    SC_METHOD(thread_ap_CS_fsm_pp0_stage0);
    sensitive << ( ap_CS_fsm );

    SC_METHOD(thread_ap_block_pp0_stage0);

    SC_METHOD(thread_ap_block_pp0_stage0_01001);
    sensitive << ( ap_start );
    sensitive << ( ap_done_reg );
    sensitive << ( ap_enable_reg_pp0_iter1 );
    sensitive << ( ap_enable_reg_pp0_iter2 );
    sensitive << ( txApp2eventEng_merge_1_empty_n );
    sensitive << ( tmp_nbreadreq_fu_42_p3 );
    sensitive << ( txAppStream2event_me_1_empty_n );
    sensitive << ( ap_predicate_op9_read_state2 );
    sensitive << ( txApp2eventEng_setEv_1_full_n );
    sensitive << ( tmp_reg_112_pp0_iter1_reg );
    sensitive << ( ap_predicate_op18_write_state3 );
    sensitive << ( txApp_txEventCache_V_full_n );
    sensitive << ( ap_predicate_op21_write_state3 );

    SC_METHOD(thread_ap_block_pp0_stage0_11001);
    sensitive << ( ap_start );
    sensitive << ( ap_done_reg );
    sensitive << ( ap_enable_reg_pp0_iter1 );
    sensitive << ( ap_enable_reg_pp0_iter2 );
    sensitive << ( txApp2eventEng_merge_1_empty_n );
    sensitive << ( tmp_nbreadreq_fu_42_p3 );
    sensitive << ( txAppStream2event_me_1_empty_n );
    sensitive << ( ap_predicate_op9_read_state2 );
    sensitive << ( txApp2eventEng_setEv_1_full_n );
    sensitive << ( tmp_reg_112_pp0_iter1_reg );
    sensitive << ( ap_predicate_op18_write_state3 );
    sensitive << ( txApp_txEventCache_V_full_n );
    sensitive << ( ap_predicate_op21_write_state3 );

    SC_METHOD(thread_ap_block_pp0_stage0_subdone);
    sensitive << ( ap_start );
    sensitive << ( ap_done_reg );
    sensitive << ( ap_enable_reg_pp0_iter1 );
    sensitive << ( ap_enable_reg_pp0_iter2 );
    sensitive << ( txApp2eventEng_merge_1_empty_n );
    sensitive << ( tmp_nbreadreq_fu_42_p3 );
    sensitive << ( txAppStream2event_me_1_empty_n );
    sensitive << ( ap_predicate_op9_read_state2 );
    sensitive << ( txApp2eventEng_setEv_1_full_n );
    sensitive << ( tmp_reg_112_pp0_iter1_reg );
    sensitive << ( ap_predicate_op18_write_state3 );
    sensitive << ( txApp_txEventCache_V_full_n );
    sensitive << ( ap_predicate_op21_write_state3 );

    SC_METHOD(thread_ap_block_state1_pp0_stage0_iter0);
    sensitive << ( ap_start );
    sensitive << ( ap_done_reg );
    sensitive << ( txApp2eventEng_merge_1_empty_n );
    sensitive << ( tmp_nbreadreq_fu_42_p3 );

    SC_METHOD(thread_ap_block_state2_pp0_stage0_iter1);
    sensitive << ( txAppStream2event_me_1_empty_n );
    sensitive << ( ap_predicate_op9_read_state2 );

    SC_METHOD(thread_ap_block_state3_pp0_stage0_iter2);
    sensitive << ( txApp2eventEng_setEv_1_full_n );
    sensitive << ( tmp_reg_112_pp0_iter1_reg );
    sensitive << ( ap_predicate_op18_write_state3 );
    sensitive << ( txApp_txEventCache_V_full_n );
    sensitive << ( ap_predicate_op21_write_state3 );

    SC_METHOD(thread_ap_done);
    sensitive << ( ap_done_reg );
    sensitive << ( ap_enable_reg_pp0_iter2 );
    sensitive << ( ap_block_pp0_stage0_11001 );

    SC_METHOD(thread_ap_enable_pp0);
    sensitive << ( ap_idle_pp0 );

    SC_METHOD(thread_ap_enable_reg_pp0_iter0);
    sensitive << ( ap_start );

    SC_METHOD(thread_ap_idle);
    sensitive << ( ap_start );
    sensitive << ( ap_CS_fsm_pp0_stage0 );
    sensitive << ( ap_idle_pp0 );

    SC_METHOD(thread_ap_idle_pp0);
    sensitive << ( ap_enable_reg_pp0_iter0 );
    sensitive << ( ap_enable_reg_pp0_iter1 );
    sensitive << ( ap_enable_reg_pp0_iter2 );

    SC_METHOD(thread_ap_idle_pp0_0to1);
    sensitive << ( ap_enable_reg_pp0_iter0 );
    sensitive << ( ap_enable_reg_pp0_iter1 );

    SC_METHOD(thread_ap_predicate_op18_write_state3);
    sensitive << ( tmp_reg_112_pp0_iter1_reg );
    sensitive << ( tmp_21_reg_121 );

    SC_METHOD(thread_ap_predicate_op21_write_state3);
    sensitive << ( tmp_reg_112_pp0_iter1_reg );
    sensitive << ( tmp_21_reg_121 );
    sensitive << ( icmp_ln54_reg_131 );

    SC_METHOD(thread_ap_predicate_op9_read_state2);
    sensitive << ( tmp_reg_112 );
    sensitive << ( tmp_21_nbreadreq_fu_56_p3 );

    SC_METHOD(thread_ap_ready);
    sensitive << ( ap_start );
    sensitive << ( ap_CS_fsm_pp0_stage0 );
    sensitive << ( ap_block_pp0_stage0_11001 );

    SC_METHOD(thread_ap_reset_idle_pp0);
    sensitive << ( ap_start );
    sensitive << ( ap_idle_pp0_0to1 );

    SC_METHOD(thread_icmp_ln54_fu_88_p2);
    sensitive << ( ap_CS_fsm_pp0_stage0 );
    sensitive << ( ap_enable_reg_pp0_iter1 );
    sensitive << ( tmp_reg_112 );
    sensitive << ( tmp_21_nbreadreq_fu_56_p3 );
    sensitive << ( ap_block_pp0_stage0_11001 );
    sensitive << ( tmp_type_fu_84_p1 );

    SC_METHOD(thread_tmp_19_i_fu_94_p4);
    sensitive << ( tmp_2_reg_125 );

    SC_METHOD(thread_tmp_21_nbreadreq_fu_56_p3);
    sensitive << ( txAppStream2event_me_1_empty_n );

    SC_METHOD(thread_tmp_nbreadreq_fu_42_p3);
    sensitive << ( txApp2eventEng_merge_1_empty_n );

    SC_METHOD(thread_tmp_type_fu_84_p1);
    sensitive << ( txAppStream2event_me_1_dout );

    SC_METHOD(thread_txApp2eventEng_merge_1_blk_n);
    sensitive << ( ap_start );
    sensitive << ( ap_done_reg );
    sensitive << ( ap_CS_fsm_pp0_stage0 );
    sensitive << ( txApp2eventEng_merge_1_empty_n );
    sensitive << ( tmp_nbreadreq_fu_42_p3 );
    sensitive << ( ap_block_pp0_stage0 );

    SC_METHOD(thread_txApp2eventEng_merge_1_read);
    sensitive << ( ap_start );
    sensitive << ( ap_CS_fsm_pp0_stage0 );
    sensitive << ( tmp_nbreadreq_fu_42_p3 );
    sensitive << ( ap_block_pp0_stage0_11001 );

    SC_METHOD(thread_txApp2eventEng_setEv_1_blk_n);
    sensitive << ( ap_enable_reg_pp0_iter2 );
    sensitive << ( txApp2eventEng_setEv_1_full_n );
    sensitive << ( tmp_reg_112_pp0_iter1_reg );
    sensitive << ( ap_predicate_op18_write_state3 );
    sensitive << ( ap_block_pp0_stage0 );

    SC_METHOD(thread_txApp2eventEng_setEv_1_din);
    sensitive << ( ap_enable_reg_pp0_iter2 );
    sensitive << ( tmp_reg_112_pp0_iter1_reg );
    sensitive << ( ap_predicate_op18_write_state3 );
    sensitive << ( tmp114_reg_116_pp0_iter1_reg );
    sensitive << ( tmp_2_reg_125 );
    sensitive << ( ap_block_pp0_stage0_01001 );

    SC_METHOD(thread_txApp2eventEng_setEv_1_write);
    sensitive << ( ap_enable_reg_pp0_iter2 );
    sensitive << ( tmp_reg_112_pp0_iter1_reg );
    sensitive << ( ap_predicate_op18_write_state3 );
    sensitive << ( ap_block_pp0_stage0_11001 );

    SC_METHOD(thread_txAppStream2event_me_1_blk_n);
    sensitive << ( ap_CS_fsm_pp0_stage0 );
    sensitive << ( ap_enable_reg_pp0_iter1 );
    sensitive << ( txAppStream2event_me_1_empty_n );
    sensitive << ( ap_predicate_op9_read_state2 );
    sensitive << ( ap_block_pp0_stage0 );

    SC_METHOD(thread_txAppStream2event_me_1_read);
    sensitive << ( ap_CS_fsm_pp0_stage0 );
    sensitive << ( ap_enable_reg_pp0_iter1 );
    sensitive << ( ap_predicate_op9_read_state2 );
    sensitive << ( ap_block_pp0_stage0_11001 );

    SC_METHOD(thread_txApp_txEventCache_V_blk_n);
    sensitive << ( ap_enable_reg_pp0_iter2 );
    sensitive << ( txApp_txEventCache_V_full_n );
    sensitive << ( ap_predicate_op21_write_state3 );
    sensitive << ( ap_block_pp0_stage0 );

    SC_METHOD(thread_txApp_txEventCache_V_din);
    sensitive << ( ap_enable_reg_pp0_iter2 );
    sensitive << ( ap_predicate_op21_write_state3 );
    sensitive << ( ap_block_pp0_stage0_01001 );
    sensitive << ( tmp_19_i_fu_94_p4 );

    SC_METHOD(thread_txApp_txEventCache_V_write);
    sensitive << ( ap_enable_reg_pp0_iter2 );
    sensitive << ( ap_predicate_op21_write_state3 );
    sensitive << ( ap_block_pp0_stage0_11001 );

    SC_METHOD(thread_ap_NS_fsm);
    sensitive << ( ap_CS_fsm );
    sensitive << ( ap_block_pp0_stage0_subdone );
    sensitive << ( ap_reset_idle_pp0 );

    ap_done_reg = SC_LOGIC_0;
    ap_CS_fsm = "1";
    ap_enable_reg_pp0_iter1 = SC_LOGIC_0;
    ap_enable_reg_pp0_iter2 = SC_LOGIC_0;
    static int apTFileNum = 0;
    stringstream apTFilenSS;
    apTFilenSS << "txEventMerger_sc_trace_" << apTFileNum ++;
    string apTFn = apTFilenSS.str();
    mVcdFile = sc_create_vcd_trace_file(apTFn.c_str());
    mVcdFile->set_time_unit(1, SC_PS);
    if (1) {
#ifdef __HLS_TRACE_LEVEL_PORT_HIER__
    sc_trace(mVcdFile, ap_clk, "(port)ap_clk");
    sc_trace(mVcdFile, ap_rst, "(port)ap_rst");
    sc_trace(mVcdFile, ap_start, "(port)ap_start");
    sc_trace(mVcdFile, ap_done, "(port)ap_done");
    sc_trace(mVcdFile, ap_continue, "(port)ap_continue");
    sc_trace(mVcdFile, ap_idle, "(port)ap_idle");
    sc_trace(mVcdFile, ap_ready, "(port)ap_ready");
    sc_trace(mVcdFile, txApp2eventEng_merge_1_dout, "(port)txApp2eventEng_merge_1_dout");
    sc_trace(mVcdFile, txApp2eventEng_merge_1_empty_n, "(port)txApp2eventEng_merge_1_empty_n");
    sc_trace(mVcdFile, txApp2eventEng_merge_1_read, "(port)txApp2eventEng_merge_1_read");
    sc_trace(mVcdFile, txAppStream2event_me_1_dout, "(port)txAppStream2event_me_1_dout");
    sc_trace(mVcdFile, txAppStream2event_me_1_empty_n, "(port)txAppStream2event_me_1_empty_n");
    sc_trace(mVcdFile, txAppStream2event_me_1_read, "(port)txAppStream2event_me_1_read");
    sc_trace(mVcdFile, txApp2eventEng_setEv_1_din, "(port)txApp2eventEng_setEv_1_din");
    sc_trace(mVcdFile, txApp2eventEng_setEv_1_full_n, "(port)txApp2eventEng_setEv_1_full_n");
    sc_trace(mVcdFile, txApp2eventEng_setEv_1_write, "(port)txApp2eventEng_setEv_1_write");
    sc_trace(mVcdFile, txApp_txEventCache_V_din, "(port)txApp_txEventCache_V_din");
    sc_trace(mVcdFile, txApp_txEventCache_V_full_n, "(port)txApp_txEventCache_V_full_n");
    sc_trace(mVcdFile, txApp_txEventCache_V_write, "(port)txApp_txEventCache_V_write");
#endif
#ifdef __HLS_TRACE_LEVEL_INT__
    sc_trace(mVcdFile, ap_done_reg, "ap_done_reg");
    sc_trace(mVcdFile, ap_CS_fsm, "ap_CS_fsm");
    sc_trace(mVcdFile, ap_CS_fsm_pp0_stage0, "ap_CS_fsm_pp0_stage0");
    sc_trace(mVcdFile, ap_enable_reg_pp0_iter0, "ap_enable_reg_pp0_iter0");
    sc_trace(mVcdFile, ap_enable_reg_pp0_iter1, "ap_enable_reg_pp0_iter1");
    sc_trace(mVcdFile, ap_enable_reg_pp0_iter2, "ap_enable_reg_pp0_iter2");
    sc_trace(mVcdFile, ap_idle_pp0, "ap_idle_pp0");
    sc_trace(mVcdFile, tmp_nbreadreq_fu_42_p3, "tmp_nbreadreq_fu_42_p3");
    sc_trace(mVcdFile, ap_block_state1_pp0_stage0_iter0, "ap_block_state1_pp0_stage0_iter0");
    sc_trace(mVcdFile, tmp_reg_112, "tmp_reg_112");
    sc_trace(mVcdFile, tmp_21_nbreadreq_fu_56_p3, "tmp_21_nbreadreq_fu_56_p3");
    sc_trace(mVcdFile, ap_predicate_op9_read_state2, "ap_predicate_op9_read_state2");
    sc_trace(mVcdFile, ap_block_state2_pp0_stage0_iter1, "ap_block_state2_pp0_stage0_iter1");
    sc_trace(mVcdFile, tmp_reg_112_pp0_iter1_reg, "tmp_reg_112_pp0_iter1_reg");
    sc_trace(mVcdFile, tmp_21_reg_121, "tmp_21_reg_121");
    sc_trace(mVcdFile, ap_predicate_op18_write_state3, "ap_predicate_op18_write_state3");
    sc_trace(mVcdFile, icmp_ln54_reg_131, "icmp_ln54_reg_131");
    sc_trace(mVcdFile, ap_predicate_op21_write_state3, "ap_predicate_op21_write_state3");
    sc_trace(mVcdFile, ap_block_state3_pp0_stage0_iter2, "ap_block_state3_pp0_stage0_iter2");
    sc_trace(mVcdFile, ap_block_pp0_stage0_11001, "ap_block_pp0_stage0_11001");
    sc_trace(mVcdFile, txApp2eventEng_merge_1_blk_n, "txApp2eventEng_merge_1_blk_n");
    sc_trace(mVcdFile, ap_block_pp0_stage0, "ap_block_pp0_stage0");
    sc_trace(mVcdFile, txApp2eventEng_setEv_1_blk_n, "txApp2eventEng_setEv_1_blk_n");
    sc_trace(mVcdFile, txAppStream2event_me_1_blk_n, "txAppStream2event_me_1_blk_n");
    sc_trace(mVcdFile, txApp_txEventCache_V_blk_n, "txApp_txEventCache_V_blk_n");
    sc_trace(mVcdFile, tmp114_reg_116, "tmp114_reg_116");
    sc_trace(mVcdFile, tmp114_reg_116_pp0_iter1_reg, "tmp114_reg_116_pp0_iter1_reg");
    sc_trace(mVcdFile, tmp_2_reg_125, "tmp_2_reg_125");
    sc_trace(mVcdFile, icmp_ln54_fu_88_p2, "icmp_ln54_fu_88_p2");
    sc_trace(mVcdFile, ap_block_pp0_stage0_subdone, "ap_block_pp0_stage0_subdone");
    sc_trace(mVcdFile, ap_block_pp0_stage0_01001, "ap_block_pp0_stage0_01001");
    sc_trace(mVcdFile, tmp_type_fu_84_p1, "tmp_type_fu_84_p1");
    sc_trace(mVcdFile, tmp_19_i_fu_94_p4, "tmp_19_i_fu_94_p4");
    sc_trace(mVcdFile, ap_NS_fsm, "ap_NS_fsm");
    sc_trace(mVcdFile, ap_idle_pp0_0to1, "ap_idle_pp0_0to1");
    sc_trace(mVcdFile, ap_reset_idle_pp0, "ap_reset_idle_pp0");
    sc_trace(mVcdFile, ap_enable_pp0, "ap_enable_pp0");
#endif

    }
}

txEventMerger::~txEventMerger() {
    if (mVcdFile) 
        sc_close_vcd_trace_file(mVcdFile);

}

void txEventMerger::thread_ap_clk_no_reset_() {
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_CS_fsm = ap_ST_fsm_pp0_stage0;
    } else {
        ap_CS_fsm = ap_NS_fsm.read();
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_done_reg = ap_const_logic_0;
    } else {
        if (esl_seteq<1,1,1>(ap_const_logic_1, ap_continue.read())) {
            ap_done_reg = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter2.read()) && 
                    esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0))) {
            ap_done_reg = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_enable_reg_pp0_iter1 = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
             esl_seteq<1,1,1>(ap_block_pp0_stage0_subdone.read(), ap_const_boolean_0))) {
            ap_enable_reg_pp0_iter1 = ap_start.read();
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_enable_reg_pp0_iter2 = ap_const_logic_0;
    } else {
        if (esl_seteq<1,1,1>(ap_block_pp0_stage0_subdone.read(), ap_const_boolean_0)) {
            ap_enable_reg_pp0_iter2 = ap_enable_reg_pp0_iter1.read();
        }
    }
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && esl_seteq<1,1,1>(tmp_reg_112.read(), ap_const_lv1_0) && esl_seteq<1,1,1>(ap_const_lv1_1, tmp_21_nbreadreq_fu_56_p3.read()) && esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0))) {
        icmp_ln54_reg_131 = icmp_ln54_fu_88_p2.read();
    }
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && esl_seteq<1,1,1>(tmp_nbreadreq_fu_42_p3.read(), ap_const_lv1_1) && esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0))) {
        tmp114_reg_116 = txApp2eventEng_merge_1_dout.read();
    }
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0))) {
        tmp114_reg_116_pp0_iter1_reg = tmp114_reg_116.read();
        tmp_reg_112 = tmp_nbreadreq_fu_42_p3.read();
        tmp_reg_112_pp0_iter1_reg = tmp_reg_112.read();
    }
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && esl_seteq<1,1,1>(tmp_reg_112.read(), ap_const_lv1_0) && esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0))) {
        tmp_21_reg_121 = tmp_21_nbreadreq_fu_56_p3.read();
    }
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op9_read_state2.read()) && esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0))) {
        tmp_2_reg_125 = txAppStream2event_me_1_dout.read();
    }
}

void txEventMerger::thread_ap_CS_fsm_pp0_stage0() {
    ap_CS_fsm_pp0_stage0 = ap_CS_fsm.read()[0];
}

void txEventMerger::thread_ap_block_pp0_stage0() {
    ap_block_pp0_stage0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void txEventMerger::thread_ap_block_pp0_stage0_01001() {
    ap_block_pp0_stage0_01001 = (esl_seteq<1,1,1>(ap_done_reg.read(), ap_const_logic_1) || (esl_seteq<1,1,1>(ap_const_logic_1, ap_start.read()) && 
  (esl_seteq<1,1,1>(ap_const_logic_0, ap_start.read()) || 
   (esl_seteq<1,1,1>(ap_const_logic_0, txApp2eventEng_merge_1_empty_n.read()) && 
    esl_seteq<1,1,1>(tmp_nbreadreq_fu_42_p3.read(), ap_const_lv1_1)) || 
   esl_seteq<1,1,1>(ap_done_reg.read(), ap_const_logic_1))) || (esl_seteq<1,1,1>(ap_const_logic_0, txAppStream2event_me_1_empty_n.read()) && 
  esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op9_read_state2.read()) && 
  esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter1.read())) || (((esl_seteq<1,1,1>(ap_const_logic_0, txApp2eventEng_setEv_1_full_n.read()) && 
    esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op18_write_state3.read())) || 
   (esl_seteq<1,1,1>(ap_const_logic_0, txApp_txEventCache_V_full_n.read()) && 
    esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op21_write_state3.read())) || 
   (esl_seteq<1,1,1>(ap_const_logic_0, txApp2eventEng_setEv_1_full_n.read()) && 
    esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_112_pp0_iter1_reg.read()))) && 
  esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter2.read())));
}

void txEventMerger::thread_ap_block_pp0_stage0_11001() {
    ap_block_pp0_stage0_11001 = (esl_seteq<1,1,1>(ap_done_reg.read(), ap_const_logic_1) || (esl_seteq<1,1,1>(ap_const_logic_1, ap_start.read()) && 
  (esl_seteq<1,1,1>(ap_const_logic_0, ap_start.read()) || 
   (esl_seteq<1,1,1>(ap_const_logic_0, txApp2eventEng_merge_1_empty_n.read()) && 
    esl_seteq<1,1,1>(tmp_nbreadreq_fu_42_p3.read(), ap_const_lv1_1)) || 
   esl_seteq<1,1,1>(ap_done_reg.read(), ap_const_logic_1))) || (esl_seteq<1,1,1>(ap_const_logic_0, txAppStream2event_me_1_empty_n.read()) && 
  esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op9_read_state2.read()) && 
  esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter1.read())) || (((esl_seteq<1,1,1>(ap_const_logic_0, txApp2eventEng_setEv_1_full_n.read()) && 
    esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op18_write_state3.read())) || 
   (esl_seteq<1,1,1>(ap_const_logic_0, txApp_txEventCache_V_full_n.read()) && 
    esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op21_write_state3.read())) || 
   (esl_seteq<1,1,1>(ap_const_logic_0, txApp2eventEng_setEv_1_full_n.read()) && 
    esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_112_pp0_iter1_reg.read()))) && 
  esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter2.read())));
}

void txEventMerger::thread_ap_block_pp0_stage0_subdone() {
    ap_block_pp0_stage0_subdone = (esl_seteq<1,1,1>(ap_done_reg.read(), ap_const_logic_1) || (esl_seteq<1,1,1>(ap_const_logic_1, ap_start.read()) && 
  (esl_seteq<1,1,1>(ap_const_logic_0, ap_start.read()) || 
   (esl_seteq<1,1,1>(ap_const_logic_0, txApp2eventEng_merge_1_empty_n.read()) && 
    esl_seteq<1,1,1>(tmp_nbreadreq_fu_42_p3.read(), ap_const_lv1_1)) || 
   esl_seteq<1,1,1>(ap_done_reg.read(), ap_const_logic_1))) || (esl_seteq<1,1,1>(ap_const_logic_0, txAppStream2event_me_1_empty_n.read()) && 
  esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op9_read_state2.read()) && 
  esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter1.read())) || (((esl_seteq<1,1,1>(ap_const_logic_0, txApp2eventEng_setEv_1_full_n.read()) && 
    esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op18_write_state3.read())) || 
   (esl_seteq<1,1,1>(ap_const_logic_0, txApp_txEventCache_V_full_n.read()) && 
    esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op21_write_state3.read())) || 
   (esl_seteq<1,1,1>(ap_const_logic_0, txApp2eventEng_setEv_1_full_n.read()) && 
    esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_112_pp0_iter1_reg.read()))) && 
  esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter2.read())));
}

void txEventMerger::thread_ap_block_state1_pp0_stage0_iter0() {
    ap_block_state1_pp0_stage0_iter0 = (esl_seteq<1,1,1>(ap_const_logic_0, ap_start.read()) || (esl_seteq<1,1,1>(ap_const_logic_0, txApp2eventEng_merge_1_empty_n.read()) && 
  esl_seteq<1,1,1>(tmp_nbreadreq_fu_42_p3.read(), ap_const_lv1_1)) || esl_seteq<1,1,1>(ap_done_reg.read(), ap_const_logic_1));
}

void txEventMerger::thread_ap_block_state2_pp0_stage0_iter1() {
    ap_block_state2_pp0_stage0_iter1 = (esl_seteq<1,1,1>(ap_const_logic_0, txAppStream2event_me_1_empty_n.read()) && esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op9_read_state2.read()));
}

void txEventMerger::thread_ap_block_state3_pp0_stage0_iter2() {
    ap_block_state3_pp0_stage0_iter2 = ((esl_seteq<1,1,1>(ap_const_logic_0, txApp2eventEng_setEv_1_full_n.read()) && 
  esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op18_write_state3.read())) || (esl_seteq<1,1,1>(ap_const_logic_0, txApp_txEventCache_V_full_n.read()) && 
  esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op21_write_state3.read())) || (esl_seteq<1,1,1>(ap_const_logic_0, txApp2eventEng_setEv_1_full_n.read()) && 
  esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_112_pp0_iter1_reg.read())));
}

void txEventMerger::thread_ap_done() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter2.read()) && 
         esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0))) {
        ap_done = ap_const_logic_1;
    } else {
        ap_done = ap_done_reg.read();
    }
}

void txEventMerger::thread_ap_enable_pp0() {
    ap_enable_pp0 = (ap_idle_pp0.read() ^ ap_const_logic_1);
}

void txEventMerger::thread_ap_enable_reg_pp0_iter0() {
    ap_enable_reg_pp0_iter0 = ap_start.read();
}

void txEventMerger::thread_ap_idle() {
    if ((esl_seteq<1,1,1>(ap_const_logic_0, ap_start.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_idle_pp0.read()))) {
        ap_idle = ap_const_logic_1;
    } else {
        ap_idle = ap_const_logic_0;
    }
}

void txEventMerger::thread_ap_idle_pp0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_0, ap_enable_reg_pp0_iter0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, ap_enable_reg_pp0_iter1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, ap_enable_reg_pp0_iter2.read()))) {
        ap_idle_pp0 = ap_const_logic_1;
    } else {
        ap_idle_pp0 = ap_const_logic_0;
    }
}

void txEventMerger::thread_ap_idle_pp0_0to1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_0, ap_enable_reg_pp0_iter0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, ap_enable_reg_pp0_iter1.read()))) {
        ap_idle_pp0_0to1 = ap_const_logic_1;
    } else {
        ap_idle_pp0_0to1 = ap_const_logic_0;
    }
}

void txEventMerger::thread_ap_predicate_op18_write_state3() {
    ap_predicate_op18_write_state3 = (esl_seteq<1,1,1>(ap_const_lv1_0, tmp_reg_112_pp0_iter1_reg.read()) && esl_seteq<1,1,1>(ap_const_lv1_1, tmp_21_reg_121.read()));
}

void txEventMerger::thread_ap_predicate_op21_write_state3() {
    ap_predicate_op21_write_state3 = (esl_seteq<1,1,1>(ap_const_lv1_0, tmp_reg_112_pp0_iter1_reg.read()) && esl_seteq<1,1,1>(ap_const_lv1_1, tmp_21_reg_121.read()) && esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln54_reg_131.read()));
}

void txEventMerger::thread_ap_predicate_op9_read_state2() {
    ap_predicate_op9_read_state2 = (esl_seteq<1,1,1>(tmp_reg_112.read(), ap_const_lv1_0) && esl_seteq<1,1,1>(ap_const_lv1_1, tmp_21_nbreadreq_fu_56_p3.read()));
}

void txEventMerger::thread_ap_ready() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_start.read()) && 
         esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0))) {
        ap_ready = ap_const_logic_1;
    } else {
        ap_ready = ap_const_logic_0;
    }
}

void txEventMerger::thread_ap_reset_idle_pp0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_0, ap_start.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_idle_pp0_0to1.read()))) {
        ap_reset_idle_pp0 = ap_const_logic_1;
    } else {
        ap_reset_idle_pp0 = ap_const_logic_0;
    }
}

void txEventMerger::thread_icmp_ln54_fu_88_p2() {
    icmp_ln54_fu_88_p2 = (!tmp_type_fu_84_p1.read().is_01() || !ap_const_lv3_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_type_fu_84_p1.read() == ap_const_lv3_0);
}

void txEventMerger::thread_tmp_19_i_fu_94_p4() {
    tmp_19_i_fu_94_p4 = tmp_2_reg_125.read().range(55, 3);
}

void txEventMerger::thread_tmp_21_nbreadreq_fu_56_p3() {
    tmp_21_nbreadreq_fu_56_p3 =  (sc_lv<1>) ((txAppStream2event_me_1_empty_n.read()));
}

void txEventMerger::thread_tmp_nbreadreq_fu_42_p3() {
    tmp_nbreadreq_fu_42_p3 =  (sc_lv<1>) ((txApp2eventEng_merge_1_empty_n.read()));
}

void txEventMerger::thread_tmp_type_fu_84_p1() {
    tmp_type_fu_84_p1 = txAppStream2event_me_1_dout.read().range(3-1, 0);
}

void txEventMerger::thread_txApp2eventEng_merge_1_blk_n() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_start.read()) && 
         esl_seteq<1,1,1>(tmp_nbreadreq_fu_42_p3.read(), ap_const_lv1_1) && 
         !(esl_seteq<1,1,1>(ap_const_logic_0, ap_start.read()) || esl_seteq<1,1,1>(ap_done_reg.read(), ap_const_logic_1)) && 
         esl_seteq<1,1,1>(ap_block_pp0_stage0.read(), ap_const_boolean_0))) {
        txApp2eventEng_merge_1_blk_n = txApp2eventEng_merge_1_empty_n.read();
    } else {
        txApp2eventEng_merge_1_blk_n = ap_const_logic_1;
    }
}

void txEventMerger::thread_txApp2eventEng_merge_1_read() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_start.read()) && 
         esl_seteq<1,1,1>(tmp_nbreadreq_fu_42_p3.read(), ap_const_lv1_1) && 
         esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0))) {
        txApp2eventEng_merge_1_read = ap_const_logic_1;
    } else {
        txApp2eventEng_merge_1_read = ap_const_logic_0;
    }
}

void txEventMerger::thread_txApp2eventEng_setEv_1_blk_n() {
    if (((esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_112_pp0_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter2.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage0.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op18_write_state3.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter2.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage0.read(), ap_const_boolean_0)))) {
        txApp2eventEng_setEv_1_blk_n = txApp2eventEng_setEv_1_full_n.read();
    } else {
        txApp2eventEng_setEv_1_blk_n = ap_const_logic_1;
    }
}

void txEventMerger::thread_txApp2eventEng_setEv_1_din() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter2.read()) && esl_seteq<1,1,1>(ap_block_pp0_stage0_01001.read(), ap_const_boolean_0))) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_112_pp0_iter1_reg.read())) {
            txApp2eventEng_setEv_1_din = tmp114_reg_116_pp0_iter1_reg.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op18_write_state3.read())) {
            txApp2eventEng_setEv_1_din = tmp_2_reg_125.read();
        } else {
            txApp2eventEng_setEv_1_din = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
        }
    } else {
        txApp2eventEng_setEv_1_din = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    }
}

void txEventMerger::thread_txApp2eventEng_setEv_1_write() {
    if (((esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op18_write_state3.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter2.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_112_pp0_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter2.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0)))) {
        txApp2eventEng_setEv_1_write = ap_const_logic_1;
    } else {
        txApp2eventEng_setEv_1_write = ap_const_logic_0;
    }
}

void txEventMerger::thread_txAppStream2event_me_1_blk_n() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op9_read_state2.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter1.read()) && 
         esl_seteq<1,1,1>(ap_block_pp0_stage0.read(), ap_const_boolean_0))) {
        txAppStream2event_me_1_blk_n = txAppStream2event_me_1_empty_n.read();
    } else {
        txAppStream2event_me_1_blk_n = ap_const_logic_1;
    }
}

void txEventMerger::thread_txAppStream2event_me_1_read() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op9_read_state2.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter1.read()) && 
         esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0))) {
        txAppStream2event_me_1_read = ap_const_logic_1;
    } else {
        txAppStream2event_me_1_read = ap_const_logic_0;
    }
}

void txEventMerger::thread_txApp_txEventCache_V_blk_n() {
    if ((esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op21_write_state3.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter2.read()) && 
         esl_seteq<1,1,1>(ap_block_pp0_stage0.read(), ap_const_boolean_0))) {
        txApp_txEventCache_V_blk_n = txApp_txEventCache_V_full_n.read();
    } else {
        txApp_txEventCache_V_blk_n = ap_const_logic_1;
    }
}

void txEventMerger::thread_txApp_txEventCache_V_din() {
    txApp_txEventCache_V_din = esl_concat<53,3>(tmp_19_i_fu_94_p4.read(), ap_const_lv3_0);
}

void txEventMerger::thread_txApp_txEventCache_V_write() {
    if ((esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op21_write_state3.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter2.read()) && 
         esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0))) {
        txApp_txEventCache_V_write = ap_const_logic_1;
    } else {
        txApp_txEventCache_V_write = ap_const_logic_0;
    }
}

void txEventMerger::thread_ap_NS_fsm() {
    switch (ap_CS_fsm.read().to_uint64()) {
        case 1 : 
            ap_NS_fsm = ap_ST_fsm_pp0_stage0;
break;
        default : 
            ap_NS_fsm =  (sc_lv<1>) ("X");
            break;
    }
}

}

