// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
// Version: 2019.2.1
// Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

#ifndef _probe_timer_HH_
#define _probe_timer_HH_

#include "systemc.h"
#include "AESL_pkg.h"

#include "probe_timer_probeAem.h"

namespace ap_rtl {

struct probe_timer : public sc_module {
    // Port declarations 16
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_in< sc_logic > ap_start;
    sc_out< sc_logic > ap_done;
    sc_in< sc_logic > ap_continue;
    sc_out< sc_logic > ap_idle;
    sc_out< sc_logic > ap_ready;
    sc_in< sc_lv<16> > rxEng2timer_clearPro_1_dout;
    sc_in< sc_logic > rxEng2timer_clearPro_1_empty_n;
    sc_out< sc_logic > rxEng2timer_clearPro_1_read;
    sc_in< sc_lv<16> > txEng2timer_setProbe_1_dout;
    sc_in< sc_logic > txEng2timer_setProbe_1_empty_n;
    sc_out< sc_logic > txEng2timer_setProbe_1_read;
    sc_out< sc_lv<56> > probeTimer2eventEng_1_din;
    sc_in< sc_logic > probeTimer2eventEng_1_full_n;
    sc_out< sc_logic > probeTimer2eventEng_1_write;
    sc_signal< sc_lv<33> > ap_var_for_const0;


    // Module declarations
    probe_timer(sc_module_name name);
    SC_HAS_PROCESS(probe_timer);

    ~probe_timer();

    sc_trace_file* mVcdFile;

    probe_timer_probeAem* probeTimerTable_U;
    sc_signal< sc_logic > ap_done_reg;
    sc_signal< sc_lv<1> > ap_CS_fsm;
    sc_signal< sc_logic > ap_CS_fsm_pp0_stage0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter1;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter2;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter3;
    sc_signal< sc_logic > ap_idle_pp0;
    sc_signal< sc_lv<1> > pt_WaitForWrite_load_load_fu_174_p1;
    sc_signal< sc_lv<1> > tmp_nbreadreq_fu_80_p3;
    sc_signal< sc_lv<1> > tmp_211_nbreadreq_fu_88_p3;
    sc_signal< bool > ap_predicate_op19_read_state1;
    sc_signal< bool > ap_predicate_op23_read_state1;
    sc_signal< bool > ap_block_state1_pp0_stage0_iter0;
    sc_signal< bool > ap_block_state2_pp0_stage0_iter1;
    sc_signal< bool > ap_block_state3_pp0_stage0_iter2;
    sc_signal< sc_lv<1> > pt_WaitForWrite_load_reg_320;
    sc_signal< sc_lv<1> > pt_WaitForWrite_load_reg_320_pp0_iter2_reg;
    sc_signal< sc_lv<1> > tmp_reg_329;
    sc_signal< sc_lv<1> > tmp_reg_329_pp0_iter2_reg;
    sc_signal< sc_lv<1> > tmp_212_reg_352;
    sc_signal< sc_lv<1> > tmp_213_nbwritereq_fu_108_p3;
    sc_signal< sc_lv<1> > or_ln101_fu_285_p2;
    sc_signal< bool > ap_predicate_op58_write_state4;
    sc_signal< bool > ap_block_state4_pp0_stage0_iter3;
    sc_signal< bool > ap_block_pp0_stage0_11001;
    sc_signal< sc_lv<1> > pt_WaitForWrite;
    sc_signal< sc_lv<16> > pt_updSessionID_V;
    sc_signal< sc_lv<16> > pt_prevSessionID_V;
    sc_signal< sc_lv<10> > probeTimerTable_address0;
    sc_signal< sc_logic > probeTimerTable_ce0;
    sc_signal< sc_logic > probeTimerTable_we0;
    sc_signal< sc_lv<33> > probeTimerTable_q0;
    sc_signal< sc_lv<10> > probeTimerTable_address1;
    sc_signal< sc_logic > probeTimerTable_ce1;
    sc_signal< sc_logic > probeTimerTable_we1;
    sc_signal< sc_lv<33> > probeTimerTable_d1;
    sc_signal< sc_lv<16> > pt_currSessionID_V;
    sc_signal< sc_logic > txEng2timer_setProbe_1_blk_n;
    sc_signal< bool > ap_block_pp0_stage0;
    sc_signal< sc_logic > rxEng2timer_clearPro_1_blk_n;
    sc_signal< sc_logic > probeTimer2eventEng_1_blk_n;
    sc_signal< sc_lv<16> > tmp_sessionID_V_reg_150;
    sc_signal< sc_lv<16> > tmp_sessionID_V_reg_150_pp0_iter1_reg;
    sc_signal< sc_lv<16> > tmp_sessionID_V_reg_150_pp0_iter2_reg;
    sc_signal< sc_lv<1> > pt_WaitForWrite_load_reg_320_pp0_iter1_reg;
    sc_signal< sc_lv<16> > pt_updSessionID_V_lo_reg_324;
    sc_signal< sc_lv<1> > tmp_reg_329_pp0_iter1_reg;
    sc_signal< sc_lv<1> > icmp_ln883_fu_235_p2;
    sc_signal< sc_lv<1> > icmp_ln883_reg_336;
    sc_signal< sc_lv<10> > probeTimerTable_addr_1_reg_340;
    sc_signal< sc_lv<10> > probeTimerTable_addr_1_reg_340_pp0_iter2_reg;
    sc_signal< sc_lv<33> > probeTimerTable_load_reg_346;
    sc_signal< bool > ap_block_pp0_stage0_subdone;
    sc_signal< sc_lv<16> > ap_phi_mux_tmp_sessionID_V_phi_fu_153_p4;
    sc_signal< sc_lv<16> > ap_phi_reg_pp0_iter0_tmp_sessionID_V_reg_150;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter0_fastResume_0_i_reg_161;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter1_fastResume_0_i_reg_161;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter2_fastResume_0_i_reg_161;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter3_fastResume_0_i_reg_161;
    sc_signal< sc_lv<64> > zext_ln544_19_fu_259_p1;
    sc_signal< sc_lv<64> > zext_ln544_fu_264_p1;
    sc_signal< sc_lv<16> > add_ln701_fu_247_p2;
    sc_signal< sc_lv<16> > select_ln92_fu_203_p3;
    sc_signal< bool > ap_block_pp0_stage0_01001;
    sc_signal< sc_lv<33> > probeTimerTable_time_1_fu_297_p5;
    sc_signal< sc_lv<16> > add_ln700_fu_191_p2;
    sc_signal< sc_lv<1> > icmp_ln879_fu_197_p2;
    sc_signal< sc_lv<32> > trunc_ln879_fu_276_p1;
    sc_signal< sc_lv<1> > icmp_ln879_16_fu_279_p2;
    sc_signal< sc_lv<32> > add_ln701_1_fu_291_p2;
    sc_signal< sc_lv<1> > ap_NS_fsm;
    sc_signal< sc_logic > ap_idle_pp0_0to2;
    sc_signal< sc_logic > ap_reset_idle_pp0;
    sc_signal< bool > ap_block_pp0;
    sc_signal< bool > ap_predicate_op36_store_state2;
    sc_signal< bool > ap_enable_operation_36;
    sc_signal< bool > ap_enable_state2_pp0_iter1_stage0;
    sc_signal< bool > ap_predicate_op33_load_state2;
    sc_signal< bool > ap_enable_operation_33;
    sc_signal< bool > ap_predicate_op37_load_state3;
    sc_signal< bool > ap_enable_operation_37;
    sc_signal< bool > ap_enable_state3_pp0_iter2_stage0;
    sc_signal< bool > ap_predicate_op54_store_state4;
    sc_signal< bool > ap_enable_operation_54;
    sc_signal< bool > ap_enable_state4_pp0_iter3_stage0;
    sc_signal< bool > ap_predicate_op56_store_state4;
    sc_signal< bool > ap_enable_operation_56;
    sc_signal< sc_logic > ap_enable_pp0;
    sc_signal< bool > ap_condition_188;
    sc_signal< bool > ap_condition_53;
    sc_signal< bool > ap_condition_108;
    sc_signal< bool > ap_condition_202;
    sc_signal< bool > ap_condition_373;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<1> ap_ST_fsm_pp0_stage0;
    static const sc_lv<32> ap_const_lv32_0;
    static const bool ap_const_boolean_1;
    static const sc_lv<1> ap_const_lv1_0;
    static const sc_lv<1> ap_const_lv1_1;
    static const bool ap_const_boolean_0;
    static const sc_lv<16> ap_const_lv16_0;
    static const sc_lv<33> ap_const_lv33_100001E85;
    static const sc_lv<33> ap_const_lv33_0;
    static const sc_lv<16> ap_const_lv16_1;
    static const sc_lv<16> ap_const_lv16_3E8;
    static const sc_lv<16> ap_const_lv16_FFFF;
    static const sc_lv<32> ap_const_lv32_20;
    static const sc_lv<32> ap_const_lv32_FFFFFFFF;
    static const sc_lv<32> ap_const_lv32_1F;
    static const sc_lv<37> ap_const_lv37_0;
    static const sc_lv<3> ap_const_lv3_1;
    // Thread declarations
    void thread_ap_var_for_const0();
    void thread_ap_clk_no_reset_();
    void thread_add_ln700_fu_191_p2();
    void thread_add_ln701_1_fu_291_p2();
    void thread_add_ln701_fu_247_p2();
    void thread_ap_CS_fsm_pp0_stage0();
    void thread_ap_block_pp0();
    void thread_ap_block_pp0_stage0();
    void thread_ap_block_pp0_stage0_01001();
    void thread_ap_block_pp0_stage0_11001();
    void thread_ap_block_pp0_stage0_subdone();
    void thread_ap_block_state1_pp0_stage0_iter0();
    void thread_ap_block_state2_pp0_stage0_iter1();
    void thread_ap_block_state3_pp0_stage0_iter2();
    void thread_ap_block_state4_pp0_stage0_iter3();
    void thread_ap_condition_108();
    void thread_ap_condition_188();
    void thread_ap_condition_202();
    void thread_ap_condition_373();
    void thread_ap_condition_53();
    void thread_ap_done();
    void thread_ap_enable_operation_33();
    void thread_ap_enable_operation_36();
    void thread_ap_enable_operation_37();
    void thread_ap_enable_operation_54();
    void thread_ap_enable_operation_56();
    void thread_ap_enable_pp0();
    void thread_ap_enable_reg_pp0_iter0();
    void thread_ap_enable_state2_pp0_iter1_stage0();
    void thread_ap_enable_state3_pp0_iter2_stage0();
    void thread_ap_enable_state4_pp0_iter3_stage0();
    void thread_ap_idle();
    void thread_ap_idle_pp0();
    void thread_ap_idle_pp0_0to2();
    void thread_ap_phi_mux_tmp_sessionID_V_phi_fu_153_p4();
    void thread_ap_phi_reg_pp0_iter0_fastResume_0_i_reg_161();
    void thread_ap_phi_reg_pp0_iter0_tmp_sessionID_V_reg_150();
    void thread_ap_predicate_op19_read_state1();
    void thread_ap_predicate_op23_read_state1();
    void thread_ap_predicate_op33_load_state2();
    void thread_ap_predicate_op36_store_state2();
    void thread_ap_predicate_op37_load_state3();
    void thread_ap_predicate_op54_store_state4();
    void thread_ap_predicate_op56_store_state4();
    void thread_ap_predicate_op58_write_state4();
    void thread_ap_ready();
    void thread_ap_reset_idle_pp0();
    void thread_icmp_ln879_16_fu_279_p2();
    void thread_icmp_ln879_fu_197_p2();
    void thread_icmp_ln883_fu_235_p2();
    void thread_or_ln101_fu_285_p2();
    void thread_probeTimer2eventEng_1_blk_n();
    void thread_probeTimer2eventEng_1_din();
    void thread_probeTimer2eventEng_1_write();
    void thread_probeTimerTable_address0();
    void thread_probeTimerTable_address1();
    void thread_probeTimerTable_ce0();
    void thread_probeTimerTable_ce1();
    void thread_probeTimerTable_d1();
    void thread_probeTimerTable_time_1_fu_297_p5();
    void thread_probeTimerTable_we0();
    void thread_probeTimerTable_we1();
    void thread_pt_WaitForWrite_load_load_fu_174_p1();
    void thread_rxEng2timer_clearPro_1_blk_n();
    void thread_rxEng2timer_clearPro_1_read();
    void thread_select_ln92_fu_203_p3();
    void thread_tmp_211_nbreadreq_fu_88_p3();
    void thread_tmp_213_nbwritereq_fu_108_p3();
    void thread_tmp_nbreadreq_fu_80_p3();
    void thread_trunc_ln879_fu_276_p1();
    void thread_txEng2timer_setProbe_1_blk_n();
    void thread_txEng2timer_setProbe_1_read();
    void thread_zext_ln544_19_fu_259_p1();
    void thread_zext_ln544_fu_264_p1();
    void thread_ap_NS_fsm();
};

}

using namespace ap_rtl;

#endif
