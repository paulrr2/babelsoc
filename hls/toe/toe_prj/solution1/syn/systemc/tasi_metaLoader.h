// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
// Version: 2019.2.1
// Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

#ifndef _tasi_metaLoader_HH_
#define _tasi_metaLoader_HH_

#include "systemc.h"
#include "AESL_pkg.h"


namespace ap_rtl {

struct tasi_metaLoader : public sc_module {
    // Port declarations 31
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_in< sc_logic > ap_start;
    sc_out< sc_logic > ap_done;
    sc_in< sc_logic > ap_continue;
    sc_out< sc_logic > ap_idle;
    sc_out< sc_logic > ap_ready;
    sc_in< sc_logic > appTxDataReqMetaData_V_TVALID;
    sc_in< sc_lv<4> > stateTable2txApp_rsp_1_dout;
    sc_in< sc_logic > stateTable2txApp_rsp_1_empty_n;
    sc_out< sc_logic > stateTable2txApp_rsp_1_read;
    sc_in< sc_lv<70> > txSar2txApp_upd_rsp_s_1_dout;
    sc_in< sc_logic > txSar2txApp_upd_rsp_s_1_empty_n;
    sc_out< sc_logic > txSar2txApp_upd_rsp_s_1_read;
    sc_out< sc_lv<16> > txApp2stateTable_req_1_din;
    sc_in< sc_logic > txApp2stateTable_req_1_full_n;
    sc_out< sc_logic > txApp2stateTable_req_1_write;
    sc_out< sc_lv<35> > txApp2txSar_upd_req_s_11_din;
    sc_in< sc_logic > txApp2txSar_upd_req_s_11_full_n;
    sc_out< sc_logic > txApp2txSar_upd_req_s_11_write;
    sc_out< sc_lv<72> > tasi_meta2pkgPushCmd_1_din;
    sc_in< sc_logic > tasi_meta2pkgPushCmd_1_full_n;
    sc_out< sc_logic > tasi_meta2pkgPushCmd_1_write;
    sc_out< sc_lv<56> > txAppStream2event_me_1_din;
    sc_in< sc_logic > txAppStream2event_me_1_full_n;
    sc_out< sc_logic > txAppStream2event_me_1_write;
    sc_in< sc_logic > appTxDataRsp_V_TREADY;
    sc_in< sc_lv<32> > appTxDataReqMetaData_V_TDATA;
    sc_out< sc_logic > appTxDataReqMetaData_V_TREADY;
    sc_out< sc_lv<64> > appTxDataRsp_V_TDATA;
    sc_out< sc_logic > appTxDataRsp_V_TVALID;


    // Module declarations
    tasi_metaLoader(sc_module_name name);
    SC_HAS_PROCESS(tasi_metaLoader);

    ~tasi_metaLoader();

    sc_trace_file* mVcdFile;

    regslice_both<64>* regslice_both_appTxDataRsp_V_U;
    sc_signal< sc_logic > ap_done_reg;
    sc_signal< sc_lv<1> > ap_CS_fsm;
    sc_signal< sc_logic > ap_CS_fsm_pp0_stage0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter1;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter2;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter3;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter4;
    sc_signal< sc_logic > ap_idle_pp0;
    sc_signal< sc_lv<1> > tmp_69_nbreadreq_fu_120_p3;
    sc_signal< bool > ap_predicate_op10_read_state1;
    sc_signal< sc_lv<1> > tmp_nbreadreq_fu_134_p3;
    sc_signal< sc_lv<1> > tmp_70_nbreadreq_fu_142_p3;
    sc_signal< bool > ap_predicate_op18_read_state1;
    sc_signal< bool > ap_predicate_op19_read_state1;
    sc_signal< bool > ap_block_state1_pp0_stage0_iter0;
    sc_signal< bool > ap_block_state2_pp0_stage0_iter1;
    sc_signal< bool > ap_block_state3_pp0_stage0_iter2;
    sc_signal< sc_lv<1> > tai_state_load_reg_435;
    sc_signal< sc_lv<1> > tai_state_load_reg_435_pp0_iter2_reg;
    sc_signal< sc_lv<1> > tmp_69_reg_439;
    sc_signal< sc_lv<1> > tmp_69_reg_439_pp0_iter2_reg;
    sc_signal< bool > ap_predicate_op41_write_state4;
    sc_signal< bool > ap_predicate_op43_write_state4;
    sc_signal< sc_lv<1> > tmp_reg_455;
    sc_signal< sc_lv<1> > tmp_reg_455_pp0_iter2_reg;
    sc_signal< sc_lv<1> > tmp_70_reg_459;
    sc_signal< sc_lv<1> > tmp_70_reg_459_pp0_iter2_reg;
    sc_signal< sc_lv<1> > icmp_ln86_reg_484;
    sc_signal< sc_lv<1> > icmp_ln86_reg_484_pp0_iter2_reg;
    sc_signal< sc_lv<1> > or_ln98_reg_515;
    sc_signal< bool > ap_predicate_op50_write_state4;
    sc_signal< bool > ap_predicate_op55_write_state4;
    sc_signal< bool > ap_predicate_op58_write_state4;
    sc_signal< bool > ap_block_state4_pp0_stage0_iter3;
    sc_signal< bool > ap_predicate_op46_write_state4;
    sc_signal< bool > ap_predicate_op53_write_state4;
    sc_signal< bool > ap_predicate_op61_write_state4;
    sc_signal< bool > ap_block_state4_io;
    sc_signal< sc_logic > regslice_both_appTxDataRsp_V_U_apdone_blk;
    sc_signal< bool > ap_block_state5_pp0_stage0_iter4;
    sc_signal< sc_lv<1> > tai_state_load_reg_435_pp0_iter3_reg;
    sc_signal< sc_lv<1> > tmp_reg_455_pp0_iter3_reg;
    sc_signal< sc_lv<1> > tmp_70_reg_459_pp0_iter3_reg;
    sc_signal< sc_lv<1> > icmp_ln86_reg_484_pp0_iter3_reg;
    sc_signal< bool > ap_predicate_op73_write_state5;
    sc_signal< sc_lv<1> > or_ln98_reg_515_pp0_iter3_reg;
    sc_signal< bool > ap_predicate_op75_write_state5;
    sc_signal< bool > ap_predicate_op77_write_state5;
    sc_signal< bool > ap_block_state5_io;
    sc_signal< bool > ap_block_pp0_stage0_11001;
    sc_signal< sc_lv<1> > tai_state;
    sc_signal< sc_lv<16> > tasi_writeMeta_sessi;
    sc_signal< sc_lv<16> > tasi_writeMeta_lengt;
    sc_signal< sc_logic > appTxDataReqMetaData_V_TDATA_blk_n;
    sc_signal< bool > ap_block_pp0_stage0;
    sc_signal< sc_logic > appTxDataRsp_V_TDATA_blk_n;
    sc_signal< sc_logic > txApp2stateTable_req_1_blk_n;
    sc_signal< sc_logic > txApp2txSar_upd_req_s_11_blk_n;
    sc_signal< sc_logic > txSar2txApp_upd_rsp_s_1_blk_n;
    sc_signal< sc_logic > stateTable2txApp_rsp_1_blk_n;
    sc_signal< sc_logic > tasi_meta2pkgPushCmd_1_blk_n;
    sc_signal< sc_logic > txAppStream2event_me_1_blk_n;
    sc_signal< sc_lv<1> > tai_state_load_reg_435_pp0_iter1_reg;
    sc_signal< sc_lv<1> > tmp_69_reg_439_pp0_iter1_reg;
    sc_signal< sc_lv<16> > tmp_V_fu_201_p1;
    sc_signal< sc_lv<16> > tmp_V_reg_443;
    sc_signal< sc_lv<16> > tmp_V_reg_443_pp0_iter1_reg;
    sc_signal< sc_lv<16> > tmp_V_reg_443_pp0_iter2_reg;
    sc_signal< sc_lv<16> > tmp_length_V_load_ne_reg_450;
    sc_signal< sc_lv<16> > tmp_length_V_load_ne_reg_450_pp0_iter1_reg;
    sc_signal< sc_lv<1> > tmp_reg_455_pp0_iter1_reg;
    sc_signal< sc_lv<1> > tmp_70_reg_459_pp0_iter1_reg;
    sc_signal< sc_lv<18> > tmp_ackd_V_load_new_s_reg_463;
    sc_signal< sc_lv<18> > tmp_address_V_reg_469;
    sc_signal< sc_lv<18> > tmp_address_V_reg_469_pp0_iter1_reg;
    sc_signal< sc_lv<18> > tmp_address_V_reg_469_pp0_iter2_reg;
    sc_signal< sc_lv<18> > tmp_min_window_V_loa_reg_478;
    sc_signal< sc_lv<1> > icmp_ln86_fu_251_p2;
    sc_signal< sc_lv<1> > icmp_ln86_reg_484_pp0_iter1_reg;
    sc_signal< sc_lv<18> > maxWriteLength_V_fu_267_p2;
    sc_signal< sc_lv<18> > maxWriteLength_V_reg_488;
    sc_signal< sc_lv<18> > maxWriteLength_V_reg_488_pp0_iter2_reg;
    sc_signal< sc_lv<18> > usableWindow_V_1_fu_287_p3;
    sc_signal< sc_lv<18> > usableWindow_V_1_reg_495;
    sc_signal< sc_lv<18> > usableWindow_V_1_reg_495_pp0_iter2_reg;
    sc_signal< sc_lv<16> > tmp_length_V_reg_501;
    sc_signal< sc_lv<18> > zext_ln895_fu_304_p1;
    sc_signal< sc_lv<18> > zext_ln895_reg_510;
    sc_signal< sc_lv<1> > or_ln98_fu_318_p2;
    sc_signal< sc_lv<64> > tmp_3_fu_344_p5;
    sc_signal< sc_lv<64> > tmp_6_fu_380_p5;
    sc_signal< sc_lv<64> > tmp_4_fu_423_p5;
    sc_signal< bool > ap_block_pp0_stage0_subdone;
    sc_signal< bool > ap_block_pp0_stage0_01001;
    sc_signal< sc_lv<35> > tmp_1_fu_333_p3;
    sc_signal< sc_lv<35> > tmp_8_fu_409_p4;
    sc_signal< sc_lv<18> > sub_ln214_fu_263_p2;
    sc_signal< sc_lv<18> > usedLength_V_fu_273_p2;
    sc_signal< sc_lv<1> > icmp_ln895_fu_277_p2;
    sc_signal< sc_lv<18> > usableWindow_V_fu_282_p2;
    sc_signal< sc_lv<1> > icmp_ln895_2_fu_308_p2;
    sc_signal< sc_lv<1> > icmp_ln887_fu_313_p2;
    sc_signal< sc_lv<30> > tmp_remaining_space_s_fu_341_p1;
    sc_signal< sc_lv<12> > trunc_ln215_fu_356_p1;
    sc_signal< sc_lv<23> > tmp_bbt_V_fu_360_p1;
    sc_signal< sc_lv<30> > tmp_remaining_space_2_fu_377_p1;
    sc_signal< sc_lv<18> > tmp_mempt_V_fu_405_p2;
    sc_signal< sc_lv<30> > tmp_remaining_space_1_fu_420_p1;
    sc_signal< sc_lv<1> > ap_NS_fsm;
    sc_signal< sc_logic > ap_idle_pp0_0to3;
    sc_signal< sc_logic > ap_reset_idle_pp0;
    sc_signal< sc_logic > ap_enable_pp0;
    sc_signal< sc_lv<64> > appTxDataRsp_V_TDATA_int;
    sc_signal< sc_logic > appTxDataRsp_V_TVALID_int;
    sc_signal< sc_logic > appTxDataRsp_V_TREADY_int;
    sc_signal< sc_logic > regslice_both_appTxDataRsp_V_U_vld_out;
    sc_signal< bool > ap_condition_65;
    sc_signal< bool > ap_condition_215;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<1> ap_ST_fsm_pp0_stage0;
    static const sc_lv<32> ap_const_lv32_0;
    static const bool ap_const_boolean_1;
    static const sc_lv<1> ap_const_lv1_0;
    static const sc_lv<1> ap_const_lv1_1;
    static const bool ap_const_boolean_0;
    static const sc_lv<32> ap_const_lv32_10;
    static const sc_lv<32> ap_const_lv32_1F;
    static const sc_lv<32> ap_const_lv32_21;
    static const sc_lv<32> ap_const_lv32_22;
    static const sc_lv<32> ap_const_lv32_33;
    static const sc_lv<32> ap_const_lv32_34;
    static const sc_lv<32> ap_const_lv32_45;
    static const sc_lv<4> ap_const_lv4_3;
    static const sc_lv<18> ap_const_lv18_3FFFF;
    static const sc_lv<18> ap_const_lv18_0;
    static const sc_lv<19> ap_const_lv19_0;
    static const sc_lv<2> ap_const_lv2_1;
    static const sc_lv<10> ap_const_lv10_1;
    static const sc_lv<9> ap_const_lv9_181;
    static const sc_lv<2> ap_const_lv2_0;
    static const sc_lv<3> ap_const_lv3_0;
    static const sc_lv<2> ap_const_lv2_2;
    // Thread declarations
    void thread_ap_clk_no_reset_();
    void thread_ap_CS_fsm_pp0_stage0();
    void thread_ap_block_pp0_stage0();
    void thread_ap_block_pp0_stage0_01001();
    void thread_ap_block_pp0_stage0_11001();
    void thread_ap_block_pp0_stage0_subdone();
    void thread_ap_block_state1_pp0_stage0_iter0();
    void thread_ap_block_state2_pp0_stage0_iter1();
    void thread_ap_block_state3_pp0_stage0_iter2();
    void thread_ap_block_state4_io();
    void thread_ap_block_state4_pp0_stage0_iter3();
    void thread_ap_block_state5_io();
    void thread_ap_block_state5_pp0_stage0_iter4();
    void thread_ap_condition_215();
    void thread_ap_condition_65();
    void thread_ap_done();
    void thread_ap_enable_pp0();
    void thread_ap_enable_reg_pp0_iter0();
    void thread_ap_idle();
    void thread_ap_idle_pp0();
    void thread_ap_idle_pp0_0to3();
    void thread_ap_predicate_op10_read_state1();
    void thread_ap_predicate_op18_read_state1();
    void thread_ap_predicate_op19_read_state1();
    void thread_ap_predicate_op41_write_state4();
    void thread_ap_predicate_op43_write_state4();
    void thread_ap_predicate_op46_write_state4();
    void thread_ap_predicate_op50_write_state4();
    void thread_ap_predicate_op53_write_state4();
    void thread_ap_predicate_op55_write_state4();
    void thread_ap_predicate_op58_write_state4();
    void thread_ap_predicate_op61_write_state4();
    void thread_ap_predicate_op73_write_state5();
    void thread_ap_predicate_op75_write_state5();
    void thread_ap_predicate_op77_write_state5();
    void thread_ap_ready();
    void thread_ap_reset_idle_pp0();
    void thread_appTxDataReqMetaData_V_TDATA_blk_n();
    void thread_appTxDataReqMetaData_V_TREADY();
    void thread_appTxDataRsp_V_TDATA_blk_n();
    void thread_appTxDataRsp_V_TDATA_int();
    void thread_appTxDataRsp_V_TVALID();
    void thread_appTxDataRsp_V_TVALID_int();
    void thread_icmp_ln86_fu_251_p2();
    void thread_icmp_ln887_fu_313_p2();
    void thread_icmp_ln895_2_fu_308_p2();
    void thread_icmp_ln895_fu_277_p2();
    void thread_maxWriteLength_V_fu_267_p2();
    void thread_or_ln98_fu_318_p2();
    void thread_stateTable2txApp_rsp_1_blk_n();
    void thread_stateTable2txApp_rsp_1_read();
    void thread_sub_ln214_fu_263_p2();
    void thread_tasi_meta2pkgPushCmd_1_blk_n();
    void thread_tasi_meta2pkgPushCmd_1_din();
    void thread_tasi_meta2pkgPushCmd_1_write();
    void thread_tmp_1_fu_333_p3();
    void thread_tmp_3_fu_344_p5();
    void thread_tmp_4_fu_423_p5();
    void thread_tmp_69_nbreadreq_fu_120_p3();
    void thread_tmp_6_fu_380_p5();
    void thread_tmp_70_nbreadreq_fu_142_p3();
    void thread_tmp_8_fu_409_p4();
    void thread_tmp_V_fu_201_p1();
    void thread_tmp_bbt_V_fu_360_p1();
    void thread_tmp_mempt_V_fu_405_p2();
    void thread_tmp_nbreadreq_fu_134_p3();
    void thread_tmp_remaining_space_1_fu_420_p1();
    void thread_tmp_remaining_space_2_fu_377_p1();
    void thread_tmp_remaining_space_s_fu_341_p1();
    void thread_trunc_ln215_fu_356_p1();
    void thread_txApp2stateTable_req_1_blk_n();
    void thread_txApp2stateTable_req_1_din();
    void thread_txApp2stateTable_req_1_write();
    void thread_txApp2txSar_upd_req_s_11_blk_n();
    void thread_txApp2txSar_upd_req_s_11_din();
    void thread_txApp2txSar_upd_req_s_11_write();
    void thread_txAppStream2event_me_1_blk_n();
    void thread_txAppStream2event_me_1_din();
    void thread_txAppStream2event_me_1_write();
    void thread_txSar2txApp_upd_rsp_s_1_blk_n();
    void thread_txSar2txApp_upd_rsp_s_1_read();
    void thread_usableWindow_V_1_fu_287_p3();
    void thread_usableWindow_V_fu_282_p2();
    void thread_usedLength_V_fu_273_p2();
    void thread_zext_ln895_fu_304_p1();
    void thread_ap_NS_fsm();
};

}

using namespace ap_rtl;

#endif
