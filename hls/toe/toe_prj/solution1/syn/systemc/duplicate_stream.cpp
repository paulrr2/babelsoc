// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
// Version: 2019.2.1
// Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

#include "duplicate_stream.h"
#include "AESL_pkg.h"

using namespace std;

namespace ap_rtl {

const sc_logic duplicate_stream::ap_const_logic_1 = sc_dt::Log_1;
const sc_logic duplicate_stream::ap_const_logic_0 = sc_dt::Log_0;
const sc_lv<1> duplicate_stream::ap_ST_fsm_pp0_stage0 = "1";
const sc_lv<32> duplicate_stream::ap_const_lv32_0 = "00000000000000000000000000000000";
const bool duplicate_stream::ap_const_boolean_1 = true;
const sc_lv<1> duplicate_stream::ap_const_lv1_1 = "1";
const bool duplicate_stream::ap_const_boolean_0 = false;

duplicate_stream::duplicate_stream(sc_module_name name) : sc_module(name), mVcdFile(0) {

    SC_METHOD(thread_ap_clk_no_reset_);
    dont_initialize();
    sensitive << ( ap_clk.pos() );

    SC_METHOD(thread_ap_CS_fsm_pp0_stage0);
    sensitive << ( ap_CS_fsm );

    SC_METHOD(thread_ap_block_pp0_stage0);

    SC_METHOD(thread_ap_block_pp0_stage0_01001);
    sensitive << ( ap_start );
    sensitive << ( ap_done_reg );
    sensitive << ( ap_enable_reg_pp0_iter1 );
    sensitive << ( s_axis_tx_data_req_TVALID );
    sensitive << ( tmp_nbreadreq_fu_48_p5 );
    sensitive << ( tasi_dataFifo_V_full_n );
    sensitive << ( tmp_reg_110 );
    sensitive << ( io_acc_block_signal_op17 );

    SC_METHOD(thread_ap_block_pp0_stage0_11001);
    sensitive << ( ap_start );
    sensitive << ( ap_done_reg );
    sensitive << ( ap_enable_reg_pp0_iter1 );
    sensitive << ( s_axis_tx_data_req_TVALID );
    sensitive << ( tmp_nbreadreq_fu_48_p5 );
    sensitive << ( tasi_dataFifo_V_full_n );
    sensitive << ( tmp_reg_110 );
    sensitive << ( io_acc_block_signal_op17 );

    SC_METHOD(thread_ap_block_pp0_stage0_subdone);
    sensitive << ( ap_start );
    sensitive << ( ap_done_reg );
    sensitive << ( ap_enable_reg_pp0_iter1 );
    sensitive << ( s_axis_tx_data_req_TVALID );
    sensitive << ( tmp_nbreadreq_fu_48_p5 );
    sensitive << ( tasi_dataFifo_V_full_n );
    sensitive << ( tmp_reg_110 );
    sensitive << ( io_acc_block_signal_op17 );

    SC_METHOD(thread_ap_block_state1_pp0_stage0_iter0);
    sensitive << ( ap_start );
    sensitive << ( ap_done_reg );
    sensitive << ( s_axis_tx_data_req_TVALID );
    sensitive << ( tmp_nbreadreq_fu_48_p5 );

    SC_METHOD(thread_ap_block_state2_pp0_stage0_iter1);
    sensitive << ( tasi_dataFifo_V_full_n );
    sensitive << ( tmp_reg_110 );
    sensitive << ( io_acc_block_signal_op17 );

    SC_METHOD(thread_ap_done);
    sensitive << ( ap_done_reg );
    sensitive << ( ap_CS_fsm_pp0_stage0 );
    sensitive << ( ap_enable_reg_pp0_iter1 );
    sensitive << ( ap_block_pp0_stage0_11001 );

    SC_METHOD(thread_ap_enable_pp0);
    sensitive << ( ap_idle_pp0 );

    SC_METHOD(thread_ap_enable_reg_pp0_iter0);
    sensitive << ( ap_start );

    SC_METHOD(thread_ap_idle);
    sensitive << ( ap_start );
    sensitive << ( ap_CS_fsm_pp0_stage0 );
    sensitive << ( ap_idle_pp0 );

    SC_METHOD(thread_ap_idle_pp0);
    sensitive << ( ap_enable_reg_pp0_iter0 );
    sensitive << ( ap_enable_reg_pp0_iter1 );

    SC_METHOD(thread_ap_idle_pp0_0to0);
    sensitive << ( ap_enable_reg_pp0_iter0 );

    SC_METHOD(thread_ap_ready);
    sensitive << ( ap_start );
    sensitive << ( ap_CS_fsm_pp0_stage0 );
    sensitive << ( ap_block_pp0_stage0_11001 );

    SC_METHOD(thread_ap_reset_idle_pp0);
    sensitive << ( ap_start );
    sensitive << ( ap_idle_pp0_0to0 );

    SC_METHOD(thread_io_acc_block_signal_op17);
    sensitive << ( txApp2txEng_data_str_3_full_n );
    sensitive << ( txApp2txEng_data_str_5_full_n );
    sensitive << ( txApp2txEng_data_str_6_full_n );

    SC_METHOD(thread_s_axis_tx_data_req_TDATA_blk_n);
    sensitive << ( ap_start );
    sensitive << ( ap_done_reg );
    sensitive << ( ap_CS_fsm_pp0_stage0 );
    sensitive << ( s_axis_tx_data_req_TVALID );
    sensitive << ( tmp_nbreadreq_fu_48_p5 );
    sensitive << ( ap_block_pp0_stage0 );

    SC_METHOD(thread_s_axis_tx_data_req_TREADY);
    sensitive << ( ap_start );
    sensitive << ( ap_CS_fsm_pp0_stage0 );
    sensitive << ( tmp_nbreadreq_fu_48_p5 );
    sensitive << ( ap_block_pp0_stage0_11001 );

    SC_METHOD(thread_tasi_dataFifo_V_blk_n);
    sensitive << ( ap_CS_fsm_pp0_stage0 );
    sensitive << ( ap_enable_reg_pp0_iter1 );
    sensitive << ( tasi_dataFifo_V_full_n );
    sensitive << ( tmp_reg_110 );
    sensitive << ( ap_block_pp0_stage0 );

    SC_METHOD(thread_tasi_dataFifo_V_din);
    sensitive << ( ap_CS_fsm_pp0_stage0 );
    sensitive << ( ap_enable_reg_pp0_iter1 );
    sensitive << ( tmp_reg_110 );
    sensitive << ( tmp_data_V_reg_114 );
    sensitive << ( tmp_keep_V_reg_120 );
    sensitive << ( tmp_last_V_reg_126 );
    sensitive << ( ap_block_pp0_stage0_01001 );

    SC_METHOD(thread_tasi_dataFifo_V_write);
    sensitive << ( ap_CS_fsm_pp0_stage0 );
    sensitive << ( ap_enable_reg_pp0_iter1 );
    sensitive << ( tmp_reg_110 );
    sensitive << ( ap_block_pp0_stage0_11001 );

    SC_METHOD(thread_tmp_nbreadreq_fu_48_p5);
    sensitive << ( s_axis_tx_data_req_TVALID );

    SC_METHOD(thread_txApp2txEng_data_str_3_blk_n);
    sensitive << ( ap_CS_fsm_pp0_stage0 );
    sensitive << ( ap_enable_reg_pp0_iter1 );
    sensitive << ( tmp_reg_110 );
    sensitive << ( txApp2txEng_data_str_3_full_n );
    sensitive << ( ap_block_pp0_stage0 );

    SC_METHOD(thread_txApp2txEng_data_str_3_din);
    sensitive << ( ap_CS_fsm_pp0_stage0 );
    sensitive << ( ap_enable_reg_pp0_iter1 );
    sensitive << ( tmp_reg_110 );
    sensitive << ( tmp_data_V_reg_114 );
    sensitive << ( ap_block_pp0_stage0_01001 );

    SC_METHOD(thread_txApp2txEng_data_str_3_write);
    sensitive << ( ap_CS_fsm_pp0_stage0 );
    sensitive << ( ap_enable_reg_pp0_iter1 );
    sensitive << ( tmp_reg_110 );
    sensitive << ( ap_block_pp0_stage0_11001 );

    SC_METHOD(thread_txApp2txEng_data_str_5_blk_n);
    sensitive << ( ap_CS_fsm_pp0_stage0 );
    sensitive << ( ap_enable_reg_pp0_iter1 );
    sensitive << ( tmp_reg_110 );
    sensitive << ( txApp2txEng_data_str_5_full_n );
    sensitive << ( ap_block_pp0_stage0 );

    SC_METHOD(thread_txApp2txEng_data_str_5_din);
    sensitive << ( ap_CS_fsm_pp0_stage0 );
    sensitive << ( ap_enable_reg_pp0_iter1 );
    sensitive << ( tmp_reg_110 );
    sensitive << ( tmp_keep_V_reg_120 );
    sensitive << ( ap_block_pp0_stage0_01001 );

    SC_METHOD(thread_txApp2txEng_data_str_5_write);
    sensitive << ( ap_CS_fsm_pp0_stage0 );
    sensitive << ( ap_enable_reg_pp0_iter1 );
    sensitive << ( tmp_reg_110 );
    sensitive << ( ap_block_pp0_stage0_11001 );

    SC_METHOD(thread_txApp2txEng_data_str_6_blk_n);
    sensitive << ( ap_CS_fsm_pp0_stage0 );
    sensitive << ( ap_enable_reg_pp0_iter1 );
    sensitive << ( tmp_reg_110 );
    sensitive << ( txApp2txEng_data_str_6_full_n );
    sensitive << ( ap_block_pp0_stage0 );

    SC_METHOD(thread_txApp2txEng_data_str_6_din);
    sensitive << ( ap_CS_fsm_pp0_stage0 );
    sensitive << ( ap_enable_reg_pp0_iter1 );
    sensitive << ( tmp_reg_110 );
    sensitive << ( tmp_last_V_reg_126 );
    sensitive << ( ap_block_pp0_stage0_01001 );

    SC_METHOD(thread_txApp2txEng_data_str_6_write);
    sensitive << ( ap_CS_fsm_pp0_stage0 );
    sensitive << ( ap_enable_reg_pp0_iter1 );
    sensitive << ( tmp_reg_110 );
    sensitive << ( ap_block_pp0_stage0_11001 );

    SC_METHOD(thread_ap_NS_fsm);
    sensitive << ( ap_CS_fsm );
    sensitive << ( ap_block_pp0_stage0_subdone );
    sensitive << ( ap_reset_idle_pp0 );

    ap_done_reg = SC_LOGIC_0;
    ap_CS_fsm = "1";
    ap_enable_reg_pp0_iter1 = SC_LOGIC_0;
    static int apTFileNum = 0;
    stringstream apTFilenSS;
    apTFilenSS << "duplicate_stream_sc_trace_" << apTFileNum ++;
    string apTFn = apTFilenSS.str();
    mVcdFile = sc_create_vcd_trace_file(apTFn.c_str());
    mVcdFile->set_time_unit(1, SC_PS);
    if (1) {
#ifdef __HLS_TRACE_LEVEL_PORT_HIER__
    sc_trace(mVcdFile, ap_clk, "(port)ap_clk");
    sc_trace(mVcdFile, ap_rst, "(port)ap_rst");
    sc_trace(mVcdFile, ap_start, "(port)ap_start");
    sc_trace(mVcdFile, ap_done, "(port)ap_done");
    sc_trace(mVcdFile, ap_continue, "(port)ap_continue");
    sc_trace(mVcdFile, ap_idle, "(port)ap_idle");
    sc_trace(mVcdFile, ap_ready, "(port)ap_ready");
    sc_trace(mVcdFile, s_axis_tx_data_req_TVALID, "(port)s_axis_tx_data_req_TVALID");
    sc_trace(mVcdFile, tasi_dataFifo_V_din, "(port)tasi_dataFifo_V_din");
    sc_trace(mVcdFile, tasi_dataFifo_V_full_n, "(port)tasi_dataFifo_V_full_n");
    sc_trace(mVcdFile, tasi_dataFifo_V_write, "(port)tasi_dataFifo_V_write");
    sc_trace(mVcdFile, txApp2txEng_data_str_3_din, "(port)txApp2txEng_data_str_3_din");
    sc_trace(mVcdFile, txApp2txEng_data_str_3_full_n, "(port)txApp2txEng_data_str_3_full_n");
    sc_trace(mVcdFile, txApp2txEng_data_str_3_write, "(port)txApp2txEng_data_str_3_write");
    sc_trace(mVcdFile, txApp2txEng_data_str_5_din, "(port)txApp2txEng_data_str_5_din");
    sc_trace(mVcdFile, txApp2txEng_data_str_5_full_n, "(port)txApp2txEng_data_str_5_full_n");
    sc_trace(mVcdFile, txApp2txEng_data_str_5_write, "(port)txApp2txEng_data_str_5_write");
    sc_trace(mVcdFile, txApp2txEng_data_str_6_din, "(port)txApp2txEng_data_str_6_din");
    sc_trace(mVcdFile, txApp2txEng_data_str_6_full_n, "(port)txApp2txEng_data_str_6_full_n");
    sc_trace(mVcdFile, txApp2txEng_data_str_6_write, "(port)txApp2txEng_data_str_6_write");
    sc_trace(mVcdFile, s_axis_tx_data_req_TDATA, "(port)s_axis_tx_data_req_TDATA");
    sc_trace(mVcdFile, s_axis_tx_data_req_TREADY, "(port)s_axis_tx_data_req_TREADY");
    sc_trace(mVcdFile, s_axis_tx_data_req_TKEEP, "(port)s_axis_tx_data_req_TKEEP");
    sc_trace(mVcdFile, s_axis_tx_data_req_TLAST, "(port)s_axis_tx_data_req_TLAST");
#endif
#ifdef __HLS_TRACE_LEVEL_INT__
    sc_trace(mVcdFile, ap_done_reg, "ap_done_reg");
    sc_trace(mVcdFile, ap_CS_fsm, "ap_CS_fsm");
    sc_trace(mVcdFile, ap_CS_fsm_pp0_stage0, "ap_CS_fsm_pp0_stage0");
    sc_trace(mVcdFile, ap_enable_reg_pp0_iter0, "ap_enable_reg_pp0_iter0");
    sc_trace(mVcdFile, ap_enable_reg_pp0_iter1, "ap_enable_reg_pp0_iter1");
    sc_trace(mVcdFile, ap_idle_pp0, "ap_idle_pp0");
    sc_trace(mVcdFile, tmp_nbreadreq_fu_48_p5, "tmp_nbreadreq_fu_48_p5");
    sc_trace(mVcdFile, ap_block_state1_pp0_stage0_iter0, "ap_block_state1_pp0_stage0_iter0");
    sc_trace(mVcdFile, tmp_reg_110, "tmp_reg_110");
    sc_trace(mVcdFile, io_acc_block_signal_op17, "io_acc_block_signal_op17");
    sc_trace(mVcdFile, ap_block_state2_pp0_stage0_iter1, "ap_block_state2_pp0_stage0_iter1");
    sc_trace(mVcdFile, ap_block_pp0_stage0_11001, "ap_block_pp0_stage0_11001");
    sc_trace(mVcdFile, s_axis_tx_data_req_TDATA_blk_n, "s_axis_tx_data_req_TDATA_blk_n");
    sc_trace(mVcdFile, ap_block_pp0_stage0, "ap_block_pp0_stage0");
    sc_trace(mVcdFile, tasi_dataFifo_V_blk_n, "tasi_dataFifo_V_blk_n");
    sc_trace(mVcdFile, txApp2txEng_data_str_3_blk_n, "txApp2txEng_data_str_3_blk_n");
    sc_trace(mVcdFile, txApp2txEng_data_str_5_blk_n, "txApp2txEng_data_str_5_blk_n");
    sc_trace(mVcdFile, txApp2txEng_data_str_6_blk_n, "txApp2txEng_data_str_6_blk_n");
    sc_trace(mVcdFile, tmp_data_V_reg_114, "tmp_data_V_reg_114");
    sc_trace(mVcdFile, tmp_keep_V_reg_120, "tmp_keep_V_reg_120");
    sc_trace(mVcdFile, tmp_last_V_reg_126, "tmp_last_V_reg_126");
    sc_trace(mVcdFile, ap_block_pp0_stage0_subdone, "ap_block_pp0_stage0_subdone");
    sc_trace(mVcdFile, ap_block_pp0_stage0_01001, "ap_block_pp0_stage0_01001");
    sc_trace(mVcdFile, ap_NS_fsm, "ap_NS_fsm");
    sc_trace(mVcdFile, ap_idle_pp0_0to0, "ap_idle_pp0_0to0");
    sc_trace(mVcdFile, ap_reset_idle_pp0, "ap_reset_idle_pp0");
    sc_trace(mVcdFile, ap_enable_pp0, "ap_enable_pp0");
#endif

    }
}

duplicate_stream::~duplicate_stream() {
    if (mVcdFile) 
        sc_close_vcd_trace_file(mVcdFile);

}

void duplicate_stream::thread_ap_clk_no_reset_() {
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_CS_fsm = ap_ST_fsm_pp0_stage0;
    } else {
        ap_CS_fsm = ap_NS_fsm.read();
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_done_reg = ap_const_logic_0;
    } else {
        if (esl_seteq<1,1,1>(ap_const_logic_1, ap_continue.read())) {
            ap_done_reg = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter1.read()) && 
                    esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0))) {
            ap_done_reg = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_enable_reg_pp0_iter1 = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
             esl_seteq<1,1,1>(ap_block_pp0_stage0_subdone.read(), ap_const_boolean_0))) {
            ap_enable_reg_pp0_iter1 = ap_start.read();
        }
    }
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && esl_seteq<1,1,1>(tmp_nbreadreq_fu_48_p5.read(), ap_const_lv1_1) && esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0))) {
        tmp_data_V_reg_114 = s_axis_tx_data_req_TDATA.read();
        tmp_keep_V_reg_120 = s_axis_tx_data_req_TKEEP.read();
        tmp_last_V_reg_126 = s_axis_tx_data_req_TLAST.read();
    }
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0))) {
        tmp_reg_110 = tmp_nbreadreq_fu_48_p5.read();
    }
}

void duplicate_stream::thread_ap_CS_fsm_pp0_stage0() {
    ap_CS_fsm_pp0_stage0 = ap_CS_fsm.read()[0];
}

void duplicate_stream::thread_ap_block_pp0_stage0() {
    ap_block_pp0_stage0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void duplicate_stream::thread_ap_block_pp0_stage0_01001() {
    ap_block_pp0_stage0_01001 = (esl_seteq<1,1,1>(ap_done_reg.read(), ap_const_logic_1) || (esl_seteq<1,1,1>(ap_const_logic_1, ap_start.read()) && 
  (esl_seteq<1,1,1>(ap_const_logic_0, ap_start.read()) || 
   (esl_seteq<1,1,1>(ap_const_logic_0, s_axis_tx_data_req_TVALID.read()) && 
    esl_seteq<1,1,1>(tmp_nbreadreq_fu_48_p5.read(), ap_const_lv1_1)) || 
   esl_seteq<1,1,1>(ap_done_reg.read(), ap_const_logic_1))) || (((esl_seteq<1,1,1>(ap_const_logic_0, tasi_dataFifo_V_full_n.read()) && 
    esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_110.read())) || 
   (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_110.read()) && 
    esl_seteq<1,1,1>(ap_const_logic_0, io_acc_block_signal_op17.read()))) && 
  esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter1.read())));
}

void duplicate_stream::thread_ap_block_pp0_stage0_11001() {
    ap_block_pp0_stage0_11001 = (esl_seteq<1,1,1>(ap_done_reg.read(), ap_const_logic_1) || (esl_seteq<1,1,1>(ap_const_logic_1, ap_start.read()) && 
  (esl_seteq<1,1,1>(ap_const_logic_0, ap_start.read()) || 
   (esl_seteq<1,1,1>(ap_const_logic_0, s_axis_tx_data_req_TVALID.read()) && 
    esl_seteq<1,1,1>(tmp_nbreadreq_fu_48_p5.read(), ap_const_lv1_1)) || 
   esl_seteq<1,1,1>(ap_done_reg.read(), ap_const_logic_1))) || (((esl_seteq<1,1,1>(ap_const_logic_0, tasi_dataFifo_V_full_n.read()) && 
    esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_110.read())) || 
   (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_110.read()) && 
    esl_seteq<1,1,1>(ap_const_logic_0, io_acc_block_signal_op17.read()))) && 
  esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter1.read())));
}

void duplicate_stream::thread_ap_block_pp0_stage0_subdone() {
    ap_block_pp0_stage0_subdone = (esl_seteq<1,1,1>(ap_done_reg.read(), ap_const_logic_1) || (esl_seteq<1,1,1>(ap_const_logic_1, ap_start.read()) && 
  (esl_seteq<1,1,1>(ap_const_logic_0, ap_start.read()) || 
   (esl_seteq<1,1,1>(ap_const_logic_0, s_axis_tx_data_req_TVALID.read()) && 
    esl_seteq<1,1,1>(tmp_nbreadreq_fu_48_p5.read(), ap_const_lv1_1)) || 
   esl_seteq<1,1,1>(ap_done_reg.read(), ap_const_logic_1))) || (((esl_seteq<1,1,1>(ap_const_logic_0, tasi_dataFifo_V_full_n.read()) && 
    esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_110.read())) || 
   (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_110.read()) && 
    esl_seteq<1,1,1>(ap_const_logic_0, io_acc_block_signal_op17.read()))) && 
  esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter1.read())));
}

void duplicate_stream::thread_ap_block_state1_pp0_stage0_iter0() {
    ap_block_state1_pp0_stage0_iter0 = (esl_seteq<1,1,1>(ap_const_logic_0, ap_start.read()) || (esl_seteq<1,1,1>(ap_const_logic_0, s_axis_tx_data_req_TVALID.read()) && 
  esl_seteq<1,1,1>(tmp_nbreadreq_fu_48_p5.read(), ap_const_lv1_1)) || esl_seteq<1,1,1>(ap_done_reg.read(), ap_const_logic_1));
}

void duplicate_stream::thread_ap_block_state2_pp0_stage0_iter1() {
    ap_block_state2_pp0_stage0_iter1 = ((esl_seteq<1,1,1>(ap_const_logic_0, tasi_dataFifo_V_full_n.read()) && 
  esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_110.read())) || (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_110.read()) && 
  esl_seteq<1,1,1>(ap_const_logic_0, io_acc_block_signal_op17.read())));
}

void duplicate_stream::thread_ap_done() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter1.read()) && 
         esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0))) {
        ap_done = ap_const_logic_1;
    } else {
        ap_done = ap_done_reg.read();
    }
}

void duplicate_stream::thread_ap_enable_pp0() {
    ap_enable_pp0 = (ap_idle_pp0.read() ^ ap_const_logic_1);
}

void duplicate_stream::thread_ap_enable_reg_pp0_iter0() {
    ap_enable_reg_pp0_iter0 = ap_start.read();
}

void duplicate_stream::thread_ap_idle() {
    if ((esl_seteq<1,1,1>(ap_const_logic_0, ap_start.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_idle_pp0.read()))) {
        ap_idle = ap_const_logic_1;
    } else {
        ap_idle = ap_const_logic_0;
    }
}

void duplicate_stream::thread_ap_idle_pp0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_0, ap_enable_reg_pp0_iter0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, ap_enable_reg_pp0_iter1.read()))) {
        ap_idle_pp0 = ap_const_logic_1;
    } else {
        ap_idle_pp0 = ap_const_logic_0;
    }
}

void duplicate_stream::thread_ap_idle_pp0_0to0() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_enable_reg_pp0_iter0.read())) {
        ap_idle_pp0_0to0 = ap_const_logic_1;
    } else {
        ap_idle_pp0_0to0 = ap_const_logic_0;
    }
}

void duplicate_stream::thread_ap_ready() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_start.read()) && 
         esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0))) {
        ap_ready = ap_const_logic_1;
    } else {
        ap_ready = ap_const_logic_0;
    }
}

void duplicate_stream::thread_ap_reset_idle_pp0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_0, ap_start.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_idle_pp0_0to0.read()))) {
        ap_reset_idle_pp0 = ap_const_logic_1;
    } else {
        ap_reset_idle_pp0 = ap_const_logic_0;
    }
}

void duplicate_stream::thread_io_acc_block_signal_op17() {
    io_acc_block_signal_op17 = (txApp2txEng_data_str_3_full_n.read() & txApp2txEng_data_str_5_full_n.read() & txApp2txEng_data_str_6_full_n.read());
}

void duplicate_stream::thread_s_axis_tx_data_req_TDATA_blk_n() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_start.read()) && 
         esl_seteq<1,1,1>(tmp_nbreadreq_fu_48_p5.read(), ap_const_lv1_1) && 
         !(esl_seteq<1,1,1>(ap_const_logic_0, ap_start.read()) || esl_seteq<1,1,1>(ap_done_reg.read(), ap_const_logic_1)) && 
         esl_seteq<1,1,1>(ap_block_pp0_stage0.read(), ap_const_boolean_0))) {
        s_axis_tx_data_req_TDATA_blk_n = s_axis_tx_data_req_TVALID.read();
    } else {
        s_axis_tx_data_req_TDATA_blk_n = ap_const_logic_1;
    }
}

void duplicate_stream::thread_s_axis_tx_data_req_TREADY() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_start.read()) && 
         esl_seteq<1,1,1>(tmp_nbreadreq_fu_48_p5.read(), ap_const_lv1_1) && 
         esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0))) {
        s_axis_tx_data_req_TREADY = ap_const_logic_1;
    } else {
        s_axis_tx_data_req_TREADY = ap_const_logic_0;
    }
}

void duplicate_stream::thread_tasi_dataFifo_V_blk_n() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_110.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter1.read()) && 
         esl_seteq<1,1,1>(ap_block_pp0_stage0.read(), ap_const_boolean_0))) {
        tasi_dataFifo_V_blk_n = tasi_dataFifo_V_full_n.read();
    } else {
        tasi_dataFifo_V_blk_n = ap_const_logic_1;
    }
}

void duplicate_stream::thread_tasi_dataFifo_V_din() {
    tasi_dataFifo_V_din = esl_concat<9,64>(esl_concat<1,8>(tmp_last_V_reg_126.read(), tmp_keep_V_reg_120.read()), tmp_data_V_reg_114.read());
}

void duplicate_stream::thread_tasi_dataFifo_V_write() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_110.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter1.read()) && 
         esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0))) {
        tasi_dataFifo_V_write = ap_const_logic_1;
    } else {
        tasi_dataFifo_V_write = ap_const_logic_0;
    }
}

void duplicate_stream::thread_tmp_nbreadreq_fu_48_p5() {
    tmp_nbreadreq_fu_48_p5 =  (sc_lv<1>) ((s_axis_tx_data_req_TVALID.read()));
}

void duplicate_stream::thread_txApp2txEng_data_str_3_blk_n() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_110.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter1.read()) && 
         esl_seteq<1,1,1>(ap_block_pp0_stage0.read(), ap_const_boolean_0))) {
        txApp2txEng_data_str_3_blk_n = txApp2txEng_data_str_3_full_n.read();
    } else {
        txApp2txEng_data_str_3_blk_n = ap_const_logic_1;
    }
}

void duplicate_stream::thread_txApp2txEng_data_str_3_din() {
    txApp2txEng_data_str_3_din = tmp_data_V_reg_114.read();
}

void duplicate_stream::thread_txApp2txEng_data_str_3_write() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_110.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter1.read()) && 
         esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0))) {
        txApp2txEng_data_str_3_write = ap_const_logic_1;
    } else {
        txApp2txEng_data_str_3_write = ap_const_logic_0;
    }
}

void duplicate_stream::thread_txApp2txEng_data_str_5_blk_n() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_110.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter1.read()) && 
         esl_seteq<1,1,1>(ap_block_pp0_stage0.read(), ap_const_boolean_0))) {
        txApp2txEng_data_str_5_blk_n = txApp2txEng_data_str_5_full_n.read();
    } else {
        txApp2txEng_data_str_5_blk_n = ap_const_logic_1;
    }
}

void duplicate_stream::thread_txApp2txEng_data_str_5_din() {
    txApp2txEng_data_str_5_din = tmp_keep_V_reg_120.read();
}

void duplicate_stream::thread_txApp2txEng_data_str_5_write() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_110.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter1.read()) && 
         esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0))) {
        txApp2txEng_data_str_5_write = ap_const_logic_1;
    } else {
        txApp2txEng_data_str_5_write = ap_const_logic_0;
    }
}

void duplicate_stream::thread_txApp2txEng_data_str_6_blk_n() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_110.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter1.read()) && 
         esl_seteq<1,1,1>(ap_block_pp0_stage0.read(), ap_const_boolean_0))) {
        txApp2txEng_data_str_6_blk_n = txApp2txEng_data_str_6_full_n.read();
    } else {
        txApp2txEng_data_str_6_blk_n = ap_const_logic_1;
    }
}

void duplicate_stream::thread_txApp2txEng_data_str_6_din() {
    txApp2txEng_data_str_6_din = tmp_last_V_reg_126.read();
}

void duplicate_stream::thread_txApp2txEng_data_str_6_write() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_110.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter1.read()) && 
         esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0))) {
        txApp2txEng_data_str_6_write = ap_const_logic_1;
    } else {
        txApp2txEng_data_str_6_write = ap_const_logic_0;
    }
}

void duplicate_stream::thread_ap_NS_fsm() {
    switch (ap_CS_fsm.read().to_uint64()) {
        case 1 : 
            ap_NS_fsm = ap_ST_fsm_pp0_stage0;
break;
        default : 
            ap_NS_fsm = "X";
            break;
    }
}

}

