// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
// Version: 2019.2.1
// Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

#ifndef _rshiftWordByOctet_HH_
#define _rshiftWordByOctet_HH_

#include "systemc.h"
#include "AESL_pkg.h"


namespace ap_rtl {

struct rshiftWordByOctet : public sc_module {
    // Port declarations 13
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_in< sc_logic > ap_start;
    sc_out< sc_logic > ap_done;
    sc_in< sc_logic > ap_continue;
    sc_out< sc_logic > ap_idle;
    sc_out< sc_logic > ap_ready;
    sc_in< sc_lv<73> > txEng_tcpPkgBuffer3_s_6_dout;
    sc_in< sc_logic > txEng_tcpPkgBuffer3_s_6_empty_n;
    sc_out< sc_logic > txEng_tcpPkgBuffer3_s_6_read;
    sc_out< sc_lv<73> > txEng_tcpPkgBuffer4_s_5_din;
    sc_in< sc_logic > txEng_tcpPkgBuffer4_s_5_full_n;
    sc_out< sc_logic > txEng_tcpPkgBuffer4_s_5_write;


    // Module declarations
    rshiftWordByOctet(sc_module_name name);
    SC_HAS_PROCESS(rshiftWordByOctet);

    ~rshiftWordByOctet();

    sc_trace_file* mVcdFile;

    sc_signal< sc_logic > ap_done_reg;
    sc_signal< sc_lv<1> > ap_CS_fsm;
    sc_signal< sc_logic > ap_CS_fsm_pp0_stage0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter1;
    sc_signal< sc_logic > ap_idle_pp0;
    sc_signal< sc_lv<1> > fsmState_load_load_fu_139_p1;
    sc_signal< sc_lv<1> > tmp_nbreadreq_fu_76_p3;
    sc_signal< bool > ap_predicate_op7_read_state1;
    sc_signal< bool > ap_block_state1_pp0_stage0_iter0;
    sc_signal< sc_lv<1> > fsmState_load_reg_263;
    sc_signal< sc_lv<1> > tmp_reg_267;
    sc_signal< sc_lv<1> > rs_firstWord_load_reg_285;
    sc_signal< bool > ap_predicate_op32_write_state2;
    sc_signal< bool > ap_block_state2_pp0_stage0_iter1;
    sc_signal< bool > ap_block_pp0_stage0_11001;
    sc_signal< sc_lv<1> > fsmState;
    sc_signal< sc_lv<64> > prevWord_data_V_8;
    sc_signal< sc_lv<8> > prevWord_keep_V_7;
    sc_signal< sc_lv<1> > rs_firstWord;
    sc_signal< sc_logic > txEng_tcpPkgBuffer3_s_6_blk_n;
    sc_signal< bool > ap_block_pp0_stage0;
    sc_signal< sc_logic > txEng_tcpPkgBuffer4_s_5_blk_n;
    sc_signal< sc_lv<64> > p_Val2_20_fu_143_p1;
    sc_signal< sc_lv<64> > p_Val2_20_reg_271;
    sc_signal< sc_lv<8> > p_Val2_2_reg_276;
    sc_signal< sc_lv<1> > tmp_143_fu_157_p3;
    sc_signal< sc_lv<1> > rs_firstWord_load_load_fu_165_p1;
    sc_signal< sc_lv<32> > trunc_ln647_fu_169_p1;
    sc_signal< sc_lv<32> > trunc_ln647_reg_289;
    sc_signal< sc_lv<4> > p_Result_21_i_reg_294;
    sc_signal< sc_lv<1> > tmp_last_V_fu_193_p2;
    sc_signal< sc_lv<1> > tmp_last_V_reg_299;
    sc_signal< bool > ap_block_pp0_stage0_subdone;
    sc_signal< sc_lv<1> > ap_phi_mux_p_0256_2_0_i_phi_fu_100_p4;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter0_p_0256_2_0_i_reg_97;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter0_rs_firstWord_new_0_i_reg_108;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter1_rs_firstWord_new_0_i_reg_108;
    sc_signal< sc_lv<1> > ap_sig_allocacmp_rs_firstWord_load;
    sc_signal< sc_lv<73> > tmp_1_fu_222_p6;
    sc_signal< bool > ap_block_pp0_stage0_01001;
    sc_signal< sc_lv<73> > tmp_2_fu_250_p5;
    sc_signal< sc_lv<4> > p_Result_22_i_fu_183_p4;
    sc_signal< sc_lv<4> > grp_fu_130_p4;
    sc_signal< sc_lv<32> > grp_fu_121_p4;
    sc_signal< sc_lv<1> > ap_NS_fsm;
    sc_signal< sc_logic > ap_idle_pp0_0to0;
    sc_signal< sc_logic > ap_reset_idle_pp0;
    sc_signal< sc_logic > ap_enable_pp0;
    sc_signal< bool > ap_condition_139;
    sc_signal< bool > ap_condition_143;
    sc_signal< bool > ap_condition_81;
    sc_signal< bool > ap_condition_146;
    sc_signal< bool > ap_condition_166;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<1> ap_ST_fsm_pp0_stage0;
    static const sc_lv<32> ap_const_lv32_0;
    static const bool ap_const_boolean_1;
    static const sc_lv<1> ap_const_lv1_0;
    static const sc_lv<1> ap_const_lv1_1;
    static const bool ap_const_boolean_0;
    static const sc_lv<32> ap_const_lv32_20;
    static const sc_lv<32> ap_const_lv32_3F;
    static const sc_lv<32> ap_const_lv32_4;
    static const sc_lv<32> ap_const_lv32_7;
    static const sc_lv<32> ap_const_lv32_40;
    static const sc_lv<32> ap_const_lv32_47;
    static const sc_lv<32> ap_const_lv32_48;
    static const sc_lv<32> ap_const_lv32_43;
    static const sc_lv<32> ap_const_lv32_44;
    static const sc_lv<4> ap_const_lv4_0;
    static const sc_lv<5> ap_const_lv5_10;
    // Thread declarations
    void thread_ap_clk_no_reset_();
    void thread_ap_CS_fsm_pp0_stage0();
    void thread_ap_block_pp0_stage0();
    void thread_ap_block_pp0_stage0_01001();
    void thread_ap_block_pp0_stage0_11001();
    void thread_ap_block_pp0_stage0_subdone();
    void thread_ap_block_state1_pp0_stage0_iter0();
    void thread_ap_block_state2_pp0_stage0_iter1();
    void thread_ap_condition_139();
    void thread_ap_condition_143();
    void thread_ap_condition_146();
    void thread_ap_condition_166();
    void thread_ap_condition_81();
    void thread_ap_done();
    void thread_ap_enable_pp0();
    void thread_ap_enable_reg_pp0_iter0();
    void thread_ap_idle();
    void thread_ap_idle_pp0();
    void thread_ap_idle_pp0_0to0();
    void thread_ap_phi_mux_p_0256_2_0_i_phi_fu_100_p4();
    void thread_ap_phi_reg_pp0_iter0_p_0256_2_0_i_reg_97();
    void thread_ap_phi_reg_pp0_iter0_rs_firstWord_new_0_i_reg_108();
    void thread_ap_predicate_op32_write_state2();
    void thread_ap_predicate_op7_read_state1();
    void thread_ap_ready();
    void thread_ap_reset_idle_pp0();
    void thread_ap_sig_allocacmp_rs_firstWord_load();
    void thread_fsmState_load_load_fu_139_p1();
    void thread_grp_fu_121_p4();
    void thread_grp_fu_130_p4();
    void thread_p_Result_22_i_fu_183_p4();
    void thread_p_Val2_20_fu_143_p1();
    void thread_rs_firstWord_load_load_fu_165_p1();
    void thread_tmp_143_fu_157_p3();
    void thread_tmp_1_fu_222_p6();
    void thread_tmp_2_fu_250_p5();
    void thread_tmp_last_V_fu_193_p2();
    void thread_tmp_nbreadreq_fu_76_p3();
    void thread_trunc_ln647_fu_169_p1();
    void thread_txEng_tcpPkgBuffer3_s_6_blk_n();
    void thread_txEng_tcpPkgBuffer3_s_6_read();
    void thread_txEng_tcpPkgBuffer4_s_5_blk_n();
    void thread_txEng_tcpPkgBuffer4_s_5_din();
    void thread_txEng_tcpPkgBuffer4_s_5_write();
    void thread_ap_NS_fsm();
};

}

using namespace ap_rtl;

#endif
