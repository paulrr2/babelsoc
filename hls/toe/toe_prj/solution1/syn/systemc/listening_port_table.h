// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
// Version: 2019.2.1
// Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

#ifndef _listening_port_table_HH_
#define _listening_port_table_HH_

#include "systemc.h"
#include "AESL_pkg.h"

#include "listening_port_taxdS.h"

namespace ap_rtl {

struct listening_port_table : public sc_module {
    // Port declarations 19
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_in< sc_logic > ap_start;
    sc_out< sc_logic > ap_done;
    sc_in< sc_logic > ap_continue;
    sc_out< sc_logic > ap_idle;
    sc_out< sc_logic > ap_ready;
    sc_in< sc_lv<16> > rxApp2portTable_list_1_dout;
    sc_in< sc_logic > rxApp2portTable_list_1_empty_n;
    sc_out< sc_logic > rxApp2portTable_list_1_read;
    sc_in< sc_lv<15> > pt_portCheckListenin_1_dout;
    sc_in< sc_logic > pt_portCheckListenin_1_empty_n;
    sc_out< sc_logic > pt_portCheckListenin_1_read;
    sc_out< sc_lv<1> > portTable2rxApp_list_1_din;
    sc_in< sc_logic > portTable2rxApp_list_1_full_n;
    sc_out< sc_logic > portTable2rxApp_list_1_write;
    sc_out< sc_lv<1> > pt_portCheckListenin_2_din;
    sc_in< sc_logic > pt_portCheckListenin_2_full_n;
    sc_out< sc_logic > pt_portCheckListenin_2_write;
    sc_signal< sc_lv<1> > ap_var_for_const0;


    // Module declarations
    listening_port_table(sc_module_name name);
    SC_HAS_PROCESS(listening_port_table);

    ~listening_port_table();

    sc_trace_file* mVcdFile;

    listening_port_taxdS* listeningPortTable_U;
    sc_signal< sc_logic > ap_done_reg;
    sc_signal< sc_lv<1> > ap_CS_fsm;
    sc_signal< sc_logic > ap_CS_fsm_pp0_stage0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter1;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter2;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter3;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter4;
    sc_signal< sc_logic > ap_idle_pp0;
    sc_signal< sc_lv<1> > tmp_nbreadreq_fu_54_p3;
    sc_signal< bool > ap_block_state1_pp0_stage0_iter0;
    sc_signal< sc_lv<1> > tmp_reg_179;
    sc_signal< sc_lv<1> > tmp_243_nbreadreq_fu_68_p3;
    sc_signal< bool > ap_predicate_op13_read_state2;
    sc_signal< bool > ap_block_state2_pp0_stage0_iter1;
    sc_signal< bool > ap_block_state3_pp0_stage0_iter2;
    sc_signal< sc_lv<1> > tmp_reg_179_pp0_iter2_reg;
    sc_signal< sc_lv<1> > and_ln63_reg_217;
    sc_signal< sc_lv<1> > and_ln67_reg_221;
    sc_signal< bool > ap_predicate_op31_write_state4;
    sc_signal< bool > ap_predicate_op32_write_state4;
    sc_signal< bool > ap_predicate_op33_write_state4;
    sc_signal< bool > ap_block_state4_pp0_stage0_iter3;
    sc_signal< sc_lv<1> > tmp_reg_179_pp0_iter3_reg;
    sc_signal< sc_lv<1> > tmp_243_reg_198;
    sc_signal< sc_lv<1> > tmp_243_reg_198_pp0_iter3_reg;
    sc_signal< bool > ap_predicate_op40_write_state5;
    sc_signal< bool > ap_block_state5_pp0_stage0_iter4;
    sc_signal< bool > ap_block_pp0_stage0_11001;
    sc_signal< sc_lv<15> > listeningPortTable_address0;
    sc_signal< sc_logic > listeningPortTable_ce0;
    sc_signal< sc_lv<1> > listeningPortTable_q0;
    sc_signal< sc_lv<15> > listeningPortTable_address1;
    sc_signal< sc_logic > listeningPortTable_ce1;
    sc_signal< sc_logic > listeningPortTable_we1;
    sc_signal< sc_lv<1> > listeningPortTable_q1;
    sc_signal< sc_logic > rxApp2portTable_list_1_blk_n;
    sc_signal< bool > ap_block_pp0_stage0;
    sc_signal< sc_logic > portTable2rxApp_list_1_blk_n;
    sc_signal< sc_logic > pt_portCheckListenin_1_blk_n;
    sc_signal< sc_logic > pt_portCheckListenin_2_blk_n;
    sc_signal< sc_lv<1> > tmp_reg_179_pp0_iter1_reg;
    sc_signal< sc_lv<16> > tmp_V_27_reg_183;
    sc_signal< sc_lv<16> > tmp_V_27_reg_183_pp0_iter1_reg;
    sc_signal< sc_lv<15> > trunc_ln681_fu_132_p1;
    sc_signal< sc_lv<15> > trunc_ln681_reg_188;
    sc_signal< sc_lv<1> > tmp_242_reg_193;
    sc_signal< sc_lv<1> > tmp_242_reg_193_pp0_iter1_reg;
    sc_signal< sc_lv<1> > tmp_243_reg_198_pp0_iter2_reg;
    sc_signal< sc_lv<15> > tmp_V_reg_202;
    sc_signal< sc_lv<1> > and_ln63_fu_157_p2;
    sc_signal< sc_lv<1> > and_ln67_fu_169_p2;
    sc_signal< sc_lv<1> > tmp_244_reg_225;
    sc_signal< bool > ap_block_pp0_stage0_subdone;
    sc_signal< sc_lv<64> > zext_ln681_fu_144_p1;
    sc_signal< sc_lv<64> > zext_ln544_fu_148_p1;
    sc_signal< sc_lv<64> > zext_ln544_20_fu_175_p1;
    sc_signal< bool > ap_block_pp0_stage0_01001;
    sc_signal< sc_lv<1> > xor_ln887_fu_152_p2;
    sc_signal< sc_lv<1> > xor_ln67_fu_163_p2;
    sc_signal< sc_lv<1> > ap_NS_fsm;
    sc_signal< sc_logic > ap_idle_pp0_0to3;
    sc_signal< sc_logic > ap_reset_idle_pp0;
    sc_signal< bool > ap_block_pp0;
    sc_signal< bool > ap_enable_operation_16;
    sc_signal< bool > ap_enable_state2_pp0_iter1_stage0;
    sc_signal< bool > ap_enable_operation_20;
    sc_signal< bool > ap_enable_state3_pp0_iter2_stage0;
    sc_signal< bool > ap_predicate_op29_store_state3;
    sc_signal< bool > ap_enable_operation_29;
    sc_signal< bool > ap_predicate_op19_load_state3;
    sc_signal< bool > ap_enable_operation_19;
    sc_signal< bool > ap_predicate_op30_load_state4;
    sc_signal< bool > ap_enable_operation_30;
    sc_signal< bool > ap_enable_state4_pp0_iter3_stage0;
    sc_signal< sc_logic > ap_enable_pp0;
    sc_signal< bool > ap_condition_226;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<1> ap_ST_fsm_pp0_stage0;
    static const sc_lv<32> ap_const_lv32_0;
    static const bool ap_const_boolean_1;
    static const sc_lv<1> ap_const_lv1_1;
    static const sc_lv<1> ap_const_lv1_0;
    static const bool ap_const_boolean_0;
    static const sc_lv<32> ap_const_lv32_F;
    // Thread declarations
    void thread_ap_var_for_const0();
    void thread_ap_clk_no_reset_();
    void thread_and_ln63_fu_157_p2();
    void thread_and_ln67_fu_169_p2();
    void thread_ap_CS_fsm_pp0_stage0();
    void thread_ap_block_pp0();
    void thread_ap_block_pp0_stage0();
    void thread_ap_block_pp0_stage0_01001();
    void thread_ap_block_pp0_stage0_11001();
    void thread_ap_block_pp0_stage0_subdone();
    void thread_ap_block_state1_pp0_stage0_iter0();
    void thread_ap_block_state2_pp0_stage0_iter1();
    void thread_ap_block_state3_pp0_stage0_iter2();
    void thread_ap_block_state4_pp0_stage0_iter3();
    void thread_ap_block_state5_pp0_stage0_iter4();
    void thread_ap_condition_226();
    void thread_ap_done();
    void thread_ap_enable_operation_16();
    void thread_ap_enable_operation_19();
    void thread_ap_enable_operation_20();
    void thread_ap_enable_operation_29();
    void thread_ap_enable_operation_30();
    void thread_ap_enable_pp0();
    void thread_ap_enable_reg_pp0_iter0();
    void thread_ap_enable_state2_pp0_iter1_stage0();
    void thread_ap_enable_state3_pp0_iter2_stage0();
    void thread_ap_enable_state4_pp0_iter3_stage0();
    void thread_ap_idle();
    void thread_ap_idle_pp0();
    void thread_ap_idle_pp0_0to3();
    void thread_ap_predicate_op13_read_state2();
    void thread_ap_predicate_op19_load_state3();
    void thread_ap_predicate_op29_store_state3();
    void thread_ap_predicate_op30_load_state4();
    void thread_ap_predicate_op31_write_state4();
    void thread_ap_predicate_op32_write_state4();
    void thread_ap_predicate_op33_write_state4();
    void thread_ap_predicate_op40_write_state5();
    void thread_ap_ready();
    void thread_ap_reset_idle_pp0();
    void thread_listeningPortTable_address0();
    void thread_listeningPortTable_address1();
    void thread_listeningPortTable_ce0();
    void thread_listeningPortTable_ce1();
    void thread_listeningPortTable_we1();
    void thread_portTable2rxApp_list_1_blk_n();
    void thread_portTable2rxApp_list_1_din();
    void thread_portTable2rxApp_list_1_write();
    void thread_pt_portCheckListenin_1_blk_n();
    void thread_pt_portCheckListenin_1_read();
    void thread_pt_portCheckListenin_2_blk_n();
    void thread_pt_portCheckListenin_2_din();
    void thread_pt_portCheckListenin_2_write();
    void thread_rxApp2portTable_list_1_blk_n();
    void thread_rxApp2portTable_list_1_read();
    void thread_tmp_243_nbreadreq_fu_68_p3();
    void thread_tmp_nbreadreq_fu_54_p3();
    void thread_trunc_ln681_fu_132_p1();
    void thread_xor_ln67_fu_163_p2();
    void thread_xor_ln887_fu_152_p2();
    void thread_zext_ln544_20_fu_175_p1();
    void thread_zext_ln544_fu_148_p1();
    void thread_zext_ln681_fu_144_p1();
    void thread_ap_NS_fsm();
};

}

using namespace ap_rtl;

#endif
