// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
// Version: 2019.2.1
// Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

#ifndef _rx_app_stream_if_HH_
#define _rx_app_stream_if_HH_

#include "systemc.h"
#include "AESL_pkg.h"


namespace ap_rtl {

struct rx_app_stream_if : public sc_module {
    // Port declarations 22
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_in< sc_logic > ap_start;
    sc_out< sc_logic > ap_done;
    sc_in< sc_logic > ap_continue;
    sc_out< sc_logic > ap_idle;
    sc_out< sc_logic > ap_ready;
    sc_in< sc_logic > appRxDataReq_V_TVALID;
    sc_in< sc_lv<35> > rxSar2rxApp_upd_rsp_s_16_dout;
    sc_in< sc_logic > rxSar2rxApp_upd_rsp_s_16_empty_n;
    sc_out< sc_logic > rxSar2rxApp_upd_rsp_s_16_read;
    sc_out< sc_lv<35> > rxApp2rxSar_upd_req_s_19_din;
    sc_in< sc_logic > rxApp2rxSar_upd_req_s_19_full_n;
    sc_out< sc_logic > rxApp2rxSar_upd_req_s_19_write;
    sc_out< sc_lv<1> > rxBufferReadCmd_V_V_din;
    sc_in< sc_logic > rxBufferReadCmd_V_V_full_n;
    sc_out< sc_logic > rxBufferReadCmd_V_V_write;
    sc_in< sc_logic > appRxDataRspMetadata_V_V_TREADY;
    sc_in< sc_lv<32> > appRxDataReq_V_TDATA;
    sc_out< sc_logic > appRxDataReq_V_TREADY;
    sc_out< sc_lv<16> > appRxDataRspMetadata_V_V_TDATA;
    sc_out< sc_logic > appRxDataRspMetadata_V_V_TVALID;


    // Module declarations
    rx_app_stream_if(sc_module_name name);
    SC_HAS_PROCESS(rx_app_stream_if);

    ~rx_app_stream_if();

    sc_trace_file* mVcdFile;

    regslice_both<16>* regslice_both_appRxDataRspMetadata_V_V_U;
    sc_signal< sc_logic > ap_done_reg;
    sc_signal< sc_lv<1> > ap_CS_fsm;
    sc_signal< sc_logic > ap_CS_fsm_pp0_stage0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter1;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter2;
    sc_signal< sc_logic > ap_idle_pp0;
    sc_signal< sc_lv<1> > tmp_92_nbreadreq_fu_74_p3;
    sc_signal< bool > ap_predicate_op8_read_state1;
    sc_signal< sc_lv<1> > tmp_nbreadreq_fu_88_p3;
    sc_signal< bool > ap_predicate_op17_read_state1;
    sc_signal< bool > ap_block_state1_pp0_stage0_iter0;
    sc_signal< sc_lv<1> > rasi_fsmState_V_load_reg_211;
    sc_signal< sc_lv<1> > tmp_92_reg_215;
    sc_signal< sc_lv<1> > icmp_ln883_reg_219;
    sc_signal< bool > ap_predicate_op22_write_state2;
    sc_signal< sc_lv<1> > tmp_reg_228;
    sc_signal< bool > ap_predicate_op24_write_state2;
    sc_signal< bool > ap_predicate_op29_write_state2;
    sc_signal< bool > ap_block_state2_pp0_stage0_iter1;
    sc_signal< bool > ap_predicate_op23_write_state2;
    sc_signal< bool > ap_block_state2_io;
    sc_signal< sc_logic > regslice_both_appRxDataRspMetadata_V_V_U_apdone_blk;
    sc_signal< bool > ap_block_state3_pp0_stage0_iter2;
    sc_signal< sc_lv<1> > rasi_fsmState_V_load_reg_211_pp0_iter1_reg;
    sc_signal< sc_lv<1> > tmp_reg_228_pp0_iter1_reg;
    sc_signal< bool > ap_predicate_op39_write_state3;
    sc_signal< bool > ap_block_state3_io;
    sc_signal< bool > ap_block_pp0_stage0_11001;
    sc_signal< sc_lv<1> > rasi_fsmState_V;
    sc_signal< sc_lv<16> > rasi_readLength_V;
    sc_signal< sc_logic > appRxDataReq_V_TDATA_blk_n;
    sc_signal< bool > ap_block_pp0_stage0;
    sc_signal< sc_logic > appRxDataRspMetadata_V_V_TDATA_blk_n;
    sc_signal< sc_logic > rxApp2rxSar_upd_req_s_19_blk_n;
    sc_signal< sc_logic > rxSar2rxApp_upd_rsp_s_16_blk_n;
    sc_signal< sc_logic > rxBufferReadCmd_V_V_blk_n;
    sc_signal< sc_lv<1> > icmp_ln883_fu_138_p2;
    sc_signal< sc_lv<16> > tmp_sessionID_V_6_fu_144_p1;
    sc_signal< sc_lv<16> > tmp_sessionID_V_6_reg_223;
    sc_signal< sc_lv<16> > tmp_V_fu_160_p1;
    sc_signal< sc_lv<16> > tmp_V_reg_232;
    sc_signal< sc_lv<18> > tmp_appd_V_load_new_s_reg_238;
    sc_signal< bool > ap_block_pp0_stage0_subdone;
    sc_signal< sc_lv<16> > tmp_length_V_load_ne_1_fu_128_p4;
    sc_signal< sc_lv<35> > tmp_1_fu_180_p3;
    sc_signal< bool > ap_block_pp0_stage0_01001;
    sc_signal< sc_lv<35> > tmp_3_fu_201_p4;
    sc_signal< sc_lv<18> > zext_ln209_fu_192_p1;
    sc_signal< sc_lv<18> > tmp_appd_V_fu_196_p2;
    sc_signal< sc_lv<1> > ap_NS_fsm;
    sc_signal< sc_logic > ap_idle_pp0_0to1;
    sc_signal< sc_logic > ap_reset_idle_pp0;
    sc_signal< sc_logic > ap_enable_pp0;
    sc_signal< sc_logic > appRxDataRspMetadata_V_V_TVALID_int;
    sc_signal< sc_logic > appRxDataRspMetadata_V_V_TREADY_int;
    sc_signal< sc_logic > regslice_both_appRxDataRspMetadata_V_V_U_vld_out;
    sc_signal< bool > ap_condition_177;
    sc_signal< bool > ap_condition_133;
    sc_signal< bool > ap_condition_214;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<1> ap_ST_fsm_pp0_stage0;
    static const sc_lv<32> ap_const_lv32_0;
    static const bool ap_const_boolean_1;
    static const sc_lv<1> ap_const_lv1_0;
    static const sc_lv<1> ap_const_lv1_1;
    static const bool ap_const_boolean_0;
    static const sc_lv<16> ap_const_lv16_0;
    static const sc_lv<32> ap_const_lv32_10;
    static const sc_lv<32> ap_const_lv32_1F;
    static const sc_lv<32> ap_const_lv32_21;
    static const sc_lv<19> ap_const_lv19_0;
    // Thread declarations
    void thread_ap_clk_no_reset_();
    void thread_ap_CS_fsm_pp0_stage0();
    void thread_ap_block_pp0_stage0();
    void thread_ap_block_pp0_stage0_01001();
    void thread_ap_block_pp0_stage0_11001();
    void thread_ap_block_pp0_stage0_subdone();
    void thread_ap_block_state1_pp0_stage0_iter0();
    void thread_ap_block_state2_io();
    void thread_ap_block_state2_pp0_stage0_iter1();
    void thread_ap_block_state3_io();
    void thread_ap_block_state3_pp0_stage0_iter2();
    void thread_ap_condition_133();
    void thread_ap_condition_177();
    void thread_ap_condition_214();
    void thread_ap_done();
    void thread_ap_enable_pp0();
    void thread_ap_enable_reg_pp0_iter0();
    void thread_ap_idle();
    void thread_ap_idle_pp0();
    void thread_ap_idle_pp0_0to1();
    void thread_ap_predicate_op17_read_state1();
    void thread_ap_predicate_op22_write_state2();
    void thread_ap_predicate_op23_write_state2();
    void thread_ap_predicate_op24_write_state2();
    void thread_ap_predicate_op29_write_state2();
    void thread_ap_predicate_op39_write_state3();
    void thread_ap_predicate_op8_read_state1();
    void thread_ap_ready();
    void thread_ap_reset_idle_pp0();
    void thread_appRxDataReq_V_TDATA_blk_n();
    void thread_appRxDataReq_V_TREADY();
    void thread_appRxDataRspMetadata_V_V_TDATA_blk_n();
    void thread_appRxDataRspMetadata_V_V_TVALID();
    void thread_appRxDataRspMetadata_V_V_TVALID_int();
    void thread_icmp_ln883_fu_138_p2();
    void thread_rxApp2rxSar_upd_req_s_19_blk_n();
    void thread_rxApp2rxSar_upd_req_s_19_din();
    void thread_rxApp2rxSar_upd_req_s_19_write();
    void thread_rxBufferReadCmd_V_V_blk_n();
    void thread_rxBufferReadCmd_V_V_din();
    void thread_rxBufferReadCmd_V_V_write();
    void thread_rxSar2rxApp_upd_rsp_s_16_blk_n();
    void thread_rxSar2rxApp_upd_rsp_s_16_read();
    void thread_tmp_1_fu_180_p3();
    void thread_tmp_3_fu_201_p4();
    void thread_tmp_92_nbreadreq_fu_74_p3();
    void thread_tmp_V_fu_160_p1();
    void thread_tmp_appd_V_fu_196_p2();
    void thread_tmp_length_V_load_ne_1_fu_128_p4();
    void thread_tmp_nbreadreq_fu_88_p3();
    void thread_tmp_sessionID_V_6_fu_144_p1();
    void thread_zext_ln209_fu_192_p1();
    void thread_ap_NS_fsm();
};

}

using namespace ap_rtl;

#endif
