// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
// Version: 2019.2.1
// Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

module two_complement_subch (
        ap_clk,
        ap_rst,
        ap_start,
        ap_done,
        ap_continue,
        ap_idle,
        ap_ready,
        txEng_tcpPkgBuffer1_s_8_dout,
        txEng_tcpPkgBuffer1_s_8_empty_n,
        txEng_tcpPkgBuffer1_s_8_read,
        txEng_tcpPkgBuffer2_s_7_din,
        txEng_tcpPkgBuffer2_s_7_full_n,
        txEng_tcpPkgBuffer2_s_7_write,
        txEng_subChecksumsFi_1_din,
        txEng_subChecksumsFi_1_full_n,
        txEng_subChecksumsFi_1_write
);

parameter    ap_ST_fsm_pp0_stage0 = 1'd1;

input   ap_clk;
input   ap_rst;
input   ap_start;
output   ap_done;
input   ap_continue;
output   ap_idle;
output   ap_ready;
input  [72:0] txEng_tcpPkgBuffer1_s_8_dout;
input   txEng_tcpPkgBuffer1_s_8_empty_n;
output   txEng_tcpPkgBuffer1_s_8_read;
output  [72:0] txEng_tcpPkgBuffer2_s_7_din;
input   txEng_tcpPkgBuffer2_s_7_full_n;
output   txEng_tcpPkgBuffer2_s_7_write;
output  [67:0] txEng_subChecksumsFi_1_din;
input   txEng_subChecksumsFi_1_full_n;
output   txEng_subChecksumsFi_1_write;

reg ap_done;
reg ap_idle;
reg ap_ready;
reg txEng_tcpPkgBuffer1_s_8_read;
reg txEng_tcpPkgBuffer2_s_7_write;
reg txEng_subChecksumsFi_1_write;

reg    ap_done_reg;
(* fsm_encoding = "none" *) reg   [0:0] ap_CS_fsm;
wire    ap_CS_fsm_pp0_stage0;
wire    ap_enable_reg_pp0_iter0;
reg    ap_enable_reg_pp0_iter1;
reg    ap_enable_reg_pp0_iter2;
reg    ap_idle_pp0;
wire   [0:0] tmp_nbreadreq_fu_104_p3;
reg    ap_block_state1_pp0_stage0_iter0;
reg   [0:0] tmp_reg_906;
reg    ap_block_state2_pp0_stage0_iter1;
reg   [0:0] tmp_reg_906_pp0_iter1_reg;
reg   [0:0] tmp_52_reg_1010;
reg   [0:0] tmp_52_reg_1010_pp0_iter1_reg;
reg    ap_predicate_op122_write_state3;
reg    ap_block_state3_pp0_stage0_iter2;
reg    ap_block_pp0_stage0_11001;
reg   [16:0] tcts_tcp_sums_sum_V_3_12;
reg   [16:0] tcts_tcp_sums_sum_V_2_13;
reg   [16:0] tcts_tcp_sums_sum_V_1_14;
reg   [16:0] tcts_tcp_sums_sum_V_s;
reg    txEng_tcpPkgBuffer1_s_8_blk_n;
wire    ap_block_pp0_stage0;
reg    txEng_tcpPkgBuffer2_s_7_blk_n;
reg    txEng_subChecksumsFi_1_blk_n;
reg   [72:0] tmp33_reg_910;
wire   [0:0] icmp_ln879_fu_238_p2;
reg   [0:0] icmp_ln879_reg_915;
reg   [0:0] icmp_ln879_reg_915_pp0_iter1_reg;
reg   [7:0] p_Result_11_i_reg_921;
wire   [7:0] trunc_ln647_fu_254_p1;
reg   [7:0] trunc_ln647_reg_926;
wire   [0:0] and_ln647_fu_264_p2;
reg   [0:0] and_ln647_reg_932;
reg   [0:0] and_ln647_reg_932_pp0_iter1_reg;
wire   [0:0] icmp_ln879_1_fu_280_p2;
reg   [0:0] icmp_ln879_1_reg_938;
reg   [0:0] icmp_ln879_1_reg_938_pp0_iter1_reg;
reg   [7:0] p_Result_11_1_i_reg_945;
reg   [7:0] p_Result_13_1_i_reg_950;
reg   [0:0] tmp_44_reg_956;
reg   [0:0] tmp_44_reg_956_pp0_iter1_reg;
wire   [0:0] icmp_ln879_2_fu_324_p2;
reg   [0:0] icmp_ln879_2_reg_962;
reg   [0:0] icmp_ln879_2_reg_962_pp0_iter1_reg;
reg   [7:0] p_Result_11_2_i_reg_969;
reg   [7:0] p_Result_13_2_i_reg_974;
reg   [0:0] tmp_47_reg_980;
reg   [0:0] tmp_47_reg_980_pp0_iter1_reg;
wire   [0:0] icmp_ln879_3_fu_368_p2;
reg   [0:0] icmp_ln879_3_reg_986;
reg   [0:0] icmp_ln879_3_reg_986_pp0_iter1_reg;
reg   [7:0] p_Result_11_3_i_reg_993;
reg   [7:0] p_Result_13_3_i_reg_998;
reg   [0:0] tmp_50_reg_1004;
reg   [0:0] tmp_50_reg_1004_pp0_iter1_reg;
reg   [16:0] tcts_tcp_sums_sum_V_9_reg_1014;
wire   [15:0] add_ln214_1_fu_452_p2;
reg   [15:0] add_ln214_1_reg_1019;
wire   [15:0] add_ln214_3_fu_493_p2;
reg   [15:0] add_ln214_3_reg_1024;
reg   [16:0] tcts_tcp_sums_sum_V_10_reg_1029;
wire   [15:0] add_ln214_5_fu_541_p2;
reg   [15:0] add_ln214_5_reg_1034;
wire   [15:0] add_ln214_7_fu_582_p2;
reg   [15:0] add_ln214_7_reg_1039;
reg   [16:0] tcts_tcp_sums_sum_V_11_reg_1044;
wire   [15:0] add_ln214_9_fu_630_p2;
reg   [15:0] add_ln214_9_reg_1049;
wire   [15:0] add_ln214_11_fu_671_p2;
reg   [15:0] add_ln214_11_reg_1054;
reg   [16:0] tcts_tcp_sums_sum_V_12_reg_1059;
wire   [15:0] add_ln214_13_fu_719_p2;
reg   [15:0] add_ln214_13_reg_1064;
wire   [15:0] add_ln214_15_fu_760_p2;
reg   [15:0] add_ln214_15_reg_1069;
reg    ap_block_pp0_stage0_subdone;
reg   [0:0] ap_phi_mux_tcts_tcp_sums_sum_V_21_phi_fu_136_p4;
wire   [0:0] ap_phi_reg_pp0_iter0_tcts_tcp_sums_sum_V_21_reg_132;
reg   [0:0] ap_phi_reg_pp0_iter1_tcts_tcp_sums_sum_V_21_reg_132;
reg   [0:0] ap_phi_reg_pp0_iter2_tcts_tcp_sums_sum_V_21_reg_132;
wire   [0:0] or_ln647_fu_772_p2;
reg   [16:0] ap_phi_mux_tcts_tcp_sums_sum_V_22_phi_fu_147_p4;
wire   [16:0] ap_phi_reg_pp0_iter0_tcts_tcp_sums_sum_V_22_reg_143;
reg   [16:0] ap_phi_reg_pp0_iter1_tcts_tcp_sums_sum_V_22_reg_143;
reg   [16:0] ap_phi_reg_pp0_iter2_tcts_tcp_sums_sum_V_22_reg_143;
wire   [16:0] select_ln647_fu_783_p3;
reg   [0:0] ap_phi_mux_tcts_tcp_sums_sum_V_23_phi_fu_158_p4;
wire   [0:0] ap_phi_reg_pp0_iter0_tcts_tcp_sums_sum_V_23_reg_154;
reg   [0:0] ap_phi_reg_pp0_iter1_tcts_tcp_sums_sum_V_23_reg_154;
reg   [0:0] ap_phi_reg_pp0_iter2_tcts_tcp_sums_sum_V_23_reg_154;
wire   [0:0] or_ln791_fu_791_p2;
reg   [16:0] ap_phi_mux_tcts_tcp_sums_sum_V_24_phi_fu_169_p4;
wire   [16:0] ap_phi_reg_pp0_iter0_tcts_tcp_sums_sum_V_24_reg_165;
reg   [16:0] ap_phi_reg_pp0_iter1_tcts_tcp_sums_sum_V_24_reg_165;
reg   [16:0] ap_phi_reg_pp0_iter2_tcts_tcp_sums_sum_V_24_reg_165;
wire   [16:0] select_ln791_fu_809_p3;
reg   [0:0] ap_phi_mux_tcts_tcp_sums_sum_V_25_phi_fu_180_p4;
wire   [0:0] ap_phi_reg_pp0_iter0_tcts_tcp_sums_sum_V_25_reg_176;
reg   [0:0] ap_phi_reg_pp0_iter1_tcts_tcp_sums_sum_V_25_reg_176;
reg   [0:0] ap_phi_reg_pp0_iter2_tcts_tcp_sums_sum_V_25_reg_176;
wire   [0:0] or_ln791_1_fu_817_p2;
reg   [16:0] ap_phi_mux_tcts_tcp_sums_sum_V_26_phi_fu_191_p4;
wire   [16:0] ap_phi_reg_pp0_iter0_tcts_tcp_sums_sum_V_26_reg_187;
reg   [16:0] ap_phi_reg_pp0_iter1_tcts_tcp_sums_sum_V_26_reg_187;
reg   [16:0] ap_phi_reg_pp0_iter2_tcts_tcp_sums_sum_V_26_reg_187;
wire   [16:0] select_ln791_1_fu_835_p3;
reg   [0:0] ap_phi_mux_tcts_tcp_sums_sum_V_27_phi_fu_202_p4;
wire   [0:0] ap_phi_reg_pp0_iter0_tcts_tcp_sums_sum_V_27_reg_198;
reg   [0:0] ap_phi_reg_pp0_iter1_tcts_tcp_sums_sum_V_27_reg_198;
reg   [0:0] ap_phi_reg_pp0_iter2_tcts_tcp_sums_sum_V_27_reg_198;
wire   [0:0] or_ln791_2_fu_843_p2;
reg   [16:0] ap_phi_mux_tcts_tcp_sums_sum_V_28_phi_fu_213_p4;
wire   [16:0] ap_phi_reg_pp0_iter0_tcts_tcp_sums_sum_V_28_reg_209;
reg   [16:0] ap_phi_reg_pp0_iter1_tcts_tcp_sums_sum_V_28_reg_209;
reg   [16:0] ap_phi_reg_pp0_iter2_tcts_tcp_sums_sum_V_28_reg_209;
wire   [16:0] select_ln791_2_fu_861_p3;
reg   [16:0] ap_sig_allocacmp_tcts_tcp_sums_sum_V_9;
reg   [16:0] ap_sig_allocacmp_tcts_tcp_sums_sum_V_10;
reg   [16:0] ap_sig_allocacmp_tcts_tcp_sums_sum_V_11;
reg   [16:0] ap_sig_allocacmp_tcts_tcp_sums_sum_V_12;
reg    ap_block_pp0_stage0_01001;
wire   [1:0] p_Result_10_i_fu_228_p4;
wire   [0:0] tmp_40_fu_220_p3;
wire   [0:0] xor_ln879_fu_258_p2;
wire   [1:0] p_Result_10_1_i_fu_270_p4;
wire   [1:0] p_Result_10_2_i_fu_314_p4;
wire   [1:0] p_Result_10_3_i_fu_358_p4;
wire   [15:0] p_Result_14_i_fu_414_p3;
wire   [16:0] zext_ln700_fu_420_p1;
wire   [16:0] add_ln700_fu_428_p2;
wire   [0:0] tmp_41_fu_434_p3;
wire   [15:0] zext_ln214_10_fu_442_p1;
wire   [15:0] trunc_ln700_fu_424_p1;
wire   [15:0] add_ln214_fu_446_p2;
wire   [15:0] p_Result_17_i_fu_458_p3;
wire   [16:0] zext_ln700_1_fu_465_p1;
wire   [16:0] add_ln700_1_fu_469_p2;
wire   [0:0] tmp_42_fu_475_p3;
wire   [15:0] zext_ln214_11_fu_483_p1;
wire   [15:0] add_ln214_2_fu_487_p2;
wire   [15:0] p_Result_14_1_i_fu_503_p3;
wire   [16:0] zext_ln700_2_fu_509_p1;
wire   [16:0] add_ln700_2_fu_517_p2;
wire   [0:0] tmp_43_fu_523_p3;
wire   [15:0] zext_ln214_12_fu_531_p1;
wire   [15:0] trunc_ln700_4_fu_513_p1;
wire   [15:0] add_ln214_4_fu_535_p2;
wire   [15:0] p_Result_17_1_i_fu_547_p3;
wire   [16:0] zext_ln700_3_fu_554_p1;
wire   [16:0] add_ln700_3_fu_558_p2;
wire   [0:0] tmp_45_fu_564_p3;
wire   [15:0] zext_ln214_13_fu_572_p1;
wire   [15:0] add_ln214_6_fu_576_p2;
wire   [15:0] p_Result_14_2_i_fu_592_p3;
wire   [16:0] zext_ln700_4_fu_598_p1;
wire   [16:0] add_ln700_4_fu_606_p2;
wire   [0:0] tmp_46_fu_612_p3;
wire   [15:0] zext_ln214_14_fu_620_p1;
wire   [15:0] trunc_ln700_5_fu_602_p1;
wire   [15:0] add_ln214_8_fu_624_p2;
wire   [15:0] p_Result_17_2_i_fu_636_p3;
wire   [16:0] zext_ln700_5_fu_643_p1;
wire   [16:0] add_ln700_5_fu_647_p2;
wire   [0:0] tmp_48_fu_653_p3;
wire   [15:0] zext_ln214_15_fu_661_p1;
wire   [15:0] add_ln214_10_fu_665_p2;
wire   [15:0] p_Result_14_3_i_fu_681_p3;
wire   [16:0] zext_ln700_6_fu_687_p1;
wire   [16:0] add_ln700_6_fu_695_p2;
wire   [0:0] tmp_49_fu_701_p3;
wire   [15:0] zext_ln214_16_fu_709_p1;
wire   [15:0] trunc_ln700_6_fu_691_p1;
wire   [15:0] add_ln214_12_fu_713_p2;
wire   [15:0] p_Result_17_3_i_fu_725_p3;
wire   [16:0] zext_ln700_7_fu_732_p1;
wire   [16:0] add_ln700_7_fu_736_p2;
wire   [0:0] tmp_51_fu_742_p3;
wire   [15:0] zext_ln214_17_fu_750_p1;
wire   [15:0] add_ln214_14_fu_754_p2;
wire   [16:0] zext_ln214_fu_766_p1;
wire   [16:0] zext_ln214_1_fu_769_p1;
wire   [16:0] select_ln879_fu_777_p3;
wire   [15:0] select_ln879_1_fu_796_p3;
wire   [0:0] or_ln791_6_fu_805_p2;
wire   [16:0] zext_ln879_fu_801_p1;
wire   [15:0] select_ln879_2_fu_822_p3;
wire   [0:0] or_ln791_7_fu_831_p2;
wire   [16:0] zext_ln879_1_fu_827_p1;
wire   [15:0] select_ln879_3_fu_848_p3;
wire   [0:0] or_ln791_8_fu_857_p2;
wire   [16:0] zext_ln879_2_fu_853_p1;
reg   [0:0] ap_NS_fsm;
reg    ap_idle_pp0_0to1;
reg    ap_reset_idle_pp0;
wire    ap_enable_pp0;
reg    ap_condition_237;

// power-on initialization
initial begin
#0 ap_done_reg = 1'b0;
#0 ap_CS_fsm = 1'd1;
#0 ap_enable_reg_pp0_iter1 = 1'b0;
#0 ap_enable_reg_pp0_iter2 = 1'b0;
#0 tcts_tcp_sums_sum_V_3_12 = 17'd0;
#0 tcts_tcp_sums_sum_V_2_13 = 17'd0;
#0 tcts_tcp_sums_sum_V_1_14 = 17'd0;
#0 tcts_tcp_sums_sum_V_s = 17'd0;
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_CS_fsm <= ap_ST_fsm_pp0_stage0;
    end else begin
        ap_CS_fsm <= ap_NS_fsm;
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_done_reg <= 1'b0;
    end else begin
        if ((ap_continue == 1'b1)) begin
            ap_done_reg <= 1'b0;
        end else if (((1'b0 == ap_block_pp0_stage0_11001) & (ap_enable_reg_pp0_iter2 == 1'b1))) begin
            ap_done_reg <= 1'b1;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_enable_reg_pp0_iter1 <= 1'b0;
    end else begin
        if (((1'b0 == ap_block_pp0_stage0_subdone) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
            ap_enable_reg_pp0_iter1 <= ap_start;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_enable_reg_pp0_iter2 <= 1'b0;
    end else begin
        if ((1'b0 == ap_block_pp0_stage0_subdone)) begin
            ap_enable_reg_pp0_iter2 <= ap_enable_reg_pp0_iter1;
        end
    end
end

always @ (posedge ap_clk) begin
    if ((1'b1 == ap_condition_237)) begin
        if (((tmp_52_reg_1010 == 1'd1) & (tmp_reg_906 == 1'd1))) begin
            ap_phi_reg_pp0_iter2_tcts_tcp_sums_sum_V_21_reg_132 <= 1'd1;
        end else if ((1'b1 == 1'b1)) begin
            ap_phi_reg_pp0_iter2_tcts_tcp_sums_sum_V_21_reg_132 <= ap_phi_reg_pp0_iter1_tcts_tcp_sums_sum_V_21_reg_132;
        end
    end
end

always @ (posedge ap_clk) begin
    if ((1'b1 == ap_condition_237)) begin
        if (((tmp_52_reg_1010 == 1'd1) & (tmp_reg_906 == 1'd1))) begin
            ap_phi_reg_pp0_iter2_tcts_tcp_sums_sum_V_22_reg_143 <= 17'd0;
        end else if ((1'b1 == 1'b1)) begin
            ap_phi_reg_pp0_iter2_tcts_tcp_sums_sum_V_22_reg_143 <= ap_phi_reg_pp0_iter1_tcts_tcp_sums_sum_V_22_reg_143;
        end
    end
end

always @ (posedge ap_clk) begin
    if ((1'b1 == ap_condition_237)) begin
        if (((tmp_52_reg_1010 == 1'd1) & (tmp_reg_906 == 1'd1))) begin
            ap_phi_reg_pp0_iter2_tcts_tcp_sums_sum_V_23_reg_154 <= 1'd1;
        end else if ((1'b1 == 1'b1)) begin
            ap_phi_reg_pp0_iter2_tcts_tcp_sums_sum_V_23_reg_154 <= ap_phi_reg_pp0_iter1_tcts_tcp_sums_sum_V_23_reg_154;
        end
    end
end

always @ (posedge ap_clk) begin
    if ((1'b1 == ap_condition_237)) begin
        if (((tmp_52_reg_1010 == 1'd1) & (tmp_reg_906 == 1'd1))) begin
            ap_phi_reg_pp0_iter2_tcts_tcp_sums_sum_V_24_reg_165 <= 17'd0;
        end else if ((1'b1 == 1'b1)) begin
            ap_phi_reg_pp0_iter2_tcts_tcp_sums_sum_V_24_reg_165 <= ap_phi_reg_pp0_iter1_tcts_tcp_sums_sum_V_24_reg_165;
        end
    end
end

always @ (posedge ap_clk) begin
    if ((1'b1 == ap_condition_237)) begin
        if (((tmp_52_reg_1010 == 1'd1) & (tmp_reg_906 == 1'd1))) begin
            ap_phi_reg_pp0_iter2_tcts_tcp_sums_sum_V_25_reg_176 <= 1'd1;
        end else if ((1'b1 == 1'b1)) begin
            ap_phi_reg_pp0_iter2_tcts_tcp_sums_sum_V_25_reg_176 <= ap_phi_reg_pp0_iter1_tcts_tcp_sums_sum_V_25_reg_176;
        end
    end
end

always @ (posedge ap_clk) begin
    if ((1'b1 == ap_condition_237)) begin
        if (((tmp_52_reg_1010 == 1'd1) & (tmp_reg_906 == 1'd1))) begin
            ap_phi_reg_pp0_iter2_tcts_tcp_sums_sum_V_26_reg_187 <= 17'd0;
        end else if ((1'b1 == 1'b1)) begin
            ap_phi_reg_pp0_iter2_tcts_tcp_sums_sum_V_26_reg_187 <= ap_phi_reg_pp0_iter1_tcts_tcp_sums_sum_V_26_reg_187;
        end
    end
end

always @ (posedge ap_clk) begin
    if ((1'b1 == ap_condition_237)) begin
        if (((tmp_52_reg_1010 == 1'd1) & (tmp_reg_906 == 1'd1))) begin
            ap_phi_reg_pp0_iter2_tcts_tcp_sums_sum_V_27_reg_198 <= 1'd1;
        end else if ((1'b1 == 1'b1)) begin
            ap_phi_reg_pp0_iter2_tcts_tcp_sums_sum_V_27_reg_198 <= ap_phi_reg_pp0_iter1_tcts_tcp_sums_sum_V_27_reg_198;
        end
    end
end

always @ (posedge ap_clk) begin
    if ((1'b1 == ap_condition_237)) begin
        if (((tmp_52_reg_1010 == 1'd1) & (tmp_reg_906 == 1'd1))) begin
            ap_phi_reg_pp0_iter2_tcts_tcp_sums_sum_V_28_reg_209 <= 17'd0;
        end else if ((1'b1 == 1'b1)) begin
            ap_phi_reg_pp0_iter2_tcts_tcp_sums_sum_V_28_reg_209 <= ap_phi_reg_pp0_iter1_tcts_tcp_sums_sum_V_28_reg_209;
        end
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (tmp_reg_906 == 1'd1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (icmp_ln879_2_reg_962 == 1'd0))) begin
        add_ln214_11_reg_1054 <= add_ln214_11_fu_671_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (tmp_reg_906 == 1'd1) & (icmp_ln879_3_reg_986 == 1'd1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        add_ln214_13_reg_1064 <= add_ln214_13_fu_719_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (tmp_reg_906 == 1'd1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (icmp_ln879_3_reg_986 == 1'd0))) begin
        add_ln214_15_reg_1069 <= add_ln214_15_fu_760_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (tmp_reg_906 == 1'd1) & (icmp_ln879_reg_915 == 1'd1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'd0 == and_ln647_reg_932))) begin
        add_ln214_1_reg_1019 <= add_ln214_1_fu_452_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (tmp_reg_906 == 1'd1) & (1'd1 == and_ln647_reg_932) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        add_ln214_3_reg_1024 <= add_ln214_3_fu_493_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (tmp_reg_906 == 1'd1) & (icmp_ln879_1_reg_938 == 1'd1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        add_ln214_5_reg_1034 <= add_ln214_5_fu_541_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (tmp_reg_906 == 1'd1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (icmp_ln879_1_reg_938 == 1'd0))) begin
        add_ln214_7_reg_1039 <= add_ln214_7_fu_582_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (tmp_reg_906 == 1'd1) & (icmp_ln879_2_reg_962 == 1'd1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        add_ln214_9_reg_1049 <= add_ln214_9_fu_630_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (tmp_nbreadreq_fu_104_p3 == 1'd1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        and_ln647_reg_932 <= and_ln647_fu_264_p2;
        icmp_ln879_1_reg_938 <= icmp_ln879_1_fu_280_p2;
        icmp_ln879_2_reg_962 <= icmp_ln879_2_fu_324_p2;
        icmp_ln879_3_reg_986 <= icmp_ln879_3_fu_368_p2;
        icmp_ln879_reg_915 <= icmp_ln879_fu_238_p2;
        p_Result_11_1_i_reg_945 <= {{txEng_tcpPkgBuffer1_s_8_dout[31:24]}};
        p_Result_11_2_i_reg_969 <= {{txEng_tcpPkgBuffer1_s_8_dout[47:40]}};
        p_Result_11_3_i_reg_993 <= {{txEng_tcpPkgBuffer1_s_8_dout[63:56]}};
        p_Result_11_i_reg_921 <= {{txEng_tcpPkgBuffer1_s_8_dout[15:8]}};
        p_Result_13_1_i_reg_950 <= {{txEng_tcpPkgBuffer1_s_8_dout[23:16]}};
        p_Result_13_2_i_reg_974 <= {{txEng_tcpPkgBuffer1_s_8_dout[39:32]}};
        p_Result_13_3_i_reg_998 <= {{txEng_tcpPkgBuffer1_s_8_dout[55:48]}};
        tmp33_reg_910 <= txEng_tcpPkgBuffer1_s_8_dout;
        tmp_44_reg_956 <= txEng_tcpPkgBuffer1_s_8_dout[32'd66];
        tmp_47_reg_980 <= txEng_tcpPkgBuffer1_s_8_dout[32'd68];
        tmp_50_reg_1004 <= txEng_tcpPkgBuffer1_s_8_dout[32'd70];
        tmp_52_reg_1010 <= txEng_tcpPkgBuffer1_s_8_dout[32'd72];
        trunc_ln647_reg_926 <= trunc_ln647_fu_254_p1;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        and_ln647_reg_932_pp0_iter1_reg <= and_ln647_reg_932;
        icmp_ln879_1_reg_938_pp0_iter1_reg <= icmp_ln879_1_reg_938;
        icmp_ln879_2_reg_962_pp0_iter1_reg <= icmp_ln879_2_reg_962;
        icmp_ln879_3_reg_986_pp0_iter1_reg <= icmp_ln879_3_reg_986;
        icmp_ln879_reg_915_pp0_iter1_reg <= icmp_ln879_reg_915;
        tmp_44_reg_956_pp0_iter1_reg <= tmp_44_reg_956;
        tmp_47_reg_980_pp0_iter1_reg <= tmp_47_reg_980;
        tmp_50_reg_1004_pp0_iter1_reg <= tmp_50_reg_1004;
        tmp_52_reg_1010_pp0_iter1_reg <= tmp_52_reg_1010;
        tmp_reg_906 <= tmp_nbreadreq_fu_104_p3;
        tmp_reg_906_pp0_iter1_reg <= tmp_reg_906;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (ap_start == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        ap_phi_reg_pp0_iter1_tcts_tcp_sums_sum_V_21_reg_132 <= ap_phi_reg_pp0_iter0_tcts_tcp_sums_sum_V_21_reg_132;
        ap_phi_reg_pp0_iter1_tcts_tcp_sums_sum_V_22_reg_143 <= ap_phi_reg_pp0_iter0_tcts_tcp_sums_sum_V_22_reg_143;
        ap_phi_reg_pp0_iter1_tcts_tcp_sums_sum_V_23_reg_154 <= ap_phi_reg_pp0_iter0_tcts_tcp_sums_sum_V_23_reg_154;
        ap_phi_reg_pp0_iter1_tcts_tcp_sums_sum_V_24_reg_165 <= ap_phi_reg_pp0_iter0_tcts_tcp_sums_sum_V_24_reg_165;
        ap_phi_reg_pp0_iter1_tcts_tcp_sums_sum_V_25_reg_176 <= ap_phi_reg_pp0_iter0_tcts_tcp_sums_sum_V_25_reg_176;
        ap_phi_reg_pp0_iter1_tcts_tcp_sums_sum_V_26_reg_187 <= ap_phi_reg_pp0_iter0_tcts_tcp_sums_sum_V_26_reg_187;
        ap_phi_reg_pp0_iter1_tcts_tcp_sums_sum_V_27_reg_198 <= ap_phi_reg_pp0_iter0_tcts_tcp_sums_sum_V_27_reg_198;
        ap_phi_reg_pp0_iter1_tcts_tcp_sums_sum_V_28_reg_209 <= ap_phi_reg_pp0_iter0_tcts_tcp_sums_sum_V_28_reg_209;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (tmp_reg_906 == 1'd1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        tcts_tcp_sums_sum_V_10_reg_1029 <= ap_sig_allocacmp_tcts_tcp_sums_sum_V_10;
        tcts_tcp_sums_sum_V_11_reg_1044 <= ap_sig_allocacmp_tcts_tcp_sums_sum_V_11;
        tcts_tcp_sums_sum_V_12_reg_1059 <= ap_sig_allocacmp_tcts_tcp_sums_sum_V_12;
        tcts_tcp_sums_sum_V_9_reg_1014 <= ap_sig_allocacmp_tcts_tcp_sums_sum_V_9;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (tmp_reg_906_pp0_iter1_reg == 1'd1) & (ap_phi_mux_tcts_tcp_sums_sum_V_25_phi_fu_180_p4 == 1'd1) & (ap_enable_reg_pp0_iter2 == 1'b1))) begin
        tcts_tcp_sums_sum_V_1_14 <= ap_phi_mux_tcts_tcp_sums_sum_V_26_phi_fu_191_p4;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (tmp_reg_906_pp0_iter1_reg == 1'd1) & (ap_phi_mux_tcts_tcp_sums_sum_V_23_phi_fu_158_p4 == 1'd1) & (ap_enable_reg_pp0_iter2 == 1'b1))) begin
        tcts_tcp_sums_sum_V_2_13 <= ap_phi_mux_tcts_tcp_sums_sum_V_24_phi_fu_169_p4;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (tmp_reg_906_pp0_iter1_reg == 1'd1) & (ap_phi_mux_tcts_tcp_sums_sum_V_21_phi_fu_136_p4 == 1'd1) & (ap_enable_reg_pp0_iter2 == 1'b1))) begin
        tcts_tcp_sums_sum_V_3_12 <= ap_phi_mux_tcts_tcp_sums_sum_V_22_phi_fu_147_p4;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (tmp_reg_906_pp0_iter1_reg == 1'd1) & (ap_phi_mux_tcts_tcp_sums_sum_V_27_phi_fu_202_p4 == 1'd1) & (ap_enable_reg_pp0_iter2 == 1'b1))) begin
        tcts_tcp_sums_sum_V_s <= ap_phi_mux_tcts_tcp_sums_sum_V_28_phi_fu_213_p4;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (ap_enable_reg_pp0_iter2 == 1'b1))) begin
        ap_done = 1'b1;
    end else begin
        ap_done = ap_done_reg;
    end
end

always @ (*) begin
    if (((ap_start == 1'b0) & (ap_idle_pp0 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        ap_idle = 1'b1;
    end else begin
        ap_idle = 1'b0;
    end
end

always @ (*) begin
    if (((ap_enable_reg_pp0_iter2 == 1'b0) & (ap_enable_reg_pp0_iter1 == 1'b0) & (ap_enable_reg_pp0_iter0 == 1'b0))) begin
        ap_idle_pp0 = 1'b1;
    end else begin
        ap_idle_pp0 = 1'b0;
    end
end

always @ (*) begin
    if (((ap_enable_reg_pp0_iter1 == 1'b0) & (ap_enable_reg_pp0_iter0 == 1'b0))) begin
        ap_idle_pp0_0to1 = 1'b1;
    end else begin
        ap_idle_pp0_0to1 = 1'b0;
    end
end

always @ (*) begin
    if (((tmp_52_reg_1010_pp0_iter1_reg == 1'd0) & (tmp_reg_906_pp0_iter1_reg == 1'd1))) begin
        ap_phi_mux_tcts_tcp_sums_sum_V_21_phi_fu_136_p4 = or_ln647_fu_772_p2;
    end else begin
        ap_phi_mux_tcts_tcp_sums_sum_V_21_phi_fu_136_p4 = ap_phi_reg_pp0_iter2_tcts_tcp_sums_sum_V_21_reg_132;
    end
end

always @ (*) begin
    if (((tmp_52_reg_1010_pp0_iter1_reg == 1'd0) & (tmp_reg_906_pp0_iter1_reg == 1'd1))) begin
        ap_phi_mux_tcts_tcp_sums_sum_V_22_phi_fu_147_p4 = select_ln647_fu_783_p3;
    end else begin
        ap_phi_mux_tcts_tcp_sums_sum_V_22_phi_fu_147_p4 = ap_phi_reg_pp0_iter2_tcts_tcp_sums_sum_V_22_reg_143;
    end
end

always @ (*) begin
    if (((tmp_52_reg_1010_pp0_iter1_reg == 1'd0) & (tmp_reg_906_pp0_iter1_reg == 1'd1))) begin
        ap_phi_mux_tcts_tcp_sums_sum_V_23_phi_fu_158_p4 = or_ln791_fu_791_p2;
    end else begin
        ap_phi_mux_tcts_tcp_sums_sum_V_23_phi_fu_158_p4 = ap_phi_reg_pp0_iter2_tcts_tcp_sums_sum_V_23_reg_154;
    end
end

always @ (*) begin
    if (((tmp_52_reg_1010_pp0_iter1_reg == 1'd0) & (tmp_reg_906_pp0_iter1_reg == 1'd1))) begin
        ap_phi_mux_tcts_tcp_sums_sum_V_24_phi_fu_169_p4 = select_ln791_fu_809_p3;
    end else begin
        ap_phi_mux_tcts_tcp_sums_sum_V_24_phi_fu_169_p4 = ap_phi_reg_pp0_iter2_tcts_tcp_sums_sum_V_24_reg_165;
    end
end

always @ (*) begin
    if (((tmp_52_reg_1010_pp0_iter1_reg == 1'd0) & (tmp_reg_906_pp0_iter1_reg == 1'd1))) begin
        ap_phi_mux_tcts_tcp_sums_sum_V_25_phi_fu_180_p4 = or_ln791_1_fu_817_p2;
    end else begin
        ap_phi_mux_tcts_tcp_sums_sum_V_25_phi_fu_180_p4 = ap_phi_reg_pp0_iter2_tcts_tcp_sums_sum_V_25_reg_176;
    end
end

always @ (*) begin
    if (((tmp_52_reg_1010_pp0_iter1_reg == 1'd0) & (tmp_reg_906_pp0_iter1_reg == 1'd1))) begin
        ap_phi_mux_tcts_tcp_sums_sum_V_26_phi_fu_191_p4 = select_ln791_1_fu_835_p3;
    end else begin
        ap_phi_mux_tcts_tcp_sums_sum_V_26_phi_fu_191_p4 = ap_phi_reg_pp0_iter2_tcts_tcp_sums_sum_V_26_reg_187;
    end
end

always @ (*) begin
    if (((tmp_52_reg_1010_pp0_iter1_reg == 1'd0) & (tmp_reg_906_pp0_iter1_reg == 1'd1))) begin
        ap_phi_mux_tcts_tcp_sums_sum_V_27_phi_fu_202_p4 = or_ln791_2_fu_843_p2;
    end else begin
        ap_phi_mux_tcts_tcp_sums_sum_V_27_phi_fu_202_p4 = ap_phi_reg_pp0_iter2_tcts_tcp_sums_sum_V_27_reg_198;
    end
end

always @ (*) begin
    if (((tmp_52_reg_1010_pp0_iter1_reg == 1'd0) & (tmp_reg_906_pp0_iter1_reg == 1'd1))) begin
        ap_phi_mux_tcts_tcp_sums_sum_V_28_phi_fu_213_p4 = select_ln791_2_fu_861_p3;
    end else begin
        ap_phi_mux_tcts_tcp_sums_sum_V_28_phi_fu_213_p4 = ap_phi_reg_pp0_iter2_tcts_tcp_sums_sum_V_28_reg_209;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (ap_start == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        ap_ready = 1'b1;
    end else begin
        ap_ready = 1'b0;
    end
end

always @ (*) begin
    if (((ap_start == 1'b0) & (ap_idle_pp0_0to1 == 1'b1))) begin
        ap_reset_idle_pp0 = 1'b1;
    end else begin
        ap_reset_idle_pp0 = 1'b0;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0) & (tmp_reg_906_pp0_iter1_reg == 1'd1) & (ap_phi_mux_tcts_tcp_sums_sum_V_23_phi_fu_158_p4 == 1'd1) & (ap_enable_reg_pp0_iter2 == 1'b1))) begin
        ap_sig_allocacmp_tcts_tcp_sums_sum_V_10 = ap_phi_mux_tcts_tcp_sums_sum_V_24_phi_fu_169_p4;
    end else begin
        ap_sig_allocacmp_tcts_tcp_sums_sum_V_10 = tcts_tcp_sums_sum_V_2_13;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0) & (tmp_reg_906_pp0_iter1_reg == 1'd1) & (ap_phi_mux_tcts_tcp_sums_sum_V_25_phi_fu_180_p4 == 1'd1) & (ap_enable_reg_pp0_iter2 == 1'b1))) begin
        ap_sig_allocacmp_tcts_tcp_sums_sum_V_11 = ap_phi_mux_tcts_tcp_sums_sum_V_26_phi_fu_191_p4;
    end else begin
        ap_sig_allocacmp_tcts_tcp_sums_sum_V_11 = tcts_tcp_sums_sum_V_1_14;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0) & (tmp_reg_906_pp0_iter1_reg == 1'd1) & (ap_phi_mux_tcts_tcp_sums_sum_V_27_phi_fu_202_p4 == 1'd1) & (ap_enable_reg_pp0_iter2 == 1'b1))) begin
        ap_sig_allocacmp_tcts_tcp_sums_sum_V_12 = ap_phi_mux_tcts_tcp_sums_sum_V_28_phi_fu_213_p4;
    end else begin
        ap_sig_allocacmp_tcts_tcp_sums_sum_V_12 = tcts_tcp_sums_sum_V_s;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0) & (tmp_reg_906_pp0_iter1_reg == 1'd1) & (ap_phi_mux_tcts_tcp_sums_sum_V_21_phi_fu_136_p4 == 1'd1) & (ap_enable_reg_pp0_iter2 == 1'b1))) begin
        ap_sig_allocacmp_tcts_tcp_sums_sum_V_9 = ap_phi_mux_tcts_tcp_sums_sum_V_22_phi_fu_147_p4;
    end else begin
        ap_sig_allocacmp_tcts_tcp_sums_sum_V_9 = tcts_tcp_sums_sum_V_3_12;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0) & (ap_enable_reg_pp0_iter2 == 1'b1) & (ap_predicate_op122_write_state3 == 1'b1))) begin
        txEng_subChecksumsFi_1_blk_n = txEng_subChecksumsFi_1_full_n;
    end else begin
        txEng_subChecksumsFi_1_blk_n = 1'b1;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (ap_enable_reg_pp0_iter2 == 1'b1) & (ap_predicate_op122_write_state3 == 1'b1))) begin
        txEng_subChecksumsFi_1_write = 1'b1;
    end else begin
        txEng_subChecksumsFi_1_write = 1'b0;
    end
end

always @ (*) begin
    if ((~((ap_start == 1'b0) | (ap_done_reg == 1'b1)) & (1'b0 == ap_block_pp0_stage0) & (tmp_nbreadreq_fu_104_p3 == 1'd1) & (ap_start == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        txEng_tcpPkgBuffer1_s_8_blk_n = txEng_tcpPkgBuffer1_s_8_empty_n;
    end else begin
        txEng_tcpPkgBuffer1_s_8_blk_n = 1'b1;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (tmp_nbreadreq_fu_104_p3 == 1'd1) & (ap_start == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        txEng_tcpPkgBuffer1_s_8_read = 1'b1;
    end else begin
        txEng_tcpPkgBuffer1_s_8_read = 1'b0;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0) & (tmp_reg_906 == 1'd1) & (ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        txEng_tcpPkgBuffer2_s_7_blk_n = txEng_tcpPkgBuffer2_s_7_full_n;
    end else begin
        txEng_tcpPkgBuffer2_s_7_blk_n = 1'b1;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (tmp_reg_906 == 1'd1) & (ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        txEng_tcpPkgBuffer2_s_7_write = 1'b1;
    end else begin
        txEng_tcpPkgBuffer2_s_7_write = 1'b0;
    end
end

always @ (*) begin
    case (ap_CS_fsm)
        ap_ST_fsm_pp0_stage0 : begin
            ap_NS_fsm = ap_ST_fsm_pp0_stage0;
        end
        default : begin
            ap_NS_fsm = 'bx;
        end
    endcase
end

assign add_ln214_10_fu_665_p2 = (zext_ln214_15_fu_661_p1 + trunc_ln700_5_fu_602_p1);

assign add_ln214_11_fu_671_p2 = (p_Result_17_2_i_fu_636_p3 + add_ln214_10_fu_665_p2);

assign add_ln214_12_fu_713_p2 = (zext_ln214_16_fu_709_p1 + trunc_ln700_6_fu_691_p1);

assign add_ln214_13_fu_719_p2 = (p_Result_14_3_i_fu_681_p3 + add_ln214_12_fu_713_p2);

assign add_ln214_14_fu_754_p2 = (zext_ln214_17_fu_750_p1 + trunc_ln700_6_fu_691_p1);

assign add_ln214_15_fu_760_p2 = (p_Result_17_3_i_fu_725_p3 + add_ln214_14_fu_754_p2);

assign add_ln214_1_fu_452_p2 = (p_Result_14_i_fu_414_p3 + add_ln214_fu_446_p2);

assign add_ln214_2_fu_487_p2 = (zext_ln214_11_fu_483_p1 + trunc_ln700_fu_424_p1);

assign add_ln214_3_fu_493_p2 = (p_Result_17_i_fu_458_p3 + add_ln214_2_fu_487_p2);

assign add_ln214_4_fu_535_p2 = (zext_ln214_12_fu_531_p1 + trunc_ln700_4_fu_513_p1);

assign add_ln214_5_fu_541_p2 = (p_Result_14_1_i_fu_503_p3 + add_ln214_4_fu_535_p2);

assign add_ln214_6_fu_576_p2 = (zext_ln214_13_fu_572_p1 + trunc_ln700_4_fu_513_p1);

assign add_ln214_7_fu_582_p2 = (p_Result_17_1_i_fu_547_p3 + add_ln214_6_fu_576_p2);

assign add_ln214_8_fu_624_p2 = (zext_ln214_14_fu_620_p1 + trunc_ln700_5_fu_602_p1);

assign add_ln214_9_fu_630_p2 = (p_Result_14_2_i_fu_592_p3 + add_ln214_8_fu_624_p2);

assign add_ln214_fu_446_p2 = (zext_ln214_10_fu_442_p1 + trunc_ln700_fu_424_p1);

assign add_ln700_1_fu_469_p2 = (zext_ln700_1_fu_465_p1 + ap_sig_allocacmp_tcts_tcp_sums_sum_V_9);

assign add_ln700_2_fu_517_p2 = (zext_ln700_2_fu_509_p1 + ap_sig_allocacmp_tcts_tcp_sums_sum_V_10);

assign add_ln700_3_fu_558_p2 = (zext_ln700_3_fu_554_p1 + ap_sig_allocacmp_tcts_tcp_sums_sum_V_10);

assign add_ln700_4_fu_606_p2 = (zext_ln700_4_fu_598_p1 + ap_sig_allocacmp_tcts_tcp_sums_sum_V_11);

assign add_ln700_5_fu_647_p2 = (zext_ln700_5_fu_643_p1 + ap_sig_allocacmp_tcts_tcp_sums_sum_V_11);

assign add_ln700_6_fu_695_p2 = (zext_ln700_6_fu_687_p1 + ap_sig_allocacmp_tcts_tcp_sums_sum_V_12);

assign add_ln700_7_fu_736_p2 = (zext_ln700_7_fu_732_p1 + ap_sig_allocacmp_tcts_tcp_sums_sum_V_12);

assign add_ln700_fu_428_p2 = (zext_ln700_fu_420_p1 + ap_sig_allocacmp_tcts_tcp_sums_sum_V_9);

assign and_ln647_fu_264_p2 = (xor_ln879_fu_258_p2 & tmp_40_fu_220_p3);

assign ap_CS_fsm_pp0_stage0 = ap_CS_fsm[32'd0];

assign ap_block_pp0_stage0 = ~(1'b1 == 1'b1);

always @ (*) begin
    ap_block_pp0_stage0_01001 = ((ap_done_reg == 1'b1) | ((txEng_subChecksumsFi_1_full_n == 1'b0) & (ap_enable_reg_pp0_iter2 == 1'b1) & (ap_predicate_op122_write_state3 == 1'b1)) | ((tmp_reg_906 == 1'd1) & (txEng_tcpPkgBuffer2_s_7_full_n == 1'b0) & (ap_enable_reg_pp0_iter1 == 1'b1)) | ((ap_start == 1'b1) & ((ap_start == 1'b0) | (ap_done_reg == 1'b1) | ((tmp_nbreadreq_fu_104_p3 == 1'd1) & (txEng_tcpPkgBuffer1_s_8_empty_n == 1'b0)))));
end

always @ (*) begin
    ap_block_pp0_stage0_11001 = ((ap_done_reg == 1'b1) | ((txEng_subChecksumsFi_1_full_n == 1'b0) & (ap_enable_reg_pp0_iter2 == 1'b1) & (ap_predicate_op122_write_state3 == 1'b1)) | ((tmp_reg_906 == 1'd1) & (txEng_tcpPkgBuffer2_s_7_full_n == 1'b0) & (ap_enable_reg_pp0_iter1 == 1'b1)) | ((ap_start == 1'b1) & ((ap_start == 1'b0) | (ap_done_reg == 1'b1) | ((tmp_nbreadreq_fu_104_p3 == 1'd1) & (txEng_tcpPkgBuffer1_s_8_empty_n == 1'b0)))));
end

always @ (*) begin
    ap_block_pp0_stage0_subdone = ((ap_done_reg == 1'b1) | ((txEng_subChecksumsFi_1_full_n == 1'b0) & (ap_enable_reg_pp0_iter2 == 1'b1) & (ap_predicate_op122_write_state3 == 1'b1)) | ((tmp_reg_906 == 1'd1) & (txEng_tcpPkgBuffer2_s_7_full_n == 1'b0) & (ap_enable_reg_pp0_iter1 == 1'b1)) | ((ap_start == 1'b1) & ((ap_start == 1'b0) | (ap_done_reg == 1'b1) | ((tmp_nbreadreq_fu_104_p3 == 1'd1) & (txEng_tcpPkgBuffer1_s_8_empty_n == 1'b0)))));
end

always @ (*) begin
    ap_block_state1_pp0_stage0_iter0 = ((ap_start == 1'b0) | (ap_done_reg == 1'b1) | ((tmp_nbreadreq_fu_104_p3 == 1'd1) & (txEng_tcpPkgBuffer1_s_8_empty_n == 1'b0)));
end

always @ (*) begin
    ap_block_state2_pp0_stage0_iter1 = ((tmp_reg_906 == 1'd1) & (txEng_tcpPkgBuffer2_s_7_full_n == 1'b0));
end

always @ (*) begin
    ap_block_state3_pp0_stage0_iter2 = ((txEng_subChecksumsFi_1_full_n == 1'b0) & (ap_predicate_op122_write_state3 == 1'b1));
end

always @ (*) begin
    ap_condition_237 = ((1'b0 == ap_block_pp0_stage0_11001) & (ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0));
end

assign ap_enable_pp0 = (ap_idle_pp0 ^ 1'b1);

assign ap_enable_reg_pp0_iter0 = ap_start;

assign ap_phi_reg_pp0_iter0_tcts_tcp_sums_sum_V_21_reg_132 = 'bx;

assign ap_phi_reg_pp0_iter0_tcts_tcp_sums_sum_V_22_reg_143 = 'bx;

assign ap_phi_reg_pp0_iter0_tcts_tcp_sums_sum_V_23_reg_154 = 'bx;

assign ap_phi_reg_pp0_iter0_tcts_tcp_sums_sum_V_24_reg_165 = 'bx;

assign ap_phi_reg_pp0_iter0_tcts_tcp_sums_sum_V_25_reg_176 = 'bx;

assign ap_phi_reg_pp0_iter0_tcts_tcp_sums_sum_V_26_reg_187 = 'bx;

assign ap_phi_reg_pp0_iter0_tcts_tcp_sums_sum_V_27_reg_198 = 'bx;

assign ap_phi_reg_pp0_iter0_tcts_tcp_sums_sum_V_28_reg_209 = 'bx;

always @ (*) begin
    ap_predicate_op122_write_state3 = ((tmp_52_reg_1010_pp0_iter1_reg == 1'd1) & (tmp_reg_906_pp0_iter1_reg == 1'd1));
end

assign icmp_ln879_1_fu_280_p2 = ((p_Result_10_1_i_fu_270_p4 == 2'd3) ? 1'b1 : 1'b0);

assign icmp_ln879_2_fu_324_p2 = ((p_Result_10_2_i_fu_314_p4 == 2'd3) ? 1'b1 : 1'b0);

assign icmp_ln879_3_fu_368_p2 = ((p_Result_10_3_i_fu_358_p4 == 2'd3) ? 1'b1 : 1'b0);

assign icmp_ln879_fu_238_p2 = ((p_Result_10_i_fu_228_p4 == 2'd3) ? 1'b1 : 1'b0);

assign or_ln647_fu_772_p2 = (icmp_ln879_reg_915_pp0_iter1_reg | and_ln647_reg_932_pp0_iter1_reg);

assign or_ln791_1_fu_817_p2 = (tmp_47_reg_980_pp0_iter1_reg | icmp_ln879_2_reg_962_pp0_iter1_reg);

assign or_ln791_2_fu_843_p2 = (tmp_50_reg_1004_pp0_iter1_reg | icmp_ln879_3_reg_986_pp0_iter1_reg);

assign or_ln791_6_fu_805_p2 = (tmp_44_reg_956_pp0_iter1_reg | icmp_ln879_1_reg_938_pp0_iter1_reg);

assign or_ln791_7_fu_831_p2 = (tmp_47_reg_980_pp0_iter1_reg | icmp_ln879_2_reg_962_pp0_iter1_reg);

assign or_ln791_8_fu_857_p2 = (tmp_50_reg_1004_pp0_iter1_reg | icmp_ln879_3_reg_986_pp0_iter1_reg);

assign or_ln791_fu_791_p2 = (tmp_44_reg_956_pp0_iter1_reg | icmp_ln879_1_reg_938_pp0_iter1_reg);

assign p_Result_10_1_i_fu_270_p4 = {{txEng_tcpPkgBuffer1_s_8_dout[67:66]}};

assign p_Result_10_2_i_fu_314_p4 = {{txEng_tcpPkgBuffer1_s_8_dout[69:68]}};

assign p_Result_10_3_i_fu_358_p4 = {{txEng_tcpPkgBuffer1_s_8_dout[71:70]}};

assign p_Result_10_i_fu_228_p4 = {{txEng_tcpPkgBuffer1_s_8_dout[65:64]}};

assign p_Result_14_1_i_fu_503_p3 = {{p_Result_13_1_i_reg_950}, {p_Result_11_1_i_reg_945}};

assign p_Result_14_2_i_fu_592_p3 = {{p_Result_13_2_i_reg_974}, {p_Result_11_2_i_reg_969}};

assign p_Result_14_3_i_fu_681_p3 = {{p_Result_13_3_i_reg_998}, {p_Result_11_3_i_reg_993}};

assign p_Result_14_i_fu_414_p3 = {{trunc_ln647_reg_926}, {p_Result_11_i_reg_921}};

assign p_Result_17_1_i_fu_547_p3 = {{p_Result_13_1_i_reg_950}, {8'd0}};

assign p_Result_17_2_i_fu_636_p3 = {{p_Result_13_2_i_reg_974}, {8'd0}};

assign p_Result_17_3_i_fu_725_p3 = {{p_Result_13_3_i_reg_998}, {8'd0}};

assign p_Result_17_i_fu_458_p3 = {{trunc_ln647_reg_926}, {8'd0}};

assign select_ln647_fu_783_p3 = ((and_ln647_reg_932_pp0_iter1_reg[0:0] === 1'b1) ? zext_ln214_1_fu_769_p1 : select_ln879_fu_777_p3);

assign select_ln791_1_fu_835_p3 = ((or_ln791_7_fu_831_p2[0:0] === 1'b1) ? zext_ln879_1_fu_827_p1 : tcts_tcp_sums_sum_V_11_reg_1044);

assign select_ln791_2_fu_861_p3 = ((or_ln791_8_fu_857_p2[0:0] === 1'b1) ? zext_ln879_2_fu_853_p1 : tcts_tcp_sums_sum_V_12_reg_1059);

assign select_ln791_fu_809_p3 = ((or_ln791_6_fu_805_p2[0:0] === 1'b1) ? zext_ln879_fu_801_p1 : tcts_tcp_sums_sum_V_10_reg_1029);

assign select_ln879_1_fu_796_p3 = ((icmp_ln879_1_reg_938_pp0_iter1_reg[0:0] === 1'b1) ? add_ln214_5_reg_1034 : add_ln214_7_reg_1039);

assign select_ln879_2_fu_822_p3 = ((icmp_ln879_2_reg_962_pp0_iter1_reg[0:0] === 1'b1) ? add_ln214_9_reg_1049 : add_ln214_11_reg_1054);

assign select_ln879_3_fu_848_p3 = ((icmp_ln879_3_reg_986_pp0_iter1_reg[0:0] === 1'b1) ? add_ln214_13_reg_1064 : add_ln214_15_reg_1069);

assign select_ln879_fu_777_p3 = ((icmp_ln879_reg_915_pp0_iter1_reg[0:0] === 1'b1) ? zext_ln214_fu_766_p1 : tcts_tcp_sums_sum_V_9_reg_1014);

assign tmp_40_fu_220_p3 = txEng_tcpPkgBuffer1_s_8_dout[32'd64];

assign tmp_41_fu_434_p3 = add_ln700_fu_428_p2[32'd16];

assign tmp_42_fu_475_p3 = add_ln700_1_fu_469_p2[32'd16];

assign tmp_43_fu_523_p3 = add_ln700_2_fu_517_p2[32'd16];

assign tmp_45_fu_564_p3 = add_ln700_3_fu_558_p2[32'd16];

assign tmp_46_fu_612_p3 = add_ln700_4_fu_606_p2[32'd16];

assign tmp_48_fu_653_p3 = add_ln700_5_fu_647_p2[32'd16];

assign tmp_49_fu_701_p3 = add_ln700_6_fu_695_p2[32'd16];

assign tmp_51_fu_742_p3 = add_ln700_7_fu_736_p2[32'd16];

assign tmp_nbreadreq_fu_104_p3 = txEng_tcpPkgBuffer1_s_8_empty_n;

assign trunc_ln647_fu_254_p1 = txEng_tcpPkgBuffer1_s_8_dout[7:0];

assign trunc_ln700_4_fu_513_p1 = ap_sig_allocacmp_tcts_tcp_sums_sum_V_10[15:0];

assign trunc_ln700_5_fu_602_p1 = ap_sig_allocacmp_tcts_tcp_sums_sum_V_11[15:0];

assign trunc_ln700_6_fu_691_p1 = ap_sig_allocacmp_tcts_tcp_sums_sum_V_12[15:0];

assign trunc_ln700_fu_424_p1 = ap_sig_allocacmp_tcts_tcp_sums_sum_V_9[15:0];

assign txEng_subChecksumsFi_1_din = {{{{select_ln791_2_fu_861_p3}, {select_ln791_1_fu_835_p3}}, {select_ln791_fu_809_p3}}, {select_ln647_fu_783_p3}};

assign txEng_tcpPkgBuffer2_s_7_din = tmp33_reg_910;

assign xor_ln879_fu_258_p2 = (icmp_ln879_fu_238_p2 ^ 1'd1);

assign zext_ln214_10_fu_442_p1 = tmp_41_fu_434_p3;

assign zext_ln214_11_fu_483_p1 = tmp_42_fu_475_p3;

assign zext_ln214_12_fu_531_p1 = tmp_43_fu_523_p3;

assign zext_ln214_13_fu_572_p1 = tmp_45_fu_564_p3;

assign zext_ln214_14_fu_620_p1 = tmp_46_fu_612_p3;

assign zext_ln214_15_fu_661_p1 = tmp_48_fu_653_p3;

assign zext_ln214_16_fu_709_p1 = tmp_49_fu_701_p3;

assign zext_ln214_17_fu_750_p1 = tmp_51_fu_742_p3;

assign zext_ln214_1_fu_769_p1 = add_ln214_3_reg_1024;

assign zext_ln214_fu_766_p1 = add_ln214_1_reg_1019;

assign zext_ln700_1_fu_465_p1 = p_Result_17_i_fu_458_p3;

assign zext_ln700_2_fu_509_p1 = p_Result_14_1_i_fu_503_p3;

assign zext_ln700_3_fu_554_p1 = p_Result_17_1_i_fu_547_p3;

assign zext_ln700_4_fu_598_p1 = p_Result_14_2_i_fu_592_p3;

assign zext_ln700_5_fu_643_p1 = p_Result_17_2_i_fu_636_p3;

assign zext_ln700_6_fu_687_p1 = p_Result_14_3_i_fu_681_p3;

assign zext_ln700_7_fu_732_p1 = p_Result_17_3_i_fu_725_p3;

assign zext_ln700_fu_420_p1 = p_Result_14_i_fu_414_p3;

assign zext_ln879_1_fu_827_p1 = select_ln879_2_fu_822_p3;

assign zext_ln879_2_fu_853_p1 = select_ln879_3_fu_848_p3;

assign zext_ln879_fu_801_p1 = select_ln879_1_fu_796_p3;

endmodule //two_complement_subch
