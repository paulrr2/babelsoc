// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================
// AXILiteS
// 0x00 : reserved
// 0x04 : reserved
// 0x08 : reserved
// 0x0c : reserved
// 0x10 : Data signal of myMacAddress_V
//        bit 31~0 - myMacAddress_V[31:0] (Read/Write)
// 0x14 : Data signal of myMacAddress_V
//        bit 15~0 - myMacAddress_V[47:32] (Read/Write)
//        others   - reserved
// 0x18 : reserved
// 0x1c : Data signal of regSubNetMask_V
//        bit 31~0 - regSubNetMask_V[31:0] (Read/Write)
// 0x20 : reserved
// 0x24 : Data signal of regDefaultGateway_V
//        bit 31~0 - regDefaultGateway_V[31:0] (Read/Write)
// 0x28 : reserved
// (SC = Self Clear, COR = Clear on Read, TOW = Toggle on Write, COH = Clear on Handshake)

#define XMAC_IP_ENCODE_TOP_AXILITES_ADDR_MYMACADDRESS_V_DATA      0x10
#define XMAC_IP_ENCODE_TOP_AXILITES_BITS_MYMACADDRESS_V_DATA      48
#define XMAC_IP_ENCODE_TOP_AXILITES_ADDR_REGSUBNETMASK_V_DATA     0x1c
#define XMAC_IP_ENCODE_TOP_AXILITES_BITS_REGSUBNETMASK_V_DATA     32
#define XMAC_IP_ENCODE_TOP_AXILITES_ADDR_REGDEFAULTGATEWAY_V_DATA 0x24
#define XMAC_IP_ENCODE_TOP_AXILITES_BITS_REGDEFAULTGATEWAY_V_DATA 32

