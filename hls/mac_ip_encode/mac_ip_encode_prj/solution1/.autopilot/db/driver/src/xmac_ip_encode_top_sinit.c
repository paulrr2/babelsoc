// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef __linux__

#include "xstatus.h"
#include "xparameters.h"
#include "xmac_ip_encode_top.h"

extern XMac_ip_encode_top_Config XMac_ip_encode_top_ConfigTable[];

XMac_ip_encode_top_Config *XMac_ip_encode_top_LookupConfig(u16 DeviceId) {
	XMac_ip_encode_top_Config *ConfigPtr = NULL;

	int Index;

	for (Index = 0; Index < XPAR_XMAC_IP_ENCODE_TOP_NUM_INSTANCES; Index++) {
		if (XMac_ip_encode_top_ConfigTable[Index].DeviceId == DeviceId) {
			ConfigPtr = &XMac_ip_encode_top_ConfigTable[Index];
			break;
		}
	}

	return ConfigPtr;
}

int XMac_ip_encode_top_Initialize(XMac_ip_encode_top *InstancePtr, u16 DeviceId) {
	XMac_ip_encode_top_Config *ConfigPtr;

	Xil_AssertNonvoid(InstancePtr != NULL);

	ConfigPtr = XMac_ip_encode_top_LookupConfig(DeviceId);
	if (ConfigPtr == NULL) {
		InstancePtr->IsReady = 0;
		return (XST_DEVICE_NOT_FOUND);
	}

	return XMac_ip_encode_top_CfgInitialize(InstancePtr, ConfigPtr);
}

#endif

