set moduleName mac_ip_encode_top
set isTopModule 1
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type dataflow
set FunctionProtocol ap_ctrl_none
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {mac_ip_encode_top}
set C_modelType { void 0 }
set C_modelArgList {
	{ s_axis_ip_V_data_V int 64 regular {axi_s 0 volatile  { s_axis_ip Data } }  }
	{ s_axis_ip_V_keep_V int 8 regular {axi_s 0 volatile  { s_axis_ip Keep } }  }
	{ s_axis_ip_V_last_V int 1 regular {axi_s 0 volatile  { s_axis_ip Last } }  }
	{ s_axis_arp_lookup_reply_V int 56 regular {axi_s 0 volatile  { s_axis_arp_lookup_reply_V Data } }  }
	{ m_axis_ip_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_ip Data } }  }
	{ m_axis_ip_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_ip Keep } }  }
	{ m_axis_ip_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_ip Last } }  }
	{ m_axis_arp_lookup_request_V_V int 32 regular {axi_s 1 volatile  { m_axis_arp_lookup_request_V_V Data } }  }
	{ myMacAddress_V int 48 regular {ap_stable 0} }
	{ regSubNetMask_V int 32 regular {ap_stable 0} }
	{ regDefaultGateway_V int 32 regular {ap_stable 0} }
}
set C_modelArgMapList {[ 
	{ "Name" : "s_axis_ip_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "READONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "s_axis_ip.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_ip_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "s_axis_ip.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_ip_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "s_axis_ip.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_arp_lookup_reply_V", "interface" : "axis", "bitwidth" : 56, "direction" : "READONLY", "bitSlice":[{"low":0,"up":47,"cElement": [{"cName": "s_axis_arp_lookup_reply.V.macAddress.V","cData": "uint48","bit_use": { "low": 0,"up": 47},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":48,"up":48,"cElement": [{"cName": "s_axis_arp_lookup_reply.V.hit","cData": "bool","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_ip_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "m_axis_ip.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_ip_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "m_axis_ip.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_ip_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "m_axis_ip.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_arp_lookup_request_V_V", "interface" : "axis", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "m_axis_arp_lookup_request.V.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "myMacAddress_V", "interface" : "wire", "bitwidth" : 48, "direction" : "READONLY", "bitSlice":[{"low":0,"up":47,"cElement": [{"cName": "myMacAddress.V","cData": "uint48","bit_use": { "low": 0,"up": 47},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}]} , 
 	{ "Name" : "regSubNetMask_V", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "regSubNetMask.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}]} , 
 	{ "Name" : "regDefaultGateway_V", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "regDefaultGateway.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}]} ]}
# RTL Port declarations: 
set portNum 21
set portList { 
	{ s_axis_ip_TDATA sc_in sc_lv 64 signal 0 } 
	{ s_axis_ip_TKEEP sc_in sc_lv 8 signal 1 } 
	{ s_axis_ip_TLAST sc_in sc_lv 1 signal 2 } 
	{ s_axis_arp_lookup_reply_V_TDATA sc_in sc_lv 56 signal 3 } 
	{ m_axis_ip_TDATA sc_out sc_lv 64 signal 4 } 
	{ m_axis_ip_TKEEP sc_out sc_lv 8 signal 5 } 
	{ m_axis_ip_TLAST sc_out sc_lv 1 signal 6 } 
	{ m_axis_arp_lookup_request_V_V_TDATA sc_out sc_lv 32 signal 7 } 
	{ myMacAddress_V sc_in sc_lv 48 signal 8 } 
	{ regSubNetMask_V sc_in sc_lv 32 signal 9 } 
	{ regDefaultGateway_V sc_in sc_lv 32 signal 10 } 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst_n sc_in sc_logic 1 reset -1 active_low_sync } 
	{ s_axis_ip_TVALID sc_in sc_logic 1 invld 2 } 
	{ s_axis_ip_TREADY sc_out sc_logic 1 inacc 2 } 
	{ m_axis_arp_lookup_request_V_V_TVALID sc_out sc_logic 1 outvld 7 } 
	{ m_axis_arp_lookup_request_V_V_TREADY sc_in sc_logic 1 outacc 7 } 
	{ s_axis_arp_lookup_reply_V_TVALID sc_in sc_logic 1 invld 3 } 
	{ s_axis_arp_lookup_reply_V_TREADY sc_out sc_logic 1 inacc 3 } 
	{ m_axis_ip_TVALID sc_out sc_logic 1 outvld 6 } 
	{ m_axis_ip_TREADY sc_in sc_logic 1 outacc 6 } 
}
set NewPortList {[ 
	{ "name": "s_axis_ip_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "s_axis_ip_V_data_V", "role": "default" }} , 
 	{ "name": "s_axis_ip_TKEEP", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "s_axis_ip_V_keep_V", "role": "default" }} , 
 	{ "name": "s_axis_ip_TLAST", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "s_axis_ip_V_last_V", "role": "default" }} , 
 	{ "name": "s_axis_arp_lookup_reply_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":56, "type": "signal", "bundle":{"name": "s_axis_arp_lookup_reply_V", "role": "TDATA" }} , 
 	{ "name": "m_axis_ip_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "m_axis_ip_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_ip_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "m_axis_ip_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_ip_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "m_axis_ip_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_arp_lookup_request_V_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "m_axis_arp_lookup_request_V_V", "role": "TDATA" }} , 
 	{ "name": "myMacAddress_V", "direction": "in", "datatype": "sc_lv", "bitwidth":48, "type": "signal", "bundle":{"name": "myMacAddress_V", "role": "default" }} , 
 	{ "name": "regSubNetMask_V", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regSubNetMask_V", "role": "default" }} , 
 	{ "name": "regDefaultGateway_V", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regDefaultGateway_V", "role": "default" }} , 
 	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst_n", "role": "default" }} , 
 	{ "name": "s_axis_ip_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "s_axis_ip_V_last_V", "role": "default" }} , 
 	{ "name": "s_axis_ip_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "s_axis_ip_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_arp_lookup_request_V_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_arp_lookup_request_V_V", "role": "TVALID" }} , 
 	{ "name": "m_axis_arp_lookup_request_V_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_arp_lookup_request_V_V", "role": "TREADY" }} , 
 	{ "name": "s_axis_arp_lookup_reply_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "s_axis_arp_lookup_reply_V", "role": "TVALID" }} , 
 	{ "name": "s_axis_arp_lookup_reply_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "s_axis_arp_lookup_reply_V", "role": "TREADY" }} , 
 	{ "name": "m_axis_ip_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_ip_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_ip_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_ip_V_last_V", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "3", "4", "5", "6", "7", "8", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22"],
		"CDFG" : "mac_ip_encode_top",
		"Protocol" : "ap_ctrl_none",
		"ControlExist" : "0", "ap_start" : "0", "ap_ready" : "0", "ap_done" : "0", "ap_continue" : "0", "ap_idle" : "0",
		"Pipeline" : "Dataflow", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "1",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "21", "EstimateLatencyMax" : "21",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "1",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"InputProcess" : [
			{"ID" : "1", "Name" : "extract_ip_address_U0"},
			{"ID" : "6", "Name" : "handle_arp_reply_64_U0"}],
		"OutputProcess" : [
			{"ID" : "1", "Name" : "extract_ip_address_U0"},
			{"ID" : "8", "Name" : "insert_ethernet_head_U0"}],
		"Port" : [
			{"Name" : "s_axis_ip_V_data_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "extract_ip_address_U0", "Port" : "dataIn_V_data_V"}]},
			{"Name" : "s_axis_ip_V_keep_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "extract_ip_address_U0", "Port" : "dataIn_V_keep_V"}]},
			{"Name" : "s_axis_ip_V_last_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "extract_ip_address_U0", "Port" : "dataIn_V_last_V"}]},
			{"Name" : "s_axis_arp_lookup_reply_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "6", "SubInstance" : "handle_arp_reply_64_U0", "Port" : "arpTableIn_V"}]},
			{"Name" : "m_axis_ip_V_data_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "insert_ethernet_head_U0", "Port" : "dataOut_V_data_V"}]},
			{"Name" : "m_axis_ip_V_keep_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "insert_ethernet_head_U0", "Port" : "dataOut_V_keep_V"}]},
			{"Name" : "m_axis_ip_V_last_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "insert_ethernet_head_U0", "Port" : "dataOut_V_last_V"}]},
			{"Name" : "m_axis_arp_lookup_request_V_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "extract_ip_address_U0", "Port" : "arpTableOut_V_V"}]},
			{"Name" : "myMacAddress_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "regSubNetMask_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "regDefaultGateway_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "header_ready", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "extract_ip_address_U0", "Port" : "header_ready"}]},
			{"Name" : "header_idx_1", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "extract_ip_address_U0", "Port" : "header_idx_1"}]},
			{"Name" : "header_header_V_1", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "extract_ip_address_U0", "Port" : "header_header_V_1"}]},
			{"Name" : "dataStreamBuffer0_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "extract_ip_address_U0", "Port" : "dataStreamBuffer0_V"},
					{"ID" : "3", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "dataStreamBuffer0_V"}]},
			{"Name" : "metaWritten", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "extract_ip_address_U0", "Port" : "metaWritten"}]},
			{"Name" : "dataStreamBuffer1_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "dataStreamBuffer1_V"},
					{"ID" : "5", "SubInstance" : "insert_ip_checksum_U0", "Port" : "dataStreamBuffer1_V"}]},
			{"Name" : "cics_wordCount_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "cics_wordCount_V"}]},
			{"Name" : "cics_ip_sums_sum_V_0", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "cics_ip_sums_sum_V_0"}]},
			{"Name" : "cics_ip_sums_sum_V_1", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "cics_ip_sums_sum_V_1"}]},
			{"Name" : "cics_ip_sums_sum_V_2", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "cics_ip_sums_sum_V_2"}]},
			{"Name" : "cics_ip_sums_sum_V_3", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "cics_ip_sums_sum_V_3"}]},
			{"Name" : "cics_ipHeaderLen_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "cics_ipHeaderLen_V"}]},
			{"Name" : "subSumFifo_V_sum_V_0", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "subSumFifo_V_sum_V_0"},
					{"ID" : "4", "SubInstance" : "finalize_ipv4_checks_U0", "Port" : "subSumFifo_V_sum_V_0"}]},
			{"Name" : "subSumFifo_V_sum_V_1", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "subSumFifo_V_sum_V_1"},
					{"ID" : "4", "SubInstance" : "finalize_ipv4_checks_U0", "Port" : "subSumFifo_V_sum_V_1"}]},
			{"Name" : "subSumFifo_V_sum_V_2", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "subSumFifo_V_sum_V_2"},
					{"ID" : "4", "SubInstance" : "finalize_ipv4_checks_U0", "Port" : "subSumFifo_V_sum_V_2"}]},
			{"Name" : "subSumFifo_V_sum_V_3", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "subSumFifo_V_sum_V_3"},
					{"ID" : "4", "SubInstance" : "finalize_ipv4_checks_U0", "Port" : "subSumFifo_V_sum_V_3"}]},
			{"Name" : "checksumFifo_V_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "5", "SubInstance" : "insert_ip_checksum_U0", "Port" : "checksumFifo_V_V"},
					{"ID" : "4", "SubInstance" : "finalize_ipv4_checks_U0", "Port" : "checksumFifo_V_V"}]},
			{"Name" : "wordCount_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "5", "SubInstance" : "insert_ip_checksum_U0", "Port" : "wordCount_V"}]},
			{"Name" : "checksum_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "5", "SubInstance" : "insert_ip_checksum_U0", "Port" : "checksum_V"}]},
			{"Name" : "dataStreamBuffer2_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "6", "SubInstance" : "handle_arp_reply_64_U0", "Port" : "dataStreamBuffer2_V"},
					{"ID" : "5", "SubInstance" : "insert_ip_checksum_U0", "Port" : "dataStreamBuffer2_V"}]},
			{"Name" : "har_state", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "6", "SubInstance" : "handle_arp_reply_64_U0", "Port" : "har_state"}]},
			{"Name" : "headerFifo_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "6", "SubInstance" : "handle_arp_reply_64_U0", "Port" : "headerFifo_V"},
					{"ID" : "8", "SubInstance" : "insert_ethernet_head_U0", "Port" : "headerFifo_V"}]},
			{"Name" : "dataStreamBuffer3_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "7", "SubInstance" : "lshiftWordByOctet_U0", "Port" : "dataStreamBuffer3_V"},
					{"ID" : "6", "SubInstance" : "handle_arp_reply_64_U0", "Port" : "dataStreamBuffer3_V"}]},
			{"Name" : "ls_writeRemainder", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "7", "SubInstance" : "lshiftWordByOctet_U0", "Port" : "ls_writeRemainder"}]},
			{"Name" : "prevWord_data_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "7", "SubInstance" : "lshiftWordByOctet_U0", "Port" : "prevWord_data_V"}]},
			{"Name" : "prevWord_keep_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "7", "SubInstance" : "lshiftWordByOctet_U0", "Port" : "prevWord_keep_V"}]},
			{"Name" : "dataStreamBuffer4_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "7", "SubInstance" : "lshiftWordByOctet_U0", "Port" : "dataStreamBuffer4_V"},
					{"ID" : "8", "SubInstance" : "insert_ethernet_head_U0", "Port" : "dataStreamBuffer4_V"}]},
			{"Name" : "ls_firstWord", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "7", "SubInstance" : "lshiftWordByOctet_U0", "Port" : "ls_firstWord"}]},
			{"Name" : "ge_state", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "insert_ethernet_head_U0", "Port" : "ge_state"}]},
			{"Name" : "header_header_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "insert_ethernet_head_U0", "Port" : "header_header_V"}]},
			{"Name" : "header_idx", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "insert_ethernet_head_U0", "Port" : "header_idx"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.extract_ip_address_U0", "Parent" : "0", "Child" : ["2"],
		"CDFG" : "extract_ip_address",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "6", "EstimateLatencyMin" : "6", "EstimateLatencyMax" : "6",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "dataIn_V_data_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "s_axis_ip_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataIn_V_keep_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "dataIn_V_last_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "arpTableOut_V_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "arpTableOut_V_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regSubNetMask_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "regDefaultGateway_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "header_ready", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_idx_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_header_V_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "dataStreamBuffer0_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "3", "DependentChan" : "12",
				"BlockSignal" : [
					{"Name" : "dataStreamBuffer0_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "metaWritten", "Type" : "OVld", "Direction" : "IO"}]},
	{"ID" : "2", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.extract_ip_address_U0.regslice_both_arpTableOut_V_V_U", "Parent" : "1"},
	{"ID" : "3", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.compute_ipv4_checksu_U0", "Parent" : "0",
		"CDFG" : "compute_ipv4_checksu",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "3", "EstimateLatencyMin" : "3", "EstimateLatencyMax" : "3",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "dataStreamBuffer0_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "1", "DependentChan" : "12",
				"BlockSignal" : [
					{"Name" : "dataStreamBuffer0_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataStreamBuffer1_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "5", "DependentChan" : "13",
				"BlockSignal" : [
					{"Name" : "dataStreamBuffer1_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "cics_wordCount_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ip_sums_sum_V_0", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ip_sums_sum_V_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ip_sums_sum_V_2", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ip_sums_sum_V_3", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ipHeaderLen_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "subSumFifo_V_sum_V_0", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "4", "DependentChan" : "14",
				"BlockSignal" : [
					{"Name" : "subSumFifo_V_sum_V_0_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "subSumFifo_V_sum_V_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "4", "DependentChan" : "15",
				"BlockSignal" : [
					{"Name" : "subSumFifo_V_sum_V_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "subSumFifo_V_sum_V_2", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "4", "DependentChan" : "16",
				"BlockSignal" : [
					{"Name" : "subSumFifo_V_sum_V_2_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "subSumFifo_V_sum_V_3", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "4", "DependentChan" : "17",
				"BlockSignal" : [
					{"Name" : "subSumFifo_V_sum_V_3_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "4", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.finalize_ipv4_checks_U0", "Parent" : "0",
		"CDFG" : "finalize_ipv4_checks",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "4", "EstimateLatencyMin" : "4", "EstimateLatencyMax" : "4",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "subSumFifo_V_sum_V_0", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "3", "DependentChan" : "14",
				"BlockSignal" : [
					{"Name" : "subSumFifo_V_sum_V_0_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "subSumFifo_V_sum_V_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "3", "DependentChan" : "15",
				"BlockSignal" : [
					{"Name" : "subSumFifo_V_sum_V_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "subSumFifo_V_sum_V_2", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "3", "DependentChan" : "16",
				"BlockSignal" : [
					{"Name" : "subSumFifo_V_sum_V_2_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "subSumFifo_V_sum_V_3", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "3", "DependentChan" : "17",
				"BlockSignal" : [
					{"Name" : "subSumFifo_V_sum_V_3_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "checksumFifo_V_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "5", "DependentChan" : "18",
				"BlockSignal" : [
					{"Name" : "checksumFifo_V_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "5", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.insert_ip_checksum_U0", "Parent" : "0",
		"CDFG" : "insert_ip_checksum",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "wordCount_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "dataStreamBuffer1_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "3", "DependentChan" : "13",
				"BlockSignal" : [
					{"Name" : "dataStreamBuffer1_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "checksumFifo_V_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "4", "DependentChan" : "18",
				"BlockSignal" : [
					{"Name" : "checksumFifo_V_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "checksum_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "dataStreamBuffer2_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "6", "DependentChan" : "19",
				"BlockSignal" : [
					{"Name" : "dataStreamBuffer2_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "6", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.handle_arp_reply_64_U0", "Parent" : "0",
		"CDFG" : "handle_arp_reply_64_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "arpTableIn_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "arpTableIn_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "myMacAddress_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "har_state", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "dataStreamBuffer2_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "5", "DependentChan" : "19",
				"BlockSignal" : [
					{"Name" : "dataStreamBuffer2_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "headerFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "8", "DependentChan" : "20",
				"BlockSignal" : [
					{"Name" : "headerFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataStreamBuffer3_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "7", "DependentChan" : "21",
				"BlockSignal" : [
					{"Name" : "dataStreamBuffer3_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "7", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.lshiftWordByOctet_U0", "Parent" : "0",
		"CDFG" : "lshiftWordByOctet",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "ls_writeRemainder", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "prevWord_data_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "prevWord_keep_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "dataStreamBuffer4_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "8", "DependentChan" : "22",
				"BlockSignal" : [
					{"Name" : "dataStreamBuffer4_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataStreamBuffer3_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "6", "DependentChan" : "21",
				"BlockSignal" : [
					{"Name" : "dataStreamBuffer3_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ls_firstWord", "Type" : "OVld", "Direction" : "IO"}]},
	{"ID" : "8", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.insert_ethernet_head_U0", "Parent" : "0", "Child" : ["9", "10", "11"],
		"CDFG" : "insert_ethernet_head",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "4", "EstimateLatencyMin" : "4", "EstimateLatencyMax" : "4",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "dataOut_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_ip_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataOut_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "dataOut_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "ge_state", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "headerFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "6", "DependentChan" : "20",
				"BlockSignal" : [
					{"Name" : "headerFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "header_header_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_idx", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "dataStreamBuffer4_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "7", "DependentChan" : "22",
				"BlockSignal" : [
					{"Name" : "dataStreamBuffer4_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "9", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.insert_ethernet_head_U0.regslice_both_dataOut_V_data_V_U", "Parent" : "8"},
	{"ID" : "10", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.insert_ethernet_head_U0.regslice_both_dataOut_V_keep_V_U", "Parent" : "8"},
	{"ID" : "11", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.insert_ethernet_head_U0.regslice_both_dataOut_V_last_V_U", "Parent" : "8"},
	{"ID" : "12", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.dataStreamBuffer0_V_U", "Parent" : "0"},
	{"ID" : "13", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.dataStreamBuffer1_V_U", "Parent" : "0"},
	{"ID" : "14", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.subSumFifo_V_sum_V_0_U", "Parent" : "0"},
	{"ID" : "15", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.subSumFifo_V_sum_V_1_U", "Parent" : "0"},
	{"ID" : "16", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.subSumFifo_V_sum_V_2_U", "Parent" : "0"},
	{"ID" : "17", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.subSumFifo_V_sum_V_3_U", "Parent" : "0"},
	{"ID" : "18", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.checksumFifo_V_V_U", "Parent" : "0"},
	{"ID" : "19", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.dataStreamBuffer2_V_U", "Parent" : "0"},
	{"ID" : "20", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.headerFifo_V_U", "Parent" : "0"},
	{"ID" : "21", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.dataStreamBuffer3_V_U", "Parent" : "0"},
	{"ID" : "22", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.dataStreamBuffer4_V_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	mac_ip_encode_top {
		s_axis_ip_V_data_V {Type I LastRead 0 FirstWrite -1}
		s_axis_ip_V_keep_V {Type I LastRead 0 FirstWrite -1}
		s_axis_ip_V_last_V {Type I LastRead 0 FirstWrite -1}
		s_axis_arp_lookup_reply_V {Type I LastRead 0 FirstWrite -1}
		m_axis_ip_V_data_V {Type O LastRead -1 FirstWrite 3}
		m_axis_ip_V_keep_V {Type O LastRead -1 FirstWrite 3}
		m_axis_ip_V_last_V {Type O LastRead -1 FirstWrite 3}
		m_axis_arp_lookup_request_V_V {Type O LastRead -1 FirstWrite 5}
		myMacAddress_V {Type I LastRead 18 FirstWrite -1}
		regSubNetMask_V {Type I LastRead 0 FirstWrite -1}
		regDefaultGateway_V {Type I LastRead 0 FirstWrite -1}
		header_ready {Type IO LastRead -1 FirstWrite -1}
		header_idx_1 {Type IO LastRead -1 FirstWrite -1}
		header_header_V_1 {Type IO LastRead -1 FirstWrite -1}
		dataStreamBuffer0_V {Type IO LastRead -1 FirstWrite -1}
		metaWritten {Type IO LastRead -1 FirstWrite -1}
		dataStreamBuffer1_V {Type IO LastRead -1 FirstWrite -1}
		cics_wordCount_V {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_0 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_1 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_2 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_3 {Type IO LastRead -1 FirstWrite -1}
		cics_ipHeaderLen_V {Type IO LastRead -1 FirstWrite -1}
		subSumFifo_V_sum_V_0 {Type IO LastRead -1 FirstWrite -1}
		subSumFifo_V_sum_V_1 {Type IO LastRead -1 FirstWrite -1}
		subSumFifo_V_sum_V_2 {Type IO LastRead -1 FirstWrite -1}
		subSumFifo_V_sum_V_3 {Type IO LastRead -1 FirstWrite -1}
		checksumFifo_V_V {Type IO LastRead -1 FirstWrite -1}
		wordCount_V {Type IO LastRead -1 FirstWrite -1}
		checksum_V {Type IO LastRead -1 FirstWrite -1}
		dataStreamBuffer2_V {Type IO LastRead -1 FirstWrite -1}
		har_state {Type IO LastRead -1 FirstWrite -1}
		headerFifo_V {Type IO LastRead -1 FirstWrite -1}
		dataStreamBuffer3_V {Type IO LastRead -1 FirstWrite -1}
		ls_writeRemainder {Type IO LastRead -1 FirstWrite -1}
		prevWord_data_V {Type IO LastRead -1 FirstWrite -1}
		prevWord_keep_V {Type IO LastRead -1 FirstWrite -1}
		dataStreamBuffer4_V {Type IO LastRead -1 FirstWrite -1}
		ls_firstWord {Type IO LastRead -1 FirstWrite -1}
		ge_state {Type IO LastRead -1 FirstWrite -1}
		header_header_V {Type IO LastRead -1 FirstWrite -1}
		header_idx {Type IO LastRead -1 FirstWrite -1}}
	extract_ip_address {
		dataIn_V_data_V {Type I LastRead 0 FirstWrite -1}
		dataIn_V_keep_V {Type I LastRead 0 FirstWrite -1}
		dataIn_V_last_V {Type I LastRead 0 FirstWrite -1}
		arpTableOut_V_V {Type O LastRead -1 FirstWrite 5}
		regSubNetMask_V {Type I LastRead 0 FirstWrite -1}
		regDefaultGateway_V {Type I LastRead 0 FirstWrite -1}
		header_ready {Type IO LastRead -1 FirstWrite -1}
		header_idx_1 {Type IO LastRead -1 FirstWrite -1}
		header_header_V_1 {Type IO LastRead -1 FirstWrite -1}
		dataStreamBuffer0_V {Type O LastRead -1 FirstWrite 1}
		metaWritten {Type IO LastRead -1 FirstWrite -1}}
	compute_ipv4_checksu {
		dataStreamBuffer0_V {Type I LastRead 0 FirstWrite -1}
		dataStreamBuffer1_V {Type O LastRead -1 FirstWrite 1}
		cics_wordCount_V {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_0 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_1 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_2 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_3 {Type IO LastRead -1 FirstWrite -1}
		cics_ipHeaderLen_V {Type IO LastRead -1 FirstWrite -1}
		subSumFifo_V_sum_V_0 {Type O LastRead -1 FirstWrite 3}
		subSumFifo_V_sum_V_1 {Type O LastRead -1 FirstWrite 3}
		subSumFifo_V_sum_V_2 {Type O LastRead -1 FirstWrite 3}
		subSumFifo_V_sum_V_3 {Type O LastRead -1 FirstWrite 3}}
	finalize_ipv4_checks {
		subSumFifo_V_sum_V_0 {Type I LastRead 0 FirstWrite -1}
		subSumFifo_V_sum_V_1 {Type I LastRead 0 FirstWrite -1}
		subSumFifo_V_sum_V_2 {Type I LastRead 0 FirstWrite -1}
		subSumFifo_V_sum_V_3 {Type I LastRead 0 FirstWrite -1}
		checksumFifo_V_V {Type O LastRead -1 FirstWrite 4}}
	insert_ip_checksum {
		wordCount_V {Type IO LastRead -1 FirstWrite -1}
		dataStreamBuffer1_V {Type I LastRead 0 FirstWrite -1}
		checksumFifo_V_V {Type I LastRead 0 FirstWrite -1}
		checksum_V {Type IO LastRead -1 FirstWrite -1}
		dataStreamBuffer2_V {Type O LastRead -1 FirstWrite 1}}
	handle_arp_reply_64_s {
		arpTableIn_V {Type I LastRead 0 FirstWrite -1}
		myMacAddress_V {Type I LastRead 0 FirstWrite -1}
		har_state {Type IO LastRead -1 FirstWrite -1}
		dataStreamBuffer2_V {Type I LastRead 0 FirstWrite -1}
		headerFifo_V {Type O LastRead -1 FirstWrite 1}
		dataStreamBuffer3_V {Type O LastRead -1 FirstWrite 1}}
	lshiftWordByOctet {
		ls_writeRemainder {Type IO LastRead -1 FirstWrite -1}
		prevWord_data_V {Type IO LastRead -1 FirstWrite -1}
		prevWord_keep_V {Type IO LastRead -1 FirstWrite -1}
		dataStreamBuffer4_V {Type O LastRead -1 FirstWrite 1}
		dataStreamBuffer3_V {Type I LastRead 0 FirstWrite -1}
		ls_firstWord {Type IO LastRead -1 FirstWrite -1}}
	insert_ethernet_head {
		dataOut_V_data_V {Type O LastRead -1 FirstWrite 3}
		dataOut_V_keep_V {Type O LastRead -1 FirstWrite 3}
		dataOut_V_last_V {Type O LastRead -1 FirstWrite 3}
		ge_state {Type IO LastRead -1 FirstWrite -1}
		headerFifo_V {Type I LastRead 1 FirstWrite -1}
		header_header_V {Type IO LastRead -1 FirstWrite -1}
		header_idx {Type IO LastRead -1 FirstWrite -1}
		dataStreamBuffer4_V {Type I LastRead 0 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "21", "Max" : "21"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
]}

set Spec2ImplPortList { 
	s_axis_ip_V_data_V { axis {  { s_axis_ip_TDATA in_data 0 64 } } }
	s_axis_ip_V_keep_V { axis {  { s_axis_ip_TKEEP in_data 0 8 } } }
	s_axis_ip_V_last_V { axis {  { s_axis_ip_TLAST in_data 0 1 }  { s_axis_ip_TVALID in_vld 0 1 }  { s_axis_ip_TREADY in_acc 1 1 } } }
	s_axis_arp_lookup_reply_V { axis {  { s_axis_arp_lookup_reply_V_TDATA in_data 0 56 }  { s_axis_arp_lookup_reply_V_TVALID in_vld 0 1 }  { s_axis_arp_lookup_reply_V_TREADY in_acc 1 1 } } }
	m_axis_ip_V_data_V { axis {  { m_axis_ip_TDATA out_data 1 64 } } }
	m_axis_ip_V_keep_V { axis {  { m_axis_ip_TKEEP out_data 1 8 } } }
	m_axis_ip_V_last_V { axis {  { m_axis_ip_TLAST out_data 1 1 }  { m_axis_ip_TVALID out_vld 1 1 }  { m_axis_ip_TREADY out_acc 0 1 } } }
	m_axis_arp_lookup_request_V_V { axis {  { m_axis_arp_lookup_request_V_V_TDATA out_data 1 32 }  { m_axis_arp_lookup_request_V_V_TVALID out_vld 1 1 }  { m_axis_arp_lookup_request_V_V_TREADY out_acc 0 1 } } }
	myMacAddress_V { ap_stable {  { myMacAddress_V in_data 0 48 } } }
	regSubNetMask_V { ap_stable {  { regSubNetMask_V in_data 0 32 } } }
	regDefaultGateway_V { ap_stable {  { regDefaultGateway_V in_data 0 32 } } }
}

set busDeadlockParameterList { 
}

# RTL port scheduling information:
set fifoSchedulingInfoList { 
}

# RTL bus port read request latency information:
set busReadReqLatencyList { 
}

# RTL bus port write response latency information:
set busWriteResLatencyList { 
}

# RTL array port load latency information:
set memoryLoadLatencyList { 
}
set moduleName mac_ip_encode_top
set isTopModule 1
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type dataflow
set FunctionProtocol ap_ctrl_none
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {mac_ip_encode_top}
set C_modelType { void 0 }
set C_modelArgList {
	{ s_axis_ip_V_data_V int 64 regular {axi_s 0 volatile  { s_axis_ip Data } }  }
	{ s_axis_ip_V_keep_V int 8 regular {axi_s 0 volatile  { s_axis_ip Keep } }  }
	{ s_axis_ip_V_last_V int 1 regular {axi_s 0 volatile  { s_axis_ip Last } }  }
	{ s_axis_arp_lookup_reply_V int 56 regular {axi_s 0 volatile  { s_axis_arp_lookup_reply_V Data } }  }
	{ m_axis_ip_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_ip Data } }  }
	{ m_axis_ip_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_ip Keep } }  }
	{ m_axis_ip_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_ip Last } }  }
	{ m_axis_arp_lookup_request_V_V int 32 regular {axi_s 1 volatile  { m_axis_arp_lookup_request_V_V Data } }  }
	{ myMacAddress_V int 48 regular {axi_slave 0}  }
	{ regSubNetMask_V int 32 regular {axi_slave 0}  }
	{ regDefaultGateway_V int 32 regular {axi_slave 0}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "s_axis_ip_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "READONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "s_axis_ip.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_ip_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "s_axis_ip.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_ip_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "s_axis_ip.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_arp_lookup_reply_V", "interface" : "axis", "bitwidth" : 56, "direction" : "READONLY", "bitSlice":[{"low":0,"up":47,"cElement": [{"cName": "s_axis_arp_lookup_reply.V.macAddress.V","cData": "uint48","bit_use": { "low": 0,"up": 47},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":48,"up":48,"cElement": [{"cName": "s_axis_arp_lookup_reply.V.hit","cData": "bool","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_ip_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "m_axis_ip.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_ip_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "m_axis_ip.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_ip_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "m_axis_ip.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_arp_lookup_request_V_V", "interface" : "axis", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "m_axis_arp_lookup_request.V.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "myMacAddress_V", "interface" : "axi_slave", "bundle":"AXILiteS","type":"ap_none","bitwidth" : 48, "direction" : "READONLY", "bitSlice":[{"low":0,"up":47,"cElement": [{"cName": "myMacAddress.V","cData": "uint48","bit_use": { "low": 0,"up": 47},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}], "offset" : {"in":16}, "offset_end" : {"in":27}} , 
 	{ "Name" : "regSubNetMask_V", "interface" : "axi_slave", "bundle":"AXILiteS","type":"ap_none","bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "regSubNetMask.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}], "offset" : {"in":28}, "offset_end" : {"in":35}} , 
 	{ "Name" : "regDefaultGateway_V", "interface" : "axi_slave", "bundle":"AXILiteS","type":"ap_none","bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "regDefaultGateway.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}], "offset" : {"in":36}, "offset_end" : {"in":43}} ]}
# RTL Port declarations: 
set portNum 35
set portList { 
	{ s_axi_AXILiteS_AWVALID sc_in sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_AWREADY sc_out sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_AWADDR sc_in sc_lv 6 signal -1 } 
	{ s_axi_AXILiteS_WVALID sc_in sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_WREADY sc_out sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_WDATA sc_in sc_lv 32 signal -1 } 
	{ s_axi_AXILiteS_WSTRB sc_in sc_lv 4 signal -1 } 
	{ s_axi_AXILiteS_ARVALID sc_in sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_ARREADY sc_out sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_ARADDR sc_in sc_lv 6 signal -1 } 
	{ s_axi_AXILiteS_RVALID sc_out sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_RREADY sc_in sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_RDATA sc_out sc_lv 32 signal -1 } 
	{ s_axi_AXILiteS_RRESP sc_out sc_lv 2 signal -1 } 
	{ s_axi_AXILiteS_BVALID sc_out sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_BREADY sc_in sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_BRESP sc_out sc_lv 2 signal -1 } 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst_n sc_in sc_logic 1 reset -1 active_low_sync } 
	{ s_axis_ip_TDATA sc_in sc_lv 64 signal 0 } 
	{ s_axis_ip_TKEEP sc_in sc_lv 8 signal 1 } 
	{ s_axis_ip_TLAST sc_in sc_lv 1 signal 2 } 
	{ s_axis_arp_lookup_reply_V_TDATA sc_in sc_lv 56 signal 3 } 
	{ m_axis_ip_TDATA sc_out sc_lv 64 signal 4 } 
	{ m_axis_ip_TKEEP sc_out sc_lv 8 signal 5 } 
	{ m_axis_ip_TLAST sc_out sc_lv 1 signal 6 } 
	{ m_axis_arp_lookup_request_V_V_TDATA sc_out sc_lv 32 signal 7 } 
	{ s_axis_ip_TVALID sc_in sc_logic 1 invld 2 } 
	{ s_axis_ip_TREADY sc_out sc_logic 1 inacc 2 } 
	{ m_axis_arp_lookup_request_V_V_TVALID sc_out sc_logic 1 outvld 7 } 
	{ m_axis_arp_lookup_request_V_V_TREADY sc_in sc_logic 1 outacc 7 } 
	{ s_axis_arp_lookup_reply_V_TVALID sc_in sc_logic 1 invld 3 } 
	{ s_axis_arp_lookup_reply_V_TREADY sc_out sc_logic 1 inacc 3 } 
	{ m_axis_ip_TVALID sc_out sc_logic 1 outvld 6 } 
	{ m_axis_ip_TREADY sc_in sc_logic 1 outacc 6 } 
}
set NewPortList {[ 
	{ "name": "s_axi_AXILiteS_AWADDR", "direction": "in", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "AXILiteS", "role": "AWADDR" },"address":[{"name":"myMacAddress_V","role":"data","value":"16"},{"name":"regSubNetMask_V","role":"data","value":"28"},{"name":"regDefaultGateway_V","role":"data","value":"36"}] },
	{ "name": "s_axi_AXILiteS_AWVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "AWVALID" } },
	{ "name": "s_axi_AXILiteS_AWREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "AWREADY" } },
	{ "name": "s_axi_AXILiteS_WVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "WVALID" } },
	{ "name": "s_axi_AXILiteS_WREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "WREADY" } },
	{ "name": "s_axi_AXILiteS_WDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "AXILiteS", "role": "WDATA" } },
	{ "name": "s_axi_AXILiteS_WSTRB", "direction": "in", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "AXILiteS", "role": "WSTRB" } },
	{ "name": "s_axi_AXILiteS_ARADDR", "direction": "in", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "AXILiteS", "role": "ARADDR" },"address":[] },
	{ "name": "s_axi_AXILiteS_ARVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "ARVALID" } },
	{ "name": "s_axi_AXILiteS_ARREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "ARREADY" } },
	{ "name": "s_axi_AXILiteS_RVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "RVALID" } },
	{ "name": "s_axi_AXILiteS_RREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "RREADY" } },
	{ "name": "s_axi_AXILiteS_RDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "AXILiteS", "role": "RDATA" } },
	{ "name": "s_axi_AXILiteS_RRESP", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "AXILiteS", "role": "RRESP" } },
	{ "name": "s_axi_AXILiteS_BVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "BVALID" } },
	{ "name": "s_axi_AXILiteS_BREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "BREADY" } },
	{ "name": "s_axi_AXILiteS_BRESP", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "AXILiteS", "role": "BRESP" } }, 
 	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst_n", "role": "default" }} , 
 	{ "name": "s_axis_ip_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "s_axis_ip_V_data_V", "role": "default" }} , 
 	{ "name": "s_axis_ip_TKEEP", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "s_axis_ip_V_keep_V", "role": "default" }} , 
 	{ "name": "s_axis_ip_TLAST", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "s_axis_ip_V_last_V", "role": "default" }} , 
 	{ "name": "s_axis_arp_lookup_reply_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":56, "type": "signal", "bundle":{"name": "s_axis_arp_lookup_reply_V", "role": "TDATA" }} , 
 	{ "name": "m_axis_ip_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "m_axis_ip_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_ip_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "m_axis_ip_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_ip_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "m_axis_ip_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_arp_lookup_request_V_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "m_axis_arp_lookup_request_V_V", "role": "TDATA" }} , 
 	{ "name": "s_axis_ip_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "s_axis_ip_V_last_V", "role": "default" }} , 
 	{ "name": "s_axis_ip_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "s_axis_ip_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_arp_lookup_request_V_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_arp_lookup_request_V_V", "role": "TVALID" }} , 
 	{ "name": "m_axis_arp_lookup_request_V_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_arp_lookup_request_V_V", "role": "TREADY" }} , 
 	{ "name": "s_axis_arp_lookup_reply_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "s_axis_arp_lookup_reply_V", "role": "TVALID" }} , 
 	{ "name": "s_axis_arp_lookup_reply_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "s_axis_arp_lookup_reply_V", "role": "TREADY" }} , 
 	{ "name": "m_axis_ip_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_ip_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_ip_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_ip_V_last_V", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24"],
		"CDFG" : "mac_ip_encode_top",
		"Protocol" : "ap_ctrl_none",
		"ControlExist" : "0", "ap_start" : "0", "ap_ready" : "0", "ap_done" : "0", "ap_continue" : "0", "ap_idle" : "0",
		"Pipeline" : "Dataflow", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "1",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "20", "EstimateLatencyMax" : "20",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "1",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"InputProcess" : [
			{"ID" : "2", "Name" : "mac_ip_encode_top_en_U0"},
			{"ID" : "3", "Name" : "extract_ip_address25_U0"},
			{"ID" : "7", "Name" : "handle_arp_reply_64_U0"}],
		"OutputProcess" : [
			{"ID" : "3", "Name" : "extract_ip_address25_U0"},
			{"ID" : "9", "Name" : "insert_ethernet_head_U0"}],
		"Port" : [
			{"Name" : "s_axis_ip_V_data_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "extract_ip_address25_U0", "Port" : "dataIn_V_data_V"}]},
			{"Name" : "s_axis_ip_V_keep_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "extract_ip_address25_U0", "Port" : "dataIn_V_keep_V"}]},
			{"Name" : "s_axis_ip_V_last_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "extract_ip_address25_U0", "Port" : "dataIn_V_last_V"}]},
			{"Name" : "s_axis_arp_lookup_reply_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "7", "SubInstance" : "handle_arp_reply_64_U0", "Port" : "arpTableIn_V"}]},
			{"Name" : "m_axis_ip_V_data_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "9", "SubInstance" : "insert_ethernet_head_U0", "Port" : "dataOut_V_data_V"}]},
			{"Name" : "m_axis_ip_V_keep_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "9", "SubInstance" : "insert_ethernet_head_U0", "Port" : "dataOut_V_keep_V"}]},
			{"Name" : "m_axis_ip_V_last_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "9", "SubInstance" : "insert_ethernet_head_U0", "Port" : "dataOut_V_last_V"}]},
			{"Name" : "m_axis_arp_lookup_request_V_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "extract_ip_address25_U0", "Port" : "arpTableOut_V_V"}]},
			{"Name" : "myMacAddress_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "regSubNetMask_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "regDefaultGateway_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "header_ready", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "extract_ip_address25_U0", "Port" : "header_ready"}]},
			{"Name" : "header_idx_1", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "extract_ip_address25_U0", "Port" : "header_idx_1"}]},
			{"Name" : "header_header_V_1", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "extract_ip_address25_U0", "Port" : "header_header_V_1"}]},
			{"Name" : "dataStreamBuffer0_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "dataStreamBuffer0_V"},
					{"ID" : "3", "SubInstance" : "extract_ip_address25_U0", "Port" : "dataStreamBuffer0_V"}]},
			{"Name" : "metaWritten", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "extract_ip_address25_U0", "Port" : "metaWritten"}]},
			{"Name" : "dataStreamBuffer1_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "6", "SubInstance" : "insert_ip_checksum_U0", "Port" : "dataStreamBuffer1_V"},
					{"ID" : "4", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "dataStreamBuffer1_V"}]},
			{"Name" : "cics_wordCount_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "cics_wordCount_V"}]},
			{"Name" : "cics_ip_sums_sum_V_0", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "cics_ip_sums_sum_V_0"}]},
			{"Name" : "cics_ip_sums_sum_V_1", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "cics_ip_sums_sum_V_1"}]},
			{"Name" : "cics_ip_sums_sum_V_2", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "cics_ip_sums_sum_V_2"}]},
			{"Name" : "cics_ip_sums_sum_V_3", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "cics_ip_sums_sum_V_3"}]},
			{"Name" : "cics_ipHeaderLen_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "cics_ipHeaderLen_V"}]},
			{"Name" : "subSumFifo_V_sum_V_0", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "subSumFifo_V_sum_V_0"},
					{"ID" : "5", "SubInstance" : "finalize_ipv4_checks_U0", "Port" : "subSumFifo_V_sum_V_0"}]},
			{"Name" : "subSumFifo_V_sum_V_1", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "subSumFifo_V_sum_V_1"},
					{"ID" : "5", "SubInstance" : "finalize_ipv4_checks_U0", "Port" : "subSumFifo_V_sum_V_1"}]},
			{"Name" : "subSumFifo_V_sum_V_2", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "subSumFifo_V_sum_V_2"},
					{"ID" : "5", "SubInstance" : "finalize_ipv4_checks_U0", "Port" : "subSumFifo_V_sum_V_2"}]},
			{"Name" : "subSumFifo_V_sum_V_3", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "subSumFifo_V_sum_V_3"},
					{"ID" : "5", "SubInstance" : "finalize_ipv4_checks_U0", "Port" : "subSumFifo_V_sum_V_3"}]},
			{"Name" : "checksumFifo_V_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "6", "SubInstance" : "insert_ip_checksum_U0", "Port" : "checksumFifo_V_V"},
					{"ID" : "5", "SubInstance" : "finalize_ipv4_checks_U0", "Port" : "checksumFifo_V_V"}]},
			{"Name" : "wordCount_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "6", "SubInstance" : "insert_ip_checksum_U0", "Port" : "wordCount_V"}]},
			{"Name" : "checksum_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "6", "SubInstance" : "insert_ip_checksum_U0", "Port" : "checksum_V"}]},
			{"Name" : "dataStreamBuffer2_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "7", "SubInstance" : "handle_arp_reply_64_U0", "Port" : "dataStreamBuffer2_V"},
					{"ID" : "6", "SubInstance" : "insert_ip_checksum_U0", "Port" : "dataStreamBuffer2_V"}]},
			{"Name" : "har_state", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "7", "SubInstance" : "handle_arp_reply_64_U0", "Port" : "har_state"}]},
			{"Name" : "headerFifo_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "7", "SubInstance" : "handle_arp_reply_64_U0", "Port" : "headerFifo_V"},
					{"ID" : "9", "SubInstance" : "insert_ethernet_head_U0", "Port" : "headerFifo_V"}]},
			{"Name" : "dataStreamBuffer3_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "7", "SubInstance" : "handle_arp_reply_64_U0", "Port" : "dataStreamBuffer3_V"},
					{"ID" : "8", "SubInstance" : "lshiftWordByOctet_U0", "Port" : "dataStreamBuffer3_V"}]},
			{"Name" : "ls_writeRemainder", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "lshiftWordByOctet_U0", "Port" : "ls_writeRemainder"}]},
			{"Name" : "prevWord_data_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "lshiftWordByOctet_U0", "Port" : "prevWord_data_V"}]},
			{"Name" : "prevWord_keep_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "lshiftWordByOctet_U0", "Port" : "prevWord_keep_V"}]},
			{"Name" : "dataStreamBuffer4_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "9", "SubInstance" : "insert_ethernet_head_U0", "Port" : "dataStreamBuffer4_V"},
					{"ID" : "8", "SubInstance" : "lshiftWordByOctet_U0", "Port" : "dataStreamBuffer4_V"}]},
			{"Name" : "ls_firstWord", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "lshiftWordByOctet_U0", "Port" : "ls_firstWord"}]},
			{"Name" : "ge_state", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "9", "SubInstance" : "insert_ethernet_head_U0", "Port" : "ge_state"}]},
			{"Name" : "header_header_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "9", "SubInstance" : "insert_ethernet_head_U0", "Port" : "header_header_V"}]},
			{"Name" : "header_idx", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "9", "SubInstance" : "insert_ethernet_head_U0", "Port" : "header_idx"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.mac_ip_encode_top_AXILiteS_s_axi_U", "Parent" : "0"},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.mac_ip_encode_top_en_U0", "Parent" : "0",
		"CDFG" : "mac_ip_encode_top_en",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "0", "EstimateLatencyMin" : "0", "EstimateLatencyMax" : "0",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "0",
		"Port" : [
			{"Name" : "myMacAddress_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "regSubNetMask_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "regDefaultGateway_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "myMacAddress_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "3", "DependentChan" : "10",
				"BlockSignal" : [
					{"Name" : "myMacAddress_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regSubNetMask_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "3", "DependentChan" : "11",
				"BlockSignal" : [
					{"Name" : "regSubNetMask_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regDefaultGateway_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "3", "DependentChan" : "12",
				"BlockSignal" : [
					{"Name" : "regDefaultGateway_V_out_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "3", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.extract_ip_address25_U0", "Parent" : "0",
		"CDFG" : "extract_ip_address25",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "5", "EstimateLatencyMin" : "5", "EstimateLatencyMax" : "5",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "dataIn_V_data_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "s_axis_ip_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataIn_V_keep_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "dataIn_V_last_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "arpTableOut_V_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "arpTableOut_V_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regSubNetMask_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "11",
				"BlockSignal" : [
					{"Name" : "regSubNetMask_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regDefaultGateway_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "12",
				"BlockSignal" : [
					{"Name" : "regDefaultGateway_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "myMacAddress_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "10",
				"BlockSignal" : [
					{"Name" : "myMacAddress_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "myMacAddress_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "7", "DependentChan" : "13",
				"BlockSignal" : [
					{"Name" : "myMacAddress_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "header_ready", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_idx_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_header_V_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "dataStreamBuffer0_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "4", "DependentChan" : "14",
				"BlockSignal" : [
					{"Name" : "dataStreamBuffer0_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "metaWritten", "Type" : "OVld", "Direction" : "IO"}]},
	{"ID" : "4", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.compute_ipv4_checksu_U0", "Parent" : "0",
		"CDFG" : "compute_ipv4_checksu",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "dataStreamBuffer0_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "3", "DependentChan" : "14",
				"BlockSignal" : [
					{"Name" : "dataStreamBuffer0_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataStreamBuffer1_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "6", "DependentChan" : "15",
				"BlockSignal" : [
					{"Name" : "dataStreamBuffer1_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "cics_wordCount_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ip_sums_sum_V_0", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ip_sums_sum_V_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ip_sums_sum_V_2", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ip_sums_sum_V_3", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ipHeaderLen_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "subSumFifo_V_sum_V_0", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "5", "DependentChan" : "16",
				"BlockSignal" : [
					{"Name" : "subSumFifo_V_sum_V_0_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "subSumFifo_V_sum_V_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "5", "DependentChan" : "17",
				"BlockSignal" : [
					{"Name" : "subSumFifo_V_sum_V_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "subSumFifo_V_sum_V_2", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "5", "DependentChan" : "18",
				"BlockSignal" : [
					{"Name" : "subSumFifo_V_sum_V_2_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "subSumFifo_V_sum_V_3", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "5", "DependentChan" : "19",
				"BlockSignal" : [
					{"Name" : "subSumFifo_V_sum_V_3_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "5", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.finalize_ipv4_checks_U0", "Parent" : "0",
		"CDFG" : "finalize_ipv4_checks",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "4", "EstimateLatencyMin" : "4", "EstimateLatencyMax" : "4",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "subSumFifo_V_sum_V_0", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "4", "DependentChan" : "16",
				"BlockSignal" : [
					{"Name" : "subSumFifo_V_sum_V_0_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "subSumFifo_V_sum_V_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "4", "DependentChan" : "17",
				"BlockSignal" : [
					{"Name" : "subSumFifo_V_sum_V_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "subSumFifo_V_sum_V_2", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "4", "DependentChan" : "18",
				"BlockSignal" : [
					{"Name" : "subSumFifo_V_sum_V_2_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "subSumFifo_V_sum_V_3", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "4", "DependentChan" : "19",
				"BlockSignal" : [
					{"Name" : "subSumFifo_V_sum_V_3_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "checksumFifo_V_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "6", "DependentChan" : "20",
				"BlockSignal" : [
					{"Name" : "checksumFifo_V_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "6", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.insert_ip_checksum_U0", "Parent" : "0",
		"CDFG" : "insert_ip_checksum",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "wordCount_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "dataStreamBuffer1_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "4", "DependentChan" : "15",
				"BlockSignal" : [
					{"Name" : "dataStreamBuffer1_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "checksumFifo_V_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "5", "DependentChan" : "20",
				"BlockSignal" : [
					{"Name" : "checksumFifo_V_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "checksum_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "dataStreamBuffer2_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "7", "DependentChan" : "21",
				"BlockSignal" : [
					{"Name" : "dataStreamBuffer2_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "7", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.handle_arp_reply_64_U0", "Parent" : "0",
		"CDFG" : "handle_arp_reply_64_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "arpTableIn_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "arpTableIn_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "myMacAddress_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "3", "DependentChan" : "13",
				"BlockSignal" : [
					{"Name" : "myMacAddress_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "har_state", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "dataStreamBuffer2_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "6", "DependentChan" : "21",
				"BlockSignal" : [
					{"Name" : "dataStreamBuffer2_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "headerFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "9", "DependentChan" : "22",
				"BlockSignal" : [
					{"Name" : "headerFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataStreamBuffer3_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "8", "DependentChan" : "23",
				"BlockSignal" : [
					{"Name" : "dataStreamBuffer3_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "8", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.lshiftWordByOctet_U0", "Parent" : "0",
		"CDFG" : "lshiftWordByOctet",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "ls_writeRemainder", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "prevWord_data_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "prevWord_keep_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "dataStreamBuffer4_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "9", "DependentChan" : "24",
				"BlockSignal" : [
					{"Name" : "dataStreamBuffer4_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataStreamBuffer3_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "7", "DependentChan" : "23",
				"BlockSignal" : [
					{"Name" : "dataStreamBuffer3_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ls_firstWord", "Type" : "OVld", "Direction" : "IO"}]},
	{"ID" : "9", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.insert_ethernet_head_U0", "Parent" : "0",
		"CDFG" : "insert_ethernet_head",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "4", "EstimateLatencyMin" : "4", "EstimateLatencyMax" : "4",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "dataOut_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_ip_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataOut_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "dataOut_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "ge_state", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "headerFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "7", "DependentChan" : "22",
				"BlockSignal" : [
					{"Name" : "headerFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "header_header_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_idx", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "dataStreamBuffer4_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "8", "DependentChan" : "24",
				"BlockSignal" : [
					{"Name" : "dataStreamBuffer4_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "10", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.myMacAddress_V_c1_U", "Parent" : "0"},
	{"ID" : "11", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regSubNetMask_V_c_U", "Parent" : "0"},
	{"ID" : "12", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regDefaultGateway_V_s_28_U", "Parent" : "0"},
	{"ID" : "13", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.myMacAddress_V_c_U", "Parent" : "0"},
	{"ID" : "14", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.dataStreamBuffer0_V_U", "Parent" : "0"},
	{"ID" : "15", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.dataStreamBuffer1_V_U", "Parent" : "0"},
	{"ID" : "16", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.subSumFifo_V_sum_V_0_U", "Parent" : "0"},
	{"ID" : "17", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.subSumFifo_V_sum_V_1_U", "Parent" : "0"},
	{"ID" : "18", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.subSumFifo_V_sum_V_2_U", "Parent" : "0"},
	{"ID" : "19", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.subSumFifo_V_sum_V_3_U", "Parent" : "0"},
	{"ID" : "20", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.checksumFifo_V_V_U", "Parent" : "0"},
	{"ID" : "21", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.dataStreamBuffer2_V_U", "Parent" : "0"},
	{"ID" : "22", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.headerFifo_V_U", "Parent" : "0"},
	{"ID" : "23", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.dataStreamBuffer3_V_U", "Parent" : "0"},
	{"ID" : "24", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.dataStreamBuffer4_V_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	mac_ip_encode_top {
		s_axis_ip_V_data_V {Type I LastRead 0 FirstWrite -1}
		s_axis_ip_V_keep_V {Type I LastRead 0 FirstWrite -1}
		s_axis_ip_V_last_V {Type I LastRead 0 FirstWrite -1}
		s_axis_arp_lookup_reply_V {Type I LastRead 0 FirstWrite -1}
		m_axis_ip_V_data_V {Type O LastRead -1 FirstWrite 3}
		m_axis_ip_V_keep_V {Type O LastRead -1 FirstWrite 3}
		m_axis_ip_V_last_V {Type O LastRead -1 FirstWrite 3}
		m_axis_arp_lookup_request_V_V {Type O LastRead -1 FirstWrite 4}
		myMacAddress_V {Type I LastRead 0 FirstWrite -1}
		regSubNetMask_V {Type I LastRead 0 FirstWrite -1}
		regDefaultGateway_V {Type I LastRead 0 FirstWrite -1}
		header_ready {Type IO LastRead -1 FirstWrite -1}
		header_idx_1 {Type IO LastRead -1 FirstWrite -1}
		header_header_V_1 {Type IO LastRead -1 FirstWrite -1}
		dataStreamBuffer0_V {Type IO LastRead -1 FirstWrite -1}
		metaWritten {Type IO LastRead -1 FirstWrite -1}
		dataStreamBuffer1_V {Type IO LastRead -1 FirstWrite -1}
		cics_wordCount_V {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_0 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_1 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_2 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_3 {Type IO LastRead -1 FirstWrite -1}
		cics_ipHeaderLen_V {Type IO LastRead -1 FirstWrite -1}
		subSumFifo_V_sum_V_0 {Type IO LastRead -1 FirstWrite -1}
		subSumFifo_V_sum_V_1 {Type IO LastRead -1 FirstWrite -1}
		subSumFifo_V_sum_V_2 {Type IO LastRead -1 FirstWrite -1}
		subSumFifo_V_sum_V_3 {Type IO LastRead -1 FirstWrite -1}
		checksumFifo_V_V {Type IO LastRead -1 FirstWrite -1}
		wordCount_V {Type IO LastRead -1 FirstWrite -1}
		checksum_V {Type IO LastRead -1 FirstWrite -1}
		dataStreamBuffer2_V {Type IO LastRead -1 FirstWrite -1}
		har_state {Type IO LastRead -1 FirstWrite -1}
		headerFifo_V {Type IO LastRead -1 FirstWrite -1}
		dataStreamBuffer3_V {Type IO LastRead -1 FirstWrite -1}
		ls_writeRemainder {Type IO LastRead -1 FirstWrite -1}
		prevWord_data_V {Type IO LastRead -1 FirstWrite -1}
		prevWord_keep_V {Type IO LastRead -1 FirstWrite -1}
		dataStreamBuffer4_V {Type IO LastRead -1 FirstWrite -1}
		ls_firstWord {Type IO LastRead -1 FirstWrite -1}
		ge_state {Type IO LastRead -1 FirstWrite -1}
		header_header_V {Type IO LastRead -1 FirstWrite -1}
		header_idx {Type IO LastRead -1 FirstWrite -1}}
	mac_ip_encode_top_en {
		myMacAddress_V {Type I LastRead 0 FirstWrite -1}
		regSubNetMask_V {Type I LastRead 0 FirstWrite -1}
		regDefaultGateway_V {Type I LastRead 0 FirstWrite -1}
		myMacAddress_V_out {Type O LastRead -1 FirstWrite 0}
		regSubNetMask_V_out {Type O LastRead -1 FirstWrite 0}
		regDefaultGateway_V_out {Type O LastRead -1 FirstWrite 0}}
	extract_ip_address25 {
		dataIn_V_data_V {Type I LastRead 0 FirstWrite -1}
		dataIn_V_keep_V {Type I LastRead 0 FirstWrite -1}
		dataIn_V_last_V {Type I LastRead 0 FirstWrite -1}
		arpTableOut_V_V {Type O LastRead -1 FirstWrite 4}
		regSubNetMask_V {Type I LastRead 3 FirstWrite -1}
		regDefaultGateway_V {Type I LastRead 3 FirstWrite -1}
		myMacAddress_V {Type I LastRead 3 FirstWrite -1}
		myMacAddress_V_out {Type O LastRead -1 FirstWrite 3}
		header_ready {Type IO LastRead -1 FirstWrite -1}
		header_idx_1 {Type IO LastRead -1 FirstWrite -1}
		header_header_V_1 {Type IO LastRead -1 FirstWrite -1}
		dataStreamBuffer0_V {Type O LastRead -1 FirstWrite 1}
		metaWritten {Type IO LastRead -1 FirstWrite -1}}
	compute_ipv4_checksu {
		dataStreamBuffer0_V {Type I LastRead 0 FirstWrite -1}
		dataStreamBuffer1_V {Type O LastRead -1 FirstWrite 1}
		cics_wordCount_V {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_0 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_1 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_2 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_3 {Type IO LastRead -1 FirstWrite -1}
		cics_ipHeaderLen_V {Type IO LastRead -1 FirstWrite -1}
		subSumFifo_V_sum_V_0 {Type O LastRead -1 FirstWrite 2}
		subSumFifo_V_sum_V_1 {Type O LastRead -1 FirstWrite 2}
		subSumFifo_V_sum_V_2 {Type O LastRead -1 FirstWrite 2}
		subSumFifo_V_sum_V_3 {Type O LastRead -1 FirstWrite 2}}
	finalize_ipv4_checks {
		subSumFifo_V_sum_V_0 {Type I LastRead 0 FirstWrite -1}
		subSumFifo_V_sum_V_1 {Type I LastRead 0 FirstWrite -1}
		subSumFifo_V_sum_V_2 {Type I LastRead 0 FirstWrite -1}
		subSumFifo_V_sum_V_3 {Type I LastRead 0 FirstWrite -1}
		checksumFifo_V_V {Type O LastRead -1 FirstWrite 4}}
	insert_ip_checksum {
		wordCount_V {Type IO LastRead -1 FirstWrite -1}
		dataStreamBuffer1_V {Type I LastRead 0 FirstWrite -1}
		checksumFifo_V_V {Type I LastRead 0 FirstWrite -1}
		checksum_V {Type IO LastRead -1 FirstWrite -1}
		dataStreamBuffer2_V {Type O LastRead -1 FirstWrite 1}}
	handle_arp_reply_64_s {
		arpTableIn_V {Type I LastRead 0 FirstWrite -1}
		myMacAddress_V {Type I LastRead 1 FirstWrite -1}
		har_state {Type IO LastRead -1 FirstWrite -1}
		dataStreamBuffer2_V {Type I LastRead 0 FirstWrite -1}
		headerFifo_V {Type O LastRead -1 FirstWrite 1}
		dataStreamBuffer3_V {Type O LastRead -1 FirstWrite 1}}
	lshiftWordByOctet {
		ls_writeRemainder {Type IO LastRead -1 FirstWrite -1}
		prevWord_data_V {Type IO LastRead -1 FirstWrite -1}
		prevWord_keep_V {Type IO LastRead -1 FirstWrite -1}
		dataStreamBuffer4_V {Type O LastRead -1 FirstWrite 1}
		dataStreamBuffer3_V {Type I LastRead 0 FirstWrite -1}
		ls_firstWord {Type IO LastRead -1 FirstWrite -1}}
	insert_ethernet_head {
		dataOut_V_data_V {Type O LastRead -1 FirstWrite 3}
		dataOut_V_keep_V {Type O LastRead -1 FirstWrite 3}
		dataOut_V_last_V {Type O LastRead -1 FirstWrite 3}
		ge_state {Type IO LastRead -1 FirstWrite -1}
		headerFifo_V {Type I LastRead 1 FirstWrite -1}
		header_header_V {Type IO LastRead -1 FirstWrite -1}
		header_idx {Type IO LastRead -1 FirstWrite -1}
		dataStreamBuffer4_V {Type I LastRead 0 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "20", "Max" : "20"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
]}

set Spec2ImplPortList { 
	s_axis_ip_V_data_V { axis {  { s_axis_ip_TDATA in_data 0 64 } } }
	s_axis_ip_V_keep_V { axis {  { s_axis_ip_TKEEP in_data 0 8 } } }
	s_axis_ip_V_last_V { axis {  { s_axis_ip_TLAST in_data 0 1 }  { s_axis_ip_TVALID in_vld 0 1 }  { s_axis_ip_TREADY in_acc 1 1 } } }
	s_axis_arp_lookup_reply_V { axis {  { s_axis_arp_lookup_reply_V_TDATA in_data 0 56 }  { s_axis_arp_lookup_reply_V_TVALID in_vld 0 1 }  { s_axis_arp_lookup_reply_V_TREADY in_acc 1 1 } } }
	m_axis_ip_V_data_V { axis {  { m_axis_ip_TDATA out_data 1 64 } } }
	m_axis_ip_V_keep_V { axis {  { m_axis_ip_TKEEP out_data 1 8 } } }
	m_axis_ip_V_last_V { axis {  { m_axis_ip_TLAST out_data 1 1 }  { m_axis_ip_TVALID out_vld 1 1 }  { m_axis_ip_TREADY out_acc 0 1 } } }
	m_axis_arp_lookup_request_V_V { axis {  { m_axis_arp_lookup_request_V_V_TDATA out_data 1 32 }  { m_axis_arp_lookup_request_V_V_TVALID out_vld 1 1 }  { m_axis_arp_lookup_request_V_V_TREADY out_acc 0 1 } } }
}

set busDeadlockParameterList { 
}

# RTL port scheduling information:
set fifoSchedulingInfoList { 
}

# RTL bus port read request latency information:
set busReadReqLatencyList { 
}

# RTL bus port write response latency information:
set busWriteResLatencyList { 
}

# RTL array port load latency information:
set memoryLoadLatencyList { 
}
