set clock_constraint { \
    name clk \
    module mac_ip_encode_top \
    port ap_clk \
    period 6.4 \
    uncertainty 0.8 \
}

set all_path {}

set false_path {}

