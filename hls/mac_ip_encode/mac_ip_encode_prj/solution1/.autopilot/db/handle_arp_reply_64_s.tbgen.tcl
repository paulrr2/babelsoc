set moduleName handle_arp_reply_64_s
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {handle_arp_reply<64>}
set C_modelType { void 0 }
set C_modelArgList {
	{ arpTableIn_V int 56 regular {axi_s 0 volatile  { arpTableIn_V Data } }  }
	{ myMacAddress_V int 48 regular {ap_stable 0} }
	{ dataStreamBuffer2_V int 73 regular {fifo 0 volatile } {global 0}  }
	{ headerFifo_V int 129 regular {fifo 1 volatile } {global 1}  }
	{ dataStreamBuffer3_V int 73 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "arpTableIn_V", "interface" : "axis", "bitwidth" : 56, "direction" : "READONLY"} , 
 	{ "Name" : "myMacAddress_V", "interface" : "wire", "bitwidth" : 48, "direction" : "READONLY"} , 
 	{ "Name" : "dataStreamBuffer2_V", "interface" : "fifo", "bitwidth" : 73, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "headerFifo_V", "interface" : "fifo", "bitwidth" : 129, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "dataStreamBuffer3_V", "interface" : "fifo", "bitwidth" : 73, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 20
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ dataStreamBuffer2_V_dout sc_in sc_lv 73 signal 2 } 
	{ dataStreamBuffer2_V_empty_n sc_in sc_logic 1 signal 2 } 
	{ dataStreamBuffer2_V_read sc_out sc_logic 1 signal 2 } 
	{ arpTableIn_V_TVALID sc_in sc_logic 1 invld 0 } 
	{ dataStreamBuffer3_V_din sc_out sc_lv 73 signal 4 } 
	{ dataStreamBuffer3_V_full_n sc_in sc_logic 1 signal 4 } 
	{ dataStreamBuffer3_V_write sc_out sc_logic 1 signal 4 } 
	{ headerFifo_V_din sc_out sc_lv 129 signal 3 } 
	{ headerFifo_V_full_n sc_in sc_logic 1 signal 3 } 
	{ headerFifo_V_write sc_out sc_logic 1 signal 3 } 
	{ arpTableIn_V_TDATA sc_in sc_lv 56 signal 0 } 
	{ arpTableIn_V_TREADY sc_out sc_logic 1 inacc 0 } 
	{ myMacAddress_V sc_in sc_lv 48 signal 1 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "dataStreamBuffer2_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":73, "type": "signal", "bundle":{"name": "dataStreamBuffer2_V", "role": "dout" }} , 
 	{ "name": "dataStreamBuffer2_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataStreamBuffer2_V", "role": "empty_n" }} , 
 	{ "name": "dataStreamBuffer2_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataStreamBuffer2_V", "role": "read" }} , 
 	{ "name": "arpTableIn_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "arpTableIn_V", "role": "TVALID" }} , 
 	{ "name": "dataStreamBuffer3_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":73, "type": "signal", "bundle":{"name": "dataStreamBuffer3_V", "role": "din" }} , 
 	{ "name": "dataStreamBuffer3_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataStreamBuffer3_V", "role": "full_n" }} , 
 	{ "name": "dataStreamBuffer3_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataStreamBuffer3_V", "role": "write" }} , 
 	{ "name": "headerFifo_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":129, "type": "signal", "bundle":{"name": "headerFifo_V", "role": "din" }} , 
 	{ "name": "headerFifo_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "headerFifo_V", "role": "full_n" }} , 
 	{ "name": "headerFifo_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "headerFifo_V", "role": "write" }} , 
 	{ "name": "arpTableIn_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":56, "type": "signal", "bundle":{"name": "arpTableIn_V", "role": "TDATA" }} , 
 	{ "name": "arpTableIn_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "arpTableIn_V", "role": "TREADY" }} , 
 	{ "name": "myMacAddress_V", "direction": "in", "datatype": "sc_lv", "bitwidth":48, "type": "signal", "bundle":{"name": "myMacAddress_V", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "handle_arp_reply_64_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "arpTableIn_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "arpTableIn_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "myMacAddress_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "har_state", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "dataStreamBuffer2_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "dataStreamBuffer2_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "headerFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "headerFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataStreamBuffer3_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "dataStreamBuffer3_V_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	handle_arp_reply_64_s {
		arpTableIn_V {Type I LastRead 0 FirstWrite -1}
		myMacAddress_V {Type I LastRead 0 FirstWrite -1}
		har_state {Type IO LastRead -1 FirstWrite -1}
		dataStreamBuffer2_V {Type I LastRead 0 FirstWrite -1}
		headerFifo_V {Type O LastRead -1 FirstWrite 1}
		dataStreamBuffer3_V {Type O LastRead -1 FirstWrite 1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "1", "Max" : "1"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	arpTableIn_V { axis {  { arpTableIn_V_TVALID in_vld 0 1 }  { arpTableIn_V_TDATA in_data 0 56 }  { arpTableIn_V_TREADY in_acc 1 1 } } }
	myMacAddress_V { ap_stable {  { myMacAddress_V in_data 0 48 } } }
	dataStreamBuffer2_V { ap_fifo {  { dataStreamBuffer2_V_dout fifo_data 0 73 }  { dataStreamBuffer2_V_empty_n fifo_status 0 1 }  { dataStreamBuffer2_V_read fifo_update 1 1 } } }
	headerFifo_V { ap_fifo {  { headerFifo_V_din fifo_data 1 129 }  { headerFifo_V_full_n fifo_status 0 1 }  { headerFifo_V_write fifo_update 1 1 } } }
	dataStreamBuffer3_V { ap_fifo {  { dataStreamBuffer3_V_din fifo_data 1 73 }  { dataStreamBuffer3_V_full_n fifo_status 0 1 }  { dataStreamBuffer3_V_write fifo_update 1 1 } } }
}
set moduleName handle_arp_reply_64_s
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {handle_arp_reply<64>}
set C_modelType { void 0 }
set C_modelArgList {
	{ arpTableIn_V int 56 regular {axi_s 0 volatile  { arpTableIn_V Data } }  }
	{ myMacAddress_V int 48 regular {fifo 0}  }
	{ dataStreamBuffer2_V int 73 regular {fifo 0 volatile } {global 0}  }
	{ headerFifo_V int 129 regular {fifo 1 volatile } {global 1}  }
	{ dataStreamBuffer3_V int 73 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "arpTableIn_V", "interface" : "axis", "bitwidth" : 56, "direction" : "READONLY"} , 
 	{ "Name" : "myMacAddress_V", "interface" : "fifo", "bitwidth" : 48, "direction" : "READONLY"} , 
 	{ "Name" : "dataStreamBuffer2_V", "interface" : "fifo", "bitwidth" : 73, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "headerFifo_V", "interface" : "fifo", "bitwidth" : 129, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "dataStreamBuffer3_V", "interface" : "fifo", "bitwidth" : 73, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 22
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ dataStreamBuffer2_V_dout sc_in sc_lv 73 signal 2 } 
	{ dataStreamBuffer2_V_empty_n sc_in sc_logic 1 signal 2 } 
	{ dataStreamBuffer2_V_read sc_out sc_logic 1 signal 2 } 
	{ arpTableIn_V_TVALID sc_in sc_logic 1 invld 0 } 
	{ myMacAddress_V_dout sc_in sc_lv 48 signal 1 } 
	{ myMacAddress_V_empty_n sc_in sc_logic 1 signal 1 } 
	{ myMacAddress_V_read sc_out sc_logic 1 signal 1 } 
	{ dataStreamBuffer3_V_din sc_out sc_lv 73 signal 4 } 
	{ dataStreamBuffer3_V_full_n sc_in sc_logic 1 signal 4 } 
	{ dataStreamBuffer3_V_write sc_out sc_logic 1 signal 4 } 
	{ headerFifo_V_din sc_out sc_lv 129 signal 3 } 
	{ headerFifo_V_full_n sc_in sc_logic 1 signal 3 } 
	{ headerFifo_V_write sc_out sc_logic 1 signal 3 } 
	{ arpTableIn_V_TDATA sc_in sc_lv 56 signal 0 } 
	{ arpTableIn_V_TREADY sc_out sc_logic 1 inacc 0 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "dataStreamBuffer2_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":73, "type": "signal", "bundle":{"name": "dataStreamBuffer2_V", "role": "dout" }} , 
 	{ "name": "dataStreamBuffer2_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataStreamBuffer2_V", "role": "empty_n" }} , 
 	{ "name": "dataStreamBuffer2_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataStreamBuffer2_V", "role": "read" }} , 
 	{ "name": "arpTableIn_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "arpTableIn_V", "role": "TVALID" }} , 
 	{ "name": "myMacAddress_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":48, "type": "signal", "bundle":{"name": "myMacAddress_V", "role": "dout" }} , 
 	{ "name": "myMacAddress_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "myMacAddress_V", "role": "empty_n" }} , 
 	{ "name": "myMacAddress_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "myMacAddress_V", "role": "read" }} , 
 	{ "name": "dataStreamBuffer3_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":73, "type": "signal", "bundle":{"name": "dataStreamBuffer3_V", "role": "din" }} , 
 	{ "name": "dataStreamBuffer3_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataStreamBuffer3_V", "role": "full_n" }} , 
 	{ "name": "dataStreamBuffer3_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataStreamBuffer3_V", "role": "write" }} , 
 	{ "name": "headerFifo_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":129, "type": "signal", "bundle":{"name": "headerFifo_V", "role": "din" }} , 
 	{ "name": "headerFifo_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "headerFifo_V", "role": "full_n" }} , 
 	{ "name": "headerFifo_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "headerFifo_V", "role": "write" }} , 
 	{ "name": "arpTableIn_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":56, "type": "signal", "bundle":{"name": "arpTableIn_V", "role": "TDATA" }} , 
 	{ "name": "arpTableIn_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "arpTableIn_V", "role": "TREADY" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "handle_arp_reply_64_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "arpTableIn_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "arpTableIn_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "myMacAddress_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "myMacAddress_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "har_state", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "dataStreamBuffer2_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "dataStreamBuffer2_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "headerFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "headerFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataStreamBuffer3_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "dataStreamBuffer3_V_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	handle_arp_reply_64_s {
		arpTableIn_V {Type I LastRead 0 FirstWrite -1}
		myMacAddress_V {Type I LastRead 1 FirstWrite -1}
		har_state {Type IO LastRead -1 FirstWrite -1}
		dataStreamBuffer2_V {Type I LastRead 0 FirstWrite -1}
		headerFifo_V {Type O LastRead -1 FirstWrite 1}
		dataStreamBuffer3_V {Type O LastRead -1 FirstWrite 1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "1", "Max" : "1"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	arpTableIn_V { axis {  { arpTableIn_V_TVALID in_vld 0 1 }  { arpTableIn_V_TDATA in_data 0 56 }  { arpTableIn_V_TREADY in_acc 1 1 } } }
	myMacAddress_V { ap_fifo {  { myMacAddress_V_dout fifo_data 0 48 }  { myMacAddress_V_empty_n fifo_status 0 1 }  { myMacAddress_V_read fifo_update 1 1 } } }
	dataStreamBuffer2_V { ap_fifo {  { dataStreamBuffer2_V_dout fifo_data 0 73 }  { dataStreamBuffer2_V_empty_n fifo_status 0 1 }  { dataStreamBuffer2_V_read fifo_update 1 1 } } }
	headerFifo_V { ap_fifo {  { headerFifo_V_din fifo_data 1 129 }  { headerFifo_V_full_n fifo_status 0 1 }  { headerFifo_V_write fifo_update 1 1 } } }
	dataStreamBuffer3_V { ap_fifo {  { dataStreamBuffer3_V_din fifo_data 1 73 }  { dataStreamBuffer3_V_full_n fifo_status 0 1 }  { dataStreamBuffer3_V_write fifo_update 1 1 } } }
}
