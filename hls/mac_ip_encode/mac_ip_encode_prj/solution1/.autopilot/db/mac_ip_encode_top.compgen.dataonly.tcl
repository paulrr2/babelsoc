# This script segment is generated automatically by AutoPilot

set axilite_register_dict [dict create]
set port_AXILiteS {
myMacAddress_V { 
	dir I
	width 48
	depth 1
	mode ap_none
	offset 16
	offset_end 27
}
regSubNetMask_V { 
	dir I
	width 32
	depth 1
	mode ap_none
	offset 28
	offset_end 35
}
regDefaultGateway_V { 
	dir I
	width 32
	depth 1
	mode ap_none
	offset 36
	offset_end 43
}
}
dict set axilite_register_dict AXILiteS $port_AXILiteS


