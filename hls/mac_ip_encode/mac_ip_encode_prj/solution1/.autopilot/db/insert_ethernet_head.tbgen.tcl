set moduleName insert_ethernet_head
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {insert_ethernet_head}
set C_modelType { void 0 }
set C_modelArgList {
	{ dataOut_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_ip Data } }  }
	{ dataOut_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_ip Keep } }  }
	{ dataOut_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_ip Last } }  }
	{ headerFifo_V int 129 regular {fifo 0 volatile } {global 0}  }
	{ dataStreamBuffer4_V int 73 regular {fifo 0 volatile } {global 0}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "dataOut_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY"} , 
 	{ "Name" : "dataOut_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY"} , 
 	{ "Name" : "dataOut_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY"} , 
 	{ "Name" : "headerFifo_V", "interface" : "fifo", "bitwidth" : 129, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "dataStreamBuffer4_V", "interface" : "fifo", "bitwidth" : 73, "direction" : "READONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 18
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ dataStreamBuffer4_V_dout sc_in sc_lv 73 signal 4 } 
	{ dataStreamBuffer4_V_empty_n sc_in sc_logic 1 signal 4 } 
	{ dataStreamBuffer4_V_read sc_out sc_logic 1 signal 4 } 
	{ headerFifo_V_dout sc_in sc_lv 129 signal 3 } 
	{ headerFifo_V_empty_n sc_in sc_logic 1 signal 3 } 
	{ headerFifo_V_read sc_out sc_logic 1 signal 3 } 
	{ m_axis_ip_TREADY sc_in sc_logic 1 outacc 2 } 
	{ m_axis_ip_TDATA sc_out sc_lv 64 signal 0 } 
	{ m_axis_ip_TVALID sc_out sc_logic 1 outvld 2 } 
	{ m_axis_ip_TKEEP sc_out sc_lv 8 signal 1 } 
	{ m_axis_ip_TLAST sc_out sc_lv 1 signal 2 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "dataStreamBuffer4_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":73, "type": "signal", "bundle":{"name": "dataStreamBuffer4_V", "role": "dout" }} , 
 	{ "name": "dataStreamBuffer4_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataStreamBuffer4_V", "role": "empty_n" }} , 
 	{ "name": "dataStreamBuffer4_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataStreamBuffer4_V", "role": "read" }} , 
 	{ "name": "headerFifo_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":129, "type": "signal", "bundle":{"name": "headerFifo_V", "role": "dout" }} , 
 	{ "name": "headerFifo_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "headerFifo_V", "role": "empty_n" }} , 
 	{ "name": "headerFifo_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "headerFifo_V", "role": "read" }} , 
 	{ "name": "m_axis_ip_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "dataOut_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_ip_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "dataOut_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_ip_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "dataOut_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_ip_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "dataOut_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_ip_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "dataOut_V_last_V", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2", "3"],
		"CDFG" : "insert_ethernet_head",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "4", "EstimateLatencyMin" : "4", "EstimateLatencyMax" : "4",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "dataOut_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_ip_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataOut_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "dataOut_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "ge_state", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "headerFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "headerFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "header_header_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_idx", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "dataStreamBuffer4_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "dataStreamBuffer4_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_dataOut_V_data_V_U", "Parent" : "0"},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_dataOut_V_keep_V_U", "Parent" : "0"},
	{"ID" : "3", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_dataOut_V_last_V_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	insert_ethernet_head {
		dataOut_V_data_V {Type O LastRead -1 FirstWrite 3}
		dataOut_V_keep_V {Type O LastRead -1 FirstWrite 3}
		dataOut_V_last_V {Type O LastRead -1 FirstWrite 3}
		ge_state {Type IO LastRead -1 FirstWrite -1}
		headerFifo_V {Type I LastRead 1 FirstWrite -1}
		header_header_V {Type IO LastRead -1 FirstWrite -1}
		header_idx {Type IO LastRead -1 FirstWrite -1}
		dataStreamBuffer4_V {Type I LastRead 0 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "4", "Max" : "4"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	dataOut_V_data_V { axis {  { m_axis_ip_TDATA out_data 1 64 } } }
	dataOut_V_keep_V { axis {  { m_axis_ip_TKEEP out_data 1 8 } } }
	dataOut_V_last_V { axis {  { m_axis_ip_TREADY out_acc 0 1 }  { m_axis_ip_TVALID out_vld 1 1 }  { m_axis_ip_TLAST out_data 1 1 } } }
	headerFifo_V { ap_fifo {  { headerFifo_V_dout fifo_data 0 129 }  { headerFifo_V_empty_n fifo_status 0 1 }  { headerFifo_V_read fifo_update 1 1 } } }
	dataStreamBuffer4_V { ap_fifo {  { dataStreamBuffer4_V_dout fifo_data 0 73 }  { dataStreamBuffer4_V_empty_n fifo_status 0 1 }  { dataStreamBuffer4_V_read fifo_update 1 1 } } }
}
set moduleName insert_ethernet_head
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {insert_ethernet_head}
set C_modelType { void 0 }
set C_modelArgList {
	{ dataOut_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_ip Data } }  }
	{ dataOut_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_ip Keep } }  }
	{ dataOut_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_ip Last } }  }
	{ headerFifo_V int 129 regular {fifo 0 volatile } {global 0}  }
	{ dataStreamBuffer4_V int 73 regular {fifo 0 volatile } {global 0}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "dataOut_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY"} , 
 	{ "Name" : "dataOut_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY"} , 
 	{ "Name" : "dataOut_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY"} , 
 	{ "Name" : "headerFifo_V", "interface" : "fifo", "bitwidth" : 129, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "dataStreamBuffer4_V", "interface" : "fifo", "bitwidth" : 73, "direction" : "READONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 18
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ dataStreamBuffer4_V_dout sc_in sc_lv 73 signal 4 } 
	{ dataStreamBuffer4_V_empty_n sc_in sc_logic 1 signal 4 } 
	{ dataStreamBuffer4_V_read sc_out sc_logic 1 signal 4 } 
	{ headerFifo_V_dout sc_in sc_lv 129 signal 3 } 
	{ headerFifo_V_empty_n sc_in sc_logic 1 signal 3 } 
	{ headerFifo_V_read sc_out sc_logic 1 signal 3 } 
	{ m_axis_ip_TREADY sc_in sc_logic 1 outacc 2 } 
	{ m_axis_ip_TDATA sc_out sc_lv 64 signal 0 } 
	{ m_axis_ip_TVALID sc_out sc_logic 1 outvld 2 } 
	{ m_axis_ip_TKEEP sc_out sc_lv 8 signal 1 } 
	{ m_axis_ip_TLAST sc_out sc_lv 1 signal 2 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "dataStreamBuffer4_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":73, "type": "signal", "bundle":{"name": "dataStreamBuffer4_V", "role": "dout" }} , 
 	{ "name": "dataStreamBuffer4_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataStreamBuffer4_V", "role": "empty_n" }} , 
 	{ "name": "dataStreamBuffer4_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataStreamBuffer4_V", "role": "read" }} , 
 	{ "name": "headerFifo_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":129, "type": "signal", "bundle":{"name": "headerFifo_V", "role": "dout" }} , 
 	{ "name": "headerFifo_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "headerFifo_V", "role": "empty_n" }} , 
 	{ "name": "headerFifo_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "headerFifo_V", "role": "read" }} , 
 	{ "name": "m_axis_ip_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "dataOut_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_ip_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "dataOut_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_ip_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "dataOut_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_ip_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "dataOut_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_ip_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "dataOut_V_last_V", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "insert_ethernet_head",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "4", "EstimateLatencyMin" : "4", "EstimateLatencyMax" : "4",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "dataOut_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_ip_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataOut_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "dataOut_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "ge_state", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "headerFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "headerFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "header_header_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_idx", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "dataStreamBuffer4_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "dataStreamBuffer4_V_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	insert_ethernet_head {
		dataOut_V_data_V {Type O LastRead -1 FirstWrite 3}
		dataOut_V_keep_V {Type O LastRead -1 FirstWrite 3}
		dataOut_V_last_V {Type O LastRead -1 FirstWrite 3}
		ge_state {Type IO LastRead -1 FirstWrite -1}
		headerFifo_V {Type I LastRead 1 FirstWrite -1}
		header_header_V {Type IO LastRead -1 FirstWrite -1}
		header_idx {Type IO LastRead -1 FirstWrite -1}
		dataStreamBuffer4_V {Type I LastRead 0 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "4", "Max" : "4"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	dataOut_V_data_V { axis {  { m_axis_ip_TDATA out_data 1 64 } } }
	dataOut_V_keep_V { axis {  { m_axis_ip_TKEEP out_data 1 8 } } }
	dataOut_V_last_V { axis {  { m_axis_ip_TREADY out_acc 0 1 }  { m_axis_ip_TVALID out_vld 1 1 }  { m_axis_ip_TLAST out_data 1 1 } } }
	headerFifo_V { ap_fifo {  { headerFifo_V_dout fifo_data 0 129 }  { headerFifo_V_empty_n fifo_status 0 1 }  { headerFifo_V_read fifo_update 1 1 } } }
	dataStreamBuffer4_V { ap_fifo {  { dataStreamBuffer4_V_dout fifo_data 0 73 }  { dataStreamBuffer4_V_empty_n fifo_status 0 1 }  { dataStreamBuffer4_V_read fifo_update 1 1 } } }
}
