set moduleName compute_ipv4_checksu
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {compute_ipv4_checksu}
set C_modelType { void 0 }
set C_modelArgList {
	{ dataStreamBuffer0_V int 73 regular {fifo 0 volatile } {global 0}  }
	{ dataStreamBuffer1_V int 73 regular {fifo 1 volatile } {global 1}  }
	{ subSumFifo_V_sum_V_0 int 17 regular {fifo 1 volatile } {global 1}  }
	{ subSumFifo_V_sum_V_1 int 17 regular {fifo 1 volatile } {global 1}  }
	{ subSumFifo_V_sum_V_2 int 17 regular {fifo 1 volatile } {global 1}  }
	{ subSumFifo_V_sum_V_3 int 17 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "dataStreamBuffer0_V", "interface" : "fifo", "bitwidth" : 73, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "dataStreamBuffer1_V", "interface" : "fifo", "bitwidth" : 73, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "subSumFifo_V_sum_V_0", "interface" : "fifo", "bitwidth" : 17, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "subSumFifo_V_sum_V_1", "interface" : "fifo", "bitwidth" : 17, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "subSumFifo_V_sum_V_2", "interface" : "fifo", "bitwidth" : 17, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "subSumFifo_V_sum_V_3", "interface" : "fifo", "bitwidth" : 17, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 25
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ dataStreamBuffer0_V_dout sc_in sc_lv 73 signal 0 } 
	{ dataStreamBuffer0_V_empty_n sc_in sc_logic 1 signal 0 } 
	{ dataStreamBuffer0_V_read sc_out sc_logic 1 signal 0 } 
	{ dataStreamBuffer1_V_din sc_out sc_lv 73 signal 1 } 
	{ dataStreamBuffer1_V_full_n sc_in sc_logic 1 signal 1 } 
	{ dataStreamBuffer1_V_write sc_out sc_logic 1 signal 1 } 
	{ subSumFifo_V_sum_V_0_din sc_out sc_lv 17 signal 2 } 
	{ subSumFifo_V_sum_V_0_full_n sc_in sc_logic 1 signal 2 } 
	{ subSumFifo_V_sum_V_0_write sc_out sc_logic 1 signal 2 } 
	{ subSumFifo_V_sum_V_1_din sc_out sc_lv 17 signal 3 } 
	{ subSumFifo_V_sum_V_1_full_n sc_in sc_logic 1 signal 3 } 
	{ subSumFifo_V_sum_V_1_write sc_out sc_logic 1 signal 3 } 
	{ subSumFifo_V_sum_V_2_din sc_out sc_lv 17 signal 4 } 
	{ subSumFifo_V_sum_V_2_full_n sc_in sc_logic 1 signal 4 } 
	{ subSumFifo_V_sum_V_2_write sc_out sc_logic 1 signal 4 } 
	{ subSumFifo_V_sum_V_3_din sc_out sc_lv 17 signal 5 } 
	{ subSumFifo_V_sum_V_3_full_n sc_in sc_logic 1 signal 5 } 
	{ subSumFifo_V_sum_V_3_write sc_out sc_logic 1 signal 5 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "dataStreamBuffer0_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":73, "type": "signal", "bundle":{"name": "dataStreamBuffer0_V", "role": "dout" }} , 
 	{ "name": "dataStreamBuffer0_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataStreamBuffer0_V", "role": "empty_n" }} , 
 	{ "name": "dataStreamBuffer0_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataStreamBuffer0_V", "role": "read" }} , 
 	{ "name": "dataStreamBuffer1_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":73, "type": "signal", "bundle":{"name": "dataStreamBuffer1_V", "role": "din" }} , 
 	{ "name": "dataStreamBuffer1_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataStreamBuffer1_V", "role": "full_n" }} , 
 	{ "name": "dataStreamBuffer1_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataStreamBuffer1_V", "role": "write" }} , 
 	{ "name": "subSumFifo_V_sum_V_0_din", "direction": "out", "datatype": "sc_lv", "bitwidth":17, "type": "signal", "bundle":{"name": "subSumFifo_V_sum_V_0", "role": "din" }} , 
 	{ "name": "subSumFifo_V_sum_V_0_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "subSumFifo_V_sum_V_0", "role": "full_n" }} , 
 	{ "name": "subSumFifo_V_sum_V_0_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "subSumFifo_V_sum_V_0", "role": "write" }} , 
 	{ "name": "subSumFifo_V_sum_V_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":17, "type": "signal", "bundle":{"name": "subSumFifo_V_sum_V_1", "role": "din" }} , 
 	{ "name": "subSumFifo_V_sum_V_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "subSumFifo_V_sum_V_1", "role": "full_n" }} , 
 	{ "name": "subSumFifo_V_sum_V_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "subSumFifo_V_sum_V_1", "role": "write" }} , 
 	{ "name": "subSumFifo_V_sum_V_2_din", "direction": "out", "datatype": "sc_lv", "bitwidth":17, "type": "signal", "bundle":{"name": "subSumFifo_V_sum_V_2", "role": "din" }} , 
 	{ "name": "subSumFifo_V_sum_V_2_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "subSumFifo_V_sum_V_2", "role": "full_n" }} , 
 	{ "name": "subSumFifo_V_sum_V_2_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "subSumFifo_V_sum_V_2", "role": "write" }} , 
 	{ "name": "subSumFifo_V_sum_V_3_din", "direction": "out", "datatype": "sc_lv", "bitwidth":17, "type": "signal", "bundle":{"name": "subSumFifo_V_sum_V_3", "role": "din" }} , 
 	{ "name": "subSumFifo_V_sum_V_3_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "subSumFifo_V_sum_V_3", "role": "full_n" }} , 
 	{ "name": "subSumFifo_V_sum_V_3_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "subSumFifo_V_sum_V_3", "role": "write" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "compute_ipv4_checksu",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "3", "EstimateLatencyMin" : "3", "EstimateLatencyMax" : "3",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "dataStreamBuffer0_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "dataStreamBuffer0_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataStreamBuffer1_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "dataStreamBuffer1_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "cics_wordCount_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ip_sums_sum_V_0", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ip_sums_sum_V_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ip_sums_sum_V_2", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ip_sums_sum_V_3", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ipHeaderLen_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "subSumFifo_V_sum_V_0", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "subSumFifo_V_sum_V_0_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "subSumFifo_V_sum_V_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "subSumFifo_V_sum_V_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "subSumFifo_V_sum_V_2", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "subSumFifo_V_sum_V_2_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "subSumFifo_V_sum_V_3", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "subSumFifo_V_sum_V_3_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	compute_ipv4_checksu {
		dataStreamBuffer0_V {Type I LastRead 0 FirstWrite -1}
		dataStreamBuffer1_V {Type O LastRead -1 FirstWrite 1}
		cics_wordCount_V {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_0 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_1 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_2 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_3 {Type IO LastRead -1 FirstWrite -1}
		cics_ipHeaderLen_V {Type IO LastRead -1 FirstWrite -1}
		subSumFifo_V_sum_V_0 {Type O LastRead -1 FirstWrite 3}
		subSumFifo_V_sum_V_1 {Type O LastRead -1 FirstWrite 3}
		subSumFifo_V_sum_V_2 {Type O LastRead -1 FirstWrite 3}
		subSumFifo_V_sum_V_3 {Type O LastRead -1 FirstWrite 3}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "3", "Max" : "3"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	dataStreamBuffer0_V { ap_fifo {  { dataStreamBuffer0_V_dout fifo_data 0 73 }  { dataStreamBuffer0_V_empty_n fifo_status 0 1 }  { dataStreamBuffer0_V_read fifo_update 1 1 } } }
	dataStreamBuffer1_V { ap_fifo {  { dataStreamBuffer1_V_din fifo_data 1 73 }  { dataStreamBuffer1_V_full_n fifo_status 0 1 }  { dataStreamBuffer1_V_write fifo_update 1 1 } } }
	subSumFifo_V_sum_V_0 { ap_fifo {  { subSumFifo_V_sum_V_0_din fifo_data 1 17 }  { subSumFifo_V_sum_V_0_full_n fifo_status 0 1 }  { subSumFifo_V_sum_V_0_write fifo_update 1 1 } } }
	subSumFifo_V_sum_V_1 { ap_fifo {  { subSumFifo_V_sum_V_1_din fifo_data 1 17 }  { subSumFifo_V_sum_V_1_full_n fifo_status 0 1 }  { subSumFifo_V_sum_V_1_write fifo_update 1 1 } } }
	subSumFifo_V_sum_V_2 { ap_fifo {  { subSumFifo_V_sum_V_2_din fifo_data 1 17 }  { subSumFifo_V_sum_V_2_full_n fifo_status 0 1 }  { subSumFifo_V_sum_V_2_write fifo_update 1 1 } } }
	subSumFifo_V_sum_V_3 { ap_fifo {  { subSumFifo_V_sum_V_3_din fifo_data 1 17 }  { subSumFifo_V_sum_V_3_full_n fifo_status 0 1 }  { subSumFifo_V_sum_V_3_write fifo_update 1 1 } } }
}
set moduleName compute_ipv4_checksu
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {compute_ipv4_checksu}
set C_modelType { void 0 }
set C_modelArgList {
	{ dataStreamBuffer0_V int 73 regular {fifo 0 volatile } {global 0}  }
	{ dataStreamBuffer1_V int 73 regular {fifo 1 volatile } {global 1}  }
	{ subSumFifo_V_sum_V_0 int 17 regular {fifo 1 volatile } {global 1}  }
	{ subSumFifo_V_sum_V_1 int 17 regular {fifo 1 volatile } {global 1}  }
	{ subSumFifo_V_sum_V_2 int 17 regular {fifo 1 volatile } {global 1}  }
	{ subSumFifo_V_sum_V_3 int 17 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "dataStreamBuffer0_V", "interface" : "fifo", "bitwidth" : 73, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "dataStreamBuffer1_V", "interface" : "fifo", "bitwidth" : 73, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "subSumFifo_V_sum_V_0", "interface" : "fifo", "bitwidth" : 17, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "subSumFifo_V_sum_V_1", "interface" : "fifo", "bitwidth" : 17, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "subSumFifo_V_sum_V_2", "interface" : "fifo", "bitwidth" : 17, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "subSumFifo_V_sum_V_3", "interface" : "fifo", "bitwidth" : 17, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 25
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ dataStreamBuffer0_V_dout sc_in sc_lv 73 signal 0 } 
	{ dataStreamBuffer0_V_empty_n sc_in sc_logic 1 signal 0 } 
	{ dataStreamBuffer0_V_read sc_out sc_logic 1 signal 0 } 
	{ dataStreamBuffer1_V_din sc_out sc_lv 73 signal 1 } 
	{ dataStreamBuffer1_V_full_n sc_in sc_logic 1 signal 1 } 
	{ dataStreamBuffer1_V_write sc_out sc_logic 1 signal 1 } 
	{ subSumFifo_V_sum_V_0_din sc_out sc_lv 17 signal 2 } 
	{ subSumFifo_V_sum_V_0_full_n sc_in sc_logic 1 signal 2 } 
	{ subSumFifo_V_sum_V_0_write sc_out sc_logic 1 signal 2 } 
	{ subSumFifo_V_sum_V_1_din sc_out sc_lv 17 signal 3 } 
	{ subSumFifo_V_sum_V_1_full_n sc_in sc_logic 1 signal 3 } 
	{ subSumFifo_V_sum_V_1_write sc_out sc_logic 1 signal 3 } 
	{ subSumFifo_V_sum_V_2_din sc_out sc_lv 17 signal 4 } 
	{ subSumFifo_V_sum_V_2_full_n sc_in sc_logic 1 signal 4 } 
	{ subSumFifo_V_sum_V_2_write sc_out sc_logic 1 signal 4 } 
	{ subSumFifo_V_sum_V_3_din sc_out sc_lv 17 signal 5 } 
	{ subSumFifo_V_sum_V_3_full_n sc_in sc_logic 1 signal 5 } 
	{ subSumFifo_V_sum_V_3_write sc_out sc_logic 1 signal 5 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "dataStreamBuffer0_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":73, "type": "signal", "bundle":{"name": "dataStreamBuffer0_V", "role": "dout" }} , 
 	{ "name": "dataStreamBuffer0_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataStreamBuffer0_V", "role": "empty_n" }} , 
 	{ "name": "dataStreamBuffer0_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataStreamBuffer0_V", "role": "read" }} , 
 	{ "name": "dataStreamBuffer1_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":73, "type": "signal", "bundle":{"name": "dataStreamBuffer1_V", "role": "din" }} , 
 	{ "name": "dataStreamBuffer1_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataStreamBuffer1_V", "role": "full_n" }} , 
 	{ "name": "dataStreamBuffer1_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataStreamBuffer1_V", "role": "write" }} , 
 	{ "name": "subSumFifo_V_sum_V_0_din", "direction": "out", "datatype": "sc_lv", "bitwidth":17, "type": "signal", "bundle":{"name": "subSumFifo_V_sum_V_0", "role": "din" }} , 
 	{ "name": "subSumFifo_V_sum_V_0_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "subSumFifo_V_sum_V_0", "role": "full_n" }} , 
 	{ "name": "subSumFifo_V_sum_V_0_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "subSumFifo_V_sum_V_0", "role": "write" }} , 
 	{ "name": "subSumFifo_V_sum_V_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":17, "type": "signal", "bundle":{"name": "subSumFifo_V_sum_V_1", "role": "din" }} , 
 	{ "name": "subSumFifo_V_sum_V_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "subSumFifo_V_sum_V_1", "role": "full_n" }} , 
 	{ "name": "subSumFifo_V_sum_V_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "subSumFifo_V_sum_V_1", "role": "write" }} , 
 	{ "name": "subSumFifo_V_sum_V_2_din", "direction": "out", "datatype": "sc_lv", "bitwidth":17, "type": "signal", "bundle":{"name": "subSumFifo_V_sum_V_2", "role": "din" }} , 
 	{ "name": "subSumFifo_V_sum_V_2_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "subSumFifo_V_sum_V_2", "role": "full_n" }} , 
 	{ "name": "subSumFifo_V_sum_V_2_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "subSumFifo_V_sum_V_2", "role": "write" }} , 
 	{ "name": "subSumFifo_V_sum_V_3_din", "direction": "out", "datatype": "sc_lv", "bitwidth":17, "type": "signal", "bundle":{"name": "subSumFifo_V_sum_V_3", "role": "din" }} , 
 	{ "name": "subSumFifo_V_sum_V_3_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "subSumFifo_V_sum_V_3", "role": "full_n" }} , 
 	{ "name": "subSumFifo_V_sum_V_3_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "subSumFifo_V_sum_V_3", "role": "write" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "compute_ipv4_checksu",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "dataStreamBuffer0_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "dataStreamBuffer0_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataStreamBuffer1_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "dataStreamBuffer1_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "cics_wordCount_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ip_sums_sum_V_0", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ip_sums_sum_V_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ip_sums_sum_V_2", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ip_sums_sum_V_3", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ipHeaderLen_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "subSumFifo_V_sum_V_0", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "subSumFifo_V_sum_V_0_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "subSumFifo_V_sum_V_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "subSumFifo_V_sum_V_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "subSumFifo_V_sum_V_2", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "subSumFifo_V_sum_V_2_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "subSumFifo_V_sum_V_3", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "subSumFifo_V_sum_V_3_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	compute_ipv4_checksu {
		dataStreamBuffer0_V {Type I LastRead 0 FirstWrite -1}
		dataStreamBuffer1_V {Type O LastRead -1 FirstWrite 1}
		cics_wordCount_V {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_0 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_1 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_2 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_3 {Type IO LastRead -1 FirstWrite -1}
		cics_ipHeaderLen_V {Type IO LastRead -1 FirstWrite -1}
		subSumFifo_V_sum_V_0 {Type O LastRead -1 FirstWrite 2}
		subSumFifo_V_sum_V_1 {Type O LastRead -1 FirstWrite 2}
		subSumFifo_V_sum_V_2 {Type O LastRead -1 FirstWrite 2}
		subSumFifo_V_sum_V_3 {Type O LastRead -1 FirstWrite 2}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "2", "Max" : "2"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	dataStreamBuffer0_V { ap_fifo {  { dataStreamBuffer0_V_dout fifo_data 0 73 }  { dataStreamBuffer0_V_empty_n fifo_status 0 1 }  { dataStreamBuffer0_V_read fifo_update 1 1 } } }
	dataStreamBuffer1_V { ap_fifo {  { dataStreamBuffer1_V_din fifo_data 1 73 }  { dataStreamBuffer1_V_full_n fifo_status 0 1 }  { dataStreamBuffer1_V_write fifo_update 1 1 } } }
	subSumFifo_V_sum_V_0 { ap_fifo {  { subSumFifo_V_sum_V_0_din fifo_data 1 17 }  { subSumFifo_V_sum_V_0_full_n fifo_status 0 1 }  { subSumFifo_V_sum_V_0_write fifo_update 1 1 } } }
	subSumFifo_V_sum_V_1 { ap_fifo {  { subSumFifo_V_sum_V_1_din fifo_data 1 17 }  { subSumFifo_V_sum_V_1_full_n fifo_status 0 1 }  { subSumFifo_V_sum_V_1_write fifo_update 1 1 } } }
	subSumFifo_V_sum_V_2 { ap_fifo {  { subSumFifo_V_sum_V_2_din fifo_data 1 17 }  { subSumFifo_V_sum_V_2_full_n fifo_status 0 1 }  { subSumFifo_V_sum_V_2_write fifo_update 1 1 } } }
	subSumFifo_V_sum_V_3 { ap_fifo {  { subSumFifo_V_sum_V_3_din fifo_data 1 17 }  { subSumFifo_V_sum_V_3_full_n fifo_status 0 1 }  { subSumFifo_V_sum_V_3_write fifo_update 1 1 } } }
}
