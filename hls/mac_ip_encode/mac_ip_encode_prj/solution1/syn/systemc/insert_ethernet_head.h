// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2019.1
// Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

#ifndef _insert_ethernet_head_HH_
#define _insert_ethernet_head_HH_

#include "systemc.h"
#include "AESL_pkg.h"


namespace ap_rtl {

struct insert_ethernet_head : public sc_module {
    // Port declarations 18
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_in< sc_logic > ap_start;
    sc_out< sc_logic > ap_done;
    sc_in< sc_logic > ap_continue;
    sc_out< sc_logic > ap_idle;
    sc_out< sc_logic > ap_ready;
    sc_in< sc_lv<73> > dataStreamBuffer4_V_dout;
    sc_in< sc_logic > dataStreamBuffer4_V_empty_n;
    sc_out< sc_logic > dataStreamBuffer4_V_read;
    sc_in< sc_lv<129> > headerFifo_V_dout;
    sc_in< sc_logic > headerFifo_V_empty_n;
    sc_out< sc_logic > headerFifo_V_read;
    sc_in< sc_logic > m_axis_ip_TREADY;
    sc_out< sc_lv<64> > m_axis_ip_TDATA;
    sc_out< sc_logic > m_axis_ip_TVALID;
    sc_out< sc_lv<8> > m_axis_ip_TKEEP;
    sc_out< sc_lv<1> > m_axis_ip_TLAST;


    // Module declarations
    insert_ethernet_head(sc_module_name name);
    SC_HAS_PROCESS(insert_ethernet_head);

    ~insert_ethernet_head();

    sc_trace_file* mVcdFile;

    sc_signal< sc_logic > ap_done_reg;
    sc_signal< sc_lv<1> > ap_CS_fsm;
    sc_signal< sc_logic > ap_CS_fsm_pp0_stage0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter1;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter2;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter3;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter4;
    sc_signal< sc_logic > ap_idle_pp0;
    sc_signal< sc_lv<1> > grp_nbreadreq_fu_114_p3;
    sc_signal< bool > ap_predicate_op9_read_state1;
    sc_signal< bool > ap_predicate_op15_read_state1;
    sc_signal< sc_lv<1> > grp_nbreadreq_fu_128_p3;
    sc_signal< bool > ap_predicate_op23_read_state1;
    sc_signal< bool > ap_block_state1_pp0_stage0_iter0;
    sc_signal< bool > ap_block_state2_pp0_stage0_iter1;
    sc_signal< bool > ap_block_state3_pp0_stage0_iter2;
    sc_signal< bool > ap_block_state4_pp0_stage0_iter3;
    sc_signal< sc_logic > dataOut_V_data_V_1_ack_in;
    sc_signal< sc_lv<2> > ge_state_load_reg_840;
    sc_signal< sc_lv<2> > ge_state_load_reg_840_pp0_iter2_reg;
    sc_signal< sc_lv<1> > tmp_5_reg_844;
    sc_signal< sc_lv<1> > tmp_5_reg_844_pp0_iter2_reg;
    sc_signal< bool > ap_predicate_op111_write_state4;
    sc_signal< sc_lv<1> > tmp_4_reg_858;
    sc_signal< sc_lv<1> > tmp_4_reg_858_pp0_iter2_reg;
    sc_signal< bool > ap_predicate_op124_write_state4;
    sc_signal< sc_lv<1> > tmp_reg_873;
    sc_signal< sc_lv<1> > tmp_reg_873_pp0_iter2_reg;
    sc_signal< bool > ap_predicate_op135_write_state4;
    sc_signal< bool > ap_block_state4_io;
    sc_signal< sc_logic > dataOut_V_data_V_1_ack_out;
    sc_signal< sc_lv<2> > dataOut_V_data_V_1_state;
    sc_signal< sc_logic > dataOut_V_keep_V_1_ack_out;
    sc_signal< sc_lv<2> > dataOut_V_keep_V_1_state;
    sc_signal< sc_logic > dataOut_V_last_V_1_ack_out;
    sc_signal< sc_lv<2> > dataOut_V_last_V_1_state;
    sc_signal< bool > ap_block_state5_pp0_stage0_iter4;
    sc_signal< sc_lv<2> > ge_state_load_reg_840_pp0_iter3_reg;
    sc_signal< sc_lv<1> > tmp_5_reg_844_pp0_iter3_reg;
    sc_signal< bool > ap_predicate_op140_write_state5;
    sc_signal< sc_lv<1> > tmp_4_reg_858_pp0_iter3_reg;
    sc_signal< bool > ap_predicate_op144_write_state5;
    sc_signal< sc_lv<1> > tmp_reg_873_pp0_iter3_reg;
    sc_signal< bool > ap_predicate_op147_write_state5;
    sc_signal< bool > ap_block_state5_io;
    sc_signal< bool > ap_block_pp0_stage0_11001;
    sc_signal< sc_lv<64> > dataOut_V_data_V_1_data_in;
    sc_signal< sc_lv<64> > dataOut_V_data_V_1_data_out;
    sc_signal< sc_logic > dataOut_V_data_V_1_vld_in;
    sc_signal< sc_logic > dataOut_V_data_V_1_vld_out;
    sc_signal< sc_lv<64> > dataOut_V_data_V_1_payload_A;
    sc_signal< sc_lv<64> > dataOut_V_data_V_1_payload_B;
    sc_signal< sc_logic > dataOut_V_data_V_1_sel_rd;
    sc_signal< sc_logic > dataOut_V_data_V_1_sel_wr;
    sc_signal< sc_logic > dataOut_V_data_V_1_sel;
    sc_signal< sc_logic > dataOut_V_data_V_1_load_A;
    sc_signal< sc_logic > dataOut_V_data_V_1_load_B;
    sc_signal< sc_logic > dataOut_V_data_V_1_state_cmp_full;
    sc_signal< sc_lv<8> > dataOut_V_keep_V_1_data_in;
    sc_signal< sc_lv<8> > dataOut_V_keep_V_1_data_out;
    sc_signal< sc_logic > dataOut_V_keep_V_1_vld_in;
    sc_signal< sc_logic > dataOut_V_keep_V_1_vld_out;
    sc_signal< sc_logic > dataOut_V_keep_V_1_ack_in;
    sc_signal< sc_lv<8> > dataOut_V_keep_V_1_payload_A;
    sc_signal< sc_lv<8> > dataOut_V_keep_V_1_payload_B;
    sc_signal< sc_logic > dataOut_V_keep_V_1_sel_rd;
    sc_signal< sc_logic > dataOut_V_keep_V_1_sel_wr;
    sc_signal< sc_logic > dataOut_V_keep_V_1_sel;
    sc_signal< sc_logic > dataOut_V_keep_V_1_load_A;
    sc_signal< sc_logic > dataOut_V_keep_V_1_load_B;
    sc_signal< sc_logic > dataOut_V_keep_V_1_state_cmp_full;
    sc_signal< sc_lv<1> > dataOut_V_last_V_1_data_in;
    sc_signal< sc_lv<1> > dataOut_V_last_V_1_data_out;
    sc_signal< sc_logic > dataOut_V_last_V_1_vld_in;
    sc_signal< sc_logic > dataOut_V_last_V_1_vld_out;
    sc_signal< sc_logic > dataOut_V_last_V_1_ack_in;
    sc_signal< sc_lv<1> > dataOut_V_last_V_1_payload_A;
    sc_signal< sc_lv<1> > dataOut_V_last_V_1_payload_B;
    sc_signal< sc_logic > dataOut_V_last_V_1_sel_rd;
    sc_signal< sc_logic > dataOut_V_last_V_1_sel_wr;
    sc_signal< sc_logic > dataOut_V_last_V_1_sel;
    sc_signal< sc_logic > dataOut_V_last_V_1_load_A;
    sc_signal< sc_logic > dataOut_V_last_V_1_load_B;
    sc_signal< sc_logic > dataOut_V_last_V_1_state_cmp_full;
    sc_signal< sc_lv<2> > ge_state;
    sc_signal< sc_lv<112> > header_header_V;
    sc_signal< sc_lv<16> > header_idx;
    sc_signal< sc_logic > m_axis_ip_TDATA_blk_n;
    sc_signal< bool > ap_block_pp0_stage0;
    sc_signal< sc_logic > headerFifo_V_blk_n;
    sc_signal< sc_logic > dataStreamBuffer4_V_blk_n;
    sc_signal< sc_lv<8> > reg_175;
    sc_signal< sc_lv<8> > reg_175_pp0_iter1_reg;
    sc_signal< sc_lv<8> > reg_175_pp0_iter2_reg;
    sc_signal< sc_lv<2> > ge_state_load_reg_840_pp0_iter1_reg;
    sc_signal< sc_lv<1> > tmp_5_reg_844_pp0_iter1_reg;
    sc_signal< sc_lv<64> > tmp_data_V_3_fu_184_p1;
    sc_signal< sc_lv<64> > tmp_data_V_3_reg_848;
    sc_signal< sc_lv<64> > tmp_data_V_3_reg_848_pp0_iter1_reg;
    sc_signal< sc_lv<64> > tmp_data_V_3_reg_848_pp0_iter2_reg;
    sc_signal< sc_lv<1> > grp_fu_167_p3;
    sc_signal< sc_lv<1> > tmp_last_V_3_reg_853;
    sc_signal< sc_lv<1> > tmp_last_V_3_reg_853_pp0_iter1_reg;
    sc_signal< sc_lv<1> > tmp_last_V_3_reg_853_pp0_iter2_reg;
    sc_signal< sc_lv<1> > tmp_4_reg_858_pp0_iter1_reg;
    sc_signal< sc_lv<64> > tmp_data_V_fu_194_p1;
    sc_signal< sc_lv<64> > tmp_data_V_reg_862;
    sc_signal< sc_lv<64> > tmp_data_V_reg_862_pp0_iter1_reg;
    sc_signal< sc_lv<64> > tmp_data_V_reg_862_pp0_iter2_reg;
    sc_signal< sc_lv<1> > tmp_last_V_reg_868;
    sc_signal< sc_lv<1> > tmp_last_V_reg_868_pp0_iter1_reg;
    sc_signal< sc_lv<1> > tmp_last_V_reg_868_pp0_iter2_reg;
    sc_signal< sc_lv<1> > tmp_reg_873_pp0_iter1_reg;
    sc_signal< sc_lv<129> > tmp60_reg_877;
    sc_signal< sc_lv<16> > packetHeader_idx_rea_fu_212_p4;
    sc_signal< sc_lv<16> > packetHeader_idx_rea_reg_883;
    sc_signal< sc_lv<112> > p_Val2_s_reg_889;
    sc_signal< sc_lv<23> > add_ln76_fu_260_p2;
    sc_signal< sc_lv<23> > add_ln76_reg_896;
    sc_signal< sc_lv<1> > icmp_ln647_fu_272_p2;
    sc_signal< sc_lv<1> > icmp_ln647_reg_901;
    sc_signal< sc_lv<7> > tmp_10_fu_278_p3;
    sc_signal< sc_lv<7> > tmp_10_reg_908;
    sc_signal< sc_lv<7> > trunc_ln647_fu_286_p1;
    sc_signal< sc_lv<7> > trunc_ln647_reg_915;
    sc_signal< sc_lv<7> > sub_ln647_1_fu_290_p2;
    sc_signal< sc_lv<7> > sub_ln647_1_reg_921;
    sc_signal< sc_lv<1> > icmp_ln82_fu_296_p2;
    sc_signal< sc_lv<1> > icmp_ln82_reg_926;
    sc_signal< sc_lv<1> > icmp_ln647_1_fu_302_p2;
    sc_signal< sc_lv<1> > icmp_ln647_1_reg_931;
    sc_signal< sc_lv<7> > tmp_12_fu_308_p3;
    sc_signal< sc_lv<7> > tmp_12_reg_938;
    sc_signal< sc_lv<7> > sub_ln647_4_fu_316_p2;
    sc_signal< sc_lv<7> > sub_ln647_4_reg_945;
    sc_signal< sc_lv<1> > icmp_ln76_1_fu_358_p2;
    sc_signal< sc_lv<1> > icmp_ln76_1_reg_950;
    sc_signal< sc_lv<1> > icmp_ln76_1_reg_950_pp0_iter2_reg;
    sc_signal< sc_lv<1> > icmp_ln647_2_fu_370_p2;
    sc_signal< sc_lv<1> > icmp_ln647_2_reg_955;
    sc_signal< sc_lv<7> > tmp_15_fu_376_p3;
    sc_signal< sc_lv<7> > tmp_15_reg_962;
    sc_signal< sc_lv<7> > trunc_ln647_1_fu_384_p1;
    sc_signal< sc_lv<7> > trunc_ln647_1_reg_969;
    sc_signal< sc_lv<7> > sub_ln647_8_fu_388_p2;
    sc_signal< sc_lv<7> > sub_ln647_8_reg_975;
    sc_signal< sc_lv<1> > icmp_ln647_3_fu_406_p2;
    sc_signal< sc_lv<1> > icmp_ln647_3_reg_980;
    sc_signal< sc_lv<7> > tmp_17_fu_412_p3;
    sc_signal< sc_lv<7> > tmp_17_reg_987;
    sc_signal< sc_lv<7> > sub_ln647_11_fu_420_p2;
    sc_signal< sc_lv<7> > sub_ln647_11_reg_994;
    sc_signal< sc_lv<1> > and_ln82_1_fu_440_p2;
    sc_signal< sc_lv<1> > and_ln82_1_reg_999;
    sc_signal< sc_lv<1> > and_ln82_1_reg_999_pp0_iter2_reg;
    sc_signal< sc_lv<7> > sub_ln647_3_fu_505_p2;
    sc_signal< sc_lv<7> > sub_ln647_3_reg_1004;
    sc_signal< sc_lv<7> > sub_ln647_3_reg_1004_pp0_iter2_reg;
    sc_signal< sc_lv<112> > lshr_ln647_fu_515_p2;
    sc_signal< sc_lv<112> > lshr_ln647_reg_1009;
    sc_signal< sc_lv<112> > lshr_ln647_reg_1009_pp0_iter2_reg;
    sc_signal< sc_lv<7> > sub_ln647_6_fu_563_p2;
    sc_signal< sc_lv<7> > sub_ln647_6_reg_1014;
    sc_signal< sc_lv<7> > sub_ln647_6_reg_1014_pp0_iter2_reg;
    sc_signal< sc_lv<112> > lshr_ln647_2_fu_573_p2;
    sc_signal< sc_lv<112> > lshr_ln647_2_reg_1019;
    sc_signal< sc_lv<112> > lshr_ln647_2_reg_1019_pp0_iter2_reg;
    sc_signal< sc_lv<1> > and_ln82_fu_592_p2;
    sc_signal< sc_lv<1> > and_ln82_reg_1024;
    sc_signal< sc_lv<1> > and_ln82_reg_1024_pp0_iter2_reg;
    sc_signal< sc_lv<7> > sub_ln647_10_fu_652_p2;
    sc_signal< sc_lv<7> > sub_ln647_10_reg_1029;
    sc_signal< sc_lv<112> > lshr_ln647_4_fu_662_p2;
    sc_signal< sc_lv<112> > lshr_ln647_4_reg_1034;
    sc_signal< sc_lv<7> > sub_ln647_13_fu_707_p2;
    sc_signal< sc_lv<7> > sub_ln647_13_reg_1039;
    sc_signal< sc_lv<112> > lshr_ln647_6_fu_717_p2;
    sc_signal< sc_lv<112> > lshr_ln647_6_reg_1044;
    sc_signal< sc_lv<64> > tmp_data_V_2_fu_776_p3;
    sc_signal< sc_lv<64> > currWord_data_V_fu_832_p3;
    sc_signal< bool > ap_block_pp0_stage0_subdone;
    sc_signal< sc_lv<2> > select_ln250_fu_198_p3;
    sc_signal< sc_lv<16> > select_ln82_2_fu_446_p3;
    sc_signal< sc_lv<16> > select_ln82_fu_597_p3;
    sc_signal< bool > ap_block_pp0_stage0_01001;
    sc_signal< sc_lv<22> > Lo_assign_fu_248_p3;
    sc_signal< sc_lv<23> > zext_ln76_fu_256_p1;
    sc_signal< sc_lv<22> > or_ln78_fu_266_p2;
    sc_signal< sc_lv<1> > tmp_9_fu_240_p3;
    sc_signal< sc_lv<1> > tmp_8_fu_232_p3;
    sc_signal< sc_lv<22> > Lo_assign_1_fu_340_p3;
    sc_signal< sc_lv<23> > zext_ln76_1_fu_348_p1;
    sc_signal< sc_lv<23> > add_ln76_1_fu_352_p2;
    sc_signal< sc_lv<22> > or_ln78_1_fu_364_p2;
    sc_signal< sc_lv<1> > trunc_ln76_1_fu_336_p1;
    sc_signal< sc_lv<1> > trunc_ln76_fu_332_p1;
    sc_signal< sc_lv<16> > add_ln79_1_fu_394_p2;
    sc_signal< sc_lv<1> > icmp_ln82_1_fu_400_p2;
    sc_signal< sc_lv<1> > xor_ln76_1_fu_434_p2;
    sc_signal< sc_lv<16> > select_ln76_1_fu_426_p3;
    sc_signal< sc_lv<7> > sub_ln647_fu_479_p2;
    sc_signal< sc_lv<7> > sub_ln647_2_fu_483_p2;
    sc_signal< sc_lv<112> > tmp_11_fu_470_p4;
    sc_signal< sc_lv<7> > select_ln647_fu_487_p3;
    sc_signal< sc_lv<7> > select_ln647_2_fu_500_p3;
    sc_signal< sc_lv<112> > select_ln647_1_fu_494_p3;
    sc_signal< sc_lv<112> > zext_ln647_fu_511_p1;
    sc_signal< sc_lv<7> > add_ln647_fu_535_p2;
    sc_signal< sc_lv<7> > sub_ln647_5_fu_540_p2;
    sc_signal< sc_lv<112> > tmp_13_fu_526_p4;
    sc_signal< sc_lv<7> > select_ln647_3_fu_545_p3;
    sc_signal< sc_lv<7> > select_ln647_5_fu_558_p3;
    sc_signal< sc_lv<112> > select_ln647_4_fu_552_p3;
    sc_signal< sc_lv<112> > zext_ln647_2_fu_569_p1;
    sc_signal< sc_lv<1> > icmp_ln76_fu_465_p2;
    sc_signal< sc_lv<16> > add_ln79_fu_521_p2;
    sc_signal< sc_lv<1> > xor_ln76_fu_586_p2;
    sc_signal< sc_lv<16> > select_ln76_fu_579_p3;
    sc_signal< sc_lv<7> > sub_ln647_7_fu_625_p2;
    sc_signal< sc_lv<7> > sub_ln647_9_fu_629_p2;
    sc_signal< sc_lv<112> > tmp_16_fu_615_p4;
    sc_signal< sc_lv<7> > select_ln647_6_fu_633_p3;
    sc_signal< sc_lv<7> > select_ln647_8_fu_647_p3;
    sc_signal< sc_lv<112> > select_ln647_7_fu_640_p3;
    sc_signal< sc_lv<112> > zext_ln647_4_fu_658_p1;
    sc_signal< sc_lv<7> > add_ln647_1_fu_678_p2;
    sc_signal< sc_lv<7> > sub_ln647_12_fu_683_p2;
    sc_signal< sc_lv<112> > tmp_18_fu_668_p4;
    sc_signal< sc_lv<7> > select_ln647_9_fu_688_p3;
    sc_signal< sc_lv<7> > select_ln647_11_fu_702_p3;
    sc_signal< sc_lv<112> > select_ln647_10_fu_695_p3;
    sc_signal< sc_lv<112> > zext_ln647_6_fu_713_p1;
    sc_signal< sc_lv<112> > zext_ln647_5_fu_723_p1;
    sc_signal< sc_lv<112> > lshr_ln647_5_fu_726_p2;
    sc_signal< sc_lv<112> > p_Result_11_fu_732_p2;
    sc_signal< sc_lv<112> > zext_ln647_7_fu_741_p1;
    sc_signal< sc_lv<112> > lshr_ln647_7_fu_744_p2;
    sc_signal< sc_lv<112> > p_Result_12_fu_750_p2;
    sc_signal< sc_lv<48> > trunc_ln215_1_fu_755_p1;
    sc_signal< sc_lv<64> > trunc_ln364_1_fu_737_p1;
    sc_signal< sc_lv<64> > p_Result_6_fu_759_p5;
    sc_signal< sc_lv<64> > select_ln76_2_fu_770_p3;
    sc_signal< sc_lv<112> > zext_ln647_1_fu_784_p1;
    sc_signal< sc_lv<112> > lshr_ln647_1_fu_787_p2;
    sc_signal< sc_lv<112> > p_Result_9_fu_793_p2;
    sc_signal< sc_lv<112> > zext_ln647_3_fu_802_p1;
    sc_signal< sc_lv<112> > lshr_ln647_3_fu_805_p2;
    sc_signal< sc_lv<112> > p_Result_10_fu_811_p2;
    sc_signal< sc_lv<48> > trunc_ln215_fu_816_p1;
    sc_signal< sc_lv<64> > p_Result_s_fu_820_p5;
    sc_signal< sc_lv<64> > trunc_ln364_fu_798_p1;
    sc_signal< sc_lv<1> > ap_NS_fsm;
    sc_signal< sc_logic > ap_idle_pp0_0to3;
    sc_signal< sc_logic > ap_reset_idle_pp0;
    sc_signal< sc_logic > ap_enable_pp0;
    sc_signal< bool > ap_condition_525;
    sc_signal< bool > ap_condition_188;
    sc_signal< bool > ap_condition_532;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<1> ap_ST_fsm_pp0_stage0;
    static const sc_lv<32> ap_const_lv32_0;
    static const bool ap_const_boolean_1;
    static const sc_lv<2> ap_const_lv2_2;
    static const sc_lv<1> ap_const_lv1_1;
    static const sc_lv<2> ap_const_lv2_1;
    static const sc_lv<2> ap_const_lv2_0;
    static const sc_lv<2> ap_const_lv2_3;
    static const bool ap_const_boolean_0;
    static const sc_lv<1> ap_const_lv1_0;
    static const sc_lv<8> ap_const_lv8_FF;
    static const sc_lv<32> ap_const_lv32_40;
    static const sc_lv<32> ap_const_lv32_47;
    static const sc_lv<32> ap_const_lv32_48;
    static const sc_lv<32> ap_const_lv32_1;
    static const sc_lv<32> ap_const_lv32_10;
    static const sc_lv<32> ap_const_lv32_11;
    static const sc_lv<32> ap_const_lv32_80;
    static const sc_lv<6> ap_const_lv6_0;
    static const sc_lv<23> ap_const_lv23_40;
    static const sc_lv<22> ap_const_lv22_3F;
    static const sc_lv<7> ap_const_lv7_6F;
    static const sc_lv<22> ap_const_lv22_70;
    static const sc_lv<22> ap_const_lv22_6F;
    static const sc_lv<23> ap_const_lv23_71;
    static const sc_lv<16> ap_const_lv16_1;
    static const sc_lv<7> ap_const_lv7_11;
    static const sc_lv<32> ap_const_lv32_6F;
    static const sc_lv<112> ap_const_lv112_FFFFFFFFFFFFFFFFFFFFFFFFFFFF;
    static const sc_lv<32> ap_const_lv32_2F;
    static const sc_lv<64> ap_const_lv64_0;
    // Thread declarations
    void thread_ap_clk_no_reset_();
    void thread_Lo_assign_1_fu_340_p3();
    void thread_Lo_assign_fu_248_p3();
    void thread_add_ln647_1_fu_678_p2();
    void thread_add_ln647_fu_535_p2();
    void thread_add_ln76_1_fu_352_p2();
    void thread_add_ln76_fu_260_p2();
    void thread_add_ln79_1_fu_394_p2();
    void thread_add_ln79_fu_521_p2();
    void thread_and_ln82_1_fu_440_p2();
    void thread_and_ln82_fu_592_p2();
    void thread_ap_CS_fsm_pp0_stage0();
    void thread_ap_block_pp0_stage0();
    void thread_ap_block_pp0_stage0_01001();
    void thread_ap_block_pp0_stage0_11001();
    void thread_ap_block_pp0_stage0_subdone();
    void thread_ap_block_state1_pp0_stage0_iter0();
    void thread_ap_block_state2_pp0_stage0_iter1();
    void thread_ap_block_state3_pp0_stage0_iter2();
    void thread_ap_block_state4_io();
    void thread_ap_block_state4_pp0_stage0_iter3();
    void thread_ap_block_state5_io();
    void thread_ap_block_state5_pp0_stage0_iter4();
    void thread_ap_condition_188();
    void thread_ap_condition_525();
    void thread_ap_condition_532();
    void thread_ap_done();
    void thread_ap_enable_pp0();
    void thread_ap_enable_reg_pp0_iter0();
    void thread_ap_idle();
    void thread_ap_idle_pp0();
    void thread_ap_idle_pp0_0to3();
    void thread_ap_predicate_op111_write_state4();
    void thread_ap_predicate_op124_write_state4();
    void thread_ap_predicate_op135_write_state4();
    void thread_ap_predicate_op140_write_state5();
    void thread_ap_predicate_op144_write_state5();
    void thread_ap_predicate_op147_write_state5();
    void thread_ap_predicate_op15_read_state1();
    void thread_ap_predicate_op23_read_state1();
    void thread_ap_predicate_op9_read_state1();
    void thread_ap_ready();
    void thread_ap_reset_idle_pp0();
    void thread_currWord_data_V_fu_832_p3();
    void thread_dataOut_V_data_V_1_ack_in();
    void thread_dataOut_V_data_V_1_ack_out();
    void thread_dataOut_V_data_V_1_data_in();
    void thread_dataOut_V_data_V_1_data_out();
    void thread_dataOut_V_data_V_1_load_A();
    void thread_dataOut_V_data_V_1_load_B();
    void thread_dataOut_V_data_V_1_sel();
    void thread_dataOut_V_data_V_1_state_cmp_full();
    void thread_dataOut_V_data_V_1_vld_in();
    void thread_dataOut_V_data_V_1_vld_out();
    void thread_dataOut_V_keep_V_1_ack_in();
    void thread_dataOut_V_keep_V_1_ack_out();
    void thread_dataOut_V_keep_V_1_data_in();
    void thread_dataOut_V_keep_V_1_data_out();
    void thread_dataOut_V_keep_V_1_load_A();
    void thread_dataOut_V_keep_V_1_load_B();
    void thread_dataOut_V_keep_V_1_sel();
    void thread_dataOut_V_keep_V_1_state_cmp_full();
    void thread_dataOut_V_keep_V_1_vld_in();
    void thread_dataOut_V_keep_V_1_vld_out();
    void thread_dataOut_V_last_V_1_ack_in();
    void thread_dataOut_V_last_V_1_ack_out();
    void thread_dataOut_V_last_V_1_data_in();
    void thread_dataOut_V_last_V_1_data_out();
    void thread_dataOut_V_last_V_1_load_A();
    void thread_dataOut_V_last_V_1_load_B();
    void thread_dataOut_V_last_V_1_sel();
    void thread_dataOut_V_last_V_1_state_cmp_full();
    void thread_dataOut_V_last_V_1_vld_in();
    void thread_dataOut_V_last_V_1_vld_out();
    void thread_dataStreamBuffer4_V_blk_n();
    void thread_dataStreamBuffer4_V_read();
    void thread_grp_fu_167_p3();
    void thread_grp_nbreadreq_fu_114_p3();
    void thread_grp_nbreadreq_fu_128_p3();
    void thread_headerFifo_V_blk_n();
    void thread_headerFifo_V_read();
    void thread_icmp_ln647_1_fu_302_p2();
    void thread_icmp_ln647_2_fu_370_p2();
    void thread_icmp_ln647_3_fu_406_p2();
    void thread_icmp_ln647_fu_272_p2();
    void thread_icmp_ln76_1_fu_358_p2();
    void thread_icmp_ln76_fu_465_p2();
    void thread_icmp_ln82_1_fu_400_p2();
    void thread_icmp_ln82_fu_296_p2();
    void thread_lshr_ln647_1_fu_787_p2();
    void thread_lshr_ln647_2_fu_573_p2();
    void thread_lshr_ln647_3_fu_805_p2();
    void thread_lshr_ln647_4_fu_662_p2();
    void thread_lshr_ln647_5_fu_726_p2();
    void thread_lshr_ln647_6_fu_717_p2();
    void thread_lshr_ln647_7_fu_744_p2();
    void thread_lshr_ln647_fu_515_p2();
    void thread_m_axis_ip_TDATA();
    void thread_m_axis_ip_TDATA_blk_n();
    void thread_m_axis_ip_TKEEP();
    void thread_m_axis_ip_TLAST();
    void thread_m_axis_ip_TVALID();
    void thread_or_ln78_1_fu_364_p2();
    void thread_or_ln78_fu_266_p2();
    void thread_p_Result_10_fu_811_p2();
    void thread_p_Result_11_fu_732_p2();
    void thread_p_Result_12_fu_750_p2();
    void thread_p_Result_6_fu_759_p5();
    void thread_p_Result_9_fu_793_p2();
    void thread_p_Result_s_fu_820_p5();
    void thread_packetHeader_idx_rea_fu_212_p4();
    void thread_select_ln250_fu_198_p3();
    void thread_select_ln647_10_fu_695_p3();
    void thread_select_ln647_11_fu_702_p3();
    void thread_select_ln647_1_fu_494_p3();
    void thread_select_ln647_2_fu_500_p3();
    void thread_select_ln647_3_fu_545_p3();
    void thread_select_ln647_4_fu_552_p3();
    void thread_select_ln647_5_fu_558_p3();
    void thread_select_ln647_6_fu_633_p3();
    void thread_select_ln647_7_fu_640_p3();
    void thread_select_ln647_8_fu_647_p3();
    void thread_select_ln647_9_fu_688_p3();
    void thread_select_ln647_fu_487_p3();
    void thread_select_ln76_1_fu_426_p3();
    void thread_select_ln76_2_fu_770_p3();
    void thread_select_ln76_fu_579_p3();
    void thread_select_ln82_2_fu_446_p3();
    void thread_select_ln82_fu_597_p3();
    void thread_sub_ln647_10_fu_652_p2();
    void thread_sub_ln647_11_fu_420_p2();
    void thread_sub_ln647_12_fu_683_p2();
    void thread_sub_ln647_13_fu_707_p2();
    void thread_sub_ln647_1_fu_290_p2();
    void thread_sub_ln647_2_fu_483_p2();
    void thread_sub_ln647_3_fu_505_p2();
    void thread_sub_ln647_4_fu_316_p2();
    void thread_sub_ln647_5_fu_540_p2();
    void thread_sub_ln647_6_fu_563_p2();
    void thread_sub_ln647_7_fu_625_p2();
    void thread_sub_ln647_8_fu_388_p2();
    void thread_sub_ln647_9_fu_629_p2();
    void thread_sub_ln647_fu_479_p2();
    void thread_tmp_10_fu_278_p3();
    void thread_tmp_11_fu_470_p4();
    void thread_tmp_12_fu_308_p3();
    void thread_tmp_13_fu_526_p4();
    void thread_tmp_15_fu_376_p3();
    void thread_tmp_16_fu_615_p4();
    void thread_tmp_17_fu_412_p3();
    void thread_tmp_18_fu_668_p4();
    void thread_tmp_8_fu_232_p3();
    void thread_tmp_9_fu_240_p3();
    void thread_tmp_data_V_2_fu_776_p3();
    void thread_tmp_data_V_3_fu_184_p1();
    void thread_tmp_data_V_fu_194_p1();
    void thread_trunc_ln215_1_fu_755_p1();
    void thread_trunc_ln215_fu_816_p1();
    void thread_trunc_ln364_1_fu_737_p1();
    void thread_trunc_ln364_fu_798_p1();
    void thread_trunc_ln647_1_fu_384_p1();
    void thread_trunc_ln647_fu_286_p1();
    void thread_trunc_ln76_1_fu_336_p1();
    void thread_trunc_ln76_fu_332_p1();
    void thread_xor_ln76_1_fu_434_p2();
    void thread_xor_ln76_fu_586_p2();
    void thread_zext_ln647_1_fu_784_p1();
    void thread_zext_ln647_2_fu_569_p1();
    void thread_zext_ln647_3_fu_802_p1();
    void thread_zext_ln647_4_fu_658_p1();
    void thread_zext_ln647_5_fu_723_p1();
    void thread_zext_ln647_6_fu_713_p1();
    void thread_zext_ln647_7_fu_741_p1();
    void thread_zext_ln647_fu_511_p1();
    void thread_zext_ln76_1_fu_348_p1();
    void thread_zext_ln76_fu_256_p1();
    void thread_ap_NS_fsm();
};

}

using namespace ap_rtl;

#endif
