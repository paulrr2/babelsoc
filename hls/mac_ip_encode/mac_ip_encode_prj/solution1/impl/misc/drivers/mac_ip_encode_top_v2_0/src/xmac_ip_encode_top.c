// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================
/***************************** Include Files *********************************/
#include "xmac_ip_encode_top.h"

/************************** Function Implementation *************************/
#ifndef __linux__
int XMac_ip_encode_top_CfgInitialize(XMac_ip_encode_top *InstancePtr, XMac_ip_encode_top_Config *ConfigPtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(ConfigPtr != NULL);

    InstancePtr->Axilites_BaseAddress = ConfigPtr->Axilites_BaseAddress;
    InstancePtr->IsReady = XIL_COMPONENT_IS_READY;

    return XST_SUCCESS;
}
#endif

void XMac_ip_encode_top_Set_myMacAddress_V(XMac_ip_encode_top *InstancePtr, u64 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XMac_ip_encode_top_WriteReg(InstancePtr->Axilites_BaseAddress, XMAC_IP_ENCODE_TOP_AXILITES_ADDR_MYMACADDRESS_V_DATA, (u32)(Data));
    XMac_ip_encode_top_WriteReg(InstancePtr->Axilites_BaseAddress, XMAC_IP_ENCODE_TOP_AXILITES_ADDR_MYMACADDRESS_V_DATA + 4, (u32)(Data >> 32));
}

u64 XMac_ip_encode_top_Get_myMacAddress_V(XMac_ip_encode_top *InstancePtr) {
    u64 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XMac_ip_encode_top_ReadReg(InstancePtr->Axilites_BaseAddress, XMAC_IP_ENCODE_TOP_AXILITES_ADDR_MYMACADDRESS_V_DATA);
    Data += (u64)XMac_ip_encode_top_ReadReg(InstancePtr->Axilites_BaseAddress, XMAC_IP_ENCODE_TOP_AXILITES_ADDR_MYMACADDRESS_V_DATA + 4) << 32;
    return Data;
}

void XMac_ip_encode_top_Set_regSubNetMask_V(XMac_ip_encode_top *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XMac_ip_encode_top_WriteReg(InstancePtr->Axilites_BaseAddress, XMAC_IP_ENCODE_TOP_AXILITES_ADDR_REGSUBNETMASK_V_DATA, Data);
}

u32 XMac_ip_encode_top_Get_regSubNetMask_V(XMac_ip_encode_top *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XMac_ip_encode_top_ReadReg(InstancePtr->Axilites_BaseAddress, XMAC_IP_ENCODE_TOP_AXILITES_ADDR_REGSUBNETMASK_V_DATA);
    return Data;
}

void XMac_ip_encode_top_Set_regDefaultGateway_V(XMac_ip_encode_top *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XMac_ip_encode_top_WriteReg(InstancePtr->Axilites_BaseAddress, XMAC_IP_ENCODE_TOP_AXILITES_ADDR_REGDEFAULTGATEWAY_V_DATA, Data);
}

u32 XMac_ip_encode_top_Get_regDefaultGateway_V(XMac_ip_encode_top *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XMac_ip_encode_top_ReadReg(InstancePtr->Axilites_BaseAddress, XMAC_IP_ENCODE_TOP_AXILITES_ADDR_REGDEFAULTGATEWAY_V_DATA);
    return Data;
}

