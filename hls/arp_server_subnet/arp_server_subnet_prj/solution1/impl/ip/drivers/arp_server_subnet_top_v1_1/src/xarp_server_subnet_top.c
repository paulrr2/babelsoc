// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================
/***************************** Include Files *********************************/
#include "xarp_server_subnet_top.h"

/************************** Function Implementation *************************/
#ifndef __linux__
int XArp_server_subnet_top_CfgInitialize(XArp_server_subnet_top *InstancePtr, XArp_server_subnet_top_Config *ConfigPtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(ConfigPtr != NULL);

    InstancePtr->Axilites_BaseAddress = ConfigPtr->Axilites_BaseAddress;
    InstancePtr->IsReady = XIL_COMPONENT_IS_READY;

    return XST_SUCCESS;
}
#endif

void XArp_server_subnet_top_Set_myMacAddress_V(XArp_server_subnet_top *InstancePtr, u64 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XArp_server_subnet_top_WriteReg(InstancePtr->Axilites_BaseAddress, XARP_SERVER_SUBNET_TOP_AXILITES_ADDR_MYMACADDRESS_V_DATA, (u32)(Data));
    XArp_server_subnet_top_WriteReg(InstancePtr->Axilites_BaseAddress, XARP_SERVER_SUBNET_TOP_AXILITES_ADDR_MYMACADDRESS_V_DATA + 4, (u32)(Data >> 32));
}

u64 XArp_server_subnet_top_Get_myMacAddress_V(XArp_server_subnet_top *InstancePtr) {
    u64 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XArp_server_subnet_top_ReadReg(InstancePtr->Axilites_BaseAddress, XARP_SERVER_SUBNET_TOP_AXILITES_ADDR_MYMACADDRESS_V_DATA);
    Data += (u64)XArp_server_subnet_top_ReadReg(InstancePtr->Axilites_BaseAddress, XARP_SERVER_SUBNET_TOP_AXILITES_ADDR_MYMACADDRESS_V_DATA + 4) << 32;
    return Data;
}

void XArp_server_subnet_top_Set_myIpAddress_V(XArp_server_subnet_top *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XArp_server_subnet_top_WriteReg(InstancePtr->Axilites_BaseAddress, XARP_SERVER_SUBNET_TOP_AXILITES_ADDR_MYIPADDRESS_V_DATA, Data);
}

u32 XArp_server_subnet_top_Get_myIpAddress_V(XArp_server_subnet_top *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XArp_server_subnet_top_ReadReg(InstancePtr->Axilites_BaseAddress, XARP_SERVER_SUBNET_TOP_AXILITES_ADDR_MYIPADDRESS_V_DATA);
    return Data;
}

