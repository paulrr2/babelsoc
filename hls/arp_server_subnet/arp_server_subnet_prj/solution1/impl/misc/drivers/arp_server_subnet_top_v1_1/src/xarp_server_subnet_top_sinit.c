// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef __linux__

#include "xstatus.h"
#include "xparameters.h"
#include "xarp_server_subnet_top.h"

extern XArp_server_subnet_top_Config XArp_server_subnet_top_ConfigTable[];

XArp_server_subnet_top_Config *XArp_server_subnet_top_LookupConfig(u16 DeviceId) {
	XArp_server_subnet_top_Config *ConfigPtr = NULL;

	int Index;

	for (Index = 0; Index < XPAR_XARP_SERVER_SUBNET_TOP_NUM_INSTANCES; Index++) {
		if (XArp_server_subnet_top_ConfigTable[Index].DeviceId == DeviceId) {
			ConfigPtr = &XArp_server_subnet_top_ConfigTable[Index];
			break;
		}
	}

	return ConfigPtr;
}

int XArp_server_subnet_top_Initialize(XArp_server_subnet_top *InstancePtr, u16 DeviceId) {
	XArp_server_subnet_top_Config *ConfigPtr;

	Xil_AssertNonvoid(InstancePtr != NULL);

	ConfigPtr = XArp_server_subnet_top_LookupConfig(DeviceId);
	if (ConfigPtr == NULL) {
		InstancePtr->IsReady = 0;
		return (XST_DEVICE_NOT_FOUND);
	}

	return XArp_server_subnet_top_CfgInitialize(InstancePtr, ConfigPtr);
}

#endif

