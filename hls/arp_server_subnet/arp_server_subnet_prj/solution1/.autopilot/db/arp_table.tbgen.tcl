set moduleName arp_table
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {arp_table}
set C_modelType { void 0 }
set C_modelArgList {
	{ macIpEncode_req_V_V int 32 regular {axi_s 0 volatile  { macIpEncode_req_V_V Data } }  }
	{ hostIpEncode_req_V_V int 32 regular {axi_s 0 volatile  { hostIpEncode_req_V_V Data } }  }
	{ macIpEncode_rsp_V int 56 regular {axi_s 1 volatile  { macIpEncode_rsp_V Data } }  }
	{ hostIpEncode_rsp_V int 56 regular {axi_s 1 volatile  { hostIpEncode_rsp_V Data } }  }
	{ arpTableInsertFifo_V int 81 regular {fifo 0 volatile } {global 0}  }
	{ arpRequestMetaFifo_V int 32 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "macIpEncode_req_V_V", "interface" : "axis", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "hostIpEncode_req_V_V", "interface" : "axis", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "macIpEncode_rsp_V", "interface" : "axis", "bitwidth" : 56, "direction" : "WRITEONLY"} , 
 	{ "Name" : "hostIpEncode_rsp_V", "interface" : "axis", "bitwidth" : 56, "direction" : "WRITEONLY"} , 
 	{ "Name" : "arpTableInsertFifo_V", "interface" : "fifo", "bitwidth" : 81, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "arpRequestMetaFifo_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 25
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ hostIpEncode_req_V_V_TVALID sc_in sc_logic 1 invld 1 } 
	{ macIpEncode_req_V_V_TVALID sc_in sc_logic 1 invld 0 } 
	{ arpTableInsertFifo_V_dout sc_in sc_lv 81 signal 4 } 
	{ arpTableInsertFifo_V_empty_n sc_in sc_logic 1 signal 4 } 
	{ arpTableInsertFifo_V_read sc_out sc_logic 1 signal 4 } 
	{ hostIpEncode_rsp_V_TREADY sc_in sc_logic 1 outacc 3 } 
	{ macIpEncode_rsp_V_TREADY sc_in sc_logic 1 outacc 2 } 
	{ arpRequestMetaFifo_V_din sc_out sc_lv 32 signal 5 } 
	{ arpRequestMetaFifo_V_full_n sc_in sc_logic 1 signal 5 } 
	{ arpRequestMetaFifo_V_write sc_out sc_logic 1 signal 5 } 
	{ macIpEncode_req_V_V_TDATA sc_in sc_lv 32 signal 0 } 
	{ macIpEncode_req_V_V_TREADY sc_out sc_logic 1 inacc 0 } 
	{ hostIpEncode_req_V_V_TDATA sc_in sc_lv 32 signal 1 } 
	{ hostIpEncode_req_V_V_TREADY sc_out sc_logic 1 inacc 1 } 
	{ macIpEncode_rsp_V_TDATA sc_out sc_lv 56 signal 2 } 
	{ macIpEncode_rsp_V_TVALID sc_out sc_logic 1 outvld 2 } 
	{ hostIpEncode_rsp_V_TDATA sc_out sc_lv 56 signal 3 } 
	{ hostIpEncode_rsp_V_TVALID sc_out sc_logic 1 outvld 3 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "hostIpEncode_req_V_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "hostIpEncode_req_V_V", "role": "TVALID" }} , 
 	{ "name": "macIpEncode_req_V_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "macIpEncode_req_V_V", "role": "TVALID" }} , 
 	{ "name": "arpTableInsertFifo_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":81, "type": "signal", "bundle":{"name": "arpTableInsertFifo_V", "role": "dout" }} , 
 	{ "name": "arpTableInsertFifo_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "arpTableInsertFifo_V", "role": "empty_n" }} , 
 	{ "name": "arpTableInsertFifo_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "arpTableInsertFifo_V", "role": "read" }} , 
 	{ "name": "hostIpEncode_rsp_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "hostIpEncode_rsp_V", "role": "TREADY" }} , 
 	{ "name": "macIpEncode_rsp_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "macIpEncode_rsp_V", "role": "TREADY" }} , 
 	{ "name": "arpRequestMetaFifo_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "arpRequestMetaFifo_V", "role": "din" }} , 
 	{ "name": "arpRequestMetaFifo_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "arpRequestMetaFifo_V", "role": "full_n" }} , 
 	{ "name": "arpRequestMetaFifo_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "arpRequestMetaFifo_V", "role": "write" }} , 
 	{ "name": "macIpEncode_req_V_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "macIpEncode_req_V_V", "role": "TDATA" }} , 
 	{ "name": "macIpEncode_req_V_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "macIpEncode_req_V_V", "role": "TREADY" }} , 
 	{ "name": "hostIpEncode_req_V_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "hostIpEncode_req_V_V", "role": "TDATA" }} , 
 	{ "name": "hostIpEncode_req_V_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "hostIpEncode_req_V_V", "role": "TREADY" }} , 
 	{ "name": "macIpEncode_rsp_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":56, "type": "signal", "bundle":{"name": "macIpEncode_rsp_V", "role": "TDATA" }} , 
 	{ "name": "macIpEncode_rsp_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "macIpEncode_rsp_V", "role": "TVALID" }} , 
 	{ "name": "hostIpEncode_rsp_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":56, "type": "signal", "bundle":{"name": "hostIpEncode_rsp_V", "role": "TDATA" }} , 
 	{ "name": "hostIpEncode_rsp_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "hostIpEncode_rsp_V", "role": "TVALID" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2", "3", "4", "5"],
		"CDFG" : "arp_table",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "3", "EstimateLatencyMin" : "3", "EstimateLatencyMax" : "3",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"DependenceCheck" : [
			{"FromInitialState" : "ap_enable_state2_pp0_iter1_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter1", "FromInitialOperation" : "ap_enable_operation_34", "FromInitialSV" : "1", "FromFinalState" : "ap_enable_state2_pp0_iter1_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter1", "FromFinalOperation" : "ap_enable_operation_34", "FromFinalSV" : "1", "FromAddress" : "arpTable_macAddress_s_address0", "FromType" : "W", "ToInitialState" : "ap_enable_state2_pp0_iter1_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter1", "ToInitialNextIteration" : "ap_enable_reg_pp0_iter2", "ToInitialOperation" : "ap_enable_operation_22", "ToInitialSV" : "1", "ToFinalState" : "ap_enable_state3_pp0_iter2_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter2", "ToFinalOperation" : "ap_enable_operation_37", "ToFinalSV" : "2", "ToAddress" : "arpTable_macAddress_s_address0", "ToType" : "R", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "8", "II" : "1", "Pragma" : "(/home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/arp_server_subnet/arp_server_subnet.cpp:178:1)", "Type" : "RAW"},
			{"FromInitialState" : "ap_enable_state2_pp0_iter1_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter1", "FromInitialOperation" : "ap_enable_operation_34", "FromInitialSV" : "1", "FromFinalState" : "ap_enable_state2_pp0_iter1_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter1", "FromFinalOperation" : "ap_enable_operation_34", "FromFinalSV" : "1", "FromAddress" : "arpTable_macAddress_s_address0", "FromType" : "W", "ToInitialState" : "ap_enable_state2_pp0_iter1_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter1", "ToInitialNextIteration" : "ap_enable_reg_pp0_iter2", "ToInitialOperation" : "ap_enable_operation_27", "ToInitialSV" : "1", "ToFinalState" : "ap_enable_state3_pp0_iter2_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter2", "ToFinalOperation" : "ap_enable_operation_43", "ToFinalSV" : "2", "ToAddress" : "arpTable_macAddress_s_address0", "ToType" : "R", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "8", "II" : "1", "Pragma" : "(/home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/arp_server_subnet/arp_server_subnet.cpp:178:1)", "Type" : "RAW"},
			{"FromInitialState" : "ap_enable_state2_pp0_iter1_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter1", "FromInitialOperation" : "ap_enable_operation_36", "FromInitialSV" : "1", "FromFinalState" : "ap_enable_state2_pp0_iter1_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter1", "FromFinalOperation" : "ap_enable_operation_36", "FromFinalSV" : "1", "FromAddress" : "arpTable_valid_address0", "FromType" : "W", "ToInitialState" : "ap_enable_state2_pp0_iter1_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter1", "ToInitialNextIteration" : "ap_enable_reg_pp0_iter2", "ToInitialOperation" : "ap_enable_operation_24", "ToInitialSV" : "1", "ToFinalState" : "ap_enable_state3_pp0_iter2_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter2", "ToFinalOperation" : "ap_enable_operation_38", "ToFinalSV" : "2", "ToAddress" : "arpTable_valid_address0", "ToType" : "R", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "8", "II" : "1", "Pragma" : "(/home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/arp_server_subnet/arp_server_subnet.cpp:178:1)", "Type" : "RAW"},
			{"FromInitialState" : "ap_enable_state2_pp0_iter1_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter1", "FromInitialOperation" : "ap_enable_operation_36", "FromInitialSV" : "1", "FromFinalState" : "ap_enable_state2_pp0_iter1_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter1", "FromFinalOperation" : "ap_enable_operation_36", "FromFinalSV" : "1", "FromAddress" : "arpTable_valid_address0", "FromType" : "W", "ToInitialState" : "ap_enable_state2_pp0_iter1_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter1", "ToInitialNextIteration" : "ap_enable_reg_pp0_iter2", "ToInitialOperation" : "ap_enable_operation_29", "ToInitialSV" : "1", "ToFinalState" : "ap_enable_state3_pp0_iter2_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter2", "ToFinalOperation" : "ap_enable_operation_44", "ToFinalSV" : "2", "ToAddress" : "arpTable_valid_address0", "ToType" : "R", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "8", "II" : "1", "Pragma" : "(/home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/arp_server_subnet/arp_server_subnet.cpp:178:1)", "Type" : "RAW"},
			{"FromInitialState" : "ap_enable_state2_pp0_iter1_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter1", "FromInitialOperation" : "ap_enable_operation_22", "FromInitialSV" : "1", "FromFinalState" : "ap_enable_state3_pp0_iter2_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter2", "FromFinalOperation" : "ap_enable_operation_37", "FromFinalSV" : "2", "FromAddress" : "arpTable_macAddress_s_address0", "FromType" : "R", "ToInitialState" : "ap_enable_state2_pp0_iter1_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter1", "ToInitialNextIteration" : "ap_enable_reg_pp0_iter2", "ToInitialOperation" : "ap_enable_operation_34", "ToInitialSV" : "1", "ToFinalState" : "ap_enable_state2_pp0_iter1_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter1", "ToFinalOperation" : "ap_enable_operation_34", "ToFinalSV" : "1", "ToAddress" : "arpTable_macAddress_s_address0", "ToType" : "W", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "8", "II" : "1", "Pragma" : "(/home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/arp_server_subnet/arp_server_subnet.cpp:178:1)", "Type" : "WAR", "StateEnableSignalListForFifoShift" : ["ap_enable_state2_pp0_iter1_stage0", "ap_enable_state3_pp0_iter2_stage0"]},
			{"FromInitialState" : "ap_enable_state2_pp0_iter1_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter1", "FromInitialOperation" : "ap_enable_operation_24", "FromInitialSV" : "1", "FromFinalState" : "ap_enable_state3_pp0_iter2_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter2", "FromFinalOperation" : "ap_enable_operation_38", "FromFinalSV" : "2", "FromAddress" : "arpTable_valid_address0", "FromType" : "R", "ToInitialState" : "ap_enable_state2_pp0_iter1_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter1", "ToInitialNextIteration" : "ap_enable_reg_pp0_iter2", "ToInitialOperation" : "ap_enable_operation_36", "ToInitialSV" : "1", "ToFinalState" : "ap_enable_state2_pp0_iter1_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter1", "ToFinalOperation" : "ap_enable_operation_36", "ToFinalSV" : "1", "ToAddress" : "arpTable_valid_address0", "ToType" : "W", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "8", "II" : "1", "Pragma" : "(/home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/arp_server_subnet/arp_server_subnet.cpp:178:1)", "Type" : "WAR", "StateEnableSignalListForFifoShift" : ["ap_enable_state2_pp0_iter1_stage0", "ap_enable_state3_pp0_iter2_stage0"]},
			{"FromInitialState" : "ap_enable_state2_pp0_iter1_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter1", "FromInitialOperation" : "ap_enable_operation_27", "FromInitialSV" : "1", "FromFinalState" : "ap_enable_state3_pp0_iter2_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter2", "FromFinalOperation" : "ap_enable_operation_43", "FromFinalSV" : "2", "FromAddress" : "arpTable_macAddress_s_address0", "FromType" : "R", "ToInitialState" : "ap_enable_state2_pp0_iter1_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter1", "ToInitialNextIteration" : "ap_enable_reg_pp0_iter2", "ToInitialOperation" : "ap_enable_operation_34", "ToInitialSV" : "1", "ToFinalState" : "ap_enable_state2_pp0_iter1_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter1", "ToFinalOperation" : "ap_enable_operation_34", "ToFinalSV" : "1", "ToAddress" : "arpTable_macAddress_s_address0", "ToType" : "W", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "8", "II" : "1", "Pragma" : "(/home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/arp_server_subnet/arp_server_subnet.cpp:178:1)", "Type" : "WAR", "StateEnableSignalListForFifoShift" : ["ap_enable_state2_pp0_iter1_stage0", "ap_enable_state3_pp0_iter2_stage0"]},
			{"FromInitialState" : "ap_enable_state2_pp0_iter1_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter1", "FromInitialOperation" : "ap_enable_operation_29", "FromInitialSV" : "1", "FromFinalState" : "ap_enable_state3_pp0_iter2_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter2", "FromFinalOperation" : "ap_enable_operation_44", "FromFinalSV" : "2", "FromAddress" : "arpTable_valid_address0", "FromType" : "R", "ToInitialState" : "ap_enable_state2_pp0_iter1_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter1", "ToInitialNextIteration" : "ap_enable_reg_pp0_iter2", "ToInitialOperation" : "ap_enable_operation_36", "ToInitialSV" : "1", "ToFinalState" : "ap_enable_state2_pp0_iter1_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter1", "ToFinalOperation" : "ap_enable_operation_36", "ToFinalSV" : "1", "ToAddress" : "arpTable_valid_address0", "ToType" : "W", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "8", "II" : "1", "Pragma" : "(/home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/arp_server_subnet/arp_server_subnet.cpp:178:1)", "Type" : "WAR", "StateEnableSignalListForFifoShift" : ["ap_enable_state2_pp0_iter1_stage0", "ap_enable_state3_pp0_iter2_stage0"]}],
		"Port" : [
			{"Name" : "macIpEncode_req_V_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "macIpEncode_req_V_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "hostIpEncode_req_V_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "hostIpEncode_req_V_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "macIpEncode_rsp_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "macIpEncode_rsp_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "hostIpEncode_rsp_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "hostIpEncode_rsp_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "arpTableInsertFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "arpTableInsertFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "arpTable_ipAddress_V", "Type" : "Memory", "Direction" : "O"},
			{"Name" : "arpTable_macAddress_s", "Type" : "Memory", "Direction" : "IO"},
			{"Name" : "arpTable_valid", "Type" : "Memory", "Direction" : "IO"},
			{"Name" : "arpRequestMetaFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "arpRequestMetaFifo_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.arpTable_ipAddress_V_U", "Parent" : "0"},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.arpTable_macAddress_s_U", "Parent" : "0"},
	{"ID" : "3", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.arpTable_valid_U", "Parent" : "0"},
	{"ID" : "4", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_macIpEncode_rsp_V_U", "Parent" : "0"},
	{"ID" : "5", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_hostIpEncode_rsp_V_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	arp_table {
		macIpEncode_req_V_V {Type I LastRead 0 FirstWrite -1}
		hostIpEncode_req_V_V {Type I LastRead 0 FirstWrite -1}
		macIpEncode_rsp_V {Type O LastRead -1 FirstWrite 2}
		hostIpEncode_rsp_V {Type O LastRead -1 FirstWrite 2}
		arpTableInsertFifo_V {Type I LastRead 0 FirstWrite -1}
		arpTable_ipAddress_V {Type O LastRead -1 FirstWrite -1}
		arpTable_macAddress_s {Type IO LastRead -1 FirstWrite -1}
		arpTable_valid {Type IO LastRead -1 FirstWrite -1}
		arpRequestMetaFifo_V {Type O LastRead -1 FirstWrite 3}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "3", "Max" : "3"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	macIpEncode_req_V_V { axis {  { macIpEncode_req_V_V_TVALID in_vld 0 1 }  { macIpEncode_req_V_V_TDATA in_data 0 32 }  { macIpEncode_req_V_V_TREADY in_acc 1 1 } } }
	hostIpEncode_req_V_V { axis {  { hostIpEncode_req_V_V_TVALID in_vld 0 1 }  { hostIpEncode_req_V_V_TDATA in_data 0 32 }  { hostIpEncode_req_V_V_TREADY in_acc 1 1 } } }
	macIpEncode_rsp_V { axis {  { macIpEncode_rsp_V_TREADY out_acc 0 1 }  { macIpEncode_rsp_V_TDATA out_data 1 56 }  { macIpEncode_rsp_V_TVALID out_vld 1 1 } } }
	hostIpEncode_rsp_V { axis {  { hostIpEncode_rsp_V_TREADY out_acc 0 1 }  { hostIpEncode_rsp_V_TDATA out_data 1 56 }  { hostIpEncode_rsp_V_TVALID out_vld 1 1 } } }
	arpTableInsertFifo_V { ap_fifo {  { arpTableInsertFifo_V_dout fifo_data 0 81 }  { arpTableInsertFifo_V_empty_n fifo_status 0 1 }  { arpTableInsertFifo_V_read fifo_update 1 1 } } }
	arpRequestMetaFifo_V { ap_fifo {  { arpRequestMetaFifo_V_din fifo_data 1 32 }  { arpRequestMetaFifo_V_full_n fifo_status 0 1 }  { arpRequestMetaFifo_V_write fifo_update 1 1 } } }
}
set moduleName arp_table
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {arp_table}
set C_modelType { void 0 }
set C_modelArgList {
	{ macIpEncode_req_V_V int 32 regular {axi_s 0 volatile  { macIpEncode_req_V_V Data } }  }
	{ hostIpEncode_req_V_V int 32 regular {axi_s 0 volatile  { hostIpEncode_req_V_V Data } }  }
	{ macIpEncode_rsp_V int 56 regular {axi_s 1 volatile  { macIpEncode_rsp_V Data } }  }
	{ hostIpEncode_rsp_V int 56 regular {axi_s 1 volatile  { hostIpEncode_rsp_V Data } }  }
	{ arpTableInsertFifo_V int 81 regular {fifo 0 volatile } {global 0}  }
	{ arpRequestMetaFifo_V int 32 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "macIpEncode_req_V_V", "interface" : "axis", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "hostIpEncode_req_V_V", "interface" : "axis", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "macIpEncode_rsp_V", "interface" : "axis", "bitwidth" : 56, "direction" : "WRITEONLY"} , 
 	{ "Name" : "hostIpEncode_rsp_V", "interface" : "axis", "bitwidth" : 56, "direction" : "WRITEONLY"} , 
 	{ "Name" : "arpTableInsertFifo_V", "interface" : "fifo", "bitwidth" : 81, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "arpRequestMetaFifo_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 25
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ hostIpEncode_req_V_V_TVALID sc_in sc_logic 1 invld 1 } 
	{ macIpEncode_req_V_V_TVALID sc_in sc_logic 1 invld 0 } 
	{ arpTableInsertFifo_V_dout sc_in sc_lv 81 signal 4 } 
	{ arpTableInsertFifo_V_empty_n sc_in sc_logic 1 signal 4 } 
	{ arpTableInsertFifo_V_read sc_out sc_logic 1 signal 4 } 
	{ hostIpEncode_rsp_V_TREADY sc_in sc_logic 1 outacc 3 } 
	{ macIpEncode_rsp_V_TREADY sc_in sc_logic 1 outacc 2 } 
	{ arpRequestMetaFifo_V_din sc_out sc_lv 32 signal 5 } 
	{ arpRequestMetaFifo_V_full_n sc_in sc_logic 1 signal 5 } 
	{ arpRequestMetaFifo_V_write sc_out sc_logic 1 signal 5 } 
	{ macIpEncode_req_V_V_TDATA sc_in sc_lv 32 signal 0 } 
	{ macIpEncode_req_V_V_TREADY sc_out sc_logic 1 inacc 0 } 
	{ hostIpEncode_req_V_V_TDATA sc_in sc_lv 32 signal 1 } 
	{ hostIpEncode_req_V_V_TREADY sc_out sc_logic 1 inacc 1 } 
	{ macIpEncode_rsp_V_TDATA sc_out sc_lv 56 signal 2 } 
	{ macIpEncode_rsp_V_TVALID sc_out sc_logic 1 outvld 2 } 
	{ hostIpEncode_rsp_V_TDATA sc_out sc_lv 56 signal 3 } 
	{ hostIpEncode_rsp_V_TVALID sc_out sc_logic 1 outvld 3 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "hostIpEncode_req_V_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "hostIpEncode_req_V_V", "role": "TVALID" }} , 
 	{ "name": "macIpEncode_req_V_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "macIpEncode_req_V_V", "role": "TVALID" }} , 
 	{ "name": "arpTableInsertFifo_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":81, "type": "signal", "bundle":{"name": "arpTableInsertFifo_V", "role": "dout" }} , 
 	{ "name": "arpTableInsertFifo_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "arpTableInsertFifo_V", "role": "empty_n" }} , 
 	{ "name": "arpTableInsertFifo_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "arpTableInsertFifo_V", "role": "read" }} , 
 	{ "name": "hostIpEncode_rsp_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "hostIpEncode_rsp_V", "role": "TREADY" }} , 
 	{ "name": "macIpEncode_rsp_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "macIpEncode_rsp_V", "role": "TREADY" }} , 
 	{ "name": "arpRequestMetaFifo_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "arpRequestMetaFifo_V", "role": "din" }} , 
 	{ "name": "arpRequestMetaFifo_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "arpRequestMetaFifo_V", "role": "full_n" }} , 
 	{ "name": "arpRequestMetaFifo_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "arpRequestMetaFifo_V", "role": "write" }} , 
 	{ "name": "macIpEncode_req_V_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "macIpEncode_req_V_V", "role": "TDATA" }} , 
 	{ "name": "macIpEncode_req_V_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "macIpEncode_req_V_V", "role": "TREADY" }} , 
 	{ "name": "hostIpEncode_req_V_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "hostIpEncode_req_V_V", "role": "TDATA" }} , 
 	{ "name": "hostIpEncode_req_V_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "hostIpEncode_req_V_V", "role": "TREADY" }} , 
 	{ "name": "macIpEncode_rsp_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":56, "type": "signal", "bundle":{"name": "macIpEncode_rsp_V", "role": "TDATA" }} , 
 	{ "name": "macIpEncode_rsp_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "macIpEncode_rsp_V", "role": "TVALID" }} , 
 	{ "name": "hostIpEncode_rsp_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":56, "type": "signal", "bundle":{"name": "hostIpEncode_rsp_V", "role": "TDATA" }} , 
 	{ "name": "hostIpEncode_rsp_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "hostIpEncode_rsp_V", "role": "TVALID" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2", "3"],
		"CDFG" : "arp_table",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "3", "EstimateLatencyMin" : "3", "EstimateLatencyMax" : "3",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"DependenceCheck" : [
			{"FromInitialState" : "ap_enable_state2_pp0_iter1_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter1", "FromInitialOperation" : "ap_enable_operation_34", "FromInitialSV" : "1", "FromFinalState" : "ap_enable_state2_pp0_iter1_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter1", "FromFinalOperation" : "ap_enable_operation_34", "FromFinalSV" : "1", "FromAddress" : "arpTable_macAddress_s_address0", "FromType" : "W", "ToInitialState" : "ap_enable_state2_pp0_iter1_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter1", "ToInitialNextIteration" : "ap_enable_reg_pp0_iter2", "ToInitialOperation" : "ap_enable_operation_22", "ToInitialSV" : "1", "ToFinalState" : "ap_enable_state3_pp0_iter2_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter2", "ToFinalOperation" : "ap_enable_operation_37", "ToFinalSV" : "2", "ToAddress" : "arpTable_macAddress_s_address0", "ToType" : "R", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "8", "II" : "1", "Pragma" : "(/home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/arp_server_subnet/arp_server_subnet.cpp:178:1)", "Type" : "RAW"},
			{"FromInitialState" : "ap_enable_state2_pp0_iter1_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter1", "FromInitialOperation" : "ap_enable_operation_34", "FromInitialSV" : "1", "FromFinalState" : "ap_enable_state2_pp0_iter1_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter1", "FromFinalOperation" : "ap_enable_operation_34", "FromFinalSV" : "1", "FromAddress" : "arpTable_macAddress_s_address0", "FromType" : "W", "ToInitialState" : "ap_enable_state2_pp0_iter1_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter1", "ToInitialNextIteration" : "ap_enable_reg_pp0_iter2", "ToInitialOperation" : "ap_enable_operation_27", "ToInitialSV" : "1", "ToFinalState" : "ap_enable_state3_pp0_iter2_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter2", "ToFinalOperation" : "ap_enable_operation_43", "ToFinalSV" : "2", "ToAddress" : "arpTable_macAddress_s_address0", "ToType" : "R", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "8", "II" : "1", "Pragma" : "(/home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/arp_server_subnet/arp_server_subnet.cpp:178:1)", "Type" : "RAW"},
			{"FromInitialState" : "ap_enable_state2_pp0_iter1_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter1", "FromInitialOperation" : "ap_enable_operation_36", "FromInitialSV" : "1", "FromFinalState" : "ap_enable_state2_pp0_iter1_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter1", "FromFinalOperation" : "ap_enable_operation_36", "FromFinalSV" : "1", "FromAddress" : "arpTable_valid_address0", "FromType" : "W", "ToInitialState" : "ap_enable_state2_pp0_iter1_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter1", "ToInitialNextIteration" : "ap_enable_reg_pp0_iter2", "ToInitialOperation" : "ap_enable_operation_24", "ToInitialSV" : "1", "ToFinalState" : "ap_enable_state3_pp0_iter2_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter2", "ToFinalOperation" : "ap_enable_operation_38", "ToFinalSV" : "2", "ToAddress" : "arpTable_valid_address0", "ToType" : "R", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "8", "II" : "1", "Pragma" : "(/home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/arp_server_subnet/arp_server_subnet.cpp:178:1)", "Type" : "RAW"},
			{"FromInitialState" : "ap_enable_state2_pp0_iter1_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter1", "FromInitialOperation" : "ap_enable_operation_36", "FromInitialSV" : "1", "FromFinalState" : "ap_enable_state2_pp0_iter1_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter1", "FromFinalOperation" : "ap_enable_operation_36", "FromFinalSV" : "1", "FromAddress" : "arpTable_valid_address0", "FromType" : "W", "ToInitialState" : "ap_enable_state2_pp0_iter1_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter1", "ToInitialNextIteration" : "ap_enable_reg_pp0_iter2", "ToInitialOperation" : "ap_enable_operation_29", "ToInitialSV" : "1", "ToFinalState" : "ap_enable_state3_pp0_iter2_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter2", "ToFinalOperation" : "ap_enable_operation_44", "ToFinalSV" : "2", "ToAddress" : "arpTable_valid_address0", "ToType" : "R", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "8", "II" : "1", "Pragma" : "(/home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/arp_server_subnet/arp_server_subnet.cpp:178:1)", "Type" : "RAW"},
			{"FromInitialState" : "ap_enable_state2_pp0_iter1_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter1", "FromInitialOperation" : "ap_enable_operation_22", "FromInitialSV" : "1", "FromFinalState" : "ap_enable_state3_pp0_iter2_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter2", "FromFinalOperation" : "ap_enable_operation_37", "FromFinalSV" : "2", "FromAddress" : "arpTable_macAddress_s_address0", "FromType" : "R", "ToInitialState" : "ap_enable_state2_pp0_iter1_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter1", "ToInitialNextIteration" : "ap_enable_reg_pp0_iter2", "ToInitialOperation" : "ap_enable_operation_34", "ToInitialSV" : "1", "ToFinalState" : "ap_enable_state2_pp0_iter1_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter1", "ToFinalOperation" : "ap_enable_operation_34", "ToFinalSV" : "1", "ToAddress" : "arpTable_macAddress_s_address0", "ToType" : "W", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "8", "II" : "1", "Pragma" : "(/home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/arp_server_subnet/arp_server_subnet.cpp:178:1)", "Type" : "WAR", "StateEnableSignalListForFifoShift" : ["ap_enable_state2_pp0_iter1_stage0", "ap_enable_state3_pp0_iter2_stage0"]},
			{"FromInitialState" : "ap_enable_state2_pp0_iter1_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter1", "FromInitialOperation" : "ap_enable_operation_24", "FromInitialSV" : "1", "FromFinalState" : "ap_enable_state3_pp0_iter2_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter2", "FromFinalOperation" : "ap_enable_operation_38", "FromFinalSV" : "2", "FromAddress" : "arpTable_valid_address0", "FromType" : "R", "ToInitialState" : "ap_enable_state2_pp0_iter1_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter1", "ToInitialNextIteration" : "ap_enable_reg_pp0_iter2", "ToInitialOperation" : "ap_enable_operation_36", "ToInitialSV" : "1", "ToFinalState" : "ap_enable_state2_pp0_iter1_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter1", "ToFinalOperation" : "ap_enable_operation_36", "ToFinalSV" : "1", "ToAddress" : "arpTable_valid_address0", "ToType" : "W", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "8", "II" : "1", "Pragma" : "(/home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/arp_server_subnet/arp_server_subnet.cpp:178:1)", "Type" : "WAR", "StateEnableSignalListForFifoShift" : ["ap_enable_state2_pp0_iter1_stage0", "ap_enable_state3_pp0_iter2_stage0"]},
			{"FromInitialState" : "ap_enable_state2_pp0_iter1_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter1", "FromInitialOperation" : "ap_enable_operation_27", "FromInitialSV" : "1", "FromFinalState" : "ap_enable_state3_pp0_iter2_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter2", "FromFinalOperation" : "ap_enable_operation_43", "FromFinalSV" : "2", "FromAddress" : "arpTable_macAddress_s_address0", "FromType" : "R", "ToInitialState" : "ap_enable_state2_pp0_iter1_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter1", "ToInitialNextIteration" : "ap_enable_reg_pp0_iter2", "ToInitialOperation" : "ap_enable_operation_34", "ToInitialSV" : "1", "ToFinalState" : "ap_enable_state2_pp0_iter1_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter1", "ToFinalOperation" : "ap_enable_operation_34", "ToFinalSV" : "1", "ToAddress" : "arpTable_macAddress_s_address0", "ToType" : "W", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "8", "II" : "1", "Pragma" : "(/home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/arp_server_subnet/arp_server_subnet.cpp:178:1)", "Type" : "WAR", "StateEnableSignalListForFifoShift" : ["ap_enable_state2_pp0_iter1_stage0", "ap_enable_state3_pp0_iter2_stage0"]},
			{"FromInitialState" : "ap_enable_state2_pp0_iter1_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter1", "FromInitialOperation" : "ap_enable_operation_29", "FromInitialSV" : "1", "FromFinalState" : "ap_enable_state3_pp0_iter2_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter2", "FromFinalOperation" : "ap_enable_operation_44", "FromFinalSV" : "2", "FromAddress" : "arpTable_valid_address0", "FromType" : "R", "ToInitialState" : "ap_enable_state2_pp0_iter1_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter1", "ToInitialNextIteration" : "ap_enable_reg_pp0_iter2", "ToInitialOperation" : "ap_enable_operation_36", "ToInitialSV" : "1", "ToFinalState" : "ap_enable_state2_pp0_iter1_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter1", "ToFinalOperation" : "ap_enable_operation_36", "ToFinalSV" : "1", "ToAddress" : "arpTable_valid_address0", "ToType" : "W", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "8", "II" : "1", "Pragma" : "(/home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/arp_server_subnet/arp_server_subnet.cpp:178:1)", "Type" : "WAR", "StateEnableSignalListForFifoShift" : ["ap_enable_state2_pp0_iter1_stage0", "ap_enable_state3_pp0_iter2_stage0"]}],
		"Port" : [
			{"Name" : "macIpEncode_req_V_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "macIpEncode_req_V_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "hostIpEncode_req_V_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "hostIpEncode_req_V_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "macIpEncode_rsp_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "macIpEncode_rsp_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "hostIpEncode_rsp_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "hostIpEncode_rsp_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "arpTableInsertFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "arpTableInsertFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "arpTable_ipAddress_V", "Type" : "Memory", "Direction" : "O"},
			{"Name" : "arpTable_macAddress_s", "Type" : "Memory", "Direction" : "IO"},
			{"Name" : "arpTable_valid", "Type" : "Memory", "Direction" : "IO"},
			{"Name" : "arpRequestMetaFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "arpRequestMetaFifo_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.arpTable_ipAddress_V_U", "Parent" : "0"},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.arpTable_macAddress_s_U", "Parent" : "0"},
	{"ID" : "3", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.arpTable_valid_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	arp_table {
		macIpEncode_req_V_V {Type I LastRead 0 FirstWrite -1}
		hostIpEncode_req_V_V {Type I LastRead 0 FirstWrite -1}
		macIpEncode_rsp_V {Type O LastRead -1 FirstWrite 2}
		hostIpEncode_rsp_V {Type O LastRead -1 FirstWrite 2}
		arpTableInsertFifo_V {Type I LastRead 0 FirstWrite -1}
		arpTable_ipAddress_V {Type O LastRead -1 FirstWrite -1}
		arpTable_macAddress_s {Type IO LastRead -1 FirstWrite -1}
		arpTable_valid {Type IO LastRead -1 FirstWrite -1}
		arpRequestMetaFifo_V {Type O LastRead -1 FirstWrite 3}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "3", "Max" : "3"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	macIpEncode_req_V_V { axis {  { macIpEncode_req_V_V_TVALID in_vld 0 1 }  { macIpEncode_req_V_V_TDATA in_data 0 32 }  { macIpEncode_req_V_V_TREADY in_acc 1 1 } } }
	hostIpEncode_req_V_V { axis {  { hostIpEncode_req_V_V_TVALID in_vld 0 1 }  { hostIpEncode_req_V_V_TDATA in_data 0 32 }  { hostIpEncode_req_V_V_TREADY in_acc 1 1 } } }
	macIpEncode_rsp_V { axis {  { macIpEncode_rsp_V_TREADY out_acc 0 1 }  { macIpEncode_rsp_V_TDATA out_data 1 56 }  { macIpEncode_rsp_V_TVALID out_vld 1 1 } } }
	hostIpEncode_rsp_V { axis {  { hostIpEncode_rsp_V_TREADY out_acc 0 1 }  { hostIpEncode_rsp_V_TDATA out_data 1 56 }  { hostIpEncode_rsp_V_TVALID out_vld 1 1 } } }
	arpTableInsertFifo_V { ap_fifo {  { arpTableInsertFifo_V_dout fifo_data 0 81 }  { arpTableInsertFifo_V_empty_n fifo_status 0 1 }  { arpTableInsertFifo_V_read fifo_update 1 1 } } }
	arpRequestMetaFifo_V { ap_fifo {  { arpRequestMetaFifo_V_din fifo_data 1 32 }  { arpRequestMetaFifo_V_full_n fifo_status 0 1 }  { arpRequestMetaFifo_V_write fifo_update 1 1 } } }
}
