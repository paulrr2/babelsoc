set moduleName process_arp_pkg200
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {process_arp_pkg200}
set C_modelType { void 0 }
set C_modelArgList {
	{ dataIn_V_data_V int 64 regular {axi_s 0 volatile  { s_axis Data } }  }
	{ dataIn_V_keep_V int 8 regular {axi_s 0 volatile  { s_axis Keep } }  }
	{ dataIn_V_last_V int 1 regular {axi_s 0 volatile  { s_axis Last } }  }
	{ myIpAddress_V int 32 regular {fifo 0}  }
	{ regRequestCount_V int 16 regular {pointer 1}  }
	{ regReplyCount_V int 16 regular {pointer 1}  }
	{ myMacAddress_V int 48 regular {fifo 0}  }
	{ myIpAddress_V_out int 32 regular {fifo 1}  }
	{ myMacAddress_V_out int 48 regular {fifo 1}  }
	{ arpReplyMetaFifo_V int 128 regular {fifo 1 volatile } {global 1}  }
	{ arpTableInsertFifo_V int 81 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "dataIn_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "READONLY"} , 
 	{ "Name" : "dataIn_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "dataIn_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY"} , 
 	{ "Name" : "myIpAddress_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "regRequestCount_V", "interface" : "wire", "bitwidth" : 16, "direction" : "WRITEONLY"} , 
 	{ "Name" : "regReplyCount_V", "interface" : "wire", "bitwidth" : 16, "direction" : "WRITEONLY"} , 
 	{ "Name" : "myMacAddress_V", "interface" : "fifo", "bitwidth" : 48, "direction" : "READONLY"} , 
 	{ "Name" : "myIpAddress_V_out", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "myMacAddress_V_out", "interface" : "fifo", "bitwidth" : 48, "direction" : "WRITEONLY"} , 
 	{ "Name" : "arpReplyMetaFifo_V", "interface" : "fifo", "bitwidth" : 128, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "arpTableInsertFifo_V", "interface" : "fifo", "bitwidth" : 81, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 34
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ s_axis_TVALID sc_in sc_logic 1 invld 0 } 
	{ myIpAddress_V_dout sc_in sc_lv 32 signal 3 } 
	{ myIpAddress_V_empty_n sc_in sc_logic 1 signal 3 } 
	{ myIpAddress_V_read sc_out sc_logic 1 signal 3 } 
	{ myMacAddress_V_dout sc_in sc_lv 48 signal 6 } 
	{ myMacAddress_V_empty_n sc_in sc_logic 1 signal 6 } 
	{ myMacAddress_V_read sc_out sc_logic 1 signal 6 } 
	{ myIpAddress_V_out_din sc_out sc_lv 32 signal 7 } 
	{ myIpAddress_V_out_full_n sc_in sc_logic 1 signal 7 } 
	{ myIpAddress_V_out_write sc_out sc_logic 1 signal 7 } 
	{ myMacAddress_V_out_din sc_out sc_lv 48 signal 8 } 
	{ myMacAddress_V_out_full_n sc_in sc_logic 1 signal 8 } 
	{ myMacAddress_V_out_write sc_out sc_logic 1 signal 8 } 
	{ arpTableInsertFifo_V_din sc_out sc_lv 81 signal 10 } 
	{ arpTableInsertFifo_V_full_n sc_in sc_logic 1 signal 10 } 
	{ arpTableInsertFifo_V_write sc_out sc_logic 1 signal 10 } 
	{ arpReplyMetaFifo_V_din sc_out sc_lv 128 signal 9 } 
	{ arpReplyMetaFifo_V_full_n sc_in sc_logic 1 signal 9 } 
	{ arpReplyMetaFifo_V_write sc_out sc_logic 1 signal 9 } 
	{ s_axis_TDATA sc_in sc_lv 64 signal 0 } 
	{ s_axis_TREADY sc_out sc_logic 1 inacc 2 } 
	{ s_axis_TKEEP sc_in sc_lv 8 signal 1 } 
	{ s_axis_TLAST sc_in sc_lv 1 signal 2 } 
	{ regRequestCount_V sc_out sc_lv 16 signal 4 } 
	{ regRequestCount_V_ap_vld sc_out sc_logic 1 outvld 4 } 
	{ regReplyCount_V sc_out sc_lv 16 signal 5 } 
	{ regReplyCount_V_ap_vld sc_out sc_logic 1 outvld 5 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "s_axis_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "dataIn_V_data_V", "role": "default" }} , 
 	{ "name": "myIpAddress_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "myIpAddress_V", "role": "dout" }} , 
 	{ "name": "myIpAddress_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "myIpAddress_V", "role": "empty_n" }} , 
 	{ "name": "myIpAddress_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "myIpAddress_V", "role": "read" }} , 
 	{ "name": "myMacAddress_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":48, "type": "signal", "bundle":{"name": "myMacAddress_V", "role": "dout" }} , 
 	{ "name": "myMacAddress_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "myMacAddress_V", "role": "empty_n" }} , 
 	{ "name": "myMacAddress_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "myMacAddress_V", "role": "read" }} , 
 	{ "name": "myIpAddress_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "myIpAddress_V_out", "role": "din" }} , 
 	{ "name": "myIpAddress_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "myIpAddress_V_out", "role": "full_n" }} , 
 	{ "name": "myIpAddress_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "myIpAddress_V_out", "role": "write" }} , 
 	{ "name": "myMacAddress_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":48, "type": "signal", "bundle":{"name": "myMacAddress_V_out", "role": "din" }} , 
 	{ "name": "myMacAddress_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "myMacAddress_V_out", "role": "full_n" }} , 
 	{ "name": "myMacAddress_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "myMacAddress_V_out", "role": "write" }} , 
 	{ "name": "arpTableInsertFifo_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":81, "type": "signal", "bundle":{"name": "arpTableInsertFifo_V", "role": "din" }} , 
 	{ "name": "arpTableInsertFifo_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "arpTableInsertFifo_V", "role": "full_n" }} , 
 	{ "name": "arpTableInsertFifo_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "arpTableInsertFifo_V", "role": "write" }} , 
 	{ "name": "arpReplyMetaFifo_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":128, "type": "signal", "bundle":{"name": "arpReplyMetaFifo_V", "role": "din" }} , 
 	{ "name": "arpReplyMetaFifo_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "arpReplyMetaFifo_V", "role": "full_n" }} , 
 	{ "name": "arpReplyMetaFifo_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "arpReplyMetaFifo_V", "role": "write" }} , 
 	{ "name": "s_axis_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "dataIn_V_data_V", "role": "default" }} , 
 	{ "name": "s_axis_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "dataIn_V_last_V", "role": "default" }} , 
 	{ "name": "s_axis_TKEEP", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "dataIn_V_keep_V", "role": "default" }} , 
 	{ "name": "s_axis_TLAST", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "dataIn_V_last_V", "role": "default" }} , 
 	{ "name": "regRequestCount_V", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "regRequestCount_V", "role": "default" }} , 
 	{ "name": "regRequestCount_V_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "regRequestCount_V", "role": "ap_vld" }} , 
 	{ "name": "regReplyCount_V", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "regReplyCount_V", "role": "default" }} , 
 	{ "name": "regReplyCount_V_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "regReplyCount_V", "role": "ap_vld" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "process_arp_pkg200",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "3", "EstimateLatencyMin" : "3", "EstimateLatencyMax" : "3",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "dataIn_V_data_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "s_axis_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataIn_V_keep_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "dataIn_V_last_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "myIpAddress_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "myIpAddress_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regRequestCount_V", "Type" : "Vld", "Direction" : "O"},
			{"Name" : "regReplyCount_V", "Type" : "Vld", "Direction" : "O"},
			{"Name" : "myMacAddress_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "myMacAddress_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "myIpAddress_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "myIpAddress_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "myMacAddress_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "myMacAddress_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "header_ready", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_idx", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_header_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "arpReplyMetaFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "arpReplyMetaFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "pag_requestCounter_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "arpTableInsertFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "arpTableInsertFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "pag_replyCounter_V", "Type" : "OVld", "Direction" : "IO"}]}]}


set ArgLastReadFirstWriteLatency {
	process_arp_pkg200 {
		dataIn_V_data_V {Type I LastRead 0 FirstWrite -1}
		dataIn_V_keep_V {Type I LastRead 0 FirstWrite -1}
		dataIn_V_last_V {Type I LastRead 0 FirstWrite -1}
		myIpAddress_V {Type I LastRead 2 FirstWrite -1}
		regRequestCount_V {Type O LastRead -1 FirstWrite 3}
		regReplyCount_V {Type O LastRead -1 FirstWrite 3}
		myMacAddress_V {Type I LastRead 2 FirstWrite -1}
		myIpAddress_V_out {Type O LastRead -1 FirstWrite 2}
		myMacAddress_V_out {Type O LastRead -1 FirstWrite 2}
		header_ready {Type IO LastRead -1 FirstWrite -1}
		header_idx {Type IO LastRead -1 FirstWrite -1}
		header_header_V {Type IO LastRead -1 FirstWrite -1}
		arpReplyMetaFifo_V {Type O LastRead -1 FirstWrite 3}
		pag_requestCounter_V {Type IO LastRead -1 FirstWrite -1}
		arpTableInsertFifo_V {Type O LastRead -1 FirstWrite 3}
		pag_replyCounter_V {Type IO LastRead -1 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "3", "Max" : "3"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	dataIn_V_data_V { axis {  { s_axis_TVALID in_vld 0 1 }  { s_axis_TDATA in_data 0 64 } } }
	dataIn_V_keep_V { axis {  { s_axis_TKEEP in_data 0 8 } } }
	dataIn_V_last_V { axis {  { s_axis_TREADY in_acc 1 1 }  { s_axis_TLAST in_data 0 1 } } }
	myIpAddress_V { ap_fifo {  { myIpAddress_V_dout fifo_data 0 32 }  { myIpAddress_V_empty_n fifo_status 0 1 }  { myIpAddress_V_read fifo_update 1 1 } } }
	regRequestCount_V { ap_vld {  { regRequestCount_V out_data 1 16 }  { regRequestCount_V_ap_vld out_vld 1 1 } } }
	regReplyCount_V { ap_vld {  { regReplyCount_V out_data 1 16 }  { regReplyCount_V_ap_vld out_vld 1 1 } } }
	myMacAddress_V { ap_fifo {  { myMacAddress_V_dout fifo_data 0 48 }  { myMacAddress_V_empty_n fifo_status 0 1 }  { myMacAddress_V_read fifo_update 1 1 } } }
	myIpAddress_V_out { ap_fifo {  { myIpAddress_V_out_din fifo_data 1 32 }  { myIpAddress_V_out_full_n fifo_status 0 1 }  { myIpAddress_V_out_write fifo_update 1 1 } } }
	myMacAddress_V_out { ap_fifo {  { myMacAddress_V_out_din fifo_data 1 48 }  { myMacAddress_V_out_full_n fifo_status 0 1 }  { myMacAddress_V_out_write fifo_update 1 1 } } }
	arpReplyMetaFifo_V { ap_fifo {  { arpReplyMetaFifo_V_din fifo_data 1 128 }  { arpReplyMetaFifo_V_full_n fifo_status 0 1 }  { arpReplyMetaFifo_V_write fifo_update 1 1 } } }
	arpTableInsertFifo_V { ap_fifo {  { arpTableInsertFifo_V_din fifo_data 1 81 }  { arpTableInsertFifo_V_full_n fifo_status 0 1 }  { arpTableInsertFifo_V_write fifo_update 1 1 } } }
}
