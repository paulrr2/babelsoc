// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2019.1
// Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

#ifndef _generate_arp_pkg_64_s_HH_
#define _generate_arp_pkg_64_s_HH_

#include "systemc.h"
#include "AESL_pkg.h"

#include "arp_server_subnet_top_mux_646_64_1_1.h"

namespace ap_rtl {

struct generate_arp_pkg_64_s : public sc_module {
    // Port declarations 24
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_in< sc_logic > ap_start;
    sc_out< sc_logic > ap_done;
    sc_in< sc_logic > ap_continue;
    sc_out< sc_logic > ap_idle;
    sc_out< sc_logic > ap_ready;
    sc_in< sc_lv<48> > myMacAddress_V_dout;
    sc_in< sc_logic > myMacAddress_V_empty_n;
    sc_out< sc_logic > myMacAddress_V_read;
    sc_in< sc_lv<32> > myIpAddress_V_dout;
    sc_in< sc_logic > myIpAddress_V_empty_n;
    sc_out< sc_logic > myIpAddress_V_read;
    sc_in< sc_lv<32> > arpRequestMetaFifo_V_dout;
    sc_in< sc_logic > arpRequestMetaFifo_V_empty_n;
    sc_out< sc_logic > arpRequestMetaFifo_V_read;
    sc_in< sc_lv<128> > arpReplyMetaFifo_V_dout;
    sc_in< sc_logic > arpReplyMetaFifo_V_empty_n;
    sc_out< sc_logic > arpReplyMetaFifo_V_read;
    sc_in< sc_logic > m_axis_TREADY;
    sc_out< sc_lv<64> > m_axis_TDATA;
    sc_out< sc_logic > m_axis_TVALID;
    sc_out< sc_lv<8> > m_axis_TKEEP;
    sc_out< sc_lv<1> > m_axis_TLAST;
    sc_signal< sc_lv<64> > ap_var_for_const0;
    sc_signal< sc_lv<64> > ap_var_for_const1;
    sc_signal< sc_lv<64> > ap_var_for_const2;
    sc_signal< sc_lv<64> > ap_var_for_const3;
    sc_signal< sc_lv<64> > ap_var_for_const4;
    sc_signal< sc_lv<64> > ap_var_for_const5;
    sc_signal< sc_lv<64> > ap_var_for_const6;
    sc_signal< sc_lv<64> > ap_var_for_const7;
    sc_signal< sc_lv<64> > ap_var_for_const8;
    sc_signal< sc_lv<64> > ap_var_for_const9;
    sc_signal< sc_lv<64> > ap_var_for_const10;
    sc_signal< sc_lv<64> > ap_var_for_const11;
    sc_signal< sc_lv<64> > ap_var_for_const12;
    sc_signal< sc_lv<64> > ap_var_for_const13;
    sc_signal< sc_lv<64> > ap_var_for_const14;
    sc_signal< sc_lv<64> > ap_var_for_const15;
    sc_signal< sc_lv<64> > ap_var_for_const16;
    sc_signal< sc_lv<64> > ap_var_for_const17;
    sc_signal< sc_lv<64> > ap_var_for_const18;
    sc_signal< sc_lv<64> > ap_var_for_const19;
    sc_signal< sc_lv<64> > ap_var_for_const20;
    sc_signal< sc_lv<64> > ap_var_for_const21;
    sc_signal< sc_lv<64> > ap_var_for_const22;
    sc_signal< sc_lv<64> > ap_var_for_const23;
    sc_signal< sc_lv<64> > ap_var_for_const24;
    sc_signal< sc_lv<64> > ap_var_for_const25;
    sc_signal< sc_lv<64> > ap_var_for_const26;
    sc_signal< sc_lv<64> > ap_var_for_const27;
    sc_signal< sc_lv<64> > ap_var_for_const28;
    sc_signal< sc_lv<64> > ap_var_for_const29;
    sc_signal< sc_lv<64> > ap_var_for_const30;
    sc_signal< sc_lv<64> > ap_var_for_const31;
    sc_signal< sc_lv<64> > ap_var_for_const32;
    sc_signal< sc_lv<64> > ap_var_for_const33;
    sc_signal< sc_lv<64> > ap_var_for_const34;
    sc_signal< sc_lv<64> > ap_var_for_const35;
    sc_signal< sc_lv<64> > ap_var_for_const36;
    sc_signal< sc_lv<64> > ap_var_for_const37;
    sc_signal< sc_lv<64> > ap_var_for_const38;
    sc_signal< sc_lv<64> > ap_var_for_const39;
    sc_signal< sc_lv<64> > ap_var_for_const40;
    sc_signal< sc_lv<64> > ap_var_for_const41;
    sc_signal< sc_lv<64> > ap_var_for_const42;
    sc_signal< sc_lv<64> > ap_var_for_const43;
    sc_signal< sc_lv<64> > ap_var_for_const44;
    sc_signal< sc_lv<64> > ap_var_for_const45;
    sc_signal< sc_lv<64> > ap_var_for_const46;
    sc_signal< sc_lv<64> > ap_var_for_const47;
    sc_signal< sc_lv<64> > ap_var_for_const48;
    sc_signal< sc_lv<64> > ap_var_for_const49;
    sc_signal< sc_lv<64> > ap_var_for_const50;
    sc_signal< sc_lv<64> > ap_var_for_const51;
    sc_signal< sc_lv<64> > ap_var_for_const52;
    sc_signal< sc_lv<64> > ap_var_for_const53;
    sc_signal< sc_lv<64> > ap_var_for_const54;
    sc_signal< sc_lv<64> > ap_var_for_const55;
    sc_signal< sc_lv<64> > ap_var_for_const56;
    sc_signal< sc_lv<64> > ap_var_for_const57;
    sc_signal< sc_lv<64> > ap_var_for_const58;
    sc_signal< sc_lv<64> > ap_var_for_const59;
    sc_signal< sc_lv<64> > ap_var_for_const60;
    sc_signal< sc_lv<64> > ap_var_for_const61;
    sc_signal< sc_lv<64> > ap_var_for_const62;
    sc_signal< sc_lv<64> > ap_var_for_const63;


    // Module declarations
    generate_arp_pkg_64_s(sc_module_name name);
    SC_HAS_PROCESS(generate_arp_pkg_64_s);

    ~generate_arp_pkg_64_s();

    sc_trace_file* mVcdFile;

    arp_server_subnet_top_mux_646_64_1_1<1,1,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,6,64>* arp_server_subnet_top_mux_646_64_1_1_U16;
    sc_signal< sc_logic > ap_done_reg;
    sc_signal< sc_lv<1> > ap_CS_fsm;
    sc_signal< sc_logic > ap_CS_fsm_pp0_stage0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter1;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter2;
    sc_signal< sc_logic > ap_idle_pp0;
    sc_signal< sc_lv<2> > gap_state_load_load_fu_355_p1;
    sc_signal< sc_lv<1> > tmp_nbreadreq_fu_296_p3;
    sc_signal< sc_lv<1> > tmp_3_nbreadreq_fu_304_p3;
    sc_signal< bool > ap_predicate_op111_read_state1;
    sc_signal< bool > ap_predicate_op119_read_state1;
    sc_signal< bool > ap_block_state1_pp0_stage0_iter0;
    sc_signal< bool > ap_block_state2_pp0_stage0_iter1;
    sc_signal< sc_logic > dataOut_V_data_V_1_ack_in;
    sc_signal< sc_lv<2> > gap_state_load_reg_1359;
    sc_signal< bool > ap_block_state2_io;
    sc_signal< sc_logic > dataOut_V_data_V_1_ack_out;
    sc_signal< sc_lv<2> > dataOut_V_data_V_1_state;
    sc_signal< sc_logic > dataOut_V_keep_V_1_ack_out;
    sc_signal< sc_lv<2> > dataOut_V_keep_V_1_state;
    sc_signal< sc_logic > dataOut_V_last_V_1_ack_out;
    sc_signal< sc_lv<2> > dataOut_V_last_V_1_state;
    sc_signal< bool > ap_block_state3_pp0_stage0_iter2;
    sc_signal< sc_lv<2> > gap_state_load_reg_1359_pp0_iter1_reg;
    sc_signal< bool > ap_block_state3_io;
    sc_signal< bool > ap_block_pp0_stage0_11001;
    sc_signal< sc_lv<64> > dataOut_V_data_V_1_data_in;
    sc_signal< sc_lv<64> > dataOut_V_data_V_1_data_out;
    sc_signal< sc_logic > dataOut_V_data_V_1_vld_in;
    sc_signal< sc_logic > dataOut_V_data_V_1_vld_out;
    sc_signal< sc_lv<64> > dataOut_V_data_V_1_payload_A;
    sc_signal< sc_lv<64> > dataOut_V_data_V_1_payload_B;
    sc_signal< sc_logic > dataOut_V_data_V_1_sel_rd;
    sc_signal< sc_logic > dataOut_V_data_V_1_sel_wr;
    sc_signal< sc_logic > dataOut_V_data_V_1_sel;
    sc_signal< sc_logic > dataOut_V_data_V_1_load_A;
    sc_signal< sc_logic > dataOut_V_data_V_1_load_B;
    sc_signal< sc_logic > dataOut_V_data_V_1_state_cmp_full;
    sc_signal< sc_lv<8> > dataOut_V_keep_V_1_data_in;
    sc_signal< sc_lv<8> > dataOut_V_keep_V_1_data_out;
    sc_signal< sc_logic > dataOut_V_keep_V_1_vld_in;
    sc_signal< sc_logic > dataOut_V_keep_V_1_vld_out;
    sc_signal< sc_logic > dataOut_V_keep_V_1_ack_in;
    sc_signal< sc_lv<8> > dataOut_V_keep_V_1_payload_A;
    sc_signal< sc_lv<8> > dataOut_V_keep_V_1_payload_B;
    sc_signal< sc_logic > dataOut_V_keep_V_1_sel_rd;
    sc_signal< sc_logic > dataOut_V_keep_V_1_sel_wr;
    sc_signal< sc_logic > dataOut_V_keep_V_1_sel;
    sc_signal< sc_logic > dataOut_V_keep_V_1_load_A;
    sc_signal< sc_logic > dataOut_V_keep_V_1_load_B;
    sc_signal< sc_logic > dataOut_V_keep_V_1_state_cmp_full;
    sc_signal< sc_lv<1> > dataOut_V_last_V_1_data_in;
    sc_signal< sc_lv<1> > dataOut_V_last_V_1_data_out;
    sc_signal< sc_logic > dataOut_V_last_V_1_vld_in;
    sc_signal< sc_logic > dataOut_V_last_V_1_vld_out;
    sc_signal< sc_logic > dataOut_V_last_V_1_ack_in;
    sc_signal< sc_lv<1> > dataOut_V_last_V_1_payload_A;
    sc_signal< sc_lv<1> > dataOut_V_last_V_1_payload_B;
    sc_signal< sc_logic > dataOut_V_last_V_1_sel_rd;
    sc_signal< sc_logic > dataOut_V_last_V_1_sel_wr;
    sc_signal< sc_logic > dataOut_V_last_V_1_sel;
    sc_signal< sc_logic > dataOut_V_last_V_1_load_A;
    sc_signal< sc_logic > dataOut_V_last_V_1_load_B;
    sc_signal< sc_logic > dataOut_V_last_V_1_state_cmp_full;
    sc_signal< sc_lv<2> > gap_state;
    sc_signal< sc_lv<16> > header_idx_1;
    sc_signal< sc_lv<336> > header_header_V_1;
    sc_signal< sc_lv<8> > remainingLength_V;
    sc_signal< sc_logic > m_axis_TDATA_blk_n;
    sc_signal< bool > ap_block_pp0_stage0;
    sc_signal< sc_logic > myMacAddress_V_blk_n;
    sc_signal< sc_logic > myIpAddress_V_blk_n;
    sc_signal< sc_logic > arpReplyMetaFifo_V_blk_n;
    sc_signal< sc_logic > arpRequestMetaFifo_V_blk_n;
    sc_signal< sc_lv<1> > icmp_ln76_1_fu_390_p2;
    sc_signal< sc_lv<1> > icmp_ln76_1_reg_1363;
    sc_signal< sc_lv<9> > sub_ln647_10_fu_472_p2;
    sc_signal< sc_lv<9> > sub_ln647_10_reg_1368;
    sc_signal< sc_lv<336> > lshr_ln647_4_fu_482_p2;
    sc_signal< sc_lv<336> > lshr_ln647_4_reg_1373;
    sc_signal< sc_lv<9> > sub_ln647_13_fu_560_p2;
    sc_signal< sc_lv<9> > sub_ln647_13_reg_1378;
    sc_signal< sc_lv<336> > lshr_ln647_6_fu_570_p2;
    sc_signal< sc_lv<336> > lshr_ln647_6_reg_1383;
    sc_signal< sc_lv<1> > and_ln82_1_fu_582_p2;
    sc_signal< sc_lv<1> > and_ln82_1_reg_1388;
    sc_signal< sc_lv<1> > icmp_ln76_fu_642_p2;
    sc_signal< sc_lv<1> > icmp_ln76_reg_1393;
    sc_signal< sc_lv<9> > sub_ln647_3_fu_724_p2;
    sc_signal< sc_lv<9> > sub_ln647_3_reg_1398;
    sc_signal< sc_lv<336> > lshr_ln647_fu_734_p2;
    sc_signal< sc_lv<336> > lshr_ln647_reg_1403;
    sc_signal< sc_lv<9> > sub_ln647_6_fu_878_p2;
    sc_signal< sc_lv<9> > sub_ln647_6_reg_1408;
    sc_signal< sc_lv<336> > lshr_ln647_2_fu_888_p2;
    sc_signal< sc_lv<336> > lshr_ln647_2_reg_1413;
    sc_signal< sc_lv<1> > and_ln82_fu_908_p2;
    sc_signal< sc_lv<1> > and_ln82_reg_1418;
    sc_signal< sc_lv<1> > icmp_ln887_fu_960_p2;
    sc_signal< sc_lv<64> > sendWord_data_V_2_fu_1149_p3;
    sc_signal< sc_lv<8> > sendWord_keep_V_fu_1299_p1;
    sc_signal< sc_lv<64> > sendWord_data_V_fu_1351_p3;
    sc_signal< bool > ap_block_pp0_stage0_subdone;
    sc_signal< sc_lv<16> > select_ln82_3_fu_596_p3;
    sc_signal< sc_lv<16> > select_ln82_1_fu_930_p3;
    sc_signal< sc_lv<336> > p_Result_11_fu_1016_p5;
    sc_signal< sc_lv<336> > p_Result_s_fu_1084_p5;
    sc_signal< sc_lv<8> > select_ln82_fu_914_p3;
    sc_signal< bool > ap_block_pp0_stage0_01001;
    sc_signal< sc_lv<22> > Lo_assign_1_fu_372_p3;
    sc_signal< sc_lv<23> > zext_ln76_1_fu_380_p1;
    sc_signal< sc_lv<23> > add_ln76_1_fu_384_p2;
    sc_signal< sc_lv<22> > or_ln78_1_fu_396_p2;
    sc_signal< sc_lv<3> > trunc_ln76_3_fu_368_p1;
    sc_signal< sc_lv<9> > tmp_13_fu_408_p3;
    sc_signal< sc_lv<9> > trunc_ln647_1_fu_416_p1;
    sc_signal< sc_lv<1> > icmp_ln647_2_fu_402_p2;
    sc_signal< sc_lv<9> > sub_ln647_7_fu_430_p2;
    sc_signal< sc_lv<9> > sub_ln647_9_fu_442_p2;
    sc_signal< sc_lv<336> > tmp_14_fu_420_p4;
    sc_signal< sc_lv<9> > sub_ln647_8_fu_436_p2;
    sc_signal< sc_lv<9> > select_ln647_6_fu_448_p3;
    sc_signal< sc_lv<9> > select_ln647_8_fu_464_p3;
    sc_signal< sc_lv<336> > select_ln647_7_fu_456_p3;
    sc_signal< sc_lv<336> > zext_ln647_4_fu_478_p1;
    sc_signal< sc_lv<3> > trunc_ln76_2_fu_364_p1;
    sc_signal< sc_lv<9> > tmp_15_fu_500_p3;
    sc_signal< sc_lv<1> > icmp_ln647_3_fu_494_p2;
    sc_signal< sc_lv<9> > add_ln647_1_fu_518_p2;
    sc_signal< sc_lv<9> > sub_ln647_12_fu_530_p2;
    sc_signal< sc_lv<336> > tmp_16_fu_508_p4;
    sc_signal< sc_lv<9> > sub_ln647_11_fu_524_p2;
    sc_signal< sc_lv<9> > select_ln647_9_fu_536_p3;
    sc_signal< sc_lv<9> > select_ln647_11_fu_552_p3;
    sc_signal< sc_lv<336> > select_ln647_10_fu_544_p3;
    sc_signal< sc_lv<336> > zext_ln647_6_fu_566_p1;
    sc_signal< sc_lv<1> > icmp_ln82_1_fu_488_p2;
    sc_signal< sc_lv<1> > xor_ln76_1_fu_576_p2;
    sc_signal< sc_lv<16> > grp_fu_344_p2;
    sc_signal< sc_lv<16> > select_ln76_3_fu_588_p3;
    sc_signal< sc_lv<22> > Lo_assign_fu_624_p3;
    sc_signal< sc_lv<23> > zext_ln76_fu_632_p1;
    sc_signal< sc_lv<23> > add_ln76_fu_636_p2;
    sc_signal< sc_lv<22> > or_ln78_fu_648_p2;
    sc_signal< sc_lv<3> > trunc_ln76_1_fu_620_p1;
    sc_signal< sc_lv<9> > tmp_7_fu_660_p3;
    sc_signal< sc_lv<9> > trunc_ln647_fu_668_p1;
    sc_signal< sc_lv<1> > icmp_ln647_fu_654_p2;
    sc_signal< sc_lv<9> > sub_ln647_fu_682_p2;
    sc_signal< sc_lv<9> > sub_ln647_2_fu_694_p2;
    sc_signal< sc_lv<336> > tmp_8_fu_672_p4;
    sc_signal< sc_lv<9> > sub_ln647_1_fu_688_p2;
    sc_signal< sc_lv<9> > select_ln647_fu_700_p3;
    sc_signal< sc_lv<9> > select_ln647_2_fu_716_p3;
    sc_signal< sc_lv<336> > select_ln647_1_fu_708_p3;
    sc_signal< sc_lv<336> > zext_ln647_fu_730_p1;
    sc_signal< sc_lv<22> > shl_ln_fu_740_p3;
    sc_signal< sc_lv<23> > zext_ln80_fu_748_p1;
    sc_signal< sc_lv<23> > sub_ln80_fu_752_p2;
    sc_signal< sc_lv<23> > sub_ln80_1_fu_766_p2;
    sc_signal< sc_lv<8> > trunc_ln80_1_fu_772_p4;
    sc_signal< sc_lv<1> > tmp_9_fu_758_p3;
    sc_signal< sc_lv<8> > sub_ln80_2_fu_782_p2;
    sc_signal< sc_lv<8> > trunc_ln80_2_fu_788_p4;
    sc_signal< sc_lv<3> > trunc_ln76_fu_616_p1;
    sc_signal< sc_lv<9> > tmp_10_fu_818_p3;
    sc_signal< sc_lv<1> > icmp_ln647_1_fu_812_p2;
    sc_signal< sc_lv<9> > add_ln647_fu_836_p2;
    sc_signal< sc_lv<9> > sub_ln647_5_fu_848_p2;
    sc_signal< sc_lv<336> > tmp_11_fu_826_p4;
    sc_signal< sc_lv<9> > sub_ln647_4_fu_842_p2;
    sc_signal< sc_lv<9> > select_ln647_3_fu_854_p3;
    sc_signal< sc_lv<9> > select_ln647_5_fu_870_p3;
    sc_signal< sc_lv<336> > select_ln647_4_fu_862_p3;
    sc_signal< sc_lv<336> > zext_ln647_2_fu_884_p1;
    sc_signal< sc_lv<8> > select_ln80_fu_798_p3;
    sc_signal< sc_lv<1> > icmp_ln82_fu_806_p2;
    sc_signal< sc_lv<1> > xor_ln76_fu_902_p2;
    sc_signal< sc_lv<8> > select_ln76_fu_894_p3;
    sc_signal< sc_lv<16> > select_ln76_1_fu_922_p3;
    sc_signal< sc_lv<5> > tmp_12_fu_950_p4;
    sc_signal< sc_lv<336> > p_Result_14_fu_978_p5;
    sc_signal< sc_lv<336> > p_Result_15_fu_990_p5;
    sc_signal< sc_lv<176> > tmp_6_fu_1002_p6;
    sc_signal< sc_lv<48> > trunc_ln321_fu_1034_p1;
    sc_signal< sc_lv<336> > p_Result_12_fu_1038_p5;
    sc_signal< sc_lv<80> > tmp_4_fu_1062_p4;
    sc_signal< sc_lv<336> > p_Result_13_fu_1050_p5;
    sc_signal< sc_lv<176> > tmp_5_fu_1072_p5;
    sc_signal< sc_lv<336> > zext_ln647_5_fu_1102_p1;
    sc_signal< sc_lv<336> > lshr_ln647_5_fu_1105_p2;
    sc_signal< sc_lv<336> > p_Result_18_fu_1111_p2;
    sc_signal< sc_lv<336> > zext_ln647_7_fu_1120_p1;
    sc_signal< sc_lv<336> > lshr_ln647_7_fu_1123_p2;
    sc_signal< sc_lv<336> > p_Result_19_fu_1129_p2;
    sc_signal< sc_lv<16> > trunc_ln215_1_fu_1134_p1;
    sc_signal< sc_lv<64> > trunc_ln364_1_fu_1116_p1;
    sc_signal< sc_lv<64> > p_Result_6_fu_1138_p1;
    sc_signal< sc_lv<64> > select_ln76_4_fu_1142_p3;
    sc_signal< sc_lv<6> > agg_result_V_i_i_i_fu_1165_p65;
    sc_signal< sc_lv<64> > agg_result_V_i_i_i_fu_1165_p66;
    sc_signal< sc_lv<336> > zext_ln647_1_fu_1304_p1;
    sc_signal< sc_lv<336> > lshr_ln647_1_fu_1307_p2;
    sc_signal< sc_lv<336> > p_Result_16_fu_1313_p2;
    sc_signal< sc_lv<336> > zext_ln647_3_fu_1322_p1;
    sc_signal< sc_lv<336> > lshr_ln647_3_fu_1325_p2;
    sc_signal< sc_lv<336> > p_Result_17_fu_1331_p2;
    sc_signal< sc_lv<16> > trunc_ln215_fu_1336_p1;
    sc_signal< sc_lv<64> > trunc_ln364_fu_1318_p1;
    sc_signal< sc_lv<64> > p_Result_3_fu_1340_p1;
    sc_signal< sc_lv<64> > select_ln76_2_fu_1344_p3;
    sc_signal< sc_lv<1> > ap_NS_fsm;
    sc_signal< sc_logic > ap_idle_pp0_0to1;
    sc_signal< sc_logic > ap_reset_idle_pp0;
    sc_signal< sc_logic > ap_enable_pp0;
    sc_signal< bool > ap_condition_431;
    sc_signal< bool > ap_condition_397;
    sc_signal< bool > ap_condition_148;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<1> ap_ST_fsm_pp0_stage0;
    static const sc_lv<32> ap_const_lv32_0;
    static const bool ap_const_boolean_1;
    static const sc_lv<2> ap_const_lv2_0;
    static const sc_lv<1> ap_const_lv1_0;
    static const sc_lv<1> ap_const_lv1_1;
    static const sc_lv<2> ap_const_lv2_2;
    static const sc_lv<2> ap_const_lv2_1;
    static const sc_lv<2> ap_const_lv2_3;
    static const bool ap_const_boolean_0;
    static const sc_lv<16> ap_const_lv16_0;
    static const sc_lv<8> ap_const_lv8_0;
    static const sc_lv<8> ap_const_lv8_FF;
    static const sc_lv<16> ap_const_lv16_1;
    static const sc_lv<6> ap_const_lv6_0;
    static const sc_lv<23> ap_const_lv23_40;
    static const sc_lv<23> ap_const_lv23_151;
    static const sc_lv<22> ap_const_lv22_3F;
    static const sc_lv<32> ap_const_lv32_14F;
    static const sc_lv<9> ap_const_lv9_14F;
    static const sc_lv<22> ap_const_lv22_150;
    static const sc_lv<22> ap_const_lv22_14F;
    static const sc_lv<9> ap_const_lv9_B1;
    static const sc_lv<23> ap_const_lv23_150;
    static const sc_lv<32> ap_const_lv32_16;
    static const sc_lv<23> ap_const_lv23_0;
    static const sc_lv<32> ap_const_lv32_3;
    static const sc_lv<32> ap_const_lv32_A;
    static const sc_lv<32> ap_const_lv32_7;
    static const sc_lv<5> ap_const_lv5_0;
    static const sc_lv<48> ap_const_lv48_FFFFFFFFFFFF;
    static const sc_lv<32> ap_const_lv32_2F;
    static const sc_lv<32> ap_const_lv32_30;
    static const sc_lv<32> ap_const_lv32_5F;
    static const sc_lv<48> ap_const_lv48_0;
    static const sc_lv<16> ap_const_lv16_100;
    static const sc_lv<32> ap_const_lv32_A0;
    static const sc_lv<32> ap_const_lv32_7F;
    static const sc_lv<16> ap_const_lv16_200;
    static const sc_lv<336> ap_const_lv336_lc_2;
    static const sc_lv<64> ap_const_lv64_0;
    static const sc_lv<64> ap_const_lv64_FFFFFFFFFFFFFFFF;
    static const sc_lv<64> ap_const_lv64_1;
    static const sc_lv<64> ap_const_lv64_3;
    static const sc_lv<64> ap_const_lv64_7;
    static const sc_lv<64> ap_const_lv64_F;
    static const sc_lv<64> ap_const_lv64_1F;
    static const sc_lv<64> ap_const_lv64_3F;
    static const sc_lv<64> ap_const_lv64_7F;
    static const sc_lv<64> ap_const_lv64_FF;
    static const sc_lv<64> ap_const_lv64_1FF;
    static const sc_lv<64> ap_const_lv64_3FF;
    static const sc_lv<64> ap_const_lv64_7FF;
    static const sc_lv<64> ap_const_lv64_FFF;
    static const sc_lv<64> ap_const_lv64_1FFF;
    static const sc_lv<64> ap_const_lv64_3FFF;
    static const sc_lv<64> ap_const_lv64_7FFF;
    static const sc_lv<64> ap_const_lv64_FFFF;
    static const sc_lv<64> ap_const_lv64_1FFFF;
    static const sc_lv<64> ap_const_lv64_3FFFF;
    static const sc_lv<64> ap_const_lv64_7FFFF;
    static const sc_lv<64> ap_const_lv64_FFFFF;
    static const sc_lv<64> ap_const_lv64_1FFFFF;
    static const sc_lv<64> ap_const_lv64_3FFFFF;
    static const sc_lv<64> ap_const_lv64_7FFFFF;
    static const sc_lv<64> ap_const_lv64_FFFFFF;
    static const sc_lv<64> ap_const_lv64_1FFFFFF;
    static const sc_lv<64> ap_const_lv64_3FFFFFF;
    static const sc_lv<64> ap_const_lv64_7FFFFFF;
    static const sc_lv<64> ap_const_lv64_FFFFFFF;
    static const sc_lv<64> ap_const_lv64_1FFFFFFF;
    static const sc_lv<64> ap_const_lv64_3FFFFFFF;
    static const sc_lv<64> ap_const_lv64_7FFFFFFF;
    static const sc_lv<64> ap_const_lv64_FFFFFFFF;
    static const sc_lv<64> ap_const_lv64_1FFFFFFFF;
    static const sc_lv<64> ap_const_lv64_3FFFFFFFF;
    static const sc_lv<64> ap_const_lv64_7FFFFFFFF;
    static const sc_lv<64> ap_const_lv64_FFFFFFFFF;
    static const sc_lv<64> ap_const_lv64_1FFFFFFFFF;
    static const sc_lv<64> ap_const_lv64_3FFFFFFFFF;
    static const sc_lv<64> ap_const_lv64_7FFFFFFFFF;
    static const sc_lv<64> ap_const_lv64_FFFFFFFFFF;
    static const sc_lv<64> ap_const_lv64_1FFFFFFFFFF;
    static const sc_lv<64> ap_const_lv64_3FFFFFFFFFF;
    static const sc_lv<64> ap_const_lv64_7FFFFFFFFFF;
    static const sc_lv<64> ap_const_lv64_FFFFFFFFFFF;
    static const sc_lv<64> ap_const_lv64_1FFFFFFFFFFF;
    static const sc_lv<64> ap_const_lv64_3FFFFFFFFFFF;
    static const sc_lv<64> ap_const_lv64_7FFFFFFFFFFF;
    static const sc_lv<64> ap_const_lv64_FFFFFFFFFFFF;
    static const sc_lv<64> ap_const_lv64_1FFFFFFFFFFFF;
    static const sc_lv<64> ap_const_lv64_3FFFFFFFFFFFF;
    static const sc_lv<64> ap_const_lv64_7FFFFFFFFFFFF;
    static const sc_lv<64> ap_const_lv64_FFFFFFFFFFFFF;
    static const sc_lv<64> ap_const_lv64_1FFFFFFFFFFFFF;
    static const sc_lv<64> ap_const_lv64_3FFFFFFFFFFFFF;
    static const sc_lv<64> ap_const_lv64_7FFFFFFFFFFFFF;
    static const sc_lv<64> ap_const_lv64_FFFFFFFFFFFFFF;
    static const sc_lv<64> ap_const_lv64_1FFFFFFFFFFFFFF;
    static const sc_lv<64> ap_const_lv64_3FFFFFFFFFFFFFF;
    static const sc_lv<64> ap_const_lv64_7FFFFFFFFFFFFFF;
    static const sc_lv<64> ap_const_lv64_FFFFFFFFFFFFFFF;
    static const sc_lv<64> ap_const_lv64_1FFFFFFFFFFFFFFF;
    static const sc_lv<64> ap_const_lv64_3FFFFFFFFFFFFFFF;
    static const sc_lv<64> ap_const_lv64_7FFFFFFFFFFFFFFF;
    // Thread declarations
    void thread_ap_var_for_const0();
    void thread_ap_var_for_const1();
    void thread_ap_var_for_const2();
    void thread_ap_var_for_const3();
    void thread_ap_var_for_const4();
    void thread_ap_var_for_const5();
    void thread_ap_var_for_const6();
    void thread_ap_var_for_const7();
    void thread_ap_var_for_const8();
    void thread_ap_var_for_const9();
    void thread_ap_var_for_const10();
    void thread_ap_var_for_const11();
    void thread_ap_var_for_const12();
    void thread_ap_var_for_const13();
    void thread_ap_var_for_const14();
    void thread_ap_var_for_const15();
    void thread_ap_var_for_const16();
    void thread_ap_var_for_const17();
    void thread_ap_var_for_const18();
    void thread_ap_var_for_const19();
    void thread_ap_var_for_const20();
    void thread_ap_var_for_const21();
    void thread_ap_var_for_const22();
    void thread_ap_var_for_const23();
    void thread_ap_var_for_const24();
    void thread_ap_var_for_const25();
    void thread_ap_var_for_const26();
    void thread_ap_var_for_const27();
    void thread_ap_var_for_const28();
    void thread_ap_var_for_const29();
    void thread_ap_var_for_const30();
    void thread_ap_var_for_const31();
    void thread_ap_var_for_const32();
    void thread_ap_var_for_const33();
    void thread_ap_var_for_const34();
    void thread_ap_var_for_const35();
    void thread_ap_var_for_const36();
    void thread_ap_var_for_const37();
    void thread_ap_var_for_const38();
    void thread_ap_var_for_const39();
    void thread_ap_var_for_const40();
    void thread_ap_var_for_const41();
    void thread_ap_var_for_const42();
    void thread_ap_var_for_const43();
    void thread_ap_var_for_const44();
    void thread_ap_var_for_const45();
    void thread_ap_var_for_const46();
    void thread_ap_var_for_const47();
    void thread_ap_var_for_const48();
    void thread_ap_var_for_const49();
    void thread_ap_var_for_const50();
    void thread_ap_var_for_const51();
    void thread_ap_var_for_const52();
    void thread_ap_var_for_const53();
    void thread_ap_var_for_const54();
    void thread_ap_var_for_const55();
    void thread_ap_var_for_const56();
    void thread_ap_var_for_const57();
    void thread_ap_var_for_const58();
    void thread_ap_var_for_const59();
    void thread_ap_var_for_const60();
    void thread_ap_var_for_const61();
    void thread_ap_var_for_const62();
    void thread_ap_var_for_const63();
    void thread_ap_clk_no_reset_();
    void thread_Lo_assign_1_fu_372_p3();
    void thread_Lo_assign_fu_624_p3();
    void thread_add_ln647_1_fu_518_p2();
    void thread_add_ln647_fu_836_p2();
    void thread_add_ln76_1_fu_384_p2();
    void thread_add_ln76_fu_636_p2();
    void thread_agg_result_V_i_i_i_fu_1165_p65();
    void thread_and_ln82_1_fu_582_p2();
    void thread_and_ln82_fu_908_p2();
    void thread_ap_CS_fsm_pp0_stage0();
    void thread_ap_block_pp0_stage0();
    void thread_ap_block_pp0_stage0_01001();
    void thread_ap_block_pp0_stage0_11001();
    void thread_ap_block_pp0_stage0_subdone();
    void thread_ap_block_state1_pp0_stage0_iter0();
    void thread_ap_block_state2_io();
    void thread_ap_block_state2_pp0_stage0_iter1();
    void thread_ap_block_state3_io();
    void thread_ap_block_state3_pp0_stage0_iter2();
    void thread_ap_condition_148();
    void thread_ap_condition_397();
    void thread_ap_condition_431();
    void thread_ap_done();
    void thread_ap_enable_pp0();
    void thread_ap_enable_reg_pp0_iter0();
    void thread_ap_idle();
    void thread_ap_idle_pp0();
    void thread_ap_idle_pp0_0to1();
    void thread_ap_predicate_op111_read_state1();
    void thread_ap_predicate_op119_read_state1();
    void thread_ap_ready();
    void thread_ap_reset_idle_pp0();
    void thread_arpReplyMetaFifo_V_blk_n();
    void thread_arpReplyMetaFifo_V_read();
    void thread_arpRequestMetaFifo_V_blk_n();
    void thread_arpRequestMetaFifo_V_read();
    void thread_dataOut_V_data_V_1_ack_in();
    void thread_dataOut_V_data_V_1_ack_out();
    void thread_dataOut_V_data_V_1_data_in();
    void thread_dataOut_V_data_V_1_data_out();
    void thread_dataOut_V_data_V_1_load_A();
    void thread_dataOut_V_data_V_1_load_B();
    void thread_dataOut_V_data_V_1_sel();
    void thread_dataOut_V_data_V_1_state_cmp_full();
    void thread_dataOut_V_data_V_1_vld_in();
    void thread_dataOut_V_data_V_1_vld_out();
    void thread_dataOut_V_keep_V_1_ack_in();
    void thread_dataOut_V_keep_V_1_ack_out();
    void thread_dataOut_V_keep_V_1_data_in();
    void thread_dataOut_V_keep_V_1_data_out();
    void thread_dataOut_V_keep_V_1_load_A();
    void thread_dataOut_V_keep_V_1_load_B();
    void thread_dataOut_V_keep_V_1_sel();
    void thread_dataOut_V_keep_V_1_state_cmp_full();
    void thread_dataOut_V_keep_V_1_vld_in();
    void thread_dataOut_V_keep_V_1_vld_out();
    void thread_dataOut_V_last_V_1_ack_in();
    void thread_dataOut_V_last_V_1_ack_out();
    void thread_dataOut_V_last_V_1_data_in();
    void thread_dataOut_V_last_V_1_data_out();
    void thread_dataOut_V_last_V_1_load_A();
    void thread_dataOut_V_last_V_1_load_B();
    void thread_dataOut_V_last_V_1_sel();
    void thread_dataOut_V_last_V_1_state_cmp_full();
    void thread_dataOut_V_last_V_1_vld_in();
    void thread_dataOut_V_last_V_1_vld_out();
    void thread_gap_state_load_load_fu_355_p1();
    void thread_grp_fu_344_p2();
    void thread_icmp_ln647_1_fu_812_p2();
    void thread_icmp_ln647_2_fu_402_p2();
    void thread_icmp_ln647_3_fu_494_p2();
    void thread_icmp_ln647_fu_654_p2();
    void thread_icmp_ln76_1_fu_390_p2();
    void thread_icmp_ln76_fu_642_p2();
    void thread_icmp_ln82_1_fu_488_p2();
    void thread_icmp_ln82_fu_806_p2();
    void thread_icmp_ln887_fu_960_p2();
    void thread_lshr_ln647_1_fu_1307_p2();
    void thread_lshr_ln647_2_fu_888_p2();
    void thread_lshr_ln647_3_fu_1325_p2();
    void thread_lshr_ln647_4_fu_482_p2();
    void thread_lshr_ln647_5_fu_1105_p2();
    void thread_lshr_ln647_6_fu_570_p2();
    void thread_lshr_ln647_7_fu_1123_p2();
    void thread_lshr_ln647_fu_734_p2();
    void thread_m_axis_TDATA();
    void thread_m_axis_TDATA_blk_n();
    void thread_m_axis_TKEEP();
    void thread_m_axis_TLAST();
    void thread_m_axis_TVALID();
    void thread_myIpAddress_V_blk_n();
    void thread_myIpAddress_V_read();
    void thread_myMacAddress_V_blk_n();
    void thread_myMacAddress_V_read();
    void thread_or_ln78_1_fu_396_p2();
    void thread_or_ln78_fu_648_p2();
    void thread_p_Result_11_fu_1016_p5();
    void thread_p_Result_12_fu_1038_p5();
    void thread_p_Result_13_fu_1050_p5();
    void thread_p_Result_14_fu_978_p5();
    void thread_p_Result_15_fu_990_p5();
    void thread_p_Result_16_fu_1313_p2();
    void thread_p_Result_17_fu_1331_p2();
    void thread_p_Result_18_fu_1111_p2();
    void thread_p_Result_19_fu_1129_p2();
    void thread_p_Result_3_fu_1340_p1();
    void thread_p_Result_6_fu_1138_p1();
    void thread_p_Result_s_fu_1084_p5();
    void thread_select_ln647_10_fu_544_p3();
    void thread_select_ln647_11_fu_552_p3();
    void thread_select_ln647_1_fu_708_p3();
    void thread_select_ln647_2_fu_716_p3();
    void thread_select_ln647_3_fu_854_p3();
    void thread_select_ln647_4_fu_862_p3();
    void thread_select_ln647_5_fu_870_p3();
    void thread_select_ln647_6_fu_448_p3();
    void thread_select_ln647_7_fu_456_p3();
    void thread_select_ln647_8_fu_464_p3();
    void thread_select_ln647_9_fu_536_p3();
    void thread_select_ln647_fu_700_p3();
    void thread_select_ln76_1_fu_922_p3();
    void thread_select_ln76_2_fu_1344_p3();
    void thread_select_ln76_3_fu_588_p3();
    void thread_select_ln76_4_fu_1142_p3();
    void thread_select_ln76_fu_894_p3();
    void thread_select_ln80_fu_798_p3();
    void thread_select_ln82_1_fu_930_p3();
    void thread_select_ln82_3_fu_596_p3();
    void thread_select_ln82_fu_914_p3();
    void thread_sendWord_data_V_2_fu_1149_p3();
    void thread_sendWord_data_V_fu_1351_p3();
    void thread_sendWord_keep_V_fu_1299_p1();
    void thread_shl_ln_fu_740_p3();
    void thread_sub_ln647_10_fu_472_p2();
    void thread_sub_ln647_11_fu_524_p2();
    void thread_sub_ln647_12_fu_530_p2();
    void thread_sub_ln647_13_fu_560_p2();
    void thread_sub_ln647_1_fu_688_p2();
    void thread_sub_ln647_2_fu_694_p2();
    void thread_sub_ln647_3_fu_724_p2();
    void thread_sub_ln647_4_fu_842_p2();
    void thread_sub_ln647_5_fu_848_p2();
    void thread_sub_ln647_6_fu_878_p2();
    void thread_sub_ln647_7_fu_430_p2();
    void thread_sub_ln647_8_fu_436_p2();
    void thread_sub_ln647_9_fu_442_p2();
    void thread_sub_ln647_fu_682_p2();
    void thread_sub_ln80_1_fu_766_p2();
    void thread_sub_ln80_2_fu_782_p2();
    void thread_sub_ln80_fu_752_p2();
    void thread_tmp_10_fu_818_p3();
    void thread_tmp_11_fu_826_p4();
    void thread_tmp_12_fu_950_p4();
    void thread_tmp_13_fu_408_p3();
    void thread_tmp_14_fu_420_p4();
    void thread_tmp_15_fu_500_p3();
    void thread_tmp_16_fu_508_p4();
    void thread_tmp_3_nbreadreq_fu_304_p3();
    void thread_tmp_4_fu_1062_p4();
    void thread_tmp_5_fu_1072_p5();
    void thread_tmp_6_fu_1002_p6();
    void thread_tmp_7_fu_660_p3();
    void thread_tmp_8_fu_672_p4();
    void thread_tmp_9_fu_758_p3();
    void thread_tmp_nbreadreq_fu_296_p3();
    void thread_trunc_ln215_1_fu_1134_p1();
    void thread_trunc_ln215_fu_1336_p1();
    void thread_trunc_ln321_fu_1034_p1();
    void thread_trunc_ln364_1_fu_1116_p1();
    void thread_trunc_ln364_fu_1318_p1();
    void thread_trunc_ln647_1_fu_416_p1();
    void thread_trunc_ln647_fu_668_p1();
    void thread_trunc_ln76_1_fu_620_p1();
    void thread_trunc_ln76_2_fu_364_p1();
    void thread_trunc_ln76_3_fu_368_p1();
    void thread_trunc_ln76_fu_616_p1();
    void thread_trunc_ln80_1_fu_772_p4();
    void thread_trunc_ln80_2_fu_788_p4();
    void thread_xor_ln76_1_fu_576_p2();
    void thread_xor_ln76_fu_902_p2();
    void thread_zext_ln647_1_fu_1304_p1();
    void thread_zext_ln647_2_fu_884_p1();
    void thread_zext_ln647_3_fu_1322_p1();
    void thread_zext_ln647_4_fu_478_p1();
    void thread_zext_ln647_5_fu_1102_p1();
    void thread_zext_ln647_6_fu_566_p1();
    void thread_zext_ln647_7_fu_1120_p1();
    void thread_zext_ln647_fu_730_p1();
    void thread_zext_ln76_1_fu_380_p1();
    void thread_zext_ln76_fu_632_p1();
    void thread_zext_ln80_fu_748_p1();
    void thread_ap_NS_fsm();
};

}

using namespace ap_rtl;

#endif
