set moduleName server
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {server}
set C_modelType { void 0 }
set C_modelArgList {
	{ rxMetaData_V_V int 16 regular {axi_s 0 volatile  { rxMetaData_V_V Data } }  }
	{ rxData_V_data_V int 64 regular {axi_s 0 volatile  { s_axis_rx_data Data } }  }
	{ rxData_V_keep_V int 8 regular {axi_s 0 volatile  { s_axis_rx_data Keep } }  }
	{ rxData_V_last_V int 1 regular {axi_s 0 volatile  { s_axis_rx_data Last } }  }
	{ esa_sessionidFifo_V_s int 16 regular {fifo 1 volatile } {global 1}  }
	{ esa_dataFifo_V_data_s int 64 regular {fifo 1 volatile } {global 1}  }
	{ esa_dataFifo_V_keep_s int 8 regular {fifo 1 volatile } {global 1}  }
	{ esa_dataFifo_V_last_s int 1 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "rxMetaData_V_V", "interface" : "axis", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "rxData_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "READONLY"} , 
 	{ "Name" : "rxData_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "rxData_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY"} , 
 	{ "Name" : "esa_sessionidFifo_V_s", "interface" : "fifo", "bitwidth" : 16, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "esa_dataFifo_V_data_s", "interface" : "fifo", "bitwidth" : 64, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "esa_dataFifo_V_keep_s", "interface" : "fifo", "bitwidth" : 8, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "esa_dataFifo_V_last_s", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 27
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ rxMetaData_V_V_TVALID sc_in sc_logic 1 invld 0 } 
	{ s_axis_rx_data_TVALID sc_in sc_logic 1 invld 1 } 
	{ esa_sessionidFifo_V_s_din sc_out sc_lv 16 signal 4 } 
	{ esa_sessionidFifo_V_s_full_n sc_in sc_logic 1 signal 4 } 
	{ esa_sessionidFifo_V_s_write sc_out sc_logic 1 signal 4 } 
	{ esa_dataFifo_V_data_s_din sc_out sc_lv 64 signal 5 } 
	{ esa_dataFifo_V_data_s_full_n sc_in sc_logic 1 signal 5 } 
	{ esa_dataFifo_V_data_s_write sc_out sc_logic 1 signal 5 } 
	{ esa_dataFifo_V_keep_s_din sc_out sc_lv 8 signal 6 } 
	{ esa_dataFifo_V_keep_s_full_n sc_in sc_logic 1 signal 6 } 
	{ esa_dataFifo_V_keep_s_write sc_out sc_logic 1 signal 6 } 
	{ esa_dataFifo_V_last_s_din sc_out sc_lv 1 signal 7 } 
	{ esa_dataFifo_V_last_s_full_n sc_in sc_logic 1 signal 7 } 
	{ esa_dataFifo_V_last_s_write sc_out sc_logic 1 signal 7 } 
	{ rxMetaData_V_V_TDATA sc_in sc_lv 16 signal 0 } 
	{ rxMetaData_V_V_TREADY sc_out sc_logic 1 inacc 0 } 
	{ s_axis_rx_data_TDATA sc_in sc_lv 64 signal 1 } 
	{ s_axis_rx_data_TREADY sc_out sc_logic 1 inacc 3 } 
	{ s_axis_rx_data_TKEEP sc_in sc_lv 8 signal 2 } 
	{ s_axis_rx_data_TLAST sc_in sc_lv 1 signal 3 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "rxMetaData_V_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "rxMetaData_V_V", "role": "TVALID" }} , 
 	{ "name": "s_axis_rx_data_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "rxData_V_data_V", "role": "VALID" }} , 
 	{ "name": "esa_sessionidFifo_V_s_din", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "esa_sessionidFifo_V_s", "role": "din" }} , 
 	{ "name": "esa_sessionidFifo_V_s_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "esa_sessionidFifo_V_s", "role": "full_n" }} , 
 	{ "name": "esa_sessionidFifo_V_s_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "esa_sessionidFifo_V_s", "role": "write" }} , 
 	{ "name": "esa_dataFifo_V_data_s_din", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "esa_dataFifo_V_data_s", "role": "din" }} , 
 	{ "name": "esa_dataFifo_V_data_s_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "esa_dataFifo_V_data_s", "role": "full_n" }} , 
 	{ "name": "esa_dataFifo_V_data_s_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "esa_dataFifo_V_data_s", "role": "write" }} , 
 	{ "name": "esa_dataFifo_V_keep_s_din", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "esa_dataFifo_V_keep_s", "role": "din" }} , 
 	{ "name": "esa_dataFifo_V_keep_s_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "esa_dataFifo_V_keep_s", "role": "full_n" }} , 
 	{ "name": "esa_dataFifo_V_keep_s_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "esa_dataFifo_V_keep_s", "role": "write" }} , 
 	{ "name": "esa_dataFifo_V_last_s_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "esa_dataFifo_V_last_s", "role": "din" }} , 
 	{ "name": "esa_dataFifo_V_last_s_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "esa_dataFifo_V_last_s", "role": "full_n" }} , 
 	{ "name": "esa_dataFifo_V_last_s_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "esa_dataFifo_V_last_s", "role": "write" }} , 
 	{ "name": "rxMetaData_V_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "rxMetaData_V_V", "role": "TDATA" }} , 
 	{ "name": "rxMetaData_V_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "rxMetaData_V_V", "role": "TREADY" }} , 
 	{ "name": "s_axis_rx_data_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "rxData_V_data_V", "role": "DATA" }} , 
 	{ "name": "s_axis_rx_data_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "rxData_V_last_V", "role": "READY" }} , 
 	{ "name": "s_axis_rx_data_TKEEP", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "rxData_V_keep_V", "role": "KEEP" }} , 
 	{ "name": "s_axis_rx_data_TLAST", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "rxData_V_last_V", "role": "LAST" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "server",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "rxMetaData_V_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "rxMetaData_V_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxData_V_data_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "s_axis_rx_data_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxData_V_keep_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "rxData_V_last_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "ksvs_fsmState_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "esa_sessionidFifo_V_s", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "esa_sessionidFifo_V_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "esa_dataFifo_V_data_s", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "esa_dataFifo_V_data_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "esa_dataFifo_V_keep_s", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "esa_dataFifo_V_keep_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "esa_dataFifo_V_last_s", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "esa_dataFifo_V_last_s_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	server {
		rxMetaData_V_V {Type I LastRead 0 FirstWrite -1}
		rxData_V_data_V {Type I LastRead 0 FirstWrite -1}
		rxData_V_keep_V {Type I LastRead 0 FirstWrite -1}
		rxData_V_last_V {Type I LastRead 0 FirstWrite -1}
		ksvs_fsmState_V {Type IO LastRead -1 FirstWrite -1}
		esa_sessionidFifo_V_s {Type O LastRead -1 FirstWrite 1}
		esa_dataFifo_V_data_s {Type O LastRead -1 FirstWrite 1}
		esa_dataFifo_V_keep_s {Type O LastRead -1 FirstWrite 1}
		esa_dataFifo_V_last_s {Type O LastRead -1 FirstWrite 1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "1", "Max" : "1"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	rxMetaData_V_V { axis {  { rxMetaData_V_V_TVALID in_vld 0 1 }  { rxMetaData_V_V_TDATA in_data 0 16 }  { rxMetaData_V_V_TREADY in_acc 1 1 } } }
	rxData_V_data_V { axis {  { s_axis_rx_data_TVALID in_vld 0 1 }  { s_axis_rx_data_TDATA in_data 0 64 } } }
	rxData_V_keep_V { axis {  { s_axis_rx_data_TKEEP in_data 0 8 } } }
	rxData_V_last_V { axis {  { s_axis_rx_data_TREADY in_acc 1 1 }  { s_axis_rx_data_TLAST in_data 0 1 } } }
	esa_sessionidFifo_V_s { ap_fifo {  { esa_sessionidFifo_V_s_din fifo_data 1 16 }  { esa_sessionidFifo_V_s_full_n fifo_status 0 1 }  { esa_sessionidFifo_V_s_write fifo_update 1 1 } } }
	esa_dataFifo_V_data_s { ap_fifo {  { esa_dataFifo_V_data_s_din fifo_data 1 64 }  { esa_dataFifo_V_data_s_full_n fifo_status 0 1 }  { esa_dataFifo_V_data_s_write fifo_update 1 1 } } }
	esa_dataFifo_V_keep_s { ap_fifo {  { esa_dataFifo_V_keep_s_din fifo_data 1 8 }  { esa_dataFifo_V_keep_s_full_n fifo_status 0 1 }  { esa_dataFifo_V_keep_s_write fifo_update 1 1 } } }
	esa_dataFifo_V_last_s { ap_fifo {  { esa_dataFifo_V_last_s_din fifo_data 1 1 }  { esa_dataFifo_V_last_s_full_n fifo_status 0 1 }  { esa_dataFifo_V_last_s_write fifo_update 1 1 } } }
}
