set moduleName dummy
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {dummy}
set C_modelType { void 0 }
set C_modelArgList {
	{ openConnection_V int 48 regular {axi_s 1 volatile  { openConnection_V Data } }  }
	{ openConStatus_V int 24 regular {axi_s 0 volatile  { openConStatus_V Data } }  }
	{ closeConnection_V_V int 16 regular {axi_s 1 volatile  { closeConnection_V_V Data } }  }
	{ txStatus_V int 64 regular {axi_s 0 volatile  { txStatus_V Data } }  }
}
set C_modelArgMapList {[ 
	{ "Name" : "openConnection_V", "interface" : "axis", "bitwidth" : 48, "direction" : "WRITEONLY"} , 
 	{ "Name" : "openConStatus_V", "interface" : "axis", "bitwidth" : 24, "direction" : "READONLY"} , 
 	{ "Name" : "closeConnection_V_V", "interface" : "axis", "bitwidth" : 16, "direction" : "WRITEONLY"} , 
 	{ "Name" : "txStatus_V", "interface" : "axis", "bitwidth" : 64, "direction" : "READONLY"} ]}
# RTL Port declarations: 
set portNum 19
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ openConStatus_V_TVALID sc_in sc_logic 1 invld 1 } 
	{ txStatus_V_TVALID sc_in sc_logic 1 invld 3 } 
	{ openConnection_V_TREADY sc_in sc_logic 1 outacc 0 } 
	{ closeConnection_V_V_TREADY sc_in sc_logic 1 outacc 2 } 
	{ openConnection_V_TDATA sc_out sc_lv 48 signal 0 } 
	{ openConnection_V_TVALID sc_out sc_logic 1 outvld 0 } 
	{ openConStatus_V_TDATA sc_in sc_lv 24 signal 1 } 
	{ openConStatus_V_TREADY sc_out sc_logic 1 inacc 1 } 
	{ closeConnection_V_V_TDATA sc_out sc_lv 16 signal 2 } 
	{ closeConnection_V_V_TVALID sc_out sc_logic 1 outvld 2 } 
	{ txStatus_V_TDATA sc_in sc_lv 64 signal 3 } 
	{ txStatus_V_TREADY sc_out sc_logic 1 inacc 3 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "openConStatus_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "openConStatus_V", "role": "TVALID" }} , 
 	{ "name": "txStatus_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "txStatus_V", "role": "TVALID" }} , 
 	{ "name": "openConnection_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "openConnection_V", "role": "TREADY" }} , 
 	{ "name": "closeConnection_V_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "closeConnection_V_V", "role": "TREADY" }} , 
 	{ "name": "openConnection_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":48, "type": "signal", "bundle":{"name": "openConnection_V", "role": "TDATA" }} , 
 	{ "name": "openConnection_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "openConnection_V", "role": "TVALID" }} , 
 	{ "name": "openConStatus_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":24, "type": "signal", "bundle":{"name": "openConStatus_V", "role": "TDATA" }} , 
 	{ "name": "openConStatus_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "openConStatus_V", "role": "TREADY" }} , 
 	{ "name": "closeConnection_V_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "closeConnection_V_V", "role": "TDATA" }} , 
 	{ "name": "closeConnection_V_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "closeConnection_V_V", "role": "TVALID" }} , 
 	{ "name": "txStatus_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "txStatus_V", "role": "TDATA" }} , 
 	{ "name": "txStatus_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "txStatus_V", "role": "TREADY" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2"],
		"CDFG" : "dummy",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "openConnection_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "openConnection_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "openConStatus_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "openConStatus_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "closeConnection_V_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "closeConnection_V_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txStatus_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "txStatus_V_TDATA_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_openConnection_V_U", "Parent" : "0"},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_closeConnection_V_V_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	dummy {
		openConnection_V {Type O LastRead -1 FirstWrite 1}
		openConStatus_V {Type I LastRead 0 FirstWrite -1}
		closeConnection_V_V {Type O LastRead -1 FirstWrite 1}
		txStatus_V {Type I LastRead 0 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "2", "Max" : "2"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	openConnection_V { axis {  { openConnection_V_TREADY out_acc 0 1 }  { openConnection_V_TDATA out_data 1 48 }  { openConnection_V_TVALID out_vld 1 1 } } }
	openConStatus_V { axis {  { openConStatus_V_TVALID in_vld 0 1 }  { openConStatus_V_TDATA in_data 0 24 }  { openConStatus_V_TREADY in_acc 1 1 } } }
	closeConnection_V_V { axis {  { closeConnection_V_V_TREADY out_acc 0 1 }  { closeConnection_V_V_TDATA out_data 1 16 }  { closeConnection_V_V_TVALID out_vld 1 1 } } }
	txStatus_V { axis {  { txStatus_V_TVALID in_vld 0 1 }  { txStatus_V_TDATA in_data 0 64 }  { txStatus_V_TREADY in_acc 1 1 } } }
}
