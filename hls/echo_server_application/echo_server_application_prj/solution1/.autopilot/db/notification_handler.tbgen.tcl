set moduleName notification_handler
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {notification_handler}
set C_modelType { void 0 }
set C_modelArgList {
	{ notific_V int 88 regular {axi_s 0 volatile  { notific_V Data } }  }
	{ readReq_V int 32 regular {axi_s 1 volatile  { readReq_V Data } }  }
	{ esa_lengthFifo_V_V int 16 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "notific_V", "interface" : "axis", "bitwidth" : 88, "direction" : "READONLY"} , 
 	{ "Name" : "readReq_V", "interface" : "axis", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "esa_lengthFifo_V_V", "interface" : "fifo", "bitwidth" : 16, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 16
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ notific_V_TVALID sc_in sc_logic 1 invld 0 } 
	{ esa_lengthFifo_V_V_din sc_out sc_lv 16 signal 2 } 
	{ esa_lengthFifo_V_V_full_n sc_in sc_logic 1 signal 2 } 
	{ esa_lengthFifo_V_V_write sc_out sc_logic 1 signal 2 } 
	{ readReq_V_TREADY sc_in sc_logic 1 outacc 1 } 
	{ notific_V_TDATA sc_in sc_lv 88 signal 0 } 
	{ notific_V_TREADY sc_out sc_logic 1 inacc 0 } 
	{ readReq_V_TDATA sc_out sc_lv 32 signal 1 } 
	{ readReq_V_TVALID sc_out sc_logic 1 outvld 1 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "notific_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "notific_V", "role": "TVALID" }} , 
 	{ "name": "esa_lengthFifo_V_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "esa_lengthFifo_V_V", "role": "din" }} , 
 	{ "name": "esa_lengthFifo_V_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "esa_lengthFifo_V_V", "role": "full_n" }} , 
 	{ "name": "esa_lengthFifo_V_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "esa_lengthFifo_V_V", "role": "write" }} , 
 	{ "name": "readReq_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "readReq_V", "role": "TREADY" }} , 
 	{ "name": "notific_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":88, "type": "signal", "bundle":{"name": "notific_V", "role": "TDATA" }} , 
 	{ "name": "notific_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "notific_V", "role": "TREADY" }} , 
 	{ "name": "readReq_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "readReq_V", "role": "TDATA" }} , 
 	{ "name": "readReq_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "readReq_V", "role": "TVALID" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1"],
		"CDFG" : "notification_handler",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "notific_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "notific_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "readReq_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "readReq_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "esa_lengthFifo_V_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "esa_lengthFifo_V_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_readReq_V_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	notification_handler {
		notific_V {Type I LastRead 0 FirstWrite -1}
		readReq_V {Type O LastRead -1 FirstWrite 1}
		esa_lengthFifo_V_V {Type O LastRead -1 FirstWrite 1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "2", "Max" : "2"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	notific_V { axis {  { notific_V_TVALID in_vld 0 1 }  { notific_V_TDATA in_data 0 88 }  { notific_V_TREADY in_acc 1 1 } } }
	readReq_V { axis {  { readReq_V_TREADY out_acc 0 1 }  { readReq_V_TDATA out_data 1 32 }  { readReq_V_TVALID out_vld 1 1 } } }
	esa_lengthFifo_V_V { ap_fifo {  { esa_lengthFifo_V_V_din fifo_data 1 16 }  { esa_lengthFifo_V_V_full_n fifo_status 0 1 }  { esa_lengthFifo_V_V_write fifo_update 1 1 } } }
}
