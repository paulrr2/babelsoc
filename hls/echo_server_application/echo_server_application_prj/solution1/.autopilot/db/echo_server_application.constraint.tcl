set clock_constraint { \
    name clk \
    module echo_server_application \
    port ap_clk \
    period 6.4 \
    uncertainty 0.8 \
}

set all_path {}

set false_path {}

