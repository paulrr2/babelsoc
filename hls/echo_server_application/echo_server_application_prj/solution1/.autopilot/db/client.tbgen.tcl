set moduleName client
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 0
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 1
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {client}
set C_modelType { void 0 }
set C_modelArgList {
	{ txMetaData_V int 32 regular {axi_s 1 volatile  { txMetaData_V Data } }  }
	{ txData_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_tx_data Data } }  }
	{ txData_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_tx_data Keep } }  }
	{ txData_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_tx_data Last } }  }
	{ esa_sessionidFifo_V_s int 16 regular {fifo 0 volatile } {global 0}  }
	{ esa_lengthFifo_V_V int 16 regular {fifo 0 volatile } {global 0}  }
	{ esa_dataFifo_V_data_s int 64 regular {fifo 0 volatile } {global 0}  }
	{ esa_dataFifo_V_keep_s int 8 regular {fifo 0 volatile } {global 0}  }
	{ esa_dataFifo_V_last_s int 1 regular {fifo 0 volatile } {global 0}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "txMetaData_V", "interface" : "axis", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "txData_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY"} , 
 	{ "Name" : "txData_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY"} , 
 	{ "Name" : "txData_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY"} , 
 	{ "Name" : "esa_sessionidFifo_V_s", "interface" : "fifo", "bitwidth" : 16, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "esa_lengthFifo_V_V", "interface" : "fifo", "bitwidth" : 16, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "esa_dataFifo_V_data_s", "interface" : "fifo", "bitwidth" : 64, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "esa_dataFifo_V_keep_s", "interface" : "fifo", "bitwidth" : 8, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "esa_dataFifo_V_last_s", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 30
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ txMetaData_V_TDATA sc_out sc_lv 32 signal 0 } 
	{ txMetaData_V_TVALID sc_out sc_logic 1 outvld 0 } 
	{ txMetaData_V_TREADY sc_in sc_logic 1 outacc 0 } 
	{ m_axis_tx_data_TDATA sc_out sc_lv 64 signal 1 } 
	{ m_axis_tx_data_TVALID sc_out sc_logic 1 outvld 3 } 
	{ m_axis_tx_data_TREADY sc_in sc_logic 1 outacc 3 } 
	{ m_axis_tx_data_TKEEP sc_out sc_lv 8 signal 2 } 
	{ m_axis_tx_data_TLAST sc_out sc_lv 1 signal 3 } 
	{ esa_sessionidFifo_V_s_dout sc_in sc_lv 16 signal 4 } 
	{ esa_sessionidFifo_V_s_empty_n sc_in sc_logic 1 signal 4 } 
	{ esa_sessionidFifo_V_s_read sc_out sc_logic 1 signal 4 } 
	{ esa_lengthFifo_V_V_dout sc_in sc_lv 16 signal 5 } 
	{ esa_lengthFifo_V_V_empty_n sc_in sc_logic 1 signal 5 } 
	{ esa_lengthFifo_V_V_read sc_out sc_logic 1 signal 5 } 
	{ esa_dataFifo_V_data_s_dout sc_in sc_lv 64 signal 6 } 
	{ esa_dataFifo_V_data_s_empty_n sc_in sc_logic 1 signal 6 } 
	{ esa_dataFifo_V_data_s_read sc_out sc_logic 1 signal 6 } 
	{ esa_dataFifo_V_keep_s_dout sc_in sc_lv 8 signal 7 } 
	{ esa_dataFifo_V_keep_s_empty_n sc_in sc_logic 1 signal 7 } 
	{ esa_dataFifo_V_keep_s_read sc_out sc_logic 1 signal 7 } 
	{ esa_dataFifo_V_last_s_dout sc_in sc_lv 1 signal 8 } 
	{ esa_dataFifo_V_last_s_empty_n sc_in sc_logic 1 signal 8 } 
	{ esa_dataFifo_V_last_s_read sc_out sc_logic 1 signal 8 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "txMetaData_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "txMetaData_V", "role": "TDATA" }} , 
 	{ "name": "txMetaData_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "txMetaData_V", "role": "TVALID" }} , 
 	{ "name": "txMetaData_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "txMetaData_V", "role": "TREADY" }} , 
 	{ "name": "m_axis_tx_data_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "txData_V_data_V", "role": "DATA" }} , 
 	{ "name": "m_axis_tx_data_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "txData_V_last_V", "role": "VALID" }} , 
 	{ "name": "m_axis_tx_data_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "txData_V_last_V", "role": "READY" }} , 
 	{ "name": "m_axis_tx_data_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "txData_V_keep_V", "role": "KEEP" }} , 
 	{ "name": "m_axis_tx_data_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "txData_V_last_V", "role": "LAST" }} , 
 	{ "name": "esa_sessionidFifo_V_s_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "esa_sessionidFifo_V_s", "role": "dout" }} , 
 	{ "name": "esa_sessionidFifo_V_s_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "esa_sessionidFifo_V_s", "role": "empty_n" }} , 
 	{ "name": "esa_sessionidFifo_V_s_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "esa_sessionidFifo_V_s", "role": "read" }} , 
 	{ "name": "esa_lengthFifo_V_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "esa_lengthFifo_V_V", "role": "dout" }} , 
 	{ "name": "esa_lengthFifo_V_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "esa_lengthFifo_V_V", "role": "empty_n" }} , 
 	{ "name": "esa_lengthFifo_V_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "esa_lengthFifo_V_V", "role": "read" }} , 
 	{ "name": "esa_dataFifo_V_data_s_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "esa_dataFifo_V_data_s", "role": "dout" }} , 
 	{ "name": "esa_dataFifo_V_data_s_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "esa_dataFifo_V_data_s", "role": "empty_n" }} , 
 	{ "name": "esa_dataFifo_V_data_s_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "esa_dataFifo_V_data_s", "role": "read" }} , 
 	{ "name": "esa_dataFifo_V_keep_s_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "esa_dataFifo_V_keep_s", "role": "dout" }} , 
 	{ "name": "esa_dataFifo_V_keep_s_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "esa_dataFifo_V_keep_s", "role": "empty_n" }} , 
 	{ "name": "esa_dataFifo_V_keep_s_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "esa_dataFifo_V_keep_s", "role": "read" }} , 
 	{ "name": "esa_dataFifo_V_last_s_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "esa_dataFifo_V_last_s", "role": "dout" }} , 
 	{ "name": "esa_dataFifo_V_last_s_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "esa_dataFifo_V_last_s", "role": "empty_n" }} , 
 	{ "name": "esa_dataFifo_V_last_s_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "esa_dataFifo_V_last_s", "role": "read" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "client",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "0", "EstimateLatencyMin" : "0", "EstimateLatencyMax" : "0",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "txMetaData_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "txMetaData_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txData_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_tx_data_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txData_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "txData_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "esac_fsmState_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "esa_sessionidFifo_V_s", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "esa_sessionidFifo_V_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "esa_lengthFifo_V_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "esa_lengthFifo_V_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "esa_dataFifo_V_data_s", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "esa_dataFifo_V_data_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "esa_dataFifo_V_keep_s", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "esa_dataFifo_V_keep_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "esa_dataFifo_V_last_s", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "esa_dataFifo_V_last_s_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	client {
		txMetaData_V {Type O LastRead 0 FirstWrite 0}
		txData_V_data_V {Type O LastRead 0 FirstWrite 0}
		txData_V_keep_V {Type O LastRead 0 FirstWrite 0}
		txData_V_last_V {Type O LastRead 0 FirstWrite 0}
		esac_fsmState_V {Type IO LastRead -1 FirstWrite -1}
		esa_sessionidFifo_V_s {Type I LastRead 0 FirstWrite -1}
		esa_lengthFifo_V_V {Type I LastRead 0 FirstWrite -1}
		esa_dataFifo_V_data_s {Type I LastRead 0 FirstWrite -1}
		esa_dataFifo_V_keep_s {Type I LastRead 0 FirstWrite -1}
		esa_dataFifo_V_last_s {Type I LastRead 0 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "0", "Max" : "0"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
]}

set Spec2ImplPortList { 
	txMetaData_V { axis {  { txMetaData_V_TDATA out_data 1 32 }  { txMetaData_V_TVALID out_vld 1 1 }  { txMetaData_V_TREADY out_acc 0 1 } } }
	txData_V_data_V { axis {  { m_axis_tx_data_TDATA out_data 1 64 } } }
	txData_V_keep_V { axis {  { m_axis_tx_data_TKEEP out_data 1 8 } } }
	txData_V_last_V { axis {  { m_axis_tx_data_TVALID out_vld 1 1 }  { m_axis_tx_data_TREADY out_acc 0 1 }  { m_axis_tx_data_TLAST out_data 1 1 } } }
	esa_sessionidFifo_V_s { ap_fifo {  { esa_sessionidFifo_V_s_dout fifo_data 0 16 }  { esa_sessionidFifo_V_s_empty_n fifo_status 0 1 }  { esa_sessionidFifo_V_s_read fifo_update 1 1 } } }
	esa_lengthFifo_V_V { ap_fifo {  { esa_lengthFifo_V_V_dout fifo_data 0 16 }  { esa_lengthFifo_V_V_empty_n fifo_status 0 1 }  { esa_lengthFifo_V_V_read fifo_update 1 1 } } }
	esa_dataFifo_V_data_s { ap_fifo {  { esa_dataFifo_V_data_s_dout fifo_data 0 64 }  { esa_dataFifo_V_data_s_empty_n fifo_status 0 1 }  { esa_dataFifo_V_data_s_read fifo_update 1 1 } } }
	esa_dataFifo_V_keep_s { ap_fifo {  { esa_dataFifo_V_keep_s_dout fifo_data 0 8 }  { esa_dataFifo_V_keep_s_empty_n fifo_status 0 1 }  { esa_dataFifo_V_keep_s_read fifo_update 1 1 } } }
	esa_dataFifo_V_last_s { ap_fifo {  { esa_dataFifo_V_last_s_dout fifo_data 0 1 }  { esa_dataFifo_V_last_s_empty_n fifo_status 0 1 }  { esa_dataFifo_V_last_s_read fifo_update 1 1 } } }
}
