set moduleName echo_server_application
set isTopModule 1
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type dataflow
set FunctionProtocol ap_ctrl_none
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {echo_server_application}
set C_modelType { void 0 }
set C_modelArgList {
	{ m_axis_listen_port_V_V int 16 regular {axi_s 1 volatile  { m_axis_listen_port_V_V Data } }  }
	{ s_axis_listen_port_status_V int 8 regular {axi_s 0 volatile  { s_axis_listen_port_status_V Data } }  }
	{ s_axis_notifications_V int 88 regular {axi_s 0 volatile  { s_axis_notifications_V Data } }  }
	{ m_axis_read_package_V int 32 regular {axi_s 1 volatile  { m_axis_read_package_V Data } }  }
	{ s_axis_rx_metadata_V_V int 16 regular {axi_s 0 volatile  { s_axis_rx_metadata_V_V Data } }  }
	{ s_axis_rx_data_V_data_V int 64 regular {axi_s 0 volatile  { s_axis_rx_data Data } }  }
	{ s_axis_rx_data_V_keep_V int 8 regular {axi_s 0 volatile  { s_axis_rx_data Keep } }  }
	{ s_axis_rx_data_V_last_V int 1 regular {axi_s 0 volatile  { s_axis_rx_data Last } }  }
	{ m_axis_open_connection_V int 48 regular {axi_s 1 volatile  { m_axis_open_connection_V Data } }  }
	{ s_axis_open_status_V int 24 regular {axi_s 0 volatile  { s_axis_open_status_V Data } }  }
	{ m_axis_close_connection_V_V int 16 regular {axi_s 1 volatile  { m_axis_close_connection_V_V Data } }  }
	{ m_axis_tx_metadata_V int 32 regular {axi_s 1 volatile  { m_axis_tx_metadata_V Data } }  }
	{ m_axis_tx_data_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_tx_data Data } }  }
	{ m_axis_tx_data_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_tx_data Keep } }  }
	{ m_axis_tx_data_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_tx_data Last } }  }
	{ s_axis_tx_status_V int 64 regular {axi_s 0 volatile  { s_axis_tx_status_V Data } }  }
}
set C_modelArgMapList {[ 
	{ "Name" : "m_axis_listen_port_V_V", "interface" : "axis", "bitwidth" : 16, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":15,"cElement": [{"cName": "m_axis_listen_port.V.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_listen_port_status_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "s_axis_listen_port_status.V","cData": "bool","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_notifications_V", "interface" : "axis", "bitwidth" : 88, "direction" : "READONLY", "bitSlice":[{"low":0,"up":15,"cElement": [{"cName": "s_axis_notifications.V.sessionID.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":16,"up":31,"cElement": [{"cName": "s_axis_notifications.V.length.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":32,"up":63,"cElement": [{"cName": "s_axis_notifications.V.ipAddress.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":64,"up":79,"cElement": [{"cName": "s_axis_notifications.V.dstPort.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":80,"up":80,"cElement": [{"cName": "s_axis_notifications.V.closed","cData": "bool","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_read_package_V", "interface" : "axis", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":15,"cElement": [{"cName": "m_axis_read_package.V.sessionID.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":16,"up":31,"cElement": [{"cName": "m_axis_read_package.V.length.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_rx_metadata_V_V", "interface" : "axis", "bitwidth" : 16, "direction" : "READONLY", "bitSlice":[{"low":0,"up":15,"cElement": [{"cName": "s_axis_rx_metadata.V.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_rx_data_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "READONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "s_axis_rx_data.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_rx_data_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "s_axis_rx_data.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_rx_data_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "s_axis_rx_data.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_open_connection_V", "interface" : "axis", "bitwidth" : 48, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "m_axis_open_connection.V.ip_address.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":32,"up":47,"cElement": [{"cName": "m_axis_open_connection.V.ip_port.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_open_status_V", "interface" : "axis", "bitwidth" : 24, "direction" : "READONLY", "bitSlice":[{"low":0,"up":15,"cElement": [{"cName": "s_axis_open_status.V.sessionID.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":16,"up":16,"cElement": [{"cName": "s_axis_open_status.V.success","cData": "bool","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_close_connection_V_V", "interface" : "axis", "bitwidth" : 16, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":15,"cElement": [{"cName": "m_axis_close_connection.V.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_tx_metadata_V", "interface" : "axis", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":15,"cElement": [{"cName": "m_axis_tx_metadata.V.sessionID.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":16,"up":31,"cElement": [{"cName": "m_axis_tx_metadata.V.length.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_tx_data_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "m_axis_tx_data.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_tx_data_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "m_axis_tx_data.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_tx_data_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "m_axis_tx_data.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_tx_status_V", "interface" : "axis", "bitwidth" : 64, "direction" : "READONLY", "bitSlice":[{"low":0,"up":15,"cElement": [{"cName": "s_axis_tx_status.V.sessionID.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":16,"up":31,"cElement": [{"cName": "s_axis_tx_status.V.length.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":32,"up":61,"cElement": [{"cName": "s_axis_tx_status.V.remaining_space.V","cData": "uint30","bit_use": { "low": 0,"up": 29},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":62,"up":63,"cElement": [{"cName": "s_axis_tx_status.V.error.V","cData": "uint2","bit_use": { "low": 0,"up": 1},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} ]}
# RTL Port declarations: 
set portNum 42
set portList { 
	{ m_axis_listen_port_V_V_TDATA sc_out sc_lv 16 signal 0 } 
	{ s_axis_listen_port_status_V_TDATA sc_in sc_lv 8 signal 1 } 
	{ s_axis_notifications_V_TDATA sc_in sc_lv 88 signal 2 } 
	{ m_axis_read_package_V_TDATA sc_out sc_lv 32 signal 3 } 
	{ s_axis_rx_metadata_V_V_TDATA sc_in sc_lv 16 signal 4 } 
	{ s_axis_rx_data_TDATA sc_in sc_lv 64 signal 5 } 
	{ s_axis_rx_data_TKEEP sc_in sc_lv 8 signal 6 } 
	{ s_axis_rx_data_TLAST sc_in sc_lv 1 signal 7 } 
	{ m_axis_open_connection_V_TDATA sc_out sc_lv 48 signal 8 } 
	{ s_axis_open_status_V_TDATA sc_in sc_lv 24 signal 9 } 
	{ m_axis_close_connection_V_V_TDATA sc_out sc_lv 16 signal 10 } 
	{ m_axis_tx_metadata_V_TDATA sc_out sc_lv 32 signal 11 } 
	{ m_axis_tx_data_TDATA sc_out sc_lv 64 signal 12 } 
	{ m_axis_tx_data_TKEEP sc_out sc_lv 8 signal 13 } 
	{ m_axis_tx_data_TLAST sc_out sc_lv 1 signal 14 } 
	{ s_axis_tx_status_V_TDATA sc_in sc_lv 64 signal 15 } 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst_n sc_in sc_logic 1 reset -1 active_low_sync } 
	{ m_axis_tx_metadata_V_TVALID sc_out sc_logic 1 outvld 11 } 
	{ m_axis_tx_metadata_V_TREADY sc_in sc_logic 1 outacc 11 } 
	{ m_axis_tx_data_TVALID sc_out sc_logic 1 outvld 14 } 
	{ m_axis_tx_data_TREADY sc_in sc_logic 1 outacc 14 } 
	{ s_axis_rx_metadata_V_V_TVALID sc_in sc_logic 1 invld 4 } 
	{ s_axis_rx_metadata_V_V_TREADY sc_out sc_logic 1 inacc 4 } 
	{ s_axis_rx_data_TVALID sc_in sc_logic 1 invld 7 } 
	{ s_axis_rx_data_TREADY sc_out sc_logic 1 inacc 7 } 
	{ m_axis_listen_port_V_V_TVALID sc_out sc_logic 1 outvld 0 } 
	{ m_axis_listen_port_V_V_TREADY sc_in sc_logic 1 outacc 0 } 
	{ s_axis_listen_port_status_V_TVALID sc_in sc_logic 1 invld 1 } 
	{ s_axis_listen_port_status_V_TREADY sc_out sc_logic 1 inacc 1 } 
	{ s_axis_notifications_V_TVALID sc_in sc_logic 1 invld 2 } 
	{ s_axis_notifications_V_TREADY sc_out sc_logic 1 inacc 2 } 
	{ m_axis_read_package_V_TVALID sc_out sc_logic 1 outvld 3 } 
	{ m_axis_read_package_V_TREADY sc_in sc_logic 1 outacc 3 } 
	{ m_axis_open_connection_V_TVALID sc_out sc_logic 1 outvld 8 } 
	{ m_axis_open_connection_V_TREADY sc_in sc_logic 1 outacc 8 } 
	{ s_axis_open_status_V_TVALID sc_in sc_logic 1 invld 9 } 
	{ s_axis_open_status_V_TREADY sc_out sc_logic 1 inacc 9 } 
	{ m_axis_close_connection_V_V_TVALID sc_out sc_logic 1 outvld 10 } 
	{ m_axis_close_connection_V_V_TREADY sc_in sc_logic 1 outacc 10 } 
	{ s_axis_tx_status_V_TVALID sc_in sc_logic 1 invld 15 } 
	{ s_axis_tx_status_V_TREADY sc_out sc_logic 1 inacc 15 } 
}
set NewPortList {[ 
	{ "name": "m_axis_listen_port_V_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "m_axis_listen_port_V_V", "role": "TDATA" }} , 
 	{ "name": "s_axis_listen_port_status_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "s_axis_listen_port_status_V", "role": "TDATA" }} , 
 	{ "name": "s_axis_notifications_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":88, "type": "signal", "bundle":{"name": "s_axis_notifications_V", "role": "TDATA" }} , 
 	{ "name": "m_axis_read_package_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "m_axis_read_package_V", "role": "TDATA" }} , 
 	{ "name": "s_axis_rx_metadata_V_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "s_axis_rx_metadata_V_V", "role": "TDATA" }} , 
 	{ "name": "s_axis_rx_data_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "s_axis_rx_data_V_data_V", "role": "default" }} , 
 	{ "name": "s_axis_rx_data_TKEEP", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "s_axis_rx_data_V_keep_V", "role": "default" }} , 
 	{ "name": "s_axis_rx_data_TLAST", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "s_axis_rx_data_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_open_connection_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":48, "type": "signal", "bundle":{"name": "m_axis_open_connection_V", "role": "TDATA" }} , 
 	{ "name": "s_axis_open_status_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":24, "type": "signal", "bundle":{"name": "s_axis_open_status_V", "role": "TDATA" }} , 
 	{ "name": "m_axis_close_connection_V_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "m_axis_close_connection_V_V", "role": "TDATA" }} , 
 	{ "name": "m_axis_tx_metadata_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "m_axis_tx_metadata_V", "role": "TDATA" }} , 
 	{ "name": "m_axis_tx_data_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "m_axis_tx_data_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_tx_data_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "m_axis_tx_data_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_tx_data_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "m_axis_tx_data_V_last_V", "role": "default" }} , 
 	{ "name": "s_axis_tx_status_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "s_axis_tx_status_V", "role": "TDATA" }} , 
 	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst_n", "role": "default" }} , 
 	{ "name": "m_axis_tx_metadata_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_tx_metadata_V", "role": "TVALID" }} , 
 	{ "name": "m_axis_tx_metadata_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_tx_metadata_V", "role": "TREADY" }} , 
 	{ "name": "m_axis_tx_data_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_tx_data_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_tx_data_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_tx_data_V_last_V", "role": "default" }} , 
 	{ "name": "s_axis_rx_metadata_V_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "s_axis_rx_metadata_V_V", "role": "TVALID" }} , 
 	{ "name": "s_axis_rx_metadata_V_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "s_axis_rx_metadata_V_V", "role": "TREADY" }} , 
 	{ "name": "s_axis_rx_data_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "s_axis_rx_data_V_last_V", "role": "default" }} , 
 	{ "name": "s_axis_rx_data_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "s_axis_rx_data_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_listen_port_V_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_listen_port_V_V", "role": "TVALID" }} , 
 	{ "name": "m_axis_listen_port_V_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_listen_port_V_V", "role": "TREADY" }} , 
 	{ "name": "s_axis_listen_port_status_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "s_axis_listen_port_status_V", "role": "TVALID" }} , 
 	{ "name": "s_axis_listen_port_status_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "s_axis_listen_port_status_V", "role": "TREADY" }} , 
 	{ "name": "s_axis_notifications_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "s_axis_notifications_V", "role": "TVALID" }} , 
 	{ "name": "s_axis_notifications_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "s_axis_notifications_V", "role": "TREADY" }} , 
 	{ "name": "m_axis_read_package_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_read_package_V", "role": "TVALID" }} , 
 	{ "name": "m_axis_read_package_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_read_package_V", "role": "TREADY" }} , 
 	{ "name": "m_axis_open_connection_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_open_connection_V", "role": "TVALID" }} , 
 	{ "name": "m_axis_open_connection_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_open_connection_V", "role": "TREADY" }} , 
 	{ "name": "s_axis_open_status_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "s_axis_open_status_V", "role": "TVALID" }} , 
 	{ "name": "s_axis_open_status_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "s_axis_open_status_V", "role": "TREADY" }} , 
 	{ "name": "m_axis_close_connection_V_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_close_connection_V_V", "role": "TVALID" }} , 
 	{ "name": "m_axis_close_connection_V_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_close_connection_V_V", "role": "TREADY" }} , 
 	{ "name": "s_axis_tx_status_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "s_axis_tx_status_V", "role": "TVALID" }} , 
 	{ "name": "s_axis_tx_status_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "s_axis_tx_status_V", "role": "TREADY" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2", "3", "5", "7", "10", "11", "12", "13", "14"],
		"CDFG" : "echo_server_application",
		"Protocol" : "ap_ctrl_none",
		"ControlExist" : "0", "ap_start" : "0", "ap_ready" : "0", "ap_done" : "0", "ap_continue" : "0", "ap_idle" : "0",
		"Pipeline" : "Dataflow", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "1",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "1",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"InputProcess" : [
			{"ID" : "1", "Name" : "client_U0"},
			{"ID" : "2", "Name" : "server_U0"},
			{"ID" : "3", "Name" : "open_port_U0"},
			{"ID" : "5", "Name" : "notification_handler_U0"},
			{"ID" : "7", "Name" : "dummy_U0"}],
		"OutputProcess" : [
			{"ID" : "1", "Name" : "client_U0"},
			{"ID" : "2", "Name" : "server_U0"},
			{"ID" : "3", "Name" : "open_port_U0"},
			{"ID" : "5", "Name" : "notification_handler_U0"},
			{"ID" : "7", "Name" : "dummy_U0"}],
		"Port" : [
			{"Name" : "m_axis_listen_port_V_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "open_port_U0", "Port" : "listenPort_V_V"}]},
			{"Name" : "s_axis_listen_port_status_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "open_port_U0", "Port" : "listenSts_V"}]},
			{"Name" : "s_axis_notifications_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "5", "SubInstance" : "notification_handler_U0", "Port" : "notific_V"}]},
			{"Name" : "m_axis_read_package_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "5", "SubInstance" : "notification_handler_U0", "Port" : "readReq_V"}]},
			{"Name" : "s_axis_rx_metadata_V_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "server_U0", "Port" : "rxMetaData_V_V"}]},
			{"Name" : "s_axis_rx_data_V_data_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "server_U0", "Port" : "rxData_V_data_V"}]},
			{"Name" : "s_axis_rx_data_V_keep_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "server_U0", "Port" : "rxData_V_keep_V"}]},
			{"Name" : "s_axis_rx_data_V_last_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "server_U0", "Port" : "rxData_V_last_V"}]},
			{"Name" : "m_axis_open_connection_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "7", "SubInstance" : "dummy_U0", "Port" : "openConnection_V"}]},
			{"Name" : "s_axis_open_status_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "7", "SubInstance" : "dummy_U0", "Port" : "openConStatus_V"}]},
			{"Name" : "m_axis_close_connection_V_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "7", "SubInstance" : "dummy_U0", "Port" : "closeConnection_V_V"}]},
			{"Name" : "m_axis_tx_metadata_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "client_U0", "Port" : "txMetaData_V"}]},
			{"Name" : "m_axis_tx_data_V_data_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "client_U0", "Port" : "txData_V_data_V"}]},
			{"Name" : "m_axis_tx_data_V_keep_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "client_U0", "Port" : "txData_V_keep_V"}]},
			{"Name" : "m_axis_tx_data_V_last_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "client_U0", "Port" : "txData_V_last_V"}]},
			{"Name" : "s_axis_tx_status_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "7", "SubInstance" : "dummy_U0", "Port" : "txStatus_V"}]},
			{"Name" : "esac_fsmState_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "client_U0", "Port" : "esac_fsmState_V"}]},
			{"Name" : "esa_sessionidFifo_V_s", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "server_U0", "Port" : "esa_sessionidFifo_V_s"},
					{"ID" : "1", "SubInstance" : "client_U0", "Port" : "esa_sessionidFifo_V_s"}]},
			{"Name" : "esa_lengthFifo_V_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "client_U0", "Port" : "esa_lengthFifo_V_V"},
					{"ID" : "5", "SubInstance" : "notification_handler_U0", "Port" : "esa_lengthFifo_V_V"}]},
			{"Name" : "esa_dataFifo_V_data_s", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "server_U0", "Port" : "esa_dataFifo_V_data_s"},
					{"ID" : "1", "SubInstance" : "client_U0", "Port" : "esa_dataFifo_V_data_s"}]},
			{"Name" : "esa_dataFifo_V_keep_s", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "server_U0", "Port" : "esa_dataFifo_V_keep_s"},
					{"ID" : "1", "SubInstance" : "client_U0", "Port" : "esa_dataFifo_V_keep_s"}]},
			{"Name" : "esa_dataFifo_V_last_s", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "server_U0", "Port" : "esa_dataFifo_V_last_s"},
					{"ID" : "1", "SubInstance" : "client_U0", "Port" : "esa_dataFifo_V_last_s"}]},
			{"Name" : "ksvs_fsmState_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "server_U0", "Port" : "ksvs_fsmState_V"}]},
			{"Name" : "state_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "open_port_U0", "Port" : "state_V"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.client_U0", "Parent" : "0",
		"CDFG" : "client",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "0", "EstimateLatencyMin" : "0", "EstimateLatencyMax" : "0",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "txMetaData_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "txMetaData_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txData_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_tx_data_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txData_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "txData_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "esac_fsmState_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "esa_sessionidFifo_V_s", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "10",
				"BlockSignal" : [
					{"Name" : "esa_sessionidFifo_V_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "esa_lengthFifo_V_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "5", "DependentChan" : "11",
				"BlockSignal" : [
					{"Name" : "esa_lengthFifo_V_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "esa_dataFifo_V_data_s", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "12",
				"BlockSignal" : [
					{"Name" : "esa_dataFifo_V_data_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "esa_dataFifo_V_keep_s", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "13",
				"BlockSignal" : [
					{"Name" : "esa_dataFifo_V_keep_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "esa_dataFifo_V_last_s", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "14",
				"BlockSignal" : [
					{"Name" : "esa_dataFifo_V_last_s_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.server_U0", "Parent" : "0",
		"CDFG" : "server",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "rxMetaData_V_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "rxMetaData_V_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxData_V_data_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "s_axis_rx_data_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxData_V_keep_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "rxData_V_last_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "ksvs_fsmState_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "esa_sessionidFifo_V_s", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "1", "DependentChan" : "10",
				"BlockSignal" : [
					{"Name" : "esa_sessionidFifo_V_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "esa_dataFifo_V_data_s", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "1", "DependentChan" : "12",
				"BlockSignal" : [
					{"Name" : "esa_dataFifo_V_data_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "esa_dataFifo_V_keep_s", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "1", "DependentChan" : "13",
				"BlockSignal" : [
					{"Name" : "esa_dataFifo_V_keep_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "esa_dataFifo_V_last_s", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "1", "DependentChan" : "14",
				"BlockSignal" : [
					{"Name" : "esa_dataFifo_V_last_s_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "3", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.open_port_U0", "Parent" : "0", "Child" : ["4"],
		"CDFG" : "open_port",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "listenPort_V_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "listenPort_V_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "listenSts_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "listenSts_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "state_V", "Type" : "OVld", "Direction" : "IO"}]},
	{"ID" : "4", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.open_port_U0.regslice_both_listenPort_V_V_U", "Parent" : "3"},
	{"ID" : "5", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.notification_handler_U0", "Parent" : "0", "Child" : ["6"],
		"CDFG" : "notification_handler",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "notific_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "notific_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "readReq_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "readReq_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "esa_lengthFifo_V_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "1", "DependentChan" : "11",
				"BlockSignal" : [
					{"Name" : "esa_lengthFifo_V_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "6", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.notification_handler_U0.regslice_both_readReq_V_U", "Parent" : "5"},
	{"ID" : "7", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.dummy_U0", "Parent" : "0", "Child" : ["8", "9"],
		"CDFG" : "dummy",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "openConnection_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "openConnection_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "openConStatus_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "openConStatus_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "closeConnection_V_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "closeConnection_V_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txStatus_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "txStatus_V_TDATA_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "8", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.dummy_U0.regslice_both_openConnection_V_U", "Parent" : "7"},
	{"ID" : "9", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.dummy_U0.regslice_both_closeConnection_V_V_U", "Parent" : "7"},
	{"ID" : "10", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.esa_sessionidFifo_V_s_U", "Parent" : "0"},
	{"ID" : "11", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.esa_lengthFifo_V_V_U", "Parent" : "0"},
	{"ID" : "12", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.esa_dataFifo_V_data_s_U", "Parent" : "0"},
	{"ID" : "13", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.esa_dataFifo_V_keep_s_U", "Parent" : "0"},
	{"ID" : "14", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.esa_dataFifo_V_last_s_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	echo_server_application {
		m_axis_listen_port_V_V {Type O LastRead -1 FirstWrite 1}
		s_axis_listen_port_status_V {Type I LastRead 0 FirstWrite -1}
		s_axis_notifications_V {Type I LastRead 0 FirstWrite -1}
		m_axis_read_package_V {Type O LastRead -1 FirstWrite 1}
		s_axis_rx_metadata_V_V {Type I LastRead 0 FirstWrite -1}
		s_axis_rx_data_V_data_V {Type I LastRead 0 FirstWrite -1}
		s_axis_rx_data_V_keep_V {Type I LastRead 0 FirstWrite -1}
		s_axis_rx_data_V_last_V {Type I LastRead 0 FirstWrite -1}
		m_axis_open_connection_V {Type O LastRead -1 FirstWrite 1}
		s_axis_open_status_V {Type I LastRead 0 FirstWrite -1}
		m_axis_close_connection_V_V {Type O LastRead -1 FirstWrite 1}
		m_axis_tx_metadata_V {Type O LastRead 0 FirstWrite 0}
		m_axis_tx_data_V_data_V {Type O LastRead 0 FirstWrite 0}
		m_axis_tx_data_V_keep_V {Type O LastRead 0 FirstWrite 0}
		m_axis_tx_data_V_last_V {Type O LastRead 0 FirstWrite 0}
		s_axis_tx_status_V {Type I LastRead 0 FirstWrite -1}
		esac_fsmState_V {Type IO LastRead -1 FirstWrite -1}
		esa_sessionidFifo_V_s {Type IO LastRead -1 FirstWrite -1}
		esa_lengthFifo_V_V {Type IO LastRead -1 FirstWrite -1}
		esa_dataFifo_V_data_s {Type IO LastRead -1 FirstWrite -1}
		esa_dataFifo_V_keep_s {Type IO LastRead -1 FirstWrite -1}
		esa_dataFifo_V_last_s {Type IO LastRead -1 FirstWrite -1}
		ksvs_fsmState_V {Type IO LastRead -1 FirstWrite -1}
		state_V {Type IO LastRead -1 FirstWrite -1}}
	client {
		txMetaData_V {Type O LastRead 0 FirstWrite 0}
		txData_V_data_V {Type O LastRead 0 FirstWrite 0}
		txData_V_keep_V {Type O LastRead 0 FirstWrite 0}
		txData_V_last_V {Type O LastRead 0 FirstWrite 0}
		esac_fsmState_V {Type IO LastRead -1 FirstWrite -1}
		esa_sessionidFifo_V_s {Type I LastRead 0 FirstWrite -1}
		esa_lengthFifo_V_V {Type I LastRead 0 FirstWrite -1}
		esa_dataFifo_V_data_s {Type I LastRead 0 FirstWrite -1}
		esa_dataFifo_V_keep_s {Type I LastRead 0 FirstWrite -1}
		esa_dataFifo_V_last_s {Type I LastRead 0 FirstWrite -1}}
	server {
		rxMetaData_V_V {Type I LastRead 0 FirstWrite -1}
		rxData_V_data_V {Type I LastRead 0 FirstWrite -1}
		rxData_V_keep_V {Type I LastRead 0 FirstWrite -1}
		rxData_V_last_V {Type I LastRead 0 FirstWrite -1}
		ksvs_fsmState_V {Type IO LastRead -1 FirstWrite -1}
		esa_sessionidFifo_V_s {Type O LastRead -1 FirstWrite 1}
		esa_dataFifo_V_data_s {Type O LastRead -1 FirstWrite 1}
		esa_dataFifo_V_keep_s {Type O LastRead -1 FirstWrite 1}
		esa_dataFifo_V_last_s {Type O LastRead -1 FirstWrite 1}}
	open_port {
		listenPort_V_V {Type O LastRead -1 FirstWrite 1}
		listenSts_V {Type I LastRead 0 FirstWrite -1}
		state_V {Type IO LastRead -1 FirstWrite -1}}
	notification_handler {
		notific_V {Type I LastRead 0 FirstWrite -1}
		readReq_V {Type O LastRead -1 FirstWrite 1}
		esa_lengthFifo_V_V {Type O LastRead -1 FirstWrite 1}}
	dummy {
		openConnection_V {Type O LastRead -1 FirstWrite 1}
		openConStatus_V {Type I LastRead 0 FirstWrite -1}
		closeConnection_V_V {Type O LastRead -1 FirstWrite 1}
		txStatus_V {Type I LastRead 0 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "2", "Max" : "2"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
]}

set Spec2ImplPortList { 
	m_axis_listen_port_V_V { axis {  { m_axis_listen_port_V_V_TDATA out_data 1 16 }  { m_axis_listen_port_V_V_TVALID out_vld 1 1 }  { m_axis_listen_port_V_V_TREADY out_acc 0 1 } } }
	s_axis_listen_port_status_V { axis {  { s_axis_listen_port_status_V_TDATA in_data 0 8 }  { s_axis_listen_port_status_V_TVALID in_vld 0 1 }  { s_axis_listen_port_status_V_TREADY in_acc 1 1 } } }
	s_axis_notifications_V { axis {  { s_axis_notifications_V_TDATA in_data 0 88 }  { s_axis_notifications_V_TVALID in_vld 0 1 }  { s_axis_notifications_V_TREADY in_acc 1 1 } } }
	m_axis_read_package_V { axis {  { m_axis_read_package_V_TDATA out_data 1 32 }  { m_axis_read_package_V_TVALID out_vld 1 1 }  { m_axis_read_package_V_TREADY out_acc 0 1 } } }
	s_axis_rx_metadata_V_V { axis {  { s_axis_rx_metadata_V_V_TDATA in_data 0 16 }  { s_axis_rx_metadata_V_V_TVALID in_vld 0 1 }  { s_axis_rx_metadata_V_V_TREADY in_acc 1 1 } } }
	s_axis_rx_data_V_data_V { axis {  { s_axis_rx_data_TDATA in_data 0 64 } } }
	s_axis_rx_data_V_keep_V { axis {  { s_axis_rx_data_TKEEP in_data 0 8 } } }
	s_axis_rx_data_V_last_V { axis {  { s_axis_rx_data_TLAST in_data 0 1 }  { s_axis_rx_data_TVALID in_vld 0 1 }  { s_axis_rx_data_TREADY in_acc 1 1 } } }
	m_axis_open_connection_V { axis {  { m_axis_open_connection_V_TDATA out_data 1 48 }  { m_axis_open_connection_V_TVALID out_vld 1 1 }  { m_axis_open_connection_V_TREADY out_acc 0 1 } } }
	s_axis_open_status_V { axis {  { s_axis_open_status_V_TDATA in_data 0 24 }  { s_axis_open_status_V_TVALID in_vld 0 1 }  { s_axis_open_status_V_TREADY in_acc 1 1 } } }
	m_axis_close_connection_V_V { axis {  { m_axis_close_connection_V_V_TDATA out_data 1 16 }  { m_axis_close_connection_V_V_TVALID out_vld 1 1 }  { m_axis_close_connection_V_V_TREADY out_acc 0 1 } } }
	m_axis_tx_metadata_V { axis {  { m_axis_tx_metadata_V_TDATA out_data 1 32 }  { m_axis_tx_metadata_V_TVALID out_vld 1 1 }  { m_axis_tx_metadata_V_TREADY out_acc 0 1 } } }
	m_axis_tx_data_V_data_V { axis {  { m_axis_tx_data_TDATA out_data 1 64 } } }
	m_axis_tx_data_V_keep_V { axis {  { m_axis_tx_data_TKEEP out_data 1 8 } } }
	m_axis_tx_data_V_last_V { axis {  { m_axis_tx_data_TLAST out_data 1 1 }  { m_axis_tx_data_TVALID out_vld 1 1 }  { m_axis_tx_data_TREADY out_acc 0 1 } } }
	s_axis_tx_status_V { axis {  { s_axis_tx_status_V_TDATA in_data 0 64 }  { s_axis_tx_status_V_TVALID in_vld 0 1 }  { s_axis_tx_status_V_TREADY in_acc 1 1 } } }
}

set busDeadlockParameterList { 
}

# RTL port scheduling information:
set fifoSchedulingInfoList { 
}

# RTL bus port read request latency information:
set busReadReqLatencyList { 
}

# RTL bus port write response latency information:
set busWriteResLatencyList { 
}

# RTL array port load latency information:
set memoryLoadLatencyList { 
}
