
open_project ethernet_frame_padding_prj

open_solution "solution1"
set_part {xc7z020clg400-1}
create_clock -period 6.4 -name default

set_top ethernet_frame_padding

#add_files /home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/ethernet_frame_padding/../packet.hpp
#add_files /home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/ethernet_frame_padding/ethernet_frame_padding.hpp
add_files /home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/ethernet_frame_padding/ethernet_frame_padding.cpp -cflags "-I/home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/build_pynq/hls/ethernet_frame_padding"


#add_files -tb test_ethernet_frame_padding.cpp


#Check which command
set command [lindex $argv 2]

if {$command == "synthesis"} {
   csynth_design
} elseif {$command == "csim"} {
   csim_design
} elseif {$command == "ip"} {
   export_design -format ip_catalog -display_name "Ethernet Frame Padding" -vendor "ethz.systems.fpga" -version "0.1"
} elseif {$command == "installip"} {
   file mkdir /home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/iprepo
   file delete -force /home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/iprepo/ethernet_frame_padding
   file copy -force /home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/build_pynq/hls/ethernet_frame_padding/ethernet_frame_padding_prj/solution1/impl/ip /home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/iprepo/ethernet_frame_padding/
} else {
   puts "No valid command specified. Use vivado_hls -f make.tcl <synthesis|csim|ip> ."
}


exit
