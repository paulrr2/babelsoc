set clock_constraint { \
    name clk \
    module ethernet_frame_padding \
    port ap_clk \
    period 6.4 \
    uncertainty 0.8 \
}

set all_path {}

set false_path {}

