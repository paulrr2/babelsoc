// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
// Version: 2019.2.1
// Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

module process_udp_64_1989 (
        ap_clk,
        ap_rst,
        ap_start,
        ap_done,
        ap_continue,
        ap_idle,
        ap_ready,
        rx_ip2udpFifo_V_data_dout,
        rx_ip2udpFifo_V_data_empty_n,
        rx_ip2udpFifo_V_data_read,
        rx_ip2udpFifo_V_keep_dout,
        rx_ip2udpFifo_V_keep_empty_n,
        rx_ip2udpFifo_V_keep_read,
        rx_ip2udpFifo_V_last_dout,
        rx_ip2udpFifo_V_last_empty_n,
        rx_ip2udpFifo_V_last_read,
        rx_udp2shiftFifo_V_d_din,
        rx_udp2shiftFifo_V_d_full_n,
        rx_udp2shiftFifo_V_d_write,
        rx_udp2shiftFifo_V_k_din,
        rx_udp2shiftFifo_V_k_full_n,
        rx_udp2shiftFifo_V_k_write,
        rx_udp2shiftFifo_V_l_din,
        rx_udp2shiftFifo_V_l_full_n,
        rx_udp2shiftFifo_V_l_write,
        rx_udpMetaFifo_V_din,
        rx_udpMetaFifo_V_full_n,
        rx_udpMetaFifo_V_write
);

parameter    ap_ST_fsm_pp0_stage0 = 1'd1;

input   ap_clk;
input   ap_rst;
input   ap_start;
output   ap_done;
input   ap_continue;
output   ap_idle;
output   ap_ready;
input  [63:0] rx_ip2udpFifo_V_data_dout;
input   rx_ip2udpFifo_V_data_empty_n;
output   rx_ip2udpFifo_V_data_read;
input  [7:0] rx_ip2udpFifo_V_keep_dout;
input   rx_ip2udpFifo_V_keep_empty_n;
output   rx_ip2udpFifo_V_keep_read;
input  [0:0] rx_ip2udpFifo_V_last_dout;
input   rx_ip2udpFifo_V_last_empty_n;
output   rx_ip2udpFifo_V_last_read;
output  [63:0] rx_udp2shiftFifo_V_d_din;
input   rx_udp2shiftFifo_V_d_full_n;
output   rx_udp2shiftFifo_V_d_write;
output  [7:0] rx_udp2shiftFifo_V_k_din;
input   rx_udp2shiftFifo_V_k_full_n;
output   rx_udp2shiftFifo_V_k_write;
output  [0:0] rx_udp2shiftFifo_V_l_din;
input   rx_udp2shiftFifo_V_l_full_n;
output   rx_udp2shiftFifo_V_l_write;
output  [48:0] rx_udpMetaFifo_V_din;
input   rx_udpMetaFifo_V_full_n;
output   rx_udpMetaFifo_V_write;

reg ap_done;
reg ap_idle;
reg ap_ready;
reg rx_ip2udpFifo_V_data_read;
reg rx_ip2udpFifo_V_keep_read;
reg rx_ip2udpFifo_V_last_read;
reg rx_udp2shiftFifo_V_d_write;
reg rx_udp2shiftFifo_V_k_write;
reg rx_udp2shiftFifo_V_l_write;
reg rx_udpMetaFifo_V_write;

reg    ap_done_reg;
(* fsm_encoding = "none" *) reg   [0:0] ap_CS_fsm;
wire    ap_CS_fsm_pp0_stage0;
wire    ap_enable_reg_pp0_iter0;
reg    ap_enable_reg_pp0_iter1;
reg    ap_enable_reg_pp0_iter2;
reg    ap_idle_pp0;
wire    io_acc_block_signal_op6;
wire   [0:0] tmp_nbreadreq_fu_98_p5;
reg    ap_block_state1_pp0_stage0_iter0;
wire    ap_block_state2_pp0_stage0_iter1;
wire    io_acc_block_signal_op73;
reg   [0:0] tmp_reg_520;
reg   [0:0] tmp_reg_520_pp0_iter1_reg;
reg   [0:0] metaWritten_2_load_reg_561;
reg   [0:0] icmp_ln879_reg_565;
reg    ap_predicate_op73_write_state3;
reg   [0:0] or_ln73_1_reg_557;
reg    ap_predicate_op78_write_state3;
reg    ap_block_state3_pp0_stage0_iter2;
reg    ap_block_pp0_stage0_11001;
reg   [0:0] pu_header_ready;
reg   [15:0] pu_header_idx;
reg   [63:0] pu_header_header_V;
reg   [0:0] metaWritten_2;
reg    rx_ip2udpFifo_V_data_blk_n;
wire    ap_block_pp0_stage0;
reg    rx_ip2udpFifo_V_keep_blk_n;
reg    rx_ip2udpFifo_V_last_blk_n;
reg    rx_udp2shiftFifo_V_d_blk_n;
reg    rx_udp2shiftFifo_V_k_blk_n;
reg    rx_udp2shiftFifo_V_l_blk_n;
reg    rx_udpMetaFifo_V_blk_n;
reg   [63:0] tmp_data_V_reg_524;
reg   [63:0] tmp_data_V_reg_524_pp0_iter1_reg;
reg   [7:0] tmp_keep_V_reg_529;
reg   [7:0] tmp_keep_V_reg_529_pp0_iter1_reg;
reg   [0:0] tmp_last_V_reg_534;
reg   [0:0] tmp_last_V_reg_534_pp0_iter1_reg;
wire   [0:0] pu_header_ready_load_load_fu_190_p1;
reg   [0:0] pu_header_ready_load_reg_542;
wire   [15:0] add_ln67_fu_344_p2;
wire   [0:0] or_ln73_1_fu_350_p2;
wire   [0:0] metaWritten_2_load_load_fu_355_p1;
wire   [0:0] icmp_ln879_fu_391_p2;
reg   [7:0] p_Result_71_i_i9_i_reg_569;
reg   [7:0] p_Result_71_1_i_i1_reg_575;
reg   [7:0] p_Result_71_i_i14_s_reg_581;
wire   [7:0] trunc_ln647_fu_431_p1;
reg   [7:0] trunc_ln647_reg_586;
reg   [7:0] p_Result_71_i_i19_s_reg_591;
reg   [7:0] p_Result_71_1_i_i2_reg_596;
wire   [0:0] xor_ln73_fu_455_p2;
reg   [0:0] xor_ln73_reg_601;
reg    ap_block_pp0_stage0_subdone;
wire   [0:0] ap_phi_reg_pp0_iter0_write_flag_0_i_i_reg_140;
reg   [0:0] ap_phi_reg_pp0_iter1_write_flag_0_i_i_reg_140;
wire   [15:0] ap_phi_reg_pp0_iter0_packetHeader_idx_0_i_reg_153;
reg   [15:0] ap_phi_reg_pp0_iter1_packetHeader_idx_0_i_reg_153;
wire   [0:0] ap_phi_reg_pp0_iter0_metaWritten_6_flag_1_reg_162;
reg   [0:0] ap_phi_reg_pp0_iter1_metaWritten_6_flag_1_reg_162;
reg   [0:0] ap_phi_reg_pp0_iter2_metaWritten_6_flag_1_reg_162;
wire   [0:0] and_ln73_fu_460_p2;
reg   [0:0] ap_sig_allocacmp_pu_header_ready_load;
wire   [15:0] select_ln73_fu_466_p3;
reg   [15:0] ap_sig_allocacmp_pu_header_idx_load;
wire   [63:0] p_Result_s_fu_332_p2;
wire   [0:0] or_ln73_fu_510_p2;
reg   [0:0] ap_sig_allocacmp_metaWritten_2_load;
reg    ap_block_pp0_stage0_01001;
wire   [25:0] zext_ln414_fu_206_p1;
wire   [0:0] trunc_ln64_fu_198_p1;
wire   [6:0] tmp_40_fu_216_p3;
wire   [0:0] icmp_ln414_fu_210_p2;
wire   [6:0] sub_ln414_fu_224_p2;
wire   [6:0] sub_ln414_1_fu_246_p2;
wire   [6:0] select_ln414_4_fu_238_p3;
wire   [6:0] select_ln414_fu_230_p3;
wire   [6:0] select_ln414_5_fu_252_p3;
wire   [63:0] zext_ln414_1_fu_260_p1;
wire   [63:0] shl_ln414_fu_272_p2;
reg   [63:0] tmp_41_fu_278_p4;
wire   [63:0] zext_ln414_2_fu_264_p1;
wire   [63:0] zext_ln414_3_fu_268_p1;
wire   [63:0] shl_ln414_1_fu_296_p2;
wire   [63:0] lshr_ln414_fu_302_p2;
wire   [63:0] and_ln414_fu_308_p2;
wire   [63:0] xor_ln414_fu_314_p2;
wire   [63:0] select_ln414_6_fu_288_p3;
wire   [63:0] and_ln414_3_fu_320_p2;
wire   [63:0] and_ln414_4_fu_326_p2;
wire   [7:0] p_Result_71_1_i_i_s_fu_373_p4;
wire   [7:0] p_Result_71_i_i_i_fu_363_p4;
wire   [15:0] agg_result_V_0_1_i_i_fu_383_p3;
wire   [15:0] dstPort_V_fu_485_p3;
wire   [0:0] tmp_valid_fu_491_p2;
reg   [0:0] ap_NS_fsm;
reg    ap_idle_pp0_0to1;
reg    ap_reset_idle_pp0;
wire    ap_enable_pp0;
reg    ap_condition_112;

// power-on initialization
initial begin
#0 ap_done_reg = 1'b0;
#0 ap_CS_fsm = 1'd1;
#0 ap_enable_reg_pp0_iter1 = 1'b0;
#0 ap_enable_reg_pp0_iter2 = 1'b0;
#0 pu_header_ready = 1'd0;
#0 pu_header_idx = 16'd0;
#0 pu_header_header_V = 64'd0;
#0 metaWritten_2 = 1'd0;
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_CS_fsm <= ap_ST_fsm_pp0_stage0;
    end else begin
        ap_CS_fsm <= ap_NS_fsm;
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_done_reg <= 1'b0;
    end else begin
        if ((ap_continue == 1'b1)) begin
            ap_done_reg <= 1'b0;
        end else if (((ap_enable_reg_pp0_iter2 == 1'b1) & (1'b0 == ap_block_pp0_stage0_11001))) begin
            ap_done_reg <= 1'b1;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_enable_reg_pp0_iter1 <= 1'b0;
    end else begin
        if (((1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_subdone))) begin
            ap_enable_reg_pp0_iter1 <= ap_start;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_enable_reg_pp0_iter2 <= 1'b0;
    end else begin
        if ((1'b0 == ap_block_pp0_stage0_subdone)) begin
            ap_enable_reg_pp0_iter2 <= ap_enable_reg_pp0_iter1;
        end
    end
end

always @ (posedge ap_clk) begin
    if ((1'b1 == ap_condition_112)) begin
        if (((pu_header_ready_load_load_fu_190_p1 == 1'd0) & (tmp_nbreadreq_fu_98_p5 == 1'd1))) begin
            ap_phi_reg_pp0_iter1_packetHeader_idx_0_i_reg_153 <= add_ln67_fu_344_p2;
        end else if (((pu_header_ready_load_load_fu_190_p1 == 1'd1) & (tmp_nbreadreq_fu_98_p5 == 1'd1))) begin
            ap_phi_reg_pp0_iter1_packetHeader_idx_0_i_reg_153 <= ap_sig_allocacmp_pu_header_idx_load;
        end else if ((1'b1 == 1'b1)) begin
            ap_phi_reg_pp0_iter1_packetHeader_idx_0_i_reg_153 <= ap_phi_reg_pp0_iter0_packetHeader_idx_0_i_reg_153;
        end
    end
end

always @ (posedge ap_clk) begin
    if ((1'b1 == ap_condition_112)) begin
        if (((pu_header_ready_load_load_fu_190_p1 == 1'd0) & (tmp_nbreadreq_fu_98_p5 == 1'd1))) begin
            ap_phi_reg_pp0_iter1_write_flag_0_i_i_reg_140 <= 1'd1;
        end else if (((pu_header_ready_load_load_fu_190_p1 == 1'd1) & (tmp_nbreadreq_fu_98_p5 == 1'd1))) begin
            ap_phi_reg_pp0_iter1_write_flag_0_i_i_reg_140 <= 1'd0;
        end else if ((1'b1 == 1'b1)) begin
            ap_phi_reg_pp0_iter1_write_flag_0_i_i_reg_140 <= ap_phi_reg_pp0_iter0_write_flag_0_i_i_reg_140;
        end
    end
end

always @ (posedge ap_clk) begin
    if (((metaWritten_2_load_load_fu_355_p1 == 1'd0) & (tmp_reg_520 == 1'd1) & (or_ln73_1_fu_350_p2 == 1'd1) & (ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        ap_phi_reg_pp0_iter2_metaWritten_6_flag_1_reg_162 <= 1'd1;
    end else if ((((or_ln73_1_fu_350_p2 == 1'd0) & (tmp_reg_520 == 1'd1) & (ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001)) | ((tmp_reg_520 == 1'd1) & (metaWritten_2_load_load_fu_355_p1 == 1'd1) & (or_ln73_1_fu_350_p2 == 1'd1) & (ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001)))) begin
        ap_phi_reg_pp0_iter2_metaWritten_6_flag_1_reg_162 <= 1'd0;
    end else if (((ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        ap_phi_reg_pp0_iter2_metaWritten_6_flag_1_reg_162 <= ap_phi_reg_pp0_iter1_metaWritten_6_flag_1_reg_162;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_start == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        ap_phi_reg_pp0_iter1_metaWritten_6_flag_1_reg_162 <= ap_phi_reg_pp0_iter0_metaWritten_6_flag_1_reg_162;
    end
end

always @ (posedge ap_clk) begin
    if (((tmp_reg_520 == 1'd1) & (metaWritten_2_load_load_fu_355_p1 == 1'd1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        icmp_ln879_reg_565 <= icmp_ln879_fu_391_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((tmp_reg_520_pp0_iter1_reg == 1'd1) & (or_ln73_fu_510_p2 == 1'd1) & (ap_enable_reg_pp0_iter2 == 1'b1) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        metaWritten_2 <= xor_ln73_reg_601;
    end
end

always @ (posedge ap_clk) begin
    if (((tmp_reg_520 == 1'd1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        metaWritten_2_load_reg_561 <= ap_sig_allocacmp_metaWritten_2_load;
        or_ln73_1_reg_557 <= or_ln73_1_fu_350_p2;
        xor_ln73_reg_601 <= xor_ln73_fu_455_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((tmp_reg_520 == 1'd1) & (or_ln73_1_fu_350_p2 == 1'd1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        p_Result_71_1_i_i1_reg_575 <= {{pu_header_header_V[23:16]}};
        p_Result_71_i_i9_i_reg_569 <= {{pu_header_header_V[31:24]}};
    end
end

always @ (posedge ap_clk) begin
    if (((metaWritten_2_load_load_fu_355_p1 == 1'd0) & (tmp_reg_520 == 1'd1) & (or_ln73_1_fu_350_p2 == 1'd1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        p_Result_71_1_i_i2_reg_596 <= {{pu_header_header_V[39:32]}};
        p_Result_71_i_i14_s_reg_581 <= {{pu_header_header_V[15:8]}};
        p_Result_71_i_i19_s_reg_591 <= {{pu_header_header_V[47:40]}};
        trunc_ln647_reg_586 <= trunc_ln647_fu_431_p1;
    end
end

always @ (posedge ap_clk) begin
    if (((pu_header_ready_load_load_fu_190_p1 == 1'd0) & (tmp_nbreadreq_fu_98_p5 == 1'd1) & (ap_start == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        pu_header_header_V <= p_Result_s_fu_332_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((tmp_reg_520 == 1'd1) & (ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        pu_header_idx <= select_ln73_fu_466_p3;
        pu_header_ready <= and_ln73_fu_460_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((tmp_nbreadreq_fu_98_p5 == 1'd1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        pu_header_ready_load_reg_542 <= ap_sig_allocacmp_pu_header_ready_load;
        tmp_data_V_reg_524 <= rx_ip2udpFifo_V_data_dout;
        tmp_keep_V_reg_529 <= rx_ip2udpFifo_V_keep_dout;
        tmp_last_V_reg_534 <= rx_ip2udpFifo_V_last_dout;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        tmp_data_V_reg_524_pp0_iter1_reg <= tmp_data_V_reg_524;
        tmp_keep_V_reg_529_pp0_iter1_reg <= tmp_keep_V_reg_529;
        tmp_last_V_reg_534_pp0_iter1_reg <= tmp_last_V_reg_534;
        tmp_reg_520 <= tmp_nbreadreq_fu_98_p5;
        tmp_reg_520_pp0_iter1_reg <= tmp_reg_520;
    end
end

always @ (*) begin
    if (((ap_enable_reg_pp0_iter2 == 1'b1) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        ap_done = 1'b1;
    end else begin
        ap_done = ap_done_reg;
    end
end

always @ (*) begin
    if (((ap_start == 1'b0) & (ap_idle_pp0 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        ap_idle = 1'b1;
    end else begin
        ap_idle = 1'b0;
    end
end

always @ (*) begin
    if (((ap_enable_reg_pp0_iter2 == 1'b0) & (ap_enable_reg_pp0_iter1 == 1'b0) & (ap_enable_reg_pp0_iter0 == 1'b0))) begin
        ap_idle_pp0 = 1'b1;
    end else begin
        ap_idle_pp0 = 1'b0;
    end
end

always @ (*) begin
    if (((ap_enable_reg_pp0_iter1 == 1'b0) & (ap_enable_reg_pp0_iter0 == 1'b0))) begin
        ap_idle_pp0_0to1 = 1'b1;
    end else begin
        ap_idle_pp0_0to1 = 1'b0;
    end
end

always @ (*) begin
    if (((ap_start == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        ap_ready = 1'b1;
    end else begin
        ap_ready = 1'b0;
    end
end

always @ (*) begin
    if (((ap_start == 1'b0) & (ap_idle_pp0_0to1 == 1'b1))) begin
        ap_reset_idle_pp0 = 1'b1;
    end else begin
        ap_reset_idle_pp0 = 1'b0;
    end
end

always @ (*) begin
    if (((tmp_reg_520_pp0_iter1_reg == 1'd1) & (or_ln73_fu_510_p2 == 1'd1) & (ap_enable_reg_pp0_iter2 == 1'b1) & (1'b0 == ap_block_pp0_stage0))) begin
        ap_sig_allocacmp_metaWritten_2_load = xor_ln73_reg_601;
    end else begin
        ap_sig_allocacmp_metaWritten_2_load = metaWritten_2;
    end
end

always @ (*) begin
    if (((tmp_reg_520 == 1'd1) & (ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0))) begin
        ap_sig_allocacmp_pu_header_idx_load = select_ln73_fu_466_p3;
    end else begin
        ap_sig_allocacmp_pu_header_idx_load = pu_header_idx;
    end
end

always @ (*) begin
    if (((tmp_reg_520 == 1'd1) & (ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0))) begin
        ap_sig_allocacmp_pu_header_ready_load = and_ln73_fu_460_p2;
    end else begin
        ap_sig_allocacmp_pu_header_ready_load = pu_header_ready;
    end
end

always @ (*) begin
    if ((~((ap_start == 1'b0) | (ap_done_reg == 1'b1)) & (tmp_nbreadreq_fu_98_p5 == 1'd1) & (ap_start == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0))) begin
        rx_ip2udpFifo_V_data_blk_n = rx_ip2udpFifo_V_data_empty_n;
    end else begin
        rx_ip2udpFifo_V_data_blk_n = 1'b1;
    end
end

always @ (*) begin
    if (((tmp_nbreadreq_fu_98_p5 == 1'd1) & (ap_start == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        rx_ip2udpFifo_V_data_read = 1'b1;
    end else begin
        rx_ip2udpFifo_V_data_read = 1'b0;
    end
end

always @ (*) begin
    if ((~((ap_start == 1'b0) | (ap_done_reg == 1'b1)) & (tmp_nbreadreq_fu_98_p5 == 1'd1) & (ap_start == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0))) begin
        rx_ip2udpFifo_V_keep_blk_n = rx_ip2udpFifo_V_keep_empty_n;
    end else begin
        rx_ip2udpFifo_V_keep_blk_n = 1'b1;
    end
end

always @ (*) begin
    if (((tmp_nbreadreq_fu_98_p5 == 1'd1) & (ap_start == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        rx_ip2udpFifo_V_keep_read = 1'b1;
    end else begin
        rx_ip2udpFifo_V_keep_read = 1'b0;
    end
end

always @ (*) begin
    if ((~((ap_start == 1'b0) | (ap_done_reg == 1'b1)) & (tmp_nbreadreq_fu_98_p5 == 1'd1) & (ap_start == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0))) begin
        rx_ip2udpFifo_V_last_blk_n = rx_ip2udpFifo_V_last_empty_n;
    end else begin
        rx_ip2udpFifo_V_last_blk_n = 1'b1;
    end
end

always @ (*) begin
    if (((tmp_nbreadreq_fu_98_p5 == 1'd1) & (ap_start == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        rx_ip2udpFifo_V_last_read = 1'b1;
    end else begin
        rx_ip2udpFifo_V_last_read = 1'b0;
    end
end

always @ (*) begin
    if (((ap_enable_reg_pp0_iter2 == 1'b1) & (ap_predicate_op73_write_state3 == 1'b1) & (1'b0 == ap_block_pp0_stage0))) begin
        rx_udp2shiftFifo_V_d_blk_n = rx_udp2shiftFifo_V_d_full_n;
    end else begin
        rx_udp2shiftFifo_V_d_blk_n = 1'b1;
    end
end

always @ (*) begin
    if (((ap_enable_reg_pp0_iter2 == 1'b1) & (ap_predicate_op73_write_state3 == 1'b1) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        rx_udp2shiftFifo_V_d_write = 1'b1;
    end else begin
        rx_udp2shiftFifo_V_d_write = 1'b0;
    end
end

always @ (*) begin
    if (((ap_enable_reg_pp0_iter2 == 1'b1) & (ap_predicate_op73_write_state3 == 1'b1) & (1'b0 == ap_block_pp0_stage0))) begin
        rx_udp2shiftFifo_V_k_blk_n = rx_udp2shiftFifo_V_k_full_n;
    end else begin
        rx_udp2shiftFifo_V_k_blk_n = 1'b1;
    end
end

always @ (*) begin
    if (((ap_enable_reg_pp0_iter2 == 1'b1) & (ap_predicate_op73_write_state3 == 1'b1) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        rx_udp2shiftFifo_V_k_write = 1'b1;
    end else begin
        rx_udp2shiftFifo_V_k_write = 1'b0;
    end
end

always @ (*) begin
    if (((ap_enable_reg_pp0_iter2 == 1'b1) & (ap_predicate_op73_write_state3 == 1'b1) & (1'b0 == ap_block_pp0_stage0))) begin
        rx_udp2shiftFifo_V_l_blk_n = rx_udp2shiftFifo_V_l_full_n;
    end else begin
        rx_udp2shiftFifo_V_l_blk_n = 1'b1;
    end
end

always @ (*) begin
    if (((ap_enable_reg_pp0_iter2 == 1'b1) & (ap_predicate_op73_write_state3 == 1'b1) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        rx_udp2shiftFifo_V_l_write = 1'b1;
    end else begin
        rx_udp2shiftFifo_V_l_write = 1'b0;
    end
end

always @ (*) begin
    if (((ap_enable_reg_pp0_iter2 == 1'b1) & (ap_predicate_op78_write_state3 == 1'b1) & (1'b0 == ap_block_pp0_stage0))) begin
        rx_udpMetaFifo_V_blk_n = rx_udpMetaFifo_V_full_n;
    end else begin
        rx_udpMetaFifo_V_blk_n = 1'b1;
    end
end

always @ (*) begin
    if (((ap_enable_reg_pp0_iter2 == 1'b1) & (ap_predicate_op78_write_state3 == 1'b1) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        rx_udpMetaFifo_V_write = 1'b1;
    end else begin
        rx_udpMetaFifo_V_write = 1'b0;
    end
end

always @ (*) begin
    case (ap_CS_fsm)
        ap_ST_fsm_pp0_stage0 : begin
            ap_NS_fsm = ap_ST_fsm_pp0_stage0;
        end
        default : begin
            ap_NS_fsm = 'bx;
        end
    endcase
end

assign add_ln67_fu_344_p2 = (ap_sig_allocacmp_pu_header_idx_load + 16'd1);

assign agg_result_V_0_1_i_i_fu_383_p3 = {{p_Result_71_1_i_i_s_fu_373_p4}, {p_Result_71_i_i_i_fu_363_p4}};

assign and_ln414_3_fu_320_p2 = (xor_ln414_fu_314_p2 & pu_header_header_V);

assign and_ln414_4_fu_326_p2 = (select_ln414_6_fu_288_p3 & and_ln414_fu_308_p2);

assign and_ln414_fu_308_p2 = (shl_ln414_1_fu_296_p2 & lshr_ln414_fu_302_p2);

assign and_ln73_fu_460_p2 = (xor_ln73_fu_455_p2 & or_ln73_1_fu_350_p2);

assign ap_CS_fsm_pp0_stage0 = ap_CS_fsm[32'd0];

assign ap_block_pp0_stage0 = ~(1'b1 == 1'b1);

always @ (*) begin
    ap_block_pp0_stage0_01001 = ((ap_done_reg == 1'b1) | ((ap_start == 1'b1) & ((ap_start == 1'b0) | (ap_done_reg == 1'b1) | ((tmp_nbreadreq_fu_98_p5 == 1'd1) & (io_acc_block_signal_op6 == 1'b0)))) | ((ap_enable_reg_pp0_iter2 == 1'b1) & (((rx_udpMetaFifo_V_full_n == 1'b0) & (ap_predicate_op78_write_state3 == 1'b1)) | ((io_acc_block_signal_op73 == 1'b0) & (ap_predicate_op73_write_state3 == 1'b1)))));
end

always @ (*) begin
    ap_block_pp0_stage0_11001 = ((ap_done_reg == 1'b1) | ((ap_start == 1'b1) & ((ap_start == 1'b0) | (ap_done_reg == 1'b1) | ((tmp_nbreadreq_fu_98_p5 == 1'd1) & (io_acc_block_signal_op6 == 1'b0)))) | ((ap_enable_reg_pp0_iter2 == 1'b1) & (((rx_udpMetaFifo_V_full_n == 1'b0) & (ap_predicate_op78_write_state3 == 1'b1)) | ((io_acc_block_signal_op73 == 1'b0) & (ap_predicate_op73_write_state3 == 1'b1)))));
end

always @ (*) begin
    ap_block_pp0_stage0_subdone = ((ap_done_reg == 1'b1) | ((ap_start == 1'b1) & ((ap_start == 1'b0) | (ap_done_reg == 1'b1) | ((tmp_nbreadreq_fu_98_p5 == 1'd1) & (io_acc_block_signal_op6 == 1'b0)))) | ((ap_enable_reg_pp0_iter2 == 1'b1) & (((rx_udpMetaFifo_V_full_n == 1'b0) & (ap_predicate_op78_write_state3 == 1'b1)) | ((io_acc_block_signal_op73 == 1'b0) & (ap_predicate_op73_write_state3 == 1'b1)))));
end

always @ (*) begin
    ap_block_state1_pp0_stage0_iter0 = ((ap_start == 1'b0) | (ap_done_reg == 1'b1) | ((tmp_nbreadreq_fu_98_p5 == 1'd1) & (io_acc_block_signal_op6 == 1'b0)));
end

assign ap_block_state2_pp0_stage0_iter1 = ~(1'b1 == 1'b1);

always @ (*) begin
    ap_block_state3_pp0_stage0_iter2 = (((rx_udpMetaFifo_V_full_n == 1'b0) & (ap_predicate_op78_write_state3 == 1'b1)) | ((io_acc_block_signal_op73 == 1'b0) & (ap_predicate_op73_write_state3 == 1'b1)));
end

always @ (*) begin
    ap_condition_112 = ((ap_start == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001));
end

assign ap_enable_pp0 = (ap_idle_pp0 ^ 1'b1);

assign ap_enable_reg_pp0_iter0 = ap_start;

assign ap_phi_reg_pp0_iter0_metaWritten_6_flag_1_reg_162 = 'bx;

assign ap_phi_reg_pp0_iter0_packetHeader_idx_0_i_reg_153 = 'bx;

assign ap_phi_reg_pp0_iter0_write_flag_0_i_i_reg_140 = 'bx;

always @ (*) begin
    ap_predicate_op73_write_state3 = ((icmp_ln879_reg_565 == 1'd1) & (metaWritten_2_load_reg_561 == 1'd1) & (tmp_reg_520_pp0_iter1_reg == 1'd1));
end

always @ (*) begin
    ap_predicate_op78_write_state3 = ((metaWritten_2_load_reg_561 == 1'd0) & (or_ln73_1_reg_557 == 1'd1) & (tmp_reg_520_pp0_iter1_reg == 1'd1));
end

assign dstPort_V_fu_485_p3 = {{p_Result_71_1_i_i1_reg_575}, {p_Result_71_i_i9_i_reg_569}};

assign icmp_ln414_fu_210_p2 = ((zext_ln414_fu_206_p1 != 26'd0) ? 1'b1 : 1'b0);

assign icmp_ln879_fu_391_p2 = ((agg_result_V_0_1_i_i_fu_383_p3 == 16'd4791) ? 1'b1 : 1'b0);

assign io_acc_block_signal_op6 = (rx_ip2udpFifo_V_last_empty_n & rx_ip2udpFifo_V_keep_empty_n & rx_ip2udpFifo_V_data_empty_n);

assign io_acc_block_signal_op73 = (rx_udp2shiftFifo_V_l_full_n & rx_udp2shiftFifo_V_k_full_n & rx_udp2shiftFifo_V_d_full_n);

assign lshr_ln414_fu_302_p2 = 64'd18446744073709551615 >> zext_ln414_3_fu_268_p1;

assign metaWritten_2_load_load_fu_355_p1 = ap_sig_allocacmp_metaWritten_2_load;

assign or_ln73_1_fu_350_p2 = (pu_header_ready_load_reg_542 | ap_phi_reg_pp0_iter1_write_flag_0_i_i_reg_140);

assign or_ln73_fu_510_p2 = (tmp_last_V_reg_534_pp0_iter1_reg | ap_phi_reg_pp0_iter2_metaWritten_6_flag_1_reg_162);

assign p_Result_71_1_i_i_s_fu_373_p4 = {{pu_header_header_V[23:16]}};

assign p_Result_71_i_i_i_fu_363_p4 = {{pu_header_header_V[31:24]}};

assign p_Result_s_fu_332_p2 = (and_ln414_4_fu_326_p2 | and_ln414_3_fu_320_p2);

assign pu_header_ready_load_load_fu_190_p1 = ap_sig_allocacmp_pu_header_ready_load;

assign rx_udp2shiftFifo_V_d_din = tmp_data_V_reg_524_pp0_iter1_reg;

assign rx_udp2shiftFifo_V_k_din = tmp_keep_V_reg_529_pp0_iter1_reg;

assign rx_udp2shiftFifo_V_l_din = tmp_last_V_reg_534_pp0_iter1_reg;

assign rx_udpMetaFifo_V_din = {{{{{{{tmp_valid_fu_491_p2}, {p_Result_71_1_i_i2_reg_596}}, {p_Result_71_i_i19_s_reg_591}}, {p_Result_71_1_i_i1_reg_575}}, {p_Result_71_i_i9_i_reg_569}}, {trunc_ln647_reg_586}}, {p_Result_71_i_i14_s_reg_581}};

assign select_ln414_4_fu_238_p3 = ((icmp_ln414_fu_210_p2[0:0] === 1'b1) ? sub_ln414_fu_224_p2 : tmp_40_fu_216_p3);

assign select_ln414_5_fu_252_p3 = ((icmp_ln414_fu_210_p2[0:0] === 1'b1) ? sub_ln414_1_fu_246_p2 : 7'd0);

assign select_ln414_6_fu_288_p3 = ((icmp_ln414_fu_210_p2[0:0] === 1'b1) ? tmp_41_fu_278_p4 : shl_ln414_fu_272_p2);

assign select_ln414_fu_230_p3 = ((icmp_ln414_fu_210_p2[0:0] === 1'b1) ? 7'd63 : tmp_40_fu_216_p3);

assign select_ln73_fu_466_p3 = ((tmp_last_V_reg_534[0:0] === 1'b1) ? 16'd0 : ap_phi_reg_pp0_iter1_packetHeader_idx_0_i_reg_153);

assign shl_ln414_1_fu_296_p2 = 64'd18446744073709551615 << zext_ln414_2_fu_264_p1;

assign shl_ln414_fu_272_p2 = rx_ip2udpFifo_V_data_dout << zext_ln414_1_fu_260_p1;

assign sub_ln414_1_fu_246_p2 = (7'd63 - tmp_40_fu_216_p3);

assign sub_ln414_fu_224_p2 = (7'd63 - tmp_40_fu_216_p3);

assign tmp_40_fu_216_p3 = {{trunc_ln64_fu_198_p1}, {6'd0}};

integer ap_tvar_int_0;

always @ (shl_ln414_fu_272_p2) begin
    for (ap_tvar_int_0 = 64 - 1; ap_tvar_int_0 >= 0; ap_tvar_int_0 = ap_tvar_int_0 - 1) begin
        if (ap_tvar_int_0 > 63 - 0) begin
            tmp_41_fu_278_p4[ap_tvar_int_0] = 1'b0;
        end else begin
            tmp_41_fu_278_p4[ap_tvar_int_0] = shl_ln414_fu_272_p2[63 - ap_tvar_int_0];
        end
    end
end

assign tmp_nbreadreq_fu_98_p5 = (rx_ip2udpFifo_V_last_empty_n & rx_ip2udpFifo_V_keep_empty_n & rx_ip2udpFifo_V_data_empty_n);

assign tmp_valid_fu_491_p2 = ((dstPort_V_fu_485_p3 == 16'd4791) ? 1'b1 : 1'b0);

assign trunc_ln647_fu_431_p1 = pu_header_header_V[7:0];

assign trunc_ln64_fu_198_p1 = ap_sig_allocacmp_pu_header_idx_load[0:0];

assign xor_ln414_fu_314_p2 = (64'd18446744073709551615 ^ and_ln414_fu_308_p2);

assign xor_ln73_fu_455_p2 = (tmp_last_V_reg_534 ^ 1'd1);

assign zext_ln414_1_fu_260_p1 = select_ln414_4_fu_238_p3;

assign zext_ln414_2_fu_264_p1 = select_ln414_fu_230_p3;

assign zext_ln414_3_fu_268_p1 = select_ln414_5_fu_252_p3;

assign zext_ln414_fu_206_p1 = ap_sig_allocacmp_pu_header_idx_load;

endmodule //process_udp_64_1989
