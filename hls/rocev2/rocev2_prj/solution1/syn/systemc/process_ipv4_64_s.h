// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
// Version: 2019.2.1
// Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

#ifndef _process_ipv4_64_s_HH_
#define _process_ipv4_64_s_HH_

#include "systemc.h"
#include "AESL_pkg.h"


namespace ap_rtl {

struct process_ipv4_64_s : public sc_module {
    // Port declarations 34
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_in< sc_logic > ap_start;
    sc_out< sc_logic > ap_done;
    sc_in< sc_logic > ap_continue;
    sc_out< sc_logic > ap_idle;
    sc_out< sc_logic > ap_ready;
    sc_in< sc_lv<64> > rx_crc2ipFifo_V_data_dout;
    sc_in< sc_logic > rx_crc2ipFifo_V_data_empty_n;
    sc_out< sc_logic > rx_crc2ipFifo_V_data_read;
    sc_in< sc_lv<8> > rx_crc2ipFifo_V_keep_dout;
    sc_in< sc_logic > rx_crc2ipFifo_V_keep_empty_n;
    sc_out< sc_logic > rx_crc2ipFifo_V_keep_read;
    sc_in< sc_lv<1> > rx_crc2ipFifo_V_last_dout;
    sc_in< sc_logic > rx_crc2ipFifo_V_last_empty_n;
    sc_out< sc_logic > rx_crc2ipFifo_V_last_read;
    sc_out< sc_lv<64> > rx_process2dropFifo_1_5_din;
    sc_in< sc_logic > rx_process2dropFifo_1_5_full_n;
    sc_out< sc_logic > rx_process2dropFifo_1_5_write;
    sc_out< sc_lv<8> > rx_process2dropFifo_2_4_din;
    sc_in< sc_logic > rx_process2dropFifo_2_4_full_n;
    sc_out< sc_logic > rx_process2dropFifo_2_4_write;
    sc_out< sc_lv<1> > rx_process2dropFifo_s_6_din;
    sc_in< sc_logic > rx_process2dropFifo_s_6_full_n;
    sc_out< sc_logic > rx_process2dropFifo_s_6_write;
    sc_out< sc_lv<4> > rx_process2dropLengt_1_din;
    sc_in< sc_logic > rx_process2dropLengt_1_full_n;
    sc_out< sc_logic > rx_process2dropLengt_1_write;
    sc_out< sc_lv<32> > rx_ip2udpMetaFifo_V_s_din;
    sc_in< sc_logic > rx_ip2udpMetaFifo_V_s_full_n;
    sc_out< sc_logic > rx_ip2udpMetaFifo_V_s_write;
    sc_out< sc_lv<16> > rx_ip2udpMetaFifo_V_1_din;
    sc_in< sc_logic > rx_ip2udpMetaFifo_V_1_full_n;
    sc_out< sc_logic > rx_ip2udpMetaFifo_V_1_write;


    // Module declarations
    process_ipv4_64_s(sc_module_name name);
    SC_HAS_PROCESS(process_ipv4_64_s);

    ~process_ipv4_64_s();

    sc_trace_file* mVcdFile;

    sc_signal< sc_logic > ap_done_reg;
    sc_signal< sc_lv<1> > ap_CS_fsm;
    sc_signal< sc_logic > ap_CS_fsm_pp0_stage0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter1;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter2;
    sc_signal< sc_logic > ap_idle_pp0;
    sc_signal< sc_logic > io_acc_block_signal_op6;
    sc_signal< sc_lv<1> > tmp_nbreadreq_fu_106_p5;
    sc_signal< bool > ap_block_state1_pp0_stage0_iter0;
    sc_signal< bool > ap_block_state2_pp0_stage0_iter1;
    sc_signal< sc_logic > io_acc_block_signal_op70;
    sc_signal< sc_lv<1> > tmp_reg_533;
    sc_signal< sc_lv<1> > tmp_reg_533_pp0_iter1_reg;
    sc_signal< sc_lv<1> > or_ln73_reg_592;
    sc_signal< bool > ap_predicate_op70_write_state3;
    sc_signal< sc_lv<1> > metaWritten_3_load_reg_596;
    sc_signal< bool > ap_predicate_op74_write_state3;
    sc_signal< sc_logic > io_acc_block_signal_op79;
    sc_signal< bool > ap_predicate_op79_write_state3;
    sc_signal< bool > ap_block_state3_pp0_stage0_iter2;
    sc_signal< bool > ap_block_pp0_stage0_11001;
    sc_signal< sc_lv<1> > header_ready;
    sc_signal< sc_lv<16> > header_idx_3;
    sc_signal< sc_lv<160> > header_header_V_6;
    sc_signal< sc_lv<1> > metaWritten_3;
    sc_signal< sc_lv<4> > headerWordsDropped_V;
    sc_signal< sc_logic > rx_crc2ipFifo_V_data_blk_n;
    sc_signal< bool > ap_block_pp0_stage0;
    sc_signal< sc_logic > rx_crc2ipFifo_V_keep_blk_n;
    sc_signal< sc_logic > rx_crc2ipFifo_V_last_blk_n;
    sc_signal< sc_logic > rx_process2dropFifo_1_5_blk_n;
    sc_signal< sc_logic > rx_process2dropFifo_2_4_blk_n;
    sc_signal< sc_logic > rx_process2dropFifo_s_6_blk_n;
    sc_signal< sc_logic > rx_process2dropLengt_1_blk_n;
    sc_signal< sc_logic > rx_ip2udpMetaFifo_V_s_blk_n;
    sc_signal< sc_logic > rx_ip2udpMetaFifo_V_1_blk_n;
    sc_signal< sc_lv<64> > tmp_data_V_reg_537;
    sc_signal< sc_lv<64> > tmp_data_V_reg_537_pp0_iter1_reg;
    sc_signal< sc_lv<8> > tmp_keep_V_reg_543;
    sc_signal< sc_lv<8> > tmp_keep_V_reg_543_pp0_iter1_reg;
    sc_signal< sc_lv<1> > tmp_last_V_reg_548;
    sc_signal< sc_lv<1> > tmp_last_V_reg_548_pp0_iter1_reg;
    sc_signal< sc_lv<1> > header_ready_load_load_fu_206_p1;
    sc_signal< sc_lv<1> > header_ready_load_reg_557;
    sc_signal< sc_lv<16> > header_idx_3_load_reg_562;
    sc_signal< sc_lv<1> > icmp_ln58_3_fu_252_p2;
    sc_signal< sc_lv<1> > icmp_ln58_3_reg_568;
    sc_signal< sc_lv<8> > trunc_ln58_3_fu_258_p1;
    sc_signal< sc_lv<8> > trunc_ln58_3_reg_576;
    sc_signal< sc_lv<1> > xor_ln58_fu_262_p2;
    sc_signal< sc_lv<16> > add_ln67_fu_268_p2;
    sc_signal< sc_lv<1> > or_ln73_fu_406_p2;
    sc_signal< sc_lv<1> > metaWritten_3_load_load_fu_411_p1;
    sc_signal< sc_lv<1> > xor_ln63_fu_415_p2;
    sc_signal< sc_lv<1> > xor_ln63_reg_600;
    sc_signal< bool > ap_block_pp0_stage0_subdone;
    sc_signal< sc_lv<16> > ap_phi_reg_pp0_iter0_phi_ln73_reg_158;
    sc_signal< sc_lv<16> > ap_phi_reg_pp0_iter1_phi_ln73_reg_158;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter0_write_flag_1_i_i_reg_167;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter1_write_flag_1_i_i_reg_167;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter0_metaWritten_5_flag_1_reg_178;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter1_metaWritten_5_flag_1_reg_178;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter2_metaWritten_5_flag_1_reg_178;
    sc_signal< sc_lv<1> > and_ln63_fu_420_p2;
    sc_signal< sc_lv<1> > ap_sig_allocacmp_header_ready_load;
    sc_signal< sc_lv<16> > select_ln63_fu_426_p3;
    sc_signal< sc_lv<16> > ap_sig_allocacmp_header_idx_3_load;
    sc_signal< sc_lv<160> > or_ln58_fu_394_p2;
    sc_signal< sc_lv<1> > or_ln63_fu_510_p2;
    sc_signal< sc_lv<1> > ap_sig_allocacmp_metaWritten_3_load;
    sc_signal< sc_lv<4> > select_ln63_1_fu_515_p3;
    sc_signal< bool > ap_block_pp0_stage0_01001;
    sc_signal< sc_lv<22> > Lo_assign_fu_214_p3;
    sc_signal< sc_lv<23> > zext_ln58_fu_222_p1;
    sc_signal< sc_lv<23> > add_ln58_fu_226_p2;
    sc_signal< sc_lv<1> > icmp_ln58_fu_232_p2;
    sc_signal< sc_lv<22> > or_ln60_fu_238_p2;
    sc_signal< sc_lv<22> > select_ln58_fu_244_p3;
    sc_signal< sc_lv<2> > trunc_ln58_fu_274_p1;
    sc_signal< sc_lv<8> > tmp_42_fu_284_p3;
    sc_signal< sc_lv<8> > sub_ln58_fu_292_p2;
    sc_signal< sc_lv<8> > select_ln58_9_fu_298_p3;
    sc_signal< sc_lv<8> > select_ln58_11_fu_310_p3;
    sc_signal< sc_lv<8> > select_ln58_10_fu_304_p3;
    sc_signal< sc_lv<8> > sub_ln58_3_fu_317_p2;
    sc_signal< sc_lv<160> > tmp_V_4_fu_281_p1;
    sc_signal< sc_lv<160> > zext_ln58_6_fu_323_p1;
    sc_signal< sc_lv<160> > shl_ln58_fu_335_p2;
    sc_signal< sc_lv<160> > tmp_43_fu_341_p4;
    sc_signal< sc_lv<160> > zext_ln58_7_fu_327_p1;
    sc_signal< sc_lv<160> > zext_ln58_8_fu_331_p1;
    sc_signal< sc_lv<160> > shl_ln58_3_fu_358_p2;
    sc_signal< sc_lv<160> > lshr_ln58_fu_364_p2;
    sc_signal< sc_lv<160> > and_ln58_fu_370_p2;
    sc_signal< sc_lv<160> > xor_ln58_2_fu_376_p2;
    sc_signal< sc_lv<160> > select_ln58_12_fu_351_p3;
    sc_signal< sc_lv<160> > and_ln58_5_fu_382_p2;
    sc_signal< sc_lv<160> > and_ln58_6_fu_388_p2;
    sc_signal< sc_lv<4> > trunc_ln647_fu_453_p1;
    sc_signal< sc_lv<8> > p_Result_71_1_i_i_s_fu_485_p4;
    sc_signal< sc_lv<8> > p_Result_71_i_i_i_fu_475_p4;
    sc_signal< sc_lv<4> > add_ln700_fu_504_p2;
    sc_signal< sc_lv<1> > ap_NS_fsm;
    sc_signal< sc_logic > ap_idle_pp0_0to1;
    sc_signal< sc_logic > ap_reset_idle_pp0;
    sc_signal< sc_logic > ap_enable_pp0;
    sc_signal< bool > ap_condition_122;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<1> ap_ST_fsm_pp0_stage0;
    static const sc_lv<32> ap_const_lv32_0;
    static const bool ap_const_boolean_1;
    static const sc_lv<1> ap_const_lv1_1;
    static const sc_lv<1> ap_const_lv1_0;
    static const bool ap_const_boolean_0;
    static const sc_lv<16> ap_const_lv16_0;
    static const sc_lv<4> ap_const_lv4_0;
    static const sc_lv<6> ap_const_lv6_0;
    static const sc_lv<23> ap_const_lv23_40;
    static const sc_lv<23> ap_const_lv23_A0;
    static const sc_lv<22> ap_const_lv22_3F;
    static const sc_lv<22> ap_const_lv22_9F;
    static const sc_lv<16> ap_const_lv16_1;
    static const sc_lv<8> ap_const_lv8_9F;
    static const sc_lv<32> ap_const_lv32_9F;
    static const sc_lv<160> ap_const_lv160_lc_2;
    static const sc_lv<32> ap_const_lv32_60;
    static const sc_lv<32> ap_const_lv32_7F;
    static const sc_lv<32> ap_const_lv32_18;
    static const sc_lv<32> ap_const_lv32_1F;
    static const sc_lv<32> ap_const_lv32_10;
    static const sc_lv<32> ap_const_lv32_17;
    static const sc_lv<4> ap_const_lv4_2;
    // Thread declarations
    void thread_ap_clk_no_reset_();
    void thread_Lo_assign_fu_214_p3();
    void thread_add_ln58_fu_226_p2();
    void thread_add_ln67_fu_268_p2();
    void thread_add_ln700_fu_504_p2();
    void thread_and_ln58_5_fu_382_p2();
    void thread_and_ln58_6_fu_388_p2();
    void thread_and_ln58_fu_370_p2();
    void thread_and_ln63_fu_420_p2();
    void thread_ap_CS_fsm_pp0_stage0();
    void thread_ap_block_pp0_stage0();
    void thread_ap_block_pp0_stage0_01001();
    void thread_ap_block_pp0_stage0_11001();
    void thread_ap_block_pp0_stage0_subdone();
    void thread_ap_block_state1_pp0_stage0_iter0();
    void thread_ap_block_state2_pp0_stage0_iter1();
    void thread_ap_block_state3_pp0_stage0_iter2();
    void thread_ap_condition_122();
    void thread_ap_done();
    void thread_ap_enable_pp0();
    void thread_ap_enable_reg_pp0_iter0();
    void thread_ap_idle();
    void thread_ap_idle_pp0();
    void thread_ap_idle_pp0_0to1();
    void thread_ap_phi_reg_pp0_iter0_metaWritten_5_flag_1_reg_178();
    void thread_ap_phi_reg_pp0_iter0_phi_ln73_reg_158();
    void thread_ap_phi_reg_pp0_iter0_write_flag_1_i_i_reg_167();
    void thread_ap_predicate_op70_write_state3();
    void thread_ap_predicate_op74_write_state3();
    void thread_ap_predicate_op79_write_state3();
    void thread_ap_ready();
    void thread_ap_reset_idle_pp0();
    void thread_ap_sig_allocacmp_header_idx_3_load();
    void thread_ap_sig_allocacmp_header_ready_load();
    void thread_ap_sig_allocacmp_metaWritten_3_load();
    void thread_header_ready_load_load_fu_206_p1();
    void thread_icmp_ln58_3_fu_252_p2();
    void thread_icmp_ln58_fu_232_p2();
    void thread_io_acc_block_signal_op6();
    void thread_io_acc_block_signal_op70();
    void thread_io_acc_block_signal_op79();
    void thread_lshr_ln58_fu_364_p2();
    void thread_metaWritten_3_load_load_fu_411_p1();
    void thread_or_ln58_fu_394_p2();
    void thread_or_ln60_fu_238_p2();
    void thread_or_ln63_fu_510_p2();
    void thread_or_ln73_fu_406_p2();
    void thread_p_Result_71_1_i_i_s_fu_485_p4();
    void thread_p_Result_71_i_i_i_fu_475_p4();
    void thread_rx_crc2ipFifo_V_data_blk_n();
    void thread_rx_crc2ipFifo_V_data_read();
    void thread_rx_crc2ipFifo_V_keep_blk_n();
    void thread_rx_crc2ipFifo_V_keep_read();
    void thread_rx_crc2ipFifo_V_last_blk_n();
    void thread_rx_crc2ipFifo_V_last_read();
    void thread_rx_ip2udpMetaFifo_V_1_blk_n();
    void thread_rx_ip2udpMetaFifo_V_1_din();
    void thread_rx_ip2udpMetaFifo_V_1_write();
    void thread_rx_ip2udpMetaFifo_V_s_blk_n();
    void thread_rx_ip2udpMetaFifo_V_s_din();
    void thread_rx_ip2udpMetaFifo_V_s_write();
    void thread_rx_process2dropFifo_1_5_blk_n();
    void thread_rx_process2dropFifo_1_5_din();
    void thread_rx_process2dropFifo_1_5_write();
    void thread_rx_process2dropFifo_2_4_blk_n();
    void thread_rx_process2dropFifo_2_4_din();
    void thread_rx_process2dropFifo_2_4_write();
    void thread_rx_process2dropFifo_s_6_blk_n();
    void thread_rx_process2dropFifo_s_6_din();
    void thread_rx_process2dropFifo_s_6_write();
    void thread_rx_process2dropLengt_1_blk_n();
    void thread_rx_process2dropLengt_1_din();
    void thread_rx_process2dropLengt_1_write();
    void thread_select_ln58_10_fu_304_p3();
    void thread_select_ln58_11_fu_310_p3();
    void thread_select_ln58_12_fu_351_p3();
    void thread_select_ln58_9_fu_298_p3();
    void thread_select_ln58_fu_244_p3();
    void thread_select_ln63_1_fu_515_p3();
    void thread_select_ln63_fu_426_p3();
    void thread_shl_ln58_3_fu_358_p2();
    void thread_shl_ln58_fu_335_p2();
    void thread_sub_ln58_3_fu_317_p2();
    void thread_sub_ln58_fu_292_p2();
    void thread_tmp_42_fu_284_p3();
    void thread_tmp_43_fu_341_p4();
    void thread_tmp_V_4_fu_281_p1();
    void thread_tmp_nbreadreq_fu_106_p5();
    void thread_trunc_ln58_3_fu_258_p1();
    void thread_trunc_ln58_fu_274_p1();
    void thread_trunc_ln647_fu_453_p1();
    void thread_xor_ln58_2_fu_376_p2();
    void thread_xor_ln58_fu_262_p2();
    void thread_xor_ln63_fu_415_p2();
    void thread_zext_ln58_6_fu_323_p1();
    void thread_zext_ln58_7_fu_327_p1();
    void thread_zext_ln58_8_fu_331_p1();
    void thread_zext_ln58_fu_222_p1();
    void thread_ap_NS_fsm();
};

}

using namespace ap_rtl;

#endif
