// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
// Version: 2019.2.1
// Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

#ifndef _state_table_HH_
#define _state_table_HH_

#include "systemc.h"
#include "AESL_pkg.h"

#include "state_table_statefYi.h"
#include "state_table_statehbi.h"

namespace ap_rtl {

struct state_table : public sc_module {
    // Port declarations 25
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_in< sc_logic > ap_start;
    sc_out< sc_logic > ap_done;
    sc_in< sc_logic > ap_continue;
    sc_out< sc_logic > ap_idle;
    sc_out< sc_logic > ap_ready;
    sc_in< sc_lv<45> > rxIbh2stateTable_upd_1_dout;
    sc_in< sc_logic > rxIbh2stateTable_upd_1_empty_n;
    sc_out< sc_logic > rxIbh2stateTable_upd_1_read;
    sc_in< sc_lv<41> > txIbh2stateTable_upd_1_dout;
    sc_in< sc_logic > txIbh2stateTable_upd_1_empty_n;
    sc_out< sc_logic > txIbh2stateTable_upd_1_read;
    sc_in< sc_lv<68> > qpi2stateTable_upd_r_1_dout;
    sc_in< sc_logic > qpi2stateTable_upd_r_1_empty_n;
    sc_out< sc_logic > qpi2stateTable_upd_r_1_read;
    sc_out< sc_lv<123> > stateTable2qpi_rsp_V_din;
    sc_in< sc_logic > stateTable2qpi_rsp_V_full_n;
    sc_out< sc_logic > stateTable2qpi_rsp_V_write;
    sc_out< sc_lv<123> > stateTable2txIbh_rsp_1_din;
    sc_in< sc_logic > stateTable2txIbh_rsp_1_full_n;
    sc_out< sc_logic > stateTable2txIbh_rsp_1_write;
    sc_out< sc_lv<75> > stateTable2rxIbh_rsp_1_din;
    sc_in< sc_logic > stateTable2rxIbh_rsp_1_full_n;
    sc_out< sc_logic > stateTable2rxIbh_rsp_1_write;


    // Module declarations
    state_table(sc_module_name name);
    SC_HAS_PROCESS(state_table);

    ~state_table();

    sc_trace_file* mVcdFile;

    state_table_statefYi* state_table_req_old_1_U;
    state_table_statefYi* state_table_resp_eps_U;
    state_table_statehbi* state_table_retryCou_U;
    state_table_statefYi* state_table_resp_old_U;
    state_table_statefYi* state_table_req_next_U;
    state_table_statefYi* state_table_req_old_s_U;
    sc_signal< sc_logic > ap_done_reg;
    sc_signal< sc_lv<1> > ap_CS_fsm;
    sc_signal< sc_logic > ap_CS_fsm_pp0_stage0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter1;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter2;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter3;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter4;
    sc_signal< sc_logic > ap_idle_pp0;
    sc_signal< sc_lv<1> > tmp_nbreadreq_fu_106_p3;
    sc_signal< bool > ap_block_state1_pp0_stage0_iter0;
    sc_signal< sc_lv<1> > tmp_reg_667;
    sc_signal< sc_lv<1> > tmp_9_nbreadreq_fu_120_p3;
    sc_signal< bool > ap_predicate_op19_read_state2;
    sc_signal< bool > ap_block_state2_pp0_stage0_iter1;
    sc_signal< sc_lv<1> > tmp_reg_667_pp0_iter1_reg;
    sc_signal< sc_lv<1> > tmp_9_reg_695;
    sc_signal< sc_lv<1> > tmp_14_nbreadreq_fu_134_p3;
    sc_signal< bool > ap_predicate_op26_read_state3;
    sc_signal< bool > ap_block_state3_pp0_stage0_iter2;
    sc_signal< bool > ap_block_state4_pp0_stage0_iter3;
    sc_signal< sc_lv<1> > tmp_reg_667_pp0_iter3_reg;
    sc_signal< sc_lv<1> > tmp_9_reg_695_pp0_iter3_reg;
    sc_signal< sc_lv<1> > tmp_14_reg_713;
    sc_signal< sc_lv<1> > tmp_14_reg_713_pp0_iter3_reg;
    sc_signal< sc_lv<1> > tmp_15_reg_717;
    sc_signal< sc_lv<1> > tmp_15_reg_717_pp0_iter3_reg;
    sc_signal< bool > ap_predicate_op117_write_state5;
    sc_signal< sc_lv<1> > tmp_13_reg_709;
    sc_signal< sc_lv<1> > tmp_13_reg_709_pp0_iter3_reg;
    sc_signal< bool > ap_predicate_op123_write_state5;
    sc_signal< sc_lv<1> > tmp_12_reg_691;
    sc_signal< sc_lv<1> > tmp_12_reg_691_pp0_iter3_reg;
    sc_signal< sc_lv<1> > tmp_11_reg_687;
    sc_signal< sc_lv<1> > tmp_11_reg_687_pp0_iter3_reg;
    sc_signal< bool > ap_predicate_op129_write_state5;
    sc_signal< bool > ap_predicate_op133_write_state5;
    sc_signal< bool > ap_block_state5_pp0_stage0_iter4;
    sc_signal< bool > ap_block_pp0_stage0_11001;
    sc_signal< sc_lv<9> > state_table_req_old_1_address0;
    sc_signal< sc_logic > state_table_req_old_1_ce0;
    sc_signal< sc_lv<24> > state_table_req_old_1_q0;
    sc_signal< sc_lv<9> > state_table_req_old_1_address1;
    sc_signal< sc_logic > state_table_req_old_1_ce1;
    sc_signal< sc_logic > state_table_req_old_1_we1;
    sc_signal< sc_lv<24> > state_table_req_old_1_d1;
    sc_signal< sc_lv<9> > state_table_resp_eps_address0;
    sc_signal< sc_logic > state_table_resp_eps_ce0;
    sc_signal< sc_lv<24> > state_table_resp_eps_q0;
    sc_signal< sc_lv<9> > state_table_resp_eps_address1;
    sc_signal< sc_logic > state_table_resp_eps_ce1;
    sc_signal< sc_logic > state_table_resp_eps_we1;
    sc_signal< sc_lv<24> > state_table_resp_eps_d1;
    sc_signal< sc_lv<9> > state_table_retryCou_address0;
    sc_signal< sc_logic > state_table_retryCou_ce0;
    sc_signal< sc_lv<3> > state_table_retryCou_q0;
    sc_signal< sc_lv<9> > state_table_retryCou_address1;
    sc_signal< sc_logic > state_table_retryCou_ce1;
    sc_signal< sc_logic > state_table_retryCou_we1;
    sc_signal< sc_lv<3> > state_table_retryCou_d1;
    sc_signal< sc_lv<9> > state_table_resp_old_address0;
    sc_signal< sc_logic > state_table_resp_old_ce0;
    sc_signal< sc_lv<24> > state_table_resp_old_q0;
    sc_signal< sc_lv<9> > state_table_resp_old_address1;
    sc_signal< sc_logic > state_table_resp_old_ce1;
    sc_signal< sc_logic > state_table_resp_old_we1;
    sc_signal< sc_lv<24> > state_table_resp_old_d1;
    sc_signal< sc_lv<9> > state_table_req_next_address0;
    sc_signal< sc_logic > state_table_req_next_ce0;
    sc_signal< sc_lv<24> > state_table_req_next_q0;
    sc_signal< sc_lv<9> > state_table_req_next_address1;
    sc_signal< sc_logic > state_table_req_next_ce1;
    sc_signal< sc_logic > state_table_req_next_we1;
    sc_signal< sc_lv<24> > state_table_req_next_d1;
    sc_signal< sc_lv<9> > state_table_req_old_s_address0;
    sc_signal< sc_logic > state_table_req_old_s_ce0;
    sc_signal< sc_lv<24> > state_table_req_old_s_q0;
    sc_signal< sc_lv<9> > state_table_req_old_s_address1;
    sc_signal< sc_logic > state_table_req_old_s_ce1;
    sc_signal< sc_logic > state_table_req_old_s_we1;
    sc_signal< sc_lv<24> > state_table_req_old_s_d1;
    sc_signal< sc_logic > rxIbh2stateTable_upd_1_blk_n;
    sc_signal< bool > ap_block_pp0_stage0;
    sc_signal< sc_logic > stateTable2rxIbh_rsp_1_blk_n;
    sc_signal< sc_logic > txIbh2stateTable_upd_1_blk_n;
    sc_signal< sc_logic > stateTable2txIbh_rsp_1_blk_n;
    sc_signal< sc_logic > qpi2stateTable_upd_r_1_blk_n;
    sc_signal< sc_logic > stateTable2qpi_rsp_V_blk_n;
    sc_signal< sc_lv<24> > reg_441;
    sc_signal< sc_lv<1> > tmp_reg_667_pp0_iter2_reg;
    sc_signal< sc_lv<1> > tmp_9_reg_695_pp0_iter2_reg;
    sc_signal< sc_lv<1> > tmp_13_reg_709_pp0_iter2_reg;
    sc_signal< sc_lv<1> > tmp_12_reg_691_pp0_iter2_reg;
    sc_signal< sc_lv<24> > reg_445;
    sc_signal< sc_lv<24> > reg_449;
    sc_signal< sc_lv<24> > reg_453;
    sc_signal< sc_lv<24> > reg_457;
    sc_signal< sc_lv<3> > reg_461;
    sc_signal< sc_lv<16> > p_Val2_s_fu_465_p1;
    sc_signal< sc_lv<16> > p_Val2_s_reg_671;
    sc_signal< sc_lv<16> > p_Val2_s_reg_671_pp0_iter1_reg;
    sc_signal< sc_lv<24> > tmp_epsn_V_load_new_s_reg_676;
    sc_signal< sc_lv<24> > tmp_epsn_V_load_new_s_reg_676_pp0_iter1_reg;
    sc_signal< sc_lv<3> > tmp_retryCounter_V_l_reg_682;
    sc_signal< sc_lv<3> > tmp_retryCounter_V_l_reg_682_pp0_iter1_reg;
    sc_signal< sc_lv<1> > tmp_11_reg_687_pp0_iter1_reg;
    sc_signal< sc_lv<1> > tmp_11_reg_687_pp0_iter2_reg;
    sc_signal< sc_lv<1> > tmp_12_reg_691_pp0_iter1_reg;
    sc_signal< sc_lv<16> > trunc_ln321_fu_505_p1;
    sc_signal< sc_lv<16> > trunc_ln321_reg_699;
    sc_signal< sc_lv<24> > tmp_psn_V_load_new_i_reg_704;
    sc_signal< sc_lv<1> > tmp_15_fu_556_p3;
    sc_signal< bool > ap_block_pp0_stage0_subdone;
    sc_signal< sc_lv<64> > zext_ln544_2_fu_564_p1;
    sc_signal< sc_lv<64> > zext_ln544_1_fu_579_p1;
    sc_signal< sc_lv<64> > zext_ln544_fu_589_p1;
    sc_signal< bool > ap_block_pp0_stage0_01001;
    sc_signal< sc_lv<75> > tmp_2_fu_635_p5;
    sc_signal< sc_lv<75> > tmp_1_fu_654_p5;
    sc_signal< sc_lv<16> > p_Val2_1_fu_527_p1;
    sc_signal< sc_lv<24> > tmp_max_forward_V_fu_648_p2;
    sc_signal< sc_lv<1> > ap_NS_fsm;
    sc_signal< sc_logic > ap_idle_pp0_0to3;
    sc_signal< sc_logic > ap_reset_idle_pp0;
    sc_signal< sc_logic > ap_enable_pp0;
    sc_signal< bool > ap_condition_329;
    sc_signal< bool > ap_condition_344;
    sc_signal< bool > ap_condition_577;
    sc_signal< bool > ap_condition_575;
    sc_signal< bool > ap_condition_381;
    sc_signal< bool > ap_condition_404;
    sc_signal< bool > ap_condition_399;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<1> ap_ST_fsm_pp0_stage0;
    static const sc_lv<32> ap_const_lv32_0;
    static const bool ap_const_boolean_1;
    static const sc_lv<1> ap_const_lv1_1;
    static const sc_lv<1> ap_const_lv1_0;
    static const bool ap_const_boolean_0;
    static const sc_lv<3> ap_const_lv3_7;
    static const sc_lv<32> ap_const_lv32_10;
    static const sc_lv<32> ap_const_lv32_27;
    static const sc_lv<32> ap_const_lv32_28;
    static const sc_lv<32> ap_const_lv32_2A;
    static const sc_lv<32> ap_const_lv32_2B;
    static const sc_lv<32> ap_const_lv32_2C;
    static const sc_lv<32> ap_const_lv32_13;
    static const sc_lv<32> ap_const_lv32_42;
    static const sc_lv<32> ap_const_lv32_43;
    static const sc_lv<24> ap_const_lv24_FFFFFF;
    static const sc_lv<3> ap_const_lv3_0;
    // Thread declarations
    void thread_ap_clk_no_reset_();
    void thread_ap_CS_fsm_pp0_stage0();
    void thread_ap_block_pp0_stage0();
    void thread_ap_block_pp0_stage0_01001();
    void thread_ap_block_pp0_stage0_11001();
    void thread_ap_block_pp0_stage0_subdone();
    void thread_ap_block_state1_pp0_stage0_iter0();
    void thread_ap_block_state2_pp0_stage0_iter1();
    void thread_ap_block_state3_pp0_stage0_iter2();
    void thread_ap_block_state4_pp0_stage0_iter3();
    void thread_ap_block_state5_pp0_stage0_iter4();
    void thread_ap_condition_329();
    void thread_ap_condition_344();
    void thread_ap_condition_381();
    void thread_ap_condition_399();
    void thread_ap_condition_404();
    void thread_ap_condition_575();
    void thread_ap_condition_577();
    void thread_ap_done();
    void thread_ap_enable_pp0();
    void thread_ap_enable_reg_pp0_iter0();
    void thread_ap_idle();
    void thread_ap_idle_pp0();
    void thread_ap_idle_pp0_0to3();
    void thread_ap_predicate_op117_write_state5();
    void thread_ap_predicate_op123_write_state5();
    void thread_ap_predicate_op129_write_state5();
    void thread_ap_predicate_op133_write_state5();
    void thread_ap_predicate_op19_read_state2();
    void thread_ap_predicate_op26_read_state3();
    void thread_ap_ready();
    void thread_ap_reset_idle_pp0();
    void thread_p_Val2_1_fu_527_p1();
    void thread_p_Val2_s_fu_465_p1();
    void thread_qpi2stateTable_upd_r_1_blk_n();
    void thread_qpi2stateTable_upd_r_1_read();
    void thread_rxIbh2stateTable_upd_1_blk_n();
    void thread_rxIbh2stateTable_upd_1_read();
    void thread_stateTable2qpi_rsp_V_blk_n();
    void thread_stateTable2qpi_rsp_V_din();
    void thread_stateTable2qpi_rsp_V_write();
    void thread_stateTable2rxIbh_rsp_1_blk_n();
    void thread_stateTable2rxIbh_rsp_1_din();
    void thread_stateTable2rxIbh_rsp_1_write();
    void thread_stateTable2txIbh_rsp_1_blk_n();
    void thread_stateTable2txIbh_rsp_1_din();
    void thread_stateTable2txIbh_rsp_1_write();
    void thread_state_table_req_next_address0();
    void thread_state_table_req_next_address1();
    void thread_state_table_req_next_ce0();
    void thread_state_table_req_next_ce1();
    void thread_state_table_req_next_d1();
    void thread_state_table_req_next_we1();
    void thread_state_table_req_old_1_address0();
    void thread_state_table_req_old_1_address1();
    void thread_state_table_req_old_1_ce0();
    void thread_state_table_req_old_1_ce1();
    void thread_state_table_req_old_1_d1();
    void thread_state_table_req_old_1_we1();
    void thread_state_table_req_old_s_address0();
    void thread_state_table_req_old_s_address1();
    void thread_state_table_req_old_s_ce0();
    void thread_state_table_req_old_s_ce1();
    void thread_state_table_req_old_s_d1();
    void thread_state_table_req_old_s_we1();
    void thread_state_table_resp_eps_address0();
    void thread_state_table_resp_eps_address1();
    void thread_state_table_resp_eps_ce0();
    void thread_state_table_resp_eps_ce1();
    void thread_state_table_resp_eps_d1();
    void thread_state_table_resp_eps_we1();
    void thread_state_table_resp_old_address0();
    void thread_state_table_resp_old_address1();
    void thread_state_table_resp_old_ce0();
    void thread_state_table_resp_old_ce1();
    void thread_state_table_resp_old_d1();
    void thread_state_table_resp_old_we1();
    void thread_state_table_retryCou_address0();
    void thread_state_table_retryCou_address1();
    void thread_state_table_retryCou_ce0();
    void thread_state_table_retryCou_ce1();
    void thread_state_table_retryCou_d1();
    void thread_state_table_retryCou_we1();
    void thread_tmp_14_nbreadreq_fu_134_p3();
    void thread_tmp_15_fu_556_p3();
    void thread_tmp_1_fu_654_p5();
    void thread_tmp_2_fu_635_p5();
    void thread_tmp_9_nbreadreq_fu_120_p3();
    void thread_tmp_max_forward_V_fu_648_p2();
    void thread_tmp_nbreadreq_fu_106_p3();
    void thread_trunc_ln321_fu_505_p1();
    void thread_txIbh2stateTable_upd_1_blk_n();
    void thread_txIbh2stateTable_upd_1_read();
    void thread_zext_ln544_1_fu_579_p1();
    void thread_zext_ln544_2_fu_564_p1();
    void thread_zext_ln544_fu_589_p1();
    void thread_ap_NS_fsm();
};

}

using namespace ap_rtl;

#endif
