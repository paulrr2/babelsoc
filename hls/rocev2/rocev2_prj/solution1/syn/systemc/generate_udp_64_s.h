// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
// Version: 2019.2.1
// Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

#ifndef _generate_udp_64_s_HH_
#define _generate_udp_64_s_HH_

#include "systemc.h"
#include "AESL_pkg.h"


namespace ap_rtl {

struct generate_udp_64_s : public sc_module {
    // Port declarations 37
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_in< sc_logic > ap_start;
    sc_out< sc_logic > ap_done;
    sc_in< sc_logic > ap_continue;
    sc_out< sc_logic > ap_idle;
    sc_out< sc_logic > ap_ready;
    sc_in< sc_lv<64> > tx_shift2udpFifo_V_d_dout;
    sc_in< sc_logic > tx_shift2udpFifo_V_d_empty_n;
    sc_out< sc_logic > tx_shift2udpFifo_V_d_read;
    sc_in< sc_lv<8> > tx_shift2udpFifo_V_k_dout;
    sc_in< sc_logic > tx_shift2udpFifo_V_k_empty_n;
    sc_out< sc_logic > tx_shift2udpFifo_V_k_read;
    sc_in< sc_lv<1> > tx_shift2udpFifo_V_l_dout;
    sc_in< sc_logic > tx_shift2udpFifo_V_l_empty_n;
    sc_out< sc_logic > tx_shift2udpFifo_V_l_read;
    sc_in< sc_lv<16> > tx_udpMetaFifo_V_the_dout;
    sc_in< sc_logic > tx_udpMetaFifo_V_the_empty_n;
    sc_out< sc_logic > tx_udpMetaFifo_V_the_read;
    sc_in< sc_lv<16> > tx_udpMetaFifo_V_my_s_dout;
    sc_in< sc_logic > tx_udpMetaFifo_V_my_s_empty_n;
    sc_out< sc_logic > tx_udpMetaFifo_V_my_s_read;
    sc_in< sc_lv<16> > tx_udpMetaFifo_V_len_dout;
    sc_in< sc_logic > tx_udpMetaFifo_V_len_empty_n;
    sc_out< sc_logic > tx_udpMetaFifo_V_len_read;
    sc_in< sc_lv<1> > tx_udpMetaFifo_V_val_dout;
    sc_in< sc_logic > tx_udpMetaFifo_V_val_empty_n;
    sc_out< sc_logic > tx_udpMetaFifo_V_val_read;
    sc_out< sc_lv<64> > tx_udp2ipFifo_V_data_din;
    sc_in< sc_logic > tx_udp2ipFifo_V_data_full_n;
    sc_out< sc_logic > tx_udp2ipFifo_V_data_write;
    sc_out< sc_lv<8> > tx_udp2ipFifo_V_keep_din;
    sc_in< sc_logic > tx_udp2ipFifo_V_keep_full_n;
    sc_out< sc_logic > tx_udp2ipFifo_V_keep_write;
    sc_out< sc_lv<1> > tx_udp2ipFifo_V_last_din;
    sc_in< sc_logic > tx_udp2ipFifo_V_last_full_n;
    sc_out< sc_logic > tx_udp2ipFifo_V_last_write;


    // Module declarations
    generate_udp_64_s(sc_module_name name);
    SC_HAS_PROCESS(generate_udp_64_s);

    ~generate_udp_64_s();

    sc_trace_file* mVcdFile;

    sc_signal< sc_logic > ap_done_reg;
    sc_signal< sc_lv<1> > ap_CS_fsm;
    sc_signal< sc_logic > ap_CS_fsm_pp0_stage0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter1;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter2;
    sc_signal< sc_logic > ap_idle_pp0;
    sc_signal< sc_logic > io_acc_block_signal_op8;
    sc_signal< sc_lv<2> > state_4_load_load_fu_226_p1;
    sc_signal< sc_lv<1> > grp_nbreadreq_fu_136_p5;
    sc_signal< bool > ap_predicate_op8_read_state1;
    sc_signal< sc_logic > io_acc_block_signal_op14;
    sc_signal< bool > ap_predicate_op14_read_state1;
    sc_signal< sc_logic > io_acc_block_signal_op93;
    sc_signal< sc_lv<1> > tmp_nbreadreq_fu_158_p6;
    sc_signal< bool > ap_predicate_op93_read_state1;
    sc_signal< bool > ap_block_state1_pp0_stage0_iter0;
    sc_signal< bool > ap_block_state2_pp0_stage0_iter1;
    sc_signal< sc_logic > io_acc_block_signal_op138;
    sc_signal< sc_lv<2> > state_4_load_reg_957;
    sc_signal< sc_lv<2> > state_4_load_reg_957_pp0_iter1_reg;
    sc_signal< sc_lv<1> > tmp_94_reg_961;
    sc_signal< sc_lv<1> > tmp_94_reg_961_pp0_iter1_reg;
    sc_signal< bool > ap_predicate_op138_write_state3;
    sc_signal< sc_logic > io_acc_block_signal_op152;
    sc_signal< sc_lv<1> > tmp_93_reg_975;
    sc_signal< sc_lv<1> > tmp_93_reg_975_pp0_iter1_reg;
    sc_signal< bool > ap_predicate_op152_write_state3;
    sc_signal< sc_logic > io_acc_block_signal_op159;
    sc_signal< bool > ap_block_state3_pp0_stage0_iter2;
    sc_signal< bool > ap_block_pp0_stage0_11001;
    sc_signal< sc_lv<2> > state_4;
    sc_signal< sc_lv<16> > header_idx_4;
    sc_signal< sc_lv<64> > header_header_V_5;
    sc_signal< sc_logic > tx_udpMetaFifo_V_the_blk_n;
    sc_signal< bool > ap_block_pp0_stage0;
    sc_signal< sc_logic > tx_udpMetaFifo_V_my_s_blk_n;
    sc_signal< sc_logic > tx_udpMetaFifo_V_len_blk_n;
    sc_signal< sc_logic > tx_udpMetaFifo_V_val_blk_n;
    sc_signal< sc_logic > tx_udp2ipFifo_V_data_blk_n;
    sc_signal< sc_logic > tx_udp2ipFifo_V_keep_blk_n;
    sc_signal< sc_logic > tx_udp2ipFifo_V_last_blk_n;
    sc_signal< sc_logic > tx_shift2udpFifo_V_d_blk_n;
    sc_signal< sc_logic > tx_shift2udpFifo_V_k_blk_n;
    sc_signal< sc_logic > tx_shift2udpFifo_V_l_blk_n;
    sc_signal< sc_lv<8> > reg_221;
    sc_signal< sc_lv<8> > reg_221_pp0_iter1_reg;
    sc_signal< sc_lv<64> > tmp_data_V_34_reg_965;
    sc_signal< sc_lv<64> > tmp_data_V_34_reg_965_pp0_iter1_reg;
    sc_signal< sc_lv<1> > grp_fu_203_p1;
    sc_signal< sc_lv<1> > tmp_last_V_28_reg_970;
    sc_signal< sc_lv<1> > tmp_last_V_28_reg_970_pp0_iter1_reg;
    sc_signal< sc_lv<64> > empty_411_reg_979_0;
    sc_signal< sc_lv<64> > empty_411_reg_979_pp0_iter1_reg_0;
    sc_signal< sc_lv<1> > tmp_last_V_reg_984;
    sc_signal< sc_lv<1> > tmp_last_V_reg_984_pp0_iter1_reg;
    sc_signal< sc_lv<1> > icmp_ln76_2_fu_272_p2;
    sc_signal< sc_lv<1> > icmp_ln76_2_reg_989;
    sc_signal< sc_lv<1> > icmp_ln76_2_reg_989_pp0_iter1_reg;
    sc_signal< sc_lv<7> > sub_ln647_16_fu_354_p2;
    sc_signal< sc_lv<7> > sub_ln647_16_reg_994;
    sc_signal< sc_lv<7> > sub_ln647_16_reg_994_pp0_iter1_reg;
    sc_signal< sc_lv<64> > lshr_ln647_fu_364_p2;
    sc_signal< sc_lv<64> > lshr_ln647_reg_999;
    sc_signal< sc_lv<64> > lshr_ln647_reg_999_pp0_iter1_reg;
    sc_signal< sc_lv<7> > sub_ln647_19_fu_440_p2;
    sc_signal< sc_lv<7> > sub_ln647_19_reg_1004;
    sc_signal< sc_lv<7> > sub_ln647_19_reg_1004_pp0_iter1_reg;
    sc_signal< sc_lv<64> > lshr_ln647_9_fu_450_p2;
    sc_signal< sc_lv<64> > lshr_ln647_9_reg_1009;
    sc_signal< sc_lv<64> > lshr_ln647_9_reg_1009_pp0_iter1_reg;
    sc_signal< sc_lv<1> > and_ln82_2_fu_462_p2;
    sc_signal< sc_lv<1> > and_ln82_2_reg_1014;
    sc_signal< sc_lv<1> > and_ln82_2_reg_1014_pp0_iter1_reg;
    sc_signal< sc_lv<1> > trunc_ln76_fu_510_p1;
    sc_signal< sc_lv<1> > trunc_ln76_reg_1019;
    sc_signal< sc_lv<1> > icmp_ln82_3_fu_628_p2;
    sc_signal< sc_lv<1> > icmp_ln82_3_reg_1024;
    sc_signal< sc_lv<7> > trunc_ln82_fu_634_p1;
    sc_signal< sc_lv<7> > trunc_ln82_reg_1031;
    sc_signal< sc_lv<1> > or_ln887_fu_678_p2;
    sc_signal< sc_lv<7> > sub_ln82_3_fu_880_p2;
    sc_signal< sc_lv<7> > sub_ln82_3_reg_1045;
    sc_signal< sc_lv<64> > lshr_ln82_fu_890_p2;
    sc_signal< sc_lv<64> > lshr_ln82_reg_1050;
    sc_signal< bool > ap_block_pp0_stage0_subdone;
    sc_signal< sc_lv<2> > select_ln132_fu_496_p3;
    sc_signal< sc_lv<16> > select_ln82_5_fu_476_p3;
    sc_signal< sc_lv<16> > select_ln82_fu_612_p3;
    sc_signal< sc_lv<64> > p_Result_s_fu_798_p5;
    sc_signal< bool > ap_block_pp0_stage0_01001;
    sc_signal< sc_lv<64> > ret_V_fu_934_p3;
    sc_signal< sc_lv<64> > ret_V_7_fu_951_p2;
    sc_signal< sc_lv<22> > Lo_assign_3_fu_254_p3;
    sc_signal< sc_lv<23> > zext_ln76_2_fu_262_p1;
    sc_signal< sc_lv<23> > add_ln76_2_fu_266_p2;
    sc_signal< sc_lv<22> > or_ln78_2_fu_278_p2;
    sc_signal< sc_lv<1> > trunc_ln76_5_fu_250_p1;
    sc_signal< sc_lv<7> > tmp_95_fu_290_p3;
    sc_signal< sc_lv<7> > trunc_ln647_11_fu_298_p1;
    sc_signal< sc_lv<1> > icmp_ln647_fu_284_p2;
    sc_signal< sc_lv<7> > sub_ln647_fu_312_p2;
    sc_signal< sc_lv<7> > sub_ln647_15_fu_324_p2;
    sc_signal< sc_lv<64> > tmp_96_fu_302_p4;
    sc_signal< sc_lv<7> > sub_ln647_14_fu_318_p2;
    sc_signal< sc_lv<7> > select_ln647_fu_330_p3;
    sc_signal< sc_lv<7> > select_ln647_13_fu_346_p3;
    sc_signal< sc_lv<64> > select_ln647_12_fu_338_p3;
    sc_signal< sc_lv<64> > zext_ln647_fu_360_p1;
    sc_signal< sc_lv<26> > zext_ln647_9_fu_370_p1;
    sc_signal< sc_lv<1> > trunc_ln76_4_fu_246_p1;
    sc_signal< sc_lv<7> > tmp_97_fu_380_p3;
    sc_signal< sc_lv<1> > icmp_ln647_4_fu_374_p2;
    sc_signal< sc_lv<7> > add_ln647_fu_398_p2;
    sc_signal< sc_lv<7> > sub_ln647_18_fu_410_p2;
    sc_signal< sc_lv<64> > tmp_98_fu_388_p4;
    sc_signal< sc_lv<7> > sub_ln647_17_fu_404_p2;
    sc_signal< sc_lv<7> > select_ln647_14_fu_416_p3;
    sc_signal< sc_lv<7> > select_ln647_16_fu_432_p3;
    sc_signal< sc_lv<64> > select_ln647_15_fu_424_p3;
    sc_signal< sc_lv<64> > zext_ln647_10_fu_446_p1;
    sc_signal< sc_lv<1> > grp_fu_216_p2;
    sc_signal< sc_lv<1> > xor_ln76_2_fu_456_p2;
    sc_signal< sc_lv<16> > grp_fu_211_p2;
    sc_signal< sc_lv<16> > select_ln76_3_fu_468_p3;
    sc_signal< sc_lv<1> > xor_ln132_fu_490_p2;
    sc_signal< sc_lv<22> > Lo_assign_fu_514_p3;
    sc_signal< sc_lv<23> > zext_ln76_fu_522_p1;
    sc_signal< sc_lv<23> > add_ln76_fu_526_p2;
    sc_signal< sc_lv<22> > shl_ln_fu_544_p3;
    sc_signal< sc_lv<23> > zext_ln80_fu_552_p1;
    sc_signal< sc_lv<23> > sub_ln80_fu_556_p2;
    sc_signal< sc_lv<23> > sub_ln80_3_fu_570_p2;
    sc_signal< sc_lv<8> > trunc_ln80_3_fu_576_p4;
    sc_signal< sc_lv<1> > icmp_ln76_fu_532_p2;
    sc_signal< sc_lv<1> > xor_ln76_fu_592_p2;
    sc_signal< sc_lv<1> > and_ln82_fu_598_p2;
    sc_signal< sc_lv<16> > select_ln76_fu_604_p3;
    sc_signal< sc_lv<22> > or_ln78_fu_538_p2;
    sc_signal< sc_lv<22> > select_ln82_4_fu_620_p3;
    sc_signal< sc_lv<8> > sub_ln80_4_fu_586_p2;
    sc_signal< sc_lv<1> > tmp_88_fu_562_p3;
    sc_signal< sc_lv<5> > tmp_91_fu_644_p4;
    sc_signal< sc_lv<5> > tmp_92_fu_654_p4;
    sc_signal< sc_lv<5> > select_ln80_fu_664_p3;
    sc_signal< sc_lv<1> > icmp_ln887_fu_672_p2;
    sc_signal< sc_lv<8> > trunc_ln647_fu_718_p1;
    sc_signal< sc_lv<8> > p_Result_71_i_i_i_fu_708_p4;
    sc_signal< sc_lv<16> > agg_result_V_0_1_i_i_fu_722_p3;
    sc_signal< sc_lv<8> > trunc_ln647_9_fu_752_p1;
    sc_signal< sc_lv<8> > p_Result_71_i_i81_s_fu_742_p4;
    sc_signal< sc_lv<64> > p_Result_38_fu_730_p5;
    sc_signal< sc_lv<16> > agg_result_V_0_1_i_i_1_fu_756_p3;
    sc_signal< sc_lv<8> > trunc_ln647_10_fu_786_p1;
    sc_signal< sc_lv<8> > p_Result_71_i_i86_s_fu_776_p4;
    sc_signal< sc_lv<64> > p_Result_39_fu_764_p5;
    sc_signal< sc_lv<16> > agg_result_V_0_1_i_i_2_fu_790_p3;
    sc_signal< sc_lv<7> > tmp_89_fu_826_p3;
    sc_signal< sc_lv<7> > sub_ln82_fu_843_p2;
    sc_signal< sc_lv<7> > sub_ln82_2_fu_854_p2;
    sc_signal< sc_lv<64> > tmp_90_fu_833_p4;
    sc_signal< sc_lv<7> > sub_ln82_1_fu_848_p2;
    sc_signal< sc_lv<7> > select_ln82_6_fu_859_p3;
    sc_signal< sc_lv<7> > select_ln82_8_fu_873_p3;
    sc_signal< sc_lv<64> > select_ln82_7_fu_866_p3;
    sc_signal< sc_lv<64> > zext_ln82_fu_886_p1;
    sc_signal< sc_lv<64> > zext_ln647_8_fu_899_p1;
    sc_signal< sc_lv<64> > lshr_ln647_8_fu_902_p2;
    sc_signal< sc_lv<64> > zext_ln647_11_fu_913_p1;
    sc_signal< sc_lv<64> > lshr_ln647_10_fu_916_p2;
    sc_signal< sc_lv<64> > p_Result_40_fu_908_p2;
    sc_signal< sc_lv<64> > p_Result_37_fu_922_p2;
    sc_signal< sc_lv<64> > ret_V_5_fu_927_p3;
    sc_signal< sc_lv<64> > zext_ln82_1_fu_942_p1;
    sc_signal< sc_lv<64> > lshr_ln82_1_fu_945_p2;
    sc_signal< sc_lv<1> > ap_NS_fsm;
    sc_signal< sc_logic > ap_idle_pp0_0to1;
    sc_signal< sc_logic > ap_reset_idle_pp0;
    sc_signal< sc_logic > ap_enable_pp0;
    sc_signal< bool > ap_condition_155;
    sc_signal< bool > ap_condition_280;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<1> ap_ST_fsm_pp0_stage0;
    static const sc_lv<32> ap_const_lv32_0;
    static const bool ap_const_boolean_1;
    static const sc_lv<2> ap_const_lv2_3;
    static const sc_lv<1> ap_const_lv1_1;
    static const sc_lv<2> ap_const_lv2_2;
    static const sc_lv<2> ap_const_lv2_0;
    static const sc_lv<2> ap_const_lv2_1;
    static const bool ap_const_boolean_0;
    static const sc_lv<16> ap_const_lv16_0;
    static const sc_lv<8> ap_const_lv8_FF;
    static const sc_lv<1> ap_const_lv1_0;
    static const sc_lv<16> ap_const_lv16_1;
    static const sc_lv<6> ap_const_lv6_0;
    static const sc_lv<23> ap_const_lv23_40;
    static const sc_lv<23> ap_const_lv23_41;
    static const sc_lv<22> ap_const_lv22_3F;
    static const sc_lv<32> ap_const_lv32_3F;
    static const sc_lv<7> ap_const_lv7_3F;
    static const sc_lv<26> ap_const_lv26_0;
    static const sc_lv<7> ap_const_lv7_41;
    static const sc_lv<32> ap_const_lv32_16;
    static const sc_lv<23> ap_const_lv23_0;
    static const sc_lv<32> ap_const_lv32_3;
    static const sc_lv<32> ap_const_lv32_A;
    static const sc_lv<8> ap_const_lv8_0;
    static const sc_lv<32> ap_const_lv32_7;
    static const sc_lv<32> ap_const_lv32_6;
    static const sc_lv<5> ap_const_lv5_0;
    static const sc_lv<32> ap_const_lv32_8;
    static const sc_lv<32> ap_const_lv32_F;
    static const sc_lv<32> ap_const_lv32_10;
    static const sc_lv<32> ap_const_lv32_1F;
    static const sc_lv<32> ap_const_lv32_20;
    static const sc_lv<32> ap_const_lv32_2F;
    static const sc_lv<64> ap_const_lv64_FFFFFFFFFFFFFFFF;
    // Thread declarations
    void thread_ap_clk_no_reset_();
    void thread_Lo_assign_3_fu_254_p3();
    void thread_Lo_assign_fu_514_p3();
    void thread_add_ln647_fu_398_p2();
    void thread_add_ln76_2_fu_266_p2();
    void thread_add_ln76_fu_526_p2();
    void thread_agg_result_V_0_1_i_i_1_fu_756_p3();
    void thread_agg_result_V_0_1_i_i_2_fu_790_p3();
    void thread_agg_result_V_0_1_i_i_fu_722_p3();
    void thread_and_ln82_2_fu_462_p2();
    void thread_and_ln82_fu_598_p2();
    void thread_ap_CS_fsm_pp0_stage0();
    void thread_ap_block_pp0_stage0();
    void thread_ap_block_pp0_stage0_01001();
    void thread_ap_block_pp0_stage0_11001();
    void thread_ap_block_pp0_stage0_subdone();
    void thread_ap_block_state1_pp0_stage0_iter0();
    void thread_ap_block_state2_pp0_stage0_iter1();
    void thread_ap_block_state3_pp0_stage0_iter2();
    void thread_ap_condition_155();
    void thread_ap_condition_280();
    void thread_ap_done();
    void thread_ap_enable_pp0();
    void thread_ap_enable_reg_pp0_iter0();
    void thread_ap_idle();
    void thread_ap_idle_pp0();
    void thread_ap_idle_pp0_0to1();
    void thread_ap_predicate_op138_write_state3();
    void thread_ap_predicate_op14_read_state1();
    void thread_ap_predicate_op152_write_state3();
    void thread_ap_predicate_op8_read_state1();
    void thread_ap_predicate_op93_read_state1();
    void thread_ap_ready();
    void thread_ap_reset_idle_pp0();
    void thread_grp_fu_203_p1();
    void thread_grp_fu_211_p2();
    void thread_grp_fu_216_p2();
    void thread_grp_nbreadreq_fu_136_p5();
    void thread_icmp_ln647_4_fu_374_p2();
    void thread_icmp_ln647_fu_284_p2();
    void thread_icmp_ln76_2_fu_272_p2();
    void thread_icmp_ln76_fu_532_p2();
    void thread_icmp_ln82_3_fu_628_p2();
    void thread_icmp_ln887_fu_672_p2();
    void thread_io_acc_block_signal_op138();
    void thread_io_acc_block_signal_op14();
    void thread_io_acc_block_signal_op152();
    void thread_io_acc_block_signal_op159();
    void thread_io_acc_block_signal_op8();
    void thread_io_acc_block_signal_op93();
    void thread_lshr_ln647_10_fu_916_p2();
    void thread_lshr_ln647_8_fu_902_p2();
    void thread_lshr_ln647_9_fu_450_p2();
    void thread_lshr_ln647_fu_364_p2();
    void thread_lshr_ln82_1_fu_945_p2();
    void thread_lshr_ln82_fu_890_p2();
    void thread_or_ln78_2_fu_278_p2();
    void thread_or_ln78_fu_538_p2();
    void thread_or_ln887_fu_678_p2();
    void thread_p_Result_37_fu_922_p2();
    void thread_p_Result_38_fu_730_p5();
    void thread_p_Result_39_fu_764_p5();
    void thread_p_Result_40_fu_908_p2();
    void thread_p_Result_71_i_i81_s_fu_742_p4();
    void thread_p_Result_71_i_i86_s_fu_776_p4();
    void thread_p_Result_71_i_i_i_fu_708_p4();
    void thread_p_Result_s_fu_798_p5();
    void thread_ret_V_5_fu_927_p3();
    void thread_ret_V_7_fu_951_p2();
    void thread_ret_V_fu_934_p3();
    void thread_select_ln132_fu_496_p3();
    void thread_select_ln647_12_fu_338_p3();
    void thread_select_ln647_13_fu_346_p3();
    void thread_select_ln647_14_fu_416_p3();
    void thread_select_ln647_15_fu_424_p3();
    void thread_select_ln647_16_fu_432_p3();
    void thread_select_ln647_fu_330_p3();
    void thread_select_ln76_3_fu_468_p3();
    void thread_select_ln76_fu_604_p3();
    void thread_select_ln80_fu_664_p3();
    void thread_select_ln82_4_fu_620_p3();
    void thread_select_ln82_5_fu_476_p3();
    void thread_select_ln82_6_fu_859_p3();
    void thread_select_ln82_7_fu_866_p3();
    void thread_select_ln82_8_fu_873_p3();
    void thread_select_ln82_fu_612_p3();
    void thread_shl_ln_fu_544_p3();
    void thread_state_4_load_load_fu_226_p1();
    void thread_sub_ln647_14_fu_318_p2();
    void thread_sub_ln647_15_fu_324_p2();
    void thread_sub_ln647_16_fu_354_p2();
    void thread_sub_ln647_17_fu_404_p2();
    void thread_sub_ln647_18_fu_410_p2();
    void thread_sub_ln647_19_fu_440_p2();
    void thread_sub_ln647_fu_312_p2();
    void thread_sub_ln80_3_fu_570_p2();
    void thread_sub_ln80_4_fu_586_p2();
    void thread_sub_ln80_fu_556_p2();
    void thread_sub_ln82_1_fu_848_p2();
    void thread_sub_ln82_2_fu_854_p2();
    void thread_sub_ln82_3_fu_880_p2();
    void thread_sub_ln82_fu_843_p2();
    void thread_tmp_88_fu_562_p3();
    void thread_tmp_89_fu_826_p3();
    void thread_tmp_90_fu_833_p4();
    void thread_tmp_91_fu_644_p4();
    void thread_tmp_92_fu_654_p4();
    void thread_tmp_95_fu_290_p3();
    void thread_tmp_96_fu_302_p4();
    void thread_tmp_97_fu_380_p3();
    void thread_tmp_98_fu_388_p4();
    void thread_tmp_nbreadreq_fu_158_p6();
    void thread_trunc_ln647_10_fu_786_p1();
    void thread_trunc_ln647_11_fu_298_p1();
    void thread_trunc_ln647_9_fu_752_p1();
    void thread_trunc_ln647_fu_718_p1();
    void thread_trunc_ln76_4_fu_246_p1();
    void thread_trunc_ln76_5_fu_250_p1();
    void thread_trunc_ln76_fu_510_p1();
    void thread_trunc_ln80_3_fu_576_p4();
    void thread_trunc_ln82_fu_634_p1();
    void thread_tx_shift2udpFifo_V_d_blk_n();
    void thread_tx_shift2udpFifo_V_d_read();
    void thread_tx_shift2udpFifo_V_k_blk_n();
    void thread_tx_shift2udpFifo_V_k_read();
    void thread_tx_shift2udpFifo_V_l_blk_n();
    void thread_tx_shift2udpFifo_V_l_read();
    void thread_tx_udp2ipFifo_V_data_blk_n();
    void thread_tx_udp2ipFifo_V_data_din();
    void thread_tx_udp2ipFifo_V_data_write();
    void thread_tx_udp2ipFifo_V_keep_blk_n();
    void thread_tx_udp2ipFifo_V_keep_din();
    void thread_tx_udp2ipFifo_V_keep_write();
    void thread_tx_udp2ipFifo_V_last_blk_n();
    void thread_tx_udp2ipFifo_V_last_din();
    void thread_tx_udp2ipFifo_V_last_write();
    void thread_tx_udpMetaFifo_V_len_blk_n();
    void thread_tx_udpMetaFifo_V_len_read();
    void thread_tx_udpMetaFifo_V_my_s_blk_n();
    void thread_tx_udpMetaFifo_V_my_s_read();
    void thread_tx_udpMetaFifo_V_the_blk_n();
    void thread_tx_udpMetaFifo_V_the_read();
    void thread_tx_udpMetaFifo_V_val_blk_n();
    void thread_tx_udpMetaFifo_V_val_read();
    void thread_xor_ln132_fu_490_p2();
    void thread_xor_ln76_2_fu_456_p2();
    void thread_xor_ln76_fu_592_p2();
    void thread_zext_ln647_10_fu_446_p1();
    void thread_zext_ln647_11_fu_913_p1();
    void thread_zext_ln647_8_fu_899_p1();
    void thread_zext_ln647_9_fu_370_p1();
    void thread_zext_ln647_fu_360_p1();
    void thread_zext_ln76_2_fu_262_p1();
    void thread_zext_ln76_fu_522_p1();
    void thread_zext_ln80_fu_552_p1();
    void thread_zext_ln82_1_fu_942_p1();
    void thread_zext_ln82_fu_886_p1();
    void thread_ap_NS_fsm();
};

}

using namespace ap_rtl;

#endif
