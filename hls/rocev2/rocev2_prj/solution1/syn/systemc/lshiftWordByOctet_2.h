// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
// Version: 2019.2.1
// Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

#ifndef _lshiftWordByOctet_2_HH_
#define _lshiftWordByOctet_2_HH_

#include "systemc.h"
#include "AESL_pkg.h"


namespace ap_rtl {

struct lshiftWordByOctet_2 : public sc_module {
    // Port declarations 25
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_in< sc_logic > ap_start;
    sc_out< sc_logic > ap_done;
    sc_in< sc_logic > ap_continue;
    sc_out< sc_logic > ap_idle;
    sc_out< sc_logic > ap_ready;
    sc_in< sc_lv<64> > tx_rethMerge2rethShi_3_dout;
    sc_in< sc_logic > tx_rethMerge2rethShi_3_empty_n;
    sc_out< sc_logic > tx_rethMerge2rethShi_3_read;
    sc_in< sc_lv<8> > tx_rethMerge2rethShi_5_dout;
    sc_in< sc_logic > tx_rethMerge2rethShi_5_empty_n;
    sc_out< sc_logic > tx_rethMerge2rethShi_5_read;
    sc_in< sc_lv<1> > tx_rethMerge2rethShi_6_dout;
    sc_in< sc_logic > tx_rethMerge2rethShi_6_empty_n;
    sc_out< sc_logic > tx_rethMerge2rethShi_6_read;
    sc_out< sc_lv<64> > tx_rethShift2payFifo_3_din;
    sc_in< sc_logic > tx_rethShift2payFifo_3_full_n;
    sc_out< sc_logic > tx_rethShift2payFifo_3_write;
    sc_out< sc_lv<8> > tx_rethShift2payFifo_5_din;
    sc_in< sc_logic > tx_rethShift2payFifo_5_full_n;
    sc_out< sc_logic > tx_rethShift2payFifo_5_write;
    sc_out< sc_lv<1> > tx_rethShift2payFifo_6_din;
    sc_in< sc_logic > tx_rethShift2payFifo_6_full_n;
    sc_out< sc_logic > tx_rethShift2payFifo_6_write;


    // Module declarations
    lshiftWordByOctet_2(sc_module_name name);
    SC_HAS_PROCESS(lshiftWordByOctet_2);

    ~lshiftWordByOctet_2();

    sc_trace_file* mVcdFile;

    sc_signal< sc_logic > ap_done_reg;
    sc_signal< sc_lv<1> > ap_CS_fsm;
    sc_signal< sc_logic > ap_CS_fsm_pp0_stage0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter1;
    sc_signal< sc_logic > ap_idle_pp0;
    sc_signal< sc_logic > io_acc_block_signal_op5;
    sc_signal< sc_lv<1> > tmp_nbreadreq_fu_36_p5;
    sc_signal< bool > ap_block_state1_pp0_stage0_iter0;
    sc_signal< sc_logic > io_acc_block_signal_op16;
    sc_signal< sc_lv<1> > tmp_reg_83;
    sc_signal< bool > ap_block_state2_pp0_stage0_iter1;
    sc_signal< bool > ap_block_pp0_stage0_11001;
    sc_signal< sc_logic > tx_rethMerge2rethShi_3_blk_n;
    sc_signal< bool > ap_block_pp0_stage0;
    sc_signal< sc_logic > tx_rethMerge2rethShi_5_blk_n;
    sc_signal< sc_logic > tx_rethMerge2rethShi_6_blk_n;
    sc_signal< sc_logic > tx_rethShift2payFifo_3_blk_n;
    sc_signal< sc_logic > tx_rethShift2payFifo_5_blk_n;
    sc_signal< sc_logic > tx_rethShift2payFifo_6_blk_n;
    sc_signal< sc_lv<64> > tmp_data_V_reg_87;
    sc_signal< sc_lv<8> > tmp_keep_V_reg_92;
    sc_signal< sc_lv<1> > tmp_last_V_reg_97;
    sc_signal< bool > ap_block_pp0_stage0_subdone;
    sc_signal< bool > ap_block_pp0_stage0_01001;
    sc_signal< sc_lv<1> > ap_NS_fsm;
    sc_signal< sc_logic > ap_idle_pp0_0to0;
    sc_signal< sc_logic > ap_reset_idle_pp0;
    sc_signal< sc_logic > ap_enable_pp0;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<1> ap_ST_fsm_pp0_stage0;
    static const sc_lv<32> ap_const_lv32_0;
    static const bool ap_const_boolean_1;
    static const sc_lv<1> ap_const_lv1_1;
    static const bool ap_const_boolean_0;
    // Thread declarations
    void thread_ap_clk_no_reset_();
    void thread_ap_CS_fsm_pp0_stage0();
    void thread_ap_block_pp0_stage0();
    void thread_ap_block_pp0_stage0_01001();
    void thread_ap_block_pp0_stage0_11001();
    void thread_ap_block_pp0_stage0_subdone();
    void thread_ap_block_state1_pp0_stage0_iter0();
    void thread_ap_block_state2_pp0_stage0_iter1();
    void thread_ap_done();
    void thread_ap_enable_pp0();
    void thread_ap_enable_reg_pp0_iter0();
    void thread_ap_idle();
    void thread_ap_idle_pp0();
    void thread_ap_idle_pp0_0to0();
    void thread_ap_ready();
    void thread_ap_reset_idle_pp0();
    void thread_io_acc_block_signal_op16();
    void thread_io_acc_block_signal_op5();
    void thread_tmp_nbreadreq_fu_36_p5();
    void thread_tx_rethMerge2rethShi_3_blk_n();
    void thread_tx_rethMerge2rethShi_3_read();
    void thread_tx_rethMerge2rethShi_5_blk_n();
    void thread_tx_rethMerge2rethShi_5_read();
    void thread_tx_rethMerge2rethShi_6_blk_n();
    void thread_tx_rethMerge2rethShi_6_read();
    void thread_tx_rethShift2payFifo_3_blk_n();
    void thread_tx_rethShift2payFifo_3_din();
    void thread_tx_rethShift2payFifo_3_write();
    void thread_tx_rethShift2payFifo_5_blk_n();
    void thread_tx_rethShift2payFifo_5_din();
    void thread_tx_rethShift2payFifo_5_write();
    void thread_tx_rethShift2payFifo_6_blk_n();
    void thread_tx_rethShift2payFifo_6_din();
    void thread_tx_rethShift2payFifo_6_write();
    void thread_ap_NS_fsm();
};

}

using namespace ap_rtl;

#endif
