// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
// Version: 2019.2.1
// Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

#ifndef _process_udp_64_1989_HH_
#define _process_udp_64_1989_HH_

#include "systemc.h"
#include "AESL_pkg.h"


namespace ap_rtl {

struct process_udp_64_1989 : public sc_module {
    // Port declarations 28
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_in< sc_logic > ap_start;
    sc_out< sc_logic > ap_done;
    sc_in< sc_logic > ap_continue;
    sc_out< sc_logic > ap_idle;
    sc_out< sc_logic > ap_ready;
    sc_in< sc_lv<64> > rx_ip2udpFifo_V_data_dout;
    sc_in< sc_logic > rx_ip2udpFifo_V_data_empty_n;
    sc_out< sc_logic > rx_ip2udpFifo_V_data_read;
    sc_in< sc_lv<8> > rx_ip2udpFifo_V_keep_dout;
    sc_in< sc_logic > rx_ip2udpFifo_V_keep_empty_n;
    sc_out< sc_logic > rx_ip2udpFifo_V_keep_read;
    sc_in< sc_lv<1> > rx_ip2udpFifo_V_last_dout;
    sc_in< sc_logic > rx_ip2udpFifo_V_last_empty_n;
    sc_out< sc_logic > rx_ip2udpFifo_V_last_read;
    sc_out< sc_lv<64> > rx_udp2shiftFifo_V_d_din;
    sc_in< sc_logic > rx_udp2shiftFifo_V_d_full_n;
    sc_out< sc_logic > rx_udp2shiftFifo_V_d_write;
    sc_out< sc_lv<8> > rx_udp2shiftFifo_V_k_din;
    sc_in< sc_logic > rx_udp2shiftFifo_V_k_full_n;
    sc_out< sc_logic > rx_udp2shiftFifo_V_k_write;
    sc_out< sc_lv<1> > rx_udp2shiftFifo_V_l_din;
    sc_in< sc_logic > rx_udp2shiftFifo_V_l_full_n;
    sc_out< sc_logic > rx_udp2shiftFifo_V_l_write;
    sc_out< sc_lv<49> > rx_udpMetaFifo_V_din;
    sc_in< sc_logic > rx_udpMetaFifo_V_full_n;
    sc_out< sc_logic > rx_udpMetaFifo_V_write;


    // Module declarations
    process_udp_64_1989(sc_module_name name);
    SC_HAS_PROCESS(process_udp_64_1989);

    ~process_udp_64_1989();

    sc_trace_file* mVcdFile;

    sc_signal< sc_logic > ap_done_reg;
    sc_signal< sc_lv<1> > ap_CS_fsm;
    sc_signal< sc_logic > ap_CS_fsm_pp0_stage0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter1;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter2;
    sc_signal< sc_logic > ap_idle_pp0;
    sc_signal< sc_logic > io_acc_block_signal_op6;
    sc_signal< sc_lv<1> > tmp_nbreadreq_fu_98_p5;
    sc_signal< bool > ap_block_state1_pp0_stage0_iter0;
    sc_signal< bool > ap_block_state2_pp0_stage0_iter1;
    sc_signal< sc_logic > io_acc_block_signal_op73;
    sc_signal< sc_lv<1> > tmp_reg_520;
    sc_signal< sc_lv<1> > tmp_reg_520_pp0_iter1_reg;
    sc_signal< sc_lv<1> > metaWritten_2_load_reg_561;
    sc_signal< sc_lv<1> > icmp_ln879_reg_565;
    sc_signal< bool > ap_predicate_op73_write_state3;
    sc_signal< sc_lv<1> > or_ln73_1_reg_557;
    sc_signal< bool > ap_predicate_op78_write_state3;
    sc_signal< bool > ap_block_state3_pp0_stage0_iter2;
    sc_signal< bool > ap_block_pp0_stage0_11001;
    sc_signal< sc_lv<1> > pu_header_ready;
    sc_signal< sc_lv<16> > pu_header_idx;
    sc_signal< sc_lv<64> > pu_header_header_V;
    sc_signal< sc_lv<1> > metaWritten_2;
    sc_signal< sc_logic > rx_ip2udpFifo_V_data_blk_n;
    sc_signal< bool > ap_block_pp0_stage0;
    sc_signal< sc_logic > rx_ip2udpFifo_V_keep_blk_n;
    sc_signal< sc_logic > rx_ip2udpFifo_V_last_blk_n;
    sc_signal< sc_logic > rx_udp2shiftFifo_V_d_blk_n;
    sc_signal< sc_logic > rx_udp2shiftFifo_V_k_blk_n;
    sc_signal< sc_logic > rx_udp2shiftFifo_V_l_blk_n;
    sc_signal< sc_logic > rx_udpMetaFifo_V_blk_n;
    sc_signal< sc_lv<64> > tmp_data_V_reg_524;
    sc_signal< sc_lv<64> > tmp_data_V_reg_524_pp0_iter1_reg;
    sc_signal< sc_lv<8> > tmp_keep_V_reg_529;
    sc_signal< sc_lv<8> > tmp_keep_V_reg_529_pp0_iter1_reg;
    sc_signal< sc_lv<1> > tmp_last_V_reg_534;
    sc_signal< sc_lv<1> > tmp_last_V_reg_534_pp0_iter1_reg;
    sc_signal< sc_lv<1> > pu_header_ready_load_load_fu_190_p1;
    sc_signal< sc_lv<1> > pu_header_ready_load_reg_542;
    sc_signal< sc_lv<16> > add_ln67_fu_344_p2;
    sc_signal< sc_lv<1> > or_ln73_1_fu_350_p2;
    sc_signal< sc_lv<1> > metaWritten_2_load_load_fu_355_p1;
    sc_signal< sc_lv<1> > icmp_ln879_fu_391_p2;
    sc_signal< sc_lv<8> > p_Result_71_i_i9_i_reg_569;
    sc_signal< sc_lv<8> > p_Result_71_1_i_i1_reg_575;
    sc_signal< sc_lv<8> > p_Result_71_i_i14_s_reg_581;
    sc_signal< sc_lv<8> > trunc_ln647_fu_431_p1;
    sc_signal< sc_lv<8> > trunc_ln647_reg_586;
    sc_signal< sc_lv<8> > p_Result_71_i_i19_s_reg_591;
    sc_signal< sc_lv<8> > p_Result_71_1_i_i2_reg_596;
    sc_signal< sc_lv<1> > xor_ln73_fu_455_p2;
    sc_signal< sc_lv<1> > xor_ln73_reg_601;
    sc_signal< bool > ap_block_pp0_stage0_subdone;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter0_write_flag_0_i_i_reg_140;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter1_write_flag_0_i_i_reg_140;
    sc_signal< sc_lv<16> > ap_phi_reg_pp0_iter0_packetHeader_idx_0_i_reg_153;
    sc_signal< sc_lv<16> > ap_phi_reg_pp0_iter1_packetHeader_idx_0_i_reg_153;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter0_metaWritten_6_flag_1_reg_162;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter1_metaWritten_6_flag_1_reg_162;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter2_metaWritten_6_flag_1_reg_162;
    sc_signal< sc_lv<1> > and_ln73_fu_460_p2;
    sc_signal< sc_lv<1> > ap_sig_allocacmp_pu_header_ready_load;
    sc_signal< sc_lv<16> > select_ln73_fu_466_p3;
    sc_signal< sc_lv<16> > ap_sig_allocacmp_pu_header_idx_load;
    sc_signal< sc_lv<64> > p_Result_s_fu_332_p2;
    sc_signal< sc_lv<1> > or_ln73_fu_510_p2;
    sc_signal< sc_lv<1> > ap_sig_allocacmp_metaWritten_2_load;
    sc_signal< bool > ap_block_pp0_stage0_01001;
    sc_signal< sc_lv<26> > zext_ln414_fu_206_p1;
    sc_signal< sc_lv<1> > trunc_ln64_fu_198_p1;
    sc_signal< sc_lv<7> > tmp_40_fu_216_p3;
    sc_signal< sc_lv<1> > icmp_ln414_fu_210_p2;
    sc_signal< sc_lv<7> > sub_ln414_fu_224_p2;
    sc_signal< sc_lv<7> > sub_ln414_1_fu_246_p2;
    sc_signal< sc_lv<7> > select_ln414_4_fu_238_p3;
    sc_signal< sc_lv<7> > select_ln414_fu_230_p3;
    sc_signal< sc_lv<7> > select_ln414_5_fu_252_p3;
    sc_signal< sc_lv<64> > zext_ln414_1_fu_260_p1;
    sc_signal< sc_lv<64> > shl_ln414_fu_272_p2;
    sc_signal< sc_lv<64> > tmp_41_fu_278_p4;
    sc_signal< sc_lv<64> > zext_ln414_2_fu_264_p1;
    sc_signal< sc_lv<64> > zext_ln414_3_fu_268_p1;
    sc_signal< sc_lv<64> > shl_ln414_1_fu_296_p2;
    sc_signal< sc_lv<64> > lshr_ln414_fu_302_p2;
    sc_signal< sc_lv<64> > and_ln414_fu_308_p2;
    sc_signal< sc_lv<64> > xor_ln414_fu_314_p2;
    sc_signal< sc_lv<64> > select_ln414_6_fu_288_p3;
    sc_signal< sc_lv<64> > and_ln414_3_fu_320_p2;
    sc_signal< sc_lv<64> > and_ln414_4_fu_326_p2;
    sc_signal< sc_lv<8> > p_Result_71_1_i_i_s_fu_373_p4;
    sc_signal< sc_lv<8> > p_Result_71_i_i_i_fu_363_p4;
    sc_signal< sc_lv<16> > agg_result_V_0_1_i_i_fu_383_p3;
    sc_signal< sc_lv<16> > dstPort_V_fu_485_p3;
    sc_signal< sc_lv<1> > tmp_valid_fu_491_p2;
    sc_signal< sc_lv<1> > ap_NS_fsm;
    sc_signal< sc_logic > ap_idle_pp0_0to1;
    sc_signal< sc_logic > ap_reset_idle_pp0;
    sc_signal< sc_logic > ap_enable_pp0;
    sc_signal< bool > ap_condition_112;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<1> ap_ST_fsm_pp0_stage0;
    static const sc_lv<32> ap_const_lv32_0;
    static const bool ap_const_boolean_1;
    static const sc_lv<1> ap_const_lv1_1;
    static const sc_lv<1> ap_const_lv1_0;
    static const bool ap_const_boolean_0;
    static const sc_lv<16> ap_const_lv16_0;
    static const sc_lv<26> ap_const_lv26_0;
    static const sc_lv<6> ap_const_lv6_0;
    static const sc_lv<7> ap_const_lv7_3F;
    static const sc_lv<7> ap_const_lv7_0;
    static const sc_lv<32> ap_const_lv32_3F;
    static const sc_lv<64> ap_const_lv64_FFFFFFFFFFFFFFFF;
    static const sc_lv<16> ap_const_lv16_1;
    static const sc_lv<32> ap_const_lv32_18;
    static const sc_lv<32> ap_const_lv32_1F;
    static const sc_lv<32> ap_const_lv32_10;
    static const sc_lv<32> ap_const_lv32_17;
    static const sc_lv<16> ap_const_lv16_12B7;
    static const sc_lv<32> ap_const_lv32_8;
    static const sc_lv<32> ap_const_lv32_F;
    static const sc_lv<32> ap_const_lv32_28;
    static const sc_lv<32> ap_const_lv32_2F;
    static const sc_lv<32> ap_const_lv32_20;
    static const sc_lv<32> ap_const_lv32_27;
    // Thread declarations
    void thread_ap_clk_no_reset_();
    void thread_add_ln67_fu_344_p2();
    void thread_agg_result_V_0_1_i_i_fu_383_p3();
    void thread_and_ln414_3_fu_320_p2();
    void thread_and_ln414_4_fu_326_p2();
    void thread_and_ln414_fu_308_p2();
    void thread_and_ln73_fu_460_p2();
    void thread_ap_CS_fsm_pp0_stage0();
    void thread_ap_block_pp0_stage0();
    void thread_ap_block_pp0_stage0_01001();
    void thread_ap_block_pp0_stage0_11001();
    void thread_ap_block_pp0_stage0_subdone();
    void thread_ap_block_state1_pp0_stage0_iter0();
    void thread_ap_block_state2_pp0_stage0_iter1();
    void thread_ap_block_state3_pp0_stage0_iter2();
    void thread_ap_condition_112();
    void thread_ap_done();
    void thread_ap_enable_pp0();
    void thread_ap_enable_reg_pp0_iter0();
    void thread_ap_idle();
    void thread_ap_idle_pp0();
    void thread_ap_idle_pp0_0to1();
    void thread_ap_phi_reg_pp0_iter0_metaWritten_6_flag_1_reg_162();
    void thread_ap_phi_reg_pp0_iter0_packetHeader_idx_0_i_reg_153();
    void thread_ap_phi_reg_pp0_iter0_write_flag_0_i_i_reg_140();
    void thread_ap_predicate_op73_write_state3();
    void thread_ap_predicate_op78_write_state3();
    void thread_ap_ready();
    void thread_ap_reset_idle_pp0();
    void thread_ap_sig_allocacmp_metaWritten_2_load();
    void thread_ap_sig_allocacmp_pu_header_idx_load();
    void thread_ap_sig_allocacmp_pu_header_ready_load();
    void thread_dstPort_V_fu_485_p3();
    void thread_icmp_ln414_fu_210_p2();
    void thread_icmp_ln879_fu_391_p2();
    void thread_io_acc_block_signal_op6();
    void thread_io_acc_block_signal_op73();
    void thread_lshr_ln414_fu_302_p2();
    void thread_metaWritten_2_load_load_fu_355_p1();
    void thread_or_ln73_1_fu_350_p2();
    void thread_or_ln73_fu_510_p2();
    void thread_p_Result_71_1_i_i_s_fu_373_p4();
    void thread_p_Result_71_i_i_i_fu_363_p4();
    void thread_p_Result_s_fu_332_p2();
    void thread_pu_header_ready_load_load_fu_190_p1();
    void thread_rx_ip2udpFifo_V_data_blk_n();
    void thread_rx_ip2udpFifo_V_data_read();
    void thread_rx_ip2udpFifo_V_keep_blk_n();
    void thread_rx_ip2udpFifo_V_keep_read();
    void thread_rx_ip2udpFifo_V_last_blk_n();
    void thread_rx_ip2udpFifo_V_last_read();
    void thread_rx_udp2shiftFifo_V_d_blk_n();
    void thread_rx_udp2shiftFifo_V_d_din();
    void thread_rx_udp2shiftFifo_V_d_write();
    void thread_rx_udp2shiftFifo_V_k_blk_n();
    void thread_rx_udp2shiftFifo_V_k_din();
    void thread_rx_udp2shiftFifo_V_k_write();
    void thread_rx_udp2shiftFifo_V_l_blk_n();
    void thread_rx_udp2shiftFifo_V_l_din();
    void thread_rx_udp2shiftFifo_V_l_write();
    void thread_rx_udpMetaFifo_V_blk_n();
    void thread_rx_udpMetaFifo_V_din();
    void thread_rx_udpMetaFifo_V_write();
    void thread_select_ln414_4_fu_238_p3();
    void thread_select_ln414_5_fu_252_p3();
    void thread_select_ln414_6_fu_288_p3();
    void thread_select_ln414_fu_230_p3();
    void thread_select_ln73_fu_466_p3();
    void thread_shl_ln414_1_fu_296_p2();
    void thread_shl_ln414_fu_272_p2();
    void thread_sub_ln414_1_fu_246_p2();
    void thread_sub_ln414_fu_224_p2();
    void thread_tmp_40_fu_216_p3();
    void thread_tmp_41_fu_278_p4();
    void thread_tmp_nbreadreq_fu_98_p5();
    void thread_tmp_valid_fu_491_p2();
    void thread_trunc_ln647_fu_431_p1();
    void thread_trunc_ln64_fu_198_p1();
    void thread_xor_ln414_fu_314_p2();
    void thread_xor_ln73_fu_455_p2();
    void thread_zext_ln414_1_fu_260_p1();
    void thread_zext_ln414_2_fu_264_p1();
    void thread_zext_ln414_3_fu_268_p1();
    void thread_zext_ln414_fu_206_p1();
    void thread_ap_NS_fsm();
};

}

using namespace ap_rtl;

#endif
