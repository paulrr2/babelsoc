// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
// Version: 2019.2.1
// Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

module handle_read_requests (
        ap_clk,
        ap_rst,
        ap_start,
        ap_done,
        ap_continue,
        ap_idle,
        ap_ready,
        rx_readRequestFifo_V_dout,
        rx_readRequestFifo_V_empty_n,
        rx_readRequestFifo_V_read,
        rx_remoteMemCmd_V_din,
        rx_remoteMemCmd_V_full_n,
        rx_remoteMemCmd_V_write,
        rx_readEvenFifo_V_din,
        rx_readEvenFifo_V_full_n,
        rx_readEvenFifo_V_write
);

parameter    ap_ST_fsm_pp0_stage0 = 1'd1;
parameter    ap_const_lv113_0 = 113'd0;

input   ap_clk;
input   ap_rst;
input   ap_start;
output   ap_done;
input   ap_continue;
output   ap_idle;
output   ap_ready;
input  [128:0] rx_readRequestFifo_V_dout;
input   rx_readRequestFifo_V_empty_n;
output   rx_readRequestFifo_V_read;
output  [112:0] rx_remoteMemCmd_V_din;
input   rx_remoteMemCmd_V_full_n;
output   rx_remoteMemCmd_V_write;
output  [134:0] rx_readEvenFifo_V_din;
input   rx_readEvenFifo_V_full_n;
output   rx_readEvenFifo_V_write;

reg ap_done;
reg ap_idle;
reg ap_ready;
reg rx_readRequestFifo_V_read;
reg rx_remoteMemCmd_V_write;
reg[134:0] rx_readEvenFifo_V_din;
reg rx_readEvenFifo_V_write;

reg    ap_done_reg;
(* fsm_encoding = "none" *) reg   [0:0] ap_CS_fsm;
wire    ap_CS_fsm_pp0_stage0;
wire    ap_enable_reg_pp0_iter0;
reg    ap_enable_reg_pp0_iter1;
reg    ap_idle_pp0;
wire   [0:0] hrr_fsmState_load_load_fu_272_p1;
wire   [0:0] tmp_nbreadreq_fu_98_p3;
reg    ap_predicate_op9_read_state1;
reg    ap_block_state1_pp0_stage0_iter0;
reg   [0:0] hrr_fsmState_load_reg_477;
reg   [0:0] tmp_reg_486;
reg    ap_predicate_op41_write_state2;
reg    ap_predicate_op43_write_state2;
reg    ap_block_state2_pp0_stage0_iter1;
reg    ap_block_pp0_stage0_11001;
reg   [0:0] hrr_fsmState;
reg   [47:0] request_vaddr_V;
reg   [31:0] request_dma_length_V;
reg   [23:0] request_qpn_V;
reg   [23:0] request_psn_V;
reg    rx_readRequestFifo_V_blk_n;
wire    ap_block_pp0_stage0;
reg    rx_remoteMemCmd_V_blk_n;
reg    rx_readEvenFifo_V_blk_n;
wire   [23:0] tmp_qpn_V_19_fu_284_p1;
reg   [23:0] tmp_qpn_V_19_reg_490;
wire   [47:0] readAddr_V_fu_294_p4;
reg   [47:0] readAddr_V_reg_495;
wire   [31:0] readLength_V_2_fu_304_p4;
reg   [31:0] readLength_V_2_reg_501;
reg   [23:0] tmp_psn_V_6_reg_508;
wire   [47:0] add_ln700_11_fu_330_p2;
wire   [0:0] icmp_ln895_3_fu_324_p2;
wire   [31:0] add_ln701_6_fu_336_p2;
wire   [15:0] tmp_qpn_V_18_fu_348_p1;
reg   [15:0] tmp_qpn_V_18_reg_527;
wire   [47:0] add_ln700_fu_364_p2;
wire   [0:0] icmp_ln895_fu_352_p2;
wire   [31:0] add_ln701_fu_370_p2;
reg    ap_block_pp0_stage0_subdone;
wire   [47:0] ap_phi_reg_pp0_iter0_request_vaddr_V_new_s_reg_126;
reg   [47:0] ap_phi_reg_pp0_iter1_request_vaddr_V_new_s_reg_126;
wire   [31:0] ap_phi_reg_pp0_iter0_request_dma_length_V_1_reg_135;
reg   [31:0] ap_phi_reg_pp0_iter1_request_dma_length_V_1_reg_135;
wire   [31:0] ap_phi_reg_pp0_iter0_tmp_length_V_5_reg_144;
reg   [31:0] ap_phi_reg_pp0_iter1_tmp_length_V_5_reg_144;
wire   [4:0] ap_phi_reg_pp0_iter0_tmp_op_code_4_reg_155;
reg   [4:0] ap_phi_reg_pp0_iter1_tmp_op_code_4_reg_155;
wire   [0:0] ap_phi_reg_pp0_iter0_request_vaddr_V_flag_reg_168;
reg   [0:0] ap_phi_reg_pp0_iter1_request_vaddr_V_flag_reg_168;
wire   [47:0] ap_phi_reg_pp0_iter0_request_vaddr_V_new_1_reg_181;
reg   [47:0] ap_phi_reg_pp0_iter1_request_vaddr_V_new_1_reg_181;
wire   [31:0] ap_phi_reg_pp0_iter0_request_dma_length_V_2_reg_192;
reg   [31:0] ap_phi_reg_pp0_iter1_request_dma_length_V_2_reg_192;
wire   [31:0] ap_phi_reg_pp0_iter0_tmp_length_V_reg_203;
reg   [31:0] ap_phi_reg_pp0_iter1_tmp_length_V_reg_203;
wire   [1:0] ap_phi_reg_pp0_iter0_tmp_op_code_reg_214;
reg   [1:0] ap_phi_reg_pp0_iter1_tmp_op_code_reg_214;
reg   [0:0] ap_phi_mux_request_vaddr_V_flag_1_phi_fu_231_p6;
reg   [0:0] ap_phi_reg_pp0_iter1_request_vaddr_V_flag_1_reg_227;
wire   [0:0] ap_phi_reg_pp0_iter0_request_vaddr_V_flag_1_reg_227;
reg   [47:0] ap_phi_mux_request_vaddr_V_new_2_phi_fu_246_p6;
wire   [47:0] ap_phi_reg_pp0_iter1_request_vaddr_V_new_2_reg_242;
reg   [31:0] ap_phi_mux_request_dma_length_V_3_phi_fu_261_p6;
wire   [31:0] ap_phi_reg_pp0_iter1_request_dma_length_V_3_reg_257;
reg   [47:0] ap_sig_allocacmp_request_vaddr_V_load;
reg   [31:0] ap_sig_allocacmp_readLength_V;
wire   [23:0] tmp_psn_V_fu_432_p2;
reg    ap_block_pp0_stage0_01001;
wire   [134:0] tmp_2_fu_405_p7;
wire   [134:0] tmp_3_fu_448_p7;
wire   [63:0] tmp_addr_V_fu_381_p1;
wire   [111:0] tmp_54_i_fu_384_p4;
wire  signed [3:0] sext_ln738_fu_420_p1;
wire   [4:0] zext_ln738_fu_424_p1;
reg   [0:0] ap_NS_fsm;
reg    ap_idle_pp0_0to0;
reg    ap_reset_idle_pp0;
wire    ap_enable_pp0;
reg    ap_condition_152;
reg    ap_condition_177;
reg    ap_condition_87;
reg    ap_condition_175;
reg    ap_condition_244;

// power-on initialization
initial begin
#0 ap_done_reg = 1'b0;
#0 ap_CS_fsm = 1'd1;
#0 ap_enable_reg_pp0_iter1 = 1'b0;
#0 hrr_fsmState = 1'd0;
#0 request_vaddr_V = 48'd0;
#0 request_dma_length_V = 32'd0;
#0 request_qpn_V = 24'd0;
#0 request_psn_V = 24'd0;
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_CS_fsm <= ap_ST_fsm_pp0_stage0;
    end else begin
        ap_CS_fsm <= ap_NS_fsm;
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_done_reg <= 1'b0;
    end else begin
        if ((ap_continue == 1'b1)) begin
            ap_done_reg <= 1'b0;
        end else if (((1'b0 == ap_block_pp0_stage0_11001) & (ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
            ap_done_reg <= 1'b1;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_enable_reg_pp0_iter1 <= 1'b0;
    end else begin
        if (((1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_subdone))) begin
            ap_enable_reg_pp0_iter1 <= ap_start;
        end
    end
end

always @ (posedge ap_clk) begin
    if ((1'b1 == ap_condition_87)) begin
        if ((1'b1 == ap_condition_177)) begin
            ap_phi_reg_pp0_iter1_request_dma_length_V_1_reg_135 <= {{rx_readRequestFifo_V_dout[103:72]}};
        end else if ((1'b1 == ap_condition_152)) begin
            ap_phi_reg_pp0_iter1_request_dma_length_V_1_reg_135 <= add_ln701_6_fu_336_p2;
        end else if ((1'b1 == 1'b1)) begin
            ap_phi_reg_pp0_iter1_request_dma_length_V_1_reg_135 <= ap_phi_reg_pp0_iter0_request_dma_length_V_1_reg_135;
        end
    end
end

always @ (posedge ap_clk) begin
    if ((1'b1 == ap_condition_87)) begin
        if (((icmp_ln895_fu_352_p2 == 1'd1) & (hrr_fsmState_load_load_fu_272_p1 == 1'd1))) begin
            ap_phi_reg_pp0_iter1_request_dma_length_V_2_reg_192 <= add_ln701_fu_370_p2;
        end else if ((1'b1 == 1'b1)) begin
            ap_phi_reg_pp0_iter1_request_dma_length_V_2_reg_192 <= ap_phi_reg_pp0_iter0_request_dma_length_V_2_reg_192;
        end
    end
end

always @ (posedge ap_clk) begin
    if ((1'b1 == ap_condition_87)) begin
        if (((tmp_nbreadreq_fu_98_p3 == 1'd0) & (hrr_fsmState == 1'd0))) begin
            ap_phi_reg_pp0_iter1_request_vaddr_V_flag_1_reg_227 <= 1'd0;
        end else if ((1'b1 == 1'b1)) begin
            ap_phi_reg_pp0_iter1_request_vaddr_V_flag_1_reg_227 <= ap_phi_reg_pp0_iter0_request_vaddr_V_flag_1_reg_227;
        end
    end
end

always @ (posedge ap_clk) begin
    if ((1'b1 == ap_condition_87)) begin
        if (((icmp_ln895_fu_352_p2 == 1'd0) & (hrr_fsmState_load_load_fu_272_p1 == 1'd1))) begin
            ap_phi_reg_pp0_iter1_request_vaddr_V_flag_reg_168 <= 1'd0;
        end else if (((icmp_ln895_fu_352_p2 == 1'd1) & (hrr_fsmState_load_load_fu_272_p1 == 1'd1))) begin
            ap_phi_reg_pp0_iter1_request_vaddr_V_flag_reg_168 <= 1'd1;
        end else if ((1'b1 == 1'b1)) begin
            ap_phi_reg_pp0_iter1_request_vaddr_V_flag_reg_168 <= ap_phi_reg_pp0_iter0_request_vaddr_V_flag_reg_168;
        end
    end
end

always @ (posedge ap_clk) begin
    if ((1'b1 == ap_condition_87)) begin
        if (((icmp_ln895_fu_352_p2 == 1'd1) & (hrr_fsmState_load_load_fu_272_p1 == 1'd1))) begin
            ap_phi_reg_pp0_iter1_request_vaddr_V_new_1_reg_181 <= add_ln700_fu_364_p2;
        end else if ((1'b1 == 1'b1)) begin
            ap_phi_reg_pp0_iter1_request_vaddr_V_new_1_reg_181 <= ap_phi_reg_pp0_iter0_request_vaddr_V_new_1_reg_181;
        end
    end
end

always @ (posedge ap_clk) begin
    if ((1'b1 == ap_condition_87)) begin
        if ((1'b1 == ap_condition_177)) begin
            ap_phi_reg_pp0_iter1_request_vaddr_V_new_s_reg_126 <= {{rx_readRequestFifo_V_dout[71:24]}};
        end else if ((1'b1 == ap_condition_152)) begin
            ap_phi_reg_pp0_iter1_request_vaddr_V_new_s_reg_126 <= add_ln700_11_fu_330_p2;
        end else if ((1'b1 == 1'b1)) begin
            ap_phi_reg_pp0_iter1_request_vaddr_V_new_s_reg_126 <= ap_phi_reg_pp0_iter0_request_vaddr_V_new_s_reg_126;
        end
    end
end

always @ (posedge ap_clk) begin
    if ((1'b1 == ap_condition_87)) begin
        if ((1'b1 == ap_condition_177)) begin
            ap_phi_reg_pp0_iter1_tmp_length_V_5_reg_144 <= {{rx_readRequestFifo_V_dout[103:72]}};
        end else if ((1'b1 == ap_condition_152)) begin
            ap_phi_reg_pp0_iter1_tmp_length_V_5_reg_144 <= 32'd1408;
        end else if ((1'b1 == 1'b1)) begin
            ap_phi_reg_pp0_iter1_tmp_length_V_5_reg_144 <= ap_phi_reg_pp0_iter0_tmp_length_V_5_reg_144;
        end
    end
end

always @ (posedge ap_clk) begin
    if ((1'b1 == ap_condition_87)) begin
        if (((icmp_ln895_fu_352_p2 == 1'd0) & (hrr_fsmState_load_load_fu_272_p1 == 1'd1))) begin
            ap_phi_reg_pp0_iter1_tmp_length_V_reg_203 <= ap_sig_allocacmp_readLength_V;
        end else if (((icmp_ln895_fu_352_p2 == 1'd1) & (hrr_fsmState_load_load_fu_272_p1 == 1'd1))) begin
            ap_phi_reg_pp0_iter1_tmp_length_V_reg_203 <= 32'd1408;
        end else if ((1'b1 == 1'b1)) begin
            ap_phi_reg_pp0_iter1_tmp_length_V_reg_203 <= ap_phi_reg_pp0_iter0_tmp_length_V_reg_203;
        end
    end
end

always @ (posedge ap_clk) begin
    if ((1'b1 == ap_condition_87)) begin
        if ((1'b1 == ap_condition_177)) begin
            ap_phi_reg_pp0_iter1_tmp_op_code_4_reg_155 <= 5'd16;
        end else if ((1'b1 == ap_condition_152)) begin
            ap_phi_reg_pp0_iter1_tmp_op_code_4_reg_155 <= 5'd13;
        end else if ((1'b1 == 1'b1)) begin
            ap_phi_reg_pp0_iter1_tmp_op_code_4_reg_155 <= ap_phi_reg_pp0_iter0_tmp_op_code_4_reg_155;
        end
    end
end

always @ (posedge ap_clk) begin
    if ((1'b1 == ap_condition_87)) begin
        if (((icmp_ln895_fu_352_p2 == 1'd0) & (hrr_fsmState_load_load_fu_272_p1 == 1'd1))) begin
            ap_phi_reg_pp0_iter1_tmp_op_code_reg_214 <= 2'd3;
        end else if (((icmp_ln895_fu_352_p2 == 1'd1) & (hrr_fsmState_load_load_fu_272_p1 == 1'd1))) begin
            ap_phi_reg_pp0_iter1_tmp_op_code_reg_214 <= 2'd2;
        end else if ((1'b1 == 1'b1)) begin
            ap_phi_reg_pp0_iter1_tmp_op_code_reg_214 <= ap_phi_reg_pp0_iter0_tmp_op_code_reg_214;
        end
    end
end

always @ (posedge ap_clk) begin
    if ((1'b1 == ap_condition_87)) begin
        if (((icmp_ln895_fu_352_p2 == 1'd0) & (hrr_fsmState_load_load_fu_272_p1 == 1'd1))) begin
            hrr_fsmState <= 1'd0;
        end else if ((1'b1 == ap_condition_152)) begin
            hrr_fsmState <= 1'd1;
        end
    end
end

always @ (posedge ap_clk) begin
    if ((1'b1 == ap_condition_175)) begin
        if ((hrr_fsmState_load_reg_477 == 1'd1)) begin
            request_psn_V <= tmp_psn_V_fu_432_p2;
        end else if (((tmp_reg_486 == 1'd1) & (hrr_fsmState_load_reg_477 == 1'd0))) begin
            request_psn_V <= tmp_psn_V_6_reg_508;
        end
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        hrr_fsmState_load_reg_477 <= hrr_fsmState;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (tmp_nbreadreq_fu_98_p3 == 1'd1) & (hrr_fsmState == 1'd0) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        readAddr_V_reg_495 <= {{rx_readRequestFifo_V_dout[71:24]}};
        readLength_V_2_reg_501 <= {{rx_readRequestFifo_V_dout[103:72]}};
        tmp_psn_V_6_reg_508 <= {{rx_readRequestFifo_V_dout[127:104]}};
        tmp_qpn_V_18_reg_527 <= tmp_qpn_V_18_fu_348_p1;
        tmp_qpn_V_19_reg_490 <= tmp_qpn_V_19_fu_284_p1;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (ap_phi_mux_request_vaddr_V_flag_1_phi_fu_231_p6 == 1'd1) & (ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        request_dma_length_V <= ap_phi_mux_request_dma_length_V_3_phi_fu_261_p6;
        request_vaddr_V <= ap_phi_mux_request_vaddr_V_new_2_phi_fu_246_p6;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (tmp_nbreadreq_fu_98_p3 == 1'd1) & (hrr_fsmState == 1'd0) & (ap_start == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        request_qpn_V <= tmp_qpn_V_19_fu_284_p1;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (hrr_fsmState == 1'd0) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        tmp_reg_486 <= tmp_nbreadreq_fu_98_p3;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        ap_done = 1'b1;
    end else begin
        ap_done = ap_done_reg;
    end
end

always @ (*) begin
    if (((ap_start == 1'b0) & (ap_idle_pp0 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        ap_idle = 1'b1;
    end else begin
        ap_idle = 1'b0;
    end
end

always @ (*) begin
    if (((ap_enable_reg_pp0_iter1 == 1'b0) & (ap_enable_reg_pp0_iter0 == 1'b0))) begin
        ap_idle_pp0 = 1'b1;
    end else begin
        ap_idle_pp0 = 1'b0;
    end
end

always @ (*) begin
    if ((ap_enable_reg_pp0_iter0 == 1'b0)) begin
        ap_idle_pp0_0to0 = 1'b1;
    end else begin
        ap_idle_pp0_0to0 = 1'b0;
    end
end

always @ (*) begin
    if (((tmp_reg_486 == 1'd1) & (hrr_fsmState_load_reg_477 == 1'd0))) begin
        ap_phi_mux_request_dma_length_V_3_phi_fu_261_p6 = ap_phi_reg_pp0_iter1_request_dma_length_V_1_reg_135;
    end else if ((hrr_fsmState_load_reg_477 == 1'd1)) begin
        ap_phi_mux_request_dma_length_V_3_phi_fu_261_p6 = ap_phi_reg_pp0_iter1_request_dma_length_V_2_reg_192;
    end else begin
        ap_phi_mux_request_dma_length_V_3_phi_fu_261_p6 = ap_phi_reg_pp0_iter1_request_dma_length_V_3_reg_257;
    end
end

always @ (*) begin
    if (((tmp_reg_486 == 1'd1) & (hrr_fsmState_load_reg_477 == 1'd0))) begin
        ap_phi_mux_request_vaddr_V_flag_1_phi_fu_231_p6 = 1'd1;
    end else if ((hrr_fsmState_load_reg_477 == 1'd1)) begin
        ap_phi_mux_request_vaddr_V_flag_1_phi_fu_231_p6 = ap_phi_reg_pp0_iter1_request_vaddr_V_flag_reg_168;
    end else begin
        ap_phi_mux_request_vaddr_V_flag_1_phi_fu_231_p6 = ap_phi_reg_pp0_iter1_request_vaddr_V_flag_1_reg_227;
    end
end

always @ (*) begin
    if (((tmp_reg_486 == 1'd1) & (hrr_fsmState_load_reg_477 == 1'd0))) begin
        ap_phi_mux_request_vaddr_V_new_2_phi_fu_246_p6 = ap_phi_reg_pp0_iter1_request_vaddr_V_new_s_reg_126;
    end else if ((hrr_fsmState_load_reg_477 == 1'd1)) begin
        ap_phi_mux_request_vaddr_V_new_2_phi_fu_246_p6 = ap_phi_reg_pp0_iter1_request_vaddr_V_new_1_reg_181;
    end else begin
        ap_phi_mux_request_vaddr_V_new_2_phi_fu_246_p6 = ap_phi_reg_pp0_iter1_request_vaddr_V_new_2_reg_242;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (ap_start == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        ap_ready = 1'b1;
    end else begin
        ap_ready = 1'b0;
    end
end

always @ (*) begin
    if (((ap_start == 1'b0) & (ap_idle_pp0_0to0 == 1'b1))) begin
        ap_reset_idle_pp0 = 1'b1;
    end else begin
        ap_reset_idle_pp0 = 1'b0;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0) & (ap_phi_mux_request_vaddr_V_flag_1_phi_fu_231_p6 == 1'd1) & (ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        ap_sig_allocacmp_readLength_V = ap_phi_mux_request_dma_length_V_3_phi_fu_261_p6;
    end else begin
        ap_sig_allocacmp_readLength_V = request_dma_length_V;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0) & (ap_phi_mux_request_vaddr_V_flag_1_phi_fu_231_p6 == 1'd1) & (ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        ap_sig_allocacmp_request_vaddr_V_load = ap_phi_mux_request_vaddr_V_new_2_phi_fu_246_p6;
    end else begin
        ap_sig_allocacmp_request_vaddr_V_load = request_vaddr_V;
    end
end

always @ (*) begin
    if ((((1'b0 == ap_block_pp0_stage0) & (hrr_fsmState_load_reg_477 == 1'd1) & (ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0)) | ((1'b0 == ap_block_pp0_stage0) & (ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (ap_predicate_op43_write_state2 == 1'b1)))) begin
        rx_readEvenFifo_V_blk_n = rx_readEvenFifo_V_full_n;
    end else begin
        rx_readEvenFifo_V_blk_n = 1'b1;
    end
end

always @ (*) begin
    if ((1'b1 == ap_condition_244)) begin
        if ((hrr_fsmState_load_reg_477 == 1'd1)) begin
            rx_readEvenFifo_V_din = tmp_3_fu_448_p7;
        end else if ((ap_predicate_op43_write_state2 == 1'b1)) begin
            rx_readEvenFifo_V_din = tmp_2_fu_405_p7;
        end else begin
            rx_readEvenFifo_V_din = 'bx;
        end
    end else begin
        rx_readEvenFifo_V_din = 'bx;
    end
end

always @ (*) begin
    if ((((1'b0 == ap_block_pp0_stage0_11001) & (hrr_fsmState_load_reg_477 == 1'd1) & (ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0)) | ((1'b0 == ap_block_pp0_stage0_11001) & (ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (ap_predicate_op43_write_state2 == 1'b1)))) begin
        rx_readEvenFifo_V_write = 1'b1;
    end else begin
        rx_readEvenFifo_V_write = 1'b0;
    end
end

always @ (*) begin
    if ((~((ap_start == 1'b0) | (ap_done_reg == 1'b1)) & (1'b0 == ap_block_pp0_stage0) & (ap_start == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (ap_predicate_op9_read_state1 == 1'b1))) begin
        rx_readRequestFifo_V_blk_n = rx_readRequestFifo_V_empty_n;
    end else begin
        rx_readRequestFifo_V_blk_n = 1'b1;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (ap_start == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (ap_predicate_op9_read_state1 == 1'b1))) begin
        rx_readRequestFifo_V_read = 1'b1;
    end else begin
        rx_readRequestFifo_V_read = 1'b0;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0) & (ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (ap_predicate_op41_write_state2 == 1'b1))) begin
        rx_remoteMemCmd_V_blk_n = rx_remoteMemCmd_V_full_n;
    end else begin
        rx_remoteMemCmd_V_blk_n = 1'b1;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (ap_predicate_op41_write_state2 == 1'b1))) begin
        rx_remoteMemCmd_V_write = 1'b1;
    end else begin
        rx_remoteMemCmd_V_write = 1'b0;
    end
end

always @ (*) begin
    case (ap_CS_fsm)
        ap_ST_fsm_pp0_stage0 : begin
            ap_NS_fsm = ap_ST_fsm_pp0_stage0;
        end
        default : begin
            ap_NS_fsm = 'bx;
        end
    endcase
end

assign add_ln700_11_fu_330_p2 = (readAddr_V_fu_294_p4 + 48'd1408);

assign add_ln700_fu_364_p2 = (ap_sig_allocacmp_request_vaddr_V_load + 48'd1408);

assign add_ln701_6_fu_336_p2 = ($signed(readLength_V_2_fu_304_p4) + $signed(32'd4294965888));

assign add_ln701_fu_370_p2 = ($signed(ap_sig_allocacmp_readLength_V) + $signed(32'd4294965888));

assign ap_CS_fsm_pp0_stage0 = ap_CS_fsm[32'd0];

assign ap_block_pp0_stage0 = ~(1'b1 == 1'b1);

always @ (*) begin
    ap_block_pp0_stage0_01001 = ((ap_done_reg == 1'b1) | ((ap_enable_reg_pp0_iter1 == 1'b1) & (((hrr_fsmState_load_reg_477 == 1'd1) & (rx_readEvenFifo_V_full_n == 1'b0)) | ((rx_readEvenFifo_V_full_n == 1'b0) & (ap_predicate_op43_write_state2 == 1'b1)) | ((rx_remoteMemCmd_V_full_n == 1'b0) & (ap_predicate_op41_write_state2 == 1'b1)))) | ((ap_start == 1'b1) & ((ap_start == 1'b0) | (ap_done_reg == 1'b1) | ((rx_readRequestFifo_V_empty_n == 1'b0) & (ap_predicate_op9_read_state1 == 1'b1)))));
end

always @ (*) begin
    ap_block_pp0_stage0_11001 = ((ap_done_reg == 1'b1) | ((ap_enable_reg_pp0_iter1 == 1'b1) & (((hrr_fsmState_load_reg_477 == 1'd1) & (rx_readEvenFifo_V_full_n == 1'b0)) | ((rx_readEvenFifo_V_full_n == 1'b0) & (ap_predicate_op43_write_state2 == 1'b1)) | ((rx_remoteMemCmd_V_full_n == 1'b0) & (ap_predicate_op41_write_state2 == 1'b1)))) | ((ap_start == 1'b1) & ((ap_start == 1'b0) | (ap_done_reg == 1'b1) | ((rx_readRequestFifo_V_empty_n == 1'b0) & (ap_predicate_op9_read_state1 == 1'b1)))));
end

always @ (*) begin
    ap_block_pp0_stage0_subdone = ((ap_done_reg == 1'b1) | ((ap_enable_reg_pp0_iter1 == 1'b1) & (((hrr_fsmState_load_reg_477 == 1'd1) & (rx_readEvenFifo_V_full_n == 1'b0)) | ((rx_readEvenFifo_V_full_n == 1'b0) & (ap_predicate_op43_write_state2 == 1'b1)) | ((rx_remoteMemCmd_V_full_n == 1'b0) & (ap_predicate_op41_write_state2 == 1'b1)))) | ((ap_start == 1'b1) & ((ap_start == 1'b0) | (ap_done_reg == 1'b1) | ((rx_readRequestFifo_V_empty_n == 1'b0) & (ap_predicate_op9_read_state1 == 1'b1)))));
end

always @ (*) begin
    ap_block_state1_pp0_stage0_iter0 = ((ap_start == 1'b0) | (ap_done_reg == 1'b1) | ((rx_readRequestFifo_V_empty_n == 1'b0) & (ap_predicate_op9_read_state1 == 1'b1)));
end

always @ (*) begin
    ap_block_state2_pp0_stage0_iter1 = (((hrr_fsmState_load_reg_477 == 1'd1) & (rx_readEvenFifo_V_full_n == 1'b0)) | ((rx_readEvenFifo_V_full_n == 1'b0) & (ap_predicate_op43_write_state2 == 1'b1)) | ((rx_remoteMemCmd_V_full_n == 1'b0) & (ap_predicate_op41_write_state2 == 1'b1)));
end

always @ (*) begin
    ap_condition_152 = ((icmp_ln895_3_fu_324_p2 == 1'd1) & (tmp_nbreadreq_fu_98_p3 == 1'd1) & (hrr_fsmState == 1'd0));
end

always @ (*) begin
    ap_condition_175 = ((1'b0 == ap_block_pp0_stage0_11001) & (ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0));
end

always @ (*) begin
    ap_condition_177 = ((tmp_nbreadreq_fu_98_p3 == 1'd1) & (icmp_ln895_3_fu_324_p2 == 1'd0) & (hrr_fsmState == 1'd0));
end

always @ (*) begin
    ap_condition_244 = ((1'b0 == ap_block_pp0_stage0_01001) & (ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0));
end

always @ (*) begin
    ap_condition_87 = ((1'b0 == ap_block_pp0_stage0_11001) & (ap_start == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0));
end

assign ap_enable_pp0 = (ap_idle_pp0 ^ 1'b1);

assign ap_enable_reg_pp0_iter0 = ap_start;

assign ap_phi_reg_pp0_iter0_request_dma_length_V_1_reg_135 = 'bx;

assign ap_phi_reg_pp0_iter0_request_dma_length_V_2_reg_192 = 'bx;

assign ap_phi_reg_pp0_iter0_request_vaddr_V_flag_1_reg_227 = 'bx;

assign ap_phi_reg_pp0_iter0_request_vaddr_V_flag_reg_168 = 'bx;

assign ap_phi_reg_pp0_iter0_request_vaddr_V_new_1_reg_181 = 'bx;

assign ap_phi_reg_pp0_iter0_request_vaddr_V_new_s_reg_126 = 'bx;

assign ap_phi_reg_pp0_iter0_tmp_length_V_5_reg_144 = 'bx;

assign ap_phi_reg_pp0_iter0_tmp_length_V_reg_203 = 'bx;

assign ap_phi_reg_pp0_iter0_tmp_op_code_4_reg_155 = 'bx;

assign ap_phi_reg_pp0_iter0_tmp_op_code_reg_214 = 'bx;

assign ap_phi_reg_pp0_iter1_request_dma_length_V_3_reg_257 = 'bx;

assign ap_phi_reg_pp0_iter1_request_vaddr_V_new_2_reg_242 = 'bx;

always @ (*) begin
    ap_predicate_op41_write_state2 = ((tmp_reg_486 == 1'd1) & (hrr_fsmState_load_reg_477 == 1'd0));
end

always @ (*) begin
    ap_predicate_op43_write_state2 = ((tmp_reg_486 == 1'd1) & (hrr_fsmState_load_reg_477 == 1'd0));
end

always @ (*) begin
    ap_predicate_op9_read_state1 = ((tmp_nbreadreq_fu_98_p3 == 1'd1) & (hrr_fsmState == 1'd0));
end

assign hrr_fsmState_load_load_fu_272_p1 = hrr_fsmState;

assign icmp_ln895_3_fu_324_p2 = ((readLength_V_2_fu_304_p4 > 32'd1408) ? 1'b1 : 1'b0);

assign icmp_ln895_fu_352_p2 = ((ap_sig_allocacmp_readLength_V > 32'd1408) ? 1'b1 : 1'b0);

assign readAddr_V_fu_294_p4 = {{rx_readRequestFifo_V_dout[71:24]}};

assign readLength_V_2_fu_304_p4 = {{rx_readRequestFifo_V_dout[103:72]}};

assign rx_remoteMemCmd_V_din = {{ap_const_lv113_0[112:112]}, {tmp_54_i_fu_384_p4}};

assign sext_ln738_fu_420_p1 = $signed(ap_phi_reg_pp0_iter1_tmp_op_code_reg_214);

assign tmp_2_fu_405_p7 = {{{{{{{{{{2'd1}, {tmp_psn_V_6_reg_508}}}, {ap_phi_reg_pp0_iter1_tmp_length_V_5_reg_144}}}, {48'd0}}}, {tmp_qpn_V_19_reg_490}}}, {ap_phi_reg_pp0_iter1_tmp_op_code_4_reg_155}};

assign tmp_3_fu_448_p7 = {{{{{{{{{{2'd1}, {tmp_psn_V_fu_432_p2}}}, {ap_phi_reg_pp0_iter1_tmp_length_V_reg_203}}}, {48'd0}}}, {request_qpn_V}}}, {zext_ln738_fu_424_p1}};

assign tmp_54_i_fu_384_p4 = {{{readLength_V_2_reg_501}, {tmp_addr_V_fu_381_p1}}, {tmp_qpn_V_18_reg_527}};

assign tmp_addr_V_fu_381_p1 = readAddr_V_reg_495;

assign tmp_nbreadreq_fu_98_p3 = rx_readRequestFifo_V_empty_n;

assign tmp_psn_V_fu_432_p2 = (request_psn_V + 24'd1);

assign tmp_qpn_V_18_fu_348_p1 = rx_readRequestFifo_V_dout[15:0];

assign tmp_qpn_V_19_fu_284_p1 = rx_readRequestFifo_V_dout[23:0];

assign zext_ln738_fu_424_p1 = $unsigned(sext_ln738_fu_420_p1);

endmodule //handle_read_requests
