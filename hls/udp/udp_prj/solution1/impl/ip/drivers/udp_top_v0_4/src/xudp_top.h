// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef XUDP_TOP_H
#define XUDP_TOP_H

#ifdef __cplusplus
extern "C" {
#endif

/***************************** Include Files *********************************/
#ifndef __linux__
#include "xil_types.h"
#include "xil_assert.h"
#include "xstatus.h"
#include "xil_io.h"
#else
#include <stdint.h>
#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stddef.h>
#endif
#include "xudp_top_hw.h"

/**************************** Type Definitions ******************************/
#ifdef __linux__
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
#else
typedef struct {
    u16 DeviceId;
    u32 Axilites_BaseAddress;
} XUdp_top_Config;
#endif

typedef struct {
    u32 Axilites_BaseAddress;
    u32 IsReady;
} XUdp_top;

typedef struct {
    u32 word_0;
    u32 word_1;
    u32 word_2;
    u32 word_3;
} XUdp_top_Reg_ip_address_v;

/***************** Macros (Inline Functions) Definitions *********************/
#ifndef __linux__
#define XUdp_top_WriteReg(BaseAddress, RegOffset, Data) \
    Xil_Out32((BaseAddress) + (RegOffset), (u32)(Data))
#define XUdp_top_ReadReg(BaseAddress, RegOffset) \
    Xil_In32((BaseAddress) + (RegOffset))
#else
#define XUdp_top_WriteReg(BaseAddress, RegOffset, Data) \
    *(volatile u32*)((BaseAddress) + (RegOffset)) = (u32)(Data)
#define XUdp_top_ReadReg(BaseAddress, RegOffset) \
    *(volatile u32*)((BaseAddress) + (RegOffset))

#define Xil_AssertVoid(expr)    assert(expr)
#define Xil_AssertNonvoid(expr) assert(expr)

#define XST_SUCCESS             0
#define XST_DEVICE_NOT_FOUND    2
#define XST_OPEN_DEVICE_FAILED  3
#define XIL_COMPONENT_IS_READY  1
#endif

/************************** Function Prototypes *****************************/
#ifndef __linux__
int XUdp_top_Initialize(XUdp_top *InstancePtr, u16 DeviceId);
XUdp_top_Config* XUdp_top_LookupConfig(u16 DeviceId);
int XUdp_top_CfgInitialize(XUdp_top *InstancePtr, XUdp_top_Config *ConfigPtr);
#else
int XUdp_top_Initialize(XUdp_top *InstancePtr, const char* InstanceName);
int XUdp_top_Release(XUdp_top *InstancePtr);
#endif


void XUdp_top_Set_reg_ip_address_V(XUdp_top *InstancePtr, XUdp_top_Reg_ip_address_v Data);
XUdp_top_Reg_ip_address_v XUdp_top_Get_reg_ip_address_V(XUdp_top *InstancePtr);
void XUdp_top_Set_reg_listen_port_V(XUdp_top *InstancePtr, u32 Data);
u32 XUdp_top_Get_reg_listen_port_V(XUdp_top *InstancePtr);

#ifdef __cplusplus
}
#endif

#endif
