// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2019.1
// Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

#include "merge_rx_meta.h"
#include "AESL_pkg.h"

using namespace std;

namespace ap_rtl {

const sc_logic merge_rx_meta::ap_const_logic_1 = sc_dt::Log_1;
const sc_logic merge_rx_meta::ap_const_logic_0 = sc_dt::Log_0;
const sc_lv<1> merge_rx_meta::ap_ST_fsm_pp0_stage0 = "1";
const sc_lv<32> merge_rx_meta::ap_const_lv32_0 = "00000000000000000000000000000000";
const bool merge_rx_meta::ap_const_boolean_1 = true;
const sc_lv<1> merge_rx_meta::ap_const_lv1_1 = "1";
const sc_lv<2> merge_rx_meta::ap_const_lv2_0 = "00";
const sc_lv<2> merge_rx_meta::ap_const_lv2_2 = "10";
const sc_lv<2> merge_rx_meta::ap_const_lv2_3 = "11";
const sc_lv<2> merge_rx_meta::ap_const_lv2_1 = "1";
const bool merge_rx_meta::ap_const_boolean_0 = false;
const sc_lv<1> merge_rx_meta::ap_const_lv1_0 = "0";
const sc_lv<32> merge_rx_meta::ap_const_lv32_30 = "110000";

merge_rx_meta::merge_rx_meta(sc_module_name name) : sc_module(name), mVcdFile(0) {

    SC_METHOD(thread_ap_clk_no_reset_);
    dont_initialize();
    sensitive << ( ap_clk.pos() );

    SC_METHOD(thread_ap_CS_fsm_pp0_stage0);
    sensitive << ( ap_CS_fsm );

    SC_METHOD(thread_ap_block_pp0_stage0);

    SC_METHOD(thread_ap_block_pp0_stage0_01001);
    sensitive << ( ap_start );
    sensitive << ( ap_done_reg );
    sensitive << ( ap_enable_reg_pp0_iter2 );
    sensitive << ( ipMetaIn_V_TVALID );
    sensitive << ( ap_predicate_op8_read_state1 );
    sensitive << ( rx_udpMetaFifo_V_empty_n );
    sensitive << ( ap_predicate_op10_read_state1 );
    sensitive << ( metaOut_V_TREADY );
    sensitive << ( metaOut_V_1_state );

    SC_METHOD(thread_ap_block_pp0_stage0_11001);
    sensitive << ( ap_start );
    sensitive << ( ap_done_reg );
    sensitive << ( ap_enable_reg_pp0_iter1 );
    sensitive << ( ap_enable_reg_pp0_iter2 );
    sensitive << ( ipMetaIn_V_TVALID );
    sensitive << ( ap_predicate_op8_read_state1 );
    sensitive << ( rx_udpMetaFifo_V_empty_n );
    sensitive << ( ap_predicate_op10_read_state1 );
    sensitive << ( metaOut_V_TREADY );
    sensitive << ( ap_block_state2_io );
    sensitive << ( metaOut_V_1_state );
    sensitive << ( ap_block_state3_io );

    SC_METHOD(thread_ap_block_pp0_stage0_subdone);
    sensitive << ( ap_start );
    sensitive << ( ap_done_reg );
    sensitive << ( ap_enable_reg_pp0_iter1 );
    sensitive << ( ap_enable_reg_pp0_iter2 );
    sensitive << ( ipMetaIn_V_TVALID );
    sensitive << ( ap_predicate_op8_read_state1 );
    sensitive << ( rx_udpMetaFifo_V_empty_n );
    sensitive << ( ap_predicate_op10_read_state1 );
    sensitive << ( metaOut_V_TREADY );
    sensitive << ( ap_block_state2_io );
    sensitive << ( metaOut_V_1_state );
    sensitive << ( ap_block_state3_io );

    SC_METHOD(thread_ap_block_state1_pp0_stage0_iter0);
    sensitive << ( ap_start );
    sensitive << ( ap_done_reg );
    sensitive << ( ipMetaIn_V_TVALID );
    sensitive << ( ap_predicate_op8_read_state1 );
    sensitive << ( rx_udpMetaFifo_V_empty_n );
    sensitive << ( ap_predicate_op10_read_state1 );

    SC_METHOD(thread_ap_block_state2_io);
    sensitive << ( metaOut_V_1_ack_in );
    sensitive << ( ap_predicate_op16_write_state2 );

    SC_METHOD(thread_ap_block_state2_pp0_stage0_iter1);

    SC_METHOD(thread_ap_block_state3_io);
    sensitive << ( metaOut_V_1_ack_in );
    sensitive << ( ap_predicate_op21_write_state3 );

    SC_METHOD(thread_ap_block_state3_pp0_stage0_iter2);
    sensitive << ( metaOut_V_TREADY );
    sensitive << ( metaOut_V_1_state );

    SC_METHOD(thread_ap_done);
    sensitive << ( ap_done_reg );
    sensitive << ( ap_enable_reg_pp0_iter2 );
    sensitive << ( ap_block_pp0_stage0_11001 );

    SC_METHOD(thread_ap_enable_pp0);
    sensitive << ( ap_idle_pp0 );

    SC_METHOD(thread_ap_enable_reg_pp0_iter0);
    sensitive << ( ap_start );

    SC_METHOD(thread_ap_idle);
    sensitive << ( ap_start );
    sensitive << ( ap_CS_fsm_pp0_stage0 );
    sensitive << ( ap_idle_pp0 );

    SC_METHOD(thread_ap_idle_pp0);
    sensitive << ( ap_enable_reg_pp0_iter0 );
    sensitive << ( ap_enable_reg_pp0_iter1 );
    sensitive << ( ap_enable_reg_pp0_iter2 );

    SC_METHOD(thread_ap_idle_pp0_0to1);
    sensitive << ( ap_enable_reg_pp0_iter0 );
    sensitive << ( ap_enable_reg_pp0_iter1 );

    SC_METHOD(thread_ap_predicate_op10_read_state1);
    sensitive << ( tmp_nbreadreq_fu_42_p3 );
    sensitive << ( tmp_3_nbreadreq_fu_50_p3 );

    SC_METHOD(thread_ap_predicate_op16_write_state2);
    sensitive << ( tmp_reg_104 );
    sensitive << ( tmp_3_reg_108 );
    sensitive << ( tmp_4_reg_117 );

    SC_METHOD(thread_ap_predicate_op21_write_state3);
    sensitive << ( tmp_reg_104_pp0_iter1_reg );
    sensitive << ( tmp_3_reg_108_pp0_iter1_reg );
    sensitive << ( tmp_4_reg_117_pp0_iter1_reg );

    SC_METHOD(thread_ap_predicate_op8_read_state1);
    sensitive << ( tmp_nbreadreq_fu_42_p3 );
    sensitive << ( tmp_3_nbreadreq_fu_50_p3 );

    SC_METHOD(thread_ap_ready);
    sensitive << ( ap_start );
    sensitive << ( ap_CS_fsm_pp0_stage0 );
    sensitive << ( ap_block_pp0_stage0_11001 );

    SC_METHOD(thread_ap_reset_idle_pp0);
    sensitive << ( ap_start );
    sensitive << ( ap_idle_pp0_0to1 );

    SC_METHOD(thread_ipMetaIn_V_TDATA_blk_n);
    sensitive << ( ap_start );
    sensitive << ( ap_done_reg );
    sensitive << ( ap_CS_fsm_pp0_stage0 );
    sensitive << ( ipMetaIn_V_TVALID );
    sensitive << ( ap_predicate_op8_read_state1 );
    sensitive << ( ap_block_pp0_stage0 );

    SC_METHOD(thread_ipMetaIn_V_TREADY);
    sensitive << ( ap_start );
    sensitive << ( ap_CS_fsm_pp0_stage0 );
    sensitive << ( ap_predicate_op8_read_state1 );
    sensitive << ( ap_block_pp0_stage0_11001 );

    SC_METHOD(thread_metaOut_V_1_ack_in);
    sensitive << ( metaOut_V_1_state );

    SC_METHOD(thread_metaOut_V_1_ack_out);
    sensitive << ( metaOut_V_TREADY );

    SC_METHOD(thread_metaOut_V_1_data_out);
    sensitive << ( metaOut_V_1_payload_A );
    sensitive << ( metaOut_V_1_payload_B );
    sensitive << ( metaOut_V_1_sel );

    SC_METHOD(thread_metaOut_V_1_load_A);
    sensitive << ( metaOut_V_1_sel_wr );
    sensitive << ( metaOut_V_1_state_cmp_full );

    SC_METHOD(thread_metaOut_V_1_load_B);
    sensitive << ( metaOut_V_1_sel_wr );
    sensitive << ( metaOut_V_1_state_cmp_full );

    SC_METHOD(thread_metaOut_V_1_sel);
    sensitive << ( metaOut_V_1_sel_rd );

    SC_METHOD(thread_metaOut_V_1_state_cmp_full);
    sensitive << ( metaOut_V_1_state );

    SC_METHOD(thread_metaOut_V_1_vld_in);
    sensitive << ( ap_CS_fsm_pp0_stage0 );
    sensitive << ( ap_enable_reg_pp0_iter1 );
    sensitive << ( ap_predicate_op16_write_state2 );
    sensitive << ( ap_block_pp0_stage0_11001 );

    SC_METHOD(thread_metaOut_V_1_vld_out);
    sensitive << ( metaOut_V_1_state );

    SC_METHOD(thread_metaOut_V_TDATA);
    sensitive << ( metaOut_V_1_data_out );

    SC_METHOD(thread_metaOut_V_TDATA_blk_n);
    sensitive << ( ap_CS_fsm_pp0_stage0 );
    sensitive << ( ap_enable_reg_pp0_iter1 );
    sensitive << ( ap_enable_reg_pp0_iter2 );
    sensitive << ( ap_predicate_op16_write_state2 );
    sensitive << ( metaOut_V_1_state );
    sensitive << ( ap_predicate_op21_write_state3 );
    sensitive << ( ap_block_pp0_stage0 );

    SC_METHOD(thread_metaOut_V_TVALID);
    sensitive << ( metaOut_V_1_state );

    SC_METHOD(thread_rx_udpMetaFifo_V_blk_n);
    sensitive << ( ap_start );
    sensitive << ( ap_done_reg );
    sensitive << ( ap_CS_fsm_pp0_stage0 );
    sensitive << ( rx_udpMetaFifo_V_empty_n );
    sensitive << ( ap_predicate_op10_read_state1 );
    sensitive << ( ap_block_pp0_stage0 );

    SC_METHOD(thread_rx_udpMetaFifo_V_read);
    sensitive << ( ap_start );
    sensitive << ( ap_CS_fsm_pp0_stage0 );
    sensitive << ( ap_predicate_op10_read_state1 );
    sensitive << ( ap_block_pp0_stage0_11001 );

    SC_METHOD(thread_tmp_28_fu_96_p3);
    sensitive << ( trunc_ln162_reg_121 );
    sensitive << ( tmp_their_address_V_fu_93_p1 );

    SC_METHOD(thread_tmp_3_nbreadreq_fu_50_p3);
    sensitive << ( rx_udpMetaFifo_V_empty_n );

    SC_METHOD(thread_tmp_4_fu_81_p3);
    sensitive << ( rx_udpMetaFifo_V_dout );

    SC_METHOD(thread_tmp_nbreadreq_fu_42_p3);
    sensitive << ( ipMetaIn_V_TVALID );

    SC_METHOD(thread_tmp_their_address_V_fu_93_p1);
    sensitive << ( trunc_ln321_reg_112 );

    SC_METHOD(thread_trunc_ln162_fu_89_p1);
    sensitive << ( rx_udpMetaFifo_V_dout );

    SC_METHOD(thread_trunc_ln321_fu_77_p1);
    sensitive << ( ipMetaIn_V_TDATA );

    SC_METHOD(thread_ap_NS_fsm);
    sensitive << ( ap_CS_fsm );
    sensitive << ( ap_block_pp0_stage0_subdone );
    sensitive << ( ap_reset_idle_pp0 );

    ap_done_reg = SC_LOGIC_0;
    ap_CS_fsm = "1";
    ap_enable_reg_pp0_iter1 = SC_LOGIC_0;
    ap_enable_reg_pp0_iter2 = SC_LOGIC_0;
    metaOut_V_1_state = "00";
    metaOut_V_1_sel_rd = SC_LOGIC_0;
    metaOut_V_1_sel_wr = SC_LOGIC_0;
    static int apTFileNum = 0;
    stringstream apTFilenSS;
    apTFilenSS << "merge_rx_meta_sc_trace_" << apTFileNum ++;
    string apTFn = apTFilenSS.str();
    mVcdFile = sc_create_vcd_trace_file(apTFn.c_str());
    mVcdFile->set_time_unit(1, SC_PS);
    if (1) {
#ifdef __HLS_TRACE_LEVEL_PORT_HIER__
    sc_trace(mVcdFile, ap_clk, "(port)ap_clk");
    sc_trace(mVcdFile, ap_rst, "(port)ap_rst");
    sc_trace(mVcdFile, ap_start, "(port)ap_start");
    sc_trace(mVcdFile, ap_done, "(port)ap_done");
    sc_trace(mVcdFile, ap_continue, "(port)ap_continue");
    sc_trace(mVcdFile, ap_idle, "(port)ap_idle");
    sc_trace(mVcdFile, ap_ready, "(port)ap_ready");
    sc_trace(mVcdFile, ipMetaIn_V_TVALID, "(port)ipMetaIn_V_TVALID");
    sc_trace(mVcdFile, rx_udpMetaFifo_V_dout, "(port)rx_udpMetaFifo_V_dout");
    sc_trace(mVcdFile, rx_udpMetaFifo_V_empty_n, "(port)rx_udpMetaFifo_V_empty_n");
    sc_trace(mVcdFile, rx_udpMetaFifo_V_read, "(port)rx_udpMetaFifo_V_read");
    sc_trace(mVcdFile, metaOut_V_TREADY, "(port)metaOut_V_TREADY");
    sc_trace(mVcdFile, ipMetaIn_V_TDATA, "(port)ipMetaIn_V_TDATA");
    sc_trace(mVcdFile, ipMetaIn_V_TREADY, "(port)ipMetaIn_V_TREADY");
    sc_trace(mVcdFile, metaOut_V_TDATA, "(port)metaOut_V_TDATA");
    sc_trace(mVcdFile, metaOut_V_TVALID, "(port)metaOut_V_TVALID");
#endif
#ifdef __HLS_TRACE_LEVEL_INT__
    sc_trace(mVcdFile, ap_done_reg, "ap_done_reg");
    sc_trace(mVcdFile, ap_CS_fsm, "ap_CS_fsm");
    sc_trace(mVcdFile, ap_CS_fsm_pp0_stage0, "ap_CS_fsm_pp0_stage0");
    sc_trace(mVcdFile, ap_enable_reg_pp0_iter0, "ap_enable_reg_pp0_iter0");
    sc_trace(mVcdFile, ap_enable_reg_pp0_iter1, "ap_enable_reg_pp0_iter1");
    sc_trace(mVcdFile, ap_enable_reg_pp0_iter2, "ap_enable_reg_pp0_iter2");
    sc_trace(mVcdFile, ap_idle_pp0, "ap_idle_pp0");
    sc_trace(mVcdFile, tmp_nbreadreq_fu_42_p3, "tmp_nbreadreq_fu_42_p3");
    sc_trace(mVcdFile, tmp_3_nbreadreq_fu_50_p3, "tmp_3_nbreadreq_fu_50_p3");
    sc_trace(mVcdFile, ap_predicate_op8_read_state1, "ap_predicate_op8_read_state1");
    sc_trace(mVcdFile, ap_predicate_op10_read_state1, "ap_predicate_op10_read_state1");
    sc_trace(mVcdFile, ap_block_state1_pp0_stage0_iter0, "ap_block_state1_pp0_stage0_iter0");
    sc_trace(mVcdFile, ap_block_state2_pp0_stage0_iter1, "ap_block_state2_pp0_stage0_iter1");
    sc_trace(mVcdFile, metaOut_V_1_ack_in, "metaOut_V_1_ack_in");
    sc_trace(mVcdFile, tmp_reg_104, "tmp_reg_104");
    sc_trace(mVcdFile, tmp_3_reg_108, "tmp_3_reg_108");
    sc_trace(mVcdFile, tmp_4_reg_117, "tmp_4_reg_117");
    sc_trace(mVcdFile, ap_predicate_op16_write_state2, "ap_predicate_op16_write_state2");
    sc_trace(mVcdFile, ap_block_state2_io, "ap_block_state2_io");
    sc_trace(mVcdFile, metaOut_V_1_ack_out, "metaOut_V_1_ack_out");
    sc_trace(mVcdFile, metaOut_V_1_state, "metaOut_V_1_state");
    sc_trace(mVcdFile, ap_block_state3_pp0_stage0_iter2, "ap_block_state3_pp0_stage0_iter2");
    sc_trace(mVcdFile, tmp_reg_104_pp0_iter1_reg, "tmp_reg_104_pp0_iter1_reg");
    sc_trace(mVcdFile, tmp_3_reg_108_pp0_iter1_reg, "tmp_3_reg_108_pp0_iter1_reg");
    sc_trace(mVcdFile, tmp_4_reg_117_pp0_iter1_reg, "tmp_4_reg_117_pp0_iter1_reg");
    sc_trace(mVcdFile, ap_predicate_op21_write_state3, "ap_predicate_op21_write_state3");
    sc_trace(mVcdFile, ap_block_state3_io, "ap_block_state3_io");
    sc_trace(mVcdFile, ap_block_pp0_stage0_11001, "ap_block_pp0_stage0_11001");
    sc_trace(mVcdFile, metaOut_V_1_data_out, "metaOut_V_1_data_out");
    sc_trace(mVcdFile, metaOut_V_1_vld_in, "metaOut_V_1_vld_in");
    sc_trace(mVcdFile, metaOut_V_1_vld_out, "metaOut_V_1_vld_out");
    sc_trace(mVcdFile, metaOut_V_1_payload_A, "metaOut_V_1_payload_A");
    sc_trace(mVcdFile, metaOut_V_1_payload_B, "metaOut_V_1_payload_B");
    sc_trace(mVcdFile, metaOut_V_1_sel_rd, "metaOut_V_1_sel_rd");
    sc_trace(mVcdFile, metaOut_V_1_sel_wr, "metaOut_V_1_sel_wr");
    sc_trace(mVcdFile, metaOut_V_1_sel, "metaOut_V_1_sel");
    sc_trace(mVcdFile, metaOut_V_1_load_A, "metaOut_V_1_load_A");
    sc_trace(mVcdFile, metaOut_V_1_load_B, "metaOut_V_1_load_B");
    sc_trace(mVcdFile, metaOut_V_1_state_cmp_full, "metaOut_V_1_state_cmp_full");
    sc_trace(mVcdFile, ipMetaIn_V_TDATA_blk_n, "ipMetaIn_V_TDATA_blk_n");
    sc_trace(mVcdFile, ap_block_pp0_stage0, "ap_block_pp0_stage0");
    sc_trace(mVcdFile, metaOut_V_TDATA_blk_n, "metaOut_V_TDATA_blk_n");
    sc_trace(mVcdFile, rx_udpMetaFifo_V_blk_n, "rx_udpMetaFifo_V_blk_n");
    sc_trace(mVcdFile, trunc_ln321_fu_77_p1, "trunc_ln321_fu_77_p1");
    sc_trace(mVcdFile, trunc_ln321_reg_112, "trunc_ln321_reg_112");
    sc_trace(mVcdFile, tmp_4_fu_81_p3, "tmp_4_fu_81_p3");
    sc_trace(mVcdFile, trunc_ln162_fu_89_p1, "trunc_ln162_fu_89_p1");
    sc_trace(mVcdFile, trunc_ln162_reg_121, "trunc_ln162_reg_121");
    sc_trace(mVcdFile, tmp_28_fu_96_p3, "tmp_28_fu_96_p3");
    sc_trace(mVcdFile, ap_block_pp0_stage0_subdone, "ap_block_pp0_stage0_subdone");
    sc_trace(mVcdFile, ap_block_pp0_stage0_01001, "ap_block_pp0_stage0_01001");
    sc_trace(mVcdFile, tmp_their_address_V_fu_93_p1, "tmp_their_address_V_fu_93_p1");
    sc_trace(mVcdFile, ap_NS_fsm, "ap_NS_fsm");
    sc_trace(mVcdFile, ap_idle_pp0_0to1, "ap_idle_pp0_0to1");
    sc_trace(mVcdFile, ap_reset_idle_pp0, "ap_reset_idle_pp0");
    sc_trace(mVcdFile, ap_enable_pp0, "ap_enable_pp0");
#endif

    }
}

merge_rx_meta::~merge_rx_meta() {
    if (mVcdFile) 
        sc_close_vcd_trace_file(mVcdFile);

}

void merge_rx_meta::thread_ap_clk_no_reset_() {
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_CS_fsm = ap_ST_fsm_pp0_stage0;
    } else {
        ap_CS_fsm = ap_NS_fsm.read();
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_done_reg = ap_const_logic_0;
    } else {
        if (esl_seteq<1,1,1>(ap_const_logic_1, ap_continue.read())) {
            ap_done_reg = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter2.read()) && 
                    esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0))) {
            ap_done_reg = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_enable_reg_pp0_iter1 = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
             esl_seteq<1,1,1>(ap_block_pp0_stage0_subdone.read(), ap_const_boolean_0))) {
            ap_enable_reg_pp0_iter1 = ap_start.read();
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_enable_reg_pp0_iter2 = ap_const_logic_0;
    } else {
        if (esl_seteq<1,1,1>(ap_block_pp0_stage0_subdone.read(), ap_const_boolean_0)) {
            ap_enable_reg_pp0_iter2 = ap_enable_reg_pp0_iter1.read();
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        metaOut_V_1_sel_rd = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, metaOut_V_1_ack_out.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, metaOut_V_1_vld_out.read()))) {
            metaOut_V_1_sel_rd =  (sc_logic) (~metaOut_V_1_sel_rd.read());
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        metaOut_V_1_sel_wr = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, metaOut_V_1_vld_in.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, metaOut_V_1_ack_in.read()))) {
            metaOut_V_1_sel_wr =  (sc_logic) (~metaOut_V_1_sel_wr.read());
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        metaOut_V_1_state = ap_const_lv2_0;
    } else {
        if (((esl_seteq<1,2,2>(metaOut_V_1_state.read(), ap_const_lv2_3) && 
              esl_seteq<1,1,1>(ap_const_logic_0, metaOut_V_1_vld_in.read()) && 
              esl_seteq<1,1,1>(ap_const_logic_1, metaOut_V_1_ack_out.read())) || 
             (esl_seteq<1,2,2>(metaOut_V_1_state.read(), ap_const_lv2_2) && 
              esl_seteq<1,1,1>(ap_const_logic_0, metaOut_V_1_vld_in.read())))) {
            metaOut_V_1_state = ap_const_lv2_2;
        } else if (((esl_seteq<1,1,1>(ap_const_logic_0, metaOut_V_TREADY.read()) && 
                     esl_seteq<1,2,2>(metaOut_V_1_state.read(), ap_const_lv2_3) && 
                     esl_seteq<1,1,1>(ap_const_logic_1, metaOut_V_1_vld_in.read())) || 
                    (esl_seteq<1,1,1>(ap_const_logic_0, metaOut_V_TREADY.read()) && 
                     esl_seteq<1,2,2>(metaOut_V_1_state.read(), ap_const_lv2_1)))) {
            metaOut_V_1_state = ap_const_lv2_1;
        } else if (((esl_seteq<1,2,2>(metaOut_V_1_state.read(), ap_const_lv2_2) && 
                     esl_seteq<1,1,1>(ap_const_logic_1, metaOut_V_1_vld_in.read())) || 
                    (esl_seteq<1,2,2>(metaOut_V_1_state.read(), ap_const_lv2_1) && 
                     esl_seteq<1,1,1>(ap_const_logic_1, metaOut_V_1_ack_out.read())) || 
                    (esl_seteq<1,2,2>(metaOut_V_1_state.read(), ap_const_lv2_3) && 
                     !(esl_seteq<1,1,1>(ap_const_logic_0, metaOut_V_TREADY.read()) && esl_seteq<1,1,1>(ap_const_logic_1, metaOut_V_1_vld_in.read())) && 
                     !(esl_seteq<1,1,1>(ap_const_logic_0, metaOut_V_1_vld_in.read()) && esl_seteq<1,1,1>(ap_const_logic_1, metaOut_V_1_ack_out.read()))))) {
            metaOut_V_1_state = ap_const_lv2_3;
        } else {
            metaOut_V_1_state = ap_const_lv2_2;
        }
    }
    if (esl_seteq<1,1,1>(ap_const_logic_1, metaOut_V_1_load_A.read())) {
        metaOut_V_1_payload_A = tmp_28_fu_96_p3.read();
    }
    if (esl_seteq<1,1,1>(ap_const_logic_1, metaOut_V_1_load_B.read())) {
        metaOut_V_1_payload_B = tmp_28_fu_96_p3.read();
    }
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && esl_seteq<1,1,1>(tmp_nbreadreq_fu_42_p3.read(), ap_const_lv1_1) && esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0))) {
        tmp_3_reg_108 = tmp_3_nbreadreq_fu_50_p3.read();
    }
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0))) {
        tmp_3_reg_108_pp0_iter1_reg = tmp_3_reg_108.read();
        tmp_4_reg_117_pp0_iter1_reg = tmp_4_reg_117.read();
        tmp_reg_104 = tmp_nbreadreq_fu_42_p3.read();
        tmp_reg_104_pp0_iter1_reg = tmp_reg_104.read();
    }
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && esl_seteq<1,1,1>(tmp_nbreadreq_fu_42_p3.read(), ap_const_lv1_1) && esl_seteq<1,1,1>(ap_const_lv1_1, tmp_3_nbreadreq_fu_50_p3.read()) && esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0))) {
        tmp_4_reg_117 = rx_udpMetaFifo_V_dout.read().range(48, 48);
        trunc_ln321_reg_112 = trunc_ln321_fu_77_p1.read();
    }
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && esl_seteq<1,1,1>(tmp_nbreadreq_fu_42_p3.read(), ap_const_lv1_1) && esl_seteq<1,1,1>(ap_const_lv1_1, tmp_3_nbreadreq_fu_50_p3.read()) && esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0) && esl_seteq<1,1,1>(ap_const_lv1_1, tmp_4_fu_81_p3.read()))) {
        trunc_ln162_reg_121 = trunc_ln162_fu_89_p1.read();
    }
}

void merge_rx_meta::thread_ap_CS_fsm_pp0_stage0() {
    ap_CS_fsm_pp0_stage0 = ap_CS_fsm.read()[0];
}

void merge_rx_meta::thread_ap_block_pp0_stage0() {
    ap_block_pp0_stage0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void merge_rx_meta::thread_ap_block_pp0_stage0_01001() {
    ap_block_pp0_stage0_01001 = (esl_seteq<1,1,1>(ap_done_reg.read(), ap_const_logic_1) || (esl_seteq<1,1,1>(ap_const_logic_1, ap_start.read()) && 
  (esl_seteq<1,1,1>(ap_const_logic_0, ap_start.read()) || 
   (esl_seteq<1,1,1>(ap_const_logic_0, ipMetaIn_V_TVALID.read()) && 
    esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op8_read_state1.read())) || 
   (esl_seteq<1,1,1>(ap_const_logic_0, rx_udpMetaFifo_V_empty_n.read()) && 
    esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op10_read_state1.read())) || 
   esl_seteq<1,1,1>(ap_done_reg.read(), ap_const_logic_1))) || ((esl_seteq<1,2,2>(metaOut_V_1_state.read(), ap_const_lv2_1) || 
   (esl_seteq<1,1,1>(ap_const_logic_0, metaOut_V_TREADY.read()) && 
    esl_seteq<1,2,2>(metaOut_V_1_state.read(), ap_const_lv2_3))) && 
  esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter2.read())));
}

void merge_rx_meta::thread_ap_block_pp0_stage0_11001() {
    ap_block_pp0_stage0_11001 = (esl_seteq<1,1,1>(ap_done_reg.read(), ap_const_logic_1) || (esl_seteq<1,1,1>(ap_const_logic_1, ap_start.read()) && 
  (esl_seteq<1,1,1>(ap_const_logic_0, ap_start.read()) || 
   (esl_seteq<1,1,1>(ap_const_logic_0, ipMetaIn_V_TVALID.read()) && 
    esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op8_read_state1.read())) || 
   (esl_seteq<1,1,1>(ap_const_logic_0, rx_udpMetaFifo_V_empty_n.read()) && 
    esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op10_read_state1.read())) || 
   esl_seteq<1,1,1>(ap_done_reg.read(), ap_const_logic_1))) || (esl_seteq<1,1,1>(ap_const_boolean_1, ap_block_state2_io.read()) && 
  esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter1.read())) || ((esl_seteq<1,2,2>(metaOut_V_1_state.read(), ap_const_lv2_1) || 
   (esl_seteq<1,1,1>(ap_const_logic_0, metaOut_V_TREADY.read()) && 
    esl_seteq<1,2,2>(metaOut_V_1_state.read(), ap_const_lv2_3)) || 
   esl_seteq<1,1,1>(ap_const_boolean_1, ap_block_state3_io.read())) && 
  esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter2.read())));
}

void merge_rx_meta::thread_ap_block_pp0_stage0_subdone() {
    ap_block_pp0_stage0_subdone = (esl_seteq<1,1,1>(ap_done_reg.read(), ap_const_logic_1) || (esl_seteq<1,1,1>(ap_const_logic_1, ap_start.read()) && 
  (esl_seteq<1,1,1>(ap_const_logic_0, ap_start.read()) || 
   (esl_seteq<1,1,1>(ap_const_logic_0, ipMetaIn_V_TVALID.read()) && 
    esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op8_read_state1.read())) || 
   (esl_seteq<1,1,1>(ap_const_logic_0, rx_udpMetaFifo_V_empty_n.read()) && 
    esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op10_read_state1.read())) || 
   esl_seteq<1,1,1>(ap_done_reg.read(), ap_const_logic_1))) || (esl_seteq<1,1,1>(ap_const_boolean_1, ap_block_state2_io.read()) && 
  esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter1.read())) || ((esl_seteq<1,2,2>(metaOut_V_1_state.read(), ap_const_lv2_1) || 
   (esl_seteq<1,1,1>(ap_const_logic_0, metaOut_V_TREADY.read()) && 
    esl_seteq<1,2,2>(metaOut_V_1_state.read(), ap_const_lv2_3)) || 
   esl_seteq<1,1,1>(ap_const_boolean_1, ap_block_state3_io.read())) && 
  esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter2.read())));
}

void merge_rx_meta::thread_ap_block_state1_pp0_stage0_iter0() {
    ap_block_state1_pp0_stage0_iter0 = (esl_seteq<1,1,1>(ap_const_logic_0, ap_start.read()) || (esl_seteq<1,1,1>(ap_const_logic_0, ipMetaIn_V_TVALID.read()) && 
  esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op8_read_state1.read())) || (esl_seteq<1,1,1>(ap_const_logic_0, rx_udpMetaFifo_V_empty_n.read()) && 
  esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op10_read_state1.read())) || esl_seteq<1,1,1>(ap_done_reg.read(), ap_const_logic_1));
}

void merge_rx_meta::thread_ap_block_state2_io() {
    ap_block_state2_io = (esl_seteq<1,1,1>(ap_const_logic_0, metaOut_V_1_ack_in.read()) && esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op16_write_state2.read()));
}

void merge_rx_meta::thread_ap_block_state2_pp0_stage0_iter1() {
    ap_block_state2_pp0_stage0_iter1 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void merge_rx_meta::thread_ap_block_state3_io() {
    ap_block_state3_io = (esl_seteq<1,1,1>(ap_const_logic_0, metaOut_V_1_ack_in.read()) && esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op21_write_state3.read()));
}

void merge_rx_meta::thread_ap_block_state3_pp0_stage0_iter2() {
    ap_block_state3_pp0_stage0_iter2 = (esl_seteq<1,2,2>(metaOut_V_1_state.read(), ap_const_lv2_1) || (esl_seteq<1,1,1>(ap_const_logic_0, metaOut_V_TREADY.read()) && 
  esl_seteq<1,2,2>(metaOut_V_1_state.read(), ap_const_lv2_3)));
}

void merge_rx_meta::thread_ap_done() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter2.read()) && 
         esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0))) {
        ap_done = ap_const_logic_1;
    } else {
        ap_done = ap_done_reg.read();
    }
}

void merge_rx_meta::thread_ap_enable_pp0() {
    ap_enable_pp0 = (ap_idle_pp0.read() ^ ap_const_logic_1);
}

void merge_rx_meta::thread_ap_enable_reg_pp0_iter0() {
    ap_enable_reg_pp0_iter0 = ap_start.read();
}

void merge_rx_meta::thread_ap_idle() {
    if ((esl_seteq<1,1,1>(ap_const_logic_0, ap_start.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_idle_pp0.read()))) {
        ap_idle = ap_const_logic_1;
    } else {
        ap_idle = ap_const_logic_0;
    }
}

void merge_rx_meta::thread_ap_idle_pp0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_0, ap_enable_reg_pp0_iter0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, ap_enable_reg_pp0_iter1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, ap_enable_reg_pp0_iter2.read()))) {
        ap_idle_pp0 = ap_const_logic_1;
    } else {
        ap_idle_pp0 = ap_const_logic_0;
    }
}

void merge_rx_meta::thread_ap_idle_pp0_0to1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_0, ap_enable_reg_pp0_iter0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, ap_enable_reg_pp0_iter1.read()))) {
        ap_idle_pp0_0to1 = ap_const_logic_1;
    } else {
        ap_idle_pp0_0to1 = ap_const_logic_0;
    }
}

void merge_rx_meta::thread_ap_predicate_op10_read_state1() {
    ap_predicate_op10_read_state1 = (esl_seteq<1,1,1>(tmp_nbreadreq_fu_42_p3.read(), ap_const_lv1_1) && esl_seteq<1,1,1>(ap_const_lv1_1, tmp_3_nbreadreq_fu_50_p3.read()));
}

void merge_rx_meta::thread_ap_predicate_op16_write_state2() {
    ap_predicate_op16_write_state2 = (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_104.read()) && esl_seteq<1,1,1>(ap_const_lv1_1, tmp_3_reg_108.read()) && esl_seteq<1,1,1>(ap_const_lv1_1, tmp_4_reg_117.read()));
}

void merge_rx_meta::thread_ap_predicate_op21_write_state3() {
    ap_predicate_op21_write_state3 = (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_104_pp0_iter1_reg.read()) && esl_seteq<1,1,1>(ap_const_lv1_1, tmp_3_reg_108_pp0_iter1_reg.read()) && esl_seteq<1,1,1>(ap_const_lv1_1, tmp_4_reg_117_pp0_iter1_reg.read()));
}

void merge_rx_meta::thread_ap_predicate_op8_read_state1() {
    ap_predicate_op8_read_state1 = (esl_seteq<1,1,1>(tmp_nbreadreq_fu_42_p3.read(), ap_const_lv1_1) && esl_seteq<1,1,1>(ap_const_lv1_1, tmp_3_nbreadreq_fu_50_p3.read()));
}

void merge_rx_meta::thread_ap_ready() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_start.read()) && 
         esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0))) {
        ap_ready = ap_const_logic_1;
    } else {
        ap_ready = ap_const_logic_0;
    }
}

void merge_rx_meta::thread_ap_reset_idle_pp0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_0, ap_start.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_idle_pp0_0to1.read()))) {
        ap_reset_idle_pp0 = ap_const_logic_1;
    } else {
        ap_reset_idle_pp0 = ap_const_logic_0;
    }
}

void merge_rx_meta::thread_ipMetaIn_V_TDATA_blk_n() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_start.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op8_read_state1.read()) && 
         !(esl_seteq<1,1,1>(ap_const_logic_0, ap_start.read()) || esl_seteq<1,1,1>(ap_done_reg.read(), ap_const_logic_1)) && 
         esl_seteq<1,1,1>(ap_block_pp0_stage0.read(), ap_const_boolean_0))) {
        ipMetaIn_V_TDATA_blk_n = ipMetaIn_V_TVALID.read();
    } else {
        ipMetaIn_V_TDATA_blk_n = ap_const_logic_1;
    }
}

void merge_rx_meta::thread_ipMetaIn_V_TREADY() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_start.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op8_read_state1.read()) && 
         esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0))) {
        ipMetaIn_V_TREADY = ap_const_logic_1;
    } else {
        ipMetaIn_V_TREADY = ap_const_logic_0;
    }
}

void merge_rx_meta::thread_metaOut_V_1_ack_in() {
    metaOut_V_1_ack_in = metaOut_V_1_state.read()[1];
}

void merge_rx_meta::thread_metaOut_V_1_ack_out() {
    metaOut_V_1_ack_out = metaOut_V_TREADY.read();
}

void merge_rx_meta::thread_metaOut_V_1_data_out() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, metaOut_V_1_sel.read())) {
        metaOut_V_1_data_out = metaOut_V_1_payload_B.read();
    } else {
        metaOut_V_1_data_out = metaOut_V_1_payload_A.read();
    }
}

void merge_rx_meta::thread_metaOut_V_1_load_A() {
    metaOut_V_1_load_A = (metaOut_V_1_state_cmp_full.read() & ~metaOut_V_1_sel_wr.read());
}

void merge_rx_meta::thread_metaOut_V_1_load_B() {
    metaOut_V_1_load_B = (metaOut_V_1_sel_wr.read() & metaOut_V_1_state_cmp_full.read());
}

void merge_rx_meta::thread_metaOut_V_1_sel() {
    metaOut_V_1_sel = metaOut_V_1_sel_rd.read();
}

void merge_rx_meta::thread_metaOut_V_1_state_cmp_full() {
    metaOut_V_1_state_cmp_full =  (sc_logic) ((!metaOut_V_1_state.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<1>(): sc_lv<1>(metaOut_V_1_state.read() != ap_const_lv2_1))[0];
}

void merge_rx_meta::thread_metaOut_V_1_vld_in() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op16_write_state2.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter1.read()) && 
         esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0))) {
        metaOut_V_1_vld_in = ap_const_logic_1;
    } else {
        metaOut_V_1_vld_in = ap_const_logic_0;
    }
}

void merge_rx_meta::thread_metaOut_V_1_vld_out() {
    metaOut_V_1_vld_out = metaOut_V_1_state.read()[0];
}

void merge_rx_meta::thread_metaOut_V_TDATA() {
    metaOut_V_TDATA = metaOut_V_1_data_out.read();
}

void merge_rx_meta::thread_metaOut_V_TDATA_blk_n() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op16_write_state2.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter1.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage0.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op21_write_state3.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter2.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage0.read(), ap_const_boolean_0)))) {
        metaOut_V_TDATA_blk_n = metaOut_V_1_state.read()[1];
    } else {
        metaOut_V_TDATA_blk_n = ap_const_logic_1;
    }
}

void merge_rx_meta::thread_metaOut_V_TVALID() {
    metaOut_V_TVALID = metaOut_V_1_state.read()[0];
}

void merge_rx_meta::thread_rx_udpMetaFifo_V_blk_n() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_start.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op10_read_state1.read()) && 
         !(esl_seteq<1,1,1>(ap_const_logic_0, ap_start.read()) || esl_seteq<1,1,1>(ap_done_reg.read(), ap_const_logic_1)) && 
         esl_seteq<1,1,1>(ap_block_pp0_stage0.read(), ap_const_boolean_0))) {
        rx_udpMetaFifo_V_blk_n = rx_udpMetaFifo_V_empty_n.read();
    } else {
        rx_udpMetaFifo_V_blk_n = ap_const_logic_1;
    }
}

void merge_rx_meta::thread_rx_udpMetaFifo_V_read() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_start.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op10_read_state1.read()) && 
         esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0))) {
        rx_udpMetaFifo_V_read = ap_const_logic_1;
    } else {
        rx_udpMetaFifo_V_read = ap_const_logic_0;
    }
}

void merge_rx_meta::thread_tmp_28_fu_96_p3() {
    tmp_28_fu_96_p3 = esl_concat<48,128>(trunc_ln162_reg_121.read(), tmp_their_address_V_fu_93_p1.read());
}

void merge_rx_meta::thread_tmp_3_nbreadreq_fu_50_p3() {
    tmp_3_nbreadreq_fu_50_p3 =  (sc_lv<1>) ((rx_udpMetaFifo_V_empty_n.read()));
}

void merge_rx_meta::thread_tmp_4_fu_81_p3() {
    tmp_4_fu_81_p3 = rx_udpMetaFifo_V_dout.read().range(48, 48);
}

void merge_rx_meta::thread_tmp_nbreadreq_fu_42_p3() {
    tmp_nbreadreq_fu_42_p3 =  (sc_lv<1>) ((ipMetaIn_V_TVALID.read()));
}

void merge_rx_meta::thread_tmp_their_address_V_fu_93_p1() {
    tmp_their_address_V_fu_93_p1 = esl_zext<128,32>(trunc_ln321_reg_112.read());
}

void merge_rx_meta::thread_trunc_ln162_fu_89_p1() {
    trunc_ln162_fu_89_p1 = rx_udpMetaFifo_V_dout.read().range(48-1, 0);
}

void merge_rx_meta::thread_trunc_ln321_fu_77_p1() {
    trunc_ln321_fu_77_p1 = ipMetaIn_V_TDATA.read().range(32-1, 0);
}

void merge_rx_meta::thread_ap_NS_fsm() {
    switch (ap_CS_fsm.read().to_uint64()) {
        case 1 : 
            ap_NS_fsm = ap_ST_fsm_pp0_stage0;
break;
        default : 
            ap_NS_fsm = "X";
            break;
    }
}

}

