// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2019.1
// Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

#ifndef _split_tx_meta_HH_
#define _split_tx_meta_HH_

#include "systemc.h"
#include "AESL_pkg.h"


namespace ap_rtl {

struct split_tx_meta : public sc_module {
    // Port declarations 25
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_in< sc_logic > ap_start;
    sc_out< sc_logic > ap_done;
    sc_in< sc_logic > ap_continue;
    sc_out< sc_logic > ap_idle;
    sc_out< sc_logic > ap_ready;
    sc_in< sc_logic > metaIn_V_TVALID;
    sc_out< sc_lv<16> > tx_udpMetaFifo_V_the_din;
    sc_in< sc_logic > tx_udpMetaFifo_V_the_full_n;
    sc_out< sc_logic > tx_udpMetaFifo_V_the_write;
    sc_out< sc_lv<16> > tx_udpMetaFifo_V_my_s_din;
    sc_in< sc_logic > tx_udpMetaFifo_V_my_s_full_n;
    sc_out< sc_logic > tx_udpMetaFifo_V_my_s_write;
    sc_out< sc_lv<16> > tx_udpMetaFifo_V_len_din;
    sc_in< sc_logic > tx_udpMetaFifo_V_len_full_n;
    sc_out< sc_logic > tx_udpMetaFifo_V_len_write;
    sc_out< sc_lv<1> > tx_udpMetaFifo_V_val_din;
    sc_in< sc_logic > tx_udpMetaFifo_V_val_full_n;
    sc_out< sc_logic > tx_udpMetaFifo_V_val_write;
    sc_in< sc_logic > metaOut0_V_TREADY;
    sc_in< sc_lv<176> > metaIn_V_TDATA;
    sc_out< sc_logic > metaIn_V_TREADY;
    sc_out< sc_lv<48> > metaOut0_V_TDATA;
    sc_out< sc_logic > metaOut0_V_TVALID;


    // Module declarations
    split_tx_meta(sc_module_name name);
    SC_HAS_PROCESS(split_tx_meta);

    ~split_tx_meta();

    sc_trace_file* mVcdFile;

    sc_signal< sc_logic > ap_done_reg;
    sc_signal< sc_lv<1> > ap_CS_fsm;
    sc_signal< sc_logic > ap_CS_fsm_pp0_stage0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter1;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter2;
    sc_signal< sc_logic > ap_idle_pp0;
    sc_signal< sc_lv<1> > tmp_nbreadreq_fu_66_p3;
    sc_signal< bool > ap_block_state1_pp0_stage0_iter0;
    sc_signal< sc_logic > io_acc_block_signal_op14;
    sc_signal< sc_lv<1> > tmp_reg_158;
    sc_signal< bool > ap_block_state2_pp0_stage0_iter1;
    sc_signal< sc_logic > metaOut0_V_1_ack_in;
    sc_signal< bool > ap_block_state2_io;
    sc_signal< sc_logic > metaOut0_V_1_ack_out;
    sc_signal< sc_lv<2> > metaOut0_V_1_state;
    sc_signal< bool > ap_block_state3_pp0_stage0_iter2;
    sc_signal< sc_lv<1> > tmp_reg_158_pp0_iter1_reg;
    sc_signal< bool > ap_block_state3_io;
    sc_signal< bool > ap_block_pp0_stage0_11001;
    sc_signal< sc_lv<48> > metaOut0_V_1_data_out;
    sc_signal< sc_logic > metaOut0_V_1_vld_in;
    sc_signal< sc_logic > metaOut0_V_1_vld_out;
    sc_signal< sc_lv<48> > metaOut0_V_1_payload_A;
    sc_signal< sc_lv<48> > metaOut0_V_1_payload_B;
    sc_signal< sc_logic > metaOut0_V_1_sel_rd;
    sc_signal< sc_logic > metaOut0_V_1_sel_wr;
    sc_signal< sc_logic > metaOut0_V_1_sel;
    sc_signal< sc_logic > metaOut0_V_1_load_A;
    sc_signal< sc_logic > metaOut0_V_1_load_B;
    sc_signal< sc_logic > metaOut0_V_1_state_cmp_full;
    sc_signal< sc_logic > metaIn_V_TDATA_blk_n;
    sc_signal< bool > ap_block_pp0_stage0;
    sc_signal< sc_logic > metaOut0_V_TDATA_blk_n;
    sc_signal< sc_logic > tx_udpMetaFifo_V_the_blk_n;
    sc_signal< sc_logic > tx_udpMetaFifo_V_my_s_blk_n;
    sc_signal< sc_logic > tx_udpMetaFifo_V_len_blk_n;
    sc_signal< sc_logic > tx_udpMetaFifo_V_val_blk_n;
    sc_signal< sc_lv<16> > tmp_their_port_V_reg_162;
    sc_signal< sc_lv<16> > tmp_my_port_V_reg_167;
    sc_signal< sc_lv<16> > tmp_length_V_load_ne_reg_172;
    sc_signal< sc_lv<32> > tmp_their_address_V_reg_177;
    sc_signal< sc_lv<48> > tmp_1_fu_150_p3;
    sc_signal< bool > ap_block_pp0_stage0_subdone;
    sc_signal< bool > ap_block_pp0_stage0_01001;
    sc_signal< sc_lv<16> > tempLen_V_fu_144_p2;
    sc_signal< sc_lv<1> > ap_NS_fsm;
    sc_signal< sc_logic > ap_idle_pp0_0to1;
    sc_signal< sc_logic > ap_reset_idle_pp0;
    sc_signal< sc_logic > ap_enable_pp0;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<1> ap_ST_fsm_pp0_stage0;
    static const sc_lv<32> ap_const_lv32_0;
    static const bool ap_const_boolean_1;
    static const sc_lv<1> ap_const_lv1_1;
    static const sc_lv<2> ap_const_lv2_0;
    static const sc_lv<2> ap_const_lv2_2;
    static const sc_lv<2> ap_const_lv2_3;
    static const sc_lv<2> ap_const_lv2_1;
    static const bool ap_const_boolean_0;
    static const sc_lv<1> ap_const_lv1_0;
    static const sc_lv<32> ap_const_lv32_80;
    static const sc_lv<32> ap_const_lv32_8F;
    static const sc_lv<32> ap_const_lv32_90;
    static const sc_lv<32> ap_const_lv32_9F;
    static const sc_lv<32> ap_const_lv32_A0;
    static const sc_lv<32> ap_const_lv32_AF;
    static const sc_lv<32> ap_const_lv32_60;
    static const sc_lv<32> ap_const_lv32_7F;
    static const sc_lv<16> ap_const_lv16_8;
    // Thread declarations
    void thread_ap_clk_no_reset_();
    void thread_ap_CS_fsm_pp0_stage0();
    void thread_ap_block_pp0_stage0();
    void thread_ap_block_pp0_stage0_01001();
    void thread_ap_block_pp0_stage0_11001();
    void thread_ap_block_pp0_stage0_subdone();
    void thread_ap_block_state1_pp0_stage0_iter0();
    void thread_ap_block_state2_io();
    void thread_ap_block_state2_pp0_stage0_iter1();
    void thread_ap_block_state3_io();
    void thread_ap_block_state3_pp0_stage0_iter2();
    void thread_ap_done();
    void thread_ap_enable_pp0();
    void thread_ap_enable_reg_pp0_iter0();
    void thread_ap_idle();
    void thread_ap_idle_pp0();
    void thread_ap_idle_pp0_0to1();
    void thread_ap_ready();
    void thread_ap_reset_idle_pp0();
    void thread_io_acc_block_signal_op14();
    void thread_metaIn_V_TDATA_blk_n();
    void thread_metaIn_V_TREADY();
    void thread_metaOut0_V_1_ack_in();
    void thread_metaOut0_V_1_ack_out();
    void thread_metaOut0_V_1_data_out();
    void thread_metaOut0_V_1_load_A();
    void thread_metaOut0_V_1_load_B();
    void thread_metaOut0_V_1_sel();
    void thread_metaOut0_V_1_state_cmp_full();
    void thread_metaOut0_V_1_vld_in();
    void thread_metaOut0_V_1_vld_out();
    void thread_metaOut0_V_TDATA();
    void thread_metaOut0_V_TDATA_blk_n();
    void thread_metaOut0_V_TVALID();
    void thread_tempLen_V_fu_144_p2();
    void thread_tmp_1_fu_150_p3();
    void thread_tmp_nbreadreq_fu_66_p3();
    void thread_tx_udpMetaFifo_V_len_blk_n();
    void thread_tx_udpMetaFifo_V_len_din();
    void thread_tx_udpMetaFifo_V_len_write();
    void thread_tx_udpMetaFifo_V_my_s_blk_n();
    void thread_tx_udpMetaFifo_V_my_s_din();
    void thread_tx_udpMetaFifo_V_my_s_write();
    void thread_tx_udpMetaFifo_V_the_blk_n();
    void thread_tx_udpMetaFifo_V_the_din();
    void thread_tx_udpMetaFifo_V_the_write();
    void thread_tx_udpMetaFifo_V_val_blk_n();
    void thread_tx_udpMetaFifo_V_val_din();
    void thread_tx_udpMetaFifo_V_val_write();
    void thread_ap_NS_fsm();
};

}

using namespace ap_rtl;

#endif
