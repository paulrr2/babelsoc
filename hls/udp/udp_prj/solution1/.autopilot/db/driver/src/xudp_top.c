// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================
/***************************** Include Files *********************************/
#include "xudp_top.h"

/************************** Function Implementation *************************/
#ifndef __linux__
int XUdp_top_CfgInitialize(XUdp_top *InstancePtr, XUdp_top_Config *ConfigPtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(ConfigPtr != NULL);

    InstancePtr->Axilites_BaseAddress = ConfigPtr->Axilites_BaseAddress;
    InstancePtr->IsReady = XIL_COMPONENT_IS_READY;

    return XST_SUCCESS;
}
#endif

void XUdp_top_Set_reg_ip_address_V(XUdp_top *InstancePtr, XUdp_top_Reg_ip_address_v Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XUdp_top_WriteReg(InstancePtr->Axilites_BaseAddress, XUDP_TOP_AXILITES_ADDR_REG_IP_ADDRESS_V_DATA + 0, Data.word_0);
    XUdp_top_WriteReg(InstancePtr->Axilites_BaseAddress, XUDP_TOP_AXILITES_ADDR_REG_IP_ADDRESS_V_DATA + 4, Data.word_1);
    XUdp_top_WriteReg(InstancePtr->Axilites_BaseAddress, XUDP_TOP_AXILITES_ADDR_REG_IP_ADDRESS_V_DATA + 8, Data.word_2);
    XUdp_top_WriteReg(InstancePtr->Axilites_BaseAddress, XUDP_TOP_AXILITES_ADDR_REG_IP_ADDRESS_V_DATA + 12, Data.word_3);
}

XUdp_top_Reg_ip_address_v XUdp_top_Get_reg_ip_address_V(XUdp_top *InstancePtr) {
    XUdp_top_Reg_ip_address_v Data;

    Data.word_0 = XUdp_top_ReadReg(InstancePtr->Axilites_BaseAddress, XUDP_TOP_AXILITES_ADDR_REG_IP_ADDRESS_V_DATA + 0);
    Data.word_1 = XUdp_top_ReadReg(InstancePtr->Axilites_BaseAddress, XUDP_TOP_AXILITES_ADDR_REG_IP_ADDRESS_V_DATA + 4);
    Data.word_2 = XUdp_top_ReadReg(InstancePtr->Axilites_BaseAddress, XUDP_TOP_AXILITES_ADDR_REG_IP_ADDRESS_V_DATA + 8);
    Data.word_3 = XUdp_top_ReadReg(InstancePtr->Axilites_BaseAddress, XUDP_TOP_AXILITES_ADDR_REG_IP_ADDRESS_V_DATA + 12);
    return Data;
}

void XUdp_top_Set_reg_listen_port_V(XUdp_top *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XUdp_top_WriteReg(InstancePtr->Axilites_BaseAddress, XUDP_TOP_AXILITES_ADDR_REG_LISTEN_PORT_V_DATA, Data);
}

u32 XUdp_top_Get_reg_listen_port_V(XUdp_top *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XUdp_top_ReadReg(InstancePtr->Axilites_BaseAddress, XUDP_TOP_AXILITES_ADDR_REG_LISTEN_PORT_V_DATA);
    return Data;
}

