// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef __linux__

#include "xstatus.h"
#include "xparameters.h"
#include "xudp_top.h"

extern XUdp_top_Config XUdp_top_ConfigTable[];

XUdp_top_Config *XUdp_top_LookupConfig(u16 DeviceId) {
	XUdp_top_Config *ConfigPtr = NULL;

	int Index;

	for (Index = 0; Index < XPAR_XUDP_TOP_NUM_INSTANCES; Index++) {
		if (XUdp_top_ConfigTable[Index].DeviceId == DeviceId) {
			ConfigPtr = &XUdp_top_ConfigTable[Index];
			break;
		}
	}

	return ConfigPtr;
}

int XUdp_top_Initialize(XUdp_top *InstancePtr, u16 DeviceId) {
	XUdp_top_Config *ConfigPtr;

	Xil_AssertNonvoid(InstancePtr != NULL);

	ConfigPtr = XUdp_top_LookupConfig(DeviceId);
	if (ConfigPtr == NULL) {
		InstancePtr->IsReady = 0;
		return (XST_DEVICE_NOT_FOUND);
	}

	return XUdp_top_CfgInitialize(InstancePtr, ConfigPtr);
}

#endif

