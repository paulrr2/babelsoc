// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================
// AXILiteS
// 0x00 : reserved
// 0x04 : reserved
// 0x08 : reserved
// 0x0c : reserved
// 0x10 : Data signal of reg_ip_address_V
//        bit 31~0 - reg_ip_address_V[31:0] (Read/Write)
// 0x14 : Data signal of reg_ip_address_V
//        bit 31~0 - reg_ip_address_V[63:32] (Read/Write)
// 0x18 : Data signal of reg_ip_address_V
//        bit 31~0 - reg_ip_address_V[95:64] (Read/Write)
// 0x1c : Data signal of reg_ip_address_V
//        bit 31~0 - reg_ip_address_V[127:96] (Read/Write)
// 0x20 : reserved
// 0x24 : Data signal of reg_listen_port_V
//        bit 15~0 - reg_listen_port_V[15:0] (Read/Write)
//        others   - reserved
// 0x28 : reserved
// (SC = Self Clear, COR = Clear on Read, TOW = Toggle on Write, COH = Clear on Handshake)

#define XUDP_TOP_AXILITES_ADDR_REG_IP_ADDRESS_V_DATA  0x10
#define XUDP_TOP_AXILITES_BITS_REG_IP_ADDRESS_V_DATA  128
#define XUDP_TOP_AXILITES_ADDR_REG_LISTEN_PORT_V_DATA 0x24
#define XUDP_TOP_AXILITES_BITS_REG_LISTEN_PORT_V_DATA 16

