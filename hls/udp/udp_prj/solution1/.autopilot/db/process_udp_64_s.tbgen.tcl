set moduleName process_udp_64_s
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {process_udp<64>}
set C_modelType { void 0 }
set C_modelArgList {
	{ input_V_data_V int 64 regular {axi_s 0 volatile  { s_axis_rx_data Data } }  }
	{ input_V_keep_V int 8 regular {axi_s 0 volatile  { s_axis_rx_data Keep } }  }
	{ input_V_last_V int 1 regular {axi_s 0 volatile  { s_axis_rx_data Last } }  }
	{ regListenPort_V int 16 regular {ap_stable 0} }
	{ rx_udp2shiftFifo_V_d int 64 regular {fifo 1 volatile } {global 1}  }
	{ rx_udp2shiftFifo_V_k int 8 regular {fifo 1 volatile } {global 1}  }
	{ rx_udp2shiftFifo_V_l int 1 regular {fifo 1 volatile } {global 1}  }
	{ rx_udpMetaFifo_V int 49 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "input_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "READONLY"} , 
 	{ "Name" : "input_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "input_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY"} , 
 	{ "Name" : "regListenPort_V", "interface" : "wire", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "rx_udp2shiftFifo_V_d", "interface" : "fifo", "bitwidth" : 64, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rx_udp2shiftFifo_V_k", "interface" : "fifo", "bitwidth" : 8, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rx_udp2shiftFifo_V_l", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rx_udpMetaFifo_V", "interface" : "fifo", "bitwidth" : 49, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 25
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ s_axis_rx_data_TVALID sc_in sc_logic 1 invld 0 } 
	{ rx_udpMetaFifo_V_din sc_out sc_lv 49 signal 7 } 
	{ rx_udpMetaFifo_V_full_n sc_in sc_logic 1 signal 7 } 
	{ rx_udpMetaFifo_V_write sc_out sc_logic 1 signal 7 } 
	{ rx_udp2shiftFifo_V_d_din sc_out sc_lv 64 signal 4 } 
	{ rx_udp2shiftFifo_V_d_full_n sc_in sc_logic 1 signal 4 } 
	{ rx_udp2shiftFifo_V_d_write sc_out sc_logic 1 signal 4 } 
	{ rx_udp2shiftFifo_V_k_din sc_out sc_lv 8 signal 5 } 
	{ rx_udp2shiftFifo_V_k_full_n sc_in sc_logic 1 signal 5 } 
	{ rx_udp2shiftFifo_V_k_write sc_out sc_logic 1 signal 5 } 
	{ rx_udp2shiftFifo_V_l_din sc_out sc_lv 1 signal 6 } 
	{ rx_udp2shiftFifo_V_l_full_n sc_in sc_logic 1 signal 6 } 
	{ rx_udp2shiftFifo_V_l_write sc_out sc_logic 1 signal 6 } 
	{ s_axis_rx_data_TDATA sc_in sc_lv 64 signal 0 } 
	{ s_axis_rx_data_TREADY sc_out sc_logic 1 inacc 2 } 
	{ s_axis_rx_data_TKEEP sc_in sc_lv 8 signal 1 } 
	{ s_axis_rx_data_TLAST sc_in sc_lv 1 signal 2 } 
	{ regListenPort_V sc_in sc_lv 16 signal 3 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "s_axis_rx_data_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "input_V_data_V", "role": "TVALID" }} , 
 	{ "name": "rx_udpMetaFifo_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":49, "type": "signal", "bundle":{"name": "rx_udpMetaFifo_V", "role": "din" }} , 
 	{ "name": "rx_udpMetaFifo_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_udpMetaFifo_V", "role": "full_n" }} , 
 	{ "name": "rx_udpMetaFifo_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_udpMetaFifo_V", "role": "write" }} , 
 	{ "name": "rx_udp2shiftFifo_V_d_din", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "rx_udp2shiftFifo_V_d", "role": "din" }} , 
 	{ "name": "rx_udp2shiftFifo_V_d_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_udp2shiftFifo_V_d", "role": "full_n" }} , 
 	{ "name": "rx_udp2shiftFifo_V_d_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_udp2shiftFifo_V_d", "role": "write" }} , 
 	{ "name": "rx_udp2shiftFifo_V_k_din", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "rx_udp2shiftFifo_V_k", "role": "din" }} , 
 	{ "name": "rx_udp2shiftFifo_V_k_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_udp2shiftFifo_V_k", "role": "full_n" }} , 
 	{ "name": "rx_udp2shiftFifo_V_k_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_udp2shiftFifo_V_k", "role": "write" }} , 
 	{ "name": "rx_udp2shiftFifo_V_l_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_udp2shiftFifo_V_l", "role": "din" }} , 
 	{ "name": "rx_udp2shiftFifo_V_l_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_udp2shiftFifo_V_l", "role": "full_n" }} , 
 	{ "name": "rx_udp2shiftFifo_V_l_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_udp2shiftFifo_V_l", "role": "write" }} , 
 	{ "name": "s_axis_rx_data_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "input_V_data_V", "role": "TDATA" }} , 
 	{ "name": "s_axis_rx_data_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "input_V_last_V", "role": "TREADY" }} , 
 	{ "name": "s_axis_rx_data_TKEEP", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "input_V_keep_V", "role": "TKEEP" }} , 
 	{ "name": "s_axis_rx_data_TLAST", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "input_V_last_V", "role": "TLAST" }} , 
 	{ "name": "regListenPort_V", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "regListenPort_V", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "process_udp_64_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "4", "EstimateLatencyMin" : "4", "EstimateLatencyMax" : "4",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "input_V_data_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "s_axis_rx_data_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "input_V_keep_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "input_V_last_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "regListenPort_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "pu_header_ready", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "pu_header_idx", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "pu_header_header_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "metaWritten", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "rx_udp2shiftFifo_V_d", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rx_udp2shiftFifo_V_d_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rx_udp2shiftFifo_V_k", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rx_udp2shiftFifo_V_k_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rx_udp2shiftFifo_V_l", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rx_udp2shiftFifo_V_l_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rx_udpMetaFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rx_udpMetaFifo_V_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	process_udp_64_s {
		input_V_data_V {Type I LastRead 0 FirstWrite -1}
		input_V_keep_V {Type I LastRead 0 FirstWrite -1}
		input_V_last_V {Type I LastRead 0 FirstWrite -1}
		regListenPort_V {Type I LastRead 0 FirstWrite -1}
		pu_header_ready {Type IO LastRead -1 FirstWrite -1}
		pu_header_idx {Type IO LastRead -1 FirstWrite -1}
		pu_header_header_V {Type IO LastRead -1 FirstWrite -1}
		metaWritten {Type IO LastRead -1 FirstWrite -1}
		rx_udp2shiftFifo_V_d {Type O LastRead -1 FirstWrite 4}
		rx_udp2shiftFifo_V_k {Type O LastRead -1 FirstWrite 4}
		rx_udp2shiftFifo_V_l {Type O LastRead -1 FirstWrite 4}
		rx_udpMetaFifo_V {Type O LastRead -1 FirstWrite 3}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "4", "Max" : "4"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	input_V_data_V { axis {  { s_axis_rx_data_TVALID in_vld 0 1 }  { s_axis_rx_data_TDATA in_data 0 64 } } }
	input_V_keep_V { axis {  { s_axis_rx_data_TKEEP in_data 0 8 } } }
	input_V_last_V { axis {  { s_axis_rx_data_TREADY in_acc 1 1 }  { s_axis_rx_data_TLAST in_data 0 1 } } }
	regListenPort_V { ap_stable {  { regListenPort_V in_data 0 16 } } }
	rx_udp2shiftFifo_V_d { ap_fifo {  { rx_udp2shiftFifo_V_d_din fifo_data 1 64 }  { rx_udp2shiftFifo_V_d_full_n fifo_status 0 1 }  { rx_udp2shiftFifo_V_d_write fifo_update 1 1 } } }
	rx_udp2shiftFifo_V_k { ap_fifo {  { rx_udp2shiftFifo_V_k_din fifo_data 1 8 }  { rx_udp2shiftFifo_V_k_full_n fifo_status 0 1 }  { rx_udp2shiftFifo_V_k_write fifo_update 1 1 } } }
	rx_udp2shiftFifo_V_l { ap_fifo {  { rx_udp2shiftFifo_V_l_din fifo_data 1 1 }  { rx_udp2shiftFifo_V_l_full_n fifo_status 0 1 }  { rx_udp2shiftFifo_V_l_write fifo_update 1 1 } } }
	rx_udpMetaFifo_V { ap_fifo {  { rx_udpMetaFifo_V_din fifo_data 1 49 }  { rx_udpMetaFifo_V_full_n fifo_status 0 1 }  { rx_udpMetaFifo_V_write fifo_update 1 1 } } }
}
set moduleName process_udp_64_s
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {process_udp<64>}
set C_modelType { void 0 }
set C_modelArgList {
	{ input_V_data_V int 64 regular {axi_s 0 volatile  { s_axis_rx_data Data } }  }
	{ input_V_keep_V int 8 regular {axi_s 0 volatile  { s_axis_rx_data Keep } }  }
	{ input_V_last_V int 1 regular {axi_s 0 volatile  { s_axis_rx_data Last } }  }
	{ regListenPort_V int 16 regular {fifo 0}  }
	{ rx_udp2shiftFifo_V_d int 64 regular {fifo 1 volatile } {global 1}  }
	{ rx_udp2shiftFifo_V_k int 8 regular {fifo 1 volatile } {global 1}  }
	{ rx_udp2shiftFifo_V_l int 1 regular {fifo 1 volatile } {global 1}  }
	{ rx_udpMetaFifo_V int 49 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "input_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "READONLY"} , 
 	{ "Name" : "input_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "input_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY"} , 
 	{ "Name" : "regListenPort_V", "interface" : "fifo", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "rx_udp2shiftFifo_V_d", "interface" : "fifo", "bitwidth" : 64, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rx_udp2shiftFifo_V_k", "interface" : "fifo", "bitwidth" : 8, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rx_udp2shiftFifo_V_l", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rx_udpMetaFifo_V", "interface" : "fifo", "bitwidth" : 49, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 27
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ s_axis_rx_data_TVALID sc_in sc_logic 1 invld 0 } 
	{ regListenPort_V_dout sc_in sc_lv 16 signal 3 } 
	{ regListenPort_V_empty_n sc_in sc_logic 1 signal 3 } 
	{ regListenPort_V_read sc_out sc_logic 1 signal 3 } 
	{ rx_udpMetaFifo_V_din sc_out sc_lv 49 signal 7 } 
	{ rx_udpMetaFifo_V_full_n sc_in sc_logic 1 signal 7 } 
	{ rx_udpMetaFifo_V_write sc_out sc_logic 1 signal 7 } 
	{ rx_udp2shiftFifo_V_d_din sc_out sc_lv 64 signal 4 } 
	{ rx_udp2shiftFifo_V_d_full_n sc_in sc_logic 1 signal 4 } 
	{ rx_udp2shiftFifo_V_d_write sc_out sc_logic 1 signal 4 } 
	{ rx_udp2shiftFifo_V_k_din sc_out sc_lv 8 signal 5 } 
	{ rx_udp2shiftFifo_V_k_full_n sc_in sc_logic 1 signal 5 } 
	{ rx_udp2shiftFifo_V_k_write sc_out sc_logic 1 signal 5 } 
	{ rx_udp2shiftFifo_V_l_din sc_out sc_lv 1 signal 6 } 
	{ rx_udp2shiftFifo_V_l_full_n sc_in sc_logic 1 signal 6 } 
	{ rx_udp2shiftFifo_V_l_write sc_out sc_logic 1 signal 6 } 
	{ s_axis_rx_data_TDATA sc_in sc_lv 64 signal 0 } 
	{ s_axis_rx_data_TREADY sc_out sc_logic 1 inacc 2 } 
	{ s_axis_rx_data_TKEEP sc_in sc_lv 8 signal 1 } 
	{ s_axis_rx_data_TLAST sc_in sc_lv 1 signal 2 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "s_axis_rx_data_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "input_V_data_V", "role": "TVALID" }} , 
 	{ "name": "regListenPort_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "regListenPort_V", "role": "dout" }} , 
 	{ "name": "regListenPort_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regListenPort_V", "role": "empty_n" }} , 
 	{ "name": "regListenPort_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regListenPort_V", "role": "read" }} , 
 	{ "name": "rx_udpMetaFifo_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":49, "type": "signal", "bundle":{"name": "rx_udpMetaFifo_V", "role": "din" }} , 
 	{ "name": "rx_udpMetaFifo_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_udpMetaFifo_V", "role": "full_n" }} , 
 	{ "name": "rx_udpMetaFifo_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_udpMetaFifo_V", "role": "write" }} , 
 	{ "name": "rx_udp2shiftFifo_V_d_din", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "rx_udp2shiftFifo_V_d", "role": "din" }} , 
 	{ "name": "rx_udp2shiftFifo_V_d_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_udp2shiftFifo_V_d", "role": "full_n" }} , 
 	{ "name": "rx_udp2shiftFifo_V_d_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_udp2shiftFifo_V_d", "role": "write" }} , 
 	{ "name": "rx_udp2shiftFifo_V_k_din", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "rx_udp2shiftFifo_V_k", "role": "din" }} , 
 	{ "name": "rx_udp2shiftFifo_V_k_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_udp2shiftFifo_V_k", "role": "full_n" }} , 
 	{ "name": "rx_udp2shiftFifo_V_k_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_udp2shiftFifo_V_k", "role": "write" }} , 
 	{ "name": "rx_udp2shiftFifo_V_l_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_udp2shiftFifo_V_l", "role": "din" }} , 
 	{ "name": "rx_udp2shiftFifo_V_l_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_udp2shiftFifo_V_l", "role": "full_n" }} , 
 	{ "name": "rx_udp2shiftFifo_V_l_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_udp2shiftFifo_V_l", "role": "write" }} , 
 	{ "name": "s_axis_rx_data_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "input_V_data_V", "role": "TDATA" }} , 
 	{ "name": "s_axis_rx_data_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "input_V_last_V", "role": "TREADY" }} , 
 	{ "name": "s_axis_rx_data_TKEEP", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "input_V_keep_V", "role": "TKEEP" }} , 
 	{ "name": "s_axis_rx_data_TLAST", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "input_V_last_V", "role": "TLAST" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "process_udp_64_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "4", "EstimateLatencyMin" : "4", "EstimateLatencyMax" : "4",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "input_V_data_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "s_axis_rx_data_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "input_V_keep_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "input_V_last_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "regListenPort_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regListenPort_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "pu_header_ready", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "pu_header_idx", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "pu_header_header_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "metaWritten", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "rx_udp2shiftFifo_V_d", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rx_udp2shiftFifo_V_d_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rx_udp2shiftFifo_V_k", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rx_udp2shiftFifo_V_k_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rx_udp2shiftFifo_V_l", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rx_udp2shiftFifo_V_l_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rx_udpMetaFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rx_udpMetaFifo_V_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	process_udp_64_s {
		input_V_data_V {Type I LastRead 0 FirstWrite -1}
		input_V_keep_V {Type I LastRead 0 FirstWrite -1}
		input_V_last_V {Type I LastRead 0 FirstWrite -1}
		regListenPort_V {Type I LastRead 2 FirstWrite -1}
		pu_header_ready {Type IO LastRead -1 FirstWrite -1}
		pu_header_idx {Type IO LastRead -1 FirstWrite -1}
		pu_header_header_V {Type IO LastRead -1 FirstWrite -1}
		metaWritten {Type IO LastRead -1 FirstWrite -1}
		rx_udp2shiftFifo_V_d {Type O LastRead -1 FirstWrite 4}
		rx_udp2shiftFifo_V_k {Type O LastRead -1 FirstWrite 4}
		rx_udp2shiftFifo_V_l {Type O LastRead -1 FirstWrite 4}
		rx_udpMetaFifo_V {Type O LastRead -1 FirstWrite 3}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "4", "Max" : "4"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	input_V_data_V { axis {  { s_axis_rx_data_TVALID in_vld 0 1 }  { s_axis_rx_data_TDATA in_data 0 64 } } }
	input_V_keep_V { axis {  { s_axis_rx_data_TKEEP in_data 0 8 } } }
	input_V_last_V { axis {  { s_axis_rx_data_TREADY in_acc 1 1 }  { s_axis_rx_data_TLAST in_data 0 1 } } }
	regListenPort_V { ap_fifo {  { regListenPort_V_dout fifo_data 0 16 }  { regListenPort_V_empty_n fifo_status 0 1 }  { regListenPort_V_read fifo_update 1 1 } } }
	rx_udp2shiftFifo_V_d { ap_fifo {  { rx_udp2shiftFifo_V_d_din fifo_data 1 64 }  { rx_udp2shiftFifo_V_d_full_n fifo_status 0 1 }  { rx_udp2shiftFifo_V_d_write fifo_update 1 1 } } }
	rx_udp2shiftFifo_V_k { ap_fifo {  { rx_udp2shiftFifo_V_k_din fifo_data 1 8 }  { rx_udp2shiftFifo_V_k_full_n fifo_status 0 1 }  { rx_udp2shiftFifo_V_k_write fifo_update 1 1 } } }
	rx_udp2shiftFifo_V_l { ap_fifo {  { rx_udp2shiftFifo_V_l_din fifo_data 1 1 }  { rx_udp2shiftFifo_V_l_full_n fifo_status 0 1 }  { rx_udp2shiftFifo_V_l_write fifo_update 1 1 } } }
	rx_udpMetaFifo_V { ap_fifo {  { rx_udpMetaFifo_V_din fifo_data 1 49 }  { rx_udpMetaFifo_V_full_n fifo_status 0 1 }  { rx_udpMetaFifo_V_write fifo_update 1 1 } } }
}
