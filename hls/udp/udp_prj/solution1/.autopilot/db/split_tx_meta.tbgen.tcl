set moduleName split_tx_meta
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {split_tx_meta}
set C_modelType { void 0 }
set C_modelArgList {
	{ metaIn_V int 176 regular {axi_s 0 volatile  { metaIn_V Data } }  }
	{ metaOut0_V int 48 regular {axi_s 1 volatile  { metaOut0_V Data } }  }
	{ tx_udpMetaFifo_V_the int 16 regular {fifo 1 volatile } {global 1}  }
	{ tx_udpMetaFifo_V_my_s int 16 regular {fifo 1 volatile } {global 1}  }
	{ tx_udpMetaFifo_V_len int 16 regular {fifo 1 volatile } {global 1}  }
	{ tx_udpMetaFifo_V_val int 1 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "metaIn_V", "interface" : "axis", "bitwidth" : 176, "direction" : "READONLY"} , 
 	{ "Name" : "metaOut0_V", "interface" : "axis", "bitwidth" : 48, "direction" : "WRITEONLY"} , 
 	{ "Name" : "tx_udpMetaFifo_V_the", "interface" : "fifo", "bitwidth" : 16, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "tx_udpMetaFifo_V_my_s", "interface" : "fifo", "bitwidth" : 16, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "tx_udpMetaFifo_V_len", "interface" : "fifo", "bitwidth" : 16, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "tx_udpMetaFifo_V_val", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 25
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ metaIn_V_TVALID sc_in sc_logic 1 invld 0 } 
	{ tx_udpMetaFifo_V_the_din sc_out sc_lv 16 signal 2 } 
	{ tx_udpMetaFifo_V_the_full_n sc_in sc_logic 1 signal 2 } 
	{ tx_udpMetaFifo_V_the_write sc_out sc_logic 1 signal 2 } 
	{ tx_udpMetaFifo_V_my_s_din sc_out sc_lv 16 signal 3 } 
	{ tx_udpMetaFifo_V_my_s_full_n sc_in sc_logic 1 signal 3 } 
	{ tx_udpMetaFifo_V_my_s_write sc_out sc_logic 1 signal 3 } 
	{ tx_udpMetaFifo_V_len_din sc_out sc_lv 16 signal 4 } 
	{ tx_udpMetaFifo_V_len_full_n sc_in sc_logic 1 signal 4 } 
	{ tx_udpMetaFifo_V_len_write sc_out sc_logic 1 signal 4 } 
	{ tx_udpMetaFifo_V_val_din sc_out sc_lv 1 signal 5 } 
	{ tx_udpMetaFifo_V_val_full_n sc_in sc_logic 1 signal 5 } 
	{ tx_udpMetaFifo_V_val_write sc_out sc_logic 1 signal 5 } 
	{ metaOut0_V_TREADY sc_in sc_logic 1 outacc 1 } 
	{ metaIn_V_TDATA sc_in sc_lv 176 signal 0 } 
	{ metaIn_V_TREADY sc_out sc_logic 1 inacc 0 } 
	{ metaOut0_V_TDATA sc_out sc_lv 48 signal 1 } 
	{ metaOut0_V_TVALID sc_out sc_logic 1 outvld 1 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "metaIn_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "metaIn_V", "role": "TVALID" }} , 
 	{ "name": "tx_udpMetaFifo_V_the_din", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "tx_udpMetaFifo_V_the", "role": "din" }} , 
 	{ "name": "tx_udpMetaFifo_V_the_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tx_udpMetaFifo_V_the", "role": "full_n" }} , 
 	{ "name": "tx_udpMetaFifo_V_the_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tx_udpMetaFifo_V_the", "role": "write" }} , 
 	{ "name": "tx_udpMetaFifo_V_my_s_din", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "tx_udpMetaFifo_V_my_s", "role": "din" }} , 
 	{ "name": "tx_udpMetaFifo_V_my_s_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tx_udpMetaFifo_V_my_s", "role": "full_n" }} , 
 	{ "name": "tx_udpMetaFifo_V_my_s_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tx_udpMetaFifo_V_my_s", "role": "write" }} , 
 	{ "name": "tx_udpMetaFifo_V_len_din", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "tx_udpMetaFifo_V_len", "role": "din" }} , 
 	{ "name": "tx_udpMetaFifo_V_len_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tx_udpMetaFifo_V_len", "role": "full_n" }} , 
 	{ "name": "tx_udpMetaFifo_V_len_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tx_udpMetaFifo_V_len", "role": "write" }} , 
 	{ "name": "tx_udpMetaFifo_V_val_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "tx_udpMetaFifo_V_val", "role": "din" }} , 
 	{ "name": "tx_udpMetaFifo_V_val_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tx_udpMetaFifo_V_val", "role": "full_n" }} , 
 	{ "name": "tx_udpMetaFifo_V_val_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tx_udpMetaFifo_V_val", "role": "write" }} , 
 	{ "name": "metaOut0_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "metaOut0_V", "role": "TREADY" }} , 
 	{ "name": "metaIn_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":176, "type": "signal", "bundle":{"name": "metaIn_V", "role": "TDATA" }} , 
 	{ "name": "metaIn_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "metaIn_V", "role": "TREADY" }} , 
 	{ "name": "metaOut0_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":48, "type": "signal", "bundle":{"name": "metaOut0_V", "role": "TDATA" }} , 
 	{ "name": "metaOut0_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "metaOut0_V", "role": "TVALID" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1"],
		"CDFG" : "split_tx_meta",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "metaIn_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "metaIn_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "metaOut0_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "metaOut0_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tx_udpMetaFifo_V_the", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "tx_udpMetaFifo_V_the_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tx_udpMetaFifo_V_my_s", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "tx_udpMetaFifo_V_my_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tx_udpMetaFifo_V_len", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "tx_udpMetaFifo_V_len_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tx_udpMetaFifo_V_val", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "tx_udpMetaFifo_V_val_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_metaOut0_V_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	split_tx_meta {
		metaIn_V {Type I LastRead 0 FirstWrite -1}
		metaOut0_V {Type O LastRead -1 FirstWrite 1}
		tx_udpMetaFifo_V_the {Type O LastRead -1 FirstWrite 1}
		tx_udpMetaFifo_V_my_s {Type O LastRead -1 FirstWrite 1}
		tx_udpMetaFifo_V_len {Type O LastRead -1 FirstWrite 1}
		tx_udpMetaFifo_V_val {Type O LastRead -1 FirstWrite 1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "2", "Max" : "2"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	metaIn_V { axis {  { metaIn_V_TVALID in_vld 0 1 }  { metaIn_V_TDATA in_data 0 176 }  { metaIn_V_TREADY in_acc 1 1 } } }
	metaOut0_V { axis {  { metaOut0_V_TREADY out_acc 0 1 }  { metaOut0_V_TDATA out_data 1 48 }  { metaOut0_V_TVALID out_vld 1 1 } } }
	tx_udpMetaFifo_V_the { ap_fifo {  { tx_udpMetaFifo_V_the_din fifo_data 1 16 }  { tx_udpMetaFifo_V_the_full_n fifo_status 0 1 }  { tx_udpMetaFifo_V_the_write fifo_update 1 1 } } }
	tx_udpMetaFifo_V_my_s { ap_fifo {  { tx_udpMetaFifo_V_my_s_din fifo_data 1 16 }  { tx_udpMetaFifo_V_my_s_full_n fifo_status 0 1 }  { tx_udpMetaFifo_V_my_s_write fifo_update 1 1 } } }
	tx_udpMetaFifo_V_len { ap_fifo {  { tx_udpMetaFifo_V_len_din fifo_data 1 16 }  { tx_udpMetaFifo_V_len_full_n fifo_status 0 1 }  { tx_udpMetaFifo_V_len_write fifo_update 1 1 } } }
	tx_udpMetaFifo_V_val { ap_fifo {  { tx_udpMetaFifo_V_val_din fifo_data 1 1 }  { tx_udpMetaFifo_V_val_full_n fifo_status 0 1 }  { tx_udpMetaFifo_V_val_write fifo_update 1 1 } } }
}
set moduleName split_tx_meta
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {split_tx_meta}
set C_modelType { void 0 }
set C_modelArgList {
	{ metaIn_V int 176 regular {axi_s 0 volatile  { metaIn_V Data } }  }
	{ metaOut0_V int 48 regular {axi_s 1 volatile  { metaOut0_V Data } }  }
	{ tx_udpMetaFifo_V_the int 16 regular {fifo 1 volatile } {global 1}  }
	{ tx_udpMetaFifo_V_my_s int 16 regular {fifo 1 volatile } {global 1}  }
	{ tx_udpMetaFifo_V_len int 16 regular {fifo 1 volatile } {global 1}  }
	{ tx_udpMetaFifo_V_val int 1 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "metaIn_V", "interface" : "axis", "bitwidth" : 176, "direction" : "READONLY"} , 
 	{ "Name" : "metaOut0_V", "interface" : "axis", "bitwidth" : 48, "direction" : "WRITEONLY"} , 
 	{ "Name" : "tx_udpMetaFifo_V_the", "interface" : "fifo", "bitwidth" : 16, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "tx_udpMetaFifo_V_my_s", "interface" : "fifo", "bitwidth" : 16, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "tx_udpMetaFifo_V_len", "interface" : "fifo", "bitwidth" : 16, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "tx_udpMetaFifo_V_val", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 25
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ metaIn_V_TVALID sc_in sc_logic 1 invld 0 } 
	{ tx_udpMetaFifo_V_the_din sc_out sc_lv 16 signal 2 } 
	{ tx_udpMetaFifo_V_the_full_n sc_in sc_logic 1 signal 2 } 
	{ tx_udpMetaFifo_V_the_write sc_out sc_logic 1 signal 2 } 
	{ tx_udpMetaFifo_V_my_s_din sc_out sc_lv 16 signal 3 } 
	{ tx_udpMetaFifo_V_my_s_full_n sc_in sc_logic 1 signal 3 } 
	{ tx_udpMetaFifo_V_my_s_write sc_out sc_logic 1 signal 3 } 
	{ tx_udpMetaFifo_V_len_din sc_out sc_lv 16 signal 4 } 
	{ tx_udpMetaFifo_V_len_full_n sc_in sc_logic 1 signal 4 } 
	{ tx_udpMetaFifo_V_len_write sc_out sc_logic 1 signal 4 } 
	{ tx_udpMetaFifo_V_val_din sc_out sc_lv 1 signal 5 } 
	{ tx_udpMetaFifo_V_val_full_n sc_in sc_logic 1 signal 5 } 
	{ tx_udpMetaFifo_V_val_write sc_out sc_logic 1 signal 5 } 
	{ metaOut0_V_TREADY sc_in sc_logic 1 outacc 1 } 
	{ metaIn_V_TDATA sc_in sc_lv 176 signal 0 } 
	{ metaIn_V_TREADY sc_out sc_logic 1 inacc 0 } 
	{ metaOut0_V_TDATA sc_out sc_lv 48 signal 1 } 
	{ metaOut0_V_TVALID sc_out sc_logic 1 outvld 1 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "metaIn_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "metaIn_V", "role": "TVALID" }} , 
 	{ "name": "tx_udpMetaFifo_V_the_din", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "tx_udpMetaFifo_V_the", "role": "din" }} , 
 	{ "name": "tx_udpMetaFifo_V_the_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tx_udpMetaFifo_V_the", "role": "full_n" }} , 
 	{ "name": "tx_udpMetaFifo_V_the_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tx_udpMetaFifo_V_the", "role": "write" }} , 
 	{ "name": "tx_udpMetaFifo_V_my_s_din", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "tx_udpMetaFifo_V_my_s", "role": "din" }} , 
 	{ "name": "tx_udpMetaFifo_V_my_s_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tx_udpMetaFifo_V_my_s", "role": "full_n" }} , 
 	{ "name": "tx_udpMetaFifo_V_my_s_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tx_udpMetaFifo_V_my_s", "role": "write" }} , 
 	{ "name": "tx_udpMetaFifo_V_len_din", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "tx_udpMetaFifo_V_len", "role": "din" }} , 
 	{ "name": "tx_udpMetaFifo_V_len_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tx_udpMetaFifo_V_len", "role": "full_n" }} , 
 	{ "name": "tx_udpMetaFifo_V_len_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tx_udpMetaFifo_V_len", "role": "write" }} , 
 	{ "name": "tx_udpMetaFifo_V_val_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "tx_udpMetaFifo_V_val", "role": "din" }} , 
 	{ "name": "tx_udpMetaFifo_V_val_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tx_udpMetaFifo_V_val", "role": "full_n" }} , 
 	{ "name": "tx_udpMetaFifo_V_val_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tx_udpMetaFifo_V_val", "role": "write" }} , 
 	{ "name": "metaOut0_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "metaOut0_V", "role": "TREADY" }} , 
 	{ "name": "metaIn_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":176, "type": "signal", "bundle":{"name": "metaIn_V", "role": "TDATA" }} , 
 	{ "name": "metaIn_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "metaIn_V", "role": "TREADY" }} , 
 	{ "name": "metaOut0_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":48, "type": "signal", "bundle":{"name": "metaOut0_V", "role": "TDATA" }} , 
 	{ "name": "metaOut0_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "metaOut0_V", "role": "TVALID" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "split_tx_meta",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "metaIn_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "metaIn_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "metaOut0_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "metaOut0_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tx_udpMetaFifo_V_the", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "tx_udpMetaFifo_V_the_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tx_udpMetaFifo_V_my_s", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "tx_udpMetaFifo_V_my_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tx_udpMetaFifo_V_len", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "tx_udpMetaFifo_V_len_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tx_udpMetaFifo_V_val", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "tx_udpMetaFifo_V_val_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	split_tx_meta {
		metaIn_V {Type I LastRead 0 FirstWrite -1}
		metaOut0_V {Type O LastRead -1 FirstWrite 1}
		tx_udpMetaFifo_V_the {Type O LastRead -1 FirstWrite 1}
		tx_udpMetaFifo_V_my_s {Type O LastRead -1 FirstWrite 1}
		tx_udpMetaFifo_V_len {Type O LastRead -1 FirstWrite 1}
		tx_udpMetaFifo_V_val {Type O LastRead -1 FirstWrite 1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "2", "Max" : "2"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	metaIn_V { axis {  { metaIn_V_TVALID in_vld 0 1 }  { metaIn_V_TDATA in_data 0 176 }  { metaIn_V_TREADY in_acc 1 1 } } }
	metaOut0_V { axis {  { metaOut0_V_TREADY out_acc 0 1 }  { metaOut0_V_TDATA out_data 1 48 }  { metaOut0_V_TVALID out_vld 1 1 } } }
	tx_udpMetaFifo_V_the { ap_fifo {  { tx_udpMetaFifo_V_the_din fifo_data 1 16 }  { tx_udpMetaFifo_V_the_full_n fifo_status 0 1 }  { tx_udpMetaFifo_V_the_write fifo_update 1 1 } } }
	tx_udpMetaFifo_V_my_s { ap_fifo {  { tx_udpMetaFifo_V_my_s_din fifo_data 1 16 }  { tx_udpMetaFifo_V_my_s_full_n fifo_status 0 1 }  { tx_udpMetaFifo_V_my_s_write fifo_update 1 1 } } }
	tx_udpMetaFifo_V_len { ap_fifo {  { tx_udpMetaFifo_V_len_din fifo_data 1 16 }  { tx_udpMetaFifo_V_len_full_n fifo_status 0 1 }  { tx_udpMetaFifo_V_len_write fifo_update 1 1 } } }
	tx_udpMetaFifo_V_val { ap_fifo {  { tx_udpMetaFifo_V_val_din fifo_data 1 1 }  { tx_udpMetaFifo_V_val_full_n fifo_status 0 1 }  { tx_udpMetaFifo_V_val_write fifo_update 1 1 } } }
}
