# This script segment is generated automatically by AutoPilot

set axilite_register_dict [dict create]
set port_AXILiteS {
reg_ip_address_V { 
	dir I
	width 128
	depth 1
	mode ap_none
	offset 16
	offset_end 35
}
reg_listen_port_V { 
	dir I
	width 16
	depth 1
	mode ap_none
	offset 36
	offset_end 43
}
}
dict set axilite_register_dict AXILiteS $port_AXILiteS


