set moduleName udp_top
set isTopModule 1
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type dataflow
set FunctionProtocol ap_ctrl_none
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {udp_top}
set C_modelType { void 0 }
set C_modelArgList {
	{ s_axis_rx_meta_V int 48 regular {axi_s 0 volatile  { s_axis_rx_meta_V Data } }  }
	{ s_axis_rx_data_V_data_V int 64 regular {axi_s 0 volatile  { s_axis_rx_data Data } }  }
	{ s_axis_rx_data_V_keep_V int 8 regular {axi_s 0 volatile  { s_axis_rx_data Keep } }  }
	{ s_axis_rx_data_V_last_V int 1 regular {axi_s 0 volatile  { s_axis_rx_data Last } }  }
	{ m_axis_rx_meta_V int 176 regular {axi_s 1 volatile  { m_axis_rx_meta_V Data } }  }
	{ m_axis_rx_data_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_rx_data Data } }  }
	{ m_axis_rx_data_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_rx_data Keep } }  }
	{ m_axis_rx_data_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_rx_data Last } }  }
	{ s_axis_tx_meta_V int 176 regular {axi_s 0 volatile  { s_axis_tx_meta_V Data } }  }
	{ s_axis_tx_data_V_data_V int 64 regular {axi_s 0 volatile  { s_axis_tx_data Data } }  }
	{ s_axis_tx_data_V_keep_V int 8 regular {axi_s 0 volatile  { s_axis_tx_data Keep } }  }
	{ s_axis_tx_data_V_last_V int 1 regular {axi_s 0 volatile  { s_axis_tx_data Last } }  }
	{ m_axis_tx_meta_V int 48 regular {axi_s 1 volatile  { m_axis_tx_meta_V Data } }  }
	{ m_axis_tx_data_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_tx_data Data } }  }
	{ m_axis_tx_data_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_tx_data Keep } }  }
	{ m_axis_tx_data_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_tx_data Last } }  }
	{ reg_ip_address_V int 128 unused {ap_stable 0} }
	{ reg_listen_port_V int 16 regular {ap_stable 0} }
}
set C_modelArgMapList {[ 
	{ "Name" : "s_axis_rx_meta_V", "interface" : "axis", "bitwidth" : 48, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "s_axis_rx_meta.V.their_address.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":32,"up":47,"cElement": [{"cName": "s_axis_rx_meta.V.length.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_rx_data_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "READONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "s_axis_rx_data.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_rx_data_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "s_axis_rx_data.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_rx_data_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "s_axis_rx_data.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_rx_meta_V", "interface" : "axis", "bitwidth" : 176, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":127,"cElement": [{"cName": "m_axis_rx_meta.V.their_address.V","cData": "uint128","bit_use": { "low": 0,"up": 127},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":128,"up":143,"cElement": [{"cName": "m_axis_rx_meta.V.their_port.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":144,"up":159,"cElement": [{"cName": "m_axis_rx_meta.V.my_port.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":160,"up":175,"cElement": [{"cName": "m_axis_rx_meta.V.length.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_rx_data_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "m_axis_rx_data.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_rx_data_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "m_axis_rx_data.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_rx_data_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "m_axis_rx_data.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_tx_meta_V", "interface" : "axis", "bitwidth" : 176, "direction" : "READONLY", "bitSlice":[{"low":0,"up":127,"cElement": [{"cName": "s_axis_tx_meta.V.their_address.V","cData": "uint128","bit_use": { "low": 0,"up": 127},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":128,"up":143,"cElement": [{"cName": "s_axis_tx_meta.V.their_port.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":144,"up":159,"cElement": [{"cName": "s_axis_tx_meta.V.my_port.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":160,"up":175,"cElement": [{"cName": "s_axis_tx_meta.V.length.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_tx_data_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "READONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "s_axis_tx_data.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_tx_data_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "s_axis_tx_data.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_tx_data_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "s_axis_tx_data.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_tx_meta_V", "interface" : "axis", "bitwidth" : 48, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "m_axis_tx_meta.V.their_address.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":32,"up":47,"cElement": [{"cName": "m_axis_tx_meta.V.length.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_tx_data_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "m_axis_tx_data.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_tx_data_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "m_axis_tx_data.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_tx_data_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "m_axis_tx_data.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "reg_ip_address_V", "interface" : "wire", "bitwidth" : 128, "direction" : "READONLY", "bitSlice":[{"low":0,"up":127,"cElement": [{"cName": "reg_ip_address.V","cData": "uint128","bit_use": { "low": 0,"up": 127},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}]} , 
 	{ "Name" : "reg_listen_port_V", "interface" : "wire", "bitwidth" : 16, "direction" : "READONLY", "bitSlice":[{"low":0,"up":15,"cElement": [{"cName": "reg_listen_port.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}]} ]}
# RTL Port declarations: 
set portNum 36
set portList { 
	{ s_axis_rx_meta_V_TDATA sc_in sc_lv 48 signal 0 } 
	{ s_axis_rx_data_TDATA sc_in sc_lv 64 signal 1 } 
	{ s_axis_rx_data_TKEEP sc_in sc_lv 8 signal 2 } 
	{ s_axis_rx_data_TLAST sc_in sc_lv 1 signal 3 } 
	{ m_axis_rx_meta_V_TDATA sc_out sc_lv 176 signal 4 } 
	{ m_axis_rx_data_TDATA sc_out sc_lv 64 signal 5 } 
	{ m_axis_rx_data_TKEEP sc_out sc_lv 8 signal 6 } 
	{ m_axis_rx_data_TLAST sc_out sc_lv 1 signal 7 } 
	{ s_axis_tx_meta_V_TDATA sc_in sc_lv 176 signal 8 } 
	{ s_axis_tx_data_TDATA sc_in sc_lv 64 signal 9 } 
	{ s_axis_tx_data_TKEEP sc_in sc_lv 8 signal 10 } 
	{ s_axis_tx_data_TLAST sc_in sc_lv 1 signal 11 } 
	{ m_axis_tx_meta_V_TDATA sc_out sc_lv 48 signal 12 } 
	{ m_axis_tx_data_TDATA sc_out sc_lv 64 signal 13 } 
	{ m_axis_tx_data_TKEEP sc_out sc_lv 8 signal 14 } 
	{ m_axis_tx_data_TLAST sc_out sc_lv 1 signal 15 } 
	{ reg_ip_address_V sc_in sc_lv 128 signal 16 } 
	{ reg_listen_port_V sc_in sc_lv 16 signal 17 } 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst_n sc_in sc_logic 1 reset -1 active_low_sync } 
	{ s_axis_rx_data_TVALID sc_in sc_logic 1 invld 3 } 
	{ s_axis_rx_data_TREADY sc_out sc_logic 1 inacc 3 } 
	{ m_axis_rx_data_TVALID sc_out sc_logic 1 outvld 7 } 
	{ m_axis_rx_data_TREADY sc_in sc_logic 1 outacc 7 } 
	{ s_axis_rx_meta_V_TVALID sc_in sc_logic 1 invld 0 } 
	{ s_axis_rx_meta_V_TREADY sc_out sc_logic 1 inacc 0 } 
	{ m_axis_rx_meta_V_TVALID sc_out sc_logic 1 outvld 4 } 
	{ m_axis_rx_meta_V_TREADY sc_in sc_logic 1 outacc 4 } 
	{ s_axis_tx_meta_V_TVALID sc_in sc_logic 1 invld 8 } 
	{ s_axis_tx_meta_V_TREADY sc_out sc_logic 1 inacc 8 } 
	{ m_axis_tx_meta_V_TVALID sc_out sc_logic 1 outvld 12 } 
	{ m_axis_tx_meta_V_TREADY sc_in sc_logic 1 outacc 12 } 
	{ s_axis_tx_data_TVALID sc_in sc_logic 1 invld 11 } 
	{ s_axis_tx_data_TREADY sc_out sc_logic 1 inacc 11 } 
	{ m_axis_tx_data_TVALID sc_out sc_logic 1 outvld 15 } 
	{ m_axis_tx_data_TREADY sc_in sc_logic 1 outacc 15 } 
}
set NewPortList {[ 
	{ "name": "s_axis_rx_meta_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":48, "type": "signal", "bundle":{"name": "s_axis_rx_meta_V", "role": "TDATA" }} , 
 	{ "name": "s_axis_rx_data_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "s_axis_rx_data_V_data_V", "role": "default" }} , 
 	{ "name": "s_axis_rx_data_TKEEP", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "s_axis_rx_data_V_keep_V", "role": "default" }} , 
 	{ "name": "s_axis_rx_data_TLAST", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "s_axis_rx_data_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_rx_meta_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":176, "type": "signal", "bundle":{"name": "m_axis_rx_meta_V", "role": "TDATA" }} , 
 	{ "name": "m_axis_rx_data_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "m_axis_rx_data_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_rx_data_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "m_axis_rx_data_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_rx_data_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "m_axis_rx_data_V_last_V", "role": "default" }} , 
 	{ "name": "s_axis_tx_meta_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":176, "type": "signal", "bundle":{"name": "s_axis_tx_meta_V", "role": "TDATA" }} , 
 	{ "name": "s_axis_tx_data_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "s_axis_tx_data_V_data_V", "role": "default" }} , 
 	{ "name": "s_axis_tx_data_TKEEP", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "s_axis_tx_data_V_keep_V", "role": "default" }} , 
 	{ "name": "s_axis_tx_data_TLAST", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "s_axis_tx_data_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_tx_meta_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":48, "type": "signal", "bundle":{"name": "m_axis_tx_meta_V", "role": "TDATA" }} , 
 	{ "name": "m_axis_tx_data_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "m_axis_tx_data_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_tx_data_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "m_axis_tx_data_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_tx_data_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "m_axis_tx_data_V_last_V", "role": "default" }} , 
 	{ "name": "reg_ip_address_V", "direction": "in", "datatype": "sc_lv", "bitwidth":128, "type": "signal", "bundle":{"name": "reg_ip_address_V", "role": "default" }} , 
 	{ "name": "reg_listen_port_V", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "reg_listen_port_V", "role": "default" }} , 
 	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst_n", "role": "default" }} , 
 	{ "name": "s_axis_rx_data_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "s_axis_rx_data_V_last_V", "role": "default" }} , 
 	{ "name": "s_axis_rx_data_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "s_axis_rx_data_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_rx_data_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_rx_data_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_rx_data_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_rx_data_V_last_V", "role": "default" }} , 
 	{ "name": "s_axis_rx_meta_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "s_axis_rx_meta_V", "role": "TVALID" }} , 
 	{ "name": "s_axis_rx_meta_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "s_axis_rx_meta_V", "role": "TREADY" }} , 
 	{ "name": "m_axis_rx_meta_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_rx_meta_V", "role": "TVALID" }} , 
 	{ "name": "m_axis_rx_meta_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_rx_meta_V", "role": "TREADY" }} , 
 	{ "name": "s_axis_tx_meta_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "s_axis_tx_meta_V", "role": "TVALID" }} , 
 	{ "name": "s_axis_tx_meta_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "s_axis_tx_meta_V", "role": "TREADY" }} , 
 	{ "name": "m_axis_tx_meta_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_tx_meta_V", "role": "TVALID" }} , 
 	{ "name": "m_axis_tx_meta_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_tx_meta_V", "role": "TREADY" }} , 
 	{ "name": "s_axis_tx_data_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "s_axis_tx_data_V_last_V", "role": "default" }} , 
 	{ "name": "s_axis_tx_data_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "s_axis_tx_data_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_tx_data_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_tx_data_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_tx_data_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_tx_data_V_last_V", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2", "6", "8", "10", "11", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25"],
		"CDFG" : "udp_top",
		"Protocol" : "ap_ctrl_none",
		"ControlExist" : "0", "ap_start" : "0", "ap_ready" : "0", "ap_done" : "0", "ap_continue" : "0", "ap_idle" : "0",
		"Pipeline" : "Dataflow", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "1",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "7", "EstimateLatencyMax" : "7",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "1",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"InputProcess" : [
			{"ID" : "1", "Name" : "process_udp_64_U0"},
			{"ID" : "6", "Name" : "merge_rx_meta_U0"},
			{"ID" : "8", "Name" : "split_tx_meta_U0"},
			{"ID" : "10", "Name" : "lshiftWordByOctet_U0"}],
		"OutputProcess" : [
			{"ID" : "2", "Name" : "rshiftWordByOctet_U0"},
			{"ID" : "6", "Name" : "merge_rx_meta_U0"},
			{"ID" : "8", "Name" : "split_tx_meta_U0"},
			{"ID" : "11", "Name" : "generate_udp_64_U0"}],
		"Port" : [
			{"Name" : "s_axis_rx_meta_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "6", "SubInstance" : "merge_rx_meta_U0", "Port" : "ipMetaIn_V"}]},
			{"Name" : "s_axis_rx_data_V_data_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "process_udp_64_U0", "Port" : "input_V_data_V"}]},
			{"Name" : "s_axis_rx_data_V_keep_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "process_udp_64_U0", "Port" : "input_V_keep_V"}]},
			{"Name" : "s_axis_rx_data_V_last_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "process_udp_64_U0", "Port" : "input_V_last_V"}]},
			{"Name" : "m_axis_rx_meta_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "6", "SubInstance" : "merge_rx_meta_U0", "Port" : "metaOut_V"}]},
			{"Name" : "m_axis_rx_data_V_data_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "rshiftWordByOctet_U0", "Port" : "output_V_data_V"}]},
			{"Name" : "m_axis_rx_data_V_keep_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "rshiftWordByOctet_U0", "Port" : "output_V_keep_V"}]},
			{"Name" : "m_axis_rx_data_V_last_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "rshiftWordByOctet_U0", "Port" : "output_V_last_V"}]},
			{"Name" : "s_axis_tx_meta_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "split_tx_meta_U0", "Port" : "metaIn_V"}]},
			{"Name" : "s_axis_tx_data_V_data_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "10", "SubInstance" : "lshiftWordByOctet_U0", "Port" : "input_V_data_V"}]},
			{"Name" : "s_axis_tx_data_V_keep_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "10", "SubInstance" : "lshiftWordByOctet_U0", "Port" : "input_V_keep_V"}]},
			{"Name" : "s_axis_tx_data_V_last_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "10", "SubInstance" : "lshiftWordByOctet_U0", "Port" : "input_V_last_V"}]},
			{"Name" : "m_axis_tx_meta_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "split_tx_meta_U0", "Port" : "metaOut0_V"}]},
			{"Name" : "m_axis_tx_data_V_data_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "11", "SubInstance" : "generate_udp_64_U0", "Port" : "output_V_data_V"}]},
			{"Name" : "m_axis_tx_data_V_keep_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "11", "SubInstance" : "generate_udp_64_U0", "Port" : "output_V_keep_V"}]},
			{"Name" : "m_axis_tx_data_V_last_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "11", "SubInstance" : "generate_udp_64_U0", "Port" : "output_V_last_V"}]},
			{"Name" : "reg_ip_address_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "reg_listen_port_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "pu_header_ready", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "process_udp_64_U0", "Port" : "pu_header_ready"}]},
			{"Name" : "pu_header_idx", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "process_udp_64_U0", "Port" : "pu_header_idx"}]},
			{"Name" : "pu_header_header_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "process_udp_64_U0", "Port" : "pu_header_header_V"}]},
			{"Name" : "metaWritten", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "process_udp_64_U0", "Port" : "metaWritten"}]},
			{"Name" : "rx_udp2shiftFifo_V_d", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "rshiftWordByOctet_U0", "Port" : "rx_udp2shiftFifo_V_d"},
					{"ID" : "1", "SubInstance" : "process_udp_64_U0", "Port" : "rx_udp2shiftFifo_V_d"}]},
			{"Name" : "rx_udp2shiftFifo_V_k", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "rshiftWordByOctet_U0", "Port" : "rx_udp2shiftFifo_V_k"},
					{"ID" : "1", "SubInstance" : "process_udp_64_U0", "Port" : "rx_udp2shiftFifo_V_k"}]},
			{"Name" : "rx_udp2shiftFifo_V_l", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "rshiftWordByOctet_U0", "Port" : "rx_udp2shiftFifo_V_l"},
					{"ID" : "1", "SubInstance" : "process_udp_64_U0", "Port" : "rx_udp2shiftFifo_V_l"}]},
			{"Name" : "rx_udpMetaFifo_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "6", "SubInstance" : "merge_rx_meta_U0", "Port" : "rx_udpMetaFifo_V"},
					{"ID" : "1", "SubInstance" : "process_udp_64_U0", "Port" : "rx_udpMetaFifo_V"}]},
			{"Name" : "tx_udpMetaFifo_V_the", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "split_tx_meta_U0", "Port" : "tx_udpMetaFifo_V_the"},
					{"ID" : "11", "SubInstance" : "generate_udp_64_U0", "Port" : "tx_udpMetaFifo_V_the"}]},
			{"Name" : "tx_udpMetaFifo_V_my_s", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "split_tx_meta_U0", "Port" : "tx_udpMetaFifo_V_my_s"},
					{"ID" : "11", "SubInstance" : "generate_udp_64_U0", "Port" : "tx_udpMetaFifo_V_my_s"}]},
			{"Name" : "tx_udpMetaFifo_V_len", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "split_tx_meta_U0", "Port" : "tx_udpMetaFifo_V_len"},
					{"ID" : "11", "SubInstance" : "generate_udp_64_U0", "Port" : "tx_udpMetaFifo_V_len"}]},
			{"Name" : "tx_udpMetaFifo_V_val", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "split_tx_meta_U0", "Port" : "tx_udpMetaFifo_V_val"},
					{"ID" : "11", "SubInstance" : "generate_udp_64_U0", "Port" : "tx_udpMetaFifo_V_val"}]},
			{"Name" : "tx_shift2udpFifo_V_d", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "10", "SubInstance" : "lshiftWordByOctet_U0", "Port" : "tx_shift2udpFifo_V_d"},
					{"ID" : "11", "SubInstance" : "generate_udp_64_U0", "Port" : "tx_shift2udpFifo_V_d"}]},
			{"Name" : "tx_shift2udpFifo_V_k", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "10", "SubInstance" : "lshiftWordByOctet_U0", "Port" : "tx_shift2udpFifo_V_k"},
					{"ID" : "11", "SubInstance" : "generate_udp_64_U0", "Port" : "tx_shift2udpFifo_V_k"}]},
			{"Name" : "tx_shift2udpFifo_V_l", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "10", "SubInstance" : "lshiftWordByOctet_U0", "Port" : "tx_shift2udpFifo_V_l"},
					{"ID" : "11", "SubInstance" : "generate_udp_64_U0", "Port" : "tx_shift2udpFifo_V_l"}]},
			{"Name" : "state", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "11", "SubInstance" : "generate_udp_64_U0", "Port" : "state"}]},
			{"Name" : "header_idx", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "11", "SubInstance" : "generate_udp_64_U0", "Port" : "header_idx"}]},
			{"Name" : "header_header_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "11", "SubInstance" : "generate_udp_64_U0", "Port" : "header_header_V"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.process_udp_64_U0", "Parent" : "0",
		"CDFG" : "process_udp_64_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "4", "EstimateLatencyMin" : "4", "EstimateLatencyMax" : "4",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "input_V_data_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "s_axis_rx_data_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "input_V_keep_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "input_V_last_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "regListenPort_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "pu_header_ready", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "pu_header_idx", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "pu_header_header_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "metaWritten", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "rx_udp2shiftFifo_V_d", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "2", "DependentChan" : "15",
				"BlockSignal" : [
					{"Name" : "rx_udp2shiftFifo_V_d_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rx_udp2shiftFifo_V_k", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "2", "DependentChan" : "16",
				"BlockSignal" : [
					{"Name" : "rx_udp2shiftFifo_V_k_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rx_udp2shiftFifo_V_l", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "2", "DependentChan" : "17",
				"BlockSignal" : [
					{"Name" : "rx_udp2shiftFifo_V_l_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rx_udpMetaFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "6", "DependentChan" : "18",
				"BlockSignal" : [
					{"Name" : "rx_udpMetaFifo_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.rshiftWordByOctet_U0", "Parent" : "0", "Child" : ["3", "4", "5"],
		"CDFG" : "rshiftWordByOctet",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "output_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_rx_data_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "output_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "output_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "rx_udp2shiftFifo_V_d", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "1", "DependentChan" : "15",
				"BlockSignal" : [
					{"Name" : "rx_udp2shiftFifo_V_d_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rx_udp2shiftFifo_V_k", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "1", "DependentChan" : "16",
				"BlockSignal" : [
					{"Name" : "rx_udp2shiftFifo_V_k_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rx_udp2shiftFifo_V_l", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "1", "DependentChan" : "17",
				"BlockSignal" : [
					{"Name" : "rx_udp2shiftFifo_V_l_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "3", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.rshiftWordByOctet_U0.regslice_both_output_V_data_V_U", "Parent" : "2"},
	{"ID" : "4", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.rshiftWordByOctet_U0.regslice_both_output_V_keep_V_U", "Parent" : "2"},
	{"ID" : "5", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.rshiftWordByOctet_U0.regslice_both_output_V_last_V_U", "Parent" : "2"},
	{"ID" : "6", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.merge_rx_meta_U0", "Parent" : "0", "Child" : ["7"],
		"CDFG" : "merge_rx_meta",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "ipMetaIn_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "ipMetaIn_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "metaOut_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "metaOut_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rx_udpMetaFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "1", "DependentChan" : "18",
				"BlockSignal" : [
					{"Name" : "rx_udpMetaFifo_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "7", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.merge_rx_meta_U0.regslice_both_metaOut_V_U", "Parent" : "6"},
	{"ID" : "8", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.split_tx_meta_U0", "Parent" : "0", "Child" : ["9"],
		"CDFG" : "split_tx_meta",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "metaIn_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "metaIn_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "metaOut0_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "metaOut0_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tx_udpMetaFifo_V_the", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "11", "DependentChan" : "19",
				"BlockSignal" : [
					{"Name" : "tx_udpMetaFifo_V_the_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tx_udpMetaFifo_V_my_s", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "11", "DependentChan" : "20",
				"BlockSignal" : [
					{"Name" : "tx_udpMetaFifo_V_my_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tx_udpMetaFifo_V_len", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "11", "DependentChan" : "21",
				"BlockSignal" : [
					{"Name" : "tx_udpMetaFifo_V_len_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tx_udpMetaFifo_V_val", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "11", "DependentChan" : "22",
				"BlockSignal" : [
					{"Name" : "tx_udpMetaFifo_V_val_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "9", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.split_tx_meta_U0.regslice_both_metaOut0_V_U", "Parent" : "8"},
	{"ID" : "10", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.lshiftWordByOctet_U0", "Parent" : "0",
		"CDFG" : "lshiftWordByOctet",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "input_V_data_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "s_axis_tx_data_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "input_V_keep_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "input_V_last_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "tx_shift2udpFifo_V_d", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "11", "DependentChan" : "23",
				"BlockSignal" : [
					{"Name" : "tx_shift2udpFifo_V_d_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tx_shift2udpFifo_V_k", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "11", "DependentChan" : "24",
				"BlockSignal" : [
					{"Name" : "tx_shift2udpFifo_V_k_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tx_shift2udpFifo_V_l", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "11", "DependentChan" : "25",
				"BlockSignal" : [
					{"Name" : "tx_shift2udpFifo_V_l_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "11", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.generate_udp_64_U0", "Parent" : "0", "Child" : ["12", "13", "14"],
		"CDFG" : "generate_udp_64_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "output_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_tx_data_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "output_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "output_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "state", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_idx", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tx_udpMetaFifo_V_the", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "8", "DependentChan" : "19",
				"BlockSignal" : [
					{"Name" : "tx_udpMetaFifo_V_the_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tx_udpMetaFifo_V_my_s", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "8", "DependentChan" : "20",
				"BlockSignal" : [
					{"Name" : "tx_udpMetaFifo_V_my_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tx_udpMetaFifo_V_len", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "8", "DependentChan" : "21",
				"BlockSignal" : [
					{"Name" : "tx_udpMetaFifo_V_len_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tx_udpMetaFifo_V_val", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "8", "DependentChan" : "22",
				"BlockSignal" : [
					{"Name" : "tx_udpMetaFifo_V_val_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "header_header_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tx_shift2udpFifo_V_d", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "10", "DependentChan" : "23",
				"BlockSignal" : [
					{"Name" : "tx_shift2udpFifo_V_d_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tx_shift2udpFifo_V_k", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "10", "DependentChan" : "24",
				"BlockSignal" : [
					{"Name" : "tx_shift2udpFifo_V_k_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tx_shift2udpFifo_V_l", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "10", "DependentChan" : "25",
				"BlockSignal" : [
					{"Name" : "tx_shift2udpFifo_V_l_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "12", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.generate_udp_64_U0.regslice_both_output_V_data_V_U", "Parent" : "11"},
	{"ID" : "13", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.generate_udp_64_U0.regslice_both_output_V_keep_V_U", "Parent" : "11"},
	{"ID" : "14", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.generate_udp_64_U0.regslice_both_output_V_last_V_U", "Parent" : "11"},
	{"ID" : "15", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.rx_udp2shiftFifo_V_d_U", "Parent" : "0"},
	{"ID" : "16", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.rx_udp2shiftFifo_V_k_U", "Parent" : "0"},
	{"ID" : "17", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.rx_udp2shiftFifo_V_l_U", "Parent" : "0"},
	{"ID" : "18", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.rx_udpMetaFifo_V_U", "Parent" : "0"},
	{"ID" : "19", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.tx_udpMetaFifo_V_the_U", "Parent" : "0"},
	{"ID" : "20", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.tx_udpMetaFifo_V_my_s_U", "Parent" : "0"},
	{"ID" : "21", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.tx_udpMetaFifo_V_len_U", "Parent" : "0"},
	{"ID" : "22", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.tx_udpMetaFifo_V_val_U", "Parent" : "0"},
	{"ID" : "23", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.tx_shift2udpFifo_V_d_U", "Parent" : "0"},
	{"ID" : "24", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.tx_shift2udpFifo_V_k_U", "Parent" : "0"},
	{"ID" : "25", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.tx_shift2udpFifo_V_l_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	udp_top {
		s_axis_rx_meta_V {Type I LastRead 0 FirstWrite -1}
		s_axis_rx_data_V_data_V {Type I LastRead 0 FirstWrite -1}
		s_axis_rx_data_V_keep_V {Type I LastRead 0 FirstWrite -1}
		s_axis_rx_data_V_last_V {Type I LastRead 0 FirstWrite -1}
		m_axis_rx_meta_V {Type O LastRead -1 FirstWrite 1}
		m_axis_rx_data_V_data_V {Type O LastRead -1 FirstWrite 1}
		m_axis_rx_data_V_keep_V {Type O LastRead -1 FirstWrite 1}
		m_axis_rx_data_V_last_V {Type O LastRead -1 FirstWrite 1}
		s_axis_tx_meta_V {Type I LastRead 0 FirstWrite -1}
		s_axis_tx_data_V_data_V {Type I LastRead 0 FirstWrite -1}
		s_axis_tx_data_V_keep_V {Type I LastRead 0 FirstWrite -1}
		s_axis_tx_data_V_last_V {Type I LastRead 0 FirstWrite -1}
		m_axis_tx_meta_V {Type O LastRead -1 FirstWrite 1}
		m_axis_tx_data_V_data_V {Type O LastRead -1 FirstWrite 1}
		m_axis_tx_data_V_keep_V {Type O LastRead -1 FirstWrite 1}
		m_axis_tx_data_V_last_V {Type O LastRead -1 FirstWrite 1}
		reg_ip_address_V {Type I LastRead -1 FirstWrite -1}
		reg_listen_port_V {Type I LastRead 0 FirstWrite -1}
		pu_header_ready {Type IO LastRead -1 FirstWrite -1}
		pu_header_idx {Type IO LastRead -1 FirstWrite -1}
		pu_header_header_V {Type IO LastRead -1 FirstWrite -1}
		metaWritten {Type IO LastRead -1 FirstWrite -1}
		rx_udp2shiftFifo_V_d {Type IO LastRead -1 FirstWrite -1}
		rx_udp2shiftFifo_V_k {Type IO LastRead -1 FirstWrite -1}
		rx_udp2shiftFifo_V_l {Type IO LastRead -1 FirstWrite -1}
		rx_udpMetaFifo_V {Type IO LastRead -1 FirstWrite -1}
		tx_udpMetaFifo_V_the {Type IO LastRead -1 FirstWrite -1}
		tx_udpMetaFifo_V_my_s {Type IO LastRead -1 FirstWrite -1}
		tx_udpMetaFifo_V_len {Type IO LastRead -1 FirstWrite -1}
		tx_udpMetaFifo_V_val {Type IO LastRead -1 FirstWrite -1}
		tx_shift2udpFifo_V_d {Type IO LastRead -1 FirstWrite -1}
		tx_shift2udpFifo_V_k {Type IO LastRead -1 FirstWrite -1}
		tx_shift2udpFifo_V_l {Type IO LastRead -1 FirstWrite -1}
		state {Type IO LastRead -1 FirstWrite -1}
		header_idx {Type IO LastRead -1 FirstWrite -1}
		header_header_V {Type IO LastRead -1 FirstWrite -1}}
	process_udp_64_s {
		input_V_data_V {Type I LastRead 0 FirstWrite -1}
		input_V_keep_V {Type I LastRead 0 FirstWrite -1}
		input_V_last_V {Type I LastRead 0 FirstWrite -1}
		regListenPort_V {Type I LastRead 0 FirstWrite -1}
		pu_header_ready {Type IO LastRead -1 FirstWrite -1}
		pu_header_idx {Type IO LastRead -1 FirstWrite -1}
		pu_header_header_V {Type IO LastRead -1 FirstWrite -1}
		metaWritten {Type IO LastRead -1 FirstWrite -1}
		rx_udp2shiftFifo_V_d {Type O LastRead -1 FirstWrite 4}
		rx_udp2shiftFifo_V_k {Type O LastRead -1 FirstWrite 4}
		rx_udp2shiftFifo_V_l {Type O LastRead -1 FirstWrite 4}
		rx_udpMetaFifo_V {Type O LastRead -1 FirstWrite 3}}
	rshiftWordByOctet {
		output_V_data_V {Type O LastRead -1 FirstWrite 1}
		output_V_keep_V {Type O LastRead -1 FirstWrite 1}
		output_V_last_V {Type O LastRead -1 FirstWrite 1}
		rx_udp2shiftFifo_V_d {Type I LastRead 0 FirstWrite -1}
		rx_udp2shiftFifo_V_k {Type I LastRead 0 FirstWrite -1}
		rx_udp2shiftFifo_V_l {Type I LastRead 0 FirstWrite -1}}
	merge_rx_meta {
		ipMetaIn_V {Type I LastRead 0 FirstWrite -1}
		metaOut_V {Type O LastRead -1 FirstWrite 1}
		rx_udpMetaFifo_V {Type I LastRead 0 FirstWrite -1}}
	split_tx_meta {
		metaIn_V {Type I LastRead 0 FirstWrite -1}
		metaOut0_V {Type O LastRead -1 FirstWrite 1}
		tx_udpMetaFifo_V_the {Type O LastRead -1 FirstWrite 1}
		tx_udpMetaFifo_V_my_s {Type O LastRead -1 FirstWrite 1}
		tx_udpMetaFifo_V_len {Type O LastRead -1 FirstWrite 1}
		tx_udpMetaFifo_V_val {Type O LastRead -1 FirstWrite 1}}
	lshiftWordByOctet {
		input_V_data_V {Type I LastRead 0 FirstWrite -1}
		input_V_keep_V {Type I LastRead 0 FirstWrite -1}
		input_V_last_V {Type I LastRead 0 FirstWrite -1}
		tx_shift2udpFifo_V_d {Type O LastRead -1 FirstWrite 1}
		tx_shift2udpFifo_V_k {Type O LastRead -1 FirstWrite 1}
		tx_shift2udpFifo_V_l {Type O LastRead -1 FirstWrite 1}}
	generate_udp_64_s {
		output_V_data_V {Type O LastRead -1 FirstWrite 1}
		output_V_keep_V {Type O LastRead -1 FirstWrite 1}
		output_V_last_V {Type O LastRead -1 FirstWrite 1}
		state {Type IO LastRead -1 FirstWrite -1}
		header_idx {Type IO LastRead -1 FirstWrite -1}
		tx_udpMetaFifo_V_the {Type I LastRead 0 FirstWrite -1}
		tx_udpMetaFifo_V_my_s {Type I LastRead 0 FirstWrite -1}
		tx_udpMetaFifo_V_len {Type I LastRead 0 FirstWrite -1}
		tx_udpMetaFifo_V_val {Type I LastRead 0 FirstWrite -1}
		header_header_V {Type IO LastRead -1 FirstWrite -1}
		tx_shift2udpFifo_V_d {Type I LastRead 0 FirstWrite -1}
		tx_shift2udpFifo_V_k {Type I LastRead 0 FirstWrite -1}
		tx_shift2udpFifo_V_l {Type I LastRead 0 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "7", "Max" : "7"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
]}

set Spec2ImplPortList { 
	s_axis_rx_meta_V { axis {  { s_axis_rx_meta_V_TDATA in_data 0 48 }  { s_axis_rx_meta_V_TVALID in_vld 0 1 }  { s_axis_rx_meta_V_TREADY in_acc 1 1 } } }
	s_axis_rx_data_V_data_V { axis {  { s_axis_rx_data_TDATA in_data 0 64 } } }
	s_axis_rx_data_V_keep_V { axis {  { s_axis_rx_data_TKEEP in_data 0 8 } } }
	s_axis_rx_data_V_last_V { axis {  { s_axis_rx_data_TLAST in_data 0 1 }  { s_axis_rx_data_TVALID in_vld 0 1 }  { s_axis_rx_data_TREADY in_acc 1 1 } } }
	m_axis_rx_meta_V { axis {  { m_axis_rx_meta_V_TDATA out_data 1 176 }  { m_axis_rx_meta_V_TVALID out_vld 1 1 }  { m_axis_rx_meta_V_TREADY out_acc 0 1 } } }
	m_axis_rx_data_V_data_V { axis {  { m_axis_rx_data_TDATA out_data 1 64 } } }
	m_axis_rx_data_V_keep_V { axis {  { m_axis_rx_data_TKEEP out_data 1 8 } } }
	m_axis_rx_data_V_last_V { axis {  { m_axis_rx_data_TLAST out_data 1 1 }  { m_axis_rx_data_TVALID out_vld 1 1 }  { m_axis_rx_data_TREADY out_acc 0 1 } } }
	s_axis_tx_meta_V { axis {  { s_axis_tx_meta_V_TDATA in_data 0 176 }  { s_axis_tx_meta_V_TVALID in_vld 0 1 }  { s_axis_tx_meta_V_TREADY in_acc 1 1 } } }
	s_axis_tx_data_V_data_V { axis {  { s_axis_tx_data_TDATA in_data 0 64 } } }
	s_axis_tx_data_V_keep_V { axis {  { s_axis_tx_data_TKEEP in_data 0 8 } } }
	s_axis_tx_data_V_last_V { axis {  { s_axis_tx_data_TLAST in_data 0 1 }  { s_axis_tx_data_TVALID in_vld 0 1 }  { s_axis_tx_data_TREADY in_acc 1 1 } } }
	m_axis_tx_meta_V { axis {  { m_axis_tx_meta_V_TDATA out_data 1 48 }  { m_axis_tx_meta_V_TVALID out_vld 1 1 }  { m_axis_tx_meta_V_TREADY out_acc 0 1 } } }
	m_axis_tx_data_V_data_V { axis {  { m_axis_tx_data_TDATA out_data 1 64 } } }
	m_axis_tx_data_V_keep_V { axis {  { m_axis_tx_data_TKEEP out_data 1 8 } } }
	m_axis_tx_data_V_last_V { axis {  { m_axis_tx_data_TLAST out_data 1 1 }  { m_axis_tx_data_TVALID out_vld 1 1 }  { m_axis_tx_data_TREADY out_acc 0 1 } } }
	reg_ip_address_V { ap_stable {  { reg_ip_address_V in_data 0 128 } } }
	reg_listen_port_V { ap_stable {  { reg_listen_port_V in_data 0 16 } } }
}

set busDeadlockParameterList { 
}

# RTL port scheduling information:
set fifoSchedulingInfoList { 
}

# RTL bus port read request latency information:
set busReadReqLatencyList { 
}

# RTL bus port write response latency information:
set busWriteResLatencyList { 
}

# RTL array port load latency information:
set memoryLoadLatencyList { 
}
set moduleName udp_top
set isTopModule 1
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type dataflow
set FunctionProtocol ap_ctrl_none
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {udp_top}
set C_modelType { void 0 }
set C_modelArgList {
	{ s_axis_rx_meta_V int 48 regular {axi_s 0 volatile  { s_axis_rx_meta_V Data } }  }
	{ s_axis_rx_data_V_data_V int 64 regular {axi_s 0 volatile  { s_axis_rx_data Data } }  }
	{ s_axis_rx_data_V_keep_V int 8 regular {axi_s 0 volatile  { s_axis_rx_data Keep } }  }
	{ s_axis_rx_data_V_last_V int 1 regular {axi_s 0 volatile  { s_axis_rx_data Last } }  }
	{ m_axis_rx_meta_V int 176 regular {axi_s 1 volatile  { m_axis_rx_meta_V Data } }  }
	{ m_axis_rx_data_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_rx_data Data } }  }
	{ m_axis_rx_data_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_rx_data Keep } }  }
	{ m_axis_rx_data_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_rx_data Last } }  }
	{ s_axis_tx_meta_V int 176 regular {axi_s 0 volatile  { s_axis_tx_meta_V Data } }  }
	{ s_axis_tx_data_V_data_V int 64 regular {axi_s 0 volatile  { s_axis_tx_data Data } }  }
	{ s_axis_tx_data_V_keep_V int 8 regular {axi_s 0 volatile  { s_axis_tx_data Keep } }  }
	{ s_axis_tx_data_V_last_V int 1 regular {axi_s 0 volatile  { s_axis_tx_data Last } }  }
	{ m_axis_tx_meta_V int 48 regular {axi_s 1 volatile  { m_axis_tx_meta_V Data } }  }
	{ m_axis_tx_data_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_tx_data Data } }  }
	{ m_axis_tx_data_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_tx_data Keep } }  }
	{ m_axis_tx_data_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_tx_data Last } }  }
	{ reg_ip_address_V int 128 unused {axi_slave 0}  }
	{ reg_listen_port_V int 16 regular {axi_slave 0}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "s_axis_rx_meta_V", "interface" : "axis", "bitwidth" : 48, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "s_axis_rx_meta.V.their_address.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":32,"up":47,"cElement": [{"cName": "s_axis_rx_meta.V.length.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_rx_data_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "READONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "s_axis_rx_data.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_rx_data_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "s_axis_rx_data.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_rx_data_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "s_axis_rx_data.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_rx_meta_V", "interface" : "axis", "bitwidth" : 176, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":127,"cElement": [{"cName": "m_axis_rx_meta.V.their_address.V","cData": "uint128","bit_use": { "low": 0,"up": 127},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":128,"up":143,"cElement": [{"cName": "m_axis_rx_meta.V.their_port.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":144,"up":159,"cElement": [{"cName": "m_axis_rx_meta.V.my_port.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":160,"up":175,"cElement": [{"cName": "m_axis_rx_meta.V.length.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_rx_data_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "m_axis_rx_data.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_rx_data_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "m_axis_rx_data.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_rx_data_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "m_axis_rx_data.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_tx_meta_V", "interface" : "axis", "bitwidth" : 176, "direction" : "READONLY", "bitSlice":[{"low":0,"up":127,"cElement": [{"cName": "s_axis_tx_meta.V.their_address.V","cData": "uint128","bit_use": { "low": 0,"up": 127},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":128,"up":143,"cElement": [{"cName": "s_axis_tx_meta.V.their_port.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":144,"up":159,"cElement": [{"cName": "s_axis_tx_meta.V.my_port.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":160,"up":175,"cElement": [{"cName": "s_axis_tx_meta.V.length.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_tx_data_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "READONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "s_axis_tx_data.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_tx_data_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "s_axis_tx_data.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_tx_data_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "s_axis_tx_data.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_tx_meta_V", "interface" : "axis", "bitwidth" : 48, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "m_axis_tx_meta.V.their_address.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":32,"up":47,"cElement": [{"cName": "m_axis_tx_meta.V.length.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_tx_data_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "m_axis_tx_data.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_tx_data_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "m_axis_tx_data.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_tx_data_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "m_axis_tx_data.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "reg_ip_address_V", "interface" : "axi_slave", "bundle":"AXILiteS","type":"ap_none","bitwidth" : 128, "direction" : "READONLY", "bitSlice":[{"low":0,"up":127,"cElement": [{"cName": "reg_ip_address.V","cData": "uint128","bit_use": { "low": 0,"up": 127},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}], "offset" : {"in":16}, "offset_end" : {"in":35}} , 
 	{ "Name" : "reg_listen_port_V", "interface" : "axi_slave", "bundle":"AXILiteS","type":"ap_none","bitwidth" : 16, "direction" : "READONLY", "bitSlice":[{"low":0,"up":15,"cElement": [{"cName": "reg_listen_port.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}], "offset" : {"in":36}, "offset_end" : {"in":43}} ]}
# RTL Port declarations: 
set portNum 51
set portList { 
	{ s_axi_AXILiteS_AWVALID sc_in sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_AWREADY sc_out sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_AWADDR sc_in sc_lv 6 signal -1 } 
	{ s_axi_AXILiteS_WVALID sc_in sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_WREADY sc_out sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_WDATA sc_in sc_lv 32 signal -1 } 
	{ s_axi_AXILiteS_WSTRB sc_in sc_lv 4 signal -1 } 
	{ s_axi_AXILiteS_ARVALID sc_in sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_ARREADY sc_out sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_ARADDR sc_in sc_lv 6 signal -1 } 
	{ s_axi_AXILiteS_RVALID sc_out sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_RREADY sc_in sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_RDATA sc_out sc_lv 32 signal -1 } 
	{ s_axi_AXILiteS_RRESP sc_out sc_lv 2 signal -1 } 
	{ s_axi_AXILiteS_BVALID sc_out sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_BREADY sc_in sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_BRESP sc_out sc_lv 2 signal -1 } 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst_n sc_in sc_logic 1 reset -1 active_low_sync } 
	{ s_axis_rx_meta_V_TDATA sc_in sc_lv 48 signal 0 } 
	{ s_axis_rx_data_TDATA sc_in sc_lv 64 signal 1 } 
	{ s_axis_rx_data_TKEEP sc_in sc_lv 8 signal 2 } 
	{ s_axis_rx_data_TLAST sc_in sc_lv 1 signal 3 } 
	{ m_axis_rx_meta_V_TDATA sc_out sc_lv 176 signal 4 } 
	{ m_axis_rx_data_TDATA sc_out sc_lv 64 signal 5 } 
	{ m_axis_rx_data_TKEEP sc_out sc_lv 8 signal 6 } 
	{ m_axis_rx_data_TLAST sc_out sc_lv 1 signal 7 } 
	{ s_axis_tx_meta_V_TDATA sc_in sc_lv 176 signal 8 } 
	{ s_axis_tx_data_TDATA sc_in sc_lv 64 signal 9 } 
	{ s_axis_tx_data_TKEEP sc_in sc_lv 8 signal 10 } 
	{ s_axis_tx_data_TLAST sc_in sc_lv 1 signal 11 } 
	{ m_axis_tx_meta_V_TDATA sc_out sc_lv 48 signal 12 } 
	{ m_axis_tx_data_TDATA sc_out sc_lv 64 signal 13 } 
	{ m_axis_tx_data_TKEEP sc_out sc_lv 8 signal 14 } 
	{ m_axis_tx_data_TLAST sc_out sc_lv 1 signal 15 } 
	{ s_axis_rx_data_TVALID sc_in sc_logic 1 invld 3 } 
	{ s_axis_rx_data_TREADY sc_out sc_logic 1 inacc 3 } 
	{ m_axis_rx_data_TVALID sc_out sc_logic 1 outvld 7 } 
	{ m_axis_rx_data_TREADY sc_in sc_logic 1 outacc 7 } 
	{ s_axis_rx_meta_V_TVALID sc_in sc_logic 1 invld 0 } 
	{ s_axis_rx_meta_V_TREADY sc_out sc_logic 1 inacc 0 } 
	{ m_axis_rx_meta_V_TVALID sc_out sc_logic 1 outvld 4 } 
	{ m_axis_rx_meta_V_TREADY sc_in sc_logic 1 outacc 4 } 
	{ s_axis_tx_meta_V_TVALID sc_in sc_logic 1 invld 8 } 
	{ s_axis_tx_meta_V_TREADY sc_out sc_logic 1 inacc 8 } 
	{ m_axis_tx_meta_V_TVALID sc_out sc_logic 1 outvld 12 } 
	{ m_axis_tx_meta_V_TREADY sc_in sc_logic 1 outacc 12 } 
	{ s_axis_tx_data_TVALID sc_in sc_logic 1 invld 11 } 
	{ s_axis_tx_data_TREADY sc_out sc_logic 1 inacc 11 } 
	{ m_axis_tx_data_TVALID sc_out sc_logic 1 outvld 15 } 
	{ m_axis_tx_data_TREADY sc_in sc_logic 1 outacc 15 } 
}
set NewPortList {[ 
	{ "name": "s_axi_AXILiteS_AWADDR", "direction": "in", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "AXILiteS", "role": "AWADDR" },"address":[{"name":"reg_ip_address_V","role":"data","value":"16"},{"name":"reg_listen_port_V","role":"data","value":"36"}] },
	{ "name": "s_axi_AXILiteS_AWVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "AWVALID" } },
	{ "name": "s_axi_AXILiteS_AWREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "AWREADY" } },
	{ "name": "s_axi_AXILiteS_WVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "WVALID" } },
	{ "name": "s_axi_AXILiteS_WREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "WREADY" } },
	{ "name": "s_axi_AXILiteS_WDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "AXILiteS", "role": "WDATA" } },
	{ "name": "s_axi_AXILiteS_WSTRB", "direction": "in", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "AXILiteS", "role": "WSTRB" } },
	{ "name": "s_axi_AXILiteS_ARADDR", "direction": "in", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "AXILiteS", "role": "ARADDR" },"address":[] },
	{ "name": "s_axi_AXILiteS_ARVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "ARVALID" } },
	{ "name": "s_axi_AXILiteS_ARREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "ARREADY" } },
	{ "name": "s_axi_AXILiteS_RVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "RVALID" } },
	{ "name": "s_axi_AXILiteS_RREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "RREADY" } },
	{ "name": "s_axi_AXILiteS_RDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "AXILiteS", "role": "RDATA" } },
	{ "name": "s_axi_AXILiteS_RRESP", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "AXILiteS", "role": "RRESP" } },
	{ "name": "s_axi_AXILiteS_BVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "BVALID" } },
	{ "name": "s_axi_AXILiteS_BREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "BREADY" } },
	{ "name": "s_axi_AXILiteS_BRESP", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "AXILiteS", "role": "BRESP" } }, 
 	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst_n", "role": "default" }} , 
 	{ "name": "s_axis_rx_meta_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":48, "type": "signal", "bundle":{"name": "s_axis_rx_meta_V", "role": "TDATA" }} , 
 	{ "name": "s_axis_rx_data_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "s_axis_rx_data_V_data_V", "role": "default" }} , 
 	{ "name": "s_axis_rx_data_TKEEP", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "s_axis_rx_data_V_keep_V", "role": "default" }} , 
 	{ "name": "s_axis_rx_data_TLAST", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "s_axis_rx_data_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_rx_meta_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":176, "type": "signal", "bundle":{"name": "m_axis_rx_meta_V", "role": "TDATA" }} , 
 	{ "name": "m_axis_rx_data_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "m_axis_rx_data_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_rx_data_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "m_axis_rx_data_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_rx_data_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "m_axis_rx_data_V_last_V", "role": "default" }} , 
 	{ "name": "s_axis_tx_meta_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":176, "type": "signal", "bundle":{"name": "s_axis_tx_meta_V", "role": "TDATA" }} , 
 	{ "name": "s_axis_tx_data_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "s_axis_tx_data_V_data_V", "role": "default" }} , 
 	{ "name": "s_axis_tx_data_TKEEP", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "s_axis_tx_data_V_keep_V", "role": "default" }} , 
 	{ "name": "s_axis_tx_data_TLAST", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "s_axis_tx_data_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_tx_meta_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":48, "type": "signal", "bundle":{"name": "m_axis_tx_meta_V", "role": "TDATA" }} , 
 	{ "name": "m_axis_tx_data_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "m_axis_tx_data_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_tx_data_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "m_axis_tx_data_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_tx_data_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "m_axis_tx_data_V_last_V", "role": "default" }} , 
 	{ "name": "s_axis_rx_data_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "s_axis_rx_data_V_last_V", "role": "default" }} , 
 	{ "name": "s_axis_rx_data_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "s_axis_rx_data_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_rx_data_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_rx_data_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_rx_data_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_rx_data_V_last_V", "role": "default" }} , 
 	{ "name": "s_axis_rx_meta_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "s_axis_rx_meta_V", "role": "TVALID" }} , 
 	{ "name": "s_axis_rx_meta_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "s_axis_rx_meta_V", "role": "TREADY" }} , 
 	{ "name": "m_axis_rx_meta_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_rx_meta_V", "role": "TVALID" }} , 
 	{ "name": "m_axis_rx_meta_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_rx_meta_V", "role": "TREADY" }} , 
 	{ "name": "s_axis_tx_meta_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "s_axis_tx_meta_V", "role": "TVALID" }} , 
 	{ "name": "s_axis_tx_meta_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "s_axis_tx_meta_V", "role": "TREADY" }} , 
 	{ "name": "m_axis_tx_meta_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_tx_meta_V", "role": "TVALID" }} , 
 	{ "name": "m_axis_tx_meta_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_tx_meta_V", "role": "TREADY" }} , 
 	{ "name": "s_axis_tx_data_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "s_axis_tx_data_V_last_V", "role": "default" }} , 
 	{ "name": "s_axis_tx_data_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "s_axis_tx_data_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_tx_data_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_tx_data_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_tx_data_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_tx_data_V_last_V", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22"],
		"CDFG" : "udp_top",
		"Protocol" : "ap_ctrl_none",
		"ControlExist" : "0", "ap_start" : "0", "ap_ready" : "0", "ap_done" : "0", "ap_continue" : "0", "ap_idle" : "0",
		"Pipeline" : "Dataflow", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "1",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "7", "EstimateLatencyMax" : "7",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "1",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"InputProcess" : [
			{"ID" : "2", "Name" : "udp_top_entry3_U0"},
			{"ID" : "4", "Name" : "process_udp_64_U0"},
			{"ID" : "6", "Name" : "merge_rx_meta_U0"},
			{"ID" : "7", "Name" : "split_tx_meta_U0"},
			{"ID" : "8", "Name" : "lshiftWordByOctet_U0"}],
		"OutputProcess" : [
			{"ID" : "5", "Name" : "rshiftWordByOctet_U0"},
			{"ID" : "6", "Name" : "merge_rx_meta_U0"},
			{"ID" : "7", "Name" : "split_tx_meta_U0"},
			{"ID" : "9", "Name" : "generate_udp_64_U0"}],
		"Port" : [
			{"Name" : "s_axis_rx_meta_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "6", "SubInstance" : "merge_rx_meta_U0", "Port" : "ipMetaIn_V"}]},
			{"Name" : "s_axis_rx_data_V_data_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "process_udp_64_U0", "Port" : "input_V_data_V"}]},
			{"Name" : "s_axis_rx_data_V_keep_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "process_udp_64_U0", "Port" : "input_V_keep_V"}]},
			{"Name" : "s_axis_rx_data_V_last_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "process_udp_64_U0", "Port" : "input_V_last_V"}]},
			{"Name" : "m_axis_rx_meta_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "6", "SubInstance" : "merge_rx_meta_U0", "Port" : "metaOut_V"}]},
			{"Name" : "m_axis_rx_data_V_data_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "5", "SubInstance" : "rshiftWordByOctet_U0", "Port" : "output_V_data_V"}]},
			{"Name" : "m_axis_rx_data_V_keep_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "5", "SubInstance" : "rshiftWordByOctet_U0", "Port" : "output_V_keep_V"}]},
			{"Name" : "m_axis_rx_data_V_last_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "5", "SubInstance" : "rshiftWordByOctet_U0", "Port" : "output_V_last_V"}]},
			{"Name" : "s_axis_tx_meta_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "7", "SubInstance" : "split_tx_meta_U0", "Port" : "metaIn_V"}]},
			{"Name" : "s_axis_tx_data_V_data_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "lshiftWordByOctet_U0", "Port" : "input_V_data_V"}]},
			{"Name" : "s_axis_tx_data_V_keep_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "lshiftWordByOctet_U0", "Port" : "input_V_keep_V"}]},
			{"Name" : "s_axis_tx_data_V_last_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "lshiftWordByOctet_U0", "Port" : "input_V_last_V"}]},
			{"Name" : "m_axis_tx_meta_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "7", "SubInstance" : "split_tx_meta_U0", "Port" : "metaOut0_V"}]},
			{"Name" : "m_axis_tx_data_V_data_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "9", "SubInstance" : "generate_udp_64_U0", "Port" : "output_V_data_V"}]},
			{"Name" : "m_axis_tx_data_V_keep_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "9", "SubInstance" : "generate_udp_64_U0", "Port" : "output_V_keep_V"}]},
			{"Name" : "m_axis_tx_data_V_last_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "9", "SubInstance" : "generate_udp_64_U0", "Port" : "output_V_last_V"}]},
			{"Name" : "reg_ip_address_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "reg_listen_port_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "pu_header_ready", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "process_udp_64_U0", "Port" : "pu_header_ready"}]},
			{"Name" : "pu_header_idx", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "process_udp_64_U0", "Port" : "pu_header_idx"}]},
			{"Name" : "pu_header_header_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "process_udp_64_U0", "Port" : "pu_header_header_V"}]},
			{"Name" : "metaWritten", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "process_udp_64_U0", "Port" : "metaWritten"}]},
			{"Name" : "rx_udp2shiftFifo_V_d", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "process_udp_64_U0", "Port" : "rx_udp2shiftFifo_V_d"},
					{"ID" : "5", "SubInstance" : "rshiftWordByOctet_U0", "Port" : "rx_udp2shiftFifo_V_d"}]},
			{"Name" : "rx_udp2shiftFifo_V_k", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "process_udp_64_U0", "Port" : "rx_udp2shiftFifo_V_k"},
					{"ID" : "5", "SubInstance" : "rshiftWordByOctet_U0", "Port" : "rx_udp2shiftFifo_V_k"}]},
			{"Name" : "rx_udp2shiftFifo_V_l", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "process_udp_64_U0", "Port" : "rx_udp2shiftFifo_V_l"},
					{"ID" : "5", "SubInstance" : "rshiftWordByOctet_U0", "Port" : "rx_udp2shiftFifo_V_l"}]},
			{"Name" : "rx_udpMetaFifo_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "6", "SubInstance" : "merge_rx_meta_U0", "Port" : "rx_udpMetaFifo_V"},
					{"ID" : "4", "SubInstance" : "process_udp_64_U0", "Port" : "rx_udpMetaFifo_V"}]},
			{"Name" : "tx_udpMetaFifo_V_the", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "9", "SubInstance" : "generate_udp_64_U0", "Port" : "tx_udpMetaFifo_V_the"},
					{"ID" : "7", "SubInstance" : "split_tx_meta_U0", "Port" : "tx_udpMetaFifo_V_the"}]},
			{"Name" : "tx_udpMetaFifo_V_my_s", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "9", "SubInstance" : "generate_udp_64_U0", "Port" : "tx_udpMetaFifo_V_my_s"},
					{"ID" : "7", "SubInstance" : "split_tx_meta_U0", "Port" : "tx_udpMetaFifo_V_my_s"}]},
			{"Name" : "tx_udpMetaFifo_V_len", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "9", "SubInstance" : "generate_udp_64_U0", "Port" : "tx_udpMetaFifo_V_len"},
					{"ID" : "7", "SubInstance" : "split_tx_meta_U0", "Port" : "tx_udpMetaFifo_V_len"}]},
			{"Name" : "tx_udpMetaFifo_V_val", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "9", "SubInstance" : "generate_udp_64_U0", "Port" : "tx_udpMetaFifo_V_val"},
					{"ID" : "7", "SubInstance" : "split_tx_meta_U0", "Port" : "tx_udpMetaFifo_V_val"}]},
			{"Name" : "tx_shift2udpFifo_V_d", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "9", "SubInstance" : "generate_udp_64_U0", "Port" : "tx_shift2udpFifo_V_d"},
					{"ID" : "8", "SubInstance" : "lshiftWordByOctet_U0", "Port" : "tx_shift2udpFifo_V_d"}]},
			{"Name" : "tx_shift2udpFifo_V_k", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "9", "SubInstance" : "generate_udp_64_U0", "Port" : "tx_shift2udpFifo_V_k"},
					{"ID" : "8", "SubInstance" : "lshiftWordByOctet_U0", "Port" : "tx_shift2udpFifo_V_k"}]},
			{"Name" : "tx_shift2udpFifo_V_l", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "9", "SubInstance" : "generate_udp_64_U0", "Port" : "tx_shift2udpFifo_V_l"},
					{"ID" : "8", "SubInstance" : "lshiftWordByOctet_U0", "Port" : "tx_shift2udpFifo_V_l"}]},
			{"Name" : "state", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "9", "SubInstance" : "generate_udp_64_U0", "Port" : "state"}]},
			{"Name" : "header_idx", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "9", "SubInstance" : "generate_udp_64_U0", "Port" : "header_idx"}]},
			{"Name" : "header_header_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "9", "SubInstance" : "generate_udp_64_U0", "Port" : "header_header_V"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.udp_top_AXILiteS_s_axi_U", "Parent" : "0"},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.udp_top_entry3_U0", "Parent" : "0",
		"CDFG" : "udp_top_entry3",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "0", "EstimateLatencyMin" : "0", "EstimateLatencyMax" : "0",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "0",
		"Port" : [
			{"Name" : "reg_listen_port_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "reg_listen_port_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "3", "DependentChan" : "10",
				"BlockSignal" : [
					{"Name" : "reg_listen_port_V_out_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "3", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.udp_top_entry212_U0", "Parent" : "0",
		"CDFG" : "udp_top_entry212",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "0", "EstimateLatencyMin" : "0", "EstimateLatencyMax" : "0",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "0",
		"Port" : [
			{"Name" : "reg_listen_port_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "10",
				"BlockSignal" : [
					{"Name" : "reg_listen_port_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "reg_listen_port_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "4", "DependentChan" : "11",
				"BlockSignal" : [
					{"Name" : "reg_listen_port_V_out_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "4", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.process_udp_64_U0", "Parent" : "0",
		"CDFG" : "process_udp_64_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "4", "EstimateLatencyMin" : "4", "EstimateLatencyMax" : "4",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "input_V_data_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "s_axis_rx_data_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "input_V_keep_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "input_V_last_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "regListenPort_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "3", "DependentChan" : "11",
				"BlockSignal" : [
					{"Name" : "regListenPort_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "pu_header_ready", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "pu_header_idx", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "pu_header_header_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "metaWritten", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "rx_udp2shiftFifo_V_d", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "5", "DependentChan" : "12",
				"BlockSignal" : [
					{"Name" : "rx_udp2shiftFifo_V_d_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rx_udp2shiftFifo_V_k", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "5", "DependentChan" : "13",
				"BlockSignal" : [
					{"Name" : "rx_udp2shiftFifo_V_k_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rx_udp2shiftFifo_V_l", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "5", "DependentChan" : "14",
				"BlockSignal" : [
					{"Name" : "rx_udp2shiftFifo_V_l_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rx_udpMetaFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "6", "DependentChan" : "15",
				"BlockSignal" : [
					{"Name" : "rx_udpMetaFifo_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "5", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.rshiftWordByOctet_U0", "Parent" : "0",
		"CDFG" : "rshiftWordByOctet",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "output_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_rx_data_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "output_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "output_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "rx_udp2shiftFifo_V_d", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "4", "DependentChan" : "12",
				"BlockSignal" : [
					{"Name" : "rx_udp2shiftFifo_V_d_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rx_udp2shiftFifo_V_k", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "4", "DependentChan" : "13",
				"BlockSignal" : [
					{"Name" : "rx_udp2shiftFifo_V_k_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rx_udp2shiftFifo_V_l", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "4", "DependentChan" : "14",
				"BlockSignal" : [
					{"Name" : "rx_udp2shiftFifo_V_l_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "6", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.merge_rx_meta_U0", "Parent" : "0",
		"CDFG" : "merge_rx_meta",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "ipMetaIn_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "ipMetaIn_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "metaOut_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "metaOut_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rx_udpMetaFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "4", "DependentChan" : "15",
				"BlockSignal" : [
					{"Name" : "rx_udpMetaFifo_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "7", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.split_tx_meta_U0", "Parent" : "0",
		"CDFG" : "split_tx_meta",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "metaIn_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "metaIn_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "metaOut0_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "metaOut0_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tx_udpMetaFifo_V_the", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "9", "DependentChan" : "16",
				"BlockSignal" : [
					{"Name" : "tx_udpMetaFifo_V_the_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tx_udpMetaFifo_V_my_s", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "9", "DependentChan" : "17",
				"BlockSignal" : [
					{"Name" : "tx_udpMetaFifo_V_my_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tx_udpMetaFifo_V_len", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "9", "DependentChan" : "18",
				"BlockSignal" : [
					{"Name" : "tx_udpMetaFifo_V_len_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tx_udpMetaFifo_V_val", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "9", "DependentChan" : "19",
				"BlockSignal" : [
					{"Name" : "tx_udpMetaFifo_V_val_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "8", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.lshiftWordByOctet_U0", "Parent" : "0",
		"CDFG" : "lshiftWordByOctet",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "input_V_data_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "s_axis_tx_data_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "input_V_keep_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "input_V_last_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "tx_shift2udpFifo_V_d", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "9", "DependentChan" : "20",
				"BlockSignal" : [
					{"Name" : "tx_shift2udpFifo_V_d_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tx_shift2udpFifo_V_k", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "9", "DependentChan" : "21",
				"BlockSignal" : [
					{"Name" : "tx_shift2udpFifo_V_k_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tx_shift2udpFifo_V_l", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "9", "DependentChan" : "22",
				"BlockSignal" : [
					{"Name" : "tx_shift2udpFifo_V_l_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "9", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.generate_udp_64_U0", "Parent" : "0",
		"CDFG" : "generate_udp_64_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "output_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_tx_data_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "output_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "output_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "state", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_idx", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tx_udpMetaFifo_V_the", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "7", "DependentChan" : "16",
				"BlockSignal" : [
					{"Name" : "tx_udpMetaFifo_V_the_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tx_udpMetaFifo_V_my_s", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "7", "DependentChan" : "17",
				"BlockSignal" : [
					{"Name" : "tx_udpMetaFifo_V_my_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tx_udpMetaFifo_V_len", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "7", "DependentChan" : "18",
				"BlockSignal" : [
					{"Name" : "tx_udpMetaFifo_V_len_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tx_udpMetaFifo_V_val", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "7", "DependentChan" : "19",
				"BlockSignal" : [
					{"Name" : "tx_udpMetaFifo_V_val_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "header_header_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tx_shift2udpFifo_V_d", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "8", "DependentChan" : "20",
				"BlockSignal" : [
					{"Name" : "tx_shift2udpFifo_V_d_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tx_shift2udpFifo_V_k", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "8", "DependentChan" : "21",
				"BlockSignal" : [
					{"Name" : "tx_shift2udpFifo_V_k_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tx_shift2udpFifo_V_l", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "8", "DependentChan" : "22",
				"BlockSignal" : [
					{"Name" : "tx_shift2udpFifo_V_l_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "10", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.reg_listen_port_V_c1_U", "Parent" : "0"},
	{"ID" : "11", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.reg_listen_port_V_c_U", "Parent" : "0"},
	{"ID" : "12", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.rx_udp2shiftFifo_V_d_U", "Parent" : "0"},
	{"ID" : "13", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.rx_udp2shiftFifo_V_k_U", "Parent" : "0"},
	{"ID" : "14", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.rx_udp2shiftFifo_V_l_U", "Parent" : "0"},
	{"ID" : "15", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.rx_udpMetaFifo_V_U", "Parent" : "0"},
	{"ID" : "16", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.tx_udpMetaFifo_V_the_U", "Parent" : "0"},
	{"ID" : "17", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.tx_udpMetaFifo_V_my_s_U", "Parent" : "0"},
	{"ID" : "18", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.tx_udpMetaFifo_V_len_U", "Parent" : "0"},
	{"ID" : "19", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.tx_udpMetaFifo_V_val_U", "Parent" : "0"},
	{"ID" : "20", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.tx_shift2udpFifo_V_d_U", "Parent" : "0"},
	{"ID" : "21", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.tx_shift2udpFifo_V_k_U", "Parent" : "0"},
	{"ID" : "22", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.tx_shift2udpFifo_V_l_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	udp_top {
		s_axis_rx_meta_V {Type I LastRead 0 FirstWrite -1}
		s_axis_rx_data_V_data_V {Type I LastRead 0 FirstWrite -1}
		s_axis_rx_data_V_keep_V {Type I LastRead 0 FirstWrite -1}
		s_axis_rx_data_V_last_V {Type I LastRead 0 FirstWrite -1}
		m_axis_rx_meta_V {Type O LastRead -1 FirstWrite 1}
		m_axis_rx_data_V_data_V {Type O LastRead -1 FirstWrite 1}
		m_axis_rx_data_V_keep_V {Type O LastRead -1 FirstWrite 1}
		m_axis_rx_data_V_last_V {Type O LastRead -1 FirstWrite 1}
		s_axis_tx_meta_V {Type I LastRead 0 FirstWrite -1}
		s_axis_tx_data_V_data_V {Type I LastRead 0 FirstWrite -1}
		s_axis_tx_data_V_keep_V {Type I LastRead 0 FirstWrite -1}
		s_axis_tx_data_V_last_V {Type I LastRead 0 FirstWrite -1}
		m_axis_tx_meta_V {Type O LastRead -1 FirstWrite 1}
		m_axis_tx_data_V_data_V {Type O LastRead -1 FirstWrite 1}
		m_axis_tx_data_V_keep_V {Type O LastRead -1 FirstWrite 1}
		m_axis_tx_data_V_last_V {Type O LastRead -1 FirstWrite 1}
		reg_ip_address_V {Type I LastRead -1 FirstWrite -1}
		reg_listen_port_V {Type I LastRead 0 FirstWrite -1}
		pu_header_ready {Type IO LastRead -1 FirstWrite -1}
		pu_header_idx {Type IO LastRead -1 FirstWrite -1}
		pu_header_header_V {Type IO LastRead -1 FirstWrite -1}
		metaWritten {Type IO LastRead -1 FirstWrite -1}
		rx_udp2shiftFifo_V_d {Type IO LastRead -1 FirstWrite -1}
		rx_udp2shiftFifo_V_k {Type IO LastRead -1 FirstWrite -1}
		rx_udp2shiftFifo_V_l {Type IO LastRead -1 FirstWrite -1}
		rx_udpMetaFifo_V {Type IO LastRead -1 FirstWrite -1}
		tx_udpMetaFifo_V_the {Type IO LastRead -1 FirstWrite -1}
		tx_udpMetaFifo_V_my_s {Type IO LastRead -1 FirstWrite -1}
		tx_udpMetaFifo_V_len {Type IO LastRead -1 FirstWrite -1}
		tx_udpMetaFifo_V_val {Type IO LastRead -1 FirstWrite -1}
		tx_shift2udpFifo_V_d {Type IO LastRead -1 FirstWrite -1}
		tx_shift2udpFifo_V_k {Type IO LastRead -1 FirstWrite -1}
		tx_shift2udpFifo_V_l {Type IO LastRead -1 FirstWrite -1}
		state {Type IO LastRead -1 FirstWrite -1}
		header_idx {Type IO LastRead -1 FirstWrite -1}
		header_header_V {Type IO LastRead -1 FirstWrite -1}}
	udp_top_entry3 {
		reg_listen_port_V {Type I LastRead 0 FirstWrite -1}
		reg_listen_port_V_out {Type O LastRead -1 FirstWrite 0}}
	udp_top_entry212 {
		reg_listen_port_V {Type I LastRead 0 FirstWrite -1}
		reg_listen_port_V_out {Type O LastRead -1 FirstWrite 0}}
	process_udp_64_s {
		input_V_data_V {Type I LastRead 0 FirstWrite -1}
		input_V_keep_V {Type I LastRead 0 FirstWrite -1}
		input_V_last_V {Type I LastRead 0 FirstWrite -1}
		regListenPort_V {Type I LastRead 2 FirstWrite -1}
		pu_header_ready {Type IO LastRead -1 FirstWrite -1}
		pu_header_idx {Type IO LastRead -1 FirstWrite -1}
		pu_header_header_V {Type IO LastRead -1 FirstWrite -1}
		metaWritten {Type IO LastRead -1 FirstWrite -1}
		rx_udp2shiftFifo_V_d {Type O LastRead -1 FirstWrite 4}
		rx_udp2shiftFifo_V_k {Type O LastRead -1 FirstWrite 4}
		rx_udp2shiftFifo_V_l {Type O LastRead -1 FirstWrite 4}
		rx_udpMetaFifo_V {Type O LastRead -1 FirstWrite 3}}
	rshiftWordByOctet {
		output_V_data_V {Type O LastRead -1 FirstWrite 1}
		output_V_keep_V {Type O LastRead -1 FirstWrite 1}
		output_V_last_V {Type O LastRead -1 FirstWrite 1}
		rx_udp2shiftFifo_V_d {Type I LastRead 0 FirstWrite -1}
		rx_udp2shiftFifo_V_k {Type I LastRead 0 FirstWrite -1}
		rx_udp2shiftFifo_V_l {Type I LastRead 0 FirstWrite -1}}
	merge_rx_meta {
		ipMetaIn_V {Type I LastRead 0 FirstWrite -1}
		metaOut_V {Type O LastRead -1 FirstWrite 1}
		rx_udpMetaFifo_V {Type I LastRead 0 FirstWrite -1}}
	split_tx_meta {
		metaIn_V {Type I LastRead 0 FirstWrite -1}
		metaOut0_V {Type O LastRead -1 FirstWrite 1}
		tx_udpMetaFifo_V_the {Type O LastRead -1 FirstWrite 1}
		tx_udpMetaFifo_V_my_s {Type O LastRead -1 FirstWrite 1}
		tx_udpMetaFifo_V_len {Type O LastRead -1 FirstWrite 1}
		tx_udpMetaFifo_V_val {Type O LastRead -1 FirstWrite 1}}
	lshiftWordByOctet {
		input_V_data_V {Type I LastRead 0 FirstWrite -1}
		input_V_keep_V {Type I LastRead 0 FirstWrite -1}
		input_V_last_V {Type I LastRead 0 FirstWrite -1}
		tx_shift2udpFifo_V_d {Type O LastRead -1 FirstWrite 1}
		tx_shift2udpFifo_V_k {Type O LastRead -1 FirstWrite 1}
		tx_shift2udpFifo_V_l {Type O LastRead -1 FirstWrite 1}}
	generate_udp_64_s {
		output_V_data_V {Type O LastRead -1 FirstWrite 1}
		output_V_keep_V {Type O LastRead -1 FirstWrite 1}
		output_V_last_V {Type O LastRead -1 FirstWrite 1}
		state {Type IO LastRead -1 FirstWrite -1}
		header_idx {Type IO LastRead -1 FirstWrite -1}
		tx_udpMetaFifo_V_the {Type I LastRead 0 FirstWrite -1}
		tx_udpMetaFifo_V_my_s {Type I LastRead 0 FirstWrite -1}
		tx_udpMetaFifo_V_len {Type I LastRead 0 FirstWrite -1}
		tx_udpMetaFifo_V_val {Type I LastRead 0 FirstWrite -1}
		header_header_V {Type IO LastRead -1 FirstWrite -1}
		tx_shift2udpFifo_V_d {Type I LastRead 0 FirstWrite -1}
		tx_shift2udpFifo_V_k {Type I LastRead 0 FirstWrite -1}
		tx_shift2udpFifo_V_l {Type I LastRead 0 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "7", "Max" : "7"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
]}

set Spec2ImplPortList { 
	s_axis_rx_meta_V { axis {  { s_axis_rx_meta_V_TDATA in_data 0 48 }  { s_axis_rx_meta_V_TVALID in_vld 0 1 }  { s_axis_rx_meta_V_TREADY in_acc 1 1 } } }
	s_axis_rx_data_V_data_V { axis {  { s_axis_rx_data_TDATA in_data 0 64 } } }
	s_axis_rx_data_V_keep_V { axis {  { s_axis_rx_data_TKEEP in_data 0 8 } } }
	s_axis_rx_data_V_last_V { axis {  { s_axis_rx_data_TLAST in_data 0 1 }  { s_axis_rx_data_TVALID in_vld 0 1 }  { s_axis_rx_data_TREADY in_acc 1 1 } } }
	m_axis_rx_meta_V { axis {  { m_axis_rx_meta_V_TDATA out_data 1 176 }  { m_axis_rx_meta_V_TVALID out_vld 1 1 }  { m_axis_rx_meta_V_TREADY out_acc 0 1 } } }
	m_axis_rx_data_V_data_V { axis {  { m_axis_rx_data_TDATA out_data 1 64 } } }
	m_axis_rx_data_V_keep_V { axis {  { m_axis_rx_data_TKEEP out_data 1 8 } } }
	m_axis_rx_data_V_last_V { axis {  { m_axis_rx_data_TLAST out_data 1 1 }  { m_axis_rx_data_TVALID out_vld 1 1 }  { m_axis_rx_data_TREADY out_acc 0 1 } } }
	s_axis_tx_meta_V { axis {  { s_axis_tx_meta_V_TDATA in_data 0 176 }  { s_axis_tx_meta_V_TVALID in_vld 0 1 }  { s_axis_tx_meta_V_TREADY in_acc 1 1 } } }
	s_axis_tx_data_V_data_V { axis {  { s_axis_tx_data_TDATA in_data 0 64 } } }
	s_axis_tx_data_V_keep_V { axis {  { s_axis_tx_data_TKEEP in_data 0 8 } } }
	s_axis_tx_data_V_last_V { axis {  { s_axis_tx_data_TLAST in_data 0 1 }  { s_axis_tx_data_TVALID in_vld 0 1 }  { s_axis_tx_data_TREADY in_acc 1 1 } } }
	m_axis_tx_meta_V { axis {  { m_axis_tx_meta_V_TDATA out_data 1 48 }  { m_axis_tx_meta_V_TVALID out_vld 1 1 }  { m_axis_tx_meta_V_TREADY out_acc 0 1 } } }
	m_axis_tx_data_V_data_V { axis {  { m_axis_tx_data_TDATA out_data 1 64 } } }
	m_axis_tx_data_V_keep_V { axis {  { m_axis_tx_data_TKEEP out_data 1 8 } } }
	m_axis_tx_data_V_last_V { axis {  { m_axis_tx_data_TLAST out_data 1 1 }  { m_axis_tx_data_TVALID out_vld 1 1 }  { m_axis_tx_data_TREADY out_acc 0 1 } } }
}

set busDeadlockParameterList { 
}

# RTL port scheduling information:
set fifoSchedulingInfoList { 
}

# RTL bus port read request latency information:
set busReadReqLatencyList { 
}

# RTL bus port write response latency information:
set busWriteResLatencyList { 
}

# RTL array port load latency information:
set memoryLoadLatencyList { 
}
