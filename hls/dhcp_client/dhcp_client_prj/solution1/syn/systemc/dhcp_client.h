// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
// Version: 2019.2.1
// Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

#ifndef _dhcp_client_HH_
#define _dhcp_client_HH_

#include "systemc.h"
#include "AESL_pkg.h"

#include "open_dhcp_port.h"
#include "receive_message.h"
#include "dhcp_fsm.h"
#include "send_message.h"
#include "fifo_w1_d2_A.h"
#include "fifo_w104_d4_A.h"
#include "fifo_w72_d4_A.h"

namespace ap_rtl {

struct dhcp_client : public sc_module {
    // Port declarations 35
    sc_out< sc_lv<16> > m_axis_open_port_V_V_TDATA;
    sc_in< sc_lv<8> > s_axis_open_port_status_V_TDATA;
    sc_in< sc_lv<48> > s_axis_rx_metadata_V_sourceSocket_TDATA;
    sc_in< sc_lv<48> > s_axis_rx_metadata_V_destinationSocket_TDATA;
    sc_in< sc_lv<64> > s_axis_rx_data_TDATA;
    sc_in< sc_lv<8> > s_axis_rx_data_TKEEP;
    sc_in< sc_lv<1> > s_axis_rx_data_TLAST;
    sc_out< sc_lv<96> > m_axis_tx_metadata_V_TDATA;
    sc_out< sc_lv<16> > m_axis_tx_length_V_V_TDATA;
    sc_out< sc_lv<64> > m_axis_tx_data_TDATA;
    sc_out< sc_lv<8> > m_axis_tx_data_TKEEP;
    sc_out< sc_lv<1> > m_axis_tx_data_TLAST;
    sc_in< sc_lv<1> > dhcpEnable_V;
    sc_in< sc_lv<32> > inputIpAddress_V;
    sc_out< sc_lv<32> > dhcpIpAddressOut_V;
    sc_in< sc_lv<48> > myMacAddress_V;
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst_n;
    sc_out< sc_logic > m_axis_open_port_V_V_TVALID;
    sc_in< sc_logic > m_axis_open_port_V_V_TREADY;
    sc_in< sc_logic > s_axis_open_port_status_V_TVALID;
    sc_out< sc_logic > s_axis_open_port_status_V_TREADY;
    sc_in< sc_logic > s_axis_rx_metadata_V_sourceSocket_TVALID;
    sc_out< sc_logic > s_axis_rx_metadata_V_sourceSocket_TREADY;
    sc_in< sc_logic > s_axis_rx_metadata_V_destinationSocket_TVALID;
    sc_out< sc_logic > s_axis_rx_metadata_V_destinationSocket_TREADY;
    sc_in< sc_logic > s_axis_rx_data_TVALID;
    sc_out< sc_logic > s_axis_rx_data_TREADY;
    sc_out< sc_logic > dhcpIpAddressOut_V_ap_vld;
    sc_out< sc_logic > m_axis_tx_metadata_V_TVALID;
    sc_in< sc_logic > m_axis_tx_metadata_V_TREADY;
    sc_out< sc_logic > m_axis_tx_length_V_V_TVALID;
    sc_in< sc_logic > m_axis_tx_length_V_V_TREADY;
    sc_out< sc_logic > m_axis_tx_data_TVALID;
    sc_in< sc_logic > m_axis_tx_data_TREADY;
    sc_signal< sc_logic > ap_var_for_const0;


    // Module declarations
    dhcp_client(sc_module_name name);
    SC_HAS_PROCESS(dhcp_client);

    ~dhcp_client();

    sc_trace_file* mVcdFile;

    ofstream mHdltvinHandle;
    ofstream mHdltvoutHandle;
    open_dhcp_port* open_dhcp_port_U0;
    receive_message* receive_message_U0;
    dhcp_fsm* dhcp_fsm_U0;
    send_message* send_message_U0;
    fifo_w1_d2_A* portOpen_V_V_U;
    fifo_w104_d4_A* dhcp_replyMetaFifo_V_U;
    fifo_w72_d4_A* dhcp_requestMetaFifo_1_U;
    sc_signal< sc_logic > ap_rst_n_inv;
    sc_signal< sc_logic > open_dhcp_port_U0_ap_start;
    sc_signal< sc_logic > open_dhcp_port_U0_ap_done;
    sc_signal< sc_logic > open_dhcp_port_U0_ap_continue;
    sc_signal< sc_logic > open_dhcp_port_U0_ap_idle;
    sc_signal< sc_logic > open_dhcp_port_U0_ap_ready;
    sc_signal< sc_lv<1> > open_dhcp_port_U0_portOpen_V_V_din;
    sc_signal< sc_logic > open_dhcp_port_U0_portOpen_V_V_write;
    sc_signal< sc_lv<16> > open_dhcp_port_U0_openPort_V_V_TDATA;
    sc_signal< sc_logic > open_dhcp_port_U0_openPort_V_V_TVALID;
    sc_signal< sc_logic > open_dhcp_port_U0_confirmPortStatus_V_TREADY;
    sc_signal< sc_logic > ap_sync_continue;
    sc_signal< sc_logic > receive_message_U0_ap_start;
    sc_signal< sc_logic > receive_message_U0_ap_done;
    sc_signal< sc_logic > receive_message_U0_ap_continue;
    sc_signal< sc_logic > receive_message_U0_ap_idle;
    sc_signal< sc_logic > receive_message_U0_ap_ready;
    sc_signal< sc_lv<104> > receive_message_U0_dhcp_replyMetaFifo_V_din;
    sc_signal< sc_logic > receive_message_U0_dhcp_replyMetaFifo_V_write;
    sc_signal< sc_logic > receive_message_U0_dataInMeta_V_sourceS_TREADY;
    sc_signal< sc_logic > receive_message_U0_dataInMeta_V_destina_TREADY;
    sc_signal< sc_logic > receive_message_U0_s_axis_rx_data_TREADY;
    sc_signal< sc_logic > dhcp_fsm_U0_ap_start;
    sc_signal< sc_logic > dhcp_fsm_U0_ap_done;
    sc_signal< sc_logic > dhcp_fsm_U0_ap_continue;
    sc_signal< sc_logic > dhcp_fsm_U0_ap_idle;
    sc_signal< sc_logic > dhcp_fsm_U0_ap_ready;
    sc_signal< sc_logic > dhcp_fsm_U0_dhcp_replyMetaFifo_V_read;
    sc_signal< sc_logic > dhcp_fsm_U0_portOpen_V_V_read;
    sc_signal< sc_lv<72> > dhcp_fsm_U0_dhcp_requestMetaFifo_1_din;
    sc_signal< sc_logic > dhcp_fsm_U0_dhcp_requestMetaFifo_1_write;
    sc_signal< sc_lv<32> > dhcp_fsm_U0_ipAddressOut_V;
    sc_signal< sc_logic > dhcp_fsm_U0_ipAddressOut_V_ap_vld;
    sc_signal< sc_logic > send_message_U0_ap_start;
    sc_signal< sc_logic > send_message_U0_ap_done;
    sc_signal< sc_logic > send_message_U0_ap_continue;
    sc_signal< sc_logic > send_message_U0_ap_idle;
    sc_signal< sc_logic > send_message_U0_ap_ready;
    sc_signal< sc_logic > send_message_U0_dhcp_requestMetaFifo_1_read;
    sc_signal< sc_lv<96> > send_message_U0_dataOutMeta_V_TDATA;
    sc_signal< sc_logic > send_message_U0_dataOutMeta_V_TVALID;
    sc_signal< sc_lv<16> > send_message_U0_dataOutLength_V_V_TDATA;
    sc_signal< sc_logic > send_message_U0_dataOutLength_V_V_TVALID;
    sc_signal< sc_lv<64> > send_message_U0_m_axis_tx_data_TDATA;
    sc_signal< sc_logic > send_message_U0_m_axis_tx_data_TVALID;
    sc_signal< sc_lv<8> > send_message_U0_m_axis_tx_data_TKEEP;
    sc_signal< sc_lv<1> > send_message_U0_m_axis_tx_data_TLAST;
    sc_signal< sc_logic > portOpen_V_V_full_n;
    sc_signal< sc_lv<1> > portOpen_V_V_dout;
    sc_signal< sc_logic > portOpen_V_V_empty_n;
    sc_signal< sc_logic > dhcp_replyMetaFifo_V_full_n;
    sc_signal< sc_lv<104> > dhcp_replyMetaFifo_V_dout;
    sc_signal< sc_logic > dhcp_replyMetaFifo_V_empty_n;
    sc_signal< sc_logic > dhcp_requestMetaFifo_1_full_n;
    sc_signal< sc_lv<72> > dhcp_requestMetaFifo_1_dout;
    sc_signal< sc_logic > dhcp_requestMetaFifo_1_empty_n;
    static const sc_lv<16> ap_const_lv16_0;
    static const sc_lv<96> ap_const_lv96_0;
    static const sc_lv<64> ap_const_lv64_0;
    static const sc_lv<8> ap_const_lv8_0;
    static const sc_lv<1> ap_const_lv1_0;
    static const sc_lv<32> ap_const_lv32_0;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    // Thread declarations
    void thread_ap_var_for_const0();
    void thread_ap_rst_n_inv();
    void thread_ap_sync_continue();
    void thread_dhcpIpAddressOut_V();
    void thread_dhcpIpAddressOut_V_ap_vld();
    void thread_dhcp_fsm_U0_ap_continue();
    void thread_dhcp_fsm_U0_ap_start();
    void thread_m_axis_open_port_V_V_TDATA();
    void thread_m_axis_open_port_V_V_TVALID();
    void thread_m_axis_tx_data_TDATA();
    void thread_m_axis_tx_data_TKEEP();
    void thread_m_axis_tx_data_TLAST();
    void thread_m_axis_tx_data_TVALID();
    void thread_m_axis_tx_length_V_V_TDATA();
    void thread_m_axis_tx_length_V_V_TVALID();
    void thread_m_axis_tx_metadata_V_TDATA();
    void thread_m_axis_tx_metadata_V_TVALID();
    void thread_open_dhcp_port_U0_ap_continue();
    void thread_open_dhcp_port_U0_ap_start();
    void thread_receive_message_U0_ap_continue();
    void thread_receive_message_U0_ap_start();
    void thread_s_axis_open_port_status_V_TREADY();
    void thread_s_axis_rx_data_TREADY();
    void thread_s_axis_rx_metadata_V_destinationSocket_TREADY();
    void thread_s_axis_rx_metadata_V_sourceSocket_TREADY();
    void thread_send_message_U0_ap_continue();
    void thread_send_message_U0_ap_start();
    void thread_hdltv_gen();
};

}

using namespace ap_rtl;

#endif
