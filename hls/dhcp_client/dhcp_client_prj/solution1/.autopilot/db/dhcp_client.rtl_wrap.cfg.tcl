set lang "C++"
set moduleName "dhcp_client"
set moduleIsExternC "0"
set rawDecl ""
set globalVariable ""
set PortList ""
set PortName0 "m_axis_open_port"
set BitWidth0 "16"
set ArrayOpt0 ""
set Const0 "0"
set Volatile0 "0"
set Pointer0 "2"
set Reference0 "1"
set Dims0 [list 0]
set Interface0 [list AP_STREAM 0] 
set DataType0 "[list ap_uint 16 ]"
set Port0 [list $PortName0 $Interface0 $DataType0 $Pointer0 $Dims0 $Const0 $Volatile0 $ArrayOpt0]
lappend PortList $Port0
set PortName1 "s_axis_open_port_status"
set BitWidth1 "8"
set ArrayOpt1 ""
set Const1 "0"
set Volatile1 "0"
set Pointer1 "2"
set Reference1 "1"
set Dims1 [list 0]
set Interface1 [list AP_STREAM 0] 
set DataType1 "bool"
set Port1 [list $PortName1 $Interface1 $DataType1 $Pointer1 $Dims1 $Const1 $Volatile1 $ArrayOpt1]
lappend PortList $Port1
set PortName2 "s_axis_rx_metadata"
set BitWidth2 "128"
set ArrayOpt2 ""
set Const2 "0"
set Volatile2 "0"
set Pointer2 "2"
set Reference2 "1"
set Dims2 [list 0]
set Interface2 [list AP_STREAM 0] 
set structMem2 ""
set PortName20 "sourceSocket"
set BitWidth20 "64"
set ArrayOpt20 ""
set Const20 "0"
set Volatile20 "0"
set Pointer20 "0"
set Reference20 "0"
set Dims20 [list 0]
set Interface20 "wire"
set structMem20 ""
set PortName200 "port"
set BitWidth200 "16"
set ArrayOpt200 ""
set Const200 "0"
set Volatile200 "0"
set Pointer200 "0"
set Reference200 "0"
set Dims200 [list 0]
set Interface200 "wire"
set DataType200 "[list ap_uint 16 ]"
set Port200 [list $PortName200 $Interface200 $DataType200 $Pointer200 $Dims200 $Const200 $Volatile200 $ArrayOpt200]
lappend structMem20 $Port200
set PortName201 "addr"
set BitWidth201 "32"
set ArrayOpt201 ""
set Const201 "0"
set Volatile201 "0"
set Pointer201 "0"
set Reference201 "0"
set Dims201 [list 0]
set Interface201 "wire"
set DataType201 "[list ap_uint 32 ]"
set Port201 [list $PortName201 $Interface201 $DataType201 $Pointer201 $Dims201 $Const201 $Volatile201 $ArrayOpt201]
lappend structMem20 $Port201
set structParameter20 [list ]
set structArgument20 [list ]
set NameSpace20 [list ]
set structIsPacked20 "0"
set DataType20 [list "sockaddr_in" "struct sockaddr_in" $structMem20 1 0 $structParameter20 $structArgument20 $NameSpace20 $structIsPacked20]
set Port20 [list $PortName20 $Interface20 $DataType20 $Pointer20 $Dims20 $Const20 $Volatile20 $ArrayOpt20]
lappend structMem2 $Port20
set PortName21 "destinationSocket"
set BitWidth21 "64"
set ArrayOpt21 ""
set Const21 "0"
set Volatile21 "0"
set Pointer21 "0"
set Reference21 "0"
set Dims21 [list 0]
set Interface21 "wire"
set structMem21 ""
set PortName210 "port"
set BitWidth210 "16"
set ArrayOpt210 ""
set Const210 "0"
set Volatile210 "0"
set Pointer210 "0"
set Reference210 "0"
set Dims210 [list 0]
set Interface210 "wire"
set DataType210 "[list ap_uint 16 ]"
set Port210 [list $PortName210 $Interface210 $DataType210 $Pointer210 $Dims210 $Const210 $Volatile210 $ArrayOpt210]
lappend structMem21 $Port210
set PortName211 "addr"
set BitWidth211 "32"
set ArrayOpt211 ""
set Const211 "0"
set Volatile211 "0"
set Pointer211 "0"
set Reference211 "0"
set Dims211 [list 0]
set Interface211 "wire"
set DataType211 "[list ap_uint 32 ]"
set Port211 [list $PortName211 $Interface211 $DataType211 $Pointer211 $Dims211 $Const211 $Volatile211 $ArrayOpt211]
lappend structMem21 $Port211
set structParameter21 [list ]
set structArgument21 [list ]
set NameSpace21 [list ]
set structIsPacked21 "0"
set DataType21 [list "sockaddr_in" "struct sockaddr_in" $structMem21 1 0 $structParameter21 $structArgument21 $NameSpace21 $structIsPacked21]
set Port21 [list $PortName21 $Interface21 $DataType21 $Pointer21 $Dims21 $Const21 $Volatile21 $ArrayOpt21]
lappend structMem2 $Port21
set structParameter2 [list ]
set structArgument2 [list ]
set NameSpace2 [list ]
set structIsPacked2 "0"
set DataType2 [list "udpMetadata" "struct udpMetadata" $structMem2 1 0 $structParameter2 $structArgument2 $NameSpace2 $structIsPacked2]
set Port2 [list $PortName2 $Interface2 $DataType2 $Pointer2 $Dims2 $Const2 $Volatile2 $ArrayOpt2]
lappend PortList $Port2
set PortName3 "s_axis_rx_data"
set BitWidth3 "128"
set ArrayOpt3 ""
set Const3 "0"
set Volatile3 "0"
set Pointer3 "2"
set Reference3 "1"
set Dims3 [list 0]
set Interface3 [list AP_STREAM 0] 
set structMem3 ""
set PortName30 "data"
set BitWidth30 "64"
set ArrayOpt30 ""
set Const30 "0"
set Volatile30 "0"
set Pointer30 "0"
set Reference30 "0"
set Dims30 [list 0]
set Interface30 "wire"
set DataType30 "[list ap_uint 64 ]"
set Port30 [list $PortName30 $Interface30 $DataType30 $Pointer30 $Dims30 $Const30 $Volatile30 $ArrayOpt30]
lappend structMem3 $Port30
set PortName31 "keep"
set BitWidth31 "8"
set ArrayOpt31 ""
set Const31 "0"
set Volatile31 "0"
set Pointer31 "0"
set Reference31 "0"
set Dims31 [list 0]
set Interface31 "wire"
set DataType31 "[list ap_uint 8 ]"
set Port31 [list $PortName31 $Interface31 $DataType31 $Pointer31 $Dims31 $Const31 $Volatile31 $ArrayOpt31]
lappend structMem3 $Port31
set PortName32 "last"
set BitWidth32 "8"
set ArrayOpt32 ""
set Const32 "0"
set Volatile32 "0"
set Pointer32 "0"
set Reference32 "0"
set Dims32 [list 0]
set Interface32 "wire"
set DataType32 "[list ap_uint 1 ]"
set Port32 [list $PortName32 $Interface32 $DataType32 $Pointer32 $Dims32 $Const32 $Volatile32 $ArrayOpt32]
lappend structMem3 $Port32
set DataType3tp0 "int"
set structParameter3 [list [list $DataType3tp0 D] ]
set structArgument3 [list 64 ]
set NameSpace3 [list ]
set structIsPacked3 "0"
set DataType3 [list "net_axis<64>" "struct net_axis" $structMem3 1 0 $structParameter3 $structArgument3 $NameSpace3 $structIsPacked3]
set Port3 [list $PortName3 $Interface3 $DataType3 $Pointer3 $Dims3 $Const3 $Volatile3 $ArrayOpt3]
lappend PortList $Port3
set PortName4 "m_axis_tx_metadata"
set BitWidth4 "128"
set ArrayOpt4 ""
set Const4 "0"
set Volatile4 "0"
set Pointer4 "2"
set Reference4 "1"
set Dims4 [list 0]
set Interface4 [list AP_STREAM 0] 
set structMem4 ""
set PortName40 "sourceSocket"
set BitWidth40 "64"
set ArrayOpt40 ""
set Const40 "0"
set Volatile40 "0"
set Pointer40 "0"
set Reference40 "0"
set Dims40 [list 0]
set Interface40 "wire"
set structMem40 ""
set PortName400 "port"
set BitWidth400 "16"
set ArrayOpt400 ""
set Const400 "0"
set Volatile400 "0"
set Pointer400 "0"
set Reference400 "0"
set Dims400 [list 0]
set Interface400 "wire"
set DataType400 "[list ap_uint 16 ]"
set Port400 [list $PortName400 $Interface400 $DataType400 $Pointer400 $Dims400 $Const400 $Volatile400 $ArrayOpt400]
lappend structMem40 $Port400
set PortName401 "addr"
set BitWidth401 "32"
set ArrayOpt401 ""
set Const401 "0"
set Volatile401 "0"
set Pointer401 "0"
set Reference401 "0"
set Dims401 [list 0]
set Interface401 "wire"
set DataType401 "[list ap_uint 32 ]"
set Port401 [list $PortName401 $Interface401 $DataType401 $Pointer401 $Dims401 $Const401 $Volatile401 $ArrayOpt401]
lappend structMem40 $Port401
set structParameter40 [list ]
set structArgument40 [list ]
set NameSpace40 [list ]
set structIsPacked40 "0"
set DataType40 [list "sockaddr_in" "struct sockaddr_in" $structMem40 1 0 $structParameter40 $structArgument40 $NameSpace40 $structIsPacked40]
set Port40 [list $PortName40 $Interface40 $DataType40 $Pointer40 $Dims40 $Const40 $Volatile40 $ArrayOpt40]
lappend structMem4 $Port40
set PortName41 "destinationSocket"
set BitWidth41 "64"
set ArrayOpt41 ""
set Const41 "0"
set Volatile41 "0"
set Pointer41 "0"
set Reference41 "0"
set Dims41 [list 0]
set Interface41 "wire"
set structMem41 ""
set PortName410 "port"
set BitWidth410 "16"
set ArrayOpt410 ""
set Const410 "0"
set Volatile410 "0"
set Pointer410 "0"
set Reference410 "0"
set Dims410 [list 0]
set Interface410 "wire"
set DataType410 "[list ap_uint 16 ]"
set Port410 [list $PortName410 $Interface410 $DataType410 $Pointer410 $Dims410 $Const410 $Volatile410 $ArrayOpt410]
lappend structMem41 $Port410
set PortName411 "addr"
set BitWidth411 "32"
set ArrayOpt411 ""
set Const411 "0"
set Volatile411 "0"
set Pointer411 "0"
set Reference411 "0"
set Dims411 [list 0]
set Interface411 "wire"
set DataType411 "[list ap_uint 32 ]"
set Port411 [list $PortName411 $Interface411 $DataType411 $Pointer411 $Dims411 $Const411 $Volatile411 $ArrayOpt411]
lappend structMem41 $Port411
set structParameter41 [list ]
set structArgument41 [list ]
set NameSpace41 [list ]
set structIsPacked41 "0"
set DataType41 [list "sockaddr_in" "struct sockaddr_in" $structMem41 1 0 $structParameter41 $structArgument41 $NameSpace41 $structIsPacked41]
set Port41 [list $PortName41 $Interface41 $DataType41 $Pointer41 $Dims41 $Const41 $Volatile41 $ArrayOpt41]
lappend structMem4 $Port41
set structParameter4 [list ]
set structArgument4 [list ]
set NameSpace4 [list ]
set structIsPacked4 "0"
set DataType4 [list "udpMetadata" "struct udpMetadata" $structMem4 1 0 $structParameter4 $structArgument4 $NameSpace4 $structIsPacked4]
set Port4 [list $PortName4 $Interface4 $DataType4 $Pointer4 $Dims4 $Const4 $Volatile4 $ArrayOpt4]
lappend PortList $Port4
set PortName5 "m_axis_tx_length"
set BitWidth5 "16"
set ArrayOpt5 ""
set Const5 "0"
set Volatile5 "0"
set Pointer5 "2"
set Reference5 "1"
set Dims5 [list 0]
set Interface5 [list AP_STREAM 0] 
set DataType5 "[list ap_uint 16 ]"
set Port5 [list $PortName5 $Interface5 $DataType5 $Pointer5 $Dims5 $Const5 $Volatile5 $ArrayOpt5]
lappend PortList $Port5
set PortName6 "m_axis_tx_data"
set BitWidth6 "128"
set ArrayOpt6 ""
set Const6 "0"
set Volatile6 "0"
set Pointer6 "2"
set Reference6 "1"
set Dims6 [list 0]
set Interface6 [list AP_STREAM 0] 
set structMem6 ""
set PortName60 "data"
set BitWidth60 "64"
set ArrayOpt60 ""
set Const60 "0"
set Volatile60 "0"
set Pointer60 "0"
set Reference60 "0"
set Dims60 [list 0]
set Interface60 "wire"
set DataType60 "[list ap_uint 64 ]"
set Port60 [list $PortName60 $Interface60 $DataType60 $Pointer60 $Dims60 $Const60 $Volatile60 $ArrayOpt60]
lappend structMem6 $Port60
set PortName61 "keep"
set BitWidth61 "8"
set ArrayOpt61 ""
set Const61 "0"
set Volatile61 "0"
set Pointer61 "0"
set Reference61 "0"
set Dims61 [list 0]
set Interface61 "wire"
set DataType61 "[list ap_uint 8 ]"
set Port61 [list $PortName61 $Interface61 $DataType61 $Pointer61 $Dims61 $Const61 $Volatile61 $ArrayOpt61]
lappend structMem6 $Port61
set PortName62 "last"
set BitWidth62 "8"
set ArrayOpt62 ""
set Const62 "0"
set Volatile62 "0"
set Pointer62 "0"
set Reference62 "0"
set Dims62 [list 0]
set Interface62 "wire"
set DataType62 "[list ap_uint 1 ]"
set Port62 [list $PortName62 $Interface62 $DataType62 $Pointer62 $Dims62 $Const62 $Volatile62 $ArrayOpt62]
lappend structMem6 $Port62
set DataType6tp0 "int"
set structParameter6 [list [list $DataType6tp0 D] ]
set structArgument6 [list 64 ]
set NameSpace6 [list ]
set structIsPacked6 "0"
set DataType6 [list "net_axis<64>" "struct net_axis" $structMem6 1 0 $structParameter6 $structArgument6 $NameSpace6 $structIsPacked6]
set Port6 [list $PortName6 $Interface6 $DataType6 $Pointer6 $Dims6 $Const6 $Volatile6 $ArrayOpt6]
lappend PortList $Port6
set PortName7 "dhcpEnable"
set BitWidth7 "8"
set ArrayOpt7 ""
set Const7 "0"
set Volatile7 "0"
set Pointer7 "2"
set Reference7 "1"
set Dims7 [list 0]
set Interface7 "wire"
set DataType7 "[list ap_uint 1 ]"
set Port7 [list $PortName7 $Interface7 $DataType7 $Pointer7 $Dims7 $Const7 $Volatile7 $ArrayOpt7]
lappend PortList $Port7
set PortName8 "inputIpAddress"
set BitWidth8 "32"
set ArrayOpt8 ""
set Const8 "0"
set Volatile8 "0"
set Pointer8 "2"
set Reference8 "1"
set Dims8 [list 0]
set Interface8 "wire"
set DataType8 "[list ap_uint 32 ]"
set Port8 [list $PortName8 $Interface8 $DataType8 $Pointer8 $Dims8 $Const8 $Volatile8 $ArrayOpt8]
lappend PortList $Port8
set PortName9 "dhcpIpAddressOut"
set BitWidth9 "32"
set ArrayOpt9 ""
set Const9 "0"
set Volatile9 "0"
set Pointer9 "2"
set Reference9 "1"
set Dims9 [list 0]
set Interface9 "wire"
set DataType9 "[list ap_uint 32 ]"
set Port9 [list $PortName9 $Interface9 $DataType9 $Pointer9 $Dims9 $Const9 $Volatile9 $ArrayOpt9]
lappend PortList $Port9
set PortName10 "myMacAddress"
set BitWidth10 "64"
set ArrayOpt10 ""
set Const10 "0"
set Volatile10 "0"
set Pointer10 "2"
set Reference10 "1"
set Dims10 [list 0]
set Interface10 "wire"
set DataType10 "[list ap_uint 48 ]"
set Port10 [list $PortName10 $Interface10 $DataType10 $Pointer10 $Dims10 $Const10 $Volatile10 $ArrayOpt10]
lappend PortList $Port10
set globalAPint "" 
set returnAPInt "" 
set hasCPPAPInt 1 
set argAPInt "" 
set hasCPPAPFix 0 
set hasSCFix 0 
set hasCBool 0 
set hasCPPComplex 0 
set isTemplateTop 0
set hasHalf 0 
set dataPackList ""
set module [list $moduleName $PortList $rawDecl $argAPInt $returnAPInt $dataPackList]
