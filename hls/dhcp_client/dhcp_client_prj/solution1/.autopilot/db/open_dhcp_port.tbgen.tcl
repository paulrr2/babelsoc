set moduleName open_dhcp_port
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {open_dhcp_port}
set C_modelType { void 0 }
set C_modelArgList {
	{ openPort_V_V int 16 regular {axi_s 1 volatile  { openPort_V_V Data } }  }
	{ confirmPortStatus_V int 8 regular {axi_s 0 volatile  { confirmPortStatus_V Data } }  }
	{ portOpen_V_V int 1 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "openPort_V_V", "interface" : "axis", "bitwidth" : 16, "direction" : "WRITEONLY"} , 
 	{ "Name" : "confirmPortStatus_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "portOpen_V_V", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 16
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ confirmPortStatus_V_TVALID sc_in sc_logic 1 invld 1 } 
	{ openPort_V_V_TREADY sc_in sc_logic 1 outacc 0 } 
	{ portOpen_V_V_din sc_out sc_lv 1 signal 2 } 
	{ portOpen_V_V_full_n sc_in sc_logic 1 signal 2 } 
	{ portOpen_V_V_write sc_out sc_logic 1 signal 2 } 
	{ openPort_V_V_TDATA sc_out sc_lv 16 signal 0 } 
	{ openPort_V_V_TVALID sc_out sc_logic 1 outvld 0 } 
	{ confirmPortStatus_V_TDATA sc_in sc_lv 8 signal 1 } 
	{ confirmPortStatus_V_TREADY sc_out sc_logic 1 inacc 1 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "confirmPortStatus_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "confirmPortStatus_V", "role": "TVALID" }} , 
 	{ "name": "openPort_V_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "openPort_V_V", "role": "TREADY" }} , 
 	{ "name": "portOpen_V_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "portOpen_V_V", "role": "din" }} , 
 	{ "name": "portOpen_V_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "portOpen_V_V", "role": "full_n" }} , 
 	{ "name": "portOpen_V_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "portOpen_V_V", "role": "write" }} , 
 	{ "name": "openPort_V_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "openPort_V_V", "role": "TDATA" }} , 
 	{ "name": "openPort_V_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "openPort_V_V", "role": "TVALID" }} , 
 	{ "name": "confirmPortStatus_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "confirmPortStatus_V", "role": "TDATA" }} , 
 	{ "name": "confirmPortStatus_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "confirmPortStatus_V", "role": "TREADY" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "open_dhcp_port",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "openPort_V_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "openPort_V_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "confirmPortStatus_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "confirmPortStatus_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "openPortWaitTime_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "odp_listenDone", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "odp_waitListenStatus", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "portOpen_V_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "portOpen_V_V_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	open_dhcp_port {
		openPort_V_V {Type O LastRead 0 FirstWrite 0}
		confirmPortStatus_V {Type I LastRead 0 FirstWrite -1}
		openPortWaitTime_V {Type IO LastRead -1 FirstWrite -1}
		odp_listenDone {Type IO LastRead -1 FirstWrite -1}
		odp_waitListenStatus {Type IO LastRead -1 FirstWrite -1}
		portOpen_V_V {Type O LastRead -1 FirstWrite 1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "1", "Max" : "1"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	openPort_V_V { axis {  { openPort_V_V_TREADY out_acc 0 1 }  { openPort_V_V_TDATA out_data 1 16 }  { openPort_V_V_TVALID out_vld 1 1 } } }
	confirmPortStatus_V { axis {  { confirmPortStatus_V_TVALID in_vld 0 1 }  { confirmPortStatus_V_TDATA in_data 0 8 }  { confirmPortStatus_V_TREADY in_acc 1 1 } } }
	portOpen_V_V { ap_fifo {  { portOpen_V_V_din fifo_data 1 1 }  { portOpen_V_V_full_n fifo_status 0 1 }  { portOpen_V_V_write fifo_update 1 1 } } }
}
