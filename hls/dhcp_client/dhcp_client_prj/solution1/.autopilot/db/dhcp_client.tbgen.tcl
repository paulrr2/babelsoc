set moduleName dhcp_client
set isTopModule 1
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type dataflow
set FunctionProtocol ap_ctrl_none
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {dhcp_client}
set C_modelType { void 0 }
set C_modelArgList {
	{ m_axis_open_port_V_V int 16 regular {axi_s 1 volatile  { m_axis_open_port_V_V Data } }  }
	{ s_axis_open_port_status_V int 8 regular {axi_s 0 volatile  { s_axis_open_port_status_V Data } }  }
	{ s_axis_rx_metadata_V_sourceSocket int 48 regular {axi_s 0 volatile  { s_axis_rx_metadata_V_sourceSocket Data } }  }
	{ s_axis_rx_metadata_V_destinationSocket int 48 regular {axi_s 0 volatile  { s_axis_rx_metadata_V_destinationSocket Data } }  }
	{ s_axis_rx_data_V_data_V int 64 regular {axi_s 0 volatile  { s_axis_rx_data Data } }  }
	{ s_axis_rx_data_V_keep_V int 8 regular {axi_s 0 volatile  { s_axis_rx_data Keep } }  }
	{ s_axis_rx_data_V_last_V int 1 regular {axi_s 0 volatile  { s_axis_rx_data Last } }  }
	{ m_axis_tx_metadata_V int 96 regular {axi_s 1 volatile  { m_axis_tx_metadata_V Data } }  }
	{ m_axis_tx_length_V_V int 16 regular {axi_s 1 volatile  { m_axis_tx_length_V_V Data } }  }
	{ m_axis_tx_data_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_tx_data Data } }  }
	{ m_axis_tx_data_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_tx_data Keep } }  }
	{ m_axis_tx_data_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_tx_data Last } }  }
	{ dhcpEnable_V int 1 regular {pointer 0}  }
	{ inputIpAddress_V int 32 regular {pointer 0}  }
	{ dhcpIpAddressOut_V int 32 regular {pointer 1}  }
	{ myMacAddress_V int 48 regular {pointer 0}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "m_axis_open_port_V_V", "interface" : "axis", "bitwidth" : 16, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":15,"cElement": [{"cName": "m_axis_open_port.V.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_open_port_status_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "s_axis_open_port_status.V","cData": "bool","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_rx_metadata_V_sourceSocket", "interface" : "axis", "bitwidth" : 48, "direction" : "READONLY", "bitSlice":[{"low":0,"up":15,"cElement": [{"cName": "s_axis_rx_metadata.V.sourceSocket.port.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":16,"up":47,"cElement": [{"cName": "s_axis_rx_metadata.V.sourceSocket.addr.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_rx_metadata_V_destinationSocket", "interface" : "axis", "bitwidth" : 48, "direction" : "READONLY", "bitSlice":[{"low":0,"up":15,"cElement": [{"cName": "s_axis_rx_metadata.V.destinationSocket.port.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":16,"up":47,"cElement": [{"cName": "s_axis_rx_metadata.V.destinationSocket.addr.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_rx_data_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "READONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "s_axis_rx_data.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_rx_data_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "s_axis_rx_data.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_rx_data_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "s_axis_rx_data.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_tx_metadata_V", "interface" : "axis", "bitwidth" : 96, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":15,"cElement": [{"cName": "m_axis_tx_metadata.V.sourceSocket.port.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":16,"up":47,"cElement": [{"cName": "m_axis_tx_metadata.V.sourceSocket.addr.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":48,"up":63,"cElement": [{"cName": "m_axis_tx_metadata.V.destinationSocket.port.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":64,"up":95,"cElement": [{"cName": "m_axis_tx_metadata.V.destinationSocket.addr.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_tx_length_V_V", "interface" : "axis", "bitwidth" : 16, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":15,"cElement": [{"cName": "m_axis_tx_length.V.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_tx_data_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "m_axis_tx_data.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_tx_data_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "m_axis_tx_data.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_tx_data_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "m_axis_tx_data.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "dhcpEnable_V", "interface" : "wire", "bitwidth" : 1, "direction" : "READONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "dhcpEnable.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "inputIpAddress_V", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "inputIpAddress.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "dhcpIpAddressOut_V", "interface" : "wire", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "dhcpIpAddressOut.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "myMacAddress_V", "interface" : "wire", "bitwidth" : 48, "direction" : "READONLY", "bitSlice":[{"low":0,"up":47,"cElement": [{"cName": "myMacAddress.V","cData": "uint48","bit_use": { "low": 0,"up": 47},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} ]}
# RTL Port declarations: 
set portNum 35
set portList { 
	{ m_axis_open_port_V_V_TDATA sc_out sc_lv 16 signal 0 } 
	{ s_axis_open_port_status_V_TDATA sc_in sc_lv 8 signal 1 } 
	{ s_axis_rx_metadata_V_sourceSocket_TDATA sc_in sc_lv 48 signal 2 } 
	{ s_axis_rx_metadata_V_destinationSocket_TDATA sc_in sc_lv 48 signal 3 } 
	{ s_axis_rx_data_TDATA sc_in sc_lv 64 signal 4 } 
	{ s_axis_rx_data_TKEEP sc_in sc_lv 8 signal 5 } 
	{ s_axis_rx_data_TLAST sc_in sc_lv 1 signal 6 } 
	{ m_axis_tx_metadata_V_TDATA sc_out sc_lv 96 signal 7 } 
	{ m_axis_tx_length_V_V_TDATA sc_out sc_lv 16 signal 8 } 
	{ m_axis_tx_data_TDATA sc_out sc_lv 64 signal 9 } 
	{ m_axis_tx_data_TKEEP sc_out sc_lv 8 signal 10 } 
	{ m_axis_tx_data_TLAST sc_out sc_lv 1 signal 11 } 
	{ dhcpEnable_V sc_in sc_lv 1 signal 12 } 
	{ inputIpAddress_V sc_in sc_lv 32 signal 13 } 
	{ dhcpIpAddressOut_V sc_out sc_lv 32 signal 14 } 
	{ myMacAddress_V sc_in sc_lv 48 signal 15 } 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst_n sc_in sc_logic 1 reset -1 active_low_sync } 
	{ m_axis_open_port_V_V_TVALID sc_out sc_logic 1 outvld 0 } 
	{ m_axis_open_port_V_V_TREADY sc_in sc_logic 1 outacc 0 } 
	{ s_axis_open_port_status_V_TVALID sc_in sc_logic 1 invld 1 } 
	{ s_axis_open_port_status_V_TREADY sc_out sc_logic 1 inacc 1 } 
	{ s_axis_rx_metadata_V_sourceSocket_TVALID sc_in sc_logic 1 invld 2 } 
	{ s_axis_rx_metadata_V_sourceSocket_TREADY sc_out sc_logic 1 inacc 2 } 
	{ s_axis_rx_metadata_V_destinationSocket_TVALID sc_in sc_logic 1 invld 3 } 
	{ s_axis_rx_metadata_V_destinationSocket_TREADY sc_out sc_logic 1 inacc 3 } 
	{ s_axis_rx_data_TVALID sc_in sc_logic 1 invld 6 } 
	{ s_axis_rx_data_TREADY sc_out sc_logic 1 inacc 6 } 
	{ dhcpIpAddressOut_V_ap_vld sc_out sc_logic 1 outvld 14 } 
	{ m_axis_tx_metadata_V_TVALID sc_out sc_logic 1 outvld 7 } 
	{ m_axis_tx_metadata_V_TREADY sc_in sc_logic 1 outacc 7 } 
	{ m_axis_tx_length_V_V_TVALID sc_out sc_logic 1 outvld 8 } 
	{ m_axis_tx_length_V_V_TREADY sc_in sc_logic 1 outacc 8 } 
	{ m_axis_tx_data_TVALID sc_out sc_logic 1 outvld 11 } 
	{ m_axis_tx_data_TREADY sc_in sc_logic 1 outacc 11 } 
}
set NewPortList {[ 
	{ "name": "m_axis_open_port_V_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "m_axis_open_port_V_V", "role": "TDATA" }} , 
 	{ "name": "s_axis_open_port_status_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "s_axis_open_port_status_V", "role": "TDATA" }} , 
 	{ "name": "s_axis_rx_metadata_V_sourceSocket_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":48, "type": "signal", "bundle":{"name": "s_axis_rx_metadata_V_sourceSocket", "role": "TDATA" }} , 
 	{ "name": "s_axis_rx_metadata_V_destinationSocket_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":48, "type": "signal", "bundle":{"name": "s_axis_rx_metadata_V_destinationSocket", "role": "TDATA" }} , 
 	{ "name": "s_axis_rx_data_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "s_axis_rx_data_V_data_V", "role": "default" }} , 
 	{ "name": "s_axis_rx_data_TKEEP", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "s_axis_rx_data_V_keep_V", "role": "default" }} , 
 	{ "name": "s_axis_rx_data_TLAST", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "s_axis_rx_data_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_tx_metadata_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":96, "type": "signal", "bundle":{"name": "m_axis_tx_metadata_V", "role": "TDATA" }} , 
 	{ "name": "m_axis_tx_length_V_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "m_axis_tx_length_V_V", "role": "TDATA" }} , 
 	{ "name": "m_axis_tx_data_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "m_axis_tx_data_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_tx_data_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "m_axis_tx_data_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_tx_data_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "m_axis_tx_data_V_last_V", "role": "default" }} , 
 	{ "name": "dhcpEnable_V", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "dhcpEnable_V", "role": "default" }} , 
 	{ "name": "inputIpAddress_V", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "inputIpAddress_V", "role": "default" }} , 
 	{ "name": "dhcpIpAddressOut_V", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "dhcpIpAddressOut_V", "role": "default" }} , 
 	{ "name": "myMacAddress_V", "direction": "in", "datatype": "sc_lv", "bitwidth":48, "type": "signal", "bundle":{"name": "myMacAddress_V", "role": "default" }} , 
 	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst_n", "role": "default" }} , 
 	{ "name": "m_axis_open_port_V_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_open_port_V_V", "role": "TVALID" }} , 
 	{ "name": "m_axis_open_port_V_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_open_port_V_V", "role": "TREADY" }} , 
 	{ "name": "s_axis_open_port_status_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "s_axis_open_port_status_V", "role": "TVALID" }} , 
 	{ "name": "s_axis_open_port_status_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "s_axis_open_port_status_V", "role": "TREADY" }} , 
 	{ "name": "s_axis_rx_metadata_V_sourceSocket_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "s_axis_rx_metadata_V_sourceSocket", "role": "TVALID" }} , 
 	{ "name": "s_axis_rx_metadata_V_sourceSocket_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "s_axis_rx_metadata_V_sourceSocket", "role": "TREADY" }} , 
 	{ "name": "s_axis_rx_metadata_V_destinationSocket_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "s_axis_rx_metadata_V_destinationSocket", "role": "TVALID" }} , 
 	{ "name": "s_axis_rx_metadata_V_destinationSocket_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "s_axis_rx_metadata_V_destinationSocket", "role": "TREADY" }} , 
 	{ "name": "s_axis_rx_data_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "s_axis_rx_data_V_last_V", "role": "default" }} , 
 	{ "name": "s_axis_rx_data_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "s_axis_rx_data_V_last_V", "role": "default" }} , 
 	{ "name": "dhcpIpAddressOut_V_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "dhcpIpAddressOut_V", "role": "ap_vld" }} , 
 	{ "name": "m_axis_tx_metadata_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_tx_metadata_V", "role": "TVALID" }} , 
 	{ "name": "m_axis_tx_metadata_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_tx_metadata_V", "role": "TREADY" }} , 
 	{ "name": "m_axis_tx_length_V_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_tx_length_V_V", "role": "TVALID" }} , 
 	{ "name": "m_axis_tx_length_V_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_tx_length_V_V", "role": "TREADY" }} , 
 	{ "name": "m_axis_tx_data_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_tx_data_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_tx_data_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_tx_data_V_last_V", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2", "3", "4", "10", "11", "12"],
		"CDFG" : "dhcp_client",
		"Protocol" : "ap_ctrl_none",
		"ControlExist" : "0", "ap_start" : "0", "ap_ready" : "0", "ap_done" : "0", "ap_continue" : "0", "ap_idle" : "0",
		"Pipeline" : "Dataflow", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "1",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "7", "EstimateLatencyMax" : "7",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "1",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"InputProcess" : [
			{"ID" : "1", "Name" : "open_dhcp_port_U0"},
			{"ID" : "2", "Name" : "receive_message_U0"}],
		"OutputProcess" : [
			{"ID" : "1", "Name" : "open_dhcp_port_U0"},
			{"ID" : "3", "Name" : "dhcp_fsm_U0"},
			{"ID" : "4", "Name" : "send_message_U0"}],
		"Port" : [
			{"Name" : "m_axis_open_port_V_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "open_dhcp_port_U0", "Port" : "openPort_V_V"}]},
			{"Name" : "s_axis_open_port_status_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "open_dhcp_port_U0", "Port" : "confirmPortStatus_V"}]},
			{"Name" : "s_axis_rx_metadata_V_sourceSocket", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "receive_message_U0", "Port" : "dataInMeta_V_sourceS"}]},
			{"Name" : "s_axis_rx_metadata_V_destinationSocket", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "receive_message_U0", "Port" : "dataInMeta_V_destina"}]},
			{"Name" : "s_axis_rx_data_V_data_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "receive_message_U0", "Port" : "dataIn_V_data_V"}]},
			{"Name" : "s_axis_rx_data_V_keep_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "receive_message_U0", "Port" : "dataIn_V_keep_V"}]},
			{"Name" : "s_axis_rx_data_V_last_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "receive_message_U0", "Port" : "dataIn_V_last_V"}]},
			{"Name" : "m_axis_tx_metadata_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "send_message_U0", "Port" : "dataOutMeta_V"}]},
			{"Name" : "m_axis_tx_length_V_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "send_message_U0", "Port" : "dataOutLength_V_V"}]},
			{"Name" : "m_axis_tx_data_V_data_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "send_message_U0", "Port" : "dataOut_V_data_V"}]},
			{"Name" : "m_axis_tx_data_V_keep_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "send_message_U0", "Port" : "dataOut_V_keep_V"}]},
			{"Name" : "m_axis_tx_data_V_last_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "send_message_U0", "Port" : "dataOut_V_last_V"}]},
			{"Name" : "dhcpEnable_V", "Type" : "Stable", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "dhcp_fsm_U0", "Port" : "dhcpEnable_V"}]},
			{"Name" : "inputIpAddress_V", "Type" : "Stable", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "dhcp_fsm_U0", "Port" : "inputIpAddress_V"}]},
			{"Name" : "dhcpIpAddressOut_V", "Type" : "Vld", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "dhcp_fsm_U0", "Port" : "ipAddressOut_V"}]},
			{"Name" : "myMacAddress_V", "Type" : "Stable", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "send_message_U0", "Port" : "myMacAddress_V"},
					{"ID" : "2", "SubInstance" : "receive_message_U0", "Port" : "myMacAddress_V"}]},
			{"Name" : "openPortWaitTime_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "open_dhcp_port_U0", "Port" : "openPortWaitTime_V"}]},
			{"Name" : "odp_listenDone", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "open_dhcp_port_U0", "Port" : "odp_listenDone"}]},
			{"Name" : "odp_waitListenStatus", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "open_dhcp_port_U0", "Port" : "odp_waitListenStatus"}]},
			{"Name" : "portOpen_V_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "open_dhcp_port_U0", "Port" : "portOpen_V_V"},
					{"ID" : "3", "SubInstance" : "dhcp_fsm_U0", "Port" : "portOpen_V_V"}]},
			{"Name" : "rm_wordCount_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "receive_message_U0", "Port" : "rm_wordCount_V"}]},
			{"Name" : "rm_isReply", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "receive_message_U0", "Port" : "rm_isReply"}]},
			{"Name" : "rm_correctMac", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "receive_message_U0", "Port" : "rm_correctMac"}]},
			{"Name" : "rm_isDHCP", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "receive_message_U0", "Port" : "rm_isDHCP"}]},
			{"Name" : "meta_identifier_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "receive_message_U0", "Port" : "meta_identifier_V"}]},
			{"Name" : "meta_assignedIpAddre", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "receive_message_U0", "Port" : "meta_assignedIpAddre"}]},
			{"Name" : "meta_serverAddress_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "receive_message_U0", "Port" : "meta_serverAddress_V"}]},
			{"Name" : "meta_type_V_1", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "receive_message_U0", "Port" : "meta_type_V_1"}]},
			{"Name" : "dhcp_replyMetaFifo_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "receive_message_U0", "Port" : "dhcp_replyMetaFifo_V"},
					{"ID" : "3", "SubInstance" : "dhcp_fsm_U0", "Port" : "dhcp_replyMetaFifo_V"}]},
			{"Name" : "myIpAddress_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "dhcp_fsm_U0", "Port" : "myIpAddress_V"}]},
			{"Name" : "state", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "dhcp_fsm_U0", "Port" : "state"}]},
			{"Name" : "time_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "dhcp_fsm_U0", "Port" : "time_V"}]},
			{"Name" : "randomValue_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "dhcp_fsm_U0", "Port" : "randomValue_V"}]},
			{"Name" : "myIdentity_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "dhcp_fsm_U0", "Port" : "myIdentity_V"}]},
			{"Name" : "IpAddressBuffer_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "dhcp_fsm_U0", "Port" : "IpAddressBuffer_V"}]},
			{"Name" : "dhcp_requestMetaFifo_1", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "send_message_U0", "Port" : "dhcp_requestMetaFifo_1"},
					{"ID" : "3", "SubInstance" : "dhcp_fsm_U0", "Port" : "dhcp_requestMetaFifo_1"}]},
			{"Name" : "sm_wordCount_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "send_message_U0", "Port" : "sm_wordCount_V"}]},
			{"Name" : "meta_type_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "send_message_U0", "Port" : "meta_type_V"}]},
			{"Name" : "meta_requestedIpAddr", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "send_message_U0", "Port" : "meta_requestedIpAddr"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.open_dhcp_port_U0", "Parent" : "0",
		"CDFG" : "open_dhcp_port",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "openPort_V_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "openPort_V_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "confirmPortStatus_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "confirmPortStatus_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "openPortWaitTime_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "odp_listenDone", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "odp_waitListenStatus", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "portOpen_V_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "3", "DependentChan" : "10",
				"BlockSignal" : [
					{"Name" : "portOpen_V_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.receive_message_U0", "Parent" : "0",
		"CDFG" : "receive_message",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "dataInMeta_V_sourceS", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "dataInMeta_V_sourceS_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataInMeta_V_destina", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "dataInMeta_V_destina_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataIn_V_data_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "s_axis_rx_data_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataIn_V_keep_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "dataIn_V_last_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "myMacAddress_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "rm_wordCount_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "rm_isReply", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "rm_correctMac", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "rm_isDHCP", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "meta_identifier_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "meta_assignedIpAddre", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "meta_serverAddress_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "meta_type_V_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "dhcp_replyMetaFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "3", "DependentChan" : "11",
				"BlockSignal" : [
					{"Name" : "dhcp_replyMetaFifo_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "3", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.dhcp_fsm_U0", "Parent" : "0",
		"CDFG" : "dhcp_fsm",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "ipAddressOut_V", "Type" : "Vld", "Direction" : "O"},
			{"Name" : "dhcpEnable_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "inputIpAddress_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "myIpAddress_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "state", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "time_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "randomValue_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "myIdentity_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "IpAddressBuffer_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "portOpen_V_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "1", "DependentChan" : "10",
				"BlockSignal" : [
					{"Name" : "portOpen_V_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dhcp_requestMetaFifo_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "4", "DependentChan" : "12",
				"BlockSignal" : [
					{"Name" : "dhcp_requestMetaFifo_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dhcp_replyMetaFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "11",
				"BlockSignal" : [
					{"Name" : "dhcp_replyMetaFifo_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "4", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.send_message_U0", "Parent" : "0", "Child" : ["5", "6", "7", "8", "9"],
		"CDFG" : "send_message",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "dataOutMeta_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "dataOutMeta_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataOutLength_V_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "dataOutLength_V_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataOut_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_tx_data_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataOut_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "dataOut_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "myMacAddress_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "sm_wordCount_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "meta_type_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "meta_requestedIpAddr", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "dhcp_requestMetaFifo_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "3", "DependentChan" : "12",
				"BlockSignal" : [
					{"Name" : "dhcp_requestMetaFifo_1_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "5", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.send_message_U0.regslice_both_dataOutMeta_V_U", "Parent" : "4"},
	{"ID" : "6", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.send_message_U0.regslice_both_dataOutLength_V_V_U", "Parent" : "4"},
	{"ID" : "7", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.send_message_U0.regslice_both_dataOut_V_data_V_U", "Parent" : "4"},
	{"ID" : "8", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.send_message_U0.regslice_both_dataOut_V_keep_V_U", "Parent" : "4"},
	{"ID" : "9", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.send_message_U0.regslice_both_dataOut_V_last_V_U", "Parent" : "4"},
	{"ID" : "10", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.portOpen_V_V_U", "Parent" : "0"},
	{"ID" : "11", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.dhcp_replyMetaFifo_V_U", "Parent" : "0"},
	{"ID" : "12", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.dhcp_requestMetaFifo_1_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	dhcp_client {
		m_axis_open_port_V_V {Type O LastRead 0 FirstWrite 0}
		s_axis_open_port_status_V {Type I LastRead 0 FirstWrite -1}
		s_axis_rx_metadata_V_sourceSocket {Type I LastRead 0 FirstWrite -1}
		s_axis_rx_metadata_V_destinationSocket {Type I LastRead 0 FirstWrite -1}
		s_axis_rx_data_V_data_V {Type I LastRead 0 FirstWrite -1}
		s_axis_rx_data_V_keep_V {Type I LastRead 0 FirstWrite -1}
		s_axis_rx_data_V_last_V {Type I LastRead 0 FirstWrite -1}
		m_axis_tx_metadata_V {Type O LastRead -1 FirstWrite 1}
		m_axis_tx_length_V_V {Type O LastRead -1 FirstWrite 1}
		m_axis_tx_data_V_data_V {Type O LastRead -1 FirstWrite 1}
		m_axis_tx_data_V_keep_V {Type O LastRead -1 FirstWrite 1}
		m_axis_tx_data_V_last_V {Type O LastRead -1 FirstWrite 1}
		dhcpEnable_V {Type I LastRead 0 FirstWrite -1}
		inputIpAddress_V {Type I LastRead 0 FirstWrite -1}
		dhcpIpAddressOut_V {Type O LastRead -1 FirstWrite 1}
		myMacAddress_V {Type I LastRead 0 FirstWrite -1}
		openPortWaitTime_V {Type IO LastRead -1 FirstWrite -1}
		odp_listenDone {Type IO LastRead -1 FirstWrite -1}
		odp_waitListenStatus {Type IO LastRead -1 FirstWrite -1}
		portOpen_V_V {Type IO LastRead -1 FirstWrite -1}
		rm_wordCount_V {Type IO LastRead -1 FirstWrite -1}
		rm_isReply {Type IO LastRead -1 FirstWrite -1}
		rm_correctMac {Type IO LastRead -1 FirstWrite -1}
		rm_isDHCP {Type IO LastRead -1 FirstWrite -1}
		meta_identifier_V {Type IO LastRead -1 FirstWrite -1}
		meta_assignedIpAddre {Type IO LastRead -1 FirstWrite -1}
		meta_serverAddress_V {Type IO LastRead -1 FirstWrite -1}
		meta_type_V_1 {Type IO LastRead -1 FirstWrite -1}
		dhcp_replyMetaFifo_V {Type IO LastRead -1 FirstWrite -1}
		myIpAddress_V {Type IO LastRead -1 FirstWrite -1}
		state {Type IO LastRead -1 FirstWrite -1}
		time_V {Type IO LastRead -1 FirstWrite -1}
		randomValue_V {Type IO LastRead -1 FirstWrite -1}
		myIdentity_V {Type IO LastRead -1 FirstWrite -1}
		IpAddressBuffer_V {Type IO LastRead -1 FirstWrite -1}
		dhcp_requestMetaFifo_1 {Type IO LastRead -1 FirstWrite -1}
		sm_wordCount_V {Type IO LastRead -1 FirstWrite -1}
		meta_type_V {Type IO LastRead -1 FirstWrite -1}
		meta_requestedIpAddr {Type IO LastRead -1 FirstWrite -1}}
	open_dhcp_port {
		openPort_V_V {Type O LastRead 0 FirstWrite 0}
		confirmPortStatus_V {Type I LastRead 0 FirstWrite -1}
		openPortWaitTime_V {Type IO LastRead -1 FirstWrite -1}
		odp_listenDone {Type IO LastRead -1 FirstWrite -1}
		odp_waitListenStatus {Type IO LastRead -1 FirstWrite -1}
		portOpen_V_V {Type O LastRead -1 FirstWrite 1}}
	receive_message {
		dataInMeta_V_sourceS {Type I LastRead 0 FirstWrite -1}
		dataInMeta_V_destina {Type I LastRead 0 FirstWrite -1}
		dataIn_V_data_V {Type I LastRead 0 FirstWrite -1}
		dataIn_V_keep_V {Type I LastRead 0 FirstWrite -1}
		dataIn_V_last_V {Type I LastRead 0 FirstWrite -1}
		myMacAddress_V {Type I LastRead 0 FirstWrite -1}
		rm_wordCount_V {Type IO LastRead -1 FirstWrite -1}
		rm_isReply {Type IO LastRead -1 FirstWrite -1}
		rm_correctMac {Type IO LastRead -1 FirstWrite -1}
		rm_isDHCP {Type IO LastRead -1 FirstWrite -1}
		meta_identifier_V {Type IO LastRead -1 FirstWrite -1}
		meta_assignedIpAddre {Type IO LastRead -1 FirstWrite -1}
		meta_serverAddress_V {Type IO LastRead -1 FirstWrite -1}
		meta_type_V_1 {Type IO LastRead -1 FirstWrite -1}
		dhcp_replyMetaFifo_V {Type O LastRead -1 FirstWrite 2}}
	dhcp_fsm {
		ipAddressOut_V {Type O LastRead -1 FirstWrite 1}
		dhcpEnable_V {Type I LastRead 0 FirstWrite -1}
		inputIpAddress_V {Type I LastRead 0 FirstWrite -1}
		myIpAddress_V {Type IO LastRead -1 FirstWrite -1}
		state {Type IO LastRead -1 FirstWrite -1}
		time_V {Type IO LastRead -1 FirstWrite -1}
		randomValue_V {Type IO LastRead -1 FirstWrite -1}
		myIdentity_V {Type IO LastRead -1 FirstWrite -1}
		IpAddressBuffer_V {Type IO LastRead -1 FirstWrite -1}
		portOpen_V_V {Type I LastRead 0 FirstWrite -1}
		dhcp_requestMetaFifo_1 {Type O LastRead -1 FirstWrite 1}
		dhcp_replyMetaFifo_V {Type I LastRead 0 FirstWrite -1}}
	send_message {
		dataOutMeta_V {Type O LastRead -1 FirstWrite 1}
		dataOutLength_V_V {Type O LastRead -1 FirstWrite 1}
		dataOut_V_data_V {Type O LastRead -1 FirstWrite 1}
		dataOut_V_keep_V {Type O LastRead -1 FirstWrite 1}
		dataOut_V_last_V {Type O LastRead -1 FirstWrite 1}
		myMacAddress_V {Type I LastRead 0 FirstWrite -1}
		sm_wordCount_V {Type IO LastRead -1 FirstWrite -1}
		meta_type_V {Type IO LastRead -1 FirstWrite -1}
		meta_requestedIpAddr {Type IO LastRead -1 FirstWrite -1}
		dhcp_requestMetaFifo_1 {Type I LastRead 0 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "7", "Max" : "7"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
]}

set Spec2ImplPortList { 
	m_axis_open_port_V_V { axis {  { m_axis_open_port_V_V_TDATA out_data 1 16 }  { m_axis_open_port_V_V_TVALID out_vld 1 1 }  { m_axis_open_port_V_V_TREADY out_acc 0 1 } } }
	s_axis_open_port_status_V { axis {  { s_axis_open_port_status_V_TDATA in_data 0 8 }  { s_axis_open_port_status_V_TVALID in_vld 0 1 }  { s_axis_open_port_status_V_TREADY in_acc 1 1 } } }
	s_axis_rx_metadata_V_sourceSocket { axis {  { s_axis_rx_metadata_V_sourceSocket_TDATA in_data 0 48 }  { s_axis_rx_metadata_V_sourceSocket_TVALID in_vld 0 1 }  { s_axis_rx_metadata_V_sourceSocket_TREADY in_acc 1 1 } } }
	s_axis_rx_metadata_V_destinationSocket { axis {  { s_axis_rx_metadata_V_destinationSocket_TDATA in_data 0 48 }  { s_axis_rx_metadata_V_destinationSocket_TVALID in_vld 0 1 }  { s_axis_rx_metadata_V_destinationSocket_TREADY in_acc 1 1 } } }
	s_axis_rx_data_V_data_V { axis {  { s_axis_rx_data_TDATA in_data 0 64 } } }
	s_axis_rx_data_V_keep_V { axis {  { s_axis_rx_data_TKEEP in_data 0 8 } } }
	s_axis_rx_data_V_last_V { axis {  { s_axis_rx_data_TLAST in_data 0 1 }  { s_axis_rx_data_TVALID in_vld 0 1 }  { s_axis_rx_data_TREADY in_acc 1 1 } } }
	m_axis_tx_metadata_V { axis {  { m_axis_tx_metadata_V_TDATA out_data 1 96 }  { m_axis_tx_metadata_V_TVALID out_vld 1 1 }  { m_axis_tx_metadata_V_TREADY out_acc 0 1 } } }
	m_axis_tx_length_V_V { axis {  { m_axis_tx_length_V_V_TDATA out_data 1 16 }  { m_axis_tx_length_V_V_TVALID out_vld 1 1 }  { m_axis_tx_length_V_V_TREADY out_acc 0 1 } } }
	m_axis_tx_data_V_data_V { axis {  { m_axis_tx_data_TDATA out_data 1 64 } } }
	m_axis_tx_data_V_keep_V { axis {  { m_axis_tx_data_TKEEP out_data 1 8 } } }
	m_axis_tx_data_V_last_V { axis {  { m_axis_tx_data_TLAST out_data 1 1 }  { m_axis_tx_data_TVALID out_vld 1 1 }  { m_axis_tx_data_TREADY out_acc 0 1 } } }
	dhcpEnable_V { ap_stable {  { dhcpEnable_V in_data 0 1 } } }
	inputIpAddress_V { ap_stable {  { inputIpAddress_V in_data 0 32 } } }
	dhcpIpAddressOut_V { ap_vld {  { dhcpIpAddressOut_V out_data 1 32 }  { dhcpIpAddressOut_V_ap_vld out_vld 1 1 } } }
	myMacAddress_V { ap_stable {  { myMacAddress_V in_data 0 48 } } }
}

set busDeadlockParameterList { 
}

# RTL port scheduling information:
set fifoSchedulingInfoList { 
}

# RTL bus port read request latency information:
set busReadReqLatencyList { 
}

# RTL bus port write response latency information:
set busWriteResLatencyList { 
}

# RTL array port load latency information:
set memoryLoadLatencyList { 
}
