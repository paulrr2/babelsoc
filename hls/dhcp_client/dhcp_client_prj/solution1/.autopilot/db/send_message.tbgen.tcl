set moduleName send_message
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {send_message}
set C_modelType { void 0 }
set C_modelArgList {
	{ dataOutMeta_V int 96 regular {axi_s 1 volatile  { dataOutMeta_V Data } }  }
	{ dataOutLength_V_V int 16 regular {axi_s 1 volatile  { dataOutLength_V_V Data } }  }
	{ dataOut_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_tx_data Data } }  }
	{ dataOut_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_tx_data Keep } }  }
	{ dataOut_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_tx_data Last } }  }
	{ myMacAddress_V int 48 regular {pointer 0}  }
	{ dhcp_requestMetaFifo_1 int 72 regular {fifo 0 volatile } {global 0}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "dataOutMeta_V", "interface" : "axis", "bitwidth" : 96, "direction" : "WRITEONLY"} , 
 	{ "Name" : "dataOutLength_V_V", "interface" : "axis", "bitwidth" : 16, "direction" : "WRITEONLY"} , 
 	{ "Name" : "dataOut_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY"} , 
 	{ "Name" : "dataOut_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY"} , 
 	{ "Name" : "dataOut_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY"} , 
 	{ "Name" : "myMacAddress_V", "interface" : "wire", "bitwidth" : 48, "direction" : "READONLY"} , 
 	{ "Name" : "dhcp_requestMetaFifo_1", "interface" : "fifo", "bitwidth" : 72, "direction" : "READONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 22
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ dhcp_requestMetaFifo_1_dout sc_in sc_lv 72 signal 6 } 
	{ dhcp_requestMetaFifo_1_empty_n sc_in sc_logic 1 signal 6 } 
	{ dhcp_requestMetaFifo_1_read sc_out sc_logic 1 signal 6 } 
	{ m_axis_tx_data_TREADY sc_in sc_logic 1 outacc 4 } 
	{ dataOutMeta_V_TREADY sc_in sc_logic 1 outacc 0 } 
	{ dataOutLength_V_V_TREADY sc_in sc_logic 1 outacc 1 } 
	{ dataOutMeta_V_TDATA sc_out sc_lv 96 signal 0 } 
	{ dataOutMeta_V_TVALID sc_out sc_logic 1 outvld 0 } 
	{ dataOutLength_V_V_TDATA sc_out sc_lv 16 signal 1 } 
	{ dataOutLength_V_V_TVALID sc_out sc_logic 1 outvld 1 } 
	{ m_axis_tx_data_TDATA sc_out sc_lv 64 signal 2 } 
	{ m_axis_tx_data_TVALID sc_out sc_logic 1 outvld 4 } 
	{ m_axis_tx_data_TKEEP sc_out sc_lv 8 signal 3 } 
	{ m_axis_tx_data_TLAST sc_out sc_lv 1 signal 4 } 
	{ myMacAddress_V sc_in sc_lv 48 signal 5 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "dhcp_requestMetaFifo_1_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":72, "type": "signal", "bundle":{"name": "dhcp_requestMetaFifo_1", "role": "dout" }} , 
 	{ "name": "dhcp_requestMetaFifo_1_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dhcp_requestMetaFifo_1", "role": "empty_n" }} , 
 	{ "name": "dhcp_requestMetaFifo_1_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dhcp_requestMetaFifo_1", "role": "read" }} , 
 	{ "name": "m_axis_tx_data_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "dataOut_V_last_V", "role": "EADY" }} , 
 	{ "name": "dataOutMeta_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "dataOutMeta_V", "role": "TREADY" }} , 
 	{ "name": "dataOutLength_V_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "dataOutLength_V_V", "role": "TREADY" }} , 
 	{ "name": "dataOutMeta_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":96, "type": "signal", "bundle":{"name": "dataOutMeta_V", "role": "TDATA" }} , 
 	{ "name": "dataOutMeta_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "dataOutMeta_V", "role": "TVALID" }} , 
 	{ "name": "dataOutLength_V_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "dataOutLength_V_V", "role": "TDATA" }} , 
 	{ "name": "dataOutLength_V_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "dataOutLength_V_V", "role": "TVALID" }} , 
 	{ "name": "m_axis_tx_data_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "dataOut_V_data_V", "role": "ATA" }} , 
 	{ "name": "m_axis_tx_data_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "dataOut_V_last_V", "role": "ALID" }} , 
 	{ "name": "m_axis_tx_data_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "dataOut_V_keep_V", "role": "EEP" }} , 
 	{ "name": "m_axis_tx_data_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "dataOut_V_last_V", "role": "AST" }} , 
 	{ "name": "myMacAddress_V", "direction": "in", "datatype": "sc_lv", "bitwidth":48, "type": "signal", "bundle":{"name": "myMacAddress_V", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2", "3", "4", "5"],
		"CDFG" : "send_message",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "dataOutMeta_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "dataOutMeta_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataOutLength_V_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "dataOutLength_V_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataOut_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_tx_data_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataOut_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "dataOut_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "myMacAddress_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "sm_wordCount_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "meta_type_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "meta_requestedIpAddr", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "dhcp_requestMetaFifo_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "dhcp_requestMetaFifo_1_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_dataOutMeta_V_U", "Parent" : "0"},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_dataOutLength_V_V_U", "Parent" : "0"},
	{"ID" : "3", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_dataOut_V_data_V_U", "Parent" : "0"},
	{"ID" : "4", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_dataOut_V_keep_V_U", "Parent" : "0"},
	{"ID" : "5", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_dataOut_V_last_V_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	send_message {
		dataOutMeta_V {Type O LastRead -1 FirstWrite 1}
		dataOutLength_V_V {Type O LastRead -1 FirstWrite 1}
		dataOut_V_data_V {Type O LastRead -1 FirstWrite 1}
		dataOut_V_keep_V {Type O LastRead -1 FirstWrite 1}
		dataOut_V_last_V {Type O LastRead -1 FirstWrite 1}
		myMacAddress_V {Type I LastRead 0 FirstWrite -1}
		sm_wordCount_V {Type IO LastRead -1 FirstWrite -1}
		meta_type_V {Type IO LastRead -1 FirstWrite -1}
		meta_requestedIpAddr {Type IO LastRead -1 FirstWrite -1}
		dhcp_requestMetaFifo_1 {Type I LastRead 0 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "2", "Max" : "2"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	dataOutMeta_V { axis {  { dataOutMeta_V_TREADY out_acc 0 1 }  { dataOutMeta_V_TDATA out_data 1 96 }  { dataOutMeta_V_TVALID out_vld 1 1 } } }
	dataOutLength_V_V { axis {  { dataOutLength_V_V_TREADY out_acc 0 1 }  { dataOutLength_V_V_TDATA out_data 1 16 }  { dataOutLength_V_V_TVALID out_vld 1 1 } } }
	dataOut_V_data_V { axis {  { m_axis_tx_data_TDATA out_data 1 64 } } }
	dataOut_V_keep_V { axis {  { m_axis_tx_data_TKEEP out_data 1 8 } } }
	dataOut_V_last_V { axis {  { m_axis_tx_data_TREADY out_acc 0 1 }  { m_axis_tx_data_TVALID out_vld 1 1 }  { m_axis_tx_data_TLAST out_data 1 1 } } }
	myMacAddress_V { ap_stable {  { myMacAddress_V in_data 0 48 } } }
	dhcp_requestMetaFifo_1 { ap_fifo {  { dhcp_requestMetaFifo_1_dout fifo_data 0 72 }  { dhcp_requestMetaFifo_1_empty_n fifo_status 0 1 }  { dhcp_requestMetaFifo_1_read fifo_update 1 1 } } }
}
