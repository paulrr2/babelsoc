set moduleName receive_message
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {receive_message}
set C_modelType { void 0 }
set C_modelArgList {
	{ dataInMeta_V_sourceS int 48 regular {axi_s 0 volatile  { dataInMeta_V_sourceS Data } }  }
	{ dataInMeta_V_destina int 48 regular {axi_s 0 volatile  { dataInMeta_V_destina Data } }  }
	{ dataIn_V_data_V int 64 regular {axi_s 0 volatile  { s_axis_rx_data Data } }  }
	{ dataIn_V_keep_V int 8 regular {axi_s 0 volatile  { s_axis_rx_data Keep } }  }
	{ dataIn_V_last_V int 1 regular {axi_s 0 volatile  { s_axis_rx_data Last } }  }
	{ myMacAddress_V int 48 regular {pointer 0}  }
	{ dhcp_replyMetaFifo_V int 104 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "dataInMeta_V_sourceS", "interface" : "axis", "bitwidth" : 48, "direction" : "READONLY"} , 
 	{ "Name" : "dataInMeta_V_destina", "interface" : "axis", "bitwidth" : 48, "direction" : "READONLY"} , 
 	{ "Name" : "dataIn_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "READONLY"} , 
 	{ "Name" : "dataIn_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "dataIn_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY"} , 
 	{ "Name" : "myMacAddress_V", "interface" : "wire", "bitwidth" : 48, "direction" : "READONLY"} , 
 	{ "Name" : "dhcp_replyMetaFifo_V", "interface" : "fifo", "bitwidth" : 104, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 22
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ s_axis_rx_data_TVALID sc_in sc_logic 1 invld 2 } 
	{ dataInMeta_V_sourceS_TVALID sc_in sc_logic 1 invld 0 } 
	{ dataInMeta_V_destina_TVALID sc_in sc_logic 1 invld 1 } 
	{ dhcp_replyMetaFifo_V_din sc_out sc_lv 104 signal 6 } 
	{ dhcp_replyMetaFifo_V_full_n sc_in sc_logic 1 signal 6 } 
	{ dhcp_replyMetaFifo_V_write sc_out sc_logic 1 signal 6 } 
	{ dataInMeta_V_sourceS_TDATA sc_in sc_lv 48 signal 0 } 
	{ dataInMeta_V_sourceS_TREADY sc_out sc_logic 1 inacc 0 } 
	{ dataInMeta_V_destina_TDATA sc_in sc_lv 48 signal 1 } 
	{ dataInMeta_V_destina_TREADY sc_out sc_logic 1 inacc 1 } 
	{ s_axis_rx_data_TDATA sc_in sc_lv 64 signal 2 } 
	{ s_axis_rx_data_TREADY sc_out sc_logic 1 inacc 4 } 
	{ s_axis_rx_data_TKEEP sc_in sc_lv 8 signal 3 } 
	{ s_axis_rx_data_TLAST sc_in sc_lv 1 signal 4 } 
	{ myMacAddress_V sc_in sc_lv 48 signal 5 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "s_axis_rx_data_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "dataIn_V_data_V", "role": "VALID" }} , 
 	{ "name": "dataInMeta_V_sourceS_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "dataInMeta_V_sourceS", "role": "TVALID" }} , 
 	{ "name": "dataInMeta_V_destina_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "dataInMeta_V_destina", "role": "TVALID" }} , 
 	{ "name": "dhcp_replyMetaFifo_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":104, "type": "signal", "bundle":{"name": "dhcp_replyMetaFifo_V", "role": "din" }} , 
 	{ "name": "dhcp_replyMetaFifo_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dhcp_replyMetaFifo_V", "role": "full_n" }} , 
 	{ "name": "dhcp_replyMetaFifo_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dhcp_replyMetaFifo_V", "role": "write" }} , 
 	{ "name": "dataInMeta_V_sourceS_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":48, "type": "signal", "bundle":{"name": "dataInMeta_V_sourceS", "role": "TDATA" }} , 
 	{ "name": "dataInMeta_V_sourceS_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "dataInMeta_V_sourceS", "role": "TREADY" }} , 
 	{ "name": "dataInMeta_V_destina_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":48, "type": "signal", "bundle":{"name": "dataInMeta_V_destina", "role": "TDATA" }} , 
 	{ "name": "dataInMeta_V_destina_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "dataInMeta_V_destina", "role": "TREADY" }} , 
 	{ "name": "s_axis_rx_data_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "dataIn_V_data_V", "role": "DATA" }} , 
 	{ "name": "s_axis_rx_data_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "dataIn_V_last_V", "role": "READY" }} , 
 	{ "name": "s_axis_rx_data_TKEEP", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "dataIn_V_keep_V", "role": "KEEP" }} , 
 	{ "name": "s_axis_rx_data_TLAST", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "dataIn_V_last_V", "role": "LAST" }} , 
 	{ "name": "myMacAddress_V", "direction": "in", "datatype": "sc_lv", "bitwidth":48, "type": "signal", "bundle":{"name": "myMacAddress_V", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "receive_message",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "dataInMeta_V_sourceS", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "dataInMeta_V_sourceS_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataInMeta_V_destina", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "dataInMeta_V_destina_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataIn_V_data_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "s_axis_rx_data_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataIn_V_keep_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "dataIn_V_last_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "myMacAddress_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "rm_wordCount_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "rm_isReply", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "rm_correctMac", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "rm_isDHCP", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "meta_identifier_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "meta_assignedIpAddre", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "meta_serverAddress_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "meta_type_V_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "dhcp_replyMetaFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "dhcp_replyMetaFifo_V_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	receive_message {
		dataInMeta_V_sourceS {Type I LastRead 0 FirstWrite -1}
		dataInMeta_V_destina {Type I LastRead 0 FirstWrite -1}
		dataIn_V_data_V {Type I LastRead 0 FirstWrite -1}
		dataIn_V_keep_V {Type I LastRead 0 FirstWrite -1}
		dataIn_V_last_V {Type I LastRead 0 FirstWrite -1}
		myMacAddress_V {Type I LastRead 0 FirstWrite -1}
		rm_wordCount_V {Type IO LastRead -1 FirstWrite -1}
		rm_isReply {Type IO LastRead -1 FirstWrite -1}
		rm_correctMac {Type IO LastRead -1 FirstWrite -1}
		rm_isDHCP {Type IO LastRead -1 FirstWrite -1}
		meta_identifier_V {Type IO LastRead -1 FirstWrite -1}
		meta_assignedIpAddre {Type IO LastRead -1 FirstWrite -1}
		meta_serverAddress_V {Type IO LastRead -1 FirstWrite -1}
		meta_type_V_1 {Type IO LastRead -1 FirstWrite -1}
		dhcp_replyMetaFifo_V {Type O LastRead -1 FirstWrite 2}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "2", "Max" : "2"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	dataInMeta_V_sourceS { axis {  { dataInMeta_V_sourceS_TVALID in_vld 0 1 }  { dataInMeta_V_sourceS_TDATA in_data 0 48 }  { dataInMeta_V_sourceS_TREADY in_acc 1 1 } } }
	dataInMeta_V_destina { axis {  { dataInMeta_V_destina_TVALID in_vld 0 1 }  { dataInMeta_V_destina_TDATA in_data 0 48 }  { dataInMeta_V_destina_TREADY in_acc 1 1 } } }
	dataIn_V_data_V { axis {  { s_axis_rx_data_TVALID in_vld 0 1 }  { s_axis_rx_data_TDATA in_data 0 64 } } }
	dataIn_V_keep_V { axis {  { s_axis_rx_data_TKEEP in_data 0 8 } } }
	dataIn_V_last_V { axis {  { s_axis_rx_data_TREADY in_acc 1 1 }  { s_axis_rx_data_TLAST in_data 0 1 } } }
	myMacAddress_V { ap_stable {  { myMacAddress_V in_data 0 48 } } }
	dhcp_replyMetaFifo_V { ap_fifo {  { dhcp_replyMetaFifo_V_din fifo_data 1 104 }  { dhcp_replyMetaFifo_V_full_n fifo_status 0 1 }  { dhcp_replyMetaFifo_V_write fifo_update 1 1 } } }
}
