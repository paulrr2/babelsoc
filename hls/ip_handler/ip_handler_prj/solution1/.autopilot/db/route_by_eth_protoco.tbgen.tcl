set moduleName route_by_eth_protoco
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {route_by_eth_protoco}
set C_modelType { void 0 }
set C_modelArgList {
	{ ARPdataOut_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_arp Data } }  }
	{ ARPdataOut_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_arp Keep } }  }
	{ ARPdataOut_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_arp Last } }  }
	{ etherTypeFifo_V_V int 16 regular {fifo 0 volatile } {global 0}  }
	{ ethDataFifo_V_data_V int 64 regular {fifo 0 volatile } {global 0}  }
	{ ethDataFifo_V_keep_V int 8 regular {fifo 0 volatile } {global 0}  }
	{ ethDataFifo_V_last_V int 1 regular {fifo 0 volatile } {global 0}  }
	{ ipv4ShiftFifo_V_data int 64 regular {fifo 1 volatile } {global 1}  }
	{ ipv4ShiftFifo_V_keep int 8 regular {fifo 1 volatile } {global 1}  }
	{ ipv4ShiftFifo_V_last int 1 regular {fifo 1 volatile } {global 1}  }
	{ ipv6ShiftFifo_V_data int 64 regular {fifo 1 volatile } {global 1}  }
	{ ipv6ShiftFifo_V_keep int 8 regular {fifo 1 volatile } {global 1}  }
	{ ipv6ShiftFifo_V_last int 1 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "ARPdataOut_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY"} , 
 	{ "Name" : "ARPdataOut_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY"} , 
 	{ "Name" : "ARPdataOut_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY"} , 
 	{ "Name" : "etherTypeFifo_V_V", "interface" : "fifo", "bitwidth" : 16, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "ethDataFifo_V_data_V", "interface" : "fifo", "bitwidth" : 64, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "ethDataFifo_V_keep_V", "interface" : "fifo", "bitwidth" : 8, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "ethDataFifo_V_last_V", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "ipv4ShiftFifo_V_data", "interface" : "fifo", "bitwidth" : 64, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "ipv4ShiftFifo_V_keep", "interface" : "fifo", "bitwidth" : 8, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "ipv4ShiftFifo_V_last", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "ipv6ShiftFifo_V_data", "interface" : "fifo", "bitwidth" : 64, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "ipv6ShiftFifo_V_keep", "interface" : "fifo", "bitwidth" : 8, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "ipv6ShiftFifo_V_last", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 42
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ etherTypeFifo_V_V_dout sc_in sc_lv 16 signal 3 } 
	{ etherTypeFifo_V_V_empty_n sc_in sc_logic 1 signal 3 } 
	{ etherTypeFifo_V_V_read sc_out sc_logic 1 signal 3 } 
	{ ethDataFifo_V_data_V_dout sc_in sc_lv 64 signal 4 } 
	{ ethDataFifo_V_data_V_empty_n sc_in sc_logic 1 signal 4 } 
	{ ethDataFifo_V_data_V_read sc_out sc_logic 1 signal 4 } 
	{ ethDataFifo_V_keep_V_dout sc_in sc_lv 8 signal 5 } 
	{ ethDataFifo_V_keep_V_empty_n sc_in sc_logic 1 signal 5 } 
	{ ethDataFifo_V_keep_V_read sc_out sc_logic 1 signal 5 } 
	{ ethDataFifo_V_last_V_dout sc_in sc_lv 1 signal 6 } 
	{ ethDataFifo_V_last_V_empty_n sc_in sc_logic 1 signal 6 } 
	{ ethDataFifo_V_last_V_read sc_out sc_logic 1 signal 6 } 
	{ ipv6ShiftFifo_V_data_din sc_out sc_lv 64 signal 10 } 
	{ ipv6ShiftFifo_V_data_full_n sc_in sc_logic 1 signal 10 } 
	{ ipv6ShiftFifo_V_data_write sc_out sc_logic 1 signal 10 } 
	{ ipv6ShiftFifo_V_keep_din sc_out sc_lv 8 signal 11 } 
	{ ipv6ShiftFifo_V_keep_full_n sc_in sc_logic 1 signal 11 } 
	{ ipv6ShiftFifo_V_keep_write sc_out sc_logic 1 signal 11 } 
	{ ipv6ShiftFifo_V_last_din sc_out sc_lv 1 signal 12 } 
	{ ipv6ShiftFifo_V_last_full_n sc_in sc_logic 1 signal 12 } 
	{ ipv6ShiftFifo_V_last_write sc_out sc_logic 1 signal 12 } 
	{ ipv4ShiftFifo_V_data_din sc_out sc_lv 64 signal 7 } 
	{ ipv4ShiftFifo_V_data_full_n sc_in sc_logic 1 signal 7 } 
	{ ipv4ShiftFifo_V_data_write sc_out sc_logic 1 signal 7 } 
	{ ipv4ShiftFifo_V_keep_din sc_out sc_lv 8 signal 8 } 
	{ ipv4ShiftFifo_V_keep_full_n sc_in sc_logic 1 signal 8 } 
	{ ipv4ShiftFifo_V_keep_write sc_out sc_logic 1 signal 8 } 
	{ ipv4ShiftFifo_V_last_din sc_out sc_lv 1 signal 9 } 
	{ ipv4ShiftFifo_V_last_full_n sc_in sc_logic 1 signal 9 } 
	{ ipv4ShiftFifo_V_last_write sc_out sc_logic 1 signal 9 } 
	{ m_axis_arp_TREADY sc_in sc_logic 1 outacc 2 } 
	{ m_axis_arp_TDATA sc_out sc_lv 64 signal 0 } 
	{ m_axis_arp_TVALID sc_out sc_logic 1 outvld 2 } 
	{ m_axis_arp_TKEEP sc_out sc_lv 8 signal 1 } 
	{ m_axis_arp_TLAST sc_out sc_lv 1 signal 2 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "etherTypeFifo_V_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "etherTypeFifo_V_V", "role": "dout" }} , 
 	{ "name": "etherTypeFifo_V_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "etherTypeFifo_V_V", "role": "empty_n" }} , 
 	{ "name": "etherTypeFifo_V_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "etherTypeFifo_V_V", "role": "read" }} , 
 	{ "name": "ethDataFifo_V_data_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "ethDataFifo_V_data_V", "role": "dout" }} , 
 	{ "name": "ethDataFifo_V_data_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ethDataFifo_V_data_V", "role": "empty_n" }} , 
 	{ "name": "ethDataFifo_V_data_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ethDataFifo_V_data_V", "role": "read" }} , 
 	{ "name": "ethDataFifo_V_keep_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "ethDataFifo_V_keep_V", "role": "dout" }} , 
 	{ "name": "ethDataFifo_V_keep_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ethDataFifo_V_keep_V", "role": "empty_n" }} , 
 	{ "name": "ethDataFifo_V_keep_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ethDataFifo_V_keep_V", "role": "read" }} , 
 	{ "name": "ethDataFifo_V_last_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "ethDataFifo_V_last_V", "role": "dout" }} , 
 	{ "name": "ethDataFifo_V_last_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ethDataFifo_V_last_V", "role": "empty_n" }} , 
 	{ "name": "ethDataFifo_V_last_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ethDataFifo_V_last_V", "role": "read" }} , 
 	{ "name": "ipv6ShiftFifo_V_data_din", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "ipv6ShiftFifo_V_data", "role": "din" }} , 
 	{ "name": "ipv6ShiftFifo_V_data_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv6ShiftFifo_V_data", "role": "full_n" }} , 
 	{ "name": "ipv6ShiftFifo_V_data_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv6ShiftFifo_V_data", "role": "write" }} , 
 	{ "name": "ipv6ShiftFifo_V_keep_din", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "ipv6ShiftFifo_V_keep", "role": "din" }} , 
 	{ "name": "ipv6ShiftFifo_V_keep_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv6ShiftFifo_V_keep", "role": "full_n" }} , 
 	{ "name": "ipv6ShiftFifo_V_keep_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv6ShiftFifo_V_keep", "role": "write" }} , 
 	{ "name": "ipv6ShiftFifo_V_last_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv6ShiftFifo_V_last", "role": "din" }} , 
 	{ "name": "ipv6ShiftFifo_V_last_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv6ShiftFifo_V_last", "role": "full_n" }} , 
 	{ "name": "ipv6ShiftFifo_V_last_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv6ShiftFifo_V_last", "role": "write" }} , 
 	{ "name": "ipv4ShiftFifo_V_data_din", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "ipv4ShiftFifo_V_data", "role": "din" }} , 
 	{ "name": "ipv4ShiftFifo_V_data_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv4ShiftFifo_V_data", "role": "full_n" }} , 
 	{ "name": "ipv4ShiftFifo_V_data_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv4ShiftFifo_V_data", "role": "write" }} , 
 	{ "name": "ipv4ShiftFifo_V_keep_din", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "ipv4ShiftFifo_V_keep", "role": "din" }} , 
 	{ "name": "ipv4ShiftFifo_V_keep_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv4ShiftFifo_V_keep", "role": "full_n" }} , 
 	{ "name": "ipv4ShiftFifo_V_keep_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv4ShiftFifo_V_keep", "role": "write" }} , 
 	{ "name": "ipv4ShiftFifo_V_last_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv4ShiftFifo_V_last", "role": "din" }} , 
 	{ "name": "ipv4ShiftFifo_V_last_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv4ShiftFifo_V_last", "role": "full_n" }} , 
 	{ "name": "ipv4ShiftFifo_V_last_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv4ShiftFifo_V_last", "role": "write" }} , 
 	{ "name": "m_axis_arp_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "ARPdataOut_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_arp_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "ARPdataOut_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_arp_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "ARPdataOut_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_arp_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "ARPdataOut_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_arp_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "ARPdataOut_V_last_V", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2", "3"],
		"CDFG" : "route_by_eth_protoco",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "ARPdataOut_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_arp_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ARPdataOut_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "ARPdataOut_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "rep_fsmState_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "rep_etherType_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "etherTypeFifo_V_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "etherTypeFifo_V_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ethDataFifo_V_data_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ethDataFifo_V_data_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ethDataFifo_V_keep_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ethDataFifo_V_keep_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ethDataFifo_V_last_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ethDataFifo_V_last_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv4ShiftFifo_V_data", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipv4ShiftFifo_V_data_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv4ShiftFifo_V_keep", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipv4ShiftFifo_V_keep_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv4ShiftFifo_V_last", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipv4ShiftFifo_V_last_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv6ShiftFifo_V_data", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipv6ShiftFifo_V_data_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv6ShiftFifo_V_keep", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipv6ShiftFifo_V_keep_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv6ShiftFifo_V_last", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipv6ShiftFifo_V_last_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_ARPdataOut_V_data_V_U", "Parent" : "0"},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_ARPdataOut_V_keep_V_U", "Parent" : "0"},
	{"ID" : "3", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_ARPdataOut_V_last_V_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	route_by_eth_protoco {
		ARPdataOut_V_data_V {Type O LastRead -1 FirstWrite 1}
		ARPdataOut_V_keep_V {Type O LastRead -1 FirstWrite 1}
		ARPdataOut_V_last_V {Type O LastRead -1 FirstWrite 1}
		rep_fsmState_V {Type IO LastRead -1 FirstWrite -1}
		rep_etherType_V {Type IO LastRead -1 FirstWrite -1}
		etherTypeFifo_V_V {Type I LastRead 0 FirstWrite -1}
		ethDataFifo_V_data_V {Type I LastRead 0 FirstWrite -1}
		ethDataFifo_V_keep_V {Type I LastRead 0 FirstWrite -1}
		ethDataFifo_V_last_V {Type I LastRead 0 FirstWrite -1}
		ipv4ShiftFifo_V_data {Type O LastRead -1 FirstWrite 1}
		ipv4ShiftFifo_V_keep {Type O LastRead -1 FirstWrite 1}
		ipv4ShiftFifo_V_last {Type O LastRead -1 FirstWrite 1}
		ipv6ShiftFifo_V_data {Type O LastRead -1 FirstWrite 1}
		ipv6ShiftFifo_V_keep {Type O LastRead -1 FirstWrite 1}
		ipv6ShiftFifo_V_last {Type O LastRead -1 FirstWrite 1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "2", "Max" : "2"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	ARPdataOut_V_data_V { axis {  { m_axis_arp_TDATA out_data 1 64 } } }
	ARPdataOut_V_keep_V { axis {  { m_axis_arp_TKEEP out_data 1 8 } } }
	ARPdataOut_V_last_V { axis {  { m_axis_arp_TREADY out_acc 0 1 }  { m_axis_arp_TVALID out_vld 1 1 }  { m_axis_arp_TLAST out_data 1 1 } } }
	etherTypeFifo_V_V { ap_fifo {  { etherTypeFifo_V_V_dout fifo_data 0 16 }  { etherTypeFifo_V_V_empty_n fifo_status 0 1 }  { etherTypeFifo_V_V_read fifo_update 1 1 } } }
	ethDataFifo_V_data_V { ap_fifo {  { ethDataFifo_V_data_V_dout fifo_data 0 64 }  { ethDataFifo_V_data_V_empty_n fifo_status 0 1 }  { ethDataFifo_V_data_V_read fifo_update 1 1 } } }
	ethDataFifo_V_keep_V { ap_fifo {  { ethDataFifo_V_keep_V_dout fifo_data 0 8 }  { ethDataFifo_V_keep_V_empty_n fifo_status 0 1 }  { ethDataFifo_V_keep_V_read fifo_update 1 1 } } }
	ethDataFifo_V_last_V { ap_fifo {  { ethDataFifo_V_last_V_dout fifo_data 0 1 }  { ethDataFifo_V_last_V_empty_n fifo_status 0 1 }  { ethDataFifo_V_last_V_read fifo_update 1 1 } } }
	ipv4ShiftFifo_V_data { ap_fifo {  { ipv4ShiftFifo_V_data_din fifo_data 1 64 }  { ipv4ShiftFifo_V_data_full_n fifo_status 0 1 }  { ipv4ShiftFifo_V_data_write fifo_update 1 1 } } }
	ipv4ShiftFifo_V_keep { ap_fifo {  { ipv4ShiftFifo_V_keep_din fifo_data 1 8 }  { ipv4ShiftFifo_V_keep_full_n fifo_status 0 1 }  { ipv4ShiftFifo_V_keep_write fifo_update 1 1 } } }
	ipv4ShiftFifo_V_last { ap_fifo {  { ipv4ShiftFifo_V_last_din fifo_data 1 1 }  { ipv4ShiftFifo_V_last_full_n fifo_status 0 1 }  { ipv4ShiftFifo_V_last_write fifo_update 1 1 } } }
	ipv6ShiftFifo_V_data { ap_fifo {  { ipv6ShiftFifo_V_data_din fifo_data 1 64 }  { ipv6ShiftFifo_V_data_full_n fifo_status 0 1 }  { ipv6ShiftFifo_V_data_write fifo_update 1 1 } } }
	ipv6ShiftFifo_V_keep { ap_fifo {  { ipv6ShiftFifo_V_keep_din fifo_data 1 8 }  { ipv6ShiftFifo_V_keep_full_n fifo_status 0 1 }  { ipv6ShiftFifo_V_keep_write fifo_update 1 1 } } }
	ipv6ShiftFifo_V_last { ap_fifo {  { ipv6ShiftFifo_V_last_din fifo_data 1 1 }  { ipv6ShiftFifo_V_last_full_n fifo_status 0 1 }  { ipv6ShiftFifo_V_last_write fifo_update 1 1 } } }
}
set moduleName route_by_eth_protoco
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {route_by_eth_protoco}
set C_modelType { void 0 }
set C_modelArgList {
	{ ARPdataOut_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_arp Data } }  }
	{ ARPdataOut_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_arp Keep } }  }
	{ ARPdataOut_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_arp Last } }  }
	{ etherTypeFifo_V_V int 16 regular {fifo 0 volatile } {global 0}  }
	{ ethDataFifo_V_data_V int 64 regular {fifo 0 volatile } {global 0}  }
	{ ethDataFifo_V_keep_V int 8 regular {fifo 0 volatile } {global 0}  }
	{ ethDataFifo_V_last_V int 1 regular {fifo 0 volatile } {global 0}  }
	{ ipv4ShiftFifo_V_data int 64 regular {fifo 1 volatile } {global 1}  }
	{ ipv4ShiftFifo_V_keep int 8 regular {fifo 1 volatile } {global 1}  }
	{ ipv4ShiftFifo_V_last int 1 regular {fifo 1 volatile } {global 1}  }
	{ ipv6ShiftFifo_V_data int 64 regular {fifo 1 volatile } {global 1}  }
	{ ipv6ShiftFifo_V_keep int 8 regular {fifo 1 volatile } {global 1}  }
	{ ipv6ShiftFifo_V_last int 1 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "ARPdataOut_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY"} , 
 	{ "Name" : "ARPdataOut_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY"} , 
 	{ "Name" : "ARPdataOut_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY"} , 
 	{ "Name" : "etherTypeFifo_V_V", "interface" : "fifo", "bitwidth" : 16, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "ethDataFifo_V_data_V", "interface" : "fifo", "bitwidth" : 64, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "ethDataFifo_V_keep_V", "interface" : "fifo", "bitwidth" : 8, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "ethDataFifo_V_last_V", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "ipv4ShiftFifo_V_data", "interface" : "fifo", "bitwidth" : 64, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "ipv4ShiftFifo_V_keep", "interface" : "fifo", "bitwidth" : 8, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "ipv4ShiftFifo_V_last", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "ipv6ShiftFifo_V_data", "interface" : "fifo", "bitwidth" : 64, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "ipv6ShiftFifo_V_keep", "interface" : "fifo", "bitwidth" : 8, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "ipv6ShiftFifo_V_last", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 42
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ etherTypeFifo_V_V_dout sc_in sc_lv 16 signal 3 } 
	{ etherTypeFifo_V_V_empty_n sc_in sc_logic 1 signal 3 } 
	{ etherTypeFifo_V_V_read sc_out sc_logic 1 signal 3 } 
	{ ethDataFifo_V_data_V_dout sc_in sc_lv 64 signal 4 } 
	{ ethDataFifo_V_data_V_empty_n sc_in sc_logic 1 signal 4 } 
	{ ethDataFifo_V_data_V_read sc_out sc_logic 1 signal 4 } 
	{ ethDataFifo_V_keep_V_dout sc_in sc_lv 8 signal 5 } 
	{ ethDataFifo_V_keep_V_empty_n sc_in sc_logic 1 signal 5 } 
	{ ethDataFifo_V_keep_V_read sc_out sc_logic 1 signal 5 } 
	{ ethDataFifo_V_last_V_dout sc_in sc_lv 1 signal 6 } 
	{ ethDataFifo_V_last_V_empty_n sc_in sc_logic 1 signal 6 } 
	{ ethDataFifo_V_last_V_read sc_out sc_logic 1 signal 6 } 
	{ ipv6ShiftFifo_V_data_din sc_out sc_lv 64 signal 10 } 
	{ ipv6ShiftFifo_V_data_full_n sc_in sc_logic 1 signal 10 } 
	{ ipv6ShiftFifo_V_data_write sc_out sc_logic 1 signal 10 } 
	{ ipv6ShiftFifo_V_keep_din sc_out sc_lv 8 signal 11 } 
	{ ipv6ShiftFifo_V_keep_full_n sc_in sc_logic 1 signal 11 } 
	{ ipv6ShiftFifo_V_keep_write sc_out sc_logic 1 signal 11 } 
	{ ipv6ShiftFifo_V_last_din sc_out sc_lv 1 signal 12 } 
	{ ipv6ShiftFifo_V_last_full_n sc_in sc_logic 1 signal 12 } 
	{ ipv6ShiftFifo_V_last_write sc_out sc_logic 1 signal 12 } 
	{ ipv4ShiftFifo_V_data_din sc_out sc_lv 64 signal 7 } 
	{ ipv4ShiftFifo_V_data_full_n sc_in sc_logic 1 signal 7 } 
	{ ipv4ShiftFifo_V_data_write sc_out sc_logic 1 signal 7 } 
	{ ipv4ShiftFifo_V_keep_din sc_out sc_lv 8 signal 8 } 
	{ ipv4ShiftFifo_V_keep_full_n sc_in sc_logic 1 signal 8 } 
	{ ipv4ShiftFifo_V_keep_write sc_out sc_logic 1 signal 8 } 
	{ ipv4ShiftFifo_V_last_din sc_out sc_lv 1 signal 9 } 
	{ ipv4ShiftFifo_V_last_full_n sc_in sc_logic 1 signal 9 } 
	{ ipv4ShiftFifo_V_last_write sc_out sc_logic 1 signal 9 } 
	{ m_axis_arp_TREADY sc_in sc_logic 1 outacc 2 } 
	{ m_axis_arp_TDATA sc_out sc_lv 64 signal 0 } 
	{ m_axis_arp_TVALID sc_out sc_logic 1 outvld 2 } 
	{ m_axis_arp_TKEEP sc_out sc_lv 8 signal 1 } 
	{ m_axis_arp_TLAST sc_out sc_lv 1 signal 2 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "etherTypeFifo_V_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "etherTypeFifo_V_V", "role": "dout" }} , 
 	{ "name": "etherTypeFifo_V_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "etherTypeFifo_V_V", "role": "empty_n" }} , 
 	{ "name": "etherTypeFifo_V_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "etherTypeFifo_V_V", "role": "read" }} , 
 	{ "name": "ethDataFifo_V_data_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "ethDataFifo_V_data_V", "role": "dout" }} , 
 	{ "name": "ethDataFifo_V_data_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ethDataFifo_V_data_V", "role": "empty_n" }} , 
 	{ "name": "ethDataFifo_V_data_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ethDataFifo_V_data_V", "role": "read" }} , 
 	{ "name": "ethDataFifo_V_keep_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "ethDataFifo_V_keep_V", "role": "dout" }} , 
 	{ "name": "ethDataFifo_V_keep_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ethDataFifo_V_keep_V", "role": "empty_n" }} , 
 	{ "name": "ethDataFifo_V_keep_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ethDataFifo_V_keep_V", "role": "read" }} , 
 	{ "name": "ethDataFifo_V_last_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "ethDataFifo_V_last_V", "role": "dout" }} , 
 	{ "name": "ethDataFifo_V_last_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ethDataFifo_V_last_V", "role": "empty_n" }} , 
 	{ "name": "ethDataFifo_V_last_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ethDataFifo_V_last_V", "role": "read" }} , 
 	{ "name": "ipv6ShiftFifo_V_data_din", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "ipv6ShiftFifo_V_data", "role": "din" }} , 
 	{ "name": "ipv6ShiftFifo_V_data_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv6ShiftFifo_V_data", "role": "full_n" }} , 
 	{ "name": "ipv6ShiftFifo_V_data_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv6ShiftFifo_V_data", "role": "write" }} , 
 	{ "name": "ipv6ShiftFifo_V_keep_din", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "ipv6ShiftFifo_V_keep", "role": "din" }} , 
 	{ "name": "ipv6ShiftFifo_V_keep_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv6ShiftFifo_V_keep", "role": "full_n" }} , 
 	{ "name": "ipv6ShiftFifo_V_keep_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv6ShiftFifo_V_keep", "role": "write" }} , 
 	{ "name": "ipv6ShiftFifo_V_last_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv6ShiftFifo_V_last", "role": "din" }} , 
 	{ "name": "ipv6ShiftFifo_V_last_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv6ShiftFifo_V_last", "role": "full_n" }} , 
 	{ "name": "ipv6ShiftFifo_V_last_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv6ShiftFifo_V_last", "role": "write" }} , 
 	{ "name": "ipv4ShiftFifo_V_data_din", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "ipv4ShiftFifo_V_data", "role": "din" }} , 
 	{ "name": "ipv4ShiftFifo_V_data_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv4ShiftFifo_V_data", "role": "full_n" }} , 
 	{ "name": "ipv4ShiftFifo_V_data_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv4ShiftFifo_V_data", "role": "write" }} , 
 	{ "name": "ipv4ShiftFifo_V_keep_din", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "ipv4ShiftFifo_V_keep", "role": "din" }} , 
 	{ "name": "ipv4ShiftFifo_V_keep_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv4ShiftFifo_V_keep", "role": "full_n" }} , 
 	{ "name": "ipv4ShiftFifo_V_keep_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv4ShiftFifo_V_keep", "role": "write" }} , 
 	{ "name": "ipv4ShiftFifo_V_last_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv4ShiftFifo_V_last", "role": "din" }} , 
 	{ "name": "ipv4ShiftFifo_V_last_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv4ShiftFifo_V_last", "role": "full_n" }} , 
 	{ "name": "ipv4ShiftFifo_V_last_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv4ShiftFifo_V_last", "role": "write" }} , 
 	{ "name": "m_axis_arp_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "ARPdataOut_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_arp_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "ARPdataOut_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_arp_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "ARPdataOut_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_arp_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "ARPdataOut_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_arp_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "ARPdataOut_V_last_V", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "route_by_eth_protoco",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "ARPdataOut_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_arp_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ARPdataOut_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "ARPdataOut_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "rep_fsmState_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "rep_etherType_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "etherTypeFifo_V_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "etherTypeFifo_V_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ethDataFifo_V_data_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ethDataFifo_V_data_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ethDataFifo_V_keep_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ethDataFifo_V_keep_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ethDataFifo_V_last_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ethDataFifo_V_last_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv4ShiftFifo_V_data", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipv4ShiftFifo_V_data_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv4ShiftFifo_V_keep", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipv4ShiftFifo_V_keep_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv4ShiftFifo_V_last", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipv4ShiftFifo_V_last_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv6ShiftFifo_V_data", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipv6ShiftFifo_V_data_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv6ShiftFifo_V_keep", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipv6ShiftFifo_V_keep_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv6ShiftFifo_V_last", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipv6ShiftFifo_V_last_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	route_by_eth_protoco {
		ARPdataOut_V_data_V {Type O LastRead -1 FirstWrite 1}
		ARPdataOut_V_keep_V {Type O LastRead -1 FirstWrite 1}
		ARPdataOut_V_last_V {Type O LastRead -1 FirstWrite 1}
		rep_fsmState_V {Type IO LastRead -1 FirstWrite -1}
		rep_etherType_V {Type IO LastRead -1 FirstWrite -1}
		etherTypeFifo_V_V {Type I LastRead 0 FirstWrite -1}
		ethDataFifo_V_data_V {Type I LastRead 0 FirstWrite -1}
		ethDataFifo_V_keep_V {Type I LastRead 0 FirstWrite -1}
		ethDataFifo_V_last_V {Type I LastRead 0 FirstWrite -1}
		ipv4ShiftFifo_V_data {Type O LastRead -1 FirstWrite 1}
		ipv4ShiftFifo_V_keep {Type O LastRead -1 FirstWrite 1}
		ipv4ShiftFifo_V_last {Type O LastRead -1 FirstWrite 1}
		ipv6ShiftFifo_V_data {Type O LastRead -1 FirstWrite 1}
		ipv6ShiftFifo_V_keep {Type O LastRead -1 FirstWrite 1}
		ipv6ShiftFifo_V_last {Type O LastRead -1 FirstWrite 1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "2", "Max" : "2"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	ARPdataOut_V_data_V { axis {  { m_axis_arp_TDATA out_data 1 64 } } }
	ARPdataOut_V_keep_V { axis {  { m_axis_arp_TKEEP out_data 1 8 } } }
	ARPdataOut_V_last_V { axis {  { m_axis_arp_TREADY out_acc 0 1 }  { m_axis_arp_TVALID out_vld 1 1 }  { m_axis_arp_TLAST out_data 1 1 } } }
	etherTypeFifo_V_V { ap_fifo {  { etherTypeFifo_V_V_dout fifo_data 0 16 }  { etherTypeFifo_V_V_empty_n fifo_status 0 1 }  { etherTypeFifo_V_V_read fifo_update 1 1 } } }
	ethDataFifo_V_data_V { ap_fifo {  { ethDataFifo_V_data_V_dout fifo_data 0 64 }  { ethDataFifo_V_data_V_empty_n fifo_status 0 1 }  { ethDataFifo_V_data_V_read fifo_update 1 1 } } }
	ethDataFifo_V_keep_V { ap_fifo {  { ethDataFifo_V_keep_V_dout fifo_data 0 8 }  { ethDataFifo_V_keep_V_empty_n fifo_status 0 1 }  { ethDataFifo_V_keep_V_read fifo_update 1 1 } } }
	ethDataFifo_V_last_V { ap_fifo {  { ethDataFifo_V_last_V_dout fifo_data 0 1 }  { ethDataFifo_V_last_V_empty_n fifo_status 0 1 }  { ethDataFifo_V_last_V_read fifo_update 1 1 } } }
	ipv4ShiftFifo_V_data { ap_fifo {  { ipv4ShiftFifo_V_data_din fifo_data 1 64 }  { ipv4ShiftFifo_V_data_full_n fifo_status 0 1 }  { ipv4ShiftFifo_V_data_write fifo_update 1 1 } } }
	ipv4ShiftFifo_V_keep { ap_fifo {  { ipv4ShiftFifo_V_keep_din fifo_data 1 8 }  { ipv4ShiftFifo_V_keep_full_n fifo_status 0 1 }  { ipv4ShiftFifo_V_keep_write fifo_update 1 1 } } }
	ipv4ShiftFifo_V_last { ap_fifo {  { ipv4ShiftFifo_V_last_din fifo_data 1 1 }  { ipv4ShiftFifo_V_last_full_n fifo_status 0 1 }  { ipv4ShiftFifo_V_last_write fifo_update 1 1 } } }
	ipv6ShiftFifo_V_data { ap_fifo {  { ipv6ShiftFifo_V_data_din fifo_data 1 64 }  { ipv6ShiftFifo_V_data_full_n fifo_status 0 1 }  { ipv6ShiftFifo_V_data_write fifo_update 1 1 } } }
	ipv6ShiftFifo_V_keep { ap_fifo {  { ipv6ShiftFifo_V_keep_din fifo_data 1 8 }  { ipv6ShiftFifo_V_keep_full_n fifo_status 0 1 }  { ipv6ShiftFifo_V_keep_write fifo_update 1 1 } } }
	ipv6ShiftFifo_V_last { ap_fifo {  { ipv6ShiftFifo_V_last_din fifo_data 1 1 }  { ipv6ShiftFifo_V_last_full_n fifo_status 0 1 }  { ipv6ShiftFifo_V_last_write fifo_update 1 1 } } }
}
