#include "hls_design_meta.h"
const Port_Property HLS_Design_Meta::port_props[]={
	Port_Property("s_axi_AXILiteS_AWVALID", 1, hls_in, -1, "", "", 1),
	Port_Property("s_axi_AXILiteS_AWREADY", 1, hls_out, -1, "", "", 1),
	Port_Property("s_axi_AXILiteS_AWADDR", 5, hls_in, -1, "", "", 1),
	Port_Property("s_axi_AXILiteS_WVALID", 1, hls_in, -1, "", "", 1),
	Port_Property("s_axi_AXILiteS_WREADY", 1, hls_out, -1, "", "", 1),
	Port_Property("s_axi_AXILiteS_WDATA", 32, hls_in, -1, "", "", 1),
	Port_Property("s_axi_AXILiteS_WSTRB", 4, hls_in, -1, "", "", 1),
	Port_Property("s_axi_AXILiteS_ARVALID", 1, hls_in, -1, "", "", 1),
	Port_Property("s_axi_AXILiteS_ARREADY", 1, hls_out, -1, "", "", 1),
	Port_Property("s_axi_AXILiteS_ARADDR", 5, hls_in, -1, "", "", 1),
	Port_Property("s_axi_AXILiteS_RVALID", 1, hls_out, -1, "", "", 1),
	Port_Property("s_axi_AXILiteS_RREADY", 1, hls_in, -1, "", "", 1),
	Port_Property("s_axi_AXILiteS_RDATA", 32, hls_out, -1, "", "", 1),
	Port_Property("s_axi_AXILiteS_RRESP", 2, hls_out, -1, "", "", 1),
	Port_Property("s_axi_AXILiteS_BVALID", 1, hls_out, -1, "", "", 1),
	Port_Property("s_axi_AXILiteS_BREADY", 1, hls_in, -1, "", "", 1),
	Port_Property("s_axi_AXILiteS_BRESP", 2, hls_out, -1, "", "", 1),
	Port_Property("ap_clk", 1, hls_in, -1, "", "", 1),
	Port_Property("ap_rst_n", 1, hls_in, -1, "", "", 1),
	Port_Property("s_axis_raw_TDATA", 64, hls_in, 0, "axis", "in_data", 1),
	Port_Property("s_axis_raw_TKEEP", 8, hls_in, 1, "axis", "in_data", 1),
	Port_Property("s_axis_raw_TLAST", 1, hls_in, 2, "axis", "in_data", 1),
	Port_Property("m_axis_arp_TDATA", 64, hls_out, 3, "axis", "out_data", 1),
	Port_Property("m_axis_arp_TKEEP", 8, hls_out, 4, "axis", "out_data", 1),
	Port_Property("m_axis_arp_TLAST", 1, hls_out, 5, "axis", "out_data", 1),
	Port_Property("m_axis_icmpv6_TDATA", 64, hls_out, 6, "axis", "out_data", 1),
	Port_Property("m_axis_icmpv6_TKEEP", 8, hls_out, 7, "axis", "out_data", 1),
	Port_Property("m_axis_icmpv6_TLAST", 1, hls_out, 8, "axis", "out_data", 1),
	Port_Property("m_axis_ipv6udp_TDATA", 64, hls_out, 9, "axis", "out_data", 1),
	Port_Property("m_axis_ipv6udp_TKEEP", 8, hls_out, 10, "axis", "out_data", 1),
	Port_Property("m_axis_ipv6udp_TLAST", 1, hls_out, 11, "axis", "out_data", 1),
	Port_Property("m_axis_icmp_TDATA", 64, hls_out, 12, "axis", "out_data", 1),
	Port_Property("m_axis_icmp_TKEEP", 8, hls_out, 13, "axis", "out_data", 1),
	Port_Property("m_axis_icmp_TLAST", 1, hls_out, 14, "axis", "out_data", 1),
	Port_Property("m_axis_udp_TDATA", 64, hls_out, 15, "axis", "out_data", 1),
	Port_Property("m_axis_udp_TKEEP", 8, hls_out, 16, "axis", "out_data", 1),
	Port_Property("m_axis_udp_TLAST", 1, hls_out, 17, "axis", "out_data", 1),
	Port_Property("m_axis_tcp_TDATA", 64, hls_out, 18, "axis", "out_data", 1),
	Port_Property("m_axis_tcp_TKEEP", 8, hls_out, 19, "axis", "out_data", 1),
	Port_Property("m_axis_tcp_TLAST", 1, hls_out, 20, "axis", "out_data", 1),
	Port_Property("m_axis_roce_TDATA", 64, hls_out, 21, "axis", "out_data", 1),
	Port_Property("m_axis_roce_TKEEP", 8, hls_out, 22, "axis", "out_data", 1),
	Port_Property("m_axis_roce_TLAST", 1, hls_out, 23, "axis", "out_data", 1),
	Port_Property("s_axis_raw_TVALID", 1, hls_in, 2, "axis", "in_vld", 1),
	Port_Property("s_axis_raw_TREADY", 1, hls_out, 2, "axis", "in_acc", 1),
	Port_Property("m_axis_arp_TVALID", 1, hls_out, 5, "axis", "out_vld", 1),
	Port_Property("m_axis_arp_TREADY", 1, hls_in, 5, "axis", "out_acc", 1),
	Port_Property("m_axis_icmp_TVALID", 1, hls_out, 14, "axis", "out_vld", 1),
	Port_Property("m_axis_icmp_TREADY", 1, hls_in, 14, "axis", "out_acc", 1),
	Port_Property("m_axis_tcp_TVALID", 1, hls_out, 20, "axis", "out_vld", 1),
	Port_Property("m_axis_tcp_TREADY", 1, hls_in, 20, "axis", "out_acc", 1),
	Port_Property("m_axis_icmpv6_TVALID", 1, hls_out, 8, "axis", "out_vld", 1),
	Port_Property("m_axis_icmpv6_TREADY", 1, hls_in, 8, "axis", "out_acc", 1),
	Port_Property("m_axis_ipv6udp_TVALID", 1, hls_out, 11, "axis", "out_vld", 1),
	Port_Property("m_axis_ipv6udp_TREADY", 1, hls_in, 11, "axis", "out_acc", 1),
	Port_Property("m_axis_udp_TVALID", 1, hls_out, 17, "axis", "out_vld", 1),
	Port_Property("m_axis_udp_TREADY", 1, hls_in, 17, "axis", "out_acc", 1),
	Port_Property("m_axis_roce_TVALID", 1, hls_out, 23, "axis", "out_vld", 1),
	Port_Property("m_axis_roce_TREADY", 1, hls_in, 23, "axis", "out_acc", 1),
};
const char* HLS_Design_Meta::dut_name = "ip_handler_top";
