# This script segment is generated automatically by AutoPilot

# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 35 \
    name myIpAddress_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_myIpAddress_V \
    op interface \
    ports { myIpAddress_V_dout { I 32 vector } myIpAddress_V_empty_n { I 1 bit } myIpAddress_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 36 \
    name ipDataFifo_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_ipDataFifo_V \
    op interface \
    ports { ipDataFifo_V_dout { I 73 vector } ipDataFifo_V_empty_n { I 1 bit } ipDataFifo_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 37 \
    name ipDataMetaFifo_V_dat \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_ipDataMetaFifo_V_dat \
    op interface \
    ports { ipDataMetaFifo_V_dat_din { O 64 vector } ipDataMetaFifo_V_dat_full_n { I 1 bit } ipDataMetaFifo_V_dat_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 38 \
    name ipDataMetaFifo_V_kee \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_ipDataMetaFifo_V_kee \
    op interface \
    ports { ipDataMetaFifo_V_kee_din { O 8 vector } ipDataMetaFifo_V_kee_full_n { I 1 bit } ipDataMetaFifo_V_kee_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 39 \
    name ipDataMetaFifo_V_las \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_ipDataMetaFifo_V_las \
    op interface \
    ports { ipDataMetaFifo_V_las_din { O 1 vector } ipDataMetaFifo_V_las_full_n { I 1 bit } ipDataMetaFifo_V_las_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 40 \
    name validIpAddressFifo_V \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_validIpAddressFifo_V \
    op interface \
    ports { validIpAddressFifo_V_din { O 1 vector } validIpAddressFifo_V_full_n { I 1 bit } validIpAddressFifo_V_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 41 \
    name ipv4ProtocolFifo_V_V \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_ipv4ProtocolFifo_V_V \
    op interface \
    ports { ipv4ProtocolFifo_V_V_din { O 8 vector } ipv4ProtocolFifo_V_V_full_n { I 1 bit } ipv4ProtocolFifo_V_V_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_start { I 1 bit } ap_ready { O 1 bit } ap_done { O 1 bit } ap_idle { O 1 bit } ap_continue { I 1 bit } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


