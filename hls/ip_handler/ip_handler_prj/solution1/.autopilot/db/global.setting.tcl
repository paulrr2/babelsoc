
set TopModule "ip_handler_top"
set ClockPeriod 6.4
set ClockList ap_clk
set HasVivadoClockPeriod 0
set CombLogicFlag 0
set PipelineFlag 1
set DataflowTaskPipelineFlag 1
set TrivialPipelineFlag 0
set noPortSwitchingFlag 0
set FloatingPointFlag 0
set FftOrFirFlag 0
set NbRWValue 1
set intNbAccess 1
set NewDSPMapping 1
set HasDSPModule 0
set ResetLevelFlag 0
set ResetStyle control
set ResetSyncFlag 1
set ResetRegisterFlag 0
set ResetVariableFlag 0
set FsmEncStyle onehot
set MaxFanout 0
set RtlPrefix {}
set ExtraCCFlags {}
set ExtraCLdFlags {}
set SynCheckOptions {}
set PresynOptions {}
set PreprocOptions {}
set SchedOptions {}
set BindOptions {}
set RtlGenOptions {}
set RtlWriterOptions {}
set CbcGenFlag {}
set CasGenFlag {}
set CasMonitorFlag {}
set AutoSimOptions {}
set ExportMCPathFlag 0
set SCTraceFileName mytrace
set SCTraceFileFormat vcd
set SCTraceOption all
set TargetInfo xc7z020:-clg400:-1
set SourceFiles {sc {} c {/home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/ip_handler/ip_handler.cpp /home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/ip_handler/../ipv4/ipv4_utils.cpp /home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/ip_handler/../axi_utils.cpp}}
set SourceFlags {sc {} c {-I/home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/build_pynq/hls/ip_handler {} {}}}
set DirectiveFile /home/paulrr2/Projects/ece527/babelsoc/hls/ip_handler/ip_handler_prj/solution1/solution1.directive
set TBFiles {verilog /home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/ip_handler/test_ip_handler.cpp bc /home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/ip_handler/test_ip_handler.cpp vhdl /home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/ip_handler/test_ip_handler.cpp sc /home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/ip_handler/test_ip_handler.cpp cas /home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/ip_handler/test_ip_handler.cpp c {}}
set SpecLanguage C
set TVInFiles {bc {} c {} sc {} cas {} vhdl {} verilog {}}
set TVOutFiles {bc {} c {} sc {} cas {} vhdl {} verilog {}}
set TBTops {verilog {} bc {} vhdl {} sc {} cas {} c {}}
set TBInstNames {verilog {} bc {} vhdl {} sc {} cas {} c {}}
set XDCFiles {}
set ExtraGlobalOptions {"area_timing" 1 "clock_gate" 1 "impl_flow" map "power_gate" 0}
set TBTVFileNotFound {}
set AppFile ../vivado_hls.app
set ApsFile solution1.aps
set AvePath ../..
set DefaultPlatform DefaultPlatform
set multiClockList {}
set SCPortClockMap {}
set intNbAccess 1
set PlatformFiles {{DefaultPlatform {xilinx/zynq/zynq xilinx/zynq/zynq_fpv6}}}
set HPFPO 0
