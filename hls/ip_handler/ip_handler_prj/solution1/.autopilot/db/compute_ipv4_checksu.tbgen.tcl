set moduleName compute_ipv4_checksu
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {compute_ipv4_checksu}
set C_modelType { void 0 }
set C_modelArgList {
	{ ipDataMetaFifo_V_dat int 64 regular {fifo 0 volatile } {global 0}  }
	{ ipDataMetaFifo_V_kee int 8 regular {fifo 0 volatile } {global 0}  }
	{ ipDataMetaFifo_V_las int 1 regular {fifo 0 volatile } {global 0}  }
	{ ipDataCheckFifo_V int 73 regular {fifo 1 volatile } {global 1}  }
	{ iph_subSumsFifoOut_V int 68 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "ipDataMetaFifo_V_dat", "interface" : "fifo", "bitwidth" : 64, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "ipDataMetaFifo_V_kee", "interface" : "fifo", "bitwidth" : 8, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "ipDataMetaFifo_V_las", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "ipDataCheckFifo_V", "interface" : "fifo", "bitwidth" : 73, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "iph_subSumsFifoOut_V", "interface" : "fifo", "bitwidth" : 68, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 22
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ ipDataMetaFifo_V_dat_dout sc_in sc_lv 64 signal 0 } 
	{ ipDataMetaFifo_V_dat_empty_n sc_in sc_logic 1 signal 0 } 
	{ ipDataMetaFifo_V_dat_read sc_out sc_logic 1 signal 0 } 
	{ ipDataMetaFifo_V_kee_dout sc_in sc_lv 8 signal 1 } 
	{ ipDataMetaFifo_V_kee_empty_n sc_in sc_logic 1 signal 1 } 
	{ ipDataMetaFifo_V_kee_read sc_out sc_logic 1 signal 1 } 
	{ ipDataMetaFifo_V_las_dout sc_in sc_lv 1 signal 2 } 
	{ ipDataMetaFifo_V_las_empty_n sc_in sc_logic 1 signal 2 } 
	{ ipDataMetaFifo_V_las_read sc_out sc_logic 1 signal 2 } 
	{ ipDataCheckFifo_V_din sc_out sc_lv 73 signal 3 } 
	{ ipDataCheckFifo_V_full_n sc_in sc_logic 1 signal 3 } 
	{ ipDataCheckFifo_V_write sc_out sc_logic 1 signal 3 } 
	{ iph_subSumsFifoOut_V_din sc_out sc_lv 68 signal 4 } 
	{ iph_subSumsFifoOut_V_full_n sc_in sc_logic 1 signal 4 } 
	{ iph_subSumsFifoOut_V_write sc_out sc_logic 1 signal 4 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "ipDataMetaFifo_V_dat_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_dat", "role": "dout" }} , 
 	{ "name": "ipDataMetaFifo_V_dat_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_dat", "role": "empty_n" }} , 
 	{ "name": "ipDataMetaFifo_V_dat_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_dat", "role": "read" }} , 
 	{ "name": "ipDataMetaFifo_V_kee_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_kee", "role": "dout" }} , 
 	{ "name": "ipDataMetaFifo_V_kee_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_kee", "role": "empty_n" }} , 
 	{ "name": "ipDataMetaFifo_V_kee_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_kee", "role": "read" }} , 
 	{ "name": "ipDataMetaFifo_V_las_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_las", "role": "dout" }} , 
 	{ "name": "ipDataMetaFifo_V_las_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_las", "role": "empty_n" }} , 
 	{ "name": "ipDataMetaFifo_V_las_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_las", "role": "read" }} , 
 	{ "name": "ipDataCheckFifo_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":73, "type": "signal", "bundle":{"name": "ipDataCheckFifo_V", "role": "din" }} , 
 	{ "name": "ipDataCheckFifo_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataCheckFifo_V", "role": "full_n" }} , 
 	{ "name": "ipDataCheckFifo_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataCheckFifo_V", "role": "write" }} , 
 	{ "name": "iph_subSumsFifoOut_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":68, "type": "signal", "bundle":{"name": "iph_subSumsFifoOut_V", "role": "din" }} , 
 	{ "name": "iph_subSumsFifoOut_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "iph_subSumsFifoOut_V", "role": "full_n" }} , 
 	{ "name": "iph_subSumsFifoOut_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "iph_subSumsFifoOut_V", "role": "write" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "compute_ipv4_checksu",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "3", "EstimateLatencyMin" : "3", "EstimateLatencyMax" : "3",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "ipDataMetaFifo_V_dat", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipDataMetaFifo_V_dat_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipDataMetaFifo_V_kee", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipDataMetaFifo_V_kee_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipDataMetaFifo_V_las", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipDataMetaFifo_V_las_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipDataCheckFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipDataCheckFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "cics_wordCount_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ip_sums_sum_V_0", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ip_sums_sum_V_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ip_sums_sum_V_2", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ip_sums_sum_V_3", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ipHeaderLen_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "iph_subSumsFifoOut_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "iph_subSumsFifoOut_V_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	compute_ipv4_checksu {
		ipDataMetaFifo_V_dat {Type I LastRead 0 FirstWrite -1}
		ipDataMetaFifo_V_kee {Type I LastRead 0 FirstWrite -1}
		ipDataMetaFifo_V_las {Type I LastRead 0 FirstWrite -1}
		ipDataCheckFifo_V {Type O LastRead -1 FirstWrite 1}
		cics_wordCount_V {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_0 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_1 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_2 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_3 {Type IO LastRead -1 FirstWrite -1}
		cics_ipHeaderLen_V {Type IO LastRead -1 FirstWrite -1}
		iph_subSumsFifoOut_V {Type O LastRead -1 FirstWrite 3}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "3", "Max" : "3"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	ipDataMetaFifo_V_dat { ap_fifo {  { ipDataMetaFifo_V_dat_dout fifo_data 0 64 }  { ipDataMetaFifo_V_dat_empty_n fifo_status 0 1 }  { ipDataMetaFifo_V_dat_read fifo_update 1 1 } } }
	ipDataMetaFifo_V_kee { ap_fifo {  { ipDataMetaFifo_V_kee_dout fifo_data 0 8 }  { ipDataMetaFifo_V_kee_empty_n fifo_status 0 1 }  { ipDataMetaFifo_V_kee_read fifo_update 1 1 } } }
	ipDataMetaFifo_V_las { ap_fifo {  { ipDataMetaFifo_V_las_dout fifo_data 0 1 }  { ipDataMetaFifo_V_las_empty_n fifo_status 0 1 }  { ipDataMetaFifo_V_las_read fifo_update 1 1 } } }
	ipDataCheckFifo_V { ap_fifo {  { ipDataCheckFifo_V_din fifo_data 1 73 }  { ipDataCheckFifo_V_full_n fifo_status 0 1 }  { ipDataCheckFifo_V_write fifo_update 1 1 } } }
	iph_subSumsFifoOut_V { ap_fifo {  { iph_subSumsFifoOut_V_din fifo_data 1 68 }  { iph_subSumsFifoOut_V_full_n fifo_status 0 1 }  { iph_subSumsFifoOut_V_write fifo_update 1 1 } } }
}
set moduleName compute_ipv4_checksu
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {compute_ipv4_checksu}
set C_modelType { void 0 }
set C_modelArgList {
	{ ipDataMetaFifo_V_dat int 64 regular {fifo 0 volatile } {global 0}  }
	{ ipDataMetaFifo_V_kee int 8 regular {fifo 0 volatile } {global 0}  }
	{ ipDataMetaFifo_V_las int 1 regular {fifo 0 volatile } {global 0}  }
	{ ipDataCheckFifo_V int 73 regular {fifo 1 volatile } {global 1}  }
	{ iph_subSumsFifoOut_V int 68 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "ipDataMetaFifo_V_dat", "interface" : "fifo", "bitwidth" : 64, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "ipDataMetaFifo_V_kee", "interface" : "fifo", "bitwidth" : 8, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "ipDataMetaFifo_V_las", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "ipDataCheckFifo_V", "interface" : "fifo", "bitwidth" : 73, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "iph_subSumsFifoOut_V", "interface" : "fifo", "bitwidth" : 68, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 22
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ ipDataMetaFifo_V_dat_dout sc_in sc_lv 64 signal 0 } 
	{ ipDataMetaFifo_V_dat_empty_n sc_in sc_logic 1 signal 0 } 
	{ ipDataMetaFifo_V_dat_read sc_out sc_logic 1 signal 0 } 
	{ ipDataMetaFifo_V_kee_dout sc_in sc_lv 8 signal 1 } 
	{ ipDataMetaFifo_V_kee_empty_n sc_in sc_logic 1 signal 1 } 
	{ ipDataMetaFifo_V_kee_read sc_out sc_logic 1 signal 1 } 
	{ ipDataMetaFifo_V_las_dout sc_in sc_lv 1 signal 2 } 
	{ ipDataMetaFifo_V_las_empty_n sc_in sc_logic 1 signal 2 } 
	{ ipDataMetaFifo_V_las_read sc_out sc_logic 1 signal 2 } 
	{ ipDataCheckFifo_V_din sc_out sc_lv 73 signal 3 } 
	{ ipDataCheckFifo_V_full_n sc_in sc_logic 1 signal 3 } 
	{ ipDataCheckFifo_V_write sc_out sc_logic 1 signal 3 } 
	{ iph_subSumsFifoOut_V_din sc_out sc_lv 68 signal 4 } 
	{ iph_subSumsFifoOut_V_full_n sc_in sc_logic 1 signal 4 } 
	{ iph_subSumsFifoOut_V_write sc_out sc_logic 1 signal 4 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "ipDataMetaFifo_V_dat_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_dat", "role": "dout" }} , 
 	{ "name": "ipDataMetaFifo_V_dat_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_dat", "role": "empty_n" }} , 
 	{ "name": "ipDataMetaFifo_V_dat_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_dat", "role": "read" }} , 
 	{ "name": "ipDataMetaFifo_V_kee_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_kee", "role": "dout" }} , 
 	{ "name": "ipDataMetaFifo_V_kee_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_kee", "role": "empty_n" }} , 
 	{ "name": "ipDataMetaFifo_V_kee_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_kee", "role": "read" }} , 
 	{ "name": "ipDataMetaFifo_V_las_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_las", "role": "dout" }} , 
 	{ "name": "ipDataMetaFifo_V_las_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_las", "role": "empty_n" }} , 
 	{ "name": "ipDataMetaFifo_V_las_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_las", "role": "read" }} , 
 	{ "name": "ipDataCheckFifo_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":73, "type": "signal", "bundle":{"name": "ipDataCheckFifo_V", "role": "din" }} , 
 	{ "name": "ipDataCheckFifo_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataCheckFifo_V", "role": "full_n" }} , 
 	{ "name": "ipDataCheckFifo_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataCheckFifo_V", "role": "write" }} , 
 	{ "name": "iph_subSumsFifoOut_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":68, "type": "signal", "bundle":{"name": "iph_subSumsFifoOut_V", "role": "din" }} , 
 	{ "name": "iph_subSumsFifoOut_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "iph_subSumsFifoOut_V", "role": "full_n" }} , 
 	{ "name": "iph_subSumsFifoOut_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "iph_subSumsFifoOut_V", "role": "write" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "compute_ipv4_checksu",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "ipDataMetaFifo_V_dat", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipDataMetaFifo_V_dat_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipDataMetaFifo_V_kee", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipDataMetaFifo_V_kee_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipDataMetaFifo_V_las", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipDataMetaFifo_V_las_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipDataCheckFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipDataCheckFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "cics_wordCount_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ip_sums_sum_V_0", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ip_sums_sum_V_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ip_sums_sum_V_2", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ip_sums_sum_V_3", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ipHeaderLen_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "iph_subSumsFifoOut_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "iph_subSumsFifoOut_V_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	compute_ipv4_checksu {
		ipDataMetaFifo_V_dat {Type I LastRead 0 FirstWrite -1}
		ipDataMetaFifo_V_kee {Type I LastRead 0 FirstWrite -1}
		ipDataMetaFifo_V_las {Type I LastRead 0 FirstWrite -1}
		ipDataCheckFifo_V {Type O LastRead -1 FirstWrite 1}
		cics_wordCount_V {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_0 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_1 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_2 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_3 {Type IO LastRead -1 FirstWrite -1}
		cics_ipHeaderLen_V {Type IO LastRead -1 FirstWrite -1}
		iph_subSumsFifoOut_V {Type O LastRead -1 FirstWrite 2}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "2", "Max" : "2"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	ipDataMetaFifo_V_dat { ap_fifo {  { ipDataMetaFifo_V_dat_dout fifo_data 0 64 }  { ipDataMetaFifo_V_dat_empty_n fifo_status 0 1 }  { ipDataMetaFifo_V_dat_read fifo_update 1 1 } } }
	ipDataMetaFifo_V_kee { ap_fifo {  { ipDataMetaFifo_V_kee_dout fifo_data 0 8 }  { ipDataMetaFifo_V_kee_empty_n fifo_status 0 1 }  { ipDataMetaFifo_V_kee_read fifo_update 1 1 } } }
	ipDataMetaFifo_V_las { ap_fifo {  { ipDataMetaFifo_V_las_dout fifo_data 0 1 }  { ipDataMetaFifo_V_las_empty_n fifo_status 0 1 }  { ipDataMetaFifo_V_las_read fifo_update 1 1 } } }
	ipDataCheckFifo_V { ap_fifo {  { ipDataCheckFifo_V_din fifo_data 1 73 }  { ipDataCheckFifo_V_full_n fifo_status 0 1 }  { ipDataCheckFifo_V_write fifo_update 1 1 } } }
	iph_subSumsFifoOut_V { ap_fifo {  { iph_subSumsFifoOut_V_din fifo_data 1 68 }  { iph_subSumsFifoOut_V_full_n fifo_status 0 1 }  { iph_subSumsFifoOut_V_write fifo_update 1 1 } } }
}
