set moduleName ip_handler_top
set isTopModule 1
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type dataflow
set FunctionProtocol ap_ctrl_none
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {ip_handler_top}
set C_modelType { void 0 }
set C_modelArgList {
	{ s_axis_raw_V_data_V int 64 regular {axi_s 0 volatile  { s_axis_raw Data } }  }
	{ s_axis_raw_V_keep_V int 8 regular {axi_s 0 volatile  { s_axis_raw Keep } }  }
	{ s_axis_raw_V_last_V int 1 regular {axi_s 0 volatile  { s_axis_raw Last } }  }
	{ m_axis_arp_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_arp Data } }  }
	{ m_axis_arp_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_arp Keep } }  }
	{ m_axis_arp_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_arp Last } }  }
	{ m_axis_icmpv6_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_icmpv6 Data } }  }
	{ m_axis_icmpv6_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_icmpv6 Keep } }  }
	{ m_axis_icmpv6_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_icmpv6 Last } }  }
	{ m_axis_ipv6udp_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_ipv6udp Data } }  }
	{ m_axis_ipv6udp_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_ipv6udp Keep } }  }
	{ m_axis_ipv6udp_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_ipv6udp Last } }  }
	{ m_axis_icmp_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_icmp Data } }  }
	{ m_axis_icmp_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_icmp Keep } }  }
	{ m_axis_icmp_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_icmp Last } }  }
	{ m_axis_udp_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_udp Data } }  }
	{ m_axis_udp_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_udp Keep } }  }
	{ m_axis_udp_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_udp Last } }  }
	{ m_axis_tcp_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_tcp Data } }  }
	{ m_axis_tcp_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_tcp Keep } }  }
	{ m_axis_tcp_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_tcp Last } }  }
	{ m_axis_roce_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_roce Data } }  }
	{ m_axis_roce_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_roce Keep } }  }
	{ m_axis_roce_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_roce Last } }  }
	{ myIpAddress_V int 32 regular {ap_stable 0} }
}
set C_modelArgMapList {[ 
	{ "Name" : "s_axis_raw_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "READONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "s_axis_raw.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_raw_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "s_axis_raw.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_raw_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "s_axis_raw.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_arp_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "m_axis_arp.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_arp_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "m_axis_arp.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_arp_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "m_axis_arp.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_icmpv6_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "m_axis_icmpv6.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_icmpv6_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "m_axis_icmpv6.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_icmpv6_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "m_axis_icmpv6.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_ipv6udp_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "m_axis_ipv6udp.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_ipv6udp_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "m_axis_ipv6udp.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_ipv6udp_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "m_axis_ipv6udp.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_icmp_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "m_axis_icmp.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_icmp_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "m_axis_icmp.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_icmp_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "m_axis_icmp.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_udp_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "m_axis_udp.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_udp_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "m_axis_udp.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_udp_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "m_axis_udp.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_tcp_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "m_axis_tcp.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_tcp_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "m_axis_tcp.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_tcp_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "m_axis_tcp.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_roce_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "m_axis_roce.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_roce_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "m_axis_roce.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_roce_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "m_axis_roce.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "myIpAddress_V", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "myIpAddress.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}]} ]}
# RTL Port declarations: 
set portNum 43
set portList { 
	{ s_axis_raw_TDATA sc_in sc_lv 64 signal 0 } 
	{ s_axis_raw_TKEEP sc_in sc_lv 8 signal 1 } 
	{ s_axis_raw_TLAST sc_in sc_lv 1 signal 2 } 
	{ m_axis_arp_TDATA sc_out sc_lv 64 signal 3 } 
	{ m_axis_arp_TKEEP sc_out sc_lv 8 signal 4 } 
	{ m_axis_arp_TLAST sc_out sc_lv 1 signal 5 } 
	{ m_axis_icmpv6_TDATA sc_out sc_lv 64 signal 6 } 
	{ m_axis_icmpv6_TKEEP sc_out sc_lv 8 signal 7 } 
	{ m_axis_icmpv6_TLAST sc_out sc_lv 1 signal 8 } 
	{ m_axis_ipv6udp_TDATA sc_out sc_lv 64 signal 9 } 
	{ m_axis_ipv6udp_TKEEP sc_out sc_lv 8 signal 10 } 
	{ m_axis_ipv6udp_TLAST sc_out sc_lv 1 signal 11 } 
	{ m_axis_icmp_TDATA sc_out sc_lv 64 signal 12 } 
	{ m_axis_icmp_TKEEP sc_out sc_lv 8 signal 13 } 
	{ m_axis_icmp_TLAST sc_out sc_lv 1 signal 14 } 
	{ m_axis_udp_TDATA sc_out sc_lv 64 signal 15 } 
	{ m_axis_udp_TKEEP sc_out sc_lv 8 signal 16 } 
	{ m_axis_udp_TLAST sc_out sc_lv 1 signal 17 } 
	{ m_axis_tcp_TDATA sc_out sc_lv 64 signal 18 } 
	{ m_axis_tcp_TKEEP sc_out sc_lv 8 signal 19 } 
	{ m_axis_tcp_TLAST sc_out sc_lv 1 signal 20 } 
	{ m_axis_roce_TDATA sc_out sc_lv 64 signal 21 } 
	{ m_axis_roce_TKEEP sc_out sc_lv 8 signal 22 } 
	{ m_axis_roce_TLAST sc_out sc_lv 1 signal 23 } 
	{ myIpAddress_V sc_in sc_lv 32 signal 24 } 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst_n sc_in sc_logic 1 reset -1 active_low_sync } 
	{ s_axis_raw_TVALID sc_in sc_logic 1 invld 2 } 
	{ s_axis_raw_TREADY sc_out sc_logic 1 inacc 2 } 
	{ m_axis_arp_TVALID sc_out sc_logic 1 outvld 5 } 
	{ m_axis_arp_TREADY sc_in sc_logic 1 outacc 5 } 
	{ m_axis_icmp_TVALID sc_out sc_logic 1 outvld 14 } 
	{ m_axis_icmp_TREADY sc_in sc_logic 1 outacc 14 } 
	{ m_axis_tcp_TVALID sc_out sc_logic 1 outvld 20 } 
	{ m_axis_tcp_TREADY sc_in sc_logic 1 outacc 20 } 
	{ m_axis_icmpv6_TVALID sc_out sc_logic 1 outvld 8 } 
	{ m_axis_icmpv6_TREADY sc_in sc_logic 1 outacc 8 } 
	{ m_axis_ipv6udp_TVALID sc_out sc_logic 1 outvld 11 } 
	{ m_axis_ipv6udp_TREADY sc_in sc_logic 1 outacc 11 } 
	{ m_axis_udp_TVALID sc_out sc_logic 1 outvld 17 } 
	{ m_axis_udp_TREADY sc_in sc_logic 1 outacc 17 } 
	{ m_axis_roce_TVALID sc_out sc_logic 1 outvld 23 } 
	{ m_axis_roce_TREADY sc_in sc_logic 1 outacc 23 } 
}
set NewPortList {[ 
	{ "name": "s_axis_raw_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "s_axis_raw_V_data_V", "role": "default" }} , 
 	{ "name": "s_axis_raw_TKEEP", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "s_axis_raw_V_keep_V", "role": "default" }} , 
 	{ "name": "s_axis_raw_TLAST", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "s_axis_raw_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_arp_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "m_axis_arp_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_arp_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "m_axis_arp_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_arp_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "m_axis_arp_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_icmpv6_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "m_axis_icmpv6_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_icmpv6_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "m_axis_icmpv6_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_icmpv6_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "m_axis_icmpv6_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_ipv6udp_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "m_axis_ipv6udp_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_ipv6udp_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "m_axis_ipv6udp_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_ipv6udp_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "m_axis_ipv6udp_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_icmp_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "m_axis_icmp_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_icmp_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "m_axis_icmp_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_icmp_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "m_axis_icmp_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_udp_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "m_axis_udp_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_udp_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "m_axis_udp_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_udp_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "m_axis_udp_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_tcp_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "m_axis_tcp_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_tcp_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "m_axis_tcp_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_tcp_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "m_axis_tcp_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_roce_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "m_axis_roce_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_roce_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "m_axis_roce_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_roce_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "m_axis_roce_V_last_V", "role": "default" }} , 
 	{ "name": "myIpAddress_V", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "myIpAddress_V", "role": "default" }} , 
 	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst_n", "role": "default" }} , 
 	{ "name": "s_axis_raw_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "s_axis_raw_V_last_V", "role": "default" }} , 
 	{ "name": "s_axis_raw_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "s_axis_raw_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_arp_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_arp_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_arp_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_arp_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_icmp_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_icmp_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_icmp_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_icmp_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_tcp_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_tcp_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_tcp_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_tcp_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_icmpv6_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_icmpv6_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_icmpv6_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_icmpv6_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_ipv6udp_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_ipv6udp_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_ipv6udp_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_ipv6udp_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_udp_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_udp_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_udp_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_udp_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_roce_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_roce_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_roce_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_roce_V_last_V", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2", "6", "7", "8", "9", "10", "11", "12", "14", "21", "28", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60"],
		"CDFG" : "ip_handler_top",
		"Protocol" : "ap_ctrl_none",
		"ControlExist" : "0", "ap_start" : "0", "ap_ready" : "0", "ap_done" : "0", "ap_continue" : "0", "ap_idle" : "0",
		"Pipeline" : "Dataflow", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "1",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "28", "EstimateLatencyMax" : "28",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "1",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"InputProcess" : [
			{"ID" : "1", "Name" : "detect_eth_protocol_U0"}],
		"OutputProcess" : [
			{"ID" : "2", "Name" : "route_by_eth_protoco_U0"},
			{"ID" : "14", "Name" : "detect_ipv4_protocol_U0"},
			{"ID" : "21", "Name" : "detect_ipv6_protocol_U0"},
			{"ID" : "28", "Name" : "duplicate_stream_U0"}],
		"Port" : [
			{"Name" : "s_axis_raw_V_data_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "detect_eth_protocol_U0", "Port" : "dataIn_V_data_V"}]},
			{"Name" : "s_axis_raw_V_keep_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "detect_eth_protocol_U0", "Port" : "dataIn_V_keep_V"}]},
			{"Name" : "s_axis_raw_V_last_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "detect_eth_protocol_U0", "Port" : "dataIn_V_last_V"}]},
			{"Name" : "m_axis_arp_V_data_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "route_by_eth_protoco_U0", "Port" : "ARPdataOut_V_data_V"}]},
			{"Name" : "m_axis_arp_V_keep_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "route_by_eth_protoco_U0", "Port" : "ARPdataOut_V_keep_V"}]},
			{"Name" : "m_axis_arp_V_last_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "route_by_eth_protoco_U0", "Port" : "ARPdataOut_V_last_V"}]},
			{"Name" : "m_axis_icmpv6_V_data_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "21", "SubInstance" : "detect_ipv6_protocol_U0", "Port" : "icmpv6DataOut_V_data_V"}]},
			{"Name" : "m_axis_icmpv6_V_keep_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "21", "SubInstance" : "detect_ipv6_protocol_U0", "Port" : "icmpv6DataOut_V_keep_V"}]},
			{"Name" : "m_axis_icmpv6_V_last_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "21", "SubInstance" : "detect_ipv6_protocol_U0", "Port" : "icmpv6DataOut_V_last_V"}]},
			{"Name" : "m_axis_ipv6udp_V_data_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "21", "SubInstance" : "detect_ipv6_protocol_U0", "Port" : "ipv6UdpDataOut_V_data_V"}]},
			{"Name" : "m_axis_ipv6udp_V_keep_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "21", "SubInstance" : "detect_ipv6_protocol_U0", "Port" : "ipv6UdpDataOut_V_keep_V"}]},
			{"Name" : "m_axis_ipv6udp_V_last_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "21", "SubInstance" : "detect_ipv6_protocol_U0", "Port" : "ipv6UdpDataOut_V_last_V"}]},
			{"Name" : "m_axis_icmp_V_data_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "14", "SubInstance" : "detect_ipv4_protocol_U0", "Port" : "ICMPdataOut_V_data_V"}]},
			{"Name" : "m_axis_icmp_V_keep_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "14", "SubInstance" : "detect_ipv4_protocol_U0", "Port" : "ICMPdataOut_V_keep_V"}]},
			{"Name" : "m_axis_icmp_V_last_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "14", "SubInstance" : "detect_ipv4_protocol_U0", "Port" : "ICMPdataOut_V_last_V"}]},
			{"Name" : "m_axis_udp_V_data_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "28", "SubInstance" : "duplicate_stream_U0", "Port" : "out0_V_data_V"}]},
			{"Name" : "m_axis_udp_V_keep_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "28", "SubInstance" : "duplicate_stream_U0", "Port" : "out0_V_keep_V"}]},
			{"Name" : "m_axis_udp_V_last_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "28", "SubInstance" : "duplicate_stream_U0", "Port" : "out0_V_last_V"}]},
			{"Name" : "m_axis_tcp_V_data_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "14", "SubInstance" : "detect_ipv4_protocol_U0", "Port" : "TCPdataOut_V_data_V"}]},
			{"Name" : "m_axis_tcp_V_keep_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "14", "SubInstance" : "detect_ipv4_protocol_U0", "Port" : "TCPdataOut_V_keep_V"}]},
			{"Name" : "m_axis_tcp_V_last_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "14", "SubInstance" : "detect_ipv4_protocol_U0", "Port" : "TCPdataOut_V_last_V"}]},
			{"Name" : "m_axis_roce_V_data_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "28", "SubInstance" : "duplicate_stream_U0", "Port" : "out1_V_data_V"}]},
			{"Name" : "m_axis_roce_V_keep_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "28", "SubInstance" : "duplicate_stream_U0", "Port" : "out1_V_keep_V"}]},
			{"Name" : "m_axis_roce_V_last_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "28", "SubInstance" : "duplicate_stream_U0", "Port" : "out1_V_last_V"}]},
			{"Name" : "myIpAddress_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "header_ready", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "detect_eth_protocol_U0", "Port" : "header_ready"}]},
			{"Name" : "header_idx_1", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "detect_eth_protocol_U0", "Port" : "header_idx_1"}]},
			{"Name" : "header_header_V_1", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "detect_eth_protocol_U0", "Port" : "header_header_V_1"}]},
			{"Name" : "metaWritten_1", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "detect_eth_protocol_U0", "Port" : "metaWritten_1"}]},
			{"Name" : "etherTypeFifo_V_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "route_by_eth_protoco_U0", "Port" : "etherTypeFifo_V_V"},
					{"ID" : "1", "SubInstance" : "detect_eth_protocol_U0", "Port" : "etherTypeFifo_V_V"}]},
			{"Name" : "ethDataFifo_V_data_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "route_by_eth_protoco_U0", "Port" : "ethDataFifo_V_data_V"},
					{"ID" : "1", "SubInstance" : "detect_eth_protocol_U0", "Port" : "ethDataFifo_V_data_V"}]},
			{"Name" : "ethDataFifo_V_keep_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "route_by_eth_protoco_U0", "Port" : "ethDataFifo_V_keep_V"},
					{"ID" : "1", "SubInstance" : "detect_eth_protocol_U0", "Port" : "ethDataFifo_V_keep_V"}]},
			{"Name" : "ethDataFifo_V_last_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "route_by_eth_protoco_U0", "Port" : "ethDataFifo_V_last_V"},
					{"ID" : "1", "SubInstance" : "detect_eth_protocol_U0", "Port" : "ethDataFifo_V_last_V"}]},
			{"Name" : "rep_fsmState_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "route_by_eth_protoco_U0", "Port" : "rep_fsmState_V"}]},
			{"Name" : "rep_etherType_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "route_by_eth_protoco_U0", "Port" : "rep_etherType_V"}]},
			{"Name" : "ipv4ShiftFifo_V_data", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "route_by_eth_protoco_U0", "Port" : "ipv4ShiftFifo_V_data"},
					{"ID" : "6", "SubInstance" : "rshiftWordByOctet_1_U0", "Port" : "ipv4ShiftFifo_V_data"}]},
			{"Name" : "ipv4ShiftFifo_V_keep", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "route_by_eth_protoco_U0", "Port" : "ipv4ShiftFifo_V_keep"},
					{"ID" : "6", "SubInstance" : "rshiftWordByOctet_1_U0", "Port" : "ipv4ShiftFifo_V_keep"}]},
			{"Name" : "ipv4ShiftFifo_V_last", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "route_by_eth_protoco_U0", "Port" : "ipv4ShiftFifo_V_last"},
					{"ID" : "6", "SubInstance" : "rshiftWordByOctet_1_U0", "Port" : "ipv4ShiftFifo_V_last"}]},
			{"Name" : "ipv6ShiftFifo_V_data", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "route_by_eth_protoco_U0", "Port" : "ipv6ShiftFifo_V_data"},
					{"ID" : "7", "SubInstance" : "rshiftWordByOctet_U0", "Port" : "ipv6ShiftFifo_V_data"}]},
			{"Name" : "ipv6ShiftFifo_V_keep", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "route_by_eth_protoco_U0", "Port" : "ipv6ShiftFifo_V_keep"},
					{"ID" : "7", "SubInstance" : "rshiftWordByOctet_U0", "Port" : "ipv6ShiftFifo_V_keep"}]},
			{"Name" : "ipv6ShiftFifo_V_last", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "route_by_eth_protoco_U0", "Port" : "ipv6ShiftFifo_V_last"},
					{"ID" : "7", "SubInstance" : "rshiftWordByOctet_U0", "Port" : "ipv6ShiftFifo_V_last"}]},
			{"Name" : "fsmState_1", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "6", "SubInstance" : "rshiftWordByOctet_1_U0", "Port" : "fsmState_1"}]},
			{"Name" : "prevWord_data_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "6", "SubInstance" : "rshiftWordByOctet_1_U0", "Port" : "prevWord_data_V"}]},
			{"Name" : "prevWord_keep_V_1", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "6", "SubInstance" : "rshiftWordByOctet_1_U0", "Port" : "prevWord_keep_V_1"}]},
			{"Name" : "rs_firstWord_1", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "6", "SubInstance" : "rshiftWordByOctet_1_U0", "Port" : "rs_firstWord_1"}]},
			{"Name" : "ipDataFifo_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "extract_ip_meta_64_U0", "Port" : "ipDataFifo_V"},
					{"ID" : "6", "SubInstance" : "rshiftWordByOctet_1_U0", "Port" : "ipDataFifo_V"}]},
			{"Name" : "fsmState", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "7", "SubInstance" : "rshiftWordByOctet_U0", "Port" : "fsmState"}]},
			{"Name" : "prevWord_data_V_1", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "7", "SubInstance" : "rshiftWordByOctet_U0", "Port" : "prevWord_data_V_1"}]},
			{"Name" : "prevWord_keep_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "7", "SubInstance" : "rshiftWordByOctet_U0", "Port" : "prevWord_keep_V"}]},
			{"Name" : "rs_firstWord", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "7", "SubInstance" : "rshiftWordByOctet_U0", "Port" : "rs_firstWord"}]},
			{"Name" : "ipv6DataFifo_V_data_s", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "21", "SubInstance" : "detect_ipv6_protocol_U0", "Port" : "ipv6DataFifo_V_data_s"},
					{"ID" : "7", "SubInstance" : "rshiftWordByOctet_U0", "Port" : "ipv6DataFifo_V_data_s"}]},
			{"Name" : "ipv6DataFifo_V_keep_s", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "21", "SubInstance" : "detect_ipv6_protocol_U0", "Port" : "ipv6DataFifo_V_keep_s"},
					{"ID" : "7", "SubInstance" : "rshiftWordByOctet_U0", "Port" : "ipv6DataFifo_V_keep_s"}]},
			{"Name" : "ipv6DataFifo_V_last_s", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "21", "SubInstance" : "detect_ipv6_protocol_U0", "Port" : "ipv6DataFifo_V_last_s"},
					{"ID" : "7", "SubInstance" : "rshiftWordByOctet_U0", "Port" : "ipv6DataFifo_V_last_s"}]},
			{"Name" : "header_ready_1", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "extract_ip_meta_64_U0", "Port" : "header_ready_1"}]},
			{"Name" : "header_idx", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "extract_ip_meta_64_U0", "Port" : "header_idx"}]},
			{"Name" : "header_header_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "extract_ip_meta_64_U0", "Port" : "header_header_V"}]},
			{"Name" : "ipDataMetaFifo_V_dat", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "extract_ip_meta_64_U0", "Port" : "ipDataMetaFifo_V_dat"},
					{"ID" : "9", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "ipDataMetaFifo_V_dat"}]},
			{"Name" : "ipDataMetaFifo_V_kee", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "extract_ip_meta_64_U0", "Port" : "ipDataMetaFifo_V_kee"},
					{"ID" : "9", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "ipDataMetaFifo_V_kee"}]},
			{"Name" : "ipDataMetaFifo_V_las", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "extract_ip_meta_64_U0", "Port" : "ipDataMetaFifo_V_las"},
					{"ID" : "9", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "ipDataMetaFifo_V_las"}]},
			{"Name" : "metaWritten", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "extract_ip_meta_64_U0", "Port" : "metaWritten"}]},
			{"Name" : "validIpAddressFifo_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "extract_ip_meta_64_U0", "Port" : "validIpAddressFifo_V"},
					{"ID" : "11", "SubInstance" : "ip_invalid_dropper_U0", "Port" : "validIpAddressFifo_V"}]},
			{"Name" : "ipv4ProtocolFifo_V_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "extract_ip_meta_64_U0", "Port" : "ipv4ProtocolFifo_V_V"},
					{"ID" : "14", "SubInstance" : "detect_ipv4_protocol_U0", "Port" : "ipv4ProtocolFifo_V_V"}]},
			{"Name" : "ipDataCheckFifo_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "9", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "ipDataCheckFifo_V"},
					{"ID" : "11", "SubInstance" : "ip_invalid_dropper_U0", "Port" : "ipDataCheckFifo_V"}]},
			{"Name" : "cics_wordCount_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "9", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "cics_wordCount_V"}]},
			{"Name" : "cics_ip_sums_sum_V_0", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "9", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "cics_ip_sums_sum_V_0"}]},
			{"Name" : "cics_ip_sums_sum_V_1", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "9", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "cics_ip_sums_sum_V_1"}]},
			{"Name" : "cics_ip_sums_sum_V_2", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "9", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "cics_ip_sums_sum_V_2"}]},
			{"Name" : "cics_ip_sums_sum_V_3", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "9", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "cics_ip_sums_sum_V_3"}]},
			{"Name" : "cics_ipHeaderLen_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "9", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "cics_ipHeaderLen_V"}]},
			{"Name" : "iph_subSumsFifoOut_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "9", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "iph_subSumsFifoOut_V"},
					{"ID" : "10", "SubInstance" : "check_ipv4_checksum_U0", "Port" : "iph_subSumsFifoOut_V"}]},
			{"Name" : "validChecksumFifo_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "11", "SubInstance" : "ip_invalid_dropper_U0", "Port" : "validChecksumFifo_V"},
					{"ID" : "10", "SubInstance" : "check_ipv4_checksum_U0", "Port" : "validChecksumFifo_V"}]},
			{"Name" : "iid_state", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "11", "SubInstance" : "ip_invalid_dropper_U0", "Port" : "iid_state"}]},
			{"Name" : "ipv4ValidFifo_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "14", "SubInstance" : "detect_ipv4_protocol_U0", "Port" : "ipv4ValidFifo_V"},
					{"ID" : "11", "SubInstance" : "ip_invalid_dropper_U0", "Port" : "ipv4ValidFifo_V"}]},
			{"Name" : "ipDataDropFifo_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "11", "SubInstance" : "ip_invalid_dropper_U0", "Port" : "ipDataDropFifo_V"},
					{"ID" : "12", "SubInstance" : "cut_length_64_U0", "Port" : "ipDataDropFifo_V"}]},
			{"Name" : "cl_state", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "12", "SubInstance" : "cut_length_64_U0", "Port" : "cl_state"}]},
			{"Name" : "cl_wordCount_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "12", "SubInstance" : "cut_length_64_U0", "Port" : "cl_wordCount_V"}]},
			{"Name" : "cl_totalLength_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "12", "SubInstance" : "cut_length_64_U0", "Port" : "cl_totalLength_V"}]},
			{"Name" : "ipDataCutFifo_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "14", "SubInstance" : "detect_ipv4_protocol_U0", "Port" : "ipDataCutFifo_V"},
					{"ID" : "12", "SubInstance" : "cut_length_64_U0", "Port" : "ipDataCutFifo_V"}]},
			{"Name" : "dip_state", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "14", "SubInstance" : "detect_ipv4_protocol_U0", "Port" : "dip_state"}]},
			{"Name" : "dip_ipProtocol_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "14", "SubInstance" : "detect_ipv4_protocol_U0", "Port" : "dip_ipProtocol_V"}]},
			{"Name" : "udpDataFifo_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "28", "SubInstance" : "duplicate_stream_U0", "Port" : "udpDataFifo_V"},
					{"ID" : "14", "SubInstance" : "detect_ipv4_protocol_U0", "Port" : "udpDataFifo_V"}]},
			{"Name" : "state_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "21", "SubInstance" : "detect_ipv6_protocol_U0", "Port" : "state_V"}]},
			{"Name" : "nextHeader_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "21", "SubInstance" : "detect_ipv6_protocol_U0", "Port" : "nextHeader_V"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.detect_eth_protocol_U0", "Parent" : "0",
		"CDFG" : "detect_eth_protocol",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "4", "EstimateLatencyMin" : "4", "EstimateLatencyMax" : "4",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "dataIn_V_data_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "s_axis_raw_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataIn_V_keep_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "dataIn_V_last_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "header_ready", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_idx_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_header_V_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "metaWritten_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "etherTypeFifo_V_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "2", "DependentChan" : "35",
				"BlockSignal" : [
					{"Name" : "etherTypeFifo_V_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ethDataFifo_V_data_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "2", "DependentChan" : "36",
				"BlockSignal" : [
					{"Name" : "ethDataFifo_V_data_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ethDataFifo_V_keep_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "2", "DependentChan" : "37",
				"BlockSignal" : [
					{"Name" : "ethDataFifo_V_keep_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ethDataFifo_V_last_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "2", "DependentChan" : "38",
				"BlockSignal" : [
					{"Name" : "ethDataFifo_V_last_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.route_by_eth_protoco_U0", "Parent" : "0", "Child" : ["3", "4", "5"],
		"CDFG" : "route_by_eth_protoco",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "ARPdataOut_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_arp_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ARPdataOut_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "ARPdataOut_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "rep_fsmState_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "rep_etherType_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "etherTypeFifo_V_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "1", "DependentChan" : "35",
				"BlockSignal" : [
					{"Name" : "etherTypeFifo_V_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ethDataFifo_V_data_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "1", "DependentChan" : "36",
				"BlockSignal" : [
					{"Name" : "ethDataFifo_V_data_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ethDataFifo_V_keep_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "1", "DependentChan" : "37",
				"BlockSignal" : [
					{"Name" : "ethDataFifo_V_keep_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ethDataFifo_V_last_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "1", "DependentChan" : "38",
				"BlockSignal" : [
					{"Name" : "ethDataFifo_V_last_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv4ShiftFifo_V_data", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "6", "DependentChan" : "39",
				"BlockSignal" : [
					{"Name" : "ipv4ShiftFifo_V_data_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv4ShiftFifo_V_keep", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "6", "DependentChan" : "40",
				"BlockSignal" : [
					{"Name" : "ipv4ShiftFifo_V_keep_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv4ShiftFifo_V_last", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "6", "DependentChan" : "41",
				"BlockSignal" : [
					{"Name" : "ipv4ShiftFifo_V_last_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv6ShiftFifo_V_data", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "7", "DependentChan" : "42",
				"BlockSignal" : [
					{"Name" : "ipv6ShiftFifo_V_data_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv6ShiftFifo_V_keep", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "7", "DependentChan" : "43",
				"BlockSignal" : [
					{"Name" : "ipv6ShiftFifo_V_keep_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv6ShiftFifo_V_last", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "7", "DependentChan" : "44",
				"BlockSignal" : [
					{"Name" : "ipv6ShiftFifo_V_last_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "3", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.route_by_eth_protoco_U0.regslice_both_ARPdataOut_V_data_V_U", "Parent" : "2"},
	{"ID" : "4", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.route_by_eth_protoco_U0.regslice_both_ARPdataOut_V_keep_V_U", "Parent" : "2"},
	{"ID" : "5", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.route_by_eth_protoco_U0.regslice_both_ARPdataOut_V_last_V_U", "Parent" : "2"},
	{"ID" : "6", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.rshiftWordByOctet_1_U0", "Parent" : "0",
		"CDFG" : "rshiftWordByOctet_1",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "fsmState_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "prevWord_data_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "prevWord_keep_V_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ipv4ShiftFifo_V_data", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "39",
				"BlockSignal" : [
					{"Name" : "ipv4ShiftFifo_V_data_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv4ShiftFifo_V_keep", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "40",
				"BlockSignal" : [
					{"Name" : "ipv4ShiftFifo_V_keep_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv4ShiftFifo_V_last", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "41",
				"BlockSignal" : [
					{"Name" : "ipv4ShiftFifo_V_last_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rs_firstWord_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ipDataFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "8", "DependentChan" : "45",
				"BlockSignal" : [
					{"Name" : "ipDataFifo_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "7", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.rshiftWordByOctet_U0", "Parent" : "0",
		"CDFG" : "rshiftWordByOctet",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "fsmState", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "prevWord_data_V_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "prevWord_keep_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ipv6ShiftFifo_V_data", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "42",
				"BlockSignal" : [
					{"Name" : "ipv6ShiftFifo_V_data_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv6ShiftFifo_V_keep", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "43",
				"BlockSignal" : [
					{"Name" : "ipv6ShiftFifo_V_keep_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv6ShiftFifo_V_last", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "44",
				"BlockSignal" : [
					{"Name" : "ipv6ShiftFifo_V_last_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rs_firstWord", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ipv6DataFifo_V_data_s", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "21", "DependentChan" : "46",
				"BlockSignal" : [
					{"Name" : "ipv6DataFifo_V_data_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv6DataFifo_V_keep_s", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "21", "DependentChan" : "47",
				"BlockSignal" : [
					{"Name" : "ipv6DataFifo_V_keep_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv6DataFifo_V_last_s", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "21", "DependentChan" : "48",
				"BlockSignal" : [
					{"Name" : "ipv6DataFifo_V_last_s_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "8", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.extract_ip_meta_64_U0", "Parent" : "0",
		"CDFG" : "extract_ip_meta_64_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "5", "EstimateLatencyMin" : "5", "EstimateLatencyMax" : "5",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "myIpAddress_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "ipDataFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "6", "DependentChan" : "45",
				"BlockSignal" : [
					{"Name" : "ipDataFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "header_ready_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_idx", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_header_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ipDataMetaFifo_V_dat", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "9", "DependentChan" : "49",
				"BlockSignal" : [
					{"Name" : "ipDataMetaFifo_V_dat_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipDataMetaFifo_V_kee", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "9", "DependentChan" : "50",
				"BlockSignal" : [
					{"Name" : "ipDataMetaFifo_V_kee_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipDataMetaFifo_V_las", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "9", "DependentChan" : "51",
				"BlockSignal" : [
					{"Name" : "ipDataMetaFifo_V_las_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "metaWritten", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "validIpAddressFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "11", "DependentChan" : "52",
				"BlockSignal" : [
					{"Name" : "validIpAddressFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv4ProtocolFifo_V_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "14", "DependentChan" : "53",
				"BlockSignal" : [
					{"Name" : "ipv4ProtocolFifo_V_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "9", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.compute_ipv4_checksu_U0", "Parent" : "0",
		"CDFG" : "compute_ipv4_checksu",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "3", "EstimateLatencyMin" : "3", "EstimateLatencyMax" : "3",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "ipDataMetaFifo_V_dat", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "8", "DependentChan" : "49",
				"BlockSignal" : [
					{"Name" : "ipDataMetaFifo_V_dat_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipDataMetaFifo_V_kee", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "8", "DependentChan" : "50",
				"BlockSignal" : [
					{"Name" : "ipDataMetaFifo_V_kee_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipDataMetaFifo_V_las", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "8", "DependentChan" : "51",
				"BlockSignal" : [
					{"Name" : "ipDataMetaFifo_V_las_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipDataCheckFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "11", "DependentChan" : "54",
				"BlockSignal" : [
					{"Name" : "ipDataCheckFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "cics_wordCount_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ip_sums_sum_V_0", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ip_sums_sum_V_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ip_sums_sum_V_2", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ip_sums_sum_V_3", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ipHeaderLen_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "iph_subSumsFifoOut_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "10", "DependentChan" : "55",
				"BlockSignal" : [
					{"Name" : "iph_subSumsFifoOut_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "10", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.check_ipv4_checksum_U0", "Parent" : "0",
		"CDFG" : "check_ipv4_checksum",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "4", "EstimateLatencyMin" : "4", "EstimateLatencyMax" : "4",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "iph_subSumsFifoOut_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "9", "DependentChan" : "55",
				"BlockSignal" : [
					{"Name" : "iph_subSumsFifoOut_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "validChecksumFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "11", "DependentChan" : "56",
				"BlockSignal" : [
					{"Name" : "validChecksumFifo_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "11", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ip_invalid_dropper_U0", "Parent" : "0",
		"CDFG" : "ip_invalid_dropper",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "iid_state", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "validChecksumFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "10", "DependentChan" : "56",
				"BlockSignal" : [
					{"Name" : "validChecksumFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "validIpAddressFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "8", "DependentChan" : "52",
				"BlockSignal" : [
					{"Name" : "validIpAddressFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv4ValidFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "14", "DependentChan" : "57",
				"BlockSignal" : [
					{"Name" : "ipv4ValidFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipDataCheckFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "9", "DependentChan" : "54",
				"BlockSignal" : [
					{"Name" : "ipDataCheckFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipDataDropFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "12", "DependentChan" : "58",
				"BlockSignal" : [
					{"Name" : "ipDataDropFifo_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "12", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.cut_length_64_U0", "Parent" : "0", "Child" : ["13"],
		"CDFG" : "cut_length_64_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "cl_state", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ipDataDropFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "11", "DependentChan" : "58",
				"BlockSignal" : [
					{"Name" : "ipDataDropFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "cl_wordCount_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cl_totalLength_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ipDataCutFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "14", "DependentChan" : "59",
				"BlockSignal" : [
					{"Name" : "ipDataCutFifo_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "13", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.cut_length_64_U0.ip_handler_top_mubkb_U51", "Parent" : "12"},
	{"ID" : "14", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.detect_ipv4_protocol_U0", "Parent" : "0", "Child" : ["15", "16", "17", "18", "19", "20"],
		"CDFG" : "detect_ipv4_protocol",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "ICMPdataOut_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_icmp_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ICMPdataOut_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "ICMPdataOut_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "TCPdataOut_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_tcp_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "TCPdataOut_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "TCPdataOut_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "dip_state", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ipv4ProtocolFifo_V_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "8", "DependentChan" : "53",
				"BlockSignal" : [
					{"Name" : "ipv4ProtocolFifo_V_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv4ValidFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "11", "DependentChan" : "57",
				"BlockSignal" : [
					{"Name" : "ipv4ValidFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dip_ipProtocol_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ipDataCutFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "12", "DependentChan" : "59",
				"BlockSignal" : [
					{"Name" : "ipDataCutFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "udpDataFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "28", "DependentChan" : "60",
				"BlockSignal" : [
					{"Name" : "udpDataFifo_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "15", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.detect_ipv4_protocol_U0.regslice_both_ICMPdataOut_V_data_V_U", "Parent" : "14"},
	{"ID" : "16", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.detect_ipv4_protocol_U0.regslice_both_ICMPdataOut_V_keep_V_U", "Parent" : "14"},
	{"ID" : "17", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.detect_ipv4_protocol_U0.regslice_both_ICMPdataOut_V_last_V_U", "Parent" : "14"},
	{"ID" : "18", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.detect_ipv4_protocol_U0.regslice_both_TCPdataOut_V_data_V_U", "Parent" : "14"},
	{"ID" : "19", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.detect_ipv4_protocol_U0.regslice_both_TCPdataOut_V_keep_V_U", "Parent" : "14"},
	{"ID" : "20", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.detect_ipv4_protocol_U0.regslice_both_TCPdataOut_V_last_V_U", "Parent" : "14"},
	{"ID" : "21", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.detect_ipv6_protocol_U0", "Parent" : "0", "Child" : ["22", "23", "24", "25", "26", "27"],
		"CDFG" : "detect_ipv6_protocol",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "icmpv6DataOut_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_icmpv6_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "icmpv6DataOut_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "icmpv6DataOut_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "ipv6UdpDataOut_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_ipv6udp_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv6UdpDataOut_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "ipv6UdpDataOut_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "state_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ipv6DataFifo_V_data_s", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "7", "DependentChan" : "46",
				"BlockSignal" : [
					{"Name" : "ipv6DataFifo_V_data_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv6DataFifo_V_keep_s", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "7", "DependentChan" : "47",
				"BlockSignal" : [
					{"Name" : "ipv6DataFifo_V_keep_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv6DataFifo_V_last_s", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "7", "DependentChan" : "48",
				"BlockSignal" : [
					{"Name" : "ipv6DataFifo_V_last_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "nextHeader_V", "Type" : "OVld", "Direction" : "IO"}]},
	{"ID" : "22", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.detect_ipv6_protocol_U0.regslice_both_icmpv6DataOut_V_data_V_U", "Parent" : "21"},
	{"ID" : "23", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.detect_ipv6_protocol_U0.regslice_both_icmpv6DataOut_V_keep_V_U", "Parent" : "21"},
	{"ID" : "24", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.detect_ipv6_protocol_U0.regslice_both_icmpv6DataOut_V_last_V_U", "Parent" : "21"},
	{"ID" : "25", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.detect_ipv6_protocol_U0.regslice_both_ipv6UdpDataOut_V_data_V_U", "Parent" : "21"},
	{"ID" : "26", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.detect_ipv6_protocol_U0.regslice_both_ipv6UdpDataOut_V_keep_V_U", "Parent" : "21"},
	{"ID" : "27", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.detect_ipv6_protocol_U0.regslice_both_ipv6UdpDataOut_V_last_V_U", "Parent" : "21"},
	{"ID" : "28", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.duplicate_stream_U0", "Parent" : "0", "Child" : ["29", "30", "31", "32", "33", "34"],
		"CDFG" : "duplicate_stream",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "out0_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_udp_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "out0_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "out0_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "out1_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_roce_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "out1_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "out1_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "udpDataFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "14", "DependentChan" : "60",
				"BlockSignal" : [
					{"Name" : "udpDataFifo_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "29", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.duplicate_stream_U0.regslice_both_out0_V_data_V_U", "Parent" : "28"},
	{"ID" : "30", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.duplicate_stream_U0.regslice_both_out0_V_keep_V_U", "Parent" : "28"},
	{"ID" : "31", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.duplicate_stream_U0.regslice_both_out0_V_last_V_U", "Parent" : "28"},
	{"ID" : "32", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.duplicate_stream_U0.regslice_both_out1_V_data_V_U", "Parent" : "28"},
	{"ID" : "33", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.duplicate_stream_U0.regslice_both_out1_V_keep_V_U", "Parent" : "28"},
	{"ID" : "34", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.duplicate_stream_U0.regslice_both_out1_V_last_V_U", "Parent" : "28"},
	{"ID" : "35", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.etherTypeFifo_V_V_U", "Parent" : "0"},
	{"ID" : "36", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ethDataFifo_V_data_V_U", "Parent" : "0"},
	{"ID" : "37", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ethDataFifo_V_keep_V_U", "Parent" : "0"},
	{"ID" : "38", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ethDataFifo_V_last_V_U", "Parent" : "0"},
	{"ID" : "39", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipv4ShiftFifo_V_data_U", "Parent" : "0"},
	{"ID" : "40", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipv4ShiftFifo_V_keep_U", "Parent" : "0"},
	{"ID" : "41", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipv4ShiftFifo_V_last_U", "Parent" : "0"},
	{"ID" : "42", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipv6ShiftFifo_V_data_U", "Parent" : "0"},
	{"ID" : "43", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipv6ShiftFifo_V_keep_U", "Parent" : "0"},
	{"ID" : "44", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipv6ShiftFifo_V_last_U", "Parent" : "0"},
	{"ID" : "45", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipDataFifo_V_U", "Parent" : "0"},
	{"ID" : "46", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipv6DataFifo_V_data_s_U", "Parent" : "0"},
	{"ID" : "47", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipv6DataFifo_V_keep_s_U", "Parent" : "0"},
	{"ID" : "48", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipv6DataFifo_V_last_s_U", "Parent" : "0"},
	{"ID" : "49", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipDataMetaFifo_V_dat_U", "Parent" : "0"},
	{"ID" : "50", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipDataMetaFifo_V_kee_U", "Parent" : "0"},
	{"ID" : "51", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipDataMetaFifo_V_las_U", "Parent" : "0"},
	{"ID" : "52", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.validIpAddressFifo_V_U", "Parent" : "0"},
	{"ID" : "53", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipv4ProtocolFifo_V_V_U", "Parent" : "0"},
	{"ID" : "54", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipDataCheckFifo_V_U", "Parent" : "0"},
	{"ID" : "55", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.iph_subSumsFifoOut_V_U", "Parent" : "0"},
	{"ID" : "56", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.validChecksumFifo_V_U", "Parent" : "0"},
	{"ID" : "57", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipv4ValidFifo_V_U", "Parent" : "0"},
	{"ID" : "58", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipDataDropFifo_V_U", "Parent" : "0"},
	{"ID" : "59", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipDataCutFifo_V_U", "Parent" : "0"},
	{"ID" : "60", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.udpDataFifo_V_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	ip_handler_top {
		s_axis_raw_V_data_V {Type I LastRead 0 FirstWrite -1}
		s_axis_raw_V_keep_V {Type I LastRead 0 FirstWrite -1}
		s_axis_raw_V_last_V {Type I LastRead 0 FirstWrite -1}
		m_axis_arp_V_data_V {Type O LastRead -1 FirstWrite 1}
		m_axis_arp_V_keep_V {Type O LastRead -1 FirstWrite 1}
		m_axis_arp_V_last_V {Type O LastRead -1 FirstWrite 1}
		m_axis_icmpv6_V_data_V {Type O LastRead -1 FirstWrite 1}
		m_axis_icmpv6_V_keep_V {Type O LastRead -1 FirstWrite 1}
		m_axis_icmpv6_V_last_V {Type O LastRead -1 FirstWrite 1}
		m_axis_ipv6udp_V_data_V {Type O LastRead -1 FirstWrite 1}
		m_axis_ipv6udp_V_keep_V {Type O LastRead -1 FirstWrite 1}
		m_axis_ipv6udp_V_last_V {Type O LastRead -1 FirstWrite 1}
		m_axis_icmp_V_data_V {Type O LastRead -1 FirstWrite 1}
		m_axis_icmp_V_keep_V {Type O LastRead -1 FirstWrite 1}
		m_axis_icmp_V_last_V {Type O LastRead -1 FirstWrite 1}
		m_axis_udp_V_data_V {Type O LastRead -1 FirstWrite 1}
		m_axis_udp_V_keep_V {Type O LastRead -1 FirstWrite 1}
		m_axis_udp_V_last_V {Type O LastRead -1 FirstWrite 1}
		m_axis_tcp_V_data_V {Type O LastRead -1 FirstWrite 1}
		m_axis_tcp_V_keep_V {Type O LastRead -1 FirstWrite 1}
		m_axis_tcp_V_last_V {Type O LastRead -1 FirstWrite 1}
		m_axis_roce_V_data_V {Type O LastRead -1 FirstWrite 1}
		m_axis_roce_V_keep_V {Type O LastRead -1 FirstWrite 1}
		m_axis_roce_V_last_V {Type O LastRead -1 FirstWrite 1}
		myIpAddress_V {Type I LastRead 10 FirstWrite -1}
		header_ready {Type IO LastRead -1 FirstWrite -1}
		header_idx_1 {Type IO LastRead -1 FirstWrite -1}
		header_header_V_1 {Type IO LastRead -1 FirstWrite -1}
		metaWritten_1 {Type IO LastRead -1 FirstWrite -1}
		etherTypeFifo_V_V {Type IO LastRead -1 FirstWrite -1}
		ethDataFifo_V_data_V {Type IO LastRead -1 FirstWrite -1}
		ethDataFifo_V_keep_V {Type IO LastRead -1 FirstWrite -1}
		ethDataFifo_V_last_V {Type IO LastRead -1 FirstWrite -1}
		rep_fsmState_V {Type IO LastRead -1 FirstWrite -1}
		rep_etherType_V {Type IO LastRead -1 FirstWrite -1}
		ipv4ShiftFifo_V_data {Type IO LastRead -1 FirstWrite -1}
		ipv4ShiftFifo_V_keep {Type IO LastRead -1 FirstWrite -1}
		ipv4ShiftFifo_V_last {Type IO LastRead -1 FirstWrite -1}
		ipv6ShiftFifo_V_data {Type IO LastRead -1 FirstWrite -1}
		ipv6ShiftFifo_V_keep {Type IO LastRead -1 FirstWrite -1}
		ipv6ShiftFifo_V_last {Type IO LastRead -1 FirstWrite -1}
		fsmState_1 {Type IO LastRead -1 FirstWrite -1}
		prevWord_data_V {Type IO LastRead -1 FirstWrite -1}
		prevWord_keep_V_1 {Type IO LastRead -1 FirstWrite -1}
		rs_firstWord_1 {Type IO LastRead -1 FirstWrite -1}
		ipDataFifo_V {Type IO LastRead -1 FirstWrite -1}
		fsmState {Type IO LastRead -1 FirstWrite -1}
		prevWord_data_V_1 {Type IO LastRead -1 FirstWrite -1}
		prevWord_keep_V {Type IO LastRead -1 FirstWrite -1}
		rs_firstWord {Type IO LastRead -1 FirstWrite -1}
		ipv6DataFifo_V_data_s {Type IO LastRead -1 FirstWrite -1}
		ipv6DataFifo_V_keep_s {Type IO LastRead -1 FirstWrite -1}
		ipv6DataFifo_V_last_s {Type IO LastRead -1 FirstWrite -1}
		header_ready_1 {Type IO LastRead -1 FirstWrite -1}
		header_idx {Type IO LastRead -1 FirstWrite -1}
		header_header_V {Type IO LastRead -1 FirstWrite -1}
		ipDataMetaFifo_V_dat {Type IO LastRead -1 FirstWrite -1}
		ipDataMetaFifo_V_kee {Type IO LastRead -1 FirstWrite -1}
		ipDataMetaFifo_V_las {Type IO LastRead -1 FirstWrite -1}
		metaWritten {Type IO LastRead -1 FirstWrite -1}
		validIpAddressFifo_V {Type IO LastRead -1 FirstWrite -1}
		ipv4ProtocolFifo_V_V {Type IO LastRead -1 FirstWrite -1}
		ipDataCheckFifo_V {Type IO LastRead -1 FirstWrite -1}
		cics_wordCount_V {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_0 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_1 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_2 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_3 {Type IO LastRead -1 FirstWrite -1}
		cics_ipHeaderLen_V {Type IO LastRead -1 FirstWrite -1}
		iph_subSumsFifoOut_V {Type IO LastRead -1 FirstWrite -1}
		validChecksumFifo_V {Type IO LastRead -1 FirstWrite -1}
		iid_state {Type IO LastRead -1 FirstWrite -1}
		ipv4ValidFifo_V {Type IO LastRead -1 FirstWrite -1}
		ipDataDropFifo_V {Type IO LastRead -1 FirstWrite -1}
		cl_state {Type IO LastRead -1 FirstWrite -1}
		cl_wordCount_V {Type IO LastRead -1 FirstWrite -1}
		cl_totalLength_V {Type IO LastRead -1 FirstWrite -1}
		ipDataCutFifo_V {Type IO LastRead -1 FirstWrite -1}
		dip_state {Type IO LastRead -1 FirstWrite -1}
		dip_ipProtocol_V {Type IO LastRead -1 FirstWrite -1}
		udpDataFifo_V {Type IO LastRead -1 FirstWrite -1}
		state_V {Type IO LastRead -1 FirstWrite -1}
		nextHeader_V {Type IO LastRead -1 FirstWrite -1}}
	detect_eth_protocol {
		dataIn_V_data_V {Type I LastRead 0 FirstWrite -1}
		dataIn_V_keep_V {Type I LastRead 0 FirstWrite -1}
		dataIn_V_last_V {Type I LastRead 0 FirstWrite -1}
		header_ready {Type IO LastRead -1 FirstWrite -1}
		header_idx_1 {Type IO LastRead -1 FirstWrite -1}
		header_header_V_1 {Type IO LastRead -1 FirstWrite -1}
		metaWritten_1 {Type IO LastRead -1 FirstWrite -1}
		etherTypeFifo_V_V {Type O LastRead -1 FirstWrite 4}
		ethDataFifo_V_data_V {Type O LastRead -1 FirstWrite 1}
		ethDataFifo_V_keep_V {Type O LastRead -1 FirstWrite 1}
		ethDataFifo_V_last_V {Type O LastRead -1 FirstWrite 1}}
	route_by_eth_protoco {
		ARPdataOut_V_data_V {Type O LastRead -1 FirstWrite 1}
		ARPdataOut_V_keep_V {Type O LastRead -1 FirstWrite 1}
		ARPdataOut_V_last_V {Type O LastRead -1 FirstWrite 1}
		rep_fsmState_V {Type IO LastRead -1 FirstWrite -1}
		rep_etherType_V {Type IO LastRead -1 FirstWrite -1}
		etherTypeFifo_V_V {Type I LastRead 0 FirstWrite -1}
		ethDataFifo_V_data_V {Type I LastRead 0 FirstWrite -1}
		ethDataFifo_V_keep_V {Type I LastRead 0 FirstWrite -1}
		ethDataFifo_V_last_V {Type I LastRead 0 FirstWrite -1}
		ipv4ShiftFifo_V_data {Type O LastRead -1 FirstWrite 1}
		ipv4ShiftFifo_V_keep {Type O LastRead -1 FirstWrite 1}
		ipv4ShiftFifo_V_last {Type O LastRead -1 FirstWrite 1}
		ipv6ShiftFifo_V_data {Type O LastRead -1 FirstWrite 1}
		ipv6ShiftFifo_V_keep {Type O LastRead -1 FirstWrite 1}
		ipv6ShiftFifo_V_last {Type O LastRead -1 FirstWrite 1}}
	rshiftWordByOctet_1 {
		fsmState_1 {Type IO LastRead -1 FirstWrite -1}
		prevWord_data_V {Type IO LastRead -1 FirstWrite -1}
		prevWord_keep_V_1 {Type IO LastRead -1 FirstWrite -1}
		ipv4ShiftFifo_V_data {Type I LastRead 0 FirstWrite -1}
		ipv4ShiftFifo_V_keep {Type I LastRead 0 FirstWrite -1}
		ipv4ShiftFifo_V_last {Type I LastRead 0 FirstWrite -1}
		rs_firstWord_1 {Type IO LastRead -1 FirstWrite -1}
		ipDataFifo_V {Type O LastRead -1 FirstWrite 1}}
	rshiftWordByOctet {
		fsmState {Type IO LastRead -1 FirstWrite -1}
		prevWord_data_V_1 {Type IO LastRead -1 FirstWrite -1}
		prevWord_keep_V {Type IO LastRead -1 FirstWrite -1}
		ipv6ShiftFifo_V_data {Type I LastRead 0 FirstWrite -1}
		ipv6ShiftFifo_V_keep {Type I LastRead 0 FirstWrite -1}
		ipv6ShiftFifo_V_last {Type I LastRead 0 FirstWrite -1}
		rs_firstWord {Type IO LastRead -1 FirstWrite -1}
		ipv6DataFifo_V_data_s {Type O LastRead -1 FirstWrite 1}
		ipv6DataFifo_V_keep_s {Type O LastRead -1 FirstWrite 1}
		ipv6DataFifo_V_last_s {Type O LastRead -1 FirstWrite 1}}
	extract_ip_meta_64_s {
		myIpAddress_V {Type I LastRead 0 FirstWrite -1}
		ipDataFifo_V {Type I LastRead 0 FirstWrite -1}
		header_ready_1 {Type IO LastRead -1 FirstWrite -1}
		header_idx {Type IO LastRead -1 FirstWrite -1}
		header_header_V {Type IO LastRead -1 FirstWrite -1}
		ipDataMetaFifo_V_dat {Type O LastRead -1 FirstWrite 1}
		ipDataMetaFifo_V_kee {Type O LastRead -1 FirstWrite 1}
		ipDataMetaFifo_V_las {Type O LastRead -1 FirstWrite 1}
		metaWritten {Type IO LastRead -1 FirstWrite -1}
		validIpAddressFifo_V {Type O LastRead -1 FirstWrite 5}
		ipv4ProtocolFifo_V_V {Type O LastRead -1 FirstWrite 5}}
	compute_ipv4_checksu {
		ipDataMetaFifo_V_dat {Type I LastRead 0 FirstWrite -1}
		ipDataMetaFifo_V_kee {Type I LastRead 0 FirstWrite -1}
		ipDataMetaFifo_V_las {Type I LastRead 0 FirstWrite -1}
		ipDataCheckFifo_V {Type O LastRead -1 FirstWrite 1}
		cics_wordCount_V {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_0 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_1 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_2 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_3 {Type IO LastRead -1 FirstWrite -1}
		cics_ipHeaderLen_V {Type IO LastRead -1 FirstWrite -1}
		iph_subSumsFifoOut_V {Type O LastRead -1 FirstWrite 3}}
	check_ipv4_checksum {
		iph_subSumsFifoOut_V {Type I LastRead 0 FirstWrite -1}
		validChecksumFifo_V {Type O LastRead -1 FirstWrite 4}}
	ip_invalid_dropper {
		iid_state {Type IO LastRead -1 FirstWrite -1}
		validChecksumFifo_V {Type I LastRead 0 FirstWrite -1}
		validIpAddressFifo_V {Type I LastRead 0 FirstWrite -1}
		ipv4ValidFifo_V {Type O LastRead -1 FirstWrite 1}
		ipDataCheckFifo_V {Type I LastRead 0 FirstWrite -1}
		ipDataDropFifo_V {Type O LastRead -1 FirstWrite 1}}
	cut_length_64_s {
		cl_state {Type IO LastRead -1 FirstWrite -1}
		ipDataDropFifo_V {Type I LastRead 0 FirstWrite -1}
		cl_wordCount_V {Type IO LastRead -1 FirstWrite -1}
		cl_totalLength_V {Type IO LastRead -1 FirstWrite -1}
		ipDataCutFifo_V {Type O LastRead -1 FirstWrite 1}}
	detect_ipv4_protocol {
		ICMPdataOut_V_data_V {Type O LastRead -1 FirstWrite 1}
		ICMPdataOut_V_keep_V {Type O LastRead -1 FirstWrite 1}
		ICMPdataOut_V_last_V {Type O LastRead -1 FirstWrite 1}
		TCPdataOut_V_data_V {Type O LastRead -1 FirstWrite 1}
		TCPdataOut_V_keep_V {Type O LastRead -1 FirstWrite 1}
		TCPdataOut_V_last_V {Type O LastRead -1 FirstWrite 1}
		dip_state {Type IO LastRead -1 FirstWrite -1}
		ipv4ProtocolFifo_V_V {Type I LastRead 0 FirstWrite -1}
		ipv4ValidFifo_V {Type I LastRead 0 FirstWrite -1}
		dip_ipProtocol_V {Type IO LastRead -1 FirstWrite -1}
		ipDataCutFifo_V {Type I LastRead 0 FirstWrite -1}
		udpDataFifo_V {Type O LastRead -1 FirstWrite 1}}
	detect_ipv6_protocol {
		icmpv6DataOut_V_data_V {Type O LastRead -1 FirstWrite 1}
		icmpv6DataOut_V_keep_V {Type O LastRead -1 FirstWrite 1}
		icmpv6DataOut_V_last_V {Type O LastRead -1 FirstWrite 1}
		ipv6UdpDataOut_V_data_V {Type O LastRead -1 FirstWrite 1}
		ipv6UdpDataOut_V_keep_V {Type O LastRead -1 FirstWrite 1}
		ipv6UdpDataOut_V_last_V {Type O LastRead -1 FirstWrite 1}
		state_V {Type IO LastRead -1 FirstWrite -1}
		ipv6DataFifo_V_data_s {Type I LastRead 0 FirstWrite -1}
		ipv6DataFifo_V_keep_s {Type I LastRead 0 FirstWrite -1}
		ipv6DataFifo_V_last_s {Type I LastRead 0 FirstWrite -1}
		nextHeader_V {Type IO LastRead -1 FirstWrite -1}}
	duplicate_stream {
		out0_V_data_V {Type O LastRead -1 FirstWrite 1}
		out0_V_keep_V {Type O LastRead -1 FirstWrite 1}
		out0_V_last_V {Type O LastRead -1 FirstWrite 1}
		out1_V_data_V {Type O LastRead -1 FirstWrite 1}
		out1_V_keep_V {Type O LastRead -1 FirstWrite 1}
		out1_V_last_V {Type O LastRead -1 FirstWrite 1}
		udpDataFifo_V {Type I LastRead 0 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "28", "Max" : "28"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
]}

set Spec2ImplPortList { 
	s_axis_raw_V_data_V { axis {  { s_axis_raw_TDATA in_data 0 64 } } }
	s_axis_raw_V_keep_V { axis {  { s_axis_raw_TKEEP in_data 0 8 } } }
	s_axis_raw_V_last_V { axis {  { s_axis_raw_TLAST in_data 0 1 }  { s_axis_raw_TVALID in_vld 0 1 }  { s_axis_raw_TREADY in_acc 1 1 } } }
	m_axis_arp_V_data_V { axis {  { m_axis_arp_TDATA out_data 1 64 } } }
	m_axis_arp_V_keep_V { axis {  { m_axis_arp_TKEEP out_data 1 8 } } }
	m_axis_arp_V_last_V { axis {  { m_axis_arp_TLAST out_data 1 1 }  { m_axis_arp_TVALID out_vld 1 1 }  { m_axis_arp_TREADY out_acc 0 1 } } }
	m_axis_icmpv6_V_data_V { axis {  { m_axis_icmpv6_TDATA out_data 1 64 } } }
	m_axis_icmpv6_V_keep_V { axis {  { m_axis_icmpv6_TKEEP out_data 1 8 } } }
	m_axis_icmpv6_V_last_V { axis {  { m_axis_icmpv6_TLAST out_data 1 1 }  { m_axis_icmpv6_TVALID out_vld 1 1 }  { m_axis_icmpv6_TREADY out_acc 0 1 } } }
	m_axis_ipv6udp_V_data_V { axis {  { m_axis_ipv6udp_TDATA out_data 1 64 } } }
	m_axis_ipv6udp_V_keep_V { axis {  { m_axis_ipv6udp_TKEEP out_data 1 8 } } }
	m_axis_ipv6udp_V_last_V { axis {  { m_axis_ipv6udp_TLAST out_data 1 1 }  { m_axis_ipv6udp_TVALID out_vld 1 1 }  { m_axis_ipv6udp_TREADY out_acc 0 1 } } }
	m_axis_icmp_V_data_V { axis {  { m_axis_icmp_TDATA out_data 1 64 } } }
	m_axis_icmp_V_keep_V { axis {  { m_axis_icmp_TKEEP out_data 1 8 } } }
	m_axis_icmp_V_last_V { axis {  { m_axis_icmp_TLAST out_data 1 1 }  { m_axis_icmp_TVALID out_vld 1 1 }  { m_axis_icmp_TREADY out_acc 0 1 } } }
	m_axis_udp_V_data_V { axis {  { m_axis_udp_TDATA out_data 1 64 } } }
	m_axis_udp_V_keep_V { axis {  { m_axis_udp_TKEEP out_data 1 8 } } }
	m_axis_udp_V_last_V { axis {  { m_axis_udp_TLAST out_data 1 1 }  { m_axis_udp_TVALID out_vld 1 1 }  { m_axis_udp_TREADY out_acc 0 1 } } }
	m_axis_tcp_V_data_V { axis {  { m_axis_tcp_TDATA out_data 1 64 } } }
	m_axis_tcp_V_keep_V { axis {  { m_axis_tcp_TKEEP out_data 1 8 } } }
	m_axis_tcp_V_last_V { axis {  { m_axis_tcp_TLAST out_data 1 1 }  { m_axis_tcp_TVALID out_vld 1 1 }  { m_axis_tcp_TREADY out_acc 0 1 } } }
	m_axis_roce_V_data_V { axis {  { m_axis_roce_TDATA out_data 1 64 } } }
	m_axis_roce_V_keep_V { axis {  { m_axis_roce_TKEEP out_data 1 8 } } }
	m_axis_roce_V_last_V { axis {  { m_axis_roce_TLAST out_data 1 1 }  { m_axis_roce_TVALID out_vld 1 1 }  { m_axis_roce_TREADY out_acc 0 1 } } }
	myIpAddress_V { ap_stable {  { myIpAddress_V in_data 0 32 } } }
}

set busDeadlockParameterList { 
}

# RTL port scheduling information:
set fifoSchedulingInfoList { 
}

# RTL bus port read request latency information:
set busReadReqLatencyList { 
}

# RTL bus port write response latency information:
set busWriteResLatencyList { 
}

# RTL array port load latency information:
set memoryLoadLatencyList { 
}
set moduleName ip_handler_top
set isTopModule 1
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type dataflow
set FunctionProtocol ap_ctrl_none
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {ip_handler_top}
set C_modelType { void 0 }
set C_modelArgList {
	{ s_axis_raw_V_data_V int 64 regular {axi_s 0 volatile  { s_axis_raw Data } }  }
	{ s_axis_raw_V_keep_V int 8 regular {axi_s 0 volatile  { s_axis_raw Keep } }  }
	{ s_axis_raw_V_last_V int 1 regular {axi_s 0 volatile  { s_axis_raw Last } }  }
	{ m_axis_arp_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_arp Data } }  }
	{ m_axis_arp_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_arp Keep } }  }
	{ m_axis_arp_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_arp Last } }  }
	{ m_axis_icmpv6_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_icmpv6 Data } }  }
	{ m_axis_icmpv6_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_icmpv6 Keep } }  }
	{ m_axis_icmpv6_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_icmpv6 Last } }  }
	{ m_axis_ipv6udp_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_ipv6udp Data } }  }
	{ m_axis_ipv6udp_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_ipv6udp Keep } }  }
	{ m_axis_ipv6udp_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_ipv6udp Last } }  }
	{ m_axis_icmp_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_icmp Data } }  }
	{ m_axis_icmp_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_icmp Keep } }  }
	{ m_axis_icmp_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_icmp Last } }  }
	{ m_axis_udp_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_udp Data } }  }
	{ m_axis_udp_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_udp Keep } }  }
	{ m_axis_udp_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_udp Last } }  }
	{ m_axis_tcp_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_tcp Data } }  }
	{ m_axis_tcp_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_tcp Keep } }  }
	{ m_axis_tcp_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_tcp Last } }  }
	{ m_axis_roce_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_roce Data } }  }
	{ m_axis_roce_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_roce Keep } }  }
	{ m_axis_roce_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_roce Last } }  }
	{ myIpAddress_V int 32 regular {axi_slave 0}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "s_axis_raw_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "READONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "s_axis_raw.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_raw_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "s_axis_raw.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_raw_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "s_axis_raw.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_arp_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "m_axis_arp.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_arp_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "m_axis_arp.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_arp_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "m_axis_arp.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_icmpv6_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "m_axis_icmpv6.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_icmpv6_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "m_axis_icmpv6.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_icmpv6_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "m_axis_icmpv6.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_ipv6udp_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "m_axis_ipv6udp.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_ipv6udp_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "m_axis_ipv6udp.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_ipv6udp_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "m_axis_ipv6udp.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_icmp_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "m_axis_icmp.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_icmp_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "m_axis_icmp.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_icmp_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "m_axis_icmp.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_udp_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "m_axis_udp.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_udp_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "m_axis_udp.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_udp_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "m_axis_udp.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_tcp_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "m_axis_tcp.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_tcp_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "m_axis_tcp.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_tcp_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "m_axis_tcp.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_roce_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "m_axis_roce.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_roce_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "m_axis_roce.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_roce_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "m_axis_roce.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "myIpAddress_V", "interface" : "axi_slave", "bundle":"AXILiteS","type":"ap_none","bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "myIpAddress.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}], "offset" : {"in":16}, "offset_end" : {"in":23}} ]}
# RTL Port declarations: 
set portNum 59
set portList { 
	{ s_axi_AXILiteS_AWVALID sc_in sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_AWREADY sc_out sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_AWADDR sc_in sc_lv 5 signal -1 } 
	{ s_axi_AXILiteS_WVALID sc_in sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_WREADY sc_out sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_WDATA sc_in sc_lv 32 signal -1 } 
	{ s_axi_AXILiteS_WSTRB sc_in sc_lv 4 signal -1 } 
	{ s_axi_AXILiteS_ARVALID sc_in sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_ARREADY sc_out sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_ARADDR sc_in sc_lv 5 signal -1 } 
	{ s_axi_AXILiteS_RVALID sc_out sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_RREADY sc_in sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_RDATA sc_out sc_lv 32 signal -1 } 
	{ s_axi_AXILiteS_RRESP sc_out sc_lv 2 signal -1 } 
	{ s_axi_AXILiteS_BVALID sc_out sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_BREADY sc_in sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_BRESP sc_out sc_lv 2 signal -1 } 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst_n sc_in sc_logic 1 reset -1 active_low_sync } 
	{ s_axis_raw_TDATA sc_in sc_lv 64 signal 0 } 
	{ s_axis_raw_TKEEP sc_in sc_lv 8 signal 1 } 
	{ s_axis_raw_TLAST sc_in sc_lv 1 signal 2 } 
	{ m_axis_arp_TDATA sc_out sc_lv 64 signal 3 } 
	{ m_axis_arp_TKEEP sc_out sc_lv 8 signal 4 } 
	{ m_axis_arp_TLAST sc_out sc_lv 1 signal 5 } 
	{ m_axis_icmpv6_TDATA sc_out sc_lv 64 signal 6 } 
	{ m_axis_icmpv6_TKEEP sc_out sc_lv 8 signal 7 } 
	{ m_axis_icmpv6_TLAST sc_out sc_lv 1 signal 8 } 
	{ m_axis_ipv6udp_TDATA sc_out sc_lv 64 signal 9 } 
	{ m_axis_ipv6udp_TKEEP sc_out sc_lv 8 signal 10 } 
	{ m_axis_ipv6udp_TLAST sc_out sc_lv 1 signal 11 } 
	{ m_axis_icmp_TDATA sc_out sc_lv 64 signal 12 } 
	{ m_axis_icmp_TKEEP sc_out sc_lv 8 signal 13 } 
	{ m_axis_icmp_TLAST sc_out sc_lv 1 signal 14 } 
	{ m_axis_udp_TDATA sc_out sc_lv 64 signal 15 } 
	{ m_axis_udp_TKEEP sc_out sc_lv 8 signal 16 } 
	{ m_axis_udp_TLAST sc_out sc_lv 1 signal 17 } 
	{ m_axis_tcp_TDATA sc_out sc_lv 64 signal 18 } 
	{ m_axis_tcp_TKEEP sc_out sc_lv 8 signal 19 } 
	{ m_axis_tcp_TLAST sc_out sc_lv 1 signal 20 } 
	{ m_axis_roce_TDATA sc_out sc_lv 64 signal 21 } 
	{ m_axis_roce_TKEEP sc_out sc_lv 8 signal 22 } 
	{ m_axis_roce_TLAST sc_out sc_lv 1 signal 23 } 
	{ s_axis_raw_TVALID sc_in sc_logic 1 invld 2 } 
	{ s_axis_raw_TREADY sc_out sc_logic 1 inacc 2 } 
	{ m_axis_arp_TVALID sc_out sc_logic 1 outvld 5 } 
	{ m_axis_arp_TREADY sc_in sc_logic 1 outacc 5 } 
	{ m_axis_icmp_TVALID sc_out sc_logic 1 outvld 14 } 
	{ m_axis_icmp_TREADY sc_in sc_logic 1 outacc 14 } 
	{ m_axis_tcp_TVALID sc_out sc_logic 1 outvld 20 } 
	{ m_axis_tcp_TREADY sc_in sc_logic 1 outacc 20 } 
	{ m_axis_icmpv6_TVALID sc_out sc_logic 1 outvld 8 } 
	{ m_axis_icmpv6_TREADY sc_in sc_logic 1 outacc 8 } 
	{ m_axis_ipv6udp_TVALID sc_out sc_logic 1 outvld 11 } 
	{ m_axis_ipv6udp_TREADY sc_in sc_logic 1 outacc 11 } 
	{ m_axis_udp_TVALID sc_out sc_logic 1 outvld 17 } 
	{ m_axis_udp_TREADY sc_in sc_logic 1 outacc 17 } 
	{ m_axis_roce_TVALID sc_out sc_logic 1 outvld 23 } 
	{ m_axis_roce_TREADY sc_in sc_logic 1 outacc 23 } 
}
set NewPortList {[ 
	{ "name": "s_axi_AXILiteS_AWADDR", "direction": "in", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "AXILiteS", "role": "AWADDR" },"address":[{"name":"myIpAddress_V","role":"data","value":"16"}] },
	{ "name": "s_axi_AXILiteS_AWVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "AWVALID" } },
	{ "name": "s_axi_AXILiteS_AWREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "AWREADY" } },
	{ "name": "s_axi_AXILiteS_WVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "WVALID" } },
	{ "name": "s_axi_AXILiteS_WREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "WREADY" } },
	{ "name": "s_axi_AXILiteS_WDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "AXILiteS", "role": "WDATA" } },
	{ "name": "s_axi_AXILiteS_WSTRB", "direction": "in", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "AXILiteS", "role": "WSTRB" } },
	{ "name": "s_axi_AXILiteS_ARADDR", "direction": "in", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "AXILiteS", "role": "ARADDR" },"address":[] },
	{ "name": "s_axi_AXILiteS_ARVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "ARVALID" } },
	{ "name": "s_axi_AXILiteS_ARREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "ARREADY" } },
	{ "name": "s_axi_AXILiteS_RVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "RVALID" } },
	{ "name": "s_axi_AXILiteS_RREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "RREADY" } },
	{ "name": "s_axi_AXILiteS_RDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "AXILiteS", "role": "RDATA" } },
	{ "name": "s_axi_AXILiteS_RRESP", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "AXILiteS", "role": "RRESP" } },
	{ "name": "s_axi_AXILiteS_BVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "BVALID" } },
	{ "name": "s_axi_AXILiteS_BREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "BREADY" } },
	{ "name": "s_axi_AXILiteS_BRESP", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "AXILiteS", "role": "BRESP" } }, 
 	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst_n", "role": "default" }} , 
 	{ "name": "s_axis_raw_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "s_axis_raw_V_data_V", "role": "default" }} , 
 	{ "name": "s_axis_raw_TKEEP", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "s_axis_raw_V_keep_V", "role": "default" }} , 
 	{ "name": "s_axis_raw_TLAST", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "s_axis_raw_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_arp_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "m_axis_arp_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_arp_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "m_axis_arp_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_arp_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "m_axis_arp_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_icmpv6_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "m_axis_icmpv6_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_icmpv6_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "m_axis_icmpv6_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_icmpv6_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "m_axis_icmpv6_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_ipv6udp_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "m_axis_ipv6udp_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_ipv6udp_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "m_axis_ipv6udp_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_ipv6udp_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "m_axis_ipv6udp_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_icmp_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "m_axis_icmp_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_icmp_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "m_axis_icmp_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_icmp_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "m_axis_icmp_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_udp_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "m_axis_udp_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_udp_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "m_axis_udp_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_udp_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "m_axis_udp_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_tcp_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "m_axis_tcp_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_tcp_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "m_axis_tcp_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_tcp_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "m_axis_tcp_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_roce_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "m_axis_roce_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_roce_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "m_axis_roce_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_roce_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "m_axis_roce_V_last_V", "role": "default" }} , 
 	{ "name": "s_axis_raw_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "s_axis_raw_V_last_V", "role": "default" }} , 
 	{ "name": "s_axis_raw_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "s_axis_raw_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_arp_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_arp_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_arp_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_arp_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_icmp_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_icmp_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_icmp_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_icmp_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_tcp_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_tcp_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_tcp_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_tcp_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_icmpv6_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_icmpv6_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_icmpv6_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_icmpv6_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_ipv6udp_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_ipv6udp_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_ipv6udp_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_ipv6udp_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_udp_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_udp_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_udp_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_udp_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_roce_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_roce_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_roce_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_roce_V_last_V", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43"],
		"CDFG" : "ip_handler_top",
		"Protocol" : "ap_ctrl_none",
		"ControlExist" : "0", "ap_start" : "0", "ap_ready" : "0", "ap_done" : "0", "ap_continue" : "0", "ap_idle" : "0",
		"Pipeline" : "Dataflow", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "1",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "26", "EstimateLatencyMax" : "26",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "1",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"InputProcess" : [
			{"ID" : "2", "Name" : "ip_handler_top_entry_U0"},
			{"ID" : "3", "Name" : "detect_eth_protocol3_U0"}],
		"OutputProcess" : [
			{"ID" : "4", "Name" : "route_by_eth_protoco_U0"},
			{"ID" : "13", "Name" : "detect_ipv4_protocol_U0"},
			{"ID" : "14", "Name" : "detect_ipv6_protocol_U0"},
			{"ID" : "15", "Name" : "duplicate_stream_U0"}],
		"Port" : [
			{"Name" : "s_axis_raw_V_data_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "detect_eth_protocol3_U0", "Port" : "dataIn_V_data_V"}]},
			{"Name" : "s_axis_raw_V_keep_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "detect_eth_protocol3_U0", "Port" : "dataIn_V_keep_V"}]},
			{"Name" : "s_axis_raw_V_last_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "detect_eth_protocol3_U0", "Port" : "dataIn_V_last_V"}]},
			{"Name" : "m_axis_arp_V_data_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "route_by_eth_protoco_U0", "Port" : "ARPdataOut_V_data_V"}]},
			{"Name" : "m_axis_arp_V_keep_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "route_by_eth_protoco_U0", "Port" : "ARPdataOut_V_keep_V"}]},
			{"Name" : "m_axis_arp_V_last_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "route_by_eth_protoco_U0", "Port" : "ARPdataOut_V_last_V"}]},
			{"Name" : "m_axis_icmpv6_V_data_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "14", "SubInstance" : "detect_ipv6_protocol_U0", "Port" : "icmpv6DataOut_V_data_V"}]},
			{"Name" : "m_axis_icmpv6_V_keep_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "14", "SubInstance" : "detect_ipv6_protocol_U0", "Port" : "icmpv6DataOut_V_keep_V"}]},
			{"Name" : "m_axis_icmpv6_V_last_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "14", "SubInstance" : "detect_ipv6_protocol_U0", "Port" : "icmpv6DataOut_V_last_V"}]},
			{"Name" : "m_axis_ipv6udp_V_data_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "14", "SubInstance" : "detect_ipv6_protocol_U0", "Port" : "ipv6UdpDataOut_V_data_V"}]},
			{"Name" : "m_axis_ipv6udp_V_keep_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "14", "SubInstance" : "detect_ipv6_protocol_U0", "Port" : "ipv6UdpDataOut_V_keep_V"}]},
			{"Name" : "m_axis_ipv6udp_V_last_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "14", "SubInstance" : "detect_ipv6_protocol_U0", "Port" : "ipv6UdpDataOut_V_last_V"}]},
			{"Name" : "m_axis_icmp_V_data_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "13", "SubInstance" : "detect_ipv4_protocol_U0", "Port" : "ICMPdataOut_V_data_V"}]},
			{"Name" : "m_axis_icmp_V_keep_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "13", "SubInstance" : "detect_ipv4_protocol_U0", "Port" : "ICMPdataOut_V_keep_V"}]},
			{"Name" : "m_axis_icmp_V_last_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "13", "SubInstance" : "detect_ipv4_protocol_U0", "Port" : "ICMPdataOut_V_last_V"}]},
			{"Name" : "m_axis_udp_V_data_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "15", "SubInstance" : "duplicate_stream_U0", "Port" : "out0_V_data_V"}]},
			{"Name" : "m_axis_udp_V_keep_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "15", "SubInstance" : "duplicate_stream_U0", "Port" : "out0_V_keep_V"}]},
			{"Name" : "m_axis_udp_V_last_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "15", "SubInstance" : "duplicate_stream_U0", "Port" : "out0_V_last_V"}]},
			{"Name" : "m_axis_tcp_V_data_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "13", "SubInstance" : "detect_ipv4_protocol_U0", "Port" : "TCPdataOut_V_data_V"}]},
			{"Name" : "m_axis_tcp_V_keep_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "13", "SubInstance" : "detect_ipv4_protocol_U0", "Port" : "TCPdataOut_V_keep_V"}]},
			{"Name" : "m_axis_tcp_V_last_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "13", "SubInstance" : "detect_ipv4_protocol_U0", "Port" : "TCPdataOut_V_last_V"}]},
			{"Name" : "m_axis_roce_V_data_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "15", "SubInstance" : "duplicate_stream_U0", "Port" : "out1_V_data_V"}]},
			{"Name" : "m_axis_roce_V_keep_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "15", "SubInstance" : "duplicate_stream_U0", "Port" : "out1_V_keep_V"}]},
			{"Name" : "m_axis_roce_V_last_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "15", "SubInstance" : "duplicate_stream_U0", "Port" : "out1_V_last_V"}]},
			{"Name" : "myIpAddress_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "header_ready", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "detect_eth_protocol3_U0", "Port" : "header_ready"}]},
			{"Name" : "header_idx_1", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "detect_eth_protocol3_U0", "Port" : "header_idx_1"}]},
			{"Name" : "header_header_V_1", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "detect_eth_protocol3_U0", "Port" : "header_header_V_1"}]},
			{"Name" : "metaWritten_1", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "detect_eth_protocol3_U0", "Port" : "metaWritten_1"}]},
			{"Name" : "etherTypeFifo_V_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "route_by_eth_protoco_U0", "Port" : "etherTypeFifo_V_V"},
					{"ID" : "3", "SubInstance" : "detect_eth_protocol3_U0", "Port" : "etherTypeFifo_V_V"}]},
			{"Name" : "ethDataFifo_V_data_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "route_by_eth_protoco_U0", "Port" : "ethDataFifo_V_data_V"},
					{"ID" : "3", "SubInstance" : "detect_eth_protocol3_U0", "Port" : "ethDataFifo_V_data_V"}]},
			{"Name" : "ethDataFifo_V_keep_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "route_by_eth_protoco_U0", "Port" : "ethDataFifo_V_keep_V"},
					{"ID" : "3", "SubInstance" : "detect_eth_protocol3_U0", "Port" : "ethDataFifo_V_keep_V"}]},
			{"Name" : "ethDataFifo_V_last_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "route_by_eth_protoco_U0", "Port" : "ethDataFifo_V_last_V"},
					{"ID" : "3", "SubInstance" : "detect_eth_protocol3_U0", "Port" : "ethDataFifo_V_last_V"}]},
			{"Name" : "rep_fsmState_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "route_by_eth_protoco_U0", "Port" : "rep_fsmState_V"}]},
			{"Name" : "rep_etherType_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "route_by_eth_protoco_U0", "Port" : "rep_etherType_V"}]},
			{"Name" : "ipv4ShiftFifo_V_data", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "5", "SubInstance" : "rshiftWordByOctet_1_U0", "Port" : "ipv4ShiftFifo_V_data"},
					{"ID" : "4", "SubInstance" : "route_by_eth_protoco_U0", "Port" : "ipv4ShiftFifo_V_data"}]},
			{"Name" : "ipv4ShiftFifo_V_keep", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "5", "SubInstance" : "rshiftWordByOctet_1_U0", "Port" : "ipv4ShiftFifo_V_keep"},
					{"ID" : "4", "SubInstance" : "route_by_eth_protoco_U0", "Port" : "ipv4ShiftFifo_V_keep"}]},
			{"Name" : "ipv4ShiftFifo_V_last", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "5", "SubInstance" : "rshiftWordByOctet_1_U0", "Port" : "ipv4ShiftFifo_V_last"},
					{"ID" : "4", "SubInstance" : "route_by_eth_protoco_U0", "Port" : "ipv4ShiftFifo_V_last"}]},
			{"Name" : "ipv6ShiftFifo_V_data", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "6", "SubInstance" : "rshiftWordByOctet_U0", "Port" : "ipv6ShiftFifo_V_data"},
					{"ID" : "4", "SubInstance" : "route_by_eth_protoco_U0", "Port" : "ipv6ShiftFifo_V_data"}]},
			{"Name" : "ipv6ShiftFifo_V_keep", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "6", "SubInstance" : "rshiftWordByOctet_U0", "Port" : "ipv6ShiftFifo_V_keep"},
					{"ID" : "4", "SubInstance" : "route_by_eth_protoco_U0", "Port" : "ipv6ShiftFifo_V_keep"}]},
			{"Name" : "ipv6ShiftFifo_V_last", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "6", "SubInstance" : "rshiftWordByOctet_U0", "Port" : "ipv6ShiftFifo_V_last"},
					{"ID" : "4", "SubInstance" : "route_by_eth_protoco_U0", "Port" : "ipv6ShiftFifo_V_last"}]},
			{"Name" : "fsmState_1", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "5", "SubInstance" : "rshiftWordByOctet_1_U0", "Port" : "fsmState_1"}]},
			{"Name" : "prevWord_data_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "5", "SubInstance" : "rshiftWordByOctet_1_U0", "Port" : "prevWord_data_V"}]},
			{"Name" : "prevWord_keep_V_1", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "5", "SubInstance" : "rshiftWordByOctet_1_U0", "Port" : "prevWord_keep_V_1"}]},
			{"Name" : "rs_firstWord_1", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "5", "SubInstance" : "rshiftWordByOctet_1_U0", "Port" : "rs_firstWord_1"}]},
			{"Name" : "ipDataFifo_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "7", "SubInstance" : "extract_ip_meta_64_U0", "Port" : "ipDataFifo_V"},
					{"ID" : "5", "SubInstance" : "rshiftWordByOctet_1_U0", "Port" : "ipDataFifo_V"}]},
			{"Name" : "fsmState", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "6", "SubInstance" : "rshiftWordByOctet_U0", "Port" : "fsmState"}]},
			{"Name" : "prevWord_data_V_1", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "6", "SubInstance" : "rshiftWordByOctet_U0", "Port" : "prevWord_data_V_1"}]},
			{"Name" : "prevWord_keep_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "6", "SubInstance" : "rshiftWordByOctet_U0", "Port" : "prevWord_keep_V"}]},
			{"Name" : "rs_firstWord", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "6", "SubInstance" : "rshiftWordByOctet_U0", "Port" : "rs_firstWord"}]},
			{"Name" : "ipv6DataFifo_V_data_s", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "6", "SubInstance" : "rshiftWordByOctet_U0", "Port" : "ipv6DataFifo_V_data_s"},
					{"ID" : "14", "SubInstance" : "detect_ipv6_protocol_U0", "Port" : "ipv6DataFifo_V_data_s"}]},
			{"Name" : "ipv6DataFifo_V_keep_s", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "6", "SubInstance" : "rshiftWordByOctet_U0", "Port" : "ipv6DataFifo_V_keep_s"},
					{"ID" : "14", "SubInstance" : "detect_ipv6_protocol_U0", "Port" : "ipv6DataFifo_V_keep_s"}]},
			{"Name" : "ipv6DataFifo_V_last_s", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "6", "SubInstance" : "rshiftWordByOctet_U0", "Port" : "ipv6DataFifo_V_last_s"},
					{"ID" : "14", "SubInstance" : "detect_ipv6_protocol_U0", "Port" : "ipv6DataFifo_V_last_s"}]},
			{"Name" : "header_ready_1", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "7", "SubInstance" : "extract_ip_meta_64_U0", "Port" : "header_ready_1"}]},
			{"Name" : "header_idx", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "7", "SubInstance" : "extract_ip_meta_64_U0", "Port" : "header_idx"}]},
			{"Name" : "header_header_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "7", "SubInstance" : "extract_ip_meta_64_U0", "Port" : "header_header_V"}]},
			{"Name" : "ipDataMetaFifo_V_dat", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "7", "SubInstance" : "extract_ip_meta_64_U0", "Port" : "ipDataMetaFifo_V_dat"},
					{"ID" : "8", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "ipDataMetaFifo_V_dat"}]},
			{"Name" : "ipDataMetaFifo_V_kee", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "7", "SubInstance" : "extract_ip_meta_64_U0", "Port" : "ipDataMetaFifo_V_kee"},
					{"ID" : "8", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "ipDataMetaFifo_V_kee"}]},
			{"Name" : "ipDataMetaFifo_V_las", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "7", "SubInstance" : "extract_ip_meta_64_U0", "Port" : "ipDataMetaFifo_V_las"},
					{"ID" : "8", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "ipDataMetaFifo_V_las"}]},
			{"Name" : "metaWritten", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "7", "SubInstance" : "extract_ip_meta_64_U0", "Port" : "metaWritten"}]},
			{"Name" : "validIpAddressFifo_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "10", "SubInstance" : "ip_invalid_dropper_U0", "Port" : "validIpAddressFifo_V"},
					{"ID" : "7", "SubInstance" : "extract_ip_meta_64_U0", "Port" : "validIpAddressFifo_V"}]},
			{"Name" : "ipv4ProtocolFifo_V_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "7", "SubInstance" : "extract_ip_meta_64_U0", "Port" : "ipv4ProtocolFifo_V_V"},
					{"ID" : "13", "SubInstance" : "detect_ipv4_protocol_U0", "Port" : "ipv4ProtocolFifo_V_V"}]},
			{"Name" : "ipDataCheckFifo_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "10", "SubInstance" : "ip_invalid_dropper_U0", "Port" : "ipDataCheckFifo_V"},
					{"ID" : "8", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "ipDataCheckFifo_V"}]},
			{"Name" : "cics_wordCount_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "cics_wordCount_V"}]},
			{"Name" : "cics_ip_sums_sum_V_0", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "cics_ip_sums_sum_V_0"}]},
			{"Name" : "cics_ip_sums_sum_V_1", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "cics_ip_sums_sum_V_1"}]},
			{"Name" : "cics_ip_sums_sum_V_2", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "cics_ip_sums_sum_V_2"}]},
			{"Name" : "cics_ip_sums_sum_V_3", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "cics_ip_sums_sum_V_3"}]},
			{"Name" : "cics_ipHeaderLen_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "8", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "cics_ipHeaderLen_V"}]},
			{"Name" : "iph_subSumsFifoOut_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "9", "SubInstance" : "check_ipv4_checksum_U0", "Port" : "iph_subSumsFifoOut_V"},
					{"ID" : "8", "SubInstance" : "compute_ipv4_checksu_U0", "Port" : "iph_subSumsFifoOut_V"}]},
			{"Name" : "validChecksumFifo_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "9", "SubInstance" : "check_ipv4_checksum_U0", "Port" : "validChecksumFifo_V"},
					{"ID" : "10", "SubInstance" : "ip_invalid_dropper_U0", "Port" : "validChecksumFifo_V"}]},
			{"Name" : "iid_state", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "10", "SubInstance" : "ip_invalid_dropper_U0", "Port" : "iid_state"}]},
			{"Name" : "ipv4ValidFifo_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "10", "SubInstance" : "ip_invalid_dropper_U0", "Port" : "ipv4ValidFifo_V"},
					{"ID" : "13", "SubInstance" : "detect_ipv4_protocol_U0", "Port" : "ipv4ValidFifo_V"}]},
			{"Name" : "ipDataDropFifo_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "10", "SubInstance" : "ip_invalid_dropper_U0", "Port" : "ipDataDropFifo_V"},
					{"ID" : "11", "SubInstance" : "cut_length_64_U0", "Port" : "ipDataDropFifo_V"}]},
			{"Name" : "cl_state", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "11", "SubInstance" : "cut_length_64_U0", "Port" : "cl_state"}]},
			{"Name" : "cl_wordCount_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "11", "SubInstance" : "cut_length_64_U0", "Port" : "cl_wordCount_V"}]},
			{"Name" : "cl_totalLength_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "11", "SubInstance" : "cut_length_64_U0", "Port" : "cl_totalLength_V"}]},
			{"Name" : "ipDataCutFifo_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "13", "SubInstance" : "detect_ipv4_protocol_U0", "Port" : "ipDataCutFifo_V"},
					{"ID" : "11", "SubInstance" : "cut_length_64_U0", "Port" : "ipDataCutFifo_V"}]},
			{"Name" : "dip_state", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "13", "SubInstance" : "detect_ipv4_protocol_U0", "Port" : "dip_state"}]},
			{"Name" : "dip_ipProtocol_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "13", "SubInstance" : "detect_ipv4_protocol_U0", "Port" : "dip_ipProtocol_V"}]},
			{"Name" : "udpDataFifo_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "13", "SubInstance" : "detect_ipv4_protocol_U0", "Port" : "udpDataFifo_V"},
					{"ID" : "15", "SubInstance" : "duplicate_stream_U0", "Port" : "udpDataFifo_V"}]},
			{"Name" : "state_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "14", "SubInstance" : "detect_ipv6_protocol_U0", "Port" : "state_V"}]},
			{"Name" : "nextHeader_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "14", "SubInstance" : "detect_ipv6_protocol_U0", "Port" : "nextHeader_V"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ip_handler_top_AXILiteS_s_axi_U", "Parent" : "0"},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ip_handler_top_entry_U0", "Parent" : "0",
		"CDFG" : "ip_handler_top_entry",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "0", "EstimateLatencyMin" : "0", "EstimateLatencyMax" : "0",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "0",
		"Port" : [
			{"Name" : "myIpAddress_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "myIpAddress_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "3", "DependentChan" : "16",
				"BlockSignal" : [
					{"Name" : "myIpAddress_V_out_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "3", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.detect_eth_protocol3_U0", "Parent" : "0",
		"CDFG" : "detect_eth_protocol3",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "3", "EstimateLatencyMin" : "3", "EstimateLatencyMax" : "3",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "dataIn_V_data_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "s_axis_raw_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataIn_V_keep_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "dataIn_V_last_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "myIpAddress_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "16",
				"BlockSignal" : [
					{"Name" : "myIpAddress_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "myIpAddress_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "7", "DependentChan" : "17",
				"BlockSignal" : [
					{"Name" : "myIpAddress_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "header_ready", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_idx_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_header_V_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "metaWritten_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "etherTypeFifo_V_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "4", "DependentChan" : "18",
				"BlockSignal" : [
					{"Name" : "etherTypeFifo_V_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ethDataFifo_V_data_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "4", "DependentChan" : "19",
				"BlockSignal" : [
					{"Name" : "ethDataFifo_V_data_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ethDataFifo_V_keep_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "4", "DependentChan" : "20",
				"BlockSignal" : [
					{"Name" : "ethDataFifo_V_keep_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ethDataFifo_V_last_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "4", "DependentChan" : "21",
				"BlockSignal" : [
					{"Name" : "ethDataFifo_V_last_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "4", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.route_by_eth_protoco_U0", "Parent" : "0",
		"CDFG" : "route_by_eth_protoco",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "ARPdataOut_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_arp_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ARPdataOut_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "ARPdataOut_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "rep_fsmState_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "rep_etherType_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "etherTypeFifo_V_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "3", "DependentChan" : "18",
				"BlockSignal" : [
					{"Name" : "etherTypeFifo_V_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ethDataFifo_V_data_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "3", "DependentChan" : "19",
				"BlockSignal" : [
					{"Name" : "ethDataFifo_V_data_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ethDataFifo_V_keep_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "3", "DependentChan" : "20",
				"BlockSignal" : [
					{"Name" : "ethDataFifo_V_keep_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ethDataFifo_V_last_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "3", "DependentChan" : "21",
				"BlockSignal" : [
					{"Name" : "ethDataFifo_V_last_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv4ShiftFifo_V_data", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "5", "DependentChan" : "22",
				"BlockSignal" : [
					{"Name" : "ipv4ShiftFifo_V_data_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv4ShiftFifo_V_keep", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "5", "DependentChan" : "23",
				"BlockSignal" : [
					{"Name" : "ipv4ShiftFifo_V_keep_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv4ShiftFifo_V_last", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "5", "DependentChan" : "24",
				"BlockSignal" : [
					{"Name" : "ipv4ShiftFifo_V_last_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv6ShiftFifo_V_data", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "6", "DependentChan" : "25",
				"BlockSignal" : [
					{"Name" : "ipv6ShiftFifo_V_data_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv6ShiftFifo_V_keep", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "6", "DependentChan" : "26",
				"BlockSignal" : [
					{"Name" : "ipv6ShiftFifo_V_keep_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv6ShiftFifo_V_last", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "6", "DependentChan" : "27",
				"BlockSignal" : [
					{"Name" : "ipv6ShiftFifo_V_last_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "5", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.rshiftWordByOctet_1_U0", "Parent" : "0",
		"CDFG" : "rshiftWordByOctet_1",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "fsmState_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "prevWord_data_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "prevWord_keep_V_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ipv4ShiftFifo_V_data", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "4", "DependentChan" : "22",
				"BlockSignal" : [
					{"Name" : "ipv4ShiftFifo_V_data_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv4ShiftFifo_V_keep", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "4", "DependentChan" : "23",
				"BlockSignal" : [
					{"Name" : "ipv4ShiftFifo_V_keep_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv4ShiftFifo_V_last", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "4", "DependentChan" : "24",
				"BlockSignal" : [
					{"Name" : "ipv4ShiftFifo_V_last_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rs_firstWord_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ipDataFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "7", "DependentChan" : "28",
				"BlockSignal" : [
					{"Name" : "ipDataFifo_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "6", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.rshiftWordByOctet_U0", "Parent" : "0",
		"CDFG" : "rshiftWordByOctet",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "fsmState", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "prevWord_data_V_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "prevWord_keep_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ipv6ShiftFifo_V_data", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "4", "DependentChan" : "25",
				"BlockSignal" : [
					{"Name" : "ipv6ShiftFifo_V_data_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv6ShiftFifo_V_keep", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "4", "DependentChan" : "26",
				"BlockSignal" : [
					{"Name" : "ipv6ShiftFifo_V_keep_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv6ShiftFifo_V_last", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "4", "DependentChan" : "27",
				"BlockSignal" : [
					{"Name" : "ipv6ShiftFifo_V_last_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rs_firstWord", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ipv6DataFifo_V_data_s", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "14", "DependentChan" : "29",
				"BlockSignal" : [
					{"Name" : "ipv6DataFifo_V_data_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv6DataFifo_V_keep_s", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "14", "DependentChan" : "30",
				"BlockSignal" : [
					{"Name" : "ipv6DataFifo_V_keep_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv6DataFifo_V_last_s", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "14", "DependentChan" : "31",
				"BlockSignal" : [
					{"Name" : "ipv6DataFifo_V_last_s_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "7", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.extract_ip_meta_64_U0", "Parent" : "0",
		"CDFG" : "extract_ip_meta_64_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "4", "EstimateLatencyMin" : "4", "EstimateLatencyMax" : "4",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "myIpAddress_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "3", "DependentChan" : "17",
				"BlockSignal" : [
					{"Name" : "myIpAddress_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipDataFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "5", "DependentChan" : "28",
				"BlockSignal" : [
					{"Name" : "ipDataFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "header_ready_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_idx", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_header_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ipDataMetaFifo_V_dat", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "8", "DependentChan" : "32",
				"BlockSignal" : [
					{"Name" : "ipDataMetaFifo_V_dat_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipDataMetaFifo_V_kee", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "8", "DependentChan" : "33",
				"BlockSignal" : [
					{"Name" : "ipDataMetaFifo_V_kee_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipDataMetaFifo_V_las", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "8", "DependentChan" : "34",
				"BlockSignal" : [
					{"Name" : "ipDataMetaFifo_V_las_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "metaWritten", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "validIpAddressFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "10", "DependentChan" : "35",
				"BlockSignal" : [
					{"Name" : "validIpAddressFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv4ProtocolFifo_V_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "13", "DependentChan" : "36",
				"BlockSignal" : [
					{"Name" : "ipv4ProtocolFifo_V_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "8", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.compute_ipv4_checksu_U0", "Parent" : "0",
		"CDFG" : "compute_ipv4_checksu",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "ipDataMetaFifo_V_dat", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "7", "DependentChan" : "32",
				"BlockSignal" : [
					{"Name" : "ipDataMetaFifo_V_dat_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipDataMetaFifo_V_kee", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "7", "DependentChan" : "33",
				"BlockSignal" : [
					{"Name" : "ipDataMetaFifo_V_kee_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipDataMetaFifo_V_las", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "7", "DependentChan" : "34",
				"BlockSignal" : [
					{"Name" : "ipDataMetaFifo_V_las_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipDataCheckFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "10", "DependentChan" : "37",
				"BlockSignal" : [
					{"Name" : "ipDataCheckFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "cics_wordCount_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ip_sums_sum_V_0", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ip_sums_sum_V_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ip_sums_sum_V_2", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ip_sums_sum_V_3", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_ipHeaderLen_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "iph_subSumsFifoOut_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "9", "DependentChan" : "38",
				"BlockSignal" : [
					{"Name" : "iph_subSumsFifoOut_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "9", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.check_ipv4_checksum_U0", "Parent" : "0",
		"CDFG" : "check_ipv4_checksum",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "4", "EstimateLatencyMin" : "4", "EstimateLatencyMax" : "4",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "iph_subSumsFifoOut_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "8", "DependentChan" : "38",
				"BlockSignal" : [
					{"Name" : "iph_subSumsFifoOut_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "validChecksumFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "10", "DependentChan" : "39",
				"BlockSignal" : [
					{"Name" : "validChecksumFifo_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "10", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ip_invalid_dropper_U0", "Parent" : "0",
		"CDFG" : "ip_invalid_dropper",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "iid_state", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "validChecksumFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "9", "DependentChan" : "39",
				"BlockSignal" : [
					{"Name" : "validChecksumFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "validIpAddressFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "7", "DependentChan" : "35",
				"BlockSignal" : [
					{"Name" : "validIpAddressFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv4ValidFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "13", "DependentChan" : "40",
				"BlockSignal" : [
					{"Name" : "ipv4ValidFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipDataCheckFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "8", "DependentChan" : "37",
				"BlockSignal" : [
					{"Name" : "ipDataCheckFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipDataDropFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "11", "DependentChan" : "41",
				"BlockSignal" : [
					{"Name" : "ipDataDropFifo_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "11", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.cut_length_64_U0", "Parent" : "0", "Child" : ["12"],
		"CDFG" : "cut_length_64_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "cl_state", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ipDataDropFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "10", "DependentChan" : "41",
				"BlockSignal" : [
					{"Name" : "ipDataDropFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "cl_wordCount_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cl_totalLength_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ipDataCutFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "13", "DependentChan" : "42",
				"BlockSignal" : [
					{"Name" : "ipDataCutFifo_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "12", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.cut_length_64_U0.ip_handler_top_mux_646_64_1_1_U54", "Parent" : "11"},
	{"ID" : "13", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.detect_ipv4_protocol_U0", "Parent" : "0",
		"CDFG" : "detect_ipv4_protocol",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "ICMPdataOut_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_icmp_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ICMPdataOut_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "ICMPdataOut_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "TCPdataOut_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_tcp_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "TCPdataOut_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "TCPdataOut_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "dip_state", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ipv4ProtocolFifo_V_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "7", "DependentChan" : "36",
				"BlockSignal" : [
					{"Name" : "ipv4ProtocolFifo_V_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv4ValidFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "10", "DependentChan" : "40",
				"BlockSignal" : [
					{"Name" : "ipv4ValidFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dip_ipProtocol_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ipDataCutFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "11", "DependentChan" : "42",
				"BlockSignal" : [
					{"Name" : "ipDataCutFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "udpDataFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "15", "DependentChan" : "43",
				"BlockSignal" : [
					{"Name" : "udpDataFifo_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "14", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.detect_ipv6_protocol_U0", "Parent" : "0",
		"CDFG" : "detect_ipv6_protocol",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "icmpv6DataOut_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_icmpv6_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "icmpv6DataOut_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "icmpv6DataOut_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "ipv6UdpDataOut_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_ipv6udp_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv6UdpDataOut_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "ipv6UdpDataOut_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "state_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ipv6DataFifo_V_data_s", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "6", "DependentChan" : "29",
				"BlockSignal" : [
					{"Name" : "ipv6DataFifo_V_data_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv6DataFifo_V_keep_s", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "6", "DependentChan" : "30",
				"BlockSignal" : [
					{"Name" : "ipv6DataFifo_V_keep_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv6DataFifo_V_last_s", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "6", "DependentChan" : "31",
				"BlockSignal" : [
					{"Name" : "ipv6DataFifo_V_last_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "nextHeader_V", "Type" : "OVld", "Direction" : "IO"}]},
	{"ID" : "15", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.duplicate_stream_U0", "Parent" : "0",
		"CDFG" : "duplicate_stream",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "out0_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_udp_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "out0_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "out0_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "out1_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_roce_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "out1_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "out1_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "udpDataFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "13", "DependentChan" : "43",
				"BlockSignal" : [
					{"Name" : "udpDataFifo_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "16", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.myIpAddress_V_c1_U", "Parent" : "0"},
	{"ID" : "17", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.myIpAddress_V_c_U", "Parent" : "0"},
	{"ID" : "18", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.etherTypeFifo_V_V_U", "Parent" : "0"},
	{"ID" : "19", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ethDataFifo_V_data_V_U", "Parent" : "0"},
	{"ID" : "20", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ethDataFifo_V_keep_V_U", "Parent" : "0"},
	{"ID" : "21", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ethDataFifo_V_last_V_U", "Parent" : "0"},
	{"ID" : "22", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipv4ShiftFifo_V_data_U", "Parent" : "0"},
	{"ID" : "23", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipv4ShiftFifo_V_keep_U", "Parent" : "0"},
	{"ID" : "24", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipv4ShiftFifo_V_last_U", "Parent" : "0"},
	{"ID" : "25", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipv6ShiftFifo_V_data_U", "Parent" : "0"},
	{"ID" : "26", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipv6ShiftFifo_V_keep_U", "Parent" : "0"},
	{"ID" : "27", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipv6ShiftFifo_V_last_U", "Parent" : "0"},
	{"ID" : "28", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipDataFifo_V_U", "Parent" : "0"},
	{"ID" : "29", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipv6DataFifo_V_data_s_U", "Parent" : "0"},
	{"ID" : "30", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipv6DataFifo_V_keep_s_U", "Parent" : "0"},
	{"ID" : "31", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipv6DataFifo_V_last_s_U", "Parent" : "0"},
	{"ID" : "32", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipDataMetaFifo_V_dat_U", "Parent" : "0"},
	{"ID" : "33", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipDataMetaFifo_V_kee_U", "Parent" : "0"},
	{"ID" : "34", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipDataMetaFifo_V_las_U", "Parent" : "0"},
	{"ID" : "35", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.validIpAddressFifo_V_U", "Parent" : "0"},
	{"ID" : "36", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipv4ProtocolFifo_V_V_U", "Parent" : "0"},
	{"ID" : "37", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipDataCheckFifo_V_U", "Parent" : "0"},
	{"ID" : "38", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.iph_subSumsFifoOut_V_U", "Parent" : "0"},
	{"ID" : "39", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.validChecksumFifo_V_U", "Parent" : "0"},
	{"ID" : "40", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipv4ValidFifo_V_U", "Parent" : "0"},
	{"ID" : "41", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipDataDropFifo_V_U", "Parent" : "0"},
	{"ID" : "42", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ipDataCutFifo_V_U", "Parent" : "0"},
	{"ID" : "43", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.udpDataFifo_V_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	ip_handler_top {
		s_axis_raw_V_data_V {Type I LastRead 0 FirstWrite -1}
		s_axis_raw_V_keep_V {Type I LastRead 0 FirstWrite -1}
		s_axis_raw_V_last_V {Type I LastRead 0 FirstWrite -1}
		m_axis_arp_V_data_V {Type O LastRead -1 FirstWrite 1}
		m_axis_arp_V_keep_V {Type O LastRead -1 FirstWrite 1}
		m_axis_arp_V_last_V {Type O LastRead -1 FirstWrite 1}
		m_axis_icmpv6_V_data_V {Type O LastRead -1 FirstWrite 1}
		m_axis_icmpv6_V_keep_V {Type O LastRead -1 FirstWrite 1}
		m_axis_icmpv6_V_last_V {Type O LastRead -1 FirstWrite 1}
		m_axis_ipv6udp_V_data_V {Type O LastRead -1 FirstWrite 1}
		m_axis_ipv6udp_V_keep_V {Type O LastRead -1 FirstWrite 1}
		m_axis_ipv6udp_V_last_V {Type O LastRead -1 FirstWrite 1}
		m_axis_icmp_V_data_V {Type O LastRead -1 FirstWrite 1}
		m_axis_icmp_V_keep_V {Type O LastRead -1 FirstWrite 1}
		m_axis_icmp_V_last_V {Type O LastRead -1 FirstWrite 1}
		m_axis_udp_V_data_V {Type O LastRead -1 FirstWrite 1}
		m_axis_udp_V_keep_V {Type O LastRead -1 FirstWrite 1}
		m_axis_udp_V_last_V {Type O LastRead -1 FirstWrite 1}
		m_axis_tcp_V_data_V {Type O LastRead -1 FirstWrite 1}
		m_axis_tcp_V_keep_V {Type O LastRead -1 FirstWrite 1}
		m_axis_tcp_V_last_V {Type O LastRead -1 FirstWrite 1}
		m_axis_roce_V_data_V {Type O LastRead -1 FirstWrite 1}
		m_axis_roce_V_keep_V {Type O LastRead -1 FirstWrite 1}
		m_axis_roce_V_last_V {Type O LastRead -1 FirstWrite 1}
		myIpAddress_V {Type I LastRead 0 FirstWrite -1}
		header_ready {Type IO LastRead -1 FirstWrite -1}
		header_idx_1 {Type IO LastRead -1 FirstWrite -1}
		header_header_V_1 {Type IO LastRead -1 FirstWrite -1}
		metaWritten_1 {Type IO LastRead -1 FirstWrite -1}
		etherTypeFifo_V_V {Type IO LastRead -1 FirstWrite -1}
		ethDataFifo_V_data_V {Type IO LastRead -1 FirstWrite -1}
		ethDataFifo_V_keep_V {Type IO LastRead -1 FirstWrite -1}
		ethDataFifo_V_last_V {Type IO LastRead -1 FirstWrite -1}
		rep_fsmState_V {Type IO LastRead -1 FirstWrite -1}
		rep_etherType_V {Type IO LastRead -1 FirstWrite -1}
		ipv4ShiftFifo_V_data {Type IO LastRead -1 FirstWrite -1}
		ipv4ShiftFifo_V_keep {Type IO LastRead -1 FirstWrite -1}
		ipv4ShiftFifo_V_last {Type IO LastRead -1 FirstWrite -1}
		ipv6ShiftFifo_V_data {Type IO LastRead -1 FirstWrite -1}
		ipv6ShiftFifo_V_keep {Type IO LastRead -1 FirstWrite -1}
		ipv6ShiftFifo_V_last {Type IO LastRead -1 FirstWrite -1}
		fsmState_1 {Type IO LastRead -1 FirstWrite -1}
		prevWord_data_V {Type IO LastRead -1 FirstWrite -1}
		prevWord_keep_V_1 {Type IO LastRead -1 FirstWrite -1}
		rs_firstWord_1 {Type IO LastRead -1 FirstWrite -1}
		ipDataFifo_V {Type IO LastRead -1 FirstWrite -1}
		fsmState {Type IO LastRead -1 FirstWrite -1}
		prevWord_data_V_1 {Type IO LastRead -1 FirstWrite -1}
		prevWord_keep_V {Type IO LastRead -1 FirstWrite -1}
		rs_firstWord {Type IO LastRead -1 FirstWrite -1}
		ipv6DataFifo_V_data_s {Type IO LastRead -1 FirstWrite -1}
		ipv6DataFifo_V_keep_s {Type IO LastRead -1 FirstWrite -1}
		ipv6DataFifo_V_last_s {Type IO LastRead -1 FirstWrite -1}
		header_ready_1 {Type IO LastRead -1 FirstWrite -1}
		header_idx {Type IO LastRead -1 FirstWrite -1}
		header_header_V {Type IO LastRead -1 FirstWrite -1}
		ipDataMetaFifo_V_dat {Type IO LastRead -1 FirstWrite -1}
		ipDataMetaFifo_V_kee {Type IO LastRead -1 FirstWrite -1}
		ipDataMetaFifo_V_las {Type IO LastRead -1 FirstWrite -1}
		metaWritten {Type IO LastRead -1 FirstWrite -1}
		validIpAddressFifo_V {Type IO LastRead -1 FirstWrite -1}
		ipv4ProtocolFifo_V_V {Type IO LastRead -1 FirstWrite -1}
		ipDataCheckFifo_V {Type IO LastRead -1 FirstWrite -1}
		cics_wordCount_V {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_0 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_1 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_2 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_3 {Type IO LastRead -1 FirstWrite -1}
		cics_ipHeaderLen_V {Type IO LastRead -1 FirstWrite -1}
		iph_subSumsFifoOut_V {Type IO LastRead -1 FirstWrite -1}
		validChecksumFifo_V {Type IO LastRead -1 FirstWrite -1}
		iid_state {Type IO LastRead -1 FirstWrite -1}
		ipv4ValidFifo_V {Type IO LastRead -1 FirstWrite -1}
		ipDataDropFifo_V {Type IO LastRead -1 FirstWrite -1}
		cl_state {Type IO LastRead -1 FirstWrite -1}
		cl_wordCount_V {Type IO LastRead -1 FirstWrite -1}
		cl_totalLength_V {Type IO LastRead -1 FirstWrite -1}
		ipDataCutFifo_V {Type IO LastRead -1 FirstWrite -1}
		dip_state {Type IO LastRead -1 FirstWrite -1}
		dip_ipProtocol_V {Type IO LastRead -1 FirstWrite -1}
		udpDataFifo_V {Type IO LastRead -1 FirstWrite -1}
		state_V {Type IO LastRead -1 FirstWrite -1}
		nextHeader_V {Type IO LastRead -1 FirstWrite -1}}
	ip_handler_top_entry {
		myIpAddress_V {Type I LastRead 0 FirstWrite -1}
		myIpAddress_V_out {Type O LastRead -1 FirstWrite 0}}
	detect_eth_protocol3 {
		dataIn_V_data_V {Type I LastRead 0 FirstWrite -1}
		dataIn_V_keep_V {Type I LastRead 0 FirstWrite -1}
		dataIn_V_last_V {Type I LastRead 0 FirstWrite -1}
		myIpAddress_V {Type I LastRead 3 FirstWrite -1}
		myIpAddress_V_out {Type O LastRead -1 FirstWrite 3}
		header_ready {Type IO LastRead -1 FirstWrite -1}
		header_idx_1 {Type IO LastRead -1 FirstWrite -1}
		header_header_V_1 {Type IO LastRead -1 FirstWrite -1}
		metaWritten_1 {Type IO LastRead -1 FirstWrite -1}
		etherTypeFifo_V_V {Type O LastRead -1 FirstWrite 3}
		ethDataFifo_V_data_V {Type O LastRead -1 FirstWrite 1}
		ethDataFifo_V_keep_V {Type O LastRead -1 FirstWrite 1}
		ethDataFifo_V_last_V {Type O LastRead -1 FirstWrite 1}}
	route_by_eth_protoco {
		ARPdataOut_V_data_V {Type O LastRead -1 FirstWrite 1}
		ARPdataOut_V_keep_V {Type O LastRead -1 FirstWrite 1}
		ARPdataOut_V_last_V {Type O LastRead -1 FirstWrite 1}
		rep_fsmState_V {Type IO LastRead -1 FirstWrite -1}
		rep_etherType_V {Type IO LastRead -1 FirstWrite -1}
		etherTypeFifo_V_V {Type I LastRead 0 FirstWrite -1}
		ethDataFifo_V_data_V {Type I LastRead 0 FirstWrite -1}
		ethDataFifo_V_keep_V {Type I LastRead 0 FirstWrite -1}
		ethDataFifo_V_last_V {Type I LastRead 0 FirstWrite -1}
		ipv4ShiftFifo_V_data {Type O LastRead -1 FirstWrite 1}
		ipv4ShiftFifo_V_keep {Type O LastRead -1 FirstWrite 1}
		ipv4ShiftFifo_V_last {Type O LastRead -1 FirstWrite 1}
		ipv6ShiftFifo_V_data {Type O LastRead -1 FirstWrite 1}
		ipv6ShiftFifo_V_keep {Type O LastRead -1 FirstWrite 1}
		ipv6ShiftFifo_V_last {Type O LastRead -1 FirstWrite 1}}
	rshiftWordByOctet_1 {
		fsmState_1 {Type IO LastRead -1 FirstWrite -1}
		prevWord_data_V {Type IO LastRead -1 FirstWrite -1}
		prevWord_keep_V_1 {Type IO LastRead -1 FirstWrite -1}
		ipv4ShiftFifo_V_data {Type I LastRead 0 FirstWrite -1}
		ipv4ShiftFifo_V_keep {Type I LastRead 0 FirstWrite -1}
		ipv4ShiftFifo_V_last {Type I LastRead 0 FirstWrite -1}
		rs_firstWord_1 {Type IO LastRead -1 FirstWrite -1}
		ipDataFifo_V {Type O LastRead -1 FirstWrite 1}}
	rshiftWordByOctet {
		fsmState {Type IO LastRead -1 FirstWrite -1}
		prevWord_data_V_1 {Type IO LastRead -1 FirstWrite -1}
		prevWord_keep_V {Type IO LastRead -1 FirstWrite -1}
		ipv6ShiftFifo_V_data {Type I LastRead 0 FirstWrite -1}
		ipv6ShiftFifo_V_keep {Type I LastRead 0 FirstWrite -1}
		ipv6ShiftFifo_V_last {Type I LastRead 0 FirstWrite -1}
		rs_firstWord {Type IO LastRead -1 FirstWrite -1}
		ipv6DataFifo_V_data_s {Type O LastRead -1 FirstWrite 1}
		ipv6DataFifo_V_keep_s {Type O LastRead -1 FirstWrite 1}
		ipv6DataFifo_V_last_s {Type O LastRead -1 FirstWrite 1}}
	extract_ip_meta_64_s {
		myIpAddress_V {Type I LastRead 3 FirstWrite -1}
		ipDataFifo_V {Type I LastRead 0 FirstWrite -1}
		header_ready_1 {Type IO LastRead -1 FirstWrite -1}
		header_idx {Type IO LastRead -1 FirstWrite -1}
		header_header_V {Type IO LastRead -1 FirstWrite -1}
		ipDataMetaFifo_V_dat {Type O LastRead -1 FirstWrite 1}
		ipDataMetaFifo_V_kee {Type O LastRead -1 FirstWrite 1}
		ipDataMetaFifo_V_las {Type O LastRead -1 FirstWrite 1}
		metaWritten {Type IO LastRead -1 FirstWrite -1}
		validIpAddressFifo_V {Type O LastRead -1 FirstWrite 4}
		ipv4ProtocolFifo_V_V {Type O LastRead -1 FirstWrite 4}}
	compute_ipv4_checksu {
		ipDataMetaFifo_V_dat {Type I LastRead 0 FirstWrite -1}
		ipDataMetaFifo_V_kee {Type I LastRead 0 FirstWrite -1}
		ipDataMetaFifo_V_las {Type I LastRead 0 FirstWrite -1}
		ipDataCheckFifo_V {Type O LastRead -1 FirstWrite 1}
		cics_wordCount_V {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_0 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_1 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_2 {Type IO LastRead -1 FirstWrite -1}
		cics_ip_sums_sum_V_3 {Type IO LastRead -1 FirstWrite -1}
		cics_ipHeaderLen_V {Type IO LastRead -1 FirstWrite -1}
		iph_subSumsFifoOut_V {Type O LastRead -1 FirstWrite 2}}
	check_ipv4_checksum {
		iph_subSumsFifoOut_V {Type I LastRead 0 FirstWrite -1}
		validChecksumFifo_V {Type O LastRead -1 FirstWrite 4}}
	ip_invalid_dropper {
		iid_state {Type IO LastRead -1 FirstWrite -1}
		validChecksumFifo_V {Type I LastRead 0 FirstWrite -1}
		validIpAddressFifo_V {Type I LastRead 0 FirstWrite -1}
		ipv4ValidFifo_V {Type O LastRead -1 FirstWrite 1}
		ipDataCheckFifo_V {Type I LastRead 0 FirstWrite -1}
		ipDataDropFifo_V {Type O LastRead -1 FirstWrite 1}}
	cut_length_64_s {
		cl_state {Type IO LastRead -1 FirstWrite -1}
		ipDataDropFifo_V {Type I LastRead 0 FirstWrite -1}
		cl_wordCount_V {Type IO LastRead -1 FirstWrite -1}
		cl_totalLength_V {Type IO LastRead -1 FirstWrite -1}
		ipDataCutFifo_V {Type O LastRead -1 FirstWrite 1}}
	detect_ipv4_protocol {
		ICMPdataOut_V_data_V {Type O LastRead -1 FirstWrite 1}
		ICMPdataOut_V_keep_V {Type O LastRead -1 FirstWrite 1}
		ICMPdataOut_V_last_V {Type O LastRead -1 FirstWrite 1}
		TCPdataOut_V_data_V {Type O LastRead -1 FirstWrite 1}
		TCPdataOut_V_keep_V {Type O LastRead -1 FirstWrite 1}
		TCPdataOut_V_last_V {Type O LastRead -1 FirstWrite 1}
		dip_state {Type IO LastRead -1 FirstWrite -1}
		ipv4ProtocolFifo_V_V {Type I LastRead 0 FirstWrite -1}
		ipv4ValidFifo_V {Type I LastRead 0 FirstWrite -1}
		dip_ipProtocol_V {Type IO LastRead -1 FirstWrite -1}
		ipDataCutFifo_V {Type I LastRead 0 FirstWrite -1}
		udpDataFifo_V {Type O LastRead -1 FirstWrite 1}}
	detect_ipv6_protocol {
		icmpv6DataOut_V_data_V {Type O LastRead -1 FirstWrite 1}
		icmpv6DataOut_V_keep_V {Type O LastRead -1 FirstWrite 1}
		icmpv6DataOut_V_last_V {Type O LastRead -1 FirstWrite 1}
		ipv6UdpDataOut_V_data_V {Type O LastRead -1 FirstWrite 1}
		ipv6UdpDataOut_V_keep_V {Type O LastRead -1 FirstWrite 1}
		ipv6UdpDataOut_V_last_V {Type O LastRead -1 FirstWrite 1}
		state_V {Type IO LastRead -1 FirstWrite -1}
		ipv6DataFifo_V_data_s {Type I LastRead 0 FirstWrite -1}
		ipv6DataFifo_V_keep_s {Type I LastRead 0 FirstWrite -1}
		ipv6DataFifo_V_last_s {Type I LastRead 0 FirstWrite -1}
		nextHeader_V {Type IO LastRead -1 FirstWrite -1}}
	duplicate_stream {
		out0_V_data_V {Type O LastRead -1 FirstWrite 1}
		out0_V_keep_V {Type O LastRead -1 FirstWrite 1}
		out0_V_last_V {Type O LastRead -1 FirstWrite 1}
		out1_V_data_V {Type O LastRead -1 FirstWrite 1}
		out1_V_keep_V {Type O LastRead -1 FirstWrite 1}
		out1_V_last_V {Type O LastRead -1 FirstWrite 1}
		udpDataFifo_V {Type I LastRead 0 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "26", "Max" : "26"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
]}

set Spec2ImplPortList { 
	s_axis_raw_V_data_V { axis {  { s_axis_raw_TDATA in_data 0 64 } } }
	s_axis_raw_V_keep_V { axis {  { s_axis_raw_TKEEP in_data 0 8 } } }
	s_axis_raw_V_last_V { axis {  { s_axis_raw_TLAST in_data 0 1 }  { s_axis_raw_TVALID in_vld 0 1 }  { s_axis_raw_TREADY in_acc 1 1 } } }
	m_axis_arp_V_data_V { axis {  { m_axis_arp_TDATA out_data 1 64 } } }
	m_axis_arp_V_keep_V { axis {  { m_axis_arp_TKEEP out_data 1 8 } } }
	m_axis_arp_V_last_V { axis {  { m_axis_arp_TLAST out_data 1 1 }  { m_axis_arp_TVALID out_vld 1 1 }  { m_axis_arp_TREADY out_acc 0 1 } } }
	m_axis_icmpv6_V_data_V { axis {  { m_axis_icmpv6_TDATA out_data 1 64 } } }
	m_axis_icmpv6_V_keep_V { axis {  { m_axis_icmpv6_TKEEP out_data 1 8 } } }
	m_axis_icmpv6_V_last_V { axis {  { m_axis_icmpv6_TLAST out_data 1 1 }  { m_axis_icmpv6_TVALID out_vld 1 1 }  { m_axis_icmpv6_TREADY out_acc 0 1 } } }
	m_axis_ipv6udp_V_data_V { axis {  { m_axis_ipv6udp_TDATA out_data 1 64 } } }
	m_axis_ipv6udp_V_keep_V { axis {  { m_axis_ipv6udp_TKEEP out_data 1 8 } } }
	m_axis_ipv6udp_V_last_V { axis {  { m_axis_ipv6udp_TLAST out_data 1 1 }  { m_axis_ipv6udp_TVALID out_vld 1 1 }  { m_axis_ipv6udp_TREADY out_acc 0 1 } } }
	m_axis_icmp_V_data_V { axis {  { m_axis_icmp_TDATA out_data 1 64 } } }
	m_axis_icmp_V_keep_V { axis {  { m_axis_icmp_TKEEP out_data 1 8 } } }
	m_axis_icmp_V_last_V { axis {  { m_axis_icmp_TLAST out_data 1 1 }  { m_axis_icmp_TVALID out_vld 1 1 }  { m_axis_icmp_TREADY out_acc 0 1 } } }
	m_axis_udp_V_data_V { axis {  { m_axis_udp_TDATA out_data 1 64 } } }
	m_axis_udp_V_keep_V { axis {  { m_axis_udp_TKEEP out_data 1 8 } } }
	m_axis_udp_V_last_V { axis {  { m_axis_udp_TLAST out_data 1 1 }  { m_axis_udp_TVALID out_vld 1 1 }  { m_axis_udp_TREADY out_acc 0 1 } } }
	m_axis_tcp_V_data_V { axis {  { m_axis_tcp_TDATA out_data 1 64 } } }
	m_axis_tcp_V_keep_V { axis {  { m_axis_tcp_TKEEP out_data 1 8 } } }
	m_axis_tcp_V_last_V { axis {  { m_axis_tcp_TLAST out_data 1 1 }  { m_axis_tcp_TVALID out_vld 1 1 }  { m_axis_tcp_TREADY out_acc 0 1 } } }
	m_axis_roce_V_data_V { axis {  { m_axis_roce_TDATA out_data 1 64 } } }
	m_axis_roce_V_keep_V { axis {  { m_axis_roce_TKEEP out_data 1 8 } } }
	m_axis_roce_V_last_V { axis {  { m_axis_roce_TLAST out_data 1 1 }  { m_axis_roce_TVALID out_vld 1 1 }  { m_axis_roce_TREADY out_acc 0 1 } } }
}

set busDeadlockParameterList { 
}

# RTL port scheduling information:
set fifoSchedulingInfoList { 
}

# RTL bus port read request latency information:
set busReadReqLatencyList { 
}

# RTL bus port write response latency information:
set busWriteResLatencyList { 
}

# RTL array port load latency information:
set memoryLoadLatencyList { 
}
