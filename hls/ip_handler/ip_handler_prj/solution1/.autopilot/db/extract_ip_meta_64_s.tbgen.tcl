set moduleName extract_ip_meta_64_s
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {extract_ip_meta<64>}
set C_modelType { void 0 }
set C_modelArgList {
	{ myIpAddress_V int 32 regular {ap_stable 0} }
	{ ipDataFifo_V int 73 regular {fifo 0 volatile } {global 0}  }
	{ ipDataMetaFifo_V_dat int 64 regular {fifo 1 volatile } {global 1}  }
	{ ipDataMetaFifo_V_kee int 8 regular {fifo 1 volatile } {global 1}  }
	{ ipDataMetaFifo_V_las int 1 regular {fifo 1 volatile } {global 1}  }
	{ validIpAddressFifo_V int 1 regular {fifo 1 volatile } {global 1}  }
	{ ipv4ProtocolFifo_V_V int 8 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "myIpAddress_V", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "ipDataFifo_V", "interface" : "fifo", "bitwidth" : 73, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "ipDataMetaFifo_V_dat", "interface" : "fifo", "bitwidth" : 64, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "ipDataMetaFifo_V_kee", "interface" : "fifo", "bitwidth" : 8, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "ipDataMetaFifo_V_las", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "validIpAddressFifo_V", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "ipv4ProtocolFifo_V_V", "interface" : "fifo", "bitwidth" : 8, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 26
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ ipDataFifo_V_dout sc_in sc_lv 73 signal 1 } 
	{ ipDataFifo_V_empty_n sc_in sc_logic 1 signal 1 } 
	{ ipDataFifo_V_read sc_out sc_logic 1 signal 1 } 
	{ ipDataMetaFifo_V_dat_din sc_out sc_lv 64 signal 2 } 
	{ ipDataMetaFifo_V_dat_full_n sc_in sc_logic 1 signal 2 } 
	{ ipDataMetaFifo_V_dat_write sc_out sc_logic 1 signal 2 } 
	{ ipDataMetaFifo_V_kee_din sc_out sc_lv 8 signal 3 } 
	{ ipDataMetaFifo_V_kee_full_n sc_in sc_logic 1 signal 3 } 
	{ ipDataMetaFifo_V_kee_write sc_out sc_logic 1 signal 3 } 
	{ ipDataMetaFifo_V_las_din sc_out sc_lv 1 signal 4 } 
	{ ipDataMetaFifo_V_las_full_n sc_in sc_logic 1 signal 4 } 
	{ ipDataMetaFifo_V_las_write sc_out sc_logic 1 signal 4 } 
	{ validIpAddressFifo_V_din sc_out sc_lv 1 signal 5 } 
	{ validIpAddressFifo_V_full_n sc_in sc_logic 1 signal 5 } 
	{ validIpAddressFifo_V_write sc_out sc_logic 1 signal 5 } 
	{ ipv4ProtocolFifo_V_V_din sc_out sc_lv 8 signal 6 } 
	{ ipv4ProtocolFifo_V_V_full_n sc_in sc_logic 1 signal 6 } 
	{ ipv4ProtocolFifo_V_V_write sc_out sc_logic 1 signal 6 } 
	{ myIpAddress_V sc_in sc_lv 32 signal 0 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "ipDataFifo_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":73, "type": "signal", "bundle":{"name": "ipDataFifo_V", "role": "dout" }} , 
 	{ "name": "ipDataFifo_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataFifo_V", "role": "empty_n" }} , 
 	{ "name": "ipDataFifo_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataFifo_V", "role": "read" }} , 
 	{ "name": "ipDataMetaFifo_V_dat_din", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_dat", "role": "din" }} , 
 	{ "name": "ipDataMetaFifo_V_dat_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_dat", "role": "full_n" }} , 
 	{ "name": "ipDataMetaFifo_V_dat_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_dat", "role": "write" }} , 
 	{ "name": "ipDataMetaFifo_V_kee_din", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_kee", "role": "din" }} , 
 	{ "name": "ipDataMetaFifo_V_kee_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_kee", "role": "full_n" }} , 
 	{ "name": "ipDataMetaFifo_V_kee_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_kee", "role": "write" }} , 
 	{ "name": "ipDataMetaFifo_V_las_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_las", "role": "din" }} , 
 	{ "name": "ipDataMetaFifo_V_las_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_las", "role": "full_n" }} , 
 	{ "name": "ipDataMetaFifo_V_las_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_las", "role": "write" }} , 
 	{ "name": "validIpAddressFifo_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "validIpAddressFifo_V", "role": "din" }} , 
 	{ "name": "validIpAddressFifo_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "validIpAddressFifo_V", "role": "full_n" }} , 
 	{ "name": "validIpAddressFifo_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "validIpAddressFifo_V", "role": "write" }} , 
 	{ "name": "ipv4ProtocolFifo_V_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "ipv4ProtocolFifo_V_V", "role": "din" }} , 
 	{ "name": "ipv4ProtocolFifo_V_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv4ProtocolFifo_V_V", "role": "full_n" }} , 
 	{ "name": "ipv4ProtocolFifo_V_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv4ProtocolFifo_V_V", "role": "write" }} , 
 	{ "name": "myIpAddress_V", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "myIpAddress_V", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "extract_ip_meta_64_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "5", "EstimateLatencyMin" : "5", "EstimateLatencyMax" : "5",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "myIpAddress_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "ipDataFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipDataFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "header_ready_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_idx", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_header_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ipDataMetaFifo_V_dat", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipDataMetaFifo_V_dat_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipDataMetaFifo_V_kee", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipDataMetaFifo_V_kee_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipDataMetaFifo_V_las", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipDataMetaFifo_V_las_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "metaWritten", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "validIpAddressFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "validIpAddressFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv4ProtocolFifo_V_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipv4ProtocolFifo_V_V_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	extract_ip_meta_64_s {
		myIpAddress_V {Type I LastRead 0 FirstWrite -1}
		ipDataFifo_V {Type I LastRead 0 FirstWrite -1}
		header_ready_1 {Type IO LastRead -1 FirstWrite -1}
		header_idx {Type IO LastRead -1 FirstWrite -1}
		header_header_V {Type IO LastRead -1 FirstWrite -1}
		ipDataMetaFifo_V_dat {Type O LastRead -1 FirstWrite 1}
		ipDataMetaFifo_V_kee {Type O LastRead -1 FirstWrite 1}
		ipDataMetaFifo_V_las {Type O LastRead -1 FirstWrite 1}
		metaWritten {Type IO LastRead -1 FirstWrite -1}
		validIpAddressFifo_V {Type O LastRead -1 FirstWrite 5}
		ipv4ProtocolFifo_V_V {Type O LastRead -1 FirstWrite 5}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "5", "Max" : "5"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	myIpAddress_V { ap_stable {  { myIpAddress_V in_data 0 32 } } }
	ipDataFifo_V { ap_fifo {  { ipDataFifo_V_dout fifo_data 0 73 }  { ipDataFifo_V_empty_n fifo_status 0 1 }  { ipDataFifo_V_read fifo_update 1 1 } } }
	ipDataMetaFifo_V_dat { ap_fifo {  { ipDataMetaFifo_V_dat_din fifo_data 1 64 }  { ipDataMetaFifo_V_dat_full_n fifo_status 0 1 }  { ipDataMetaFifo_V_dat_write fifo_update 1 1 } } }
	ipDataMetaFifo_V_kee { ap_fifo {  { ipDataMetaFifo_V_kee_din fifo_data 1 8 }  { ipDataMetaFifo_V_kee_full_n fifo_status 0 1 }  { ipDataMetaFifo_V_kee_write fifo_update 1 1 } } }
	ipDataMetaFifo_V_las { ap_fifo {  { ipDataMetaFifo_V_las_din fifo_data 1 1 }  { ipDataMetaFifo_V_las_full_n fifo_status 0 1 }  { ipDataMetaFifo_V_las_write fifo_update 1 1 } } }
	validIpAddressFifo_V { ap_fifo {  { validIpAddressFifo_V_din fifo_data 1 1 }  { validIpAddressFifo_V_full_n fifo_status 0 1 }  { validIpAddressFifo_V_write fifo_update 1 1 } } }
	ipv4ProtocolFifo_V_V { ap_fifo {  { ipv4ProtocolFifo_V_V_din fifo_data 1 8 }  { ipv4ProtocolFifo_V_V_full_n fifo_status 0 1 }  { ipv4ProtocolFifo_V_V_write fifo_update 1 1 } } }
}
set moduleName extract_ip_meta_64_s
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {extract_ip_meta<64>}
set C_modelType { void 0 }
set C_modelArgList {
	{ myIpAddress_V int 32 regular {fifo 0}  }
	{ ipDataFifo_V int 73 regular {fifo 0 volatile } {global 0}  }
	{ ipDataMetaFifo_V_dat int 64 regular {fifo 1 volatile } {global 1}  }
	{ ipDataMetaFifo_V_kee int 8 regular {fifo 1 volatile } {global 1}  }
	{ ipDataMetaFifo_V_las int 1 regular {fifo 1 volatile } {global 1}  }
	{ validIpAddressFifo_V int 1 regular {fifo 1 volatile } {global 1}  }
	{ ipv4ProtocolFifo_V_V int 8 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "myIpAddress_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "ipDataFifo_V", "interface" : "fifo", "bitwidth" : 73, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "ipDataMetaFifo_V_dat", "interface" : "fifo", "bitwidth" : 64, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "ipDataMetaFifo_V_kee", "interface" : "fifo", "bitwidth" : 8, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "ipDataMetaFifo_V_las", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "validIpAddressFifo_V", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "ipv4ProtocolFifo_V_V", "interface" : "fifo", "bitwidth" : 8, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 28
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ ipDataFifo_V_dout sc_in sc_lv 73 signal 1 } 
	{ ipDataFifo_V_empty_n sc_in sc_logic 1 signal 1 } 
	{ ipDataFifo_V_read sc_out sc_logic 1 signal 1 } 
	{ ipDataMetaFifo_V_dat_din sc_out sc_lv 64 signal 2 } 
	{ ipDataMetaFifo_V_dat_full_n sc_in sc_logic 1 signal 2 } 
	{ ipDataMetaFifo_V_dat_write sc_out sc_logic 1 signal 2 } 
	{ ipDataMetaFifo_V_kee_din sc_out sc_lv 8 signal 3 } 
	{ ipDataMetaFifo_V_kee_full_n sc_in sc_logic 1 signal 3 } 
	{ ipDataMetaFifo_V_kee_write sc_out sc_logic 1 signal 3 } 
	{ ipDataMetaFifo_V_las_din sc_out sc_lv 1 signal 4 } 
	{ ipDataMetaFifo_V_las_full_n sc_in sc_logic 1 signal 4 } 
	{ ipDataMetaFifo_V_las_write sc_out sc_logic 1 signal 4 } 
	{ myIpAddress_V_dout sc_in sc_lv 32 signal 0 } 
	{ myIpAddress_V_empty_n sc_in sc_logic 1 signal 0 } 
	{ myIpAddress_V_read sc_out sc_logic 1 signal 0 } 
	{ validIpAddressFifo_V_din sc_out sc_lv 1 signal 5 } 
	{ validIpAddressFifo_V_full_n sc_in sc_logic 1 signal 5 } 
	{ validIpAddressFifo_V_write sc_out sc_logic 1 signal 5 } 
	{ ipv4ProtocolFifo_V_V_din sc_out sc_lv 8 signal 6 } 
	{ ipv4ProtocolFifo_V_V_full_n sc_in sc_logic 1 signal 6 } 
	{ ipv4ProtocolFifo_V_V_write sc_out sc_logic 1 signal 6 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "ipDataFifo_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":73, "type": "signal", "bundle":{"name": "ipDataFifo_V", "role": "dout" }} , 
 	{ "name": "ipDataFifo_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataFifo_V", "role": "empty_n" }} , 
 	{ "name": "ipDataFifo_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataFifo_V", "role": "read" }} , 
 	{ "name": "ipDataMetaFifo_V_dat_din", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_dat", "role": "din" }} , 
 	{ "name": "ipDataMetaFifo_V_dat_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_dat", "role": "full_n" }} , 
 	{ "name": "ipDataMetaFifo_V_dat_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_dat", "role": "write" }} , 
 	{ "name": "ipDataMetaFifo_V_kee_din", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_kee", "role": "din" }} , 
 	{ "name": "ipDataMetaFifo_V_kee_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_kee", "role": "full_n" }} , 
 	{ "name": "ipDataMetaFifo_V_kee_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_kee", "role": "write" }} , 
 	{ "name": "ipDataMetaFifo_V_las_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_las", "role": "din" }} , 
 	{ "name": "ipDataMetaFifo_V_las_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_las", "role": "full_n" }} , 
 	{ "name": "ipDataMetaFifo_V_las_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipDataMetaFifo_V_las", "role": "write" }} , 
 	{ "name": "myIpAddress_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "myIpAddress_V", "role": "dout" }} , 
 	{ "name": "myIpAddress_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "myIpAddress_V", "role": "empty_n" }} , 
 	{ "name": "myIpAddress_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "myIpAddress_V", "role": "read" }} , 
 	{ "name": "validIpAddressFifo_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "validIpAddressFifo_V", "role": "din" }} , 
 	{ "name": "validIpAddressFifo_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "validIpAddressFifo_V", "role": "full_n" }} , 
 	{ "name": "validIpAddressFifo_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "validIpAddressFifo_V", "role": "write" }} , 
 	{ "name": "ipv4ProtocolFifo_V_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "ipv4ProtocolFifo_V_V", "role": "din" }} , 
 	{ "name": "ipv4ProtocolFifo_V_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv4ProtocolFifo_V_V", "role": "full_n" }} , 
 	{ "name": "ipv4ProtocolFifo_V_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv4ProtocolFifo_V_V", "role": "write" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "extract_ip_meta_64_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "4", "EstimateLatencyMin" : "4", "EstimateLatencyMax" : "4",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "myIpAddress_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "myIpAddress_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipDataFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipDataFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "header_ready_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_idx", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_header_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ipDataMetaFifo_V_dat", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipDataMetaFifo_V_dat_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipDataMetaFifo_V_kee", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipDataMetaFifo_V_kee_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipDataMetaFifo_V_las", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipDataMetaFifo_V_las_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "metaWritten", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "validIpAddressFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "validIpAddressFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv4ProtocolFifo_V_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipv4ProtocolFifo_V_V_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	extract_ip_meta_64_s {
		myIpAddress_V {Type I LastRead 3 FirstWrite -1}
		ipDataFifo_V {Type I LastRead 0 FirstWrite -1}
		header_ready_1 {Type IO LastRead -1 FirstWrite -1}
		header_idx {Type IO LastRead -1 FirstWrite -1}
		header_header_V {Type IO LastRead -1 FirstWrite -1}
		ipDataMetaFifo_V_dat {Type O LastRead -1 FirstWrite 1}
		ipDataMetaFifo_V_kee {Type O LastRead -1 FirstWrite 1}
		ipDataMetaFifo_V_las {Type O LastRead -1 FirstWrite 1}
		metaWritten {Type IO LastRead -1 FirstWrite -1}
		validIpAddressFifo_V {Type O LastRead -1 FirstWrite 4}
		ipv4ProtocolFifo_V_V {Type O LastRead -1 FirstWrite 4}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "4", "Max" : "4"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	myIpAddress_V { ap_fifo {  { myIpAddress_V_dout fifo_data 0 32 }  { myIpAddress_V_empty_n fifo_status 0 1 }  { myIpAddress_V_read fifo_update 1 1 } } }
	ipDataFifo_V { ap_fifo {  { ipDataFifo_V_dout fifo_data 0 73 }  { ipDataFifo_V_empty_n fifo_status 0 1 }  { ipDataFifo_V_read fifo_update 1 1 } } }
	ipDataMetaFifo_V_dat { ap_fifo {  { ipDataMetaFifo_V_dat_din fifo_data 1 64 }  { ipDataMetaFifo_V_dat_full_n fifo_status 0 1 }  { ipDataMetaFifo_V_dat_write fifo_update 1 1 } } }
	ipDataMetaFifo_V_kee { ap_fifo {  { ipDataMetaFifo_V_kee_din fifo_data 1 8 }  { ipDataMetaFifo_V_kee_full_n fifo_status 0 1 }  { ipDataMetaFifo_V_kee_write fifo_update 1 1 } } }
	ipDataMetaFifo_V_las { ap_fifo {  { ipDataMetaFifo_V_las_din fifo_data 1 1 }  { ipDataMetaFifo_V_las_full_n fifo_status 0 1 }  { ipDataMetaFifo_V_las_write fifo_update 1 1 } } }
	validIpAddressFifo_V { ap_fifo {  { validIpAddressFifo_V_din fifo_data 1 1 }  { validIpAddressFifo_V_full_n fifo_status 0 1 }  { validIpAddressFifo_V_write fifo_update 1 1 } } }
	ipv4ProtocolFifo_V_V { ap_fifo {  { ipv4ProtocolFifo_V_V_din fifo_data 1 8 }  { ipv4ProtocolFifo_V_V_full_n fifo_status 0 1 }  { ipv4ProtocolFifo_V_V_write fifo_update 1 1 } } }
}
