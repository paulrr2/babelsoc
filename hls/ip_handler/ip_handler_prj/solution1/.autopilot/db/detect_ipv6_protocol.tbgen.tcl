set moduleName detect_ipv6_protocol
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {detect_ipv6_protocol}
set C_modelType { void 0 }
set C_modelArgList {
	{ icmpv6DataOut_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_icmpv6 Data } }  }
	{ icmpv6DataOut_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_icmpv6 Keep } }  }
	{ icmpv6DataOut_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_icmpv6 Last } }  }
	{ ipv6UdpDataOut_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_ipv6udp Data } }  }
	{ ipv6UdpDataOut_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_ipv6udp Keep } }  }
	{ ipv6UdpDataOut_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_ipv6udp Last } }  }
	{ ipv6DataFifo_V_data_s int 64 regular {fifo 0 volatile } {global 0}  }
	{ ipv6DataFifo_V_keep_s int 8 regular {fifo 0 volatile } {global 0}  }
	{ ipv6DataFifo_V_last_s int 1 regular {fifo 0 volatile } {global 0}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "icmpv6DataOut_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY"} , 
 	{ "Name" : "icmpv6DataOut_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY"} , 
 	{ "Name" : "icmpv6DataOut_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY"} , 
 	{ "Name" : "ipv6UdpDataOut_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY"} , 
 	{ "Name" : "ipv6UdpDataOut_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY"} , 
 	{ "Name" : "ipv6UdpDataOut_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY"} , 
 	{ "Name" : "ipv6DataFifo_V_data_s", "interface" : "fifo", "bitwidth" : 64, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "ipv6DataFifo_V_keep_s", "interface" : "fifo", "bitwidth" : 8, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "ipv6DataFifo_V_last_s", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 26
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ ipv6DataFifo_V_data_s_dout sc_in sc_lv 64 signal 6 } 
	{ ipv6DataFifo_V_data_s_empty_n sc_in sc_logic 1 signal 6 } 
	{ ipv6DataFifo_V_data_s_read sc_out sc_logic 1 signal 6 } 
	{ ipv6DataFifo_V_keep_s_dout sc_in sc_lv 8 signal 7 } 
	{ ipv6DataFifo_V_keep_s_empty_n sc_in sc_logic 1 signal 7 } 
	{ ipv6DataFifo_V_keep_s_read sc_out sc_logic 1 signal 7 } 
	{ ipv6DataFifo_V_last_s_dout sc_in sc_lv 1 signal 8 } 
	{ ipv6DataFifo_V_last_s_empty_n sc_in sc_logic 1 signal 8 } 
	{ ipv6DataFifo_V_last_s_read sc_out sc_logic 1 signal 8 } 
	{ m_axis_ipv6udp_TREADY sc_in sc_logic 1 outacc 5 } 
	{ m_axis_icmpv6_TREADY sc_in sc_logic 1 outacc 2 } 
	{ m_axis_icmpv6_TDATA sc_out sc_lv 64 signal 0 } 
	{ m_axis_icmpv6_TVALID sc_out sc_logic 1 outvld 2 } 
	{ m_axis_icmpv6_TKEEP sc_out sc_lv 8 signal 1 } 
	{ m_axis_icmpv6_TLAST sc_out sc_lv 1 signal 2 } 
	{ m_axis_ipv6udp_TDATA sc_out sc_lv 64 signal 3 } 
	{ m_axis_ipv6udp_TVALID sc_out sc_logic 1 outvld 5 } 
	{ m_axis_ipv6udp_TKEEP sc_out sc_lv 8 signal 4 } 
	{ m_axis_ipv6udp_TLAST sc_out sc_lv 1 signal 5 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "ipv6DataFifo_V_data_s_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "ipv6DataFifo_V_data_s", "role": "dout" }} , 
 	{ "name": "ipv6DataFifo_V_data_s_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv6DataFifo_V_data_s", "role": "empty_n" }} , 
 	{ "name": "ipv6DataFifo_V_data_s_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv6DataFifo_V_data_s", "role": "read" }} , 
 	{ "name": "ipv6DataFifo_V_keep_s_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "ipv6DataFifo_V_keep_s", "role": "dout" }} , 
 	{ "name": "ipv6DataFifo_V_keep_s_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv6DataFifo_V_keep_s", "role": "empty_n" }} , 
 	{ "name": "ipv6DataFifo_V_keep_s_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv6DataFifo_V_keep_s", "role": "read" }} , 
 	{ "name": "ipv6DataFifo_V_last_s_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv6DataFifo_V_last_s", "role": "dout" }} , 
 	{ "name": "ipv6DataFifo_V_last_s_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv6DataFifo_V_last_s", "role": "empty_n" }} , 
 	{ "name": "ipv6DataFifo_V_last_s_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv6DataFifo_V_last_s", "role": "read" }} , 
 	{ "name": "m_axis_ipv6udp_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "ipv6UdpDataOut_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_icmpv6_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "icmpv6DataOut_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_icmpv6_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "icmpv6DataOut_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_icmpv6_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "icmpv6DataOut_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_icmpv6_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "icmpv6DataOut_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_icmpv6_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "icmpv6DataOut_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_ipv6udp_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "ipv6UdpDataOut_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_ipv6udp_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "ipv6UdpDataOut_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_ipv6udp_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "ipv6UdpDataOut_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_ipv6udp_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv6UdpDataOut_V_last_V", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2", "3", "4", "5", "6"],
		"CDFG" : "detect_ipv6_protocol",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "icmpv6DataOut_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_icmpv6_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "icmpv6DataOut_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "icmpv6DataOut_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "ipv6UdpDataOut_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_ipv6udp_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv6UdpDataOut_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "ipv6UdpDataOut_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "state_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ipv6DataFifo_V_data_s", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipv6DataFifo_V_data_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv6DataFifo_V_keep_s", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipv6DataFifo_V_keep_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv6DataFifo_V_last_s", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipv6DataFifo_V_last_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "nextHeader_V", "Type" : "OVld", "Direction" : "IO"}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_icmpv6DataOut_V_data_V_U", "Parent" : "0"},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_icmpv6DataOut_V_keep_V_U", "Parent" : "0"},
	{"ID" : "3", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_icmpv6DataOut_V_last_V_U", "Parent" : "0"},
	{"ID" : "4", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_ipv6UdpDataOut_V_data_V_U", "Parent" : "0"},
	{"ID" : "5", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_ipv6UdpDataOut_V_keep_V_U", "Parent" : "0"},
	{"ID" : "6", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_ipv6UdpDataOut_V_last_V_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	detect_ipv6_protocol {
		icmpv6DataOut_V_data_V {Type O LastRead -1 FirstWrite 1}
		icmpv6DataOut_V_keep_V {Type O LastRead -1 FirstWrite 1}
		icmpv6DataOut_V_last_V {Type O LastRead -1 FirstWrite 1}
		ipv6UdpDataOut_V_data_V {Type O LastRead -1 FirstWrite 1}
		ipv6UdpDataOut_V_keep_V {Type O LastRead -1 FirstWrite 1}
		ipv6UdpDataOut_V_last_V {Type O LastRead -1 FirstWrite 1}
		state_V {Type IO LastRead -1 FirstWrite -1}
		ipv6DataFifo_V_data_s {Type I LastRead 0 FirstWrite -1}
		ipv6DataFifo_V_keep_s {Type I LastRead 0 FirstWrite -1}
		ipv6DataFifo_V_last_s {Type I LastRead 0 FirstWrite -1}
		nextHeader_V {Type IO LastRead -1 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "2", "Max" : "2"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	icmpv6DataOut_V_data_V { axis {  { m_axis_icmpv6_TDATA out_data 1 64 } } }
	icmpv6DataOut_V_keep_V { axis {  { m_axis_icmpv6_TKEEP out_data 1 8 } } }
	icmpv6DataOut_V_last_V { axis {  { m_axis_icmpv6_TREADY out_acc 0 1 }  { m_axis_icmpv6_TVALID out_vld 1 1 }  { m_axis_icmpv6_TLAST out_data 1 1 } } }
	ipv6UdpDataOut_V_data_V { axis {  { m_axis_ipv6udp_TDATA out_data 1 64 } } }
	ipv6UdpDataOut_V_keep_V { axis {  { m_axis_ipv6udp_TKEEP out_data 1 8 } } }
	ipv6UdpDataOut_V_last_V { axis {  { m_axis_ipv6udp_TREADY out_acc 0 1 }  { m_axis_ipv6udp_TVALID out_vld 1 1 }  { m_axis_ipv6udp_TLAST out_data 1 1 } } }
	ipv6DataFifo_V_data_s { ap_fifo {  { ipv6DataFifo_V_data_s_dout fifo_data 0 64 }  { ipv6DataFifo_V_data_s_empty_n fifo_status 0 1 }  { ipv6DataFifo_V_data_s_read fifo_update 1 1 } } }
	ipv6DataFifo_V_keep_s { ap_fifo {  { ipv6DataFifo_V_keep_s_dout fifo_data 0 8 }  { ipv6DataFifo_V_keep_s_empty_n fifo_status 0 1 }  { ipv6DataFifo_V_keep_s_read fifo_update 1 1 } } }
	ipv6DataFifo_V_last_s { ap_fifo {  { ipv6DataFifo_V_last_s_dout fifo_data 0 1 }  { ipv6DataFifo_V_last_s_empty_n fifo_status 0 1 }  { ipv6DataFifo_V_last_s_read fifo_update 1 1 } } }
}
set moduleName detect_ipv6_protocol
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {detect_ipv6_protocol}
set C_modelType { void 0 }
set C_modelArgList {
	{ icmpv6DataOut_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_icmpv6 Data } }  }
	{ icmpv6DataOut_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_icmpv6 Keep } }  }
	{ icmpv6DataOut_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_icmpv6 Last } }  }
	{ ipv6UdpDataOut_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_ipv6udp Data } }  }
	{ ipv6UdpDataOut_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_ipv6udp Keep } }  }
	{ ipv6UdpDataOut_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_ipv6udp Last } }  }
	{ ipv6DataFifo_V_data_s int 64 regular {fifo 0 volatile } {global 0}  }
	{ ipv6DataFifo_V_keep_s int 8 regular {fifo 0 volatile } {global 0}  }
	{ ipv6DataFifo_V_last_s int 1 regular {fifo 0 volatile } {global 0}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "icmpv6DataOut_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY"} , 
 	{ "Name" : "icmpv6DataOut_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY"} , 
 	{ "Name" : "icmpv6DataOut_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY"} , 
 	{ "Name" : "ipv6UdpDataOut_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY"} , 
 	{ "Name" : "ipv6UdpDataOut_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY"} , 
 	{ "Name" : "ipv6UdpDataOut_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY"} , 
 	{ "Name" : "ipv6DataFifo_V_data_s", "interface" : "fifo", "bitwidth" : 64, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "ipv6DataFifo_V_keep_s", "interface" : "fifo", "bitwidth" : 8, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "ipv6DataFifo_V_last_s", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 26
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ ipv6DataFifo_V_data_s_dout sc_in sc_lv 64 signal 6 } 
	{ ipv6DataFifo_V_data_s_empty_n sc_in sc_logic 1 signal 6 } 
	{ ipv6DataFifo_V_data_s_read sc_out sc_logic 1 signal 6 } 
	{ ipv6DataFifo_V_keep_s_dout sc_in sc_lv 8 signal 7 } 
	{ ipv6DataFifo_V_keep_s_empty_n sc_in sc_logic 1 signal 7 } 
	{ ipv6DataFifo_V_keep_s_read sc_out sc_logic 1 signal 7 } 
	{ ipv6DataFifo_V_last_s_dout sc_in sc_lv 1 signal 8 } 
	{ ipv6DataFifo_V_last_s_empty_n sc_in sc_logic 1 signal 8 } 
	{ ipv6DataFifo_V_last_s_read sc_out sc_logic 1 signal 8 } 
	{ m_axis_ipv6udp_TREADY sc_in sc_logic 1 outacc 5 } 
	{ m_axis_icmpv6_TREADY sc_in sc_logic 1 outacc 2 } 
	{ m_axis_icmpv6_TDATA sc_out sc_lv 64 signal 0 } 
	{ m_axis_icmpv6_TVALID sc_out sc_logic 1 outvld 2 } 
	{ m_axis_icmpv6_TKEEP sc_out sc_lv 8 signal 1 } 
	{ m_axis_icmpv6_TLAST sc_out sc_lv 1 signal 2 } 
	{ m_axis_ipv6udp_TDATA sc_out sc_lv 64 signal 3 } 
	{ m_axis_ipv6udp_TVALID sc_out sc_logic 1 outvld 5 } 
	{ m_axis_ipv6udp_TKEEP sc_out sc_lv 8 signal 4 } 
	{ m_axis_ipv6udp_TLAST sc_out sc_lv 1 signal 5 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "ipv6DataFifo_V_data_s_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "ipv6DataFifo_V_data_s", "role": "dout" }} , 
 	{ "name": "ipv6DataFifo_V_data_s_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv6DataFifo_V_data_s", "role": "empty_n" }} , 
 	{ "name": "ipv6DataFifo_V_data_s_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv6DataFifo_V_data_s", "role": "read" }} , 
 	{ "name": "ipv6DataFifo_V_keep_s_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "ipv6DataFifo_V_keep_s", "role": "dout" }} , 
 	{ "name": "ipv6DataFifo_V_keep_s_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv6DataFifo_V_keep_s", "role": "empty_n" }} , 
 	{ "name": "ipv6DataFifo_V_keep_s_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv6DataFifo_V_keep_s", "role": "read" }} , 
 	{ "name": "ipv6DataFifo_V_last_s_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv6DataFifo_V_last_s", "role": "dout" }} , 
 	{ "name": "ipv6DataFifo_V_last_s_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv6DataFifo_V_last_s", "role": "empty_n" }} , 
 	{ "name": "ipv6DataFifo_V_last_s_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv6DataFifo_V_last_s", "role": "read" }} , 
 	{ "name": "m_axis_ipv6udp_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "ipv6UdpDataOut_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_icmpv6_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "icmpv6DataOut_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_icmpv6_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "icmpv6DataOut_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_icmpv6_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "icmpv6DataOut_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_icmpv6_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "icmpv6DataOut_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_icmpv6_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "icmpv6DataOut_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_ipv6udp_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "ipv6UdpDataOut_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_ipv6udp_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "ipv6UdpDataOut_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_ipv6udp_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "ipv6UdpDataOut_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_ipv6udp_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "ipv6UdpDataOut_V_last_V", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "detect_ipv6_protocol",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "icmpv6DataOut_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_icmpv6_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "icmpv6DataOut_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "icmpv6DataOut_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "ipv6UdpDataOut_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_ipv6udp_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv6UdpDataOut_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "ipv6UdpDataOut_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "state_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ipv6DataFifo_V_data_s", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipv6DataFifo_V_data_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv6DataFifo_V_keep_s", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipv6DataFifo_V_keep_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ipv6DataFifo_V_last_s", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ipv6DataFifo_V_last_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "nextHeader_V", "Type" : "OVld", "Direction" : "IO"}]}]}


set ArgLastReadFirstWriteLatency {
	detect_ipv6_protocol {
		icmpv6DataOut_V_data_V {Type O LastRead -1 FirstWrite 1}
		icmpv6DataOut_V_keep_V {Type O LastRead -1 FirstWrite 1}
		icmpv6DataOut_V_last_V {Type O LastRead -1 FirstWrite 1}
		ipv6UdpDataOut_V_data_V {Type O LastRead -1 FirstWrite 1}
		ipv6UdpDataOut_V_keep_V {Type O LastRead -1 FirstWrite 1}
		ipv6UdpDataOut_V_last_V {Type O LastRead -1 FirstWrite 1}
		state_V {Type IO LastRead -1 FirstWrite -1}
		ipv6DataFifo_V_data_s {Type I LastRead 0 FirstWrite -1}
		ipv6DataFifo_V_keep_s {Type I LastRead 0 FirstWrite -1}
		ipv6DataFifo_V_last_s {Type I LastRead 0 FirstWrite -1}
		nextHeader_V {Type IO LastRead -1 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "2", "Max" : "2"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	icmpv6DataOut_V_data_V { axis {  { m_axis_icmpv6_TDATA out_data 1 64 } } }
	icmpv6DataOut_V_keep_V { axis {  { m_axis_icmpv6_TKEEP out_data 1 8 } } }
	icmpv6DataOut_V_last_V { axis {  { m_axis_icmpv6_TREADY out_acc 0 1 }  { m_axis_icmpv6_TVALID out_vld 1 1 }  { m_axis_icmpv6_TLAST out_data 1 1 } } }
	ipv6UdpDataOut_V_data_V { axis {  { m_axis_ipv6udp_TDATA out_data 1 64 } } }
	ipv6UdpDataOut_V_keep_V { axis {  { m_axis_ipv6udp_TKEEP out_data 1 8 } } }
	ipv6UdpDataOut_V_last_V { axis {  { m_axis_ipv6udp_TREADY out_acc 0 1 }  { m_axis_ipv6udp_TVALID out_vld 1 1 }  { m_axis_ipv6udp_TLAST out_data 1 1 } } }
	ipv6DataFifo_V_data_s { ap_fifo {  { ipv6DataFifo_V_data_s_dout fifo_data 0 64 }  { ipv6DataFifo_V_data_s_empty_n fifo_status 0 1 }  { ipv6DataFifo_V_data_s_read fifo_update 1 1 } } }
	ipv6DataFifo_V_keep_s { ap_fifo {  { ipv6DataFifo_V_keep_s_dout fifo_data 0 8 }  { ipv6DataFifo_V_keep_s_empty_n fifo_status 0 1 }  { ipv6DataFifo_V_keep_s_read fifo_update 1 1 } } }
	ipv6DataFifo_V_last_s { ap_fifo {  { ipv6DataFifo_V_last_s_dout fifo_data 0 1 }  { ipv6DataFifo_V_last_s_empty_n fifo_status 0 1 }  { ipv6DataFifo_V_last_s_read fifo_update 1 1 } } }
}
