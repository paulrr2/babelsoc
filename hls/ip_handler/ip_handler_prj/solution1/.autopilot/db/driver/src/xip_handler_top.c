// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================
/***************************** Include Files *********************************/
#include "xip_handler_top.h"

/************************** Function Implementation *************************/
#ifndef __linux__
int XIp_handler_top_CfgInitialize(XIp_handler_top *InstancePtr, XIp_handler_top_Config *ConfigPtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(ConfigPtr != NULL);

    InstancePtr->Axilites_BaseAddress = ConfigPtr->Axilites_BaseAddress;
    InstancePtr->IsReady = XIL_COMPONENT_IS_READY;

    return XST_SUCCESS;
}
#endif

void XIp_handler_top_Set_myIpAddress_V(XIp_handler_top *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XIp_handler_top_WriteReg(InstancePtr->Axilites_BaseAddress, XIP_HANDLER_TOP_AXILITES_ADDR_MYIPADDRESS_V_DATA, Data);
}

u32 XIp_handler_top_Get_myIpAddress_V(XIp_handler_top *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XIp_handler_top_ReadReg(InstancePtr->Axilites_BaseAddress, XIP_HANDLER_TOP_AXILITES_ADDR_MYIPADDRESS_V_DATA);
    return Data;
}

