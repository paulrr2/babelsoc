# This script segment is generated automatically by AutoPilot

# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# Native AXIS:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::native_axis_add] == "::AESL_LIB_XILADAPTER::native_axis_add"} {
eval "::AESL_LIB_XILADAPTER::native_axis_add { \
    id 68 \
    name icmpv6DataOut_V_data_V \
    reset_level 1 \
    sync_rst true \
    corename {m_axis_icmpv6} \
    metadata {  } \
    op interface \
    ports { m_axis_icmpv6_TDATA { O 64 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'icmpv6DataOut_V_data_V'"
}
}


# Native AXIS:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::native_axis_add] == "::AESL_LIB_XILADAPTER::native_axis_add"} {
eval "::AESL_LIB_XILADAPTER::native_axis_add { \
    id 69 \
    name icmpv6DataOut_V_keep_V \
    reset_level 1 \
    sync_rst true \
    corename {m_axis_icmpv6} \
    metadata {  } \
    op interface \
    ports { m_axis_icmpv6_TKEEP { O 8 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'icmpv6DataOut_V_keep_V'"
}
}


# Native AXIS:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::native_axis_add] == "::AESL_LIB_XILADAPTER::native_axis_add"} {
eval "::AESL_LIB_XILADAPTER::native_axis_add { \
    id 70 \
    name icmpv6DataOut_V_last_V \
    reset_level 1 \
    sync_rst true \
    corename {m_axis_icmpv6} \
    metadata {  } \
    op interface \
    ports { m_axis_icmpv6_TREADY { I 1 bit } m_axis_icmpv6_TVALID { O 1 bit } m_axis_icmpv6_TLAST { O 1 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'icmpv6DataOut_V_last_V'"
}
}


# Native AXIS:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::native_axis_add] == "::AESL_LIB_XILADAPTER::native_axis_add"} {
eval "::AESL_LIB_XILADAPTER::native_axis_add { \
    id 71 \
    name ipv6UdpDataOut_V_data_V \
    reset_level 1 \
    sync_rst true \
    corename {m_axis_ipv6udp} \
    metadata {  } \
    op interface \
    ports { m_axis_ipv6udp_TDATA { O 64 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ipv6UdpDataOut_V_data_V'"
}
}


# Native AXIS:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::native_axis_add] == "::AESL_LIB_XILADAPTER::native_axis_add"} {
eval "::AESL_LIB_XILADAPTER::native_axis_add { \
    id 72 \
    name ipv6UdpDataOut_V_keep_V \
    reset_level 1 \
    sync_rst true \
    corename {m_axis_ipv6udp} \
    metadata {  } \
    op interface \
    ports { m_axis_ipv6udp_TKEEP { O 8 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ipv6UdpDataOut_V_keep_V'"
}
}


# Native AXIS:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::native_axis_add] == "::AESL_LIB_XILADAPTER::native_axis_add"} {
eval "::AESL_LIB_XILADAPTER::native_axis_add { \
    id 73 \
    name ipv6UdpDataOut_V_last_V \
    reset_level 1 \
    sync_rst true \
    corename {m_axis_ipv6udp} \
    metadata {  } \
    op interface \
    ports { m_axis_ipv6udp_TREADY { I 1 bit } m_axis_ipv6udp_TVALID { O 1 bit } m_axis_ipv6udp_TLAST { O 1 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ipv6UdpDataOut_V_last_V'"
}
}


# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 74 \
    name ipv6DataFifo_V_data_s \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_ipv6DataFifo_V_data_s \
    op interface \
    ports { ipv6DataFifo_V_data_s_dout { I 64 vector } ipv6DataFifo_V_data_s_empty_n { I 1 bit } ipv6DataFifo_V_data_s_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 75 \
    name ipv6DataFifo_V_keep_s \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_ipv6DataFifo_V_keep_s \
    op interface \
    ports { ipv6DataFifo_V_keep_s_dout { I 8 vector } ipv6DataFifo_V_keep_s_empty_n { I 1 bit } ipv6DataFifo_V_keep_s_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 76 \
    name ipv6DataFifo_V_last_s \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_ipv6DataFifo_V_last_s \
    op interface \
    ports { ipv6DataFifo_V_last_s_dout { I 1 vector } ipv6DataFifo_V_last_s_empty_n { I 1 bit } ipv6DataFifo_V_last_s_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_start { I 1 bit } ap_ready { O 1 bit } ap_done { O 1 bit } ap_idle { O 1 bit } ap_continue { I 1 bit } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


