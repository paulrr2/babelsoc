set moduleName detect_eth_protocol
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {detect_eth_protocol}
set C_modelType { void 0 }
set C_modelArgList {
	{ dataIn_V_data_V int 64 regular {axi_s 0 volatile  { s_axis_raw Data } }  }
	{ dataIn_V_keep_V int 8 regular {axi_s 0 volatile  { s_axis_raw Keep } }  }
	{ dataIn_V_last_V int 1 regular {axi_s 0 volatile  { s_axis_raw Last } }  }
	{ etherTypeFifo_V_V int 16 regular {fifo 1 volatile } {global 1}  }
	{ ethDataFifo_V_data_V int 64 regular {fifo 1 volatile } {global 1}  }
	{ ethDataFifo_V_keep_V int 8 regular {fifo 1 volatile } {global 1}  }
	{ ethDataFifo_V_last_V int 1 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "dataIn_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "READONLY"} , 
 	{ "Name" : "dataIn_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "dataIn_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY"} , 
 	{ "Name" : "etherTypeFifo_V_V", "interface" : "fifo", "bitwidth" : 16, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "ethDataFifo_V_data_V", "interface" : "fifo", "bitwidth" : 64, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "ethDataFifo_V_keep_V", "interface" : "fifo", "bitwidth" : 8, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "ethDataFifo_V_last_V", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 24
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ s_axis_raw_TVALID sc_in sc_logic 1 invld 0 } 
	{ ethDataFifo_V_data_V_din sc_out sc_lv 64 signal 4 } 
	{ ethDataFifo_V_data_V_full_n sc_in sc_logic 1 signal 4 } 
	{ ethDataFifo_V_data_V_write sc_out sc_logic 1 signal 4 } 
	{ ethDataFifo_V_keep_V_din sc_out sc_lv 8 signal 5 } 
	{ ethDataFifo_V_keep_V_full_n sc_in sc_logic 1 signal 5 } 
	{ ethDataFifo_V_keep_V_write sc_out sc_logic 1 signal 5 } 
	{ ethDataFifo_V_last_V_din sc_out sc_lv 1 signal 6 } 
	{ ethDataFifo_V_last_V_full_n sc_in sc_logic 1 signal 6 } 
	{ ethDataFifo_V_last_V_write sc_out sc_logic 1 signal 6 } 
	{ etherTypeFifo_V_V_din sc_out sc_lv 16 signal 3 } 
	{ etherTypeFifo_V_V_full_n sc_in sc_logic 1 signal 3 } 
	{ etherTypeFifo_V_V_write sc_out sc_logic 1 signal 3 } 
	{ s_axis_raw_TDATA sc_in sc_lv 64 signal 0 } 
	{ s_axis_raw_TREADY sc_out sc_logic 1 inacc 2 } 
	{ s_axis_raw_TKEEP sc_in sc_lv 8 signal 1 } 
	{ s_axis_raw_TLAST sc_in sc_lv 1 signal 2 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "s_axis_raw_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "dataIn_V_data_V", "role": "D" }} , 
 	{ "name": "ethDataFifo_V_data_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "ethDataFifo_V_data_V", "role": "din" }} , 
 	{ "name": "ethDataFifo_V_data_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ethDataFifo_V_data_V", "role": "full_n" }} , 
 	{ "name": "ethDataFifo_V_data_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ethDataFifo_V_data_V", "role": "write" }} , 
 	{ "name": "ethDataFifo_V_keep_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "ethDataFifo_V_keep_V", "role": "din" }} , 
 	{ "name": "ethDataFifo_V_keep_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ethDataFifo_V_keep_V", "role": "full_n" }} , 
 	{ "name": "ethDataFifo_V_keep_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ethDataFifo_V_keep_V", "role": "write" }} , 
 	{ "name": "ethDataFifo_V_last_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "ethDataFifo_V_last_V", "role": "din" }} , 
 	{ "name": "ethDataFifo_V_last_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ethDataFifo_V_last_V", "role": "full_n" }} , 
 	{ "name": "ethDataFifo_V_last_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "ethDataFifo_V_last_V", "role": "write" }} , 
 	{ "name": "etherTypeFifo_V_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "etherTypeFifo_V_V", "role": "din" }} , 
 	{ "name": "etherTypeFifo_V_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "etherTypeFifo_V_V", "role": "full_n" }} , 
 	{ "name": "etherTypeFifo_V_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "etherTypeFifo_V_V", "role": "write" }} , 
 	{ "name": "s_axis_raw_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "dataIn_V_data_V", "role": "" }} , 
 	{ "name": "s_axis_raw_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "dataIn_V_last_V", "role": "Y" }} , 
 	{ "name": "s_axis_raw_TKEEP", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "dataIn_V_keep_V", "role": "" }} , 
 	{ "name": "s_axis_raw_TLAST", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "dataIn_V_last_V", "role": "" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "detect_eth_protocol",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "4", "EstimateLatencyMin" : "4", "EstimateLatencyMax" : "4",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "dataIn_V_data_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "s_axis_raw_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataIn_V_keep_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "dataIn_V_last_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "header_ready", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_idx_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_header_V_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "metaWritten_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "etherTypeFifo_V_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "etherTypeFifo_V_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ethDataFifo_V_data_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ethDataFifo_V_data_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ethDataFifo_V_keep_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ethDataFifo_V_keep_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ethDataFifo_V_last_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "ethDataFifo_V_last_V_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	detect_eth_protocol {
		dataIn_V_data_V {Type I LastRead 0 FirstWrite -1}
		dataIn_V_keep_V {Type I LastRead 0 FirstWrite -1}
		dataIn_V_last_V {Type I LastRead 0 FirstWrite -1}
		header_ready {Type IO LastRead -1 FirstWrite -1}
		header_idx_1 {Type IO LastRead -1 FirstWrite -1}
		header_header_V_1 {Type IO LastRead -1 FirstWrite -1}
		metaWritten_1 {Type IO LastRead -1 FirstWrite -1}
		etherTypeFifo_V_V {Type O LastRead -1 FirstWrite 4}
		ethDataFifo_V_data_V {Type O LastRead -1 FirstWrite 1}
		ethDataFifo_V_keep_V {Type O LastRead -1 FirstWrite 1}
		ethDataFifo_V_last_V {Type O LastRead -1 FirstWrite 1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "4", "Max" : "4"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	dataIn_V_data_V { axis {  { s_axis_raw_TVALID in_vld 0 1 }  { s_axis_raw_TDATA in_data 0 64 } } }
	dataIn_V_keep_V { axis {  { s_axis_raw_TKEEP in_data 0 8 } } }
	dataIn_V_last_V { axis {  { s_axis_raw_TREADY in_acc 1 1 }  { s_axis_raw_TLAST in_data 0 1 } } }
	etherTypeFifo_V_V { ap_fifo {  { etherTypeFifo_V_V_din fifo_data 1 16 }  { etherTypeFifo_V_V_full_n fifo_status 0 1 }  { etherTypeFifo_V_V_write fifo_update 1 1 } } }
	ethDataFifo_V_data_V { ap_fifo {  { ethDataFifo_V_data_V_din fifo_data 1 64 }  { ethDataFifo_V_data_V_full_n fifo_status 0 1 }  { ethDataFifo_V_data_V_write fifo_update 1 1 } } }
	ethDataFifo_V_keep_V { ap_fifo {  { ethDataFifo_V_keep_V_din fifo_data 1 8 }  { ethDataFifo_V_keep_V_full_n fifo_status 0 1 }  { ethDataFifo_V_keep_V_write fifo_update 1 1 } } }
	ethDataFifo_V_last_V { ap_fifo {  { ethDataFifo_V_last_V_din fifo_data 1 1 }  { ethDataFifo_V_last_V_full_n fifo_status 0 1 }  { ethDataFifo_V_last_V_write fifo_update 1 1 } } }
}
