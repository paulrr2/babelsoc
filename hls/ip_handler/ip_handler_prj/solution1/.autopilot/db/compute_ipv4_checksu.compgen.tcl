# This script segment is generated automatically by AutoPilot

# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 42 \
    name ipDataMetaFifo_V_dat \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_ipDataMetaFifo_V_dat \
    op interface \
    ports { ipDataMetaFifo_V_dat_dout { I 64 vector } ipDataMetaFifo_V_dat_empty_n { I 1 bit } ipDataMetaFifo_V_dat_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 43 \
    name ipDataMetaFifo_V_kee \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_ipDataMetaFifo_V_kee \
    op interface \
    ports { ipDataMetaFifo_V_kee_dout { I 8 vector } ipDataMetaFifo_V_kee_empty_n { I 1 bit } ipDataMetaFifo_V_kee_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 44 \
    name ipDataMetaFifo_V_las \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_ipDataMetaFifo_V_las \
    op interface \
    ports { ipDataMetaFifo_V_las_dout { I 1 vector } ipDataMetaFifo_V_las_empty_n { I 1 bit } ipDataMetaFifo_V_las_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 45 \
    name ipDataCheckFifo_V \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_ipDataCheckFifo_V \
    op interface \
    ports { ipDataCheckFifo_V_din { O 73 vector } ipDataCheckFifo_V_full_n { I 1 bit } ipDataCheckFifo_V_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 46 \
    name iph_subSumsFifoOut_V \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_iph_subSumsFifoOut_V \
    op interface \
    ports { iph_subSumsFifoOut_V_din { O 68 vector } iph_subSumsFifoOut_V_full_n { I 1 bit } iph_subSumsFifoOut_V_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_start { I 1 bit } ap_ready { O 1 bit } ap_done { O 1 bit } ap_idle { O 1 bit } ap_continue { I 1 bit } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


