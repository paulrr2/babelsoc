// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef __linux__

#include "xstatus.h"
#include "xparameters.h"
#include "xip_handler_top.h"

extern XIp_handler_top_Config XIp_handler_top_ConfigTable[];

XIp_handler_top_Config *XIp_handler_top_LookupConfig(u16 DeviceId) {
	XIp_handler_top_Config *ConfigPtr = NULL;

	int Index;

	for (Index = 0; Index < XPAR_XIP_HANDLER_TOP_NUM_INSTANCES; Index++) {
		if (XIp_handler_top_ConfigTable[Index].DeviceId == DeviceId) {
			ConfigPtr = &XIp_handler_top_ConfigTable[Index];
			break;
		}
	}

	return ConfigPtr;
}

int XIp_handler_top_Initialize(XIp_handler_top *InstancePtr, u16 DeviceId) {
	XIp_handler_top_Config *ConfigPtr;

	Xil_AssertNonvoid(InstancePtr != NULL);

	ConfigPtr = XIp_handler_top_LookupConfig(DeviceId);
	if (ConfigPtr == NULL) {
		InstancePtr->IsReady = 0;
		return (XST_DEVICE_NOT_FOUND);
	}

	return XIp_handler_top_CfgInitialize(InstancePtr, ConfigPtr);
}

#endif

