// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================
// AXILiteS
// 0x00 : reserved
// 0x04 : reserved
// 0x08 : reserved
// 0x0c : reserved
// 0x10 : Data signal of myIpAddress_V
//        bit 31~0 - myIpAddress_V[31:0] (Read/Write)
// 0x14 : reserved
// (SC = Self Clear, COR = Clear on Read, TOW = Toggle on Write, COH = Clear on Handshake)

#define XIP_HANDLER_TOP_AXILITES_ADDR_MYIPADDRESS_V_DATA 0x10
#define XIP_HANDLER_TOP_AXILITES_BITS_MYIPADDRESS_V_DATA 32

