// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2019.1
// Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

#ifndef _ip_invalid_dropper_HH_
#define _ip_invalid_dropper_HH_

#include "systemc.h"
#include "AESL_pkg.h"


namespace ap_rtl {

struct ip_invalid_dropper : public sc_module {
    // Port declarations 22
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_in< sc_logic > ap_start;
    sc_out< sc_logic > ap_done;
    sc_in< sc_logic > ap_continue;
    sc_out< sc_logic > ap_idle;
    sc_out< sc_logic > ap_ready;
    sc_in< sc_lv<73> > ipDataCheckFifo_V_dout;
    sc_in< sc_logic > ipDataCheckFifo_V_empty_n;
    sc_out< sc_logic > ipDataCheckFifo_V_read;
    sc_in< sc_lv<1> > validChecksumFifo_V_dout;
    sc_in< sc_logic > validChecksumFifo_V_empty_n;
    sc_out< sc_logic > validChecksumFifo_V_read;
    sc_in< sc_lv<1> > validIpAddressFifo_V_dout;
    sc_in< sc_logic > validIpAddressFifo_V_empty_n;
    sc_out< sc_logic > validIpAddressFifo_V_read;
    sc_out< sc_lv<73> > ipDataDropFifo_V_din;
    sc_in< sc_logic > ipDataDropFifo_V_full_n;
    sc_out< sc_logic > ipDataDropFifo_V_write;
    sc_out< sc_lv<1> > ipv4ValidFifo_V_din;
    sc_in< sc_logic > ipv4ValidFifo_V_full_n;
    sc_out< sc_logic > ipv4ValidFifo_V_write;


    // Module declarations
    ip_invalid_dropper(sc_module_name name);
    SC_HAS_PROCESS(ip_invalid_dropper);

    ~ip_invalid_dropper();

    sc_trace_file* mVcdFile;

    sc_signal< sc_logic > ap_done_reg;
    sc_signal< sc_lv<1> > ap_CS_fsm;
    sc_signal< sc_logic > ap_CS_fsm_pp0_stage0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter1;
    sc_signal< sc_logic > ap_idle_pp0;
    sc_signal< sc_lv<1> > grp_nbreadreq_fu_56_p3;
    sc_signal< bool > ap_predicate_op6_read_state1;
    sc_signal< bool > ap_predicate_op11_read_state1;
    sc_signal< sc_lv<1> > tmp_nbreadreq_fu_70_p3;
    sc_signal< sc_lv<1> > tmp_5_nbreadreq_fu_78_p3;
    sc_signal< bool > ap_predicate_op19_read_state1;
    sc_signal< bool > ap_predicate_op20_read_state1;
    sc_signal< bool > ap_block_state1_pp0_stage0_iter0;
    sc_signal< sc_lv<2> > iid_state_load_reg_155;
    sc_signal< sc_lv<1> > tmp_3_reg_167;
    sc_signal< bool > ap_predicate_op38_write_state2;
    sc_signal< sc_lv<1> > tmp_reg_180;
    sc_signal< sc_lv<1> > tmp_5_reg_184;
    sc_signal< sc_lv<1> > and_ln195_reg_188;
    sc_signal< bool > ap_predicate_op42_write_state2;
    sc_signal< bool > ap_predicate_op43_write_state2;
    sc_signal< bool > ap_block_state2_pp0_stage0_iter1;
    sc_signal< bool > ap_block_pp0_stage0_11001;
    sc_signal< sc_lv<2> > iid_state;
    sc_signal< sc_logic > validChecksumFifo_V_blk_n;
    sc_signal< bool > ap_block_pp0_stage0;
    sc_signal< sc_logic > validIpAddressFifo_V_blk_n;
    sc_signal< sc_logic > ipv4ValidFifo_V_blk_n;
    sc_signal< sc_logic > ipDataCheckFifo_V_blk_n;
    sc_signal< sc_logic > ipDataDropFifo_V_blk_n;
    sc_signal< sc_lv<1> > grp_fu_125_p3;
    sc_signal< sc_lv<73> > tmp39_reg_171;
    sc_signal< sc_lv<1> > and_ln195_fu_143_p2;
    sc_signal< bool > ap_block_pp0_stage0_subdone;
    sc_signal< sc_lv<2> > ap_phi_mux_storemerge_i_phi_fu_117_p4;
    sc_signal< sc_lv<2> > ap_phi_reg_pp0_iter0_storemerge_i_reg_114;
    sc_signal< bool > ap_block_pp0_stage0_01001;
    sc_signal< sc_lv<1> > ap_NS_fsm;
    sc_signal< sc_logic > ap_idle_pp0_0to0;
    sc_signal< sc_logic > ap_reset_idle_pp0;
    sc_signal< sc_logic > ap_enable_pp0;
    sc_signal< bool > ap_condition_256;
    sc_signal< bool > ap_condition_213;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<1> ap_ST_fsm_pp0_stage0;
    static const sc_lv<32> ap_const_lv32_0;
    static const bool ap_const_boolean_1;
    static const sc_lv<2> ap_const_lv2_2;
    static const sc_lv<1> ap_const_lv1_1;
    static const sc_lv<2> ap_const_lv2_1;
    static const sc_lv<2> ap_const_lv2_0;
    static const sc_lv<1> ap_const_lv1_0;
    static const bool ap_const_boolean_0;
    static const sc_lv<32> ap_const_lv32_48;
    // Thread declarations
    void thread_ap_clk_no_reset_();
    void thread_and_ln195_fu_143_p2();
    void thread_ap_CS_fsm_pp0_stage0();
    void thread_ap_block_pp0_stage0();
    void thread_ap_block_pp0_stage0_01001();
    void thread_ap_block_pp0_stage0_11001();
    void thread_ap_block_pp0_stage0_subdone();
    void thread_ap_block_state1_pp0_stage0_iter0();
    void thread_ap_block_state2_pp0_stage0_iter1();
    void thread_ap_condition_213();
    void thread_ap_condition_256();
    void thread_ap_done();
    void thread_ap_enable_pp0();
    void thread_ap_enable_reg_pp0_iter0();
    void thread_ap_idle();
    void thread_ap_idle_pp0();
    void thread_ap_idle_pp0_0to0();
    void thread_ap_phi_mux_storemerge_i_phi_fu_117_p4();
    void thread_ap_phi_reg_pp0_iter0_storemerge_i_reg_114();
    void thread_ap_predicate_op11_read_state1();
    void thread_ap_predicate_op19_read_state1();
    void thread_ap_predicate_op20_read_state1();
    void thread_ap_predicate_op38_write_state2();
    void thread_ap_predicate_op42_write_state2();
    void thread_ap_predicate_op43_write_state2();
    void thread_ap_predicate_op6_read_state1();
    void thread_ap_ready();
    void thread_ap_reset_idle_pp0();
    void thread_grp_fu_125_p3();
    void thread_grp_nbreadreq_fu_56_p3();
    void thread_ipDataCheckFifo_V_blk_n();
    void thread_ipDataCheckFifo_V_read();
    void thread_ipDataDropFifo_V_blk_n();
    void thread_ipDataDropFifo_V_din();
    void thread_ipDataDropFifo_V_write();
    void thread_ipv4ValidFifo_V_blk_n();
    void thread_ipv4ValidFifo_V_din();
    void thread_ipv4ValidFifo_V_write();
    void thread_tmp_5_nbreadreq_fu_78_p3();
    void thread_tmp_nbreadreq_fu_70_p3();
    void thread_validChecksumFifo_V_blk_n();
    void thread_validChecksumFifo_V_read();
    void thread_validIpAddressFifo_V_blk_n();
    void thread_validIpAddressFifo_V_read();
    void thread_ap_NS_fsm();
};

}

using namespace ap_rtl;

#endif
