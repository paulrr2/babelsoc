// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2019.1
// Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

#ifndef _detect_ipv4_protocol_HH_
#define _detect_ipv4_protocol_HH_

#include "systemc.h"
#include "AESL_pkg.h"


namespace ap_rtl {

struct detect_ipv4_protocol : public sc_module {
    // Port declarations 29
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_in< sc_logic > ap_start;
    sc_out< sc_logic > ap_done;
    sc_in< sc_logic > ap_continue;
    sc_out< sc_logic > ap_idle;
    sc_out< sc_logic > ap_ready;
    sc_in< sc_lv<8> > ipv4ProtocolFifo_V_V_dout;
    sc_in< sc_logic > ipv4ProtocolFifo_V_V_empty_n;
    sc_out< sc_logic > ipv4ProtocolFifo_V_V_read;
    sc_in< sc_lv<1> > ipv4ValidFifo_V_dout;
    sc_in< sc_logic > ipv4ValidFifo_V_empty_n;
    sc_out< sc_logic > ipv4ValidFifo_V_read;
    sc_in< sc_lv<73> > ipDataCutFifo_V_dout;
    sc_in< sc_logic > ipDataCutFifo_V_empty_n;
    sc_out< sc_logic > ipDataCutFifo_V_read;
    sc_out< sc_lv<73> > udpDataFifo_V_din;
    sc_in< sc_logic > udpDataFifo_V_full_n;
    sc_out< sc_logic > udpDataFifo_V_write;
    sc_in< sc_logic > m_axis_tcp_TREADY;
    sc_in< sc_logic > m_axis_icmp_TREADY;
    sc_out< sc_lv<64> > m_axis_icmp_TDATA;
    sc_out< sc_logic > m_axis_icmp_TVALID;
    sc_out< sc_lv<8> > m_axis_icmp_TKEEP;
    sc_out< sc_lv<1> > m_axis_icmp_TLAST;
    sc_out< sc_lv<64> > m_axis_tcp_TDATA;
    sc_out< sc_logic > m_axis_tcp_TVALID;
    sc_out< sc_lv<8> > m_axis_tcp_TKEEP;
    sc_out< sc_lv<1> > m_axis_tcp_TLAST;


    // Module declarations
    detect_ipv4_protocol(sc_module_name name);
    SC_HAS_PROCESS(detect_ipv4_protocol);

    ~detect_ipv4_protocol();

    sc_trace_file* mVcdFile;

    sc_signal< sc_logic > ap_done_reg;
    sc_signal< sc_lv<1> > ap_CS_fsm;
    sc_signal< sc_logic > ap_CS_fsm_pp0_stage0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter1;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter2;
    sc_signal< sc_logic > ap_idle_pp0;
    sc_signal< sc_lv<1> > tmp_18_nbreadreq_fu_82_p3;
    sc_signal< sc_lv<1> > tmp_20_nbreadreq_fu_90_p3;
    sc_signal< bool > ap_predicate_op10_read_state1;
    sc_signal< bool > ap_predicate_op12_read_state1;
    sc_signal< sc_lv<1> > tmp_nbreadreq_fu_110_p3;
    sc_signal< bool > ap_predicate_op17_read_state1;
    sc_signal< bool > ap_block_state1_pp0_stage0_iter0;
    sc_signal< sc_lv<1> > dip_state_load_reg_205;
    sc_signal< sc_lv<1> > tmp_reg_221;
    sc_signal< sc_lv<8> > dip_ipProtocol_V_loa_reg_248;
    sc_signal< bool > ap_predicate_op25_write_state2;
    sc_signal< bool > ap_block_state2_pp0_stage0_iter1;
    sc_signal< sc_logic > TCPdataOut_V_data_V_1_ack_in;
    sc_signal< bool > ap_predicate_op24_write_state2;
    sc_signal< sc_logic > ICMPdataOut_V_data_V_1_ack_in;
    sc_signal< bool > ap_predicate_op26_write_state2;
    sc_signal< bool > ap_block_state2_io;
    sc_signal< sc_logic > ICMPdataOut_V_data_V_1_ack_out;
    sc_signal< sc_lv<2> > ICMPdataOut_V_data_V_1_state;
    sc_signal< sc_logic > ICMPdataOut_V_keep_V_1_ack_out;
    sc_signal< sc_lv<2> > ICMPdataOut_V_keep_V_1_state;
    sc_signal< sc_logic > ICMPdataOut_V_last_V_1_ack_out;
    sc_signal< sc_lv<2> > ICMPdataOut_V_last_V_1_state;
    sc_signal< sc_logic > TCPdataOut_V_data_V_1_ack_out;
    sc_signal< sc_lv<2> > TCPdataOut_V_data_V_1_state;
    sc_signal< sc_logic > TCPdataOut_V_keep_V_1_ack_out;
    sc_signal< sc_lv<2> > TCPdataOut_V_keep_V_1_state;
    sc_signal< sc_logic > TCPdataOut_V_last_V_1_ack_out;
    sc_signal< sc_lv<2> > TCPdataOut_V_last_V_1_state;
    sc_signal< bool > ap_block_state3_pp0_stage0_iter2;
    sc_signal< sc_lv<1> > dip_state_load_reg_205_pp0_iter1_reg;
    sc_signal< sc_lv<1> > tmp_reg_221_pp0_iter1_reg;
    sc_signal< sc_lv<8> > dip_ipProtocol_V_loa_reg_248_pp0_iter1_reg;
    sc_signal< bool > ap_predicate_op38_write_state3;
    sc_signal< bool > ap_predicate_op41_write_state3;
    sc_signal< bool > ap_block_state3_io;
    sc_signal< bool > ap_block_pp0_stage0_11001;
    sc_signal< sc_lv<64> > ICMPdataOut_V_data_V_1_data_out;
    sc_signal< sc_logic > ICMPdataOut_V_data_V_1_vld_in;
    sc_signal< sc_logic > ICMPdataOut_V_data_V_1_vld_out;
    sc_signal< sc_lv<64> > ICMPdataOut_V_data_V_1_payload_A;
    sc_signal< sc_lv<64> > ICMPdataOut_V_data_V_1_payload_B;
    sc_signal< sc_logic > ICMPdataOut_V_data_V_1_sel_rd;
    sc_signal< sc_logic > ICMPdataOut_V_data_V_1_sel_wr;
    sc_signal< sc_logic > ICMPdataOut_V_data_V_1_sel;
    sc_signal< sc_logic > ICMPdataOut_V_data_V_1_load_A;
    sc_signal< sc_logic > ICMPdataOut_V_data_V_1_load_B;
    sc_signal< sc_logic > ICMPdataOut_V_data_V_1_state_cmp_full;
    sc_signal< sc_lv<8> > ICMPdataOut_V_keep_V_1_data_out;
    sc_signal< sc_logic > ICMPdataOut_V_keep_V_1_vld_in;
    sc_signal< sc_logic > ICMPdataOut_V_keep_V_1_vld_out;
    sc_signal< sc_logic > ICMPdataOut_V_keep_V_1_ack_in;
    sc_signal< sc_lv<8> > ICMPdataOut_V_keep_V_1_payload_A;
    sc_signal< sc_lv<8> > ICMPdataOut_V_keep_V_1_payload_B;
    sc_signal< sc_logic > ICMPdataOut_V_keep_V_1_sel_rd;
    sc_signal< sc_logic > ICMPdataOut_V_keep_V_1_sel_wr;
    sc_signal< sc_logic > ICMPdataOut_V_keep_V_1_sel;
    sc_signal< sc_logic > ICMPdataOut_V_keep_V_1_load_A;
    sc_signal< sc_logic > ICMPdataOut_V_keep_V_1_load_B;
    sc_signal< sc_logic > ICMPdataOut_V_keep_V_1_state_cmp_full;
    sc_signal< sc_lv<1> > ICMPdataOut_V_last_V_1_data_out;
    sc_signal< sc_logic > ICMPdataOut_V_last_V_1_vld_in;
    sc_signal< sc_logic > ICMPdataOut_V_last_V_1_vld_out;
    sc_signal< sc_logic > ICMPdataOut_V_last_V_1_ack_in;
    sc_signal< sc_lv<1> > ICMPdataOut_V_last_V_1_payload_A;
    sc_signal< sc_lv<1> > ICMPdataOut_V_last_V_1_payload_B;
    sc_signal< sc_logic > ICMPdataOut_V_last_V_1_sel_rd;
    sc_signal< sc_logic > ICMPdataOut_V_last_V_1_sel_wr;
    sc_signal< sc_logic > ICMPdataOut_V_last_V_1_sel;
    sc_signal< sc_logic > ICMPdataOut_V_last_V_1_load_A;
    sc_signal< sc_logic > ICMPdataOut_V_last_V_1_load_B;
    sc_signal< sc_logic > ICMPdataOut_V_last_V_1_state_cmp_full;
    sc_signal< sc_lv<64> > TCPdataOut_V_data_V_1_data_out;
    sc_signal< sc_logic > TCPdataOut_V_data_V_1_vld_in;
    sc_signal< sc_logic > TCPdataOut_V_data_V_1_vld_out;
    sc_signal< sc_lv<64> > TCPdataOut_V_data_V_1_payload_A;
    sc_signal< sc_lv<64> > TCPdataOut_V_data_V_1_payload_B;
    sc_signal< sc_logic > TCPdataOut_V_data_V_1_sel_rd;
    sc_signal< sc_logic > TCPdataOut_V_data_V_1_sel_wr;
    sc_signal< sc_logic > TCPdataOut_V_data_V_1_sel;
    sc_signal< sc_logic > TCPdataOut_V_data_V_1_load_A;
    sc_signal< sc_logic > TCPdataOut_V_data_V_1_load_B;
    sc_signal< sc_logic > TCPdataOut_V_data_V_1_state_cmp_full;
    sc_signal< sc_lv<8> > TCPdataOut_V_keep_V_1_data_out;
    sc_signal< sc_logic > TCPdataOut_V_keep_V_1_vld_in;
    sc_signal< sc_logic > TCPdataOut_V_keep_V_1_vld_out;
    sc_signal< sc_logic > TCPdataOut_V_keep_V_1_ack_in;
    sc_signal< sc_lv<8> > TCPdataOut_V_keep_V_1_payload_A;
    sc_signal< sc_lv<8> > TCPdataOut_V_keep_V_1_payload_B;
    sc_signal< sc_logic > TCPdataOut_V_keep_V_1_sel_rd;
    sc_signal< sc_logic > TCPdataOut_V_keep_V_1_sel_wr;
    sc_signal< sc_logic > TCPdataOut_V_keep_V_1_sel;
    sc_signal< sc_logic > TCPdataOut_V_keep_V_1_load_A;
    sc_signal< sc_logic > TCPdataOut_V_keep_V_1_load_B;
    sc_signal< sc_logic > TCPdataOut_V_keep_V_1_state_cmp_full;
    sc_signal< sc_lv<1> > TCPdataOut_V_last_V_1_data_out;
    sc_signal< sc_logic > TCPdataOut_V_last_V_1_vld_in;
    sc_signal< sc_logic > TCPdataOut_V_last_V_1_vld_out;
    sc_signal< sc_logic > TCPdataOut_V_last_V_1_ack_in;
    sc_signal< sc_lv<1> > TCPdataOut_V_last_V_1_payload_A;
    sc_signal< sc_lv<1> > TCPdataOut_V_last_V_1_payload_B;
    sc_signal< sc_logic > TCPdataOut_V_last_V_1_sel_rd;
    sc_signal< sc_logic > TCPdataOut_V_last_V_1_sel_wr;
    sc_signal< sc_logic > TCPdataOut_V_last_V_1_sel;
    sc_signal< sc_logic > TCPdataOut_V_last_V_1_load_A;
    sc_signal< sc_logic > TCPdataOut_V_last_V_1_load_B;
    sc_signal< sc_logic > TCPdataOut_V_last_V_1_state_cmp_full;
    sc_signal< sc_lv<1> > dip_state;
    sc_signal< sc_lv<8> > dip_ipProtocol_V;
    sc_signal< sc_logic > m_axis_icmp_TDATA_blk_n;
    sc_signal< bool > ap_block_pp0_stage0;
    sc_signal< sc_logic > m_axis_tcp_TDATA_blk_n;
    sc_signal< sc_logic > ipv4ProtocolFifo_V_V_blk_n;
    sc_signal< sc_logic > ipv4ValidFifo_V_blk_n;
    sc_signal< sc_logic > ipDataCutFifo_V_blk_n;
    sc_signal< sc_logic > udpDataFifo_V_blk_n;
    sc_signal< sc_lv<1> > tmp_22_read_fu_104_p2;
    sc_signal< sc_lv<73> > tmp9_reg_225;
    sc_signal< sc_lv<64> > tmp_data_V_fu_173_p1;
    sc_signal< sc_lv<64> > tmp_data_V_reg_230;
    sc_signal< sc_lv<8> > tmp_keep_V_reg_236;
    sc_signal< sc_lv<1> > tmp_last_V_fu_187_p3;
    sc_signal< sc_lv<1> > tmp_last_V_reg_242;
    sc_signal< bool > ap_block_pp0_stage0_subdone;
    sc_signal< bool > ap_block_pp0_stage0_01001;
    sc_signal< sc_lv<1> > ap_NS_fsm;
    sc_signal< sc_logic > ap_idle_pp0_0to1;
    sc_signal< sc_logic > ap_reset_idle_pp0;
    sc_signal< sc_logic > ap_enable_pp0;
    sc_signal< bool > ap_condition_588;
    sc_signal< bool > ap_condition_591;
    sc_signal< bool > ap_condition_216;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<1> ap_ST_fsm_pp0_stage0;
    static const sc_lv<32> ap_const_lv32_0;
    static const bool ap_const_boolean_1;
    static const sc_lv<1> ap_const_lv1_0;
    static const sc_lv<1> ap_const_lv1_1;
    static const sc_lv<8> ap_const_lv8_11;
    static const sc_lv<8> ap_const_lv8_6;
    static const sc_lv<8> ap_const_lv8_1;
    static const sc_lv<2> ap_const_lv2_0;
    static const sc_lv<2> ap_const_lv2_2;
    static const sc_lv<2> ap_const_lv2_3;
    static const sc_lv<2> ap_const_lv2_1;
    static const bool ap_const_boolean_0;
    static const sc_lv<32> ap_const_lv32_40;
    static const sc_lv<32> ap_const_lv32_47;
    static const sc_lv<32> ap_const_lv32_48;
    // Thread declarations
    void thread_ap_clk_no_reset_();
    void thread_ICMPdataOut_V_data_V_1_ack_in();
    void thread_ICMPdataOut_V_data_V_1_ack_out();
    void thread_ICMPdataOut_V_data_V_1_data_out();
    void thread_ICMPdataOut_V_data_V_1_load_A();
    void thread_ICMPdataOut_V_data_V_1_load_B();
    void thread_ICMPdataOut_V_data_V_1_sel();
    void thread_ICMPdataOut_V_data_V_1_state_cmp_full();
    void thread_ICMPdataOut_V_data_V_1_vld_in();
    void thread_ICMPdataOut_V_data_V_1_vld_out();
    void thread_ICMPdataOut_V_keep_V_1_ack_in();
    void thread_ICMPdataOut_V_keep_V_1_ack_out();
    void thread_ICMPdataOut_V_keep_V_1_data_out();
    void thread_ICMPdataOut_V_keep_V_1_load_A();
    void thread_ICMPdataOut_V_keep_V_1_load_B();
    void thread_ICMPdataOut_V_keep_V_1_sel();
    void thread_ICMPdataOut_V_keep_V_1_state_cmp_full();
    void thread_ICMPdataOut_V_keep_V_1_vld_in();
    void thread_ICMPdataOut_V_keep_V_1_vld_out();
    void thread_ICMPdataOut_V_last_V_1_ack_in();
    void thread_ICMPdataOut_V_last_V_1_ack_out();
    void thread_ICMPdataOut_V_last_V_1_data_out();
    void thread_ICMPdataOut_V_last_V_1_load_A();
    void thread_ICMPdataOut_V_last_V_1_load_B();
    void thread_ICMPdataOut_V_last_V_1_sel();
    void thread_ICMPdataOut_V_last_V_1_state_cmp_full();
    void thread_ICMPdataOut_V_last_V_1_vld_in();
    void thread_ICMPdataOut_V_last_V_1_vld_out();
    void thread_TCPdataOut_V_data_V_1_ack_in();
    void thread_TCPdataOut_V_data_V_1_ack_out();
    void thread_TCPdataOut_V_data_V_1_data_out();
    void thread_TCPdataOut_V_data_V_1_load_A();
    void thread_TCPdataOut_V_data_V_1_load_B();
    void thread_TCPdataOut_V_data_V_1_sel();
    void thread_TCPdataOut_V_data_V_1_state_cmp_full();
    void thread_TCPdataOut_V_data_V_1_vld_in();
    void thread_TCPdataOut_V_data_V_1_vld_out();
    void thread_TCPdataOut_V_keep_V_1_ack_in();
    void thread_TCPdataOut_V_keep_V_1_ack_out();
    void thread_TCPdataOut_V_keep_V_1_data_out();
    void thread_TCPdataOut_V_keep_V_1_load_A();
    void thread_TCPdataOut_V_keep_V_1_load_B();
    void thread_TCPdataOut_V_keep_V_1_sel();
    void thread_TCPdataOut_V_keep_V_1_state_cmp_full();
    void thread_TCPdataOut_V_keep_V_1_vld_in();
    void thread_TCPdataOut_V_keep_V_1_vld_out();
    void thread_TCPdataOut_V_last_V_1_ack_in();
    void thread_TCPdataOut_V_last_V_1_ack_out();
    void thread_TCPdataOut_V_last_V_1_data_out();
    void thread_TCPdataOut_V_last_V_1_load_A();
    void thread_TCPdataOut_V_last_V_1_load_B();
    void thread_TCPdataOut_V_last_V_1_sel();
    void thread_TCPdataOut_V_last_V_1_state_cmp_full();
    void thread_TCPdataOut_V_last_V_1_vld_in();
    void thread_TCPdataOut_V_last_V_1_vld_out();
    void thread_ap_CS_fsm_pp0_stage0();
    void thread_ap_block_pp0_stage0();
    void thread_ap_block_pp0_stage0_01001();
    void thread_ap_block_pp0_stage0_11001();
    void thread_ap_block_pp0_stage0_subdone();
    void thread_ap_block_state1_pp0_stage0_iter0();
    void thread_ap_block_state2_io();
    void thread_ap_block_state2_pp0_stage0_iter1();
    void thread_ap_block_state3_io();
    void thread_ap_block_state3_pp0_stage0_iter2();
    void thread_ap_condition_216();
    void thread_ap_condition_588();
    void thread_ap_condition_591();
    void thread_ap_done();
    void thread_ap_enable_pp0();
    void thread_ap_enable_reg_pp0_iter0();
    void thread_ap_idle();
    void thread_ap_idle_pp0();
    void thread_ap_idle_pp0_0to1();
    void thread_ap_predicate_op10_read_state1();
    void thread_ap_predicate_op12_read_state1();
    void thread_ap_predicate_op17_read_state1();
    void thread_ap_predicate_op24_write_state2();
    void thread_ap_predicate_op25_write_state2();
    void thread_ap_predicate_op26_write_state2();
    void thread_ap_predicate_op38_write_state3();
    void thread_ap_predicate_op41_write_state3();
    void thread_ap_ready();
    void thread_ap_reset_idle_pp0();
    void thread_ipDataCutFifo_V_blk_n();
    void thread_ipDataCutFifo_V_read();
    void thread_ipv4ProtocolFifo_V_V_blk_n();
    void thread_ipv4ProtocolFifo_V_V_read();
    void thread_ipv4ValidFifo_V_blk_n();
    void thread_ipv4ValidFifo_V_read();
    void thread_m_axis_icmp_TDATA();
    void thread_m_axis_icmp_TDATA_blk_n();
    void thread_m_axis_icmp_TKEEP();
    void thread_m_axis_icmp_TLAST();
    void thread_m_axis_icmp_TVALID();
    void thread_m_axis_tcp_TDATA();
    void thread_m_axis_tcp_TDATA_blk_n();
    void thread_m_axis_tcp_TKEEP();
    void thread_m_axis_tcp_TLAST();
    void thread_m_axis_tcp_TVALID();
    void thread_tmp_18_nbreadreq_fu_82_p3();
    void thread_tmp_20_nbreadreq_fu_90_p3();
    void thread_tmp_22_read_fu_104_p2();
    void thread_tmp_data_V_fu_173_p1();
    void thread_tmp_last_V_fu_187_p3();
    void thread_tmp_nbreadreq_fu_110_p3();
    void thread_udpDataFifo_V_blk_n();
    void thread_udpDataFifo_V_din();
    void thread_udpDataFifo_V_write();
    void thread_ap_NS_fsm();
};

}

using namespace ap_rtl;

#endif
