set moduleName iperf_udp
set isTopModule 1
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type dataflow
set FunctionProtocol ap_ctrl_none
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {iperf_udp}
set C_modelType { void 0 }
set C_modelArgList {
	{ s_axis_rx_metadata_V int 176 regular {axi_s 0 volatile  { s_axis_rx_metadata_V Data } }  }
	{ s_axis_rx_data_V_data_V int 64 regular {axi_s 0 volatile  { s_axis_rx_data Data } }  }
	{ s_axis_rx_data_V_keep_V int 8 regular {axi_s 0 volatile  { s_axis_rx_data Keep } }  }
	{ s_axis_rx_data_V_last_V int 1 regular {axi_s 0 volatile  { s_axis_rx_data Last } }  }
	{ m_axis_tx_metadata_V int 176 regular {axi_s 1 volatile  { m_axis_tx_metadata_V Data } }  }
	{ m_axis_tx_data_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_tx_data Data } }  }
	{ m_axis_tx_data_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_tx_data Keep } }  }
	{ m_axis_tx_data_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_tx_data Last } }  }
	{ runExperiment_V int 1 regular {ap_stable 0} }
	{ pkgWordCount_V int 8 regular {ap_stable 0} }
	{ packetGap_V int 8 regular {ap_stable 0} }
	{ targetIpAddress_V int 32 regular {ap_stable 0} }
}
set C_modelArgMapList {[ 
	{ "Name" : "s_axis_rx_metadata_V", "interface" : "axis", "bitwidth" : 176, "direction" : "READONLY", "bitSlice":[{"low":0,"up":127,"cElement": [{"cName": "s_axis_rx_metadata.V.their_address.V","cData": "uint128","bit_use": { "low": 0,"up": 127},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":128,"up":143,"cElement": [{"cName": "s_axis_rx_metadata.V.their_port.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":144,"up":159,"cElement": [{"cName": "s_axis_rx_metadata.V.my_port.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":160,"up":175,"cElement": [{"cName": "s_axis_rx_metadata.V.length.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_rx_data_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "READONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "s_axis_rx_data.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_rx_data_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "s_axis_rx_data.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_rx_data_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "s_axis_rx_data.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_tx_metadata_V", "interface" : "axis", "bitwidth" : 176, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":127,"cElement": [{"cName": "m_axis_tx_metadata.V.their_address.V","cData": "uint128","bit_use": { "low": 0,"up": 127},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":128,"up":143,"cElement": [{"cName": "m_axis_tx_metadata.V.their_port.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":144,"up":159,"cElement": [{"cName": "m_axis_tx_metadata.V.my_port.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":160,"up":175,"cElement": [{"cName": "m_axis_tx_metadata.V.length.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_tx_data_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "m_axis_tx_data.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_tx_data_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "m_axis_tx_data.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_tx_data_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "m_axis_tx_data.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "runExperiment_V", "interface" : "wire", "bitwidth" : 1, "direction" : "READONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "runExperiment.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}]} , 
 	{ "Name" : "pkgWordCount_V", "interface" : "wire", "bitwidth" : 8, "direction" : "READONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "pkgWordCount.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}]} , 
 	{ "Name" : "packetGap_V", "interface" : "wire", "bitwidth" : 8, "direction" : "READONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "packetGap.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}]} , 
 	{ "Name" : "targetIpAddress_V", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "targetIpAddress.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}]} ]}
# RTL Port declarations: 
set portNum 22
set portList { 
	{ s_axis_rx_metadata_V_TDATA sc_in sc_lv 176 signal 0 } 
	{ s_axis_rx_data_TDATA sc_in sc_lv 64 signal 1 } 
	{ s_axis_rx_data_TKEEP sc_in sc_lv 8 signal 2 } 
	{ s_axis_rx_data_TLAST sc_in sc_lv 1 signal 3 } 
	{ m_axis_tx_metadata_V_TDATA sc_out sc_lv 176 signal 4 } 
	{ m_axis_tx_data_TDATA sc_out sc_lv 64 signal 5 } 
	{ m_axis_tx_data_TKEEP sc_out sc_lv 8 signal 6 } 
	{ m_axis_tx_data_TLAST sc_out sc_lv 1 signal 7 } 
	{ runExperiment_V sc_in sc_lv 1 signal 8 } 
	{ pkgWordCount_V sc_in sc_lv 8 signal 9 } 
	{ packetGap_V sc_in sc_lv 8 signal 10 } 
	{ targetIpAddress_V sc_in sc_lv 32 signal 11 } 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst_n sc_in sc_logic 1 reset -1 active_low_sync } 
	{ m_axis_tx_metadata_V_TVALID sc_out sc_logic 1 outvld 4 } 
	{ m_axis_tx_metadata_V_TREADY sc_in sc_logic 1 outacc 4 } 
	{ m_axis_tx_data_TVALID sc_out sc_logic 1 outvld 7 } 
	{ m_axis_tx_data_TREADY sc_in sc_logic 1 outacc 7 } 
	{ s_axis_rx_metadata_V_TVALID sc_in sc_logic 1 invld 0 } 
	{ s_axis_rx_metadata_V_TREADY sc_out sc_logic 1 inacc 0 } 
	{ s_axis_rx_data_TVALID sc_in sc_logic 1 invld 3 } 
	{ s_axis_rx_data_TREADY sc_out sc_logic 1 inacc 3 } 
}
set NewPortList {[ 
	{ "name": "s_axis_rx_metadata_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":176, "type": "signal", "bundle":{"name": "s_axis_rx_metadata_V", "role": "TDATA" }} , 
 	{ "name": "s_axis_rx_data_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "s_axis_rx_data_V_data_V", "role": "default" }} , 
 	{ "name": "s_axis_rx_data_TKEEP", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "s_axis_rx_data_V_keep_V", "role": "default" }} , 
 	{ "name": "s_axis_rx_data_TLAST", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "s_axis_rx_data_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_tx_metadata_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":176, "type": "signal", "bundle":{"name": "m_axis_tx_metadata_V", "role": "TDATA" }} , 
 	{ "name": "m_axis_tx_data_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "m_axis_tx_data_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_tx_data_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "m_axis_tx_data_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_tx_data_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "m_axis_tx_data_V_last_V", "role": "default" }} , 
 	{ "name": "runExperiment_V", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "runExperiment_V", "role": "default" }} , 
 	{ "name": "pkgWordCount_V", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "pkgWordCount_V", "role": "default" }} , 
 	{ "name": "packetGap_V", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "packetGap_V", "role": "default" }} , 
 	{ "name": "targetIpAddress_V", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "targetIpAddress_V", "role": "default" }} , 
 	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst_n", "role": "default" }} , 
 	{ "name": "m_axis_tx_metadata_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_tx_metadata_V", "role": "TVALID" }} , 
 	{ "name": "m_axis_tx_metadata_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_tx_metadata_V", "role": "TREADY" }} , 
 	{ "name": "m_axis_tx_data_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_tx_data_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_tx_data_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_tx_data_V_last_V", "role": "default" }} , 
 	{ "name": "s_axis_rx_metadata_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "s_axis_rx_metadata_V", "role": "TVALID" }} , 
 	{ "name": "s_axis_rx_metadata_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "s_axis_rx_metadata_V", "role": "TREADY" }} , 
 	{ "name": "s_axis_rx_data_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "s_axis_rx_data_V_last_V", "role": "default" }} , 
 	{ "name": "s_axis_rx_data_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "s_axis_rx_data_V_last_V", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "6", "7", "8", "9", "10"],
		"CDFG" : "iperf_udp",
		"Protocol" : "ap_ctrl_none",
		"ControlExist" : "0", "ap_start" : "0", "ap_ready" : "0", "ap_done" : "0", "ap_continue" : "0", "ap_idle" : "0",
		"Pipeline" : "Dataflow", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "1",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "3", "EstimateLatencyMax" : "3",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "1",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"InputProcess" : [
			{"ID" : "1", "Name" : "client_64_U0"},
			{"ID" : "6", "Name" : "check_for_response_U0"}],
		"OutputProcess" : [
			{"ID" : "1", "Name" : "client_64_U0"},
			{"ID" : "6", "Name" : "check_for_response_U0"},
			{"ID" : "7", "Name" : "clock_U0"}],
		"Port" : [
			{"Name" : "s_axis_rx_metadata_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "6", "SubInstance" : "check_for_response_U0", "Port" : "rxMetaData_V"}]},
			{"Name" : "s_axis_rx_data_V_data_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "6", "SubInstance" : "check_for_response_U0", "Port" : "rxData_V_data_V"}]},
			{"Name" : "s_axis_rx_data_V_keep_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "6", "SubInstance" : "check_for_response_U0", "Port" : "rxData_V_keep_V"}]},
			{"Name" : "s_axis_rx_data_V_last_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "6", "SubInstance" : "check_for_response_U0", "Port" : "rxData_V_last_V"}]},
			{"Name" : "m_axis_tx_metadata_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "client_64_U0", "Port" : "txMetaData_V"}]},
			{"Name" : "m_axis_tx_data_V_data_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "client_64_U0", "Port" : "txData_V_data_V"}]},
			{"Name" : "m_axis_tx_data_V_keep_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "client_64_U0", "Port" : "txData_V_keep_V"}]},
			{"Name" : "m_axis_tx_data_V_last_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "client_64_U0", "Port" : "txData_V_last_V"}]},
			{"Name" : "runExperiment_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "pkgWordCount_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "packetGap_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "targetIpAddress_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "cycleCounter_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "client_64_U0", "Port" : "cycleCounter_V"}]},
			{"Name" : "microsecondCounter_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "client_64_U0", "Port" : "microsecondCounter_V"}]},
			{"Name" : "secondCounter_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "client_64_U0", "Port" : "secondCounter_V"}]},
			{"Name" : "iperfFsmState", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "client_64_U0", "Port" : "iperfFsmState"}]},
			{"Name" : "timeOver", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "client_64_U0", "Port" : "timeOver"}]},
			{"Name" : "lastPkg", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "client_64_U0", "Port" : "lastPkg"}]},
			{"Name" : "receivedResponse", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "client_64_U0", "Port" : "receivedResponse"}]},
			{"Name" : "seqNumber_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "client_64_U0", "Port" : "seqNumber_V"}]},
			{"Name" : "header_idx", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "client_64_U0", "Port" : "header_idx"}]},
			{"Name" : "wordCount_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "client_64_U0", "Port" : "wordCount_V"}]},
			{"Name" : "startSignalFifo_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "client_64_U0", "Port" : "startSignalFifo_V"},
					{"ID" : "7", "SubInstance" : "clock_U0", "Port" : "startSignalFifo_V"}]},
			{"Name" : "header_header_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "client_64_U0", "Port" : "header_header_V"}]},
			{"Name" : "metaWritten", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "client_64_U0", "Port" : "metaWritten"}]},
			{"Name" : "waitCounter_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "client_64_U0", "Port" : "waitCounter_V"}]},
			{"Name" : "packetGapCounter_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "client_64_U0", "Port" : "packetGapCounter_V"}]},
			{"Name" : "stopSignalFifo_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "client_64_U0", "Port" : "stopSignalFifo_V"},
					{"ID" : "7", "SubInstance" : "clock_U0", "Port" : "stopSignalFifo_V"}]},
			{"Name" : "doneSignalFifo_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "client_64_U0", "Port" : "doneSignalFifo_V"},
					{"ID" : "6", "SubInstance" : "check_for_response_U0", "Port" : "doneSignalFifo_V"}]},
			{"Name" : "startClock", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "7", "SubInstance" : "clock_U0", "Port" : "startClock"}]},
			{"Name" : "time_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "7", "SubInstance" : "clock_U0", "Port" : "time_V"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.client_64_U0", "Parent" : "0", "Child" : ["2", "3", "4", "5"],
		"CDFG" : "client_64_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "txMetaData_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "txMetaData_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txData_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_tx_data_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txData_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "txData_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "runExperiment_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "pkgWordCount_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "packetGap_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "targetIpAddress_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "cycleCounter_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "microsecondCounter_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "secondCounter_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "iperfFsmState", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "timeOver", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "lastPkg", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "receivedResponse", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "seqNumber_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_idx", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "wordCount_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "startSignalFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "7", "DependentChan" : "8",
				"BlockSignal" : [
					{"Name" : "startSignalFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "header_header_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "metaWritten", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "waitCounter_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "packetGapCounter_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "stopSignalFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "7", "DependentChan" : "9",
				"BlockSignal" : [
					{"Name" : "stopSignalFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "doneSignalFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "6", "DependentChan" : "10",
				"BlockSignal" : [
					{"Name" : "doneSignalFifo_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "2", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.client_64_U0.regslice_both_txMetaData_V_U", "Parent" : "1"},
	{"ID" : "3", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.client_64_U0.regslice_both_txData_V_data_V_U", "Parent" : "1"},
	{"ID" : "4", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.client_64_U0.regslice_both_txData_V_keep_V_U", "Parent" : "1"},
	{"ID" : "5", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.client_64_U0.regslice_both_txData_V_last_V_U", "Parent" : "1"},
	{"ID" : "6", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.check_for_response_U0", "Parent" : "0",
		"CDFG" : "check_for_response",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "0", "EstimateLatencyMin" : "0", "EstimateLatencyMax" : "0",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "rxMetaData_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "rxMetaData_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxData_V_data_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "s_axis_rx_data_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxData_V_keep_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "rxData_V_last_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "doneSignalFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "1", "DependentChan" : "10",
				"BlockSignal" : [
					{"Name" : "doneSignalFifo_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "7", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.clock_U0", "Parent" : "0",
		"CDFG" : "clock",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "startClock", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "time_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "stopSignalFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "1", "DependentChan" : "9",
				"BlockSignal" : [
					{"Name" : "stopSignalFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "startSignalFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "1", "DependentChan" : "8",
				"BlockSignal" : [
					{"Name" : "startSignalFifo_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "8", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.startSignalFifo_V_U", "Parent" : "0"},
	{"ID" : "9", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.stopSignalFifo_V_U", "Parent" : "0"},
	{"ID" : "10", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.doneSignalFifo_V_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	iperf_udp {
		s_axis_rx_metadata_V {Type I LastRead 0 FirstWrite -1}
		s_axis_rx_data_V_data_V {Type I LastRead 0 FirstWrite -1}
		s_axis_rx_data_V_keep_V {Type I LastRead 0 FirstWrite -1}
		s_axis_rx_data_V_last_V {Type I LastRead 0 FirstWrite -1}
		m_axis_tx_metadata_V {Type O LastRead -1 FirstWrite 1}
		m_axis_tx_data_V_data_V {Type O LastRead -1 FirstWrite 1}
		m_axis_tx_data_V_keep_V {Type O LastRead -1 FirstWrite 1}
		m_axis_tx_data_V_last_V {Type O LastRead -1 FirstWrite 1}
		runExperiment_V {Type I LastRead 0 FirstWrite -1}
		pkgWordCount_V {Type I LastRead 0 FirstWrite -1}
		packetGap_V {Type I LastRead 0 FirstWrite -1}
		targetIpAddress_V {Type I LastRead 0 FirstWrite -1}
		cycleCounter_V {Type IO LastRead -1 FirstWrite -1}
		microsecondCounter_V {Type IO LastRead -1 FirstWrite -1}
		secondCounter_V {Type IO LastRead -1 FirstWrite -1}
		iperfFsmState {Type IO LastRead -1 FirstWrite -1}
		timeOver {Type IO LastRead -1 FirstWrite -1}
		lastPkg {Type IO LastRead -1 FirstWrite -1}
		receivedResponse {Type IO LastRead -1 FirstWrite -1}
		seqNumber_V {Type IO LastRead -1 FirstWrite -1}
		header_idx {Type IO LastRead -1 FirstWrite -1}
		wordCount_V {Type IO LastRead -1 FirstWrite -1}
		startSignalFifo_V {Type IO LastRead -1 FirstWrite -1}
		header_header_V {Type IO LastRead -1 FirstWrite -1}
		metaWritten {Type IO LastRead -1 FirstWrite -1}
		waitCounter_V {Type IO LastRead -1 FirstWrite -1}
		packetGapCounter_V {Type IO LastRead -1 FirstWrite -1}
		stopSignalFifo_V {Type IO LastRead -1 FirstWrite -1}
		doneSignalFifo_V {Type IO LastRead -1 FirstWrite -1}
		startClock {Type IO LastRead -1 FirstWrite -1}
		time_V {Type IO LastRead -1 FirstWrite -1}}
	client_64_s {
		txMetaData_V {Type O LastRead -1 FirstWrite 1}
		txData_V_data_V {Type O LastRead -1 FirstWrite 1}
		txData_V_keep_V {Type O LastRead -1 FirstWrite 1}
		txData_V_last_V {Type O LastRead -1 FirstWrite 1}
		runExperiment_V {Type I LastRead 0 FirstWrite -1}
		pkgWordCount_V {Type I LastRead 0 FirstWrite -1}
		packetGap_V {Type I LastRead 0 FirstWrite -1}
		targetIpAddress_V {Type I LastRead 0 FirstWrite -1}
		cycleCounter_V {Type IO LastRead -1 FirstWrite -1}
		microsecondCounter_V {Type IO LastRead -1 FirstWrite -1}
		secondCounter_V {Type IO LastRead -1 FirstWrite -1}
		iperfFsmState {Type IO LastRead -1 FirstWrite -1}
		timeOver {Type IO LastRead -1 FirstWrite -1}
		lastPkg {Type IO LastRead -1 FirstWrite -1}
		receivedResponse {Type IO LastRead -1 FirstWrite -1}
		seqNumber_V {Type IO LastRead -1 FirstWrite -1}
		header_idx {Type IO LastRead -1 FirstWrite -1}
		wordCount_V {Type IO LastRead -1 FirstWrite -1}
		startSignalFifo_V {Type O LastRead -1 FirstWrite 1}
		header_header_V {Type IO LastRead -1 FirstWrite -1}
		metaWritten {Type IO LastRead -1 FirstWrite -1}
		waitCounter_V {Type IO LastRead -1 FirstWrite -1}
		packetGapCounter_V {Type IO LastRead -1 FirstWrite -1}
		stopSignalFifo_V {Type I LastRead 1 FirstWrite -1}
		doneSignalFifo_V {Type I LastRead 0 FirstWrite -1}}
	check_for_response {
		rxMetaData_V {Type I LastRead 0 FirstWrite -1}
		rxData_V_data_V {Type I LastRead 0 FirstWrite -1}
		rxData_V_keep_V {Type I LastRead 0 FirstWrite -1}
		rxData_V_last_V {Type I LastRead 0 FirstWrite -1}
		doneSignalFifo_V {Type O LastRead -1 FirstWrite 0}}
	clock {
		startClock {Type IO LastRead -1 FirstWrite -1}
		time_V {Type IO LastRead -1 FirstWrite -1}
		stopSignalFifo_V {Type O LastRead -1 FirstWrite 1}
		startSignalFifo_V {Type I LastRead 0 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "3", "Max" : "3"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
]}

set Spec2ImplPortList { 
	s_axis_rx_metadata_V { axis {  { s_axis_rx_metadata_V_TDATA in_data 0 176 }  { s_axis_rx_metadata_V_TVALID in_vld 0 1 }  { s_axis_rx_metadata_V_TREADY in_acc 1 1 } } }
	s_axis_rx_data_V_data_V { axis {  { s_axis_rx_data_TDATA in_data 0 64 } } }
	s_axis_rx_data_V_keep_V { axis {  { s_axis_rx_data_TKEEP in_data 0 8 } } }
	s_axis_rx_data_V_last_V { axis {  { s_axis_rx_data_TLAST in_data 0 1 }  { s_axis_rx_data_TVALID in_vld 0 1 }  { s_axis_rx_data_TREADY in_acc 1 1 } } }
	m_axis_tx_metadata_V { axis {  { m_axis_tx_metadata_V_TDATA out_data 1 176 }  { m_axis_tx_metadata_V_TVALID out_vld 1 1 }  { m_axis_tx_metadata_V_TREADY out_acc 0 1 } } }
	m_axis_tx_data_V_data_V { axis {  { m_axis_tx_data_TDATA out_data 1 64 } } }
	m_axis_tx_data_V_keep_V { axis {  { m_axis_tx_data_TKEEP out_data 1 8 } } }
	m_axis_tx_data_V_last_V { axis {  { m_axis_tx_data_TLAST out_data 1 1 }  { m_axis_tx_data_TVALID out_vld 1 1 }  { m_axis_tx_data_TREADY out_acc 0 1 } } }
	runExperiment_V { ap_stable {  { runExperiment_V in_data 0 1 } } }
	pkgWordCount_V { ap_stable {  { pkgWordCount_V in_data 0 8 } } }
	packetGap_V { ap_stable {  { packetGap_V in_data 0 8 } } }
	targetIpAddress_V { ap_stable {  { targetIpAddress_V in_data 0 32 } } }
}

set busDeadlockParameterList { 
}

# RTL port scheduling information:
set fifoSchedulingInfoList { 
}

# RTL bus port read request latency information:
set busReadReqLatencyList { 
}

# RTL bus port write response latency information:
set busWriteResLatencyList { 
}

# RTL array port load latency information:
set memoryLoadLatencyList { 
}
