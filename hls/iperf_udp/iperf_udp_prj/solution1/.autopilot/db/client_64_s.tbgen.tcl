set moduleName client_64_s
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {client<64>}
set C_modelType { void 0 }
set C_modelArgList {
	{ txMetaData_V int 176 regular {axi_s 1 volatile  { txMetaData_V Data } }  }
	{ txData_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_tx_data Data } }  }
	{ txData_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_tx_data Keep } }  }
	{ txData_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_tx_data Last } }  }
	{ runExperiment_V int 1 regular {ap_stable 0} }
	{ pkgWordCount_V int 8 regular {ap_stable 0} }
	{ packetGap_V int 8 regular {ap_stable 0} }
	{ targetIpAddress_V int 32 regular {ap_stable 0} }
	{ startSignalFifo_V int 1 regular {fifo 1 volatile } {global 1}  }
	{ stopSignalFifo_V int 1 regular {fifo 0 volatile } {global 0}  }
	{ doneSignalFifo_V int 1 regular {fifo 0 volatile } {global 0}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "txMetaData_V", "interface" : "axis", "bitwidth" : 176, "direction" : "WRITEONLY"} , 
 	{ "Name" : "txData_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY"} , 
 	{ "Name" : "txData_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY"} , 
 	{ "Name" : "txData_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY"} , 
 	{ "Name" : "runExperiment_V", "interface" : "wire", "bitwidth" : 1, "direction" : "READONLY"} , 
 	{ "Name" : "pkgWordCount_V", "interface" : "wire", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "packetGap_V", "interface" : "wire", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "targetIpAddress_V", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "startSignalFifo_V", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "stopSignalFifo_V", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "doneSignalFifo_V", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 28
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ doneSignalFifo_V_dout sc_in sc_lv 1 signal 10 } 
	{ doneSignalFifo_V_empty_n sc_in sc_logic 1 signal 10 } 
	{ doneSignalFifo_V_read sc_out sc_logic 1 signal 10 } 
	{ startSignalFifo_V_din sc_out sc_lv 1 signal 8 } 
	{ startSignalFifo_V_full_n sc_in sc_logic 1 signal 8 } 
	{ startSignalFifo_V_write sc_out sc_logic 1 signal 8 } 
	{ stopSignalFifo_V_dout sc_in sc_lv 1 signal 9 } 
	{ stopSignalFifo_V_empty_n sc_in sc_logic 1 signal 9 } 
	{ stopSignalFifo_V_read sc_out sc_logic 1 signal 9 } 
	{ m_axis_tx_data_TREADY sc_in sc_logic 1 outacc 3 } 
	{ txMetaData_V_TREADY sc_in sc_logic 1 outacc 0 } 
	{ txMetaData_V_TDATA sc_out sc_lv 176 signal 0 } 
	{ txMetaData_V_TVALID sc_out sc_logic 1 outvld 0 } 
	{ m_axis_tx_data_TDATA sc_out sc_lv 64 signal 1 } 
	{ m_axis_tx_data_TVALID sc_out sc_logic 1 outvld 3 } 
	{ m_axis_tx_data_TKEEP sc_out sc_lv 8 signal 2 } 
	{ m_axis_tx_data_TLAST sc_out sc_lv 1 signal 3 } 
	{ runExperiment_V sc_in sc_lv 1 signal 4 } 
	{ pkgWordCount_V sc_in sc_lv 8 signal 5 } 
	{ packetGap_V sc_in sc_lv 8 signal 6 } 
	{ targetIpAddress_V sc_in sc_lv 32 signal 7 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "doneSignalFifo_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "doneSignalFifo_V", "role": "dout" }} , 
 	{ "name": "doneSignalFifo_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "doneSignalFifo_V", "role": "empty_n" }} , 
 	{ "name": "doneSignalFifo_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "doneSignalFifo_V", "role": "read" }} , 
 	{ "name": "startSignalFifo_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "startSignalFifo_V", "role": "din" }} , 
 	{ "name": "startSignalFifo_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "startSignalFifo_V", "role": "full_n" }} , 
 	{ "name": "startSignalFifo_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "startSignalFifo_V", "role": "write" }} , 
 	{ "name": "stopSignalFifo_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "stopSignalFifo_V", "role": "dout" }} , 
 	{ "name": "stopSignalFifo_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "stopSignalFifo_V", "role": "empty_n" }} , 
 	{ "name": "stopSignalFifo_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "stopSignalFifo_V", "role": "read" }} , 
 	{ "name": "m_axis_tx_data_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "txData_V_last_V", "role": "READY" }} , 
 	{ "name": "txMetaData_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "txMetaData_V", "role": "TREADY" }} , 
 	{ "name": "txMetaData_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":176, "type": "signal", "bundle":{"name": "txMetaData_V", "role": "TDATA" }} , 
 	{ "name": "txMetaData_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "txMetaData_V", "role": "TVALID" }} , 
 	{ "name": "m_axis_tx_data_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "txData_V_data_V", "role": "DATA" }} , 
 	{ "name": "m_axis_tx_data_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "txData_V_last_V", "role": "VALID" }} , 
 	{ "name": "m_axis_tx_data_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "txData_V_keep_V", "role": "KEEP" }} , 
 	{ "name": "m_axis_tx_data_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "txData_V_last_V", "role": "LAST" }} , 
 	{ "name": "runExperiment_V", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "runExperiment_V", "role": "default" }} , 
 	{ "name": "pkgWordCount_V", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "pkgWordCount_V", "role": "default" }} , 
 	{ "name": "packetGap_V", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "packetGap_V", "role": "default" }} , 
 	{ "name": "targetIpAddress_V", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "targetIpAddress_V", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2", "3", "4"],
		"CDFG" : "client_64_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "txMetaData_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "txMetaData_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txData_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_tx_data_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txData_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "txData_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "runExperiment_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "pkgWordCount_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "packetGap_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "targetIpAddress_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "cycleCounter_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "microsecondCounter_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "secondCounter_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "iperfFsmState", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "timeOver", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "lastPkg", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "receivedResponse", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "seqNumber_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_idx", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "wordCount_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "startSignalFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "startSignalFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "header_header_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "metaWritten", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "waitCounter_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "packetGapCounter_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "stopSignalFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "stopSignalFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "doneSignalFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "doneSignalFifo_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_txMetaData_V_U", "Parent" : "0"},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_txData_V_data_V_U", "Parent" : "0"},
	{"ID" : "3", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_txData_V_keep_V_U", "Parent" : "0"},
	{"ID" : "4", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_txData_V_last_V_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	client_64_s {
		txMetaData_V {Type O LastRead -1 FirstWrite 1}
		txData_V_data_V {Type O LastRead -1 FirstWrite 1}
		txData_V_keep_V {Type O LastRead -1 FirstWrite 1}
		txData_V_last_V {Type O LastRead -1 FirstWrite 1}
		runExperiment_V {Type I LastRead 0 FirstWrite -1}
		pkgWordCount_V {Type I LastRead 0 FirstWrite -1}
		packetGap_V {Type I LastRead 0 FirstWrite -1}
		targetIpAddress_V {Type I LastRead 0 FirstWrite -1}
		cycleCounter_V {Type IO LastRead -1 FirstWrite -1}
		microsecondCounter_V {Type IO LastRead -1 FirstWrite -1}
		secondCounter_V {Type IO LastRead -1 FirstWrite -1}
		iperfFsmState {Type IO LastRead -1 FirstWrite -1}
		timeOver {Type IO LastRead -1 FirstWrite -1}
		lastPkg {Type IO LastRead -1 FirstWrite -1}
		receivedResponse {Type IO LastRead -1 FirstWrite -1}
		seqNumber_V {Type IO LastRead -1 FirstWrite -1}
		header_idx {Type IO LastRead -1 FirstWrite -1}
		wordCount_V {Type IO LastRead -1 FirstWrite -1}
		startSignalFifo_V {Type O LastRead -1 FirstWrite 1}
		header_header_V {Type IO LastRead -1 FirstWrite -1}
		metaWritten {Type IO LastRead -1 FirstWrite -1}
		waitCounter_V {Type IO LastRead -1 FirstWrite -1}
		packetGapCounter_V {Type IO LastRead -1 FirstWrite -1}
		stopSignalFifo_V {Type I LastRead 1 FirstWrite -1}
		doneSignalFifo_V {Type I LastRead 0 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "2", "Max" : "2"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	txMetaData_V { axis {  { txMetaData_V_TREADY out_acc 0 1 }  { txMetaData_V_TDATA out_data 1 176 }  { txMetaData_V_TVALID out_vld 1 1 } } }
	txData_V_data_V { axis {  { m_axis_tx_data_TDATA out_data 1 64 } } }
	txData_V_keep_V { axis {  { m_axis_tx_data_TKEEP out_data 1 8 } } }
	txData_V_last_V { axis {  { m_axis_tx_data_TREADY out_acc 0 1 }  { m_axis_tx_data_TVALID out_vld 1 1 }  { m_axis_tx_data_TLAST out_data 1 1 } } }
	runExperiment_V { ap_stable {  { runExperiment_V in_data 0 1 } } }
	pkgWordCount_V { ap_stable {  { pkgWordCount_V in_data 0 8 } } }
	packetGap_V { ap_stable {  { packetGap_V in_data 0 8 } } }
	targetIpAddress_V { ap_stable {  { targetIpAddress_V in_data 0 32 } } }
	startSignalFifo_V { ap_fifo {  { startSignalFifo_V_din fifo_data 1 1 }  { startSignalFifo_V_full_n fifo_status 0 1 }  { startSignalFifo_V_write fifo_update 1 1 } } }
	stopSignalFifo_V { ap_fifo {  { stopSignalFifo_V_dout fifo_data 0 1 }  { stopSignalFifo_V_empty_n fifo_status 0 1 }  { stopSignalFifo_V_read fifo_update 1 1 } } }
	doneSignalFifo_V { ap_fifo {  { doneSignalFifo_V_dout fifo_data 0 1 }  { doneSignalFifo_V_empty_n fifo_status 0 1 }  { doneSignalFifo_V_read fifo_update 1 1 } } }
}
