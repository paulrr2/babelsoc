// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
// Version: 2019.2.1
// Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

#ifndef _iperf_udp_HH_
#define _iperf_udp_HH_

#include "systemc.h"
#include "AESL_pkg.h"

#include "client_64_s.h"
#include "check_for_response.h"
#include "clock.h"
#include "fifo_w1_d2_A.h"

namespace ap_rtl {

struct iperf_udp : public sc_module {
    // Port declarations 22
    sc_in< sc_lv<176> > s_axis_rx_metadata_V_TDATA;
    sc_in< sc_lv<64> > s_axis_rx_data_TDATA;
    sc_in< sc_lv<8> > s_axis_rx_data_TKEEP;
    sc_in< sc_lv<1> > s_axis_rx_data_TLAST;
    sc_out< sc_lv<176> > m_axis_tx_metadata_V_TDATA;
    sc_out< sc_lv<64> > m_axis_tx_data_TDATA;
    sc_out< sc_lv<8> > m_axis_tx_data_TKEEP;
    sc_out< sc_lv<1> > m_axis_tx_data_TLAST;
    sc_in< sc_lv<1> > runExperiment_V;
    sc_in< sc_lv<8> > pkgWordCount_V;
    sc_in< sc_lv<8> > packetGap_V;
    sc_in< sc_lv<32> > targetIpAddress_V;
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst_n;
    sc_out< sc_logic > m_axis_tx_metadata_V_TVALID;
    sc_in< sc_logic > m_axis_tx_metadata_V_TREADY;
    sc_out< sc_logic > m_axis_tx_data_TVALID;
    sc_in< sc_logic > m_axis_tx_data_TREADY;
    sc_in< sc_logic > s_axis_rx_metadata_V_TVALID;
    sc_out< sc_logic > s_axis_rx_metadata_V_TREADY;
    sc_in< sc_logic > s_axis_rx_data_TVALID;
    sc_out< sc_logic > s_axis_rx_data_TREADY;
    sc_signal< sc_logic > ap_var_for_const0;


    // Module declarations
    iperf_udp(sc_module_name name);
    SC_HAS_PROCESS(iperf_udp);

    ~iperf_udp();

    sc_trace_file* mVcdFile;

    ofstream mHdltvinHandle;
    ofstream mHdltvoutHandle;
    client_64_s* client_64_U0;
    check_for_response* check_for_response_U0;
    clock* clock_U0;
    fifo_w1_d2_A* startSignalFifo_V_U;
    fifo_w1_d2_A* stopSignalFifo_V_U;
    fifo_w1_d2_A* doneSignalFifo_V_U;
    sc_signal< sc_logic > ap_rst_n_inv;
    sc_signal< sc_logic > client_64_U0_ap_start;
    sc_signal< sc_logic > client_64_U0_ap_done;
    sc_signal< sc_logic > client_64_U0_ap_continue;
    sc_signal< sc_logic > client_64_U0_ap_idle;
    sc_signal< sc_logic > client_64_U0_ap_ready;
    sc_signal< sc_logic > client_64_U0_doneSignalFifo_V_read;
    sc_signal< sc_lv<1> > client_64_U0_startSignalFifo_V_din;
    sc_signal< sc_logic > client_64_U0_startSignalFifo_V_write;
    sc_signal< sc_logic > client_64_U0_stopSignalFifo_V_read;
    sc_signal< sc_lv<176> > client_64_U0_txMetaData_V_TDATA;
    sc_signal< sc_logic > client_64_U0_txMetaData_V_TVALID;
    sc_signal< sc_lv<64> > client_64_U0_m_axis_tx_data_TDATA;
    sc_signal< sc_logic > client_64_U0_m_axis_tx_data_TVALID;
    sc_signal< sc_lv<8> > client_64_U0_m_axis_tx_data_TKEEP;
    sc_signal< sc_lv<1> > client_64_U0_m_axis_tx_data_TLAST;
    sc_signal< sc_logic > ap_sync_continue;
    sc_signal< sc_logic > check_for_response_U0_ap_start;
    sc_signal< sc_logic > check_for_response_U0_ap_done;
    sc_signal< sc_logic > check_for_response_U0_ap_continue;
    sc_signal< sc_logic > check_for_response_U0_ap_idle;
    sc_signal< sc_logic > check_for_response_U0_ap_ready;
    sc_signal< sc_logic > check_for_response_U0_rxMetaData_V_TREADY;
    sc_signal< sc_logic > check_for_response_U0_s_axis_rx_data_TREADY;
    sc_signal< sc_lv<1> > check_for_response_U0_doneSignalFifo_V_din;
    sc_signal< sc_logic > check_for_response_U0_doneSignalFifo_V_write;
    sc_signal< sc_logic > clock_U0_ap_start;
    sc_signal< sc_logic > clock_U0_ap_done;
    sc_signal< sc_logic > clock_U0_ap_continue;
    sc_signal< sc_logic > clock_U0_ap_idle;
    sc_signal< sc_logic > clock_U0_ap_ready;
    sc_signal< sc_logic > clock_U0_startSignalFifo_V_read;
    sc_signal< sc_lv<1> > clock_U0_stopSignalFifo_V_din;
    sc_signal< sc_logic > clock_U0_stopSignalFifo_V_write;
    sc_signal< sc_logic > startSignalFifo_V_full_n;
    sc_signal< sc_lv<1> > startSignalFifo_V_dout;
    sc_signal< sc_logic > startSignalFifo_V_empty_n;
    sc_signal< sc_logic > stopSignalFifo_V_full_n;
    sc_signal< sc_lv<1> > stopSignalFifo_V_dout;
    sc_signal< sc_logic > stopSignalFifo_V_empty_n;
    sc_signal< sc_logic > doneSignalFifo_V_full_n;
    sc_signal< sc_lv<1> > doneSignalFifo_V_dout;
    sc_signal< sc_logic > doneSignalFifo_V_empty_n;
    static const sc_lv<176> ap_const_lv176_lc_3;
    static const sc_lv<64> ap_const_lv64_0;
    static const sc_lv<8> ap_const_lv8_0;
    static const sc_lv<1> ap_const_lv1_0;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    // Thread declarations
    void thread_ap_var_for_const0();
    void thread_ap_rst_n_inv();
    void thread_ap_sync_continue();
    void thread_check_for_response_U0_ap_continue();
    void thread_check_for_response_U0_ap_start();
    void thread_client_64_U0_ap_continue();
    void thread_client_64_U0_ap_start();
    void thread_clock_U0_ap_continue();
    void thread_clock_U0_ap_start();
    void thread_m_axis_tx_data_TDATA();
    void thread_m_axis_tx_data_TKEEP();
    void thread_m_axis_tx_data_TLAST();
    void thread_m_axis_tx_data_TVALID();
    void thread_m_axis_tx_metadata_V_TDATA();
    void thread_m_axis_tx_metadata_V_TVALID();
    void thread_s_axis_rx_data_TREADY();
    void thread_s_axis_rx_metadata_V_TREADY();
    void thread_hdltv_gen();
};

}

using namespace ap_rtl;

#endif
