// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
// Version: 2019.2.1
// Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

#ifndef _client_64_s_HH_
#define _client_64_s_HH_

#include "systemc.h"
#include "AESL_pkg.h"


namespace ap_rtl {

struct client_64_s : public sc_module {
    // Port declarations 28
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_in< sc_logic > ap_start;
    sc_out< sc_logic > ap_done;
    sc_in< sc_logic > ap_continue;
    sc_out< sc_logic > ap_idle;
    sc_out< sc_logic > ap_ready;
    sc_in< sc_lv<1> > doneSignalFifo_V_dout;
    sc_in< sc_logic > doneSignalFifo_V_empty_n;
    sc_out< sc_logic > doneSignalFifo_V_read;
    sc_out< sc_lv<1> > startSignalFifo_V_din;
    sc_in< sc_logic > startSignalFifo_V_full_n;
    sc_out< sc_logic > startSignalFifo_V_write;
    sc_in< sc_lv<1> > stopSignalFifo_V_dout;
    sc_in< sc_logic > stopSignalFifo_V_empty_n;
    sc_out< sc_logic > stopSignalFifo_V_read;
    sc_in< sc_logic > m_axis_tx_data_TREADY;
    sc_in< sc_logic > txMetaData_V_TREADY;
    sc_out< sc_lv<176> > txMetaData_V_TDATA;
    sc_out< sc_logic > txMetaData_V_TVALID;
    sc_out< sc_lv<64> > m_axis_tx_data_TDATA;
    sc_out< sc_logic > m_axis_tx_data_TVALID;
    sc_out< sc_lv<8> > m_axis_tx_data_TKEEP;
    sc_out< sc_lv<1> > m_axis_tx_data_TLAST;
    sc_in< sc_lv<1> > runExperiment_V;
    sc_in< sc_lv<8> > pkgWordCount_V;
    sc_in< sc_lv<8> > packetGap_V;
    sc_in< sc_lv<32> > targetIpAddress_V;
    sc_signal< sc_lv<8> > ap_var_for_const0;


    // Module declarations
    client_64_s(sc_module_name name);
    SC_HAS_PROCESS(client_64_s);

    ~client_64_s();

    sc_trace_file* mVcdFile;

    regslice_both<176>* regslice_both_txMetaData_V_U;
    regslice_both<64>* regslice_both_txData_V_data_V_U;
    regslice_both<8>* regslice_both_txData_V_keep_V_U;
    regslice_both<1>* regslice_both_txData_V_last_V_U;
    sc_signal< sc_logic > ap_done_reg;
    sc_signal< sc_lv<1> > ap_CS_fsm;
    sc_signal< sc_logic > ap_CS_fsm_pp0_stage0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter1;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter2;
    sc_signal< sc_logic > ap_idle_pp0;
    sc_signal< sc_lv<1> > tmp_5_nbreadreq_fu_232_p3;
    sc_signal< bool > ap_block_state1_pp0_stage0_iter0;
    sc_signal< sc_lv<3> > iperfFsmState_load_reg_1559;
    sc_signal< sc_lv<1> > tmp_3_nbreadreq_fu_277_p3;
    sc_signal< bool > ap_block_state2_pp0_stage0_iter1;
    sc_signal< sc_lv<1> > icmp_ln887_reg_1587;
    sc_signal< bool > ap_predicate_op131_write_state2;
    sc_signal< sc_lv<1> > metaWritten_load_reg_1596;
    sc_signal< bool > ap_predicate_op135_write_state2;
    sc_signal< bool > ap_block_state2_io;
    sc_signal< sc_logic > regslice_both_txMetaData_V_U_apdone_blk;
    sc_signal< sc_logic > regslice_both_txData_V_data_V_U_apdone_blk;
    sc_signal< bool > ap_block_state3_pp0_stage0_iter2;
    sc_signal< sc_lv<3> > iperfFsmState_load_reg_1559_pp0_iter1_reg;
    sc_signal< sc_lv<1> > icmp_ln887_reg_1587_pp0_iter1_reg;
    sc_signal< bool > ap_predicate_op208_write_state3;
    sc_signal< sc_lv<1> > metaWritten_load_reg_1596_pp0_iter1_reg;
    sc_signal< bool > ap_predicate_op210_write_state3;
    sc_signal< bool > ap_block_state3_io;
    sc_signal< bool > ap_block_pp0_stage0_11001;
    sc_signal< sc_lv<32> > cycleCounter_V;
    sc_signal< sc_lv<32> > microsecondCounter_V;
    sc_signal< sc_lv<32> > secondCounter_V;
    sc_signal< sc_lv<3> > iperfFsmState;
    sc_signal< sc_lv<1> > timeOver;
    sc_signal< sc_lv<1> > lastPkg;
    sc_signal< sc_lv<1> > receivedResponse;
    sc_signal< sc_lv<32> > seqNumber_V;
    sc_signal< sc_lv<16> > header_idx;
    sc_signal< sc_lv<8> > wordCount_V;
    sc_signal< sc_lv<192> > header_header_V;
    sc_signal< sc_lv<1> > metaWritten;
    sc_signal< sc_lv<32> > waitCounter_V;
    sc_signal< sc_lv<8> > packetGapCounter_V;
    sc_signal< sc_logic > txMetaData_V_TDATA_blk_n;
    sc_signal< bool > ap_block_pp0_stage0;
    sc_signal< sc_logic > m_axis_tx_data_TDATA_blk_n;
    sc_signal< sc_logic > startSignalFifo_V_blk_n;
    sc_signal< sc_logic > stopSignalFifo_V_blk_n;
    sc_signal< sc_logic > doneSignalFifo_V_blk_n;
    sc_signal< sc_lv<8> > grp_fu_632_p2;
    sc_signal< sc_lv<3> > iperfFsmState_load_load_fu_712_p1;
    sc_signal< sc_lv<1> > icmp_ln887_fu_779_p2;
    sc_signal< sc_lv<1> > runExperiment_V_read_read_fu_226_p2;
    sc_signal< sc_lv<1> > icmp_ln879_fu_664_p2;
    sc_signal< sc_lv<32> > t_V_9_fu_680_p3;
    sc_signal< sc_lv<1> > icmp_ln879_1_fu_688_p2;
    sc_signal< sc_lv<32> > t_V_10_fu_704_p3;
    sc_signal< sc_lv<1> > timeOver_load_load_fu_716_p1;
    sc_signal< sc_lv<1> > lastPkg_load_load_fu_720_p1;
    sc_signal< sc_lv<8> > add_ln700_5_fu_745_p2;
    sc_signal< sc_lv<32> > add_ln700_4_fu_761_p2;
    sc_signal< sc_lv<1> > tmp_last_V_fu_785_p2;
    sc_signal< sc_lv<1> > tmp_last_V_reg_1591;
    sc_signal< sc_lv<1> > metaWritten_load_load_fu_827_p1;
    sc_signal< sc_lv<2> > trunc_ln76_fu_831_p1;
    sc_signal< sc_lv<2> > trunc_ln76_reg_1600;
    sc_signal< sc_lv<2> > trunc_ln76_1_fu_835_p1;
    sc_signal< sc_lv<2> > trunc_ln76_1_reg_1605;
    sc_signal< sc_lv<1> > icmp_ln647_fu_869_p2;
    sc_signal< sc_lv<1> > icmp_ln647_reg_1610;
    sc_signal< sc_lv<8> > trunc_ln647_3_fu_875_p1;
    sc_signal< sc_lv<8> > trunc_ln647_3_reg_1617;
    sc_signal< sc_lv<1> > icmp_ln647_1_fu_973_p2;
    sc_signal< sc_lv<1> > icmp_ln647_1_reg_1623;
    sc_signal< sc_lv<1> > or_ln76_fu_985_p2;
    sc_signal< sc_lv<1> > or_ln76_reg_1630;
    sc_signal< sc_lv<1> > and_ln82_fu_991_p2;
    sc_signal< sc_lv<1> > and_ln82_reg_1634;
    sc_signal< sc_lv<1> > tmp_5_reg_1639;
    sc_signal< sc_lv<64> > currWord_data_V_fu_1449_p3;
    sc_signal< bool > ap_block_pp0_stage0_subdone;
    sc_signal< sc_lv<3> > ap_phi_mux_storemerge360_i_phi_fu_294_p4;
    sc_signal< sc_lv<3> > select_ln160_fu_812_p3;
    sc_signal< sc_lv<3> > ap_phi_reg_pp0_iter0_storemerge360_i_reg_291;
    sc_signal< sc_lv<3> > select_ln175_fu_803_p3;
    sc_signal< sc_lv<8> > ap_phi_reg_pp0_iter0_packetGapCounter_V_n_reg_300;
    sc_signal< sc_lv<8> > ap_phi_reg_pp0_iter1_packetGapCounter_V_n_reg_300;
    sc_signal< sc_lv<1> > icmp_ln879_3_fu_751_p2;
    sc_signal< sc_lv<32> > ap_phi_reg_pp0_iter0_waitCounter_V_new_0_s_reg_311;
    sc_signal< sc_lv<32> > ap_phi_reg_pp0_iter1_waitCounter_V_new_0_s_reg_311;
    sc_signal< sc_lv<1> > icmp_ln879_2_fu_767_p2;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter0_metaWritten_flag_0_i_reg_322;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter1_metaWritten_flag_0_i_reg_322;
    sc_signal< sc_lv<1> > ap_phi_mux_metaWritten_flag_1_i_phi_fu_339_p4;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter0_metaWritten_flag_1_i_reg_335;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter1_metaWritten_flag_1_i_reg_335;
    sc_signal< sc_lv<1> > ap_phi_mux_metaWritten_new_1_i_phi_fu_352_p4;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter0_metaWritten_new_1_i_reg_348;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter1_metaWritten_new_1_i_reg_348;
    sc_signal< sc_lv<1> > ap_phi_mux_microsecondCounter_V_1_phi_fu_364_p20;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter0_microsecondCounter_V_1_reg_360;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter1_microsecondCounter_V_1_reg_360;
    sc_signal< sc_lv<32> > ap_phi_reg_pp0_iter0_microsecondCounter_V_2_reg_388;
    sc_signal< sc_lv<32> > ap_phi_reg_pp0_iter1_microsecondCounter_V_2_reg_388;
    sc_signal< sc_lv<1> > ap_phi_mux_secondCounter_V_flag_phi_fu_419_p20;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter0_secondCounter_V_flag_reg_415;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter1_secondCounter_V_flag_reg_415;
    sc_signal< sc_lv<32> > ap_phi_reg_pp0_iter0_secondCounter_V_new_s_reg_443;
    sc_signal< sc_lv<32> > ap_phi_reg_pp0_iter1_secondCounter_V_new_s_reg_443;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter0_timeOver_flag_0_i_reg_470;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter1_timeOver_flag_0_i_reg_470;
    sc_signal< sc_lv<1> > ap_phi_mux_wordCount_V_flag_2_i_phi_fu_512_p20;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter0_wordCount_V_flag_2_i_reg_507;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter1_wordCount_V_flag_2_i_reg_507;
    sc_signal< sc_lv<8> > ap_phi_reg_pp0_iter0_wordCount_V_new_2_i_reg_545;
    sc_signal< sc_lv<8> > ap_phi_reg_pp0_iter1_wordCount_V_new_2_i_reg_545;
    sc_signal< sc_lv<1> > ap_phi_mux_timeOver_flag_1_i_phi_fu_583_p4;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter1_timeOver_flag_1_i_reg_580;
    sc_signal< sc_lv<1> > ap_phi_mux_timeOver_new_1_i_phi_fu_595_p4;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter1_timeOver_new_1_i_reg_592;
    sc_signal< sc_lv<1> > ap_phi_mux_receivedResponse_fla_phi_fu_607_p4;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter0_receivedResponse_fla_reg_603;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter1_receivedResponse_fla_reg_603;
    sc_signal< sc_lv<1> > ap_phi_mux_receivedResponse_new_phi_fu_619_p4;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter0_receivedResponse_new_reg_616;
    sc_signal< sc_lv<1> > ap_phi_reg_pp0_iter1_receivedResponse_new_reg_616;
    sc_signal< sc_lv<32> > add_ln700_fu_652_p2;
    sc_signal< sc_lv<32> > ap_sig_allocacmp_t_V_1;
    sc_signal< sc_lv<32> > ap_sig_allocacmp_t_V_3;
    sc_signal< sc_lv<1> > ap_sig_allocacmp_timeOver_load;
    sc_signal< sc_lv<1> > ap_sig_allocacmp_receivedResponse_loa;
    sc_signal< sc_lv<32> > add_ln700_3_fu_1209_p2;
    sc_signal< sc_lv<16> > select_ln82_fu_1005_p3;
    sc_signal< sc_lv<8> > ap_sig_allocacmp_t_V_8;
    sc_signal< sc_lv<192> > p_Result_s_fu_1191_p5;
    sc_signal< sc_lv<1> > ap_sig_allocacmp_metaWritten_load;
    sc_signal< sc_lv<32> > ap_sig_allocacmp_t_V_6;
    sc_signal< sc_lv<8> > ap_sig_allocacmp_t_V_7;
    sc_signal< bool > ap_block_pp0_stage0_01001;
    sc_signal< sc_lv<8> > tmp_10_fu_1364_p3;
    sc_signal< sc_lv<32> > add_ln700_1_fu_674_p2;
    sc_signal< sc_lv<32> > add_ln700_2_fu_698_p2;
    sc_signal< sc_lv<1> > icmp_ln883_fu_797_p2;
    sc_signal< sc_lv<22> > Lo_assign_fu_839_p3;
    sc_signal< sc_lv<23> > zext_ln76_fu_847_p1;
    sc_signal< sc_lv<23> > add_ln76_fu_851_p2;
    sc_signal< sc_lv<22> > or_ln78_fu_863_p2;
    sc_signal< sc_lv<16> > add_ln79_fu_879_p2;
    sc_signal< sc_lv<22> > shl_ln_fu_885_p3;
    sc_signal< sc_lv<23> > zext_ln80_fu_893_p1;
    sc_signal< sc_lv<23> > sub_ln80_fu_897_p2;
    sc_signal< sc_lv<23> > sub_ln80_1_fu_911_p2;
    sc_signal< sc_lv<8> > trunc_ln80_1_fu_917_p4;
    sc_signal< sc_lv<8> > sub_ln80_2_fu_927_p2;
    sc_signal< sc_lv<1> > tmp_4_fu_903_p3;
    sc_signal< sc_lv<5> > tmp_6_fu_933_p4;
    sc_signal< sc_lv<5> > tmp_9_fu_943_p4;
    sc_signal< sc_lv<5> > select_ln80_fu_953_p3;
    sc_signal< sc_lv<1> > icmp_ln76_fu_857_p2;
    sc_signal< sc_lv<1> > icmp_ln80_fu_961_p2;
    sc_signal< sc_lv<1> > xor_ln76_fu_979_p2;
    sc_signal< sc_lv<1> > icmp_ln82_fu_967_p2;
    sc_signal< sc_lv<16> > select_ln76_fu_997_p3;
    sc_signal< sc_lv<8> > trunc_ln647_fu_1061_p1;
    sc_signal< sc_lv<8> > p_Result_2_2_i_i_i_fu_1051_p4;
    sc_signal< sc_lv<8> > p_Result_2_1_i_i_i_fu_1041_p4;
    sc_signal< sc_lv<8> > p_Result_2_i_i_i_fu_1031_p4;
    sc_signal< sc_lv<32> > agg_result_V_0_3_i_i_fu_1065_p5;
    sc_signal< sc_lv<192> > p_Result_5_fu_1081_p5;
    sc_signal< sc_lv<8> > trunc_ln647_2_fu_1167_p1;
    sc_signal< sc_lv<8> > p_Result_2_2_i_i1_fu_1157_p4;
    sc_signal< sc_lv<8> > p_Result_2_1_i_i1_fu_1147_p4;
    sc_signal< sc_lv<8> > p_Result_2_i_i25_i_fu_1137_p4;
    sc_signal< sc_lv<8> > trunc_ln647_1_fu_1133_p1;
    sc_signal< sc_lv<8> > p_Result_2_2_i_i_fu_1123_p4;
    sc_signal< sc_lv<8> > p_Result_2_1_i_i_fu_1113_p4;
    sc_signal< sc_lv<8> > p_Result_2_i_i18_i_fu_1103_p4;
    sc_signal< sc_lv<192> > p_Result_6_fu_1093_p4;
    sc_signal< sc_lv<64> > tmp_7_fu_1171_p9;
    sc_signal< sc_lv<128> > tmp_their_address_V_fu_1257_p1;
    sc_signal< sc_lv<171> > tmp_8_fu_1260_p4;
    sc_signal< sc_lv<8> > tmp_fu_1278_p3;
    sc_signal< sc_lv<8> > sub_ln647_fu_1295_p2;
    sc_signal< sc_lv<8> > sub_ln647_2_fu_1306_p2;
    sc_signal< sc_lv<192> > tmp_2_fu_1285_p4;
    sc_signal< sc_lv<8> > sub_ln647_1_fu_1300_p2;
    sc_signal< sc_lv<8> > select_ln647_fu_1311_p3;
    sc_signal< sc_lv<8> > select_ln647_2_fu_1325_p3;
    sc_signal< sc_lv<8> > sub_ln647_3_fu_1332_p2;
    sc_signal< sc_lv<192> > select_ln647_1_fu_1318_p3;
    sc_signal< sc_lv<192> > zext_ln647_fu_1338_p1;
    sc_signal< sc_lv<192> > zext_ln647_1_fu_1342_p1;
    sc_signal< sc_lv<192> > lshr_ln647_fu_1346_p2;
    sc_signal< sc_lv<192> > lshr_ln647_1_fu_1352_p2;
    sc_signal< sc_lv<8> > add_ln647_fu_1382_p2;
    sc_signal< sc_lv<8> > grp_fu_637_p2;
    sc_signal< sc_lv<192> > tmp_11_fu_1372_p4;
    sc_signal< sc_lv<8> > select_ln647_3_fu_1388_p3;
    sc_signal< sc_lv<8> > select_ln647_5_fu_1402_p3;
    sc_signal< sc_lv<8> > sub_ln647_6_fu_1409_p2;
    sc_signal< sc_lv<192> > select_ln647_4_fu_1395_p3;
    sc_signal< sc_lv<192> > zext_ln647_2_fu_1415_p1;
    sc_signal< sc_lv<192> > zext_ln647_3_fu_1419_p1;
    sc_signal< sc_lv<192> > lshr_ln647_2_fu_1423_p2;
    sc_signal< sc_lv<192> > lshr_ln647_3_fu_1429_p2;
    sc_signal< sc_lv<192> > p_Result_8_fu_1435_p2;
    sc_signal< sc_lv<192> > p_Result_7_fu_1358_p2;
    sc_signal< sc_lv<64> > trunc_ln82_fu_1441_p1;
    sc_signal< sc_lv<64> > trunc_ln82_1_fu_1445_p1;
    sc_signal< sc_lv<1> > ap_NS_fsm;
    sc_signal< sc_logic > ap_idle_pp0_0to1;
    sc_signal< sc_logic > ap_reset_idle_pp0;
    sc_signal< sc_logic > ap_enable_pp0;
    sc_signal< sc_lv<176> > txMetaData_V_TDATA_int;
    sc_signal< sc_logic > txMetaData_V_TVALID_int;
    sc_signal< sc_logic > txMetaData_V_TREADY_int;
    sc_signal< sc_logic > regslice_both_txMetaData_V_U_vld_out;
    sc_signal< sc_lv<64> > m_axis_tx_data_TDATA_int;
    sc_signal< sc_logic > m_axis_tx_data_TVALID_int;
    sc_signal< sc_logic > m_axis_tx_data_TREADY_int;
    sc_signal< sc_logic > regslice_both_txData_V_data_V_U_vld_out;
    sc_signal< sc_logic > regslice_both_txData_V_keep_V_U_apdone_blk;
    sc_signal< sc_logic > regslice_both_txData_V_keep_V_U_ack_in_dummy;
    sc_signal< sc_logic > regslice_both_txData_V_keep_V_U_vld_out;
    sc_signal< sc_logic > regslice_both_txData_V_last_V_U_apdone_blk;
    sc_signal< sc_lv<1> > m_axis_tx_data_TLAST_int;
    sc_signal< sc_logic > regslice_both_txData_V_last_V_U_ack_in_dummy;
    sc_signal< sc_logic > regslice_both_txData_V_last_V_U_vld_out;
    sc_signal< bool > ap_condition_313;
    sc_signal< bool > ap_condition_134;
    sc_signal< bool > ap_condition_464;
    sc_signal< bool > ap_condition_518;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<1> ap_ST_fsm_pp0_stage0;
    static const sc_lv<32> ap_const_lv32_0;
    static const bool ap_const_boolean_1;
    static const sc_lv<1> ap_const_lv1_1;
    static const sc_lv<3> ap_const_lv3_1;
    static const sc_lv<3> ap_const_lv3_4;
    static const sc_lv<3> ap_const_lv3_3;
    static const sc_lv<1> ap_const_lv1_0;
    static const bool ap_const_boolean_0;
    static const sc_lv<3> ap_const_lv3_0;
    static const sc_lv<16> ap_const_lv16_0;
    static const sc_lv<8> ap_const_lv8_0;
    static const sc_lv<3> ap_const_lv3_6;
    static const sc_lv<3> ap_const_lv3_5;
    static const sc_lv<3> ap_const_lv3_7;
    static const sc_lv<3> ap_const_lv3_2;
    static const sc_lv<64> ap_const_lv64_3736353433323130;
    static const sc_lv<8> ap_const_lv8_FF;
    static const sc_lv<8> ap_const_lv8_1;
    static const sc_lv<8> ap_const_lv8_BF;
    static const sc_lv<32> ap_const_lv32_1;
    static const sc_lv<32> ap_const_lv32_2625A;
    static const sc_lv<32> ap_const_lv32_9502F90;
    static const sc_lv<32> ap_const_lv32_EE6B28;
    static const sc_lv<6> ap_const_lv6_0;
    static const sc_lv<23> ap_const_lv23_40;
    static const sc_lv<23> ap_const_lv23_C1;
    static const sc_lv<22> ap_const_lv22_3F;
    static const sc_lv<16> ap_const_lv16_1;
    static const sc_lv<23> ap_const_lv23_C0;
    static const sc_lv<32> ap_const_lv32_16;
    static const sc_lv<23> ap_const_lv23_0;
    static const sc_lv<32> ap_const_lv32_3;
    static const sc_lv<32> ap_const_lv32_A;
    static const sc_lv<32> ap_const_lv32_7;
    static const sc_lv<32> ap_const_lv32_6;
    static const sc_lv<5> ap_const_lv5_0;
    static const sc_lv<16> ap_const_lv16_3;
    static const sc_lv<22> ap_const_lv22_BF;
    static const sc_lv<32> ap_const_lv32_18;
    static const sc_lv<32> ap_const_lv32_1F;
    static const sc_lv<32> ap_const_lv32_10;
    static const sc_lv<32> ap_const_lv32_17;
    static const sc_lv<32> ap_const_lv32_8;
    static const sc_lv<32> ap_const_lv32_F;
    static const sc_lv<32> ap_const_lv32_20;
    static const sc_lv<32> ap_const_lv32_5F;
    static const sc_lv<35> ap_const_lv35_80001389;
    static const sc_lv<32> ap_const_lv32_BF;
    static const sc_lv<192> ap_const_lv192_lc_2;
    static const sc_lv<8> ap_const_lv8_41;
    // Thread declarations
    void thread_ap_var_for_const0();
    void thread_ap_clk_no_reset_();
    void thread_Lo_assign_fu_839_p3();
    void thread_add_ln647_fu_1382_p2();
    void thread_add_ln700_1_fu_674_p2();
    void thread_add_ln700_2_fu_698_p2();
    void thread_add_ln700_3_fu_1209_p2();
    void thread_add_ln700_4_fu_761_p2();
    void thread_add_ln700_5_fu_745_p2();
    void thread_add_ln700_fu_652_p2();
    void thread_add_ln76_fu_851_p2();
    void thread_add_ln79_fu_879_p2();
    void thread_agg_result_V_0_3_i_i_fu_1065_p5();
    void thread_and_ln82_fu_991_p2();
    void thread_ap_CS_fsm_pp0_stage0();
    void thread_ap_block_pp0_stage0();
    void thread_ap_block_pp0_stage0_01001();
    void thread_ap_block_pp0_stage0_11001();
    void thread_ap_block_pp0_stage0_subdone();
    void thread_ap_block_state1_pp0_stage0_iter0();
    void thread_ap_block_state2_io();
    void thread_ap_block_state2_pp0_stage0_iter1();
    void thread_ap_block_state3_io();
    void thread_ap_block_state3_pp0_stage0_iter2();
    void thread_ap_condition_134();
    void thread_ap_condition_313();
    void thread_ap_condition_464();
    void thread_ap_condition_518();
    void thread_ap_done();
    void thread_ap_enable_pp0();
    void thread_ap_enable_reg_pp0_iter0();
    void thread_ap_idle();
    void thread_ap_idle_pp0();
    void thread_ap_idle_pp0_0to1();
    void thread_ap_phi_mux_metaWritten_flag_1_i_phi_fu_339_p4();
    void thread_ap_phi_mux_metaWritten_new_1_i_phi_fu_352_p4();
    void thread_ap_phi_mux_microsecondCounter_V_1_phi_fu_364_p20();
    void thread_ap_phi_mux_receivedResponse_fla_phi_fu_607_p4();
    void thread_ap_phi_mux_receivedResponse_new_phi_fu_619_p4();
    void thread_ap_phi_mux_secondCounter_V_flag_phi_fu_419_p20();
    void thread_ap_phi_mux_storemerge360_i_phi_fu_294_p4();
    void thread_ap_phi_mux_timeOver_flag_1_i_phi_fu_583_p4();
    void thread_ap_phi_mux_timeOver_new_1_i_phi_fu_595_p4();
    void thread_ap_phi_mux_wordCount_V_flag_2_i_phi_fu_512_p20();
    void thread_ap_phi_reg_pp0_iter0_metaWritten_flag_0_i_reg_322();
    void thread_ap_phi_reg_pp0_iter0_metaWritten_flag_1_i_reg_335();
    void thread_ap_phi_reg_pp0_iter0_metaWritten_new_1_i_reg_348();
    void thread_ap_phi_reg_pp0_iter0_microsecondCounter_V_1_reg_360();
    void thread_ap_phi_reg_pp0_iter0_microsecondCounter_V_2_reg_388();
    void thread_ap_phi_reg_pp0_iter0_packetGapCounter_V_n_reg_300();
    void thread_ap_phi_reg_pp0_iter0_receivedResponse_fla_reg_603();
    void thread_ap_phi_reg_pp0_iter0_receivedResponse_new_reg_616();
    void thread_ap_phi_reg_pp0_iter0_secondCounter_V_flag_reg_415();
    void thread_ap_phi_reg_pp0_iter0_secondCounter_V_new_s_reg_443();
    void thread_ap_phi_reg_pp0_iter0_storemerge360_i_reg_291();
    void thread_ap_phi_reg_pp0_iter0_timeOver_flag_0_i_reg_470();
    void thread_ap_phi_reg_pp0_iter0_waitCounter_V_new_0_s_reg_311();
    void thread_ap_phi_reg_pp0_iter0_wordCount_V_flag_2_i_reg_507();
    void thread_ap_phi_reg_pp0_iter0_wordCount_V_new_2_i_reg_545();
    void thread_ap_phi_reg_pp0_iter1_timeOver_flag_1_i_reg_580();
    void thread_ap_phi_reg_pp0_iter1_timeOver_new_1_i_reg_592();
    void thread_ap_predicate_op131_write_state2();
    void thread_ap_predicate_op135_write_state2();
    void thread_ap_predicate_op208_write_state3();
    void thread_ap_predicate_op210_write_state3();
    void thread_ap_ready();
    void thread_ap_reset_idle_pp0();
    void thread_ap_sig_allocacmp_metaWritten_load();
    void thread_ap_sig_allocacmp_receivedResponse_loa();
    void thread_ap_sig_allocacmp_t_V_1();
    void thread_ap_sig_allocacmp_t_V_3();
    void thread_ap_sig_allocacmp_t_V_6();
    void thread_ap_sig_allocacmp_t_V_7();
    void thread_ap_sig_allocacmp_t_V_8();
    void thread_ap_sig_allocacmp_timeOver_load();
    void thread_currWord_data_V_fu_1449_p3();
    void thread_doneSignalFifo_V_blk_n();
    void thread_doneSignalFifo_V_read();
    void thread_grp_fu_632_p2();
    void thread_grp_fu_637_p2();
    void thread_icmp_ln647_1_fu_973_p2();
    void thread_icmp_ln647_fu_869_p2();
    void thread_icmp_ln76_fu_857_p2();
    void thread_icmp_ln80_fu_961_p2();
    void thread_icmp_ln82_fu_967_p2();
    void thread_icmp_ln879_1_fu_688_p2();
    void thread_icmp_ln879_2_fu_767_p2();
    void thread_icmp_ln879_3_fu_751_p2();
    void thread_icmp_ln879_fu_664_p2();
    void thread_icmp_ln883_fu_797_p2();
    void thread_icmp_ln887_fu_779_p2();
    void thread_iperfFsmState_load_load_fu_712_p1();
    void thread_lastPkg_load_load_fu_720_p1();
    void thread_lshr_ln647_1_fu_1352_p2();
    void thread_lshr_ln647_2_fu_1423_p2();
    void thread_lshr_ln647_3_fu_1429_p2();
    void thread_lshr_ln647_fu_1346_p2();
    void thread_m_axis_tx_data_TDATA_blk_n();
    void thread_m_axis_tx_data_TDATA_int();
    void thread_m_axis_tx_data_TLAST_int();
    void thread_m_axis_tx_data_TVALID();
    void thread_m_axis_tx_data_TVALID_int();
    void thread_metaWritten_load_load_fu_827_p1();
    void thread_or_ln76_fu_985_p2();
    void thread_or_ln78_fu_863_p2();
    void thread_p_Result_2_1_i_i1_fu_1147_p4();
    void thread_p_Result_2_1_i_i_fu_1113_p4();
    void thread_p_Result_2_1_i_i_i_fu_1041_p4();
    void thread_p_Result_2_2_i_i1_fu_1157_p4();
    void thread_p_Result_2_2_i_i_fu_1123_p4();
    void thread_p_Result_2_2_i_i_i_fu_1051_p4();
    void thread_p_Result_2_i_i18_i_fu_1103_p4();
    void thread_p_Result_2_i_i25_i_fu_1137_p4();
    void thread_p_Result_2_i_i_i_fu_1031_p4();
    void thread_p_Result_5_fu_1081_p5();
    void thread_p_Result_6_fu_1093_p4();
    void thread_p_Result_7_fu_1358_p2();
    void thread_p_Result_8_fu_1435_p2();
    void thread_p_Result_s_fu_1191_p5();
    void thread_runExperiment_V_read_read_fu_226_p2();
    void thread_select_ln160_fu_812_p3();
    void thread_select_ln175_fu_803_p3();
    void thread_select_ln647_1_fu_1318_p3();
    void thread_select_ln647_2_fu_1325_p3();
    void thread_select_ln647_3_fu_1388_p3();
    void thread_select_ln647_4_fu_1395_p3();
    void thread_select_ln647_5_fu_1402_p3();
    void thread_select_ln647_fu_1311_p3();
    void thread_select_ln76_fu_997_p3();
    void thread_select_ln80_fu_953_p3();
    void thread_select_ln82_fu_1005_p3();
    void thread_shl_ln_fu_885_p3();
    void thread_startSignalFifo_V_blk_n();
    void thread_startSignalFifo_V_din();
    void thread_startSignalFifo_V_write();
    void thread_stopSignalFifo_V_blk_n();
    void thread_stopSignalFifo_V_read();
    void thread_sub_ln647_1_fu_1300_p2();
    void thread_sub_ln647_2_fu_1306_p2();
    void thread_sub_ln647_3_fu_1332_p2();
    void thread_sub_ln647_6_fu_1409_p2();
    void thread_sub_ln647_fu_1295_p2();
    void thread_sub_ln80_1_fu_911_p2();
    void thread_sub_ln80_2_fu_927_p2();
    void thread_sub_ln80_fu_897_p2();
    void thread_t_V_10_fu_704_p3();
    void thread_t_V_9_fu_680_p3();
    void thread_timeOver_load_load_fu_716_p1();
    void thread_tmp_10_fu_1364_p3();
    void thread_tmp_11_fu_1372_p4();
    void thread_tmp_2_fu_1285_p4();
    void thread_tmp_3_nbreadreq_fu_277_p3();
    void thread_tmp_4_fu_903_p3();
    void thread_tmp_5_nbreadreq_fu_232_p3();
    void thread_tmp_6_fu_933_p4();
    void thread_tmp_7_fu_1171_p9();
    void thread_tmp_8_fu_1260_p4();
    void thread_tmp_9_fu_943_p4();
    void thread_tmp_fu_1278_p3();
    void thread_tmp_last_V_fu_785_p2();
    void thread_tmp_their_address_V_fu_1257_p1();
    void thread_trunc_ln647_1_fu_1133_p1();
    void thread_trunc_ln647_2_fu_1167_p1();
    void thread_trunc_ln647_3_fu_875_p1();
    void thread_trunc_ln647_fu_1061_p1();
    void thread_trunc_ln76_1_fu_835_p1();
    void thread_trunc_ln76_fu_831_p1();
    void thread_trunc_ln80_1_fu_917_p4();
    void thread_trunc_ln82_1_fu_1445_p1();
    void thread_trunc_ln82_fu_1441_p1();
    void thread_txMetaData_V_TDATA_blk_n();
    void thread_txMetaData_V_TDATA_int();
    void thread_txMetaData_V_TVALID();
    void thread_txMetaData_V_TVALID_int();
    void thread_xor_ln76_fu_979_p2();
    void thread_zext_ln647_1_fu_1342_p1();
    void thread_zext_ln647_2_fu_1415_p1();
    void thread_zext_ln647_3_fu_1419_p1();
    void thread_zext_ln647_fu_1338_p1();
    void thread_zext_ln76_fu_847_p1();
    void thread_zext_ln80_fu_893_p1();
    void thread_ap_NS_fsm();
};

}

using namespace ap_rtl;

#endif
