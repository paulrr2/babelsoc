
open_project iperf_client_prj

open_solution "solution1"
set_part xc7vx690tffg1761-2
create_clock -period 6.4 -name default

set_top iperf_client

add_files /home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/iperf_client/iperf_client.cpp -cflags "-I/home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/build_pynq/hls/iperf_client"


add_files -tb /home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/hls/iperf_client/test_iperf_client.cpp


#Check which command
set command [lindex $argv 2]

if {$command == "synthesis"} {
   csynth_design
} elseif {$command == "csim"} {
   csim_design
} elseif {$command == "ip"} {
   export_design -format ip_catalog -ipname "iperf_client" -display_name "iperf client" -vendor "ethz.systems.fpga" -version "1.0"
} elseif {$command == "installip"} {
   file mkdir /home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/iprepo
   file delete -force /home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/iprepo/iperf_client
   file copy -force /home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/build_pynq/hls/iperf_client/iperf_client_prj/solution1/impl/ip /home/paulrr2/Projects/ece527/babelsoc/fpga-network-stack/iprepo/iperf_client/
} else {
   puts "No valid command specified. Use vivado_hls -f make.tcl <synthesis|csim|ip> ."
}


exit
