set moduleName server_64_s
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {server<64>}
set C_modelType { void 0 }
set C_modelArgList {
	{ listenPort_V_V int 16 regular {axi_s 1 volatile  { listenPort_V_V Data } }  }
	{ listenPortStatus_V int 8 regular {axi_s 0 volatile  { listenPortStatus_V Data } }  }
	{ notifications_V int 88 regular {axi_s 0 volatile  { notifications_V Data } }  }
	{ readRequest_V int 32 regular {axi_s 1 volatile  { readRequest_V Data } }  }
	{ rxMetaData_V_V int 16 regular {axi_s 0 volatile  { rxMetaData_V_V Data } }  }
	{ rxData_V_data_V int 64 regular {axi_s 0 volatile  { s_axis_rx_data Data } }  }
	{ rxData_V_keep_V int 8 regular {axi_s 0 volatile  { s_axis_rx_data Keep } }  }
	{ rxData_V_last_V int 1 regular {axi_s 0 volatile  { s_axis_rx_data Last } }  }
}
set C_modelArgMapList {[ 
	{ "Name" : "listenPort_V_V", "interface" : "axis", "bitwidth" : 16, "direction" : "WRITEONLY"} , 
 	{ "Name" : "listenPortStatus_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "notifications_V", "interface" : "axis", "bitwidth" : 88, "direction" : "READONLY"} , 
 	{ "Name" : "readRequest_V", "interface" : "axis", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "rxMetaData_V_V", "interface" : "axis", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "rxData_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "READONLY"} , 
 	{ "Name" : "rxData_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "rxData_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY"} ]}
# RTL Port declarations: 
set portNum 27
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ listenPortStatus_V_TVALID sc_in sc_logic 1 invld 1 } 
	{ notifications_V_TVALID sc_in sc_logic 1 invld 2 } 
	{ rxMetaData_V_V_TVALID sc_in sc_logic 1 invld 4 } 
	{ s_axis_rx_data_TVALID sc_in sc_logic 1 invld 5 } 
	{ listenPort_V_V_TREADY sc_in sc_logic 1 outacc 0 } 
	{ readRequest_V_TREADY sc_in sc_logic 1 outacc 3 } 
	{ listenPort_V_V_TDATA sc_out sc_lv 16 signal 0 } 
	{ listenPort_V_V_TVALID sc_out sc_logic 1 outvld 0 } 
	{ listenPortStatus_V_TDATA sc_in sc_lv 8 signal 1 } 
	{ listenPortStatus_V_TREADY sc_out sc_logic 1 inacc 1 } 
	{ notifications_V_TDATA sc_in sc_lv 88 signal 2 } 
	{ notifications_V_TREADY sc_out sc_logic 1 inacc 2 } 
	{ readRequest_V_TDATA sc_out sc_lv 32 signal 3 } 
	{ readRequest_V_TVALID sc_out sc_logic 1 outvld 3 } 
	{ rxMetaData_V_V_TDATA sc_in sc_lv 16 signal 4 } 
	{ rxMetaData_V_V_TREADY sc_out sc_logic 1 inacc 4 } 
	{ s_axis_rx_data_TDATA sc_in sc_lv 64 signal 5 } 
	{ s_axis_rx_data_TREADY sc_out sc_logic 1 inacc 7 } 
	{ s_axis_rx_data_TKEEP sc_in sc_lv 8 signal 6 } 
	{ s_axis_rx_data_TLAST sc_in sc_lv 1 signal 7 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "listenPortStatus_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "listenPortStatus_V", "role": "TVALID" }} , 
 	{ "name": "notifications_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "notifications_V", "role": "TVALID" }} , 
 	{ "name": "rxMetaData_V_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "rxMetaData_V_V", "role": "TVALID" }} , 
 	{ "name": "s_axis_rx_data_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "rxData_V_data_V", "role": "VALID" }} , 
 	{ "name": "listenPort_V_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "listenPort_V_V", "role": "TREADY" }} , 
 	{ "name": "readRequest_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "readRequest_V", "role": "TREADY" }} , 
 	{ "name": "listenPort_V_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "listenPort_V_V", "role": "TDATA" }} , 
 	{ "name": "listenPort_V_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "listenPort_V_V", "role": "TVALID" }} , 
 	{ "name": "listenPortStatus_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "listenPortStatus_V", "role": "TDATA" }} , 
 	{ "name": "listenPortStatus_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "listenPortStatus_V", "role": "TREADY" }} , 
 	{ "name": "notifications_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":88, "type": "signal", "bundle":{"name": "notifications_V", "role": "TDATA" }} , 
 	{ "name": "notifications_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "notifications_V", "role": "TREADY" }} , 
 	{ "name": "readRequest_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "readRequest_V", "role": "TDATA" }} , 
 	{ "name": "readRequest_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "readRequest_V", "role": "TVALID" }} , 
 	{ "name": "rxMetaData_V_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "rxMetaData_V_V", "role": "TDATA" }} , 
 	{ "name": "rxMetaData_V_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "rxMetaData_V_V", "role": "TREADY" }} , 
 	{ "name": "s_axis_rx_data_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "rxData_V_data_V", "role": "DATA" }} , 
 	{ "name": "s_axis_rx_data_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "rxData_V_last_V", "role": "READY" }} , 
 	{ "name": "s_axis_rx_data_TKEEP", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "rxData_V_keep_V", "role": "KEEP" }} , 
 	{ "name": "s_axis_rx_data_TLAST", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "rxData_V_last_V", "role": "LAST" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2"],
		"CDFG" : "server_64_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "listenPort_V_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "listenPort_V_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "listenPortStatus_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "listenPortStatus_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "notifications_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "notifications_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "readRequest_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "readRequest_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxMetaData_V_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "rxMetaData_V_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxData_V_data_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "s_axis_rx_data_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxData_V_keep_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "rxData_V_last_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "listenState", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "serverFsmState", "Type" : "OVld", "Direction" : "IO"}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_listenPort_V_V_U", "Parent" : "0"},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_readRequest_V_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	server_64_s {
		listenPort_V_V {Type O LastRead -1 FirstWrite 1}
		listenPortStatus_V {Type I LastRead 0 FirstWrite -1}
		notifications_V {Type I LastRead 0 FirstWrite -1}
		readRequest_V {Type O LastRead -1 FirstWrite 1}
		rxMetaData_V_V {Type I LastRead 0 FirstWrite -1}
		rxData_V_data_V {Type I LastRead 0 FirstWrite -1}
		rxData_V_keep_V {Type I LastRead 0 FirstWrite -1}
		rxData_V_last_V {Type I LastRead 0 FirstWrite -1}
		listenState {Type IO LastRead -1 FirstWrite -1}
		serverFsmState {Type IO LastRead -1 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "2", "Max" : "2"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	listenPort_V_V { axis {  { listenPort_V_V_TREADY out_acc 0 1 }  { listenPort_V_V_TDATA out_data 1 16 }  { listenPort_V_V_TVALID out_vld 1 1 } } }
	listenPortStatus_V { axis {  { listenPortStatus_V_TVALID in_vld 0 1 }  { listenPortStatus_V_TDATA in_data 0 8 }  { listenPortStatus_V_TREADY in_acc 1 1 } } }
	notifications_V { axis {  { notifications_V_TVALID in_vld 0 1 }  { notifications_V_TDATA in_data 0 88 }  { notifications_V_TREADY in_acc 1 1 } } }
	readRequest_V { axis {  { readRequest_V_TREADY out_acc 0 1 }  { readRequest_V_TDATA out_data 1 32 }  { readRequest_V_TVALID out_vld 1 1 } } }
	rxMetaData_V_V { axis {  { rxMetaData_V_V_TVALID in_vld 0 1 }  { rxMetaData_V_V_TDATA in_data 0 16 }  { rxMetaData_V_V_TREADY in_acc 1 1 } } }
	rxData_V_data_V { axis {  { s_axis_rx_data_TVALID in_vld 0 1 }  { s_axis_rx_data_TDATA in_data 0 64 } } }
	rxData_V_keep_V { axis {  { s_axis_rx_data_TKEEP in_data 0 8 } } }
	rxData_V_last_V { axis {  { s_axis_rx_data_TREADY in_acc 1 1 }  { s_axis_rx_data_TLAST in_data 0 1 } } }
}
