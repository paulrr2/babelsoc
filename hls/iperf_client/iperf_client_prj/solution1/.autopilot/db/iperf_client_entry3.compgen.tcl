# This script segment is generated automatically by AutoPilot

# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 1 \
    name runExperiment_V \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_runExperiment_V \
    op interface \
    ports { runExperiment_V { I 1 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 2 \
    name dualModeEn_V \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dualModeEn_V \
    op interface \
    ports { dualModeEn_V { I 1 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 3 \
    name useConn_V \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_useConn_V \
    op interface \
    ports { useConn_V { I 14 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 4 \
    name pkgWordCount_V \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_pkgWordCount_V \
    op interface \
    ports { pkgWordCount_V { I 8 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 5 \
    name packetGap_V \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_packetGap_V \
    op interface \
    ports { packetGap_V { I 8 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 6 \
    name timeInSeconds_V \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_timeInSeconds_V \
    op interface \
    ports { timeInSeconds_V { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 7 \
    name timeInCycles_V \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_timeInCycles_V \
    op interface \
    ports { timeInCycles_V { I 64 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 8 \
    name regIpAddress0_V \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress0_V \
    op interface \
    ports { regIpAddress0_V { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 9 \
    name regIpAddress1_V \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress1_V \
    op interface \
    ports { regIpAddress1_V { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 10 \
    name regIpAddress2_V \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress2_V \
    op interface \
    ports { regIpAddress2_V { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 11 \
    name regIpAddress3_V \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress3_V \
    op interface \
    ports { regIpAddress3_V { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 12 \
    name regIpAddress4_V \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress4_V \
    op interface \
    ports { regIpAddress4_V { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 13 \
    name regIpAddress5_V \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress5_V \
    op interface \
    ports { regIpAddress5_V { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 14 \
    name regIpAddress6_V \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress6_V \
    op interface \
    ports { regIpAddress6_V { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 15 \
    name regIpAddress7_V \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress7_V \
    op interface \
    ports { regIpAddress7_V { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 16 \
    name regIpAddress8_V \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress8_V \
    op interface \
    ports { regIpAddress8_V { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 17 \
    name regIpAddress9_V \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress9_V \
    op interface \
    ports { regIpAddress9_V { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 18 \
    name runExperiment_V_out \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_runExperiment_V_out \
    op interface \
    ports { runExperiment_V_out_din { O 1 vector } runExperiment_V_out_full_n { I 1 bit } runExperiment_V_out_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 19 \
    name dualModeEn_V_out \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dualModeEn_V_out \
    op interface \
    ports { dualModeEn_V_out_din { O 1 vector } dualModeEn_V_out_full_n { I 1 bit } dualModeEn_V_out_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 20 \
    name useConn_V_out \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_useConn_V_out \
    op interface \
    ports { useConn_V_out_din { O 14 vector } useConn_V_out_full_n { I 1 bit } useConn_V_out_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 21 \
    name pkgWordCount_V_out \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_pkgWordCount_V_out \
    op interface \
    ports { pkgWordCount_V_out_din { O 8 vector } pkgWordCount_V_out_full_n { I 1 bit } pkgWordCount_V_out_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 22 \
    name packetGap_V_out \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_packetGap_V_out \
    op interface \
    ports { packetGap_V_out_din { O 8 vector } packetGap_V_out_full_n { I 1 bit } packetGap_V_out_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 23 \
    name timeInSeconds_V_out \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_timeInSeconds_V_out \
    op interface \
    ports { timeInSeconds_V_out_din { O 32 vector } timeInSeconds_V_out_full_n { I 1 bit } timeInSeconds_V_out_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 24 \
    name timeInCycles_V_out \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_timeInCycles_V_out \
    op interface \
    ports { timeInCycles_V_out_din { O 64 vector } timeInCycles_V_out_full_n { I 1 bit } timeInCycles_V_out_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 25 \
    name regIpAddress0_V_out \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress0_V_out \
    op interface \
    ports { regIpAddress0_V_out_din { O 32 vector } regIpAddress0_V_out_full_n { I 1 bit } regIpAddress0_V_out_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 26 \
    name regIpAddress1_V_out \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress1_V_out \
    op interface \
    ports { regIpAddress1_V_out_din { O 32 vector } regIpAddress1_V_out_full_n { I 1 bit } regIpAddress1_V_out_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 27 \
    name regIpAddress2_V_out \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress2_V_out \
    op interface \
    ports { regIpAddress2_V_out_din { O 32 vector } regIpAddress2_V_out_full_n { I 1 bit } regIpAddress2_V_out_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 28 \
    name regIpAddress3_V_out \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress3_V_out \
    op interface \
    ports { regIpAddress3_V_out_din { O 32 vector } regIpAddress3_V_out_full_n { I 1 bit } regIpAddress3_V_out_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 29 \
    name regIpAddress4_V_out \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress4_V_out \
    op interface \
    ports { regIpAddress4_V_out_din { O 32 vector } regIpAddress4_V_out_full_n { I 1 bit } regIpAddress4_V_out_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 30 \
    name regIpAddress5_V_out \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress5_V_out \
    op interface \
    ports { regIpAddress5_V_out_din { O 32 vector } regIpAddress5_V_out_full_n { I 1 bit } regIpAddress5_V_out_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 31 \
    name regIpAddress6_V_out \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress6_V_out \
    op interface \
    ports { regIpAddress6_V_out_din { O 32 vector } regIpAddress6_V_out_full_n { I 1 bit } regIpAddress6_V_out_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 32 \
    name regIpAddress7_V_out \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress7_V_out \
    op interface \
    ports { regIpAddress7_V_out_din { O 32 vector } regIpAddress7_V_out_full_n { I 1 bit } regIpAddress7_V_out_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 33 \
    name regIpAddress8_V_out \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress8_V_out \
    op interface \
    ports { regIpAddress8_V_out_din { O 32 vector } regIpAddress8_V_out_full_n { I 1 bit } regIpAddress8_V_out_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 34 \
    name regIpAddress9_V_out \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress9_V_out \
    op interface \
    ports { regIpAddress9_V_out_din { O 32 vector } regIpAddress9_V_out_full_n { I 1 bit } regIpAddress9_V_out_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_start { I 1 bit } ap_ready { O 1 bit } ap_done { O 1 bit } ap_idle { O 1 bit } ap_continue { I 1 bit } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


