set moduleName iperf_client_entry3
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 1
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {iperf_client.entry3}
set C_modelType { void 0 }
set C_modelArgList {
	{ runExperiment_V int 1 regular  }
	{ dualModeEn_V int 1 regular  }
	{ useConn_V int 14 regular  }
	{ pkgWordCount_V int 8 regular  }
	{ packetGap_V int 8 regular  }
	{ timeInSeconds_V int 32 regular  }
	{ timeInCycles_V int 64 regular  }
	{ regIpAddress0_V int 32 regular  }
	{ regIpAddress1_V int 32 regular  }
	{ regIpAddress2_V int 32 regular  }
	{ regIpAddress3_V int 32 regular  }
	{ regIpAddress4_V int 32 regular  }
	{ regIpAddress5_V int 32 regular  }
	{ regIpAddress6_V int 32 regular  }
	{ regIpAddress7_V int 32 regular  }
	{ regIpAddress8_V int 32 regular  }
	{ regIpAddress9_V int 32 regular  }
	{ runExperiment_V_out int 1 regular {fifo 1}  }
	{ dualModeEn_V_out int 1 regular {fifo 1}  }
	{ useConn_V_out int 14 regular {fifo 1}  }
	{ pkgWordCount_V_out int 8 regular {fifo 1}  }
	{ packetGap_V_out int 8 regular {fifo 1}  }
	{ timeInSeconds_V_out int 32 regular {fifo 1}  }
	{ timeInCycles_V_out int 64 regular {fifo 1}  }
	{ regIpAddress0_V_out int 32 regular {fifo 1}  }
	{ regIpAddress1_V_out int 32 regular {fifo 1}  }
	{ regIpAddress2_V_out int 32 regular {fifo 1}  }
	{ regIpAddress3_V_out int 32 regular {fifo 1}  }
	{ regIpAddress4_V_out int 32 regular {fifo 1}  }
	{ regIpAddress5_V_out int 32 regular {fifo 1}  }
	{ regIpAddress6_V_out int 32 regular {fifo 1}  }
	{ regIpAddress7_V_out int 32 regular {fifo 1}  }
	{ regIpAddress8_V_out int 32 regular {fifo 1}  }
	{ regIpAddress9_V_out int 32 regular {fifo 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "runExperiment_V", "interface" : "wire", "bitwidth" : 1, "direction" : "READONLY"} , 
 	{ "Name" : "dualModeEn_V", "interface" : "wire", "bitwidth" : 1, "direction" : "READONLY"} , 
 	{ "Name" : "useConn_V", "interface" : "wire", "bitwidth" : 14, "direction" : "READONLY"} , 
 	{ "Name" : "pkgWordCount_V", "interface" : "wire", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "packetGap_V", "interface" : "wire", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "timeInSeconds_V", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "timeInCycles_V", "interface" : "wire", "bitwidth" : 64, "direction" : "READONLY"} , 
 	{ "Name" : "regIpAddress0_V", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "regIpAddress1_V", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "regIpAddress2_V", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "regIpAddress3_V", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "regIpAddress4_V", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "regIpAddress5_V", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "regIpAddress6_V", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "regIpAddress7_V", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "regIpAddress8_V", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "regIpAddress9_V", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "runExperiment_V_out", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY"} , 
 	{ "Name" : "dualModeEn_V_out", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY"} , 
 	{ "Name" : "useConn_V_out", "interface" : "fifo", "bitwidth" : 14, "direction" : "WRITEONLY"} , 
 	{ "Name" : "pkgWordCount_V_out", "interface" : "fifo", "bitwidth" : 8, "direction" : "WRITEONLY"} , 
 	{ "Name" : "packetGap_V_out", "interface" : "fifo", "bitwidth" : 8, "direction" : "WRITEONLY"} , 
 	{ "Name" : "timeInSeconds_V_out", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "timeInCycles_V_out", "interface" : "fifo", "bitwidth" : 64, "direction" : "WRITEONLY"} , 
 	{ "Name" : "regIpAddress0_V_out", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "regIpAddress1_V_out", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "regIpAddress2_V_out", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "regIpAddress3_V_out", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "regIpAddress4_V_out", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "regIpAddress5_V_out", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "regIpAddress6_V_out", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "regIpAddress7_V_out", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "regIpAddress8_V_out", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "regIpAddress9_V_out", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY"} ]}
# RTL Port declarations: 
set portNum 75
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ runExperiment_V sc_in sc_lv 1 signal 0 } 
	{ dualModeEn_V sc_in sc_lv 1 signal 1 } 
	{ useConn_V sc_in sc_lv 14 signal 2 } 
	{ pkgWordCount_V sc_in sc_lv 8 signal 3 } 
	{ packetGap_V sc_in sc_lv 8 signal 4 } 
	{ timeInSeconds_V sc_in sc_lv 32 signal 5 } 
	{ timeInCycles_V sc_in sc_lv 64 signal 6 } 
	{ regIpAddress0_V sc_in sc_lv 32 signal 7 } 
	{ regIpAddress1_V sc_in sc_lv 32 signal 8 } 
	{ regIpAddress2_V sc_in sc_lv 32 signal 9 } 
	{ regIpAddress3_V sc_in sc_lv 32 signal 10 } 
	{ regIpAddress4_V sc_in sc_lv 32 signal 11 } 
	{ regIpAddress5_V sc_in sc_lv 32 signal 12 } 
	{ regIpAddress6_V sc_in sc_lv 32 signal 13 } 
	{ regIpAddress7_V sc_in sc_lv 32 signal 14 } 
	{ regIpAddress8_V sc_in sc_lv 32 signal 15 } 
	{ regIpAddress9_V sc_in sc_lv 32 signal 16 } 
	{ runExperiment_V_out_din sc_out sc_lv 1 signal 17 } 
	{ runExperiment_V_out_full_n sc_in sc_logic 1 signal 17 } 
	{ runExperiment_V_out_write sc_out sc_logic 1 signal 17 } 
	{ dualModeEn_V_out_din sc_out sc_lv 1 signal 18 } 
	{ dualModeEn_V_out_full_n sc_in sc_logic 1 signal 18 } 
	{ dualModeEn_V_out_write sc_out sc_logic 1 signal 18 } 
	{ useConn_V_out_din sc_out sc_lv 14 signal 19 } 
	{ useConn_V_out_full_n sc_in sc_logic 1 signal 19 } 
	{ useConn_V_out_write sc_out sc_logic 1 signal 19 } 
	{ pkgWordCount_V_out_din sc_out sc_lv 8 signal 20 } 
	{ pkgWordCount_V_out_full_n sc_in sc_logic 1 signal 20 } 
	{ pkgWordCount_V_out_write sc_out sc_logic 1 signal 20 } 
	{ packetGap_V_out_din sc_out sc_lv 8 signal 21 } 
	{ packetGap_V_out_full_n sc_in sc_logic 1 signal 21 } 
	{ packetGap_V_out_write sc_out sc_logic 1 signal 21 } 
	{ timeInSeconds_V_out_din sc_out sc_lv 32 signal 22 } 
	{ timeInSeconds_V_out_full_n sc_in sc_logic 1 signal 22 } 
	{ timeInSeconds_V_out_write sc_out sc_logic 1 signal 22 } 
	{ timeInCycles_V_out_din sc_out sc_lv 64 signal 23 } 
	{ timeInCycles_V_out_full_n sc_in sc_logic 1 signal 23 } 
	{ timeInCycles_V_out_write sc_out sc_logic 1 signal 23 } 
	{ regIpAddress0_V_out_din sc_out sc_lv 32 signal 24 } 
	{ regIpAddress0_V_out_full_n sc_in sc_logic 1 signal 24 } 
	{ regIpAddress0_V_out_write sc_out sc_logic 1 signal 24 } 
	{ regIpAddress1_V_out_din sc_out sc_lv 32 signal 25 } 
	{ regIpAddress1_V_out_full_n sc_in sc_logic 1 signal 25 } 
	{ regIpAddress1_V_out_write sc_out sc_logic 1 signal 25 } 
	{ regIpAddress2_V_out_din sc_out sc_lv 32 signal 26 } 
	{ regIpAddress2_V_out_full_n sc_in sc_logic 1 signal 26 } 
	{ regIpAddress2_V_out_write sc_out sc_logic 1 signal 26 } 
	{ regIpAddress3_V_out_din sc_out sc_lv 32 signal 27 } 
	{ regIpAddress3_V_out_full_n sc_in sc_logic 1 signal 27 } 
	{ regIpAddress3_V_out_write sc_out sc_logic 1 signal 27 } 
	{ regIpAddress4_V_out_din sc_out sc_lv 32 signal 28 } 
	{ regIpAddress4_V_out_full_n sc_in sc_logic 1 signal 28 } 
	{ regIpAddress4_V_out_write sc_out sc_logic 1 signal 28 } 
	{ regIpAddress5_V_out_din sc_out sc_lv 32 signal 29 } 
	{ regIpAddress5_V_out_full_n sc_in sc_logic 1 signal 29 } 
	{ regIpAddress5_V_out_write sc_out sc_logic 1 signal 29 } 
	{ regIpAddress6_V_out_din sc_out sc_lv 32 signal 30 } 
	{ regIpAddress6_V_out_full_n sc_in sc_logic 1 signal 30 } 
	{ regIpAddress6_V_out_write sc_out sc_logic 1 signal 30 } 
	{ regIpAddress7_V_out_din sc_out sc_lv 32 signal 31 } 
	{ regIpAddress7_V_out_full_n sc_in sc_logic 1 signal 31 } 
	{ regIpAddress7_V_out_write sc_out sc_logic 1 signal 31 } 
	{ regIpAddress8_V_out_din sc_out sc_lv 32 signal 32 } 
	{ regIpAddress8_V_out_full_n sc_in sc_logic 1 signal 32 } 
	{ regIpAddress8_V_out_write sc_out sc_logic 1 signal 32 } 
	{ regIpAddress9_V_out_din sc_out sc_lv 32 signal 33 } 
	{ regIpAddress9_V_out_full_n sc_in sc_logic 1 signal 33 } 
	{ regIpAddress9_V_out_write sc_out sc_logic 1 signal 33 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "runExperiment_V", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "runExperiment_V", "role": "default" }} , 
 	{ "name": "dualModeEn_V", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "dualModeEn_V", "role": "default" }} , 
 	{ "name": "useConn_V", "direction": "in", "datatype": "sc_lv", "bitwidth":14, "type": "signal", "bundle":{"name": "useConn_V", "role": "default" }} , 
 	{ "name": "pkgWordCount_V", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "pkgWordCount_V", "role": "default" }} , 
 	{ "name": "packetGap_V", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "packetGap_V", "role": "default" }} , 
 	{ "name": "timeInSeconds_V", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "timeInSeconds_V", "role": "default" }} , 
 	{ "name": "timeInCycles_V", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "timeInCycles_V", "role": "default" }} , 
 	{ "name": "regIpAddress0_V", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress0_V", "role": "default" }} , 
 	{ "name": "regIpAddress1_V", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress1_V", "role": "default" }} , 
 	{ "name": "regIpAddress2_V", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress2_V", "role": "default" }} , 
 	{ "name": "regIpAddress3_V", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress3_V", "role": "default" }} , 
 	{ "name": "regIpAddress4_V", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress4_V", "role": "default" }} , 
 	{ "name": "regIpAddress5_V", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress5_V", "role": "default" }} , 
 	{ "name": "regIpAddress6_V", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress6_V", "role": "default" }} , 
 	{ "name": "regIpAddress7_V", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress7_V", "role": "default" }} , 
 	{ "name": "regIpAddress8_V", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress8_V", "role": "default" }} , 
 	{ "name": "regIpAddress9_V", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress9_V", "role": "default" }} , 
 	{ "name": "runExperiment_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "runExperiment_V_out", "role": "din" }} , 
 	{ "name": "runExperiment_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "runExperiment_V_out", "role": "full_n" }} , 
 	{ "name": "runExperiment_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "runExperiment_V_out", "role": "write" }} , 
 	{ "name": "dualModeEn_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "dualModeEn_V_out", "role": "din" }} , 
 	{ "name": "dualModeEn_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dualModeEn_V_out", "role": "full_n" }} , 
 	{ "name": "dualModeEn_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dualModeEn_V_out", "role": "write" }} , 
 	{ "name": "useConn_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":14, "type": "signal", "bundle":{"name": "useConn_V_out", "role": "din" }} , 
 	{ "name": "useConn_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "useConn_V_out", "role": "full_n" }} , 
 	{ "name": "useConn_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "useConn_V_out", "role": "write" }} , 
 	{ "name": "pkgWordCount_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "pkgWordCount_V_out", "role": "din" }} , 
 	{ "name": "pkgWordCount_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "pkgWordCount_V_out", "role": "full_n" }} , 
 	{ "name": "pkgWordCount_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "pkgWordCount_V_out", "role": "write" }} , 
 	{ "name": "packetGap_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "packetGap_V_out", "role": "din" }} , 
 	{ "name": "packetGap_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "packetGap_V_out", "role": "full_n" }} , 
 	{ "name": "packetGap_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "packetGap_V_out", "role": "write" }} , 
 	{ "name": "timeInSeconds_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "timeInSeconds_V_out", "role": "din" }} , 
 	{ "name": "timeInSeconds_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "timeInSeconds_V_out", "role": "full_n" }} , 
 	{ "name": "timeInSeconds_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "timeInSeconds_V_out", "role": "write" }} , 
 	{ "name": "timeInCycles_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "timeInCycles_V_out", "role": "din" }} , 
 	{ "name": "timeInCycles_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "timeInCycles_V_out", "role": "full_n" }} , 
 	{ "name": "timeInCycles_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "timeInCycles_V_out", "role": "write" }} , 
 	{ "name": "regIpAddress0_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress0_V_out", "role": "din" }} , 
 	{ "name": "regIpAddress0_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress0_V_out", "role": "full_n" }} , 
 	{ "name": "regIpAddress0_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress0_V_out", "role": "write" }} , 
 	{ "name": "regIpAddress1_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress1_V_out", "role": "din" }} , 
 	{ "name": "regIpAddress1_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress1_V_out", "role": "full_n" }} , 
 	{ "name": "regIpAddress1_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress1_V_out", "role": "write" }} , 
 	{ "name": "regIpAddress2_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress2_V_out", "role": "din" }} , 
 	{ "name": "regIpAddress2_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress2_V_out", "role": "full_n" }} , 
 	{ "name": "regIpAddress2_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress2_V_out", "role": "write" }} , 
 	{ "name": "regIpAddress3_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress3_V_out", "role": "din" }} , 
 	{ "name": "regIpAddress3_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress3_V_out", "role": "full_n" }} , 
 	{ "name": "regIpAddress3_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress3_V_out", "role": "write" }} , 
 	{ "name": "regIpAddress4_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress4_V_out", "role": "din" }} , 
 	{ "name": "regIpAddress4_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress4_V_out", "role": "full_n" }} , 
 	{ "name": "regIpAddress4_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress4_V_out", "role": "write" }} , 
 	{ "name": "regIpAddress5_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress5_V_out", "role": "din" }} , 
 	{ "name": "regIpAddress5_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress5_V_out", "role": "full_n" }} , 
 	{ "name": "regIpAddress5_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress5_V_out", "role": "write" }} , 
 	{ "name": "regIpAddress6_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress6_V_out", "role": "din" }} , 
 	{ "name": "regIpAddress6_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress6_V_out", "role": "full_n" }} , 
 	{ "name": "regIpAddress6_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress6_V_out", "role": "write" }} , 
 	{ "name": "regIpAddress7_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress7_V_out", "role": "din" }} , 
 	{ "name": "regIpAddress7_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress7_V_out", "role": "full_n" }} , 
 	{ "name": "regIpAddress7_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress7_V_out", "role": "write" }} , 
 	{ "name": "regIpAddress8_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress8_V_out", "role": "din" }} , 
 	{ "name": "regIpAddress8_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress8_V_out", "role": "full_n" }} , 
 	{ "name": "regIpAddress8_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress8_V_out", "role": "write" }} , 
 	{ "name": "regIpAddress9_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress9_V_out", "role": "din" }} , 
 	{ "name": "regIpAddress9_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress9_V_out", "role": "full_n" }} , 
 	{ "name": "regIpAddress9_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress9_V_out", "role": "write" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "iperf_client_entry3",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "0", "EstimateLatencyMin" : "0", "EstimateLatencyMax" : "0",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "0",
		"Port" : [
			{"Name" : "runExperiment_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "dualModeEn_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "useConn_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "pkgWordCount_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "packetGap_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "timeInSeconds_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "timeInCycles_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "regIpAddress0_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "regIpAddress1_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "regIpAddress2_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "regIpAddress3_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "regIpAddress4_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "regIpAddress5_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "regIpAddress6_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "regIpAddress7_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "regIpAddress8_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "regIpAddress9_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "runExperiment_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "runExperiment_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dualModeEn_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "dualModeEn_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "useConn_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "useConn_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "pkgWordCount_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "pkgWordCount_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "packetGap_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "packetGap_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "timeInSeconds_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "timeInSeconds_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "timeInCycles_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "timeInCycles_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress0_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress0_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress1_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress1_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress2_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress2_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress3_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress3_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress4_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress4_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress5_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress5_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress6_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress6_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress7_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress7_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress8_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress8_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress9_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress9_V_out_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	iperf_client_entry3 {
		runExperiment_V {Type I LastRead 0 FirstWrite -1}
		dualModeEn_V {Type I LastRead 0 FirstWrite -1}
		useConn_V {Type I LastRead 0 FirstWrite -1}
		pkgWordCount_V {Type I LastRead 0 FirstWrite -1}
		packetGap_V {Type I LastRead 0 FirstWrite -1}
		timeInSeconds_V {Type I LastRead 0 FirstWrite -1}
		timeInCycles_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress0_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress1_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress2_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress3_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress4_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress5_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress6_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress7_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress8_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress9_V {Type I LastRead 0 FirstWrite -1}
		runExperiment_V_out {Type O LastRead -1 FirstWrite 0}
		dualModeEn_V_out {Type O LastRead -1 FirstWrite 0}
		useConn_V_out {Type O LastRead -1 FirstWrite 0}
		pkgWordCount_V_out {Type O LastRead -1 FirstWrite 0}
		packetGap_V_out {Type O LastRead -1 FirstWrite 0}
		timeInSeconds_V_out {Type O LastRead -1 FirstWrite 0}
		timeInCycles_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress0_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress1_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress2_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress3_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress4_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress5_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress6_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress7_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress8_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress9_V_out {Type O LastRead -1 FirstWrite 0}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "0", "Max" : "0"}
	, {"Name" : "Interval", "Min" : "0", "Max" : "0"}
]}

set PipelineEnableSignalInfo {[
]}

set Spec2ImplPortList { 
	runExperiment_V { ap_none {  { runExperiment_V in_data 0 1 } } }
	dualModeEn_V { ap_none {  { dualModeEn_V in_data 0 1 } } }
	useConn_V { ap_none {  { useConn_V in_data 0 14 } } }
	pkgWordCount_V { ap_none {  { pkgWordCount_V in_data 0 8 } } }
	packetGap_V { ap_none {  { packetGap_V in_data 0 8 } } }
	timeInSeconds_V { ap_none {  { timeInSeconds_V in_data 0 32 } } }
	timeInCycles_V { ap_none {  { timeInCycles_V in_data 0 64 } } }
	regIpAddress0_V { ap_none {  { regIpAddress0_V in_data 0 32 } } }
	regIpAddress1_V { ap_none {  { regIpAddress1_V in_data 0 32 } } }
	regIpAddress2_V { ap_none {  { regIpAddress2_V in_data 0 32 } } }
	regIpAddress3_V { ap_none {  { regIpAddress3_V in_data 0 32 } } }
	regIpAddress4_V { ap_none {  { regIpAddress4_V in_data 0 32 } } }
	regIpAddress5_V { ap_none {  { regIpAddress5_V in_data 0 32 } } }
	regIpAddress6_V { ap_none {  { regIpAddress6_V in_data 0 32 } } }
	regIpAddress7_V { ap_none {  { regIpAddress7_V in_data 0 32 } } }
	regIpAddress8_V { ap_none {  { regIpAddress8_V in_data 0 32 } } }
	regIpAddress9_V { ap_none {  { regIpAddress9_V in_data 0 32 } } }
	runExperiment_V_out { ap_fifo {  { runExperiment_V_out_din fifo_data 1 1 }  { runExperiment_V_out_full_n fifo_status 0 1 }  { runExperiment_V_out_write fifo_update 1 1 } } }
	dualModeEn_V_out { ap_fifo {  { dualModeEn_V_out_din fifo_data 1 1 }  { dualModeEn_V_out_full_n fifo_status 0 1 }  { dualModeEn_V_out_write fifo_update 1 1 } } }
	useConn_V_out { ap_fifo {  { useConn_V_out_din fifo_data 1 14 }  { useConn_V_out_full_n fifo_status 0 1 }  { useConn_V_out_write fifo_update 1 1 } } }
	pkgWordCount_V_out { ap_fifo {  { pkgWordCount_V_out_din fifo_data 1 8 }  { pkgWordCount_V_out_full_n fifo_status 0 1 }  { pkgWordCount_V_out_write fifo_update 1 1 } } }
	packetGap_V_out { ap_fifo {  { packetGap_V_out_din fifo_data 1 8 }  { packetGap_V_out_full_n fifo_status 0 1 }  { packetGap_V_out_write fifo_update 1 1 } } }
	timeInSeconds_V_out { ap_fifo {  { timeInSeconds_V_out_din fifo_data 1 32 }  { timeInSeconds_V_out_full_n fifo_status 0 1 }  { timeInSeconds_V_out_write fifo_update 1 1 } } }
	timeInCycles_V_out { ap_fifo {  { timeInCycles_V_out_din fifo_data 1 64 }  { timeInCycles_V_out_full_n fifo_status 0 1 }  { timeInCycles_V_out_write fifo_update 1 1 } } }
	regIpAddress0_V_out { ap_fifo {  { regIpAddress0_V_out_din fifo_data 1 32 }  { regIpAddress0_V_out_full_n fifo_status 0 1 }  { regIpAddress0_V_out_write fifo_update 1 1 } } }
	regIpAddress1_V_out { ap_fifo {  { regIpAddress1_V_out_din fifo_data 1 32 }  { regIpAddress1_V_out_full_n fifo_status 0 1 }  { regIpAddress1_V_out_write fifo_update 1 1 } } }
	regIpAddress2_V_out { ap_fifo {  { regIpAddress2_V_out_din fifo_data 1 32 }  { regIpAddress2_V_out_full_n fifo_status 0 1 }  { regIpAddress2_V_out_write fifo_update 1 1 } } }
	regIpAddress3_V_out { ap_fifo {  { regIpAddress3_V_out_din fifo_data 1 32 }  { regIpAddress3_V_out_full_n fifo_status 0 1 }  { regIpAddress3_V_out_write fifo_update 1 1 } } }
	regIpAddress4_V_out { ap_fifo {  { regIpAddress4_V_out_din fifo_data 1 32 }  { regIpAddress4_V_out_full_n fifo_status 0 1 }  { regIpAddress4_V_out_write fifo_update 1 1 } } }
	regIpAddress5_V_out { ap_fifo {  { regIpAddress5_V_out_din fifo_data 1 32 }  { regIpAddress5_V_out_full_n fifo_status 0 1 }  { regIpAddress5_V_out_write fifo_update 1 1 } } }
	regIpAddress6_V_out { ap_fifo {  { regIpAddress6_V_out_din fifo_data 1 32 }  { regIpAddress6_V_out_full_n fifo_status 0 1 }  { regIpAddress6_V_out_write fifo_update 1 1 } } }
	regIpAddress7_V_out { ap_fifo {  { regIpAddress7_V_out_din fifo_data 1 32 }  { regIpAddress7_V_out_full_n fifo_status 0 1 }  { regIpAddress7_V_out_write fifo_update 1 1 } } }
	regIpAddress8_V_out { ap_fifo {  { regIpAddress8_V_out_din fifo_data 1 32 }  { regIpAddress8_V_out_full_n fifo_status 0 1 }  { regIpAddress8_V_out_write fifo_update 1 1 } } }
	regIpAddress9_V_out { ap_fifo {  { regIpAddress9_V_out_din fifo_data 1 32 }  { regIpAddress9_V_out_full_n fifo_status 0 1 }  { regIpAddress9_V_out_write fifo_update 1 1 } } }
}
