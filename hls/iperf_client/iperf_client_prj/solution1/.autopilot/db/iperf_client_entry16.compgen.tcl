# This script segment is generated automatically by AutoPilot

# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 35 \
    name runExperiment_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_runExperiment_V \
    op interface \
    ports { runExperiment_V_dout { I 1 vector } runExperiment_V_empty_n { I 1 bit } runExperiment_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 36 \
    name dualModeEn_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dualModeEn_V \
    op interface \
    ports { dualModeEn_V_dout { I 1 vector } dualModeEn_V_empty_n { I 1 bit } dualModeEn_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 37 \
    name useConn_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_useConn_V \
    op interface \
    ports { useConn_V_dout { I 14 vector } useConn_V_empty_n { I 1 bit } useConn_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 38 \
    name pkgWordCount_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_pkgWordCount_V \
    op interface \
    ports { pkgWordCount_V_dout { I 8 vector } pkgWordCount_V_empty_n { I 1 bit } pkgWordCount_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 39 \
    name packetGap_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_packetGap_V \
    op interface \
    ports { packetGap_V_dout { I 8 vector } packetGap_V_empty_n { I 1 bit } packetGap_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 40 \
    name timeInSeconds_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_timeInSeconds_V \
    op interface \
    ports { timeInSeconds_V_dout { I 32 vector } timeInSeconds_V_empty_n { I 1 bit } timeInSeconds_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 41 \
    name timeInCycles_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_timeInCycles_V \
    op interface \
    ports { timeInCycles_V_dout { I 64 vector } timeInCycles_V_empty_n { I 1 bit } timeInCycles_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 42 \
    name regIpAddress0_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress0_V \
    op interface \
    ports { regIpAddress0_V_dout { I 32 vector } regIpAddress0_V_empty_n { I 1 bit } regIpAddress0_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 43 \
    name regIpAddress1_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress1_V \
    op interface \
    ports { regIpAddress1_V_dout { I 32 vector } regIpAddress1_V_empty_n { I 1 bit } regIpAddress1_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 44 \
    name regIpAddress2_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress2_V \
    op interface \
    ports { regIpAddress2_V_dout { I 32 vector } regIpAddress2_V_empty_n { I 1 bit } regIpAddress2_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 45 \
    name regIpAddress3_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress3_V \
    op interface \
    ports { regIpAddress3_V_dout { I 32 vector } regIpAddress3_V_empty_n { I 1 bit } regIpAddress3_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 46 \
    name regIpAddress4_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress4_V \
    op interface \
    ports { regIpAddress4_V_dout { I 32 vector } regIpAddress4_V_empty_n { I 1 bit } regIpAddress4_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 47 \
    name regIpAddress5_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress5_V \
    op interface \
    ports { regIpAddress5_V_dout { I 32 vector } regIpAddress5_V_empty_n { I 1 bit } regIpAddress5_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 48 \
    name regIpAddress6_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress6_V \
    op interface \
    ports { regIpAddress6_V_dout { I 32 vector } regIpAddress6_V_empty_n { I 1 bit } regIpAddress6_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 49 \
    name regIpAddress7_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress7_V \
    op interface \
    ports { regIpAddress7_V_dout { I 32 vector } regIpAddress7_V_empty_n { I 1 bit } regIpAddress7_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 50 \
    name regIpAddress8_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress8_V \
    op interface \
    ports { regIpAddress8_V_dout { I 32 vector } regIpAddress8_V_empty_n { I 1 bit } regIpAddress8_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 51 \
    name regIpAddress9_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress9_V \
    op interface \
    ports { regIpAddress9_V_dout { I 32 vector } regIpAddress9_V_empty_n { I 1 bit } regIpAddress9_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 52 \
    name runExperiment_V_out \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_runExperiment_V_out \
    op interface \
    ports { runExperiment_V_out_din { O 1 vector } runExperiment_V_out_full_n { I 1 bit } runExperiment_V_out_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 53 \
    name dualModeEn_V_out \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dualModeEn_V_out \
    op interface \
    ports { dualModeEn_V_out_din { O 1 vector } dualModeEn_V_out_full_n { I 1 bit } dualModeEn_V_out_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 54 \
    name useConn_V_out \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_useConn_V_out \
    op interface \
    ports { useConn_V_out_din { O 14 vector } useConn_V_out_full_n { I 1 bit } useConn_V_out_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 55 \
    name pkgWordCount_V_out \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_pkgWordCount_V_out \
    op interface \
    ports { pkgWordCount_V_out_din { O 8 vector } pkgWordCount_V_out_full_n { I 1 bit } pkgWordCount_V_out_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 56 \
    name packetGap_V_out \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_packetGap_V_out \
    op interface \
    ports { packetGap_V_out_din { O 8 vector } packetGap_V_out_full_n { I 1 bit } packetGap_V_out_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 57 \
    name timeInSeconds_V_out \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_timeInSeconds_V_out \
    op interface \
    ports { timeInSeconds_V_out_din { O 32 vector } timeInSeconds_V_out_full_n { I 1 bit } timeInSeconds_V_out_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 58 \
    name timeInCycles_V_out \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_timeInCycles_V_out \
    op interface \
    ports { timeInCycles_V_out_din { O 64 vector } timeInCycles_V_out_full_n { I 1 bit } timeInCycles_V_out_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 59 \
    name regIpAddress0_V_out \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress0_V_out \
    op interface \
    ports { regIpAddress0_V_out_din { O 32 vector } regIpAddress0_V_out_full_n { I 1 bit } regIpAddress0_V_out_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 60 \
    name regIpAddress1_V_out \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress1_V_out \
    op interface \
    ports { regIpAddress1_V_out_din { O 32 vector } regIpAddress1_V_out_full_n { I 1 bit } regIpAddress1_V_out_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 61 \
    name regIpAddress2_V_out \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress2_V_out \
    op interface \
    ports { regIpAddress2_V_out_din { O 32 vector } regIpAddress2_V_out_full_n { I 1 bit } regIpAddress2_V_out_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 62 \
    name regIpAddress3_V_out \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress3_V_out \
    op interface \
    ports { regIpAddress3_V_out_din { O 32 vector } regIpAddress3_V_out_full_n { I 1 bit } regIpAddress3_V_out_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 63 \
    name regIpAddress4_V_out \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress4_V_out \
    op interface \
    ports { regIpAddress4_V_out_din { O 32 vector } regIpAddress4_V_out_full_n { I 1 bit } regIpAddress4_V_out_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 64 \
    name regIpAddress5_V_out \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress5_V_out \
    op interface \
    ports { regIpAddress5_V_out_din { O 32 vector } regIpAddress5_V_out_full_n { I 1 bit } regIpAddress5_V_out_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 65 \
    name regIpAddress6_V_out \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress6_V_out \
    op interface \
    ports { regIpAddress6_V_out_din { O 32 vector } regIpAddress6_V_out_full_n { I 1 bit } regIpAddress6_V_out_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 66 \
    name regIpAddress7_V_out \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress7_V_out \
    op interface \
    ports { regIpAddress7_V_out_din { O 32 vector } regIpAddress7_V_out_full_n { I 1 bit } regIpAddress7_V_out_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 67 \
    name regIpAddress8_V_out \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress8_V_out \
    op interface \
    ports { regIpAddress8_V_out_din { O 32 vector } regIpAddress8_V_out_full_n { I 1 bit } regIpAddress8_V_out_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 68 \
    name regIpAddress9_V_out \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress9_V_out \
    op interface \
    ports { regIpAddress9_V_out_din { O 32 vector } regIpAddress9_V_out_full_n { I 1 bit } regIpAddress9_V_out_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_start { I 1 bit } ap_ready { O 1 bit } ap_done { O 1 bit } ap_idle { O 1 bit } ap_continue { I 1 bit } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


