set moduleName client_64_s
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {client<64>}
set C_modelType { void 0 }
set C_modelArgList {
	{ openConnection_V int 48 regular {axi_s 1 volatile  { openConnection_V Data } }  }
	{ openConStatus_V int 24 regular {axi_s 0 volatile  { openConStatus_V Data } }  }
	{ closeConnection_V_V int 16 regular {axi_s 1 volatile  { closeConnection_V_V Data } }  }
	{ txMetaData_V int 32 regular {axi_s 1 volatile  { txMetaData_V Data } }  }
	{ txData_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_tx_data Data } }  }
	{ txData_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_tx_data Keep } }  }
	{ txData_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_tx_data Last } }  }
	{ runExperiment_V int 1 regular {fifo 0}  }
	{ dualModeEn_V int 1 regular {fifo 0}  }
	{ useConn_V int 14 regular {fifo 0}  }
	{ pkgWordCount_V int 8 regular {fifo 0}  }
	{ packetGap_V int 8 regular {fifo 0}  }
	{ timeInSeconds_V int 32 regular {fifo 0}  }
	{ regIpAddress0_V int 32 regular {fifo 0}  }
	{ regIpAddress1_V int 32 regular {fifo 0}  }
	{ regIpAddress2_V int 32 regular {fifo 0}  }
	{ regIpAddress3_V int 32 regular {fifo 0}  }
	{ regIpAddress4_V int 32 regular {fifo 0}  }
	{ regIpAddress5_V int 32 regular {fifo 0}  }
	{ regIpAddress6_V int 32 regular {fifo 0}  }
	{ regIpAddress7_V int 32 regular {fifo 0}  }
	{ regIpAddress8_V int 32 regular {fifo 0}  }
	{ regIpAddress9_V int 32 regular {fifo 0}  }
	{ startSignalFifo_V int 1 regular {fifo 1 volatile } {global 1}  }
	{ txStatusBuffer_V_ses int 16 regular {fifo 0 volatile } {global 0}  }
	{ txStatusBuffer_V_err int 2 regular {fifo 0 volatile } {global 0}  }
	{ stopSignalFifo_V int 1 regular {fifo 0 volatile } {global 0}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "openConnection_V", "interface" : "axis", "bitwidth" : 48, "direction" : "WRITEONLY"} , 
 	{ "Name" : "openConStatus_V", "interface" : "axis", "bitwidth" : 24, "direction" : "READONLY"} , 
 	{ "Name" : "closeConnection_V_V", "interface" : "axis", "bitwidth" : 16, "direction" : "WRITEONLY"} , 
 	{ "Name" : "txMetaData_V", "interface" : "axis", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "txData_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY"} , 
 	{ "Name" : "txData_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY"} , 
 	{ "Name" : "txData_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY"} , 
 	{ "Name" : "runExperiment_V", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY"} , 
 	{ "Name" : "dualModeEn_V", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY"} , 
 	{ "Name" : "useConn_V", "interface" : "fifo", "bitwidth" : 14, "direction" : "READONLY"} , 
 	{ "Name" : "pkgWordCount_V", "interface" : "fifo", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "packetGap_V", "interface" : "fifo", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "timeInSeconds_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "regIpAddress0_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "regIpAddress1_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "regIpAddress2_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "regIpAddress3_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "regIpAddress4_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "regIpAddress5_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "regIpAddress6_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "regIpAddress7_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "regIpAddress8_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "regIpAddress9_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "startSignalFifo_V", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "txStatusBuffer_V_ses", "interface" : "fifo", "bitwidth" : 16, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "txStatusBuffer_V_err", "interface" : "fifo", "bitwidth" : 2, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "stopSignalFifo_V", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 84
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ runExperiment_V_dout sc_in sc_lv 1 signal 7 } 
	{ runExperiment_V_empty_n sc_in sc_logic 1 signal 7 } 
	{ runExperiment_V_read sc_out sc_logic 1 signal 7 } 
	{ dualModeEn_V_dout sc_in sc_lv 1 signal 8 } 
	{ dualModeEn_V_empty_n sc_in sc_logic 1 signal 8 } 
	{ dualModeEn_V_read sc_out sc_logic 1 signal 8 } 
	{ useConn_V_dout sc_in sc_lv 14 signal 9 } 
	{ useConn_V_empty_n sc_in sc_logic 1 signal 9 } 
	{ useConn_V_read sc_out sc_logic 1 signal 9 } 
	{ pkgWordCount_V_dout sc_in sc_lv 8 signal 10 } 
	{ pkgWordCount_V_empty_n sc_in sc_logic 1 signal 10 } 
	{ pkgWordCount_V_read sc_out sc_logic 1 signal 10 } 
	{ packetGap_V_dout sc_in sc_lv 8 signal 11 } 
	{ packetGap_V_empty_n sc_in sc_logic 1 signal 11 } 
	{ packetGap_V_read sc_out sc_logic 1 signal 11 } 
	{ timeInSeconds_V_dout sc_in sc_lv 32 signal 12 } 
	{ timeInSeconds_V_empty_n sc_in sc_logic 1 signal 12 } 
	{ timeInSeconds_V_read sc_out sc_logic 1 signal 12 } 
	{ regIpAddress0_V_dout sc_in sc_lv 32 signal 13 } 
	{ regIpAddress0_V_empty_n sc_in sc_logic 1 signal 13 } 
	{ regIpAddress0_V_read sc_out sc_logic 1 signal 13 } 
	{ regIpAddress1_V_dout sc_in sc_lv 32 signal 14 } 
	{ regIpAddress1_V_empty_n sc_in sc_logic 1 signal 14 } 
	{ regIpAddress1_V_read sc_out sc_logic 1 signal 14 } 
	{ regIpAddress2_V_dout sc_in sc_lv 32 signal 15 } 
	{ regIpAddress2_V_empty_n sc_in sc_logic 1 signal 15 } 
	{ regIpAddress2_V_read sc_out sc_logic 1 signal 15 } 
	{ regIpAddress3_V_dout sc_in sc_lv 32 signal 16 } 
	{ regIpAddress3_V_empty_n sc_in sc_logic 1 signal 16 } 
	{ regIpAddress3_V_read sc_out sc_logic 1 signal 16 } 
	{ regIpAddress4_V_dout sc_in sc_lv 32 signal 17 } 
	{ regIpAddress4_V_empty_n sc_in sc_logic 1 signal 17 } 
	{ regIpAddress4_V_read sc_out sc_logic 1 signal 17 } 
	{ regIpAddress5_V_dout sc_in sc_lv 32 signal 18 } 
	{ regIpAddress5_V_empty_n sc_in sc_logic 1 signal 18 } 
	{ regIpAddress5_V_read sc_out sc_logic 1 signal 18 } 
	{ regIpAddress6_V_dout sc_in sc_lv 32 signal 19 } 
	{ regIpAddress6_V_empty_n sc_in sc_logic 1 signal 19 } 
	{ regIpAddress6_V_read sc_out sc_logic 1 signal 19 } 
	{ regIpAddress7_V_dout sc_in sc_lv 32 signal 20 } 
	{ regIpAddress7_V_empty_n sc_in sc_logic 1 signal 20 } 
	{ regIpAddress7_V_read sc_out sc_logic 1 signal 20 } 
	{ regIpAddress8_V_dout sc_in sc_lv 32 signal 21 } 
	{ regIpAddress8_V_empty_n sc_in sc_logic 1 signal 21 } 
	{ regIpAddress8_V_read sc_out sc_logic 1 signal 21 } 
	{ regIpAddress9_V_dout sc_in sc_lv 32 signal 22 } 
	{ regIpAddress9_V_empty_n sc_in sc_logic 1 signal 22 } 
	{ regIpAddress9_V_read sc_out sc_logic 1 signal 22 } 
	{ txStatusBuffer_V_ses_dout sc_in sc_lv 16 signal 24 } 
	{ txStatusBuffer_V_ses_empty_n sc_in sc_logic 1 signal 24 } 
	{ txStatusBuffer_V_ses_read sc_out sc_logic 1 signal 24 } 
	{ txStatusBuffer_V_err_dout sc_in sc_lv 2 signal 25 } 
	{ txStatusBuffer_V_err_empty_n sc_in sc_logic 1 signal 25 } 
	{ txStatusBuffer_V_err_read sc_out sc_logic 1 signal 25 } 
	{ openConStatus_V_TVALID sc_in sc_logic 1 invld 1 } 
	{ startSignalFifo_V_din sc_out sc_lv 1 signal 23 } 
	{ startSignalFifo_V_full_n sc_in sc_logic 1 signal 23 } 
	{ startSignalFifo_V_write sc_out sc_logic 1 signal 23 } 
	{ stopSignalFifo_V_dout sc_in sc_lv 1 signal 26 } 
	{ stopSignalFifo_V_empty_n sc_in sc_logic 1 signal 26 } 
	{ stopSignalFifo_V_read sc_out sc_logic 1 signal 26 } 
	{ txMetaData_V_TREADY sc_in sc_logic 1 outacc 3 } 
	{ closeConnection_V_V_TREADY sc_in sc_logic 1 outacc 2 } 
	{ m_axis_tx_data_TREADY sc_in sc_logic 1 outacc 6 } 
	{ openConnection_V_TREADY sc_in sc_logic 1 outacc 0 } 
	{ openConnection_V_TDATA sc_out sc_lv 48 signal 0 } 
	{ openConnection_V_TVALID sc_out sc_logic 1 outvld 0 } 
	{ openConStatus_V_TDATA sc_in sc_lv 24 signal 1 } 
	{ openConStatus_V_TREADY sc_out sc_logic 1 inacc 1 } 
	{ closeConnection_V_V_TDATA sc_out sc_lv 16 signal 2 } 
	{ closeConnection_V_V_TVALID sc_out sc_logic 1 outvld 2 } 
	{ txMetaData_V_TDATA sc_out sc_lv 32 signal 3 } 
	{ txMetaData_V_TVALID sc_out sc_logic 1 outvld 3 } 
	{ m_axis_tx_data_TDATA sc_out sc_lv 64 signal 4 } 
	{ m_axis_tx_data_TVALID sc_out sc_logic 1 outvld 6 } 
	{ m_axis_tx_data_TKEEP sc_out sc_lv 8 signal 5 } 
	{ m_axis_tx_data_TLAST sc_out sc_lv 1 signal 6 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "runExperiment_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "runExperiment_V", "role": "dout" }} , 
 	{ "name": "runExperiment_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "runExperiment_V", "role": "empty_n" }} , 
 	{ "name": "runExperiment_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "runExperiment_V", "role": "read" }} , 
 	{ "name": "dualModeEn_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "dualModeEn_V", "role": "dout" }} , 
 	{ "name": "dualModeEn_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dualModeEn_V", "role": "empty_n" }} , 
 	{ "name": "dualModeEn_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dualModeEn_V", "role": "read" }} , 
 	{ "name": "useConn_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":14, "type": "signal", "bundle":{"name": "useConn_V", "role": "dout" }} , 
 	{ "name": "useConn_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "useConn_V", "role": "empty_n" }} , 
 	{ "name": "useConn_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "useConn_V", "role": "read" }} , 
 	{ "name": "pkgWordCount_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "pkgWordCount_V", "role": "dout" }} , 
 	{ "name": "pkgWordCount_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "pkgWordCount_V", "role": "empty_n" }} , 
 	{ "name": "pkgWordCount_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "pkgWordCount_V", "role": "read" }} , 
 	{ "name": "packetGap_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "packetGap_V", "role": "dout" }} , 
 	{ "name": "packetGap_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "packetGap_V", "role": "empty_n" }} , 
 	{ "name": "packetGap_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "packetGap_V", "role": "read" }} , 
 	{ "name": "timeInSeconds_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "timeInSeconds_V", "role": "dout" }} , 
 	{ "name": "timeInSeconds_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "timeInSeconds_V", "role": "empty_n" }} , 
 	{ "name": "timeInSeconds_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "timeInSeconds_V", "role": "read" }} , 
 	{ "name": "regIpAddress0_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress0_V", "role": "dout" }} , 
 	{ "name": "regIpAddress0_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress0_V", "role": "empty_n" }} , 
 	{ "name": "regIpAddress0_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress0_V", "role": "read" }} , 
 	{ "name": "regIpAddress1_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress1_V", "role": "dout" }} , 
 	{ "name": "regIpAddress1_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress1_V", "role": "empty_n" }} , 
 	{ "name": "regIpAddress1_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress1_V", "role": "read" }} , 
 	{ "name": "regIpAddress2_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress2_V", "role": "dout" }} , 
 	{ "name": "regIpAddress2_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress2_V", "role": "empty_n" }} , 
 	{ "name": "regIpAddress2_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress2_V", "role": "read" }} , 
 	{ "name": "regIpAddress3_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress3_V", "role": "dout" }} , 
 	{ "name": "regIpAddress3_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress3_V", "role": "empty_n" }} , 
 	{ "name": "regIpAddress3_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress3_V", "role": "read" }} , 
 	{ "name": "regIpAddress4_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress4_V", "role": "dout" }} , 
 	{ "name": "regIpAddress4_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress4_V", "role": "empty_n" }} , 
 	{ "name": "regIpAddress4_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress4_V", "role": "read" }} , 
 	{ "name": "regIpAddress5_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress5_V", "role": "dout" }} , 
 	{ "name": "regIpAddress5_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress5_V", "role": "empty_n" }} , 
 	{ "name": "regIpAddress5_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress5_V", "role": "read" }} , 
 	{ "name": "regIpAddress6_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress6_V", "role": "dout" }} , 
 	{ "name": "regIpAddress6_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress6_V", "role": "empty_n" }} , 
 	{ "name": "regIpAddress6_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress6_V", "role": "read" }} , 
 	{ "name": "regIpAddress7_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress7_V", "role": "dout" }} , 
 	{ "name": "regIpAddress7_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress7_V", "role": "empty_n" }} , 
 	{ "name": "regIpAddress7_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress7_V", "role": "read" }} , 
 	{ "name": "regIpAddress8_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress8_V", "role": "dout" }} , 
 	{ "name": "regIpAddress8_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress8_V", "role": "empty_n" }} , 
 	{ "name": "regIpAddress8_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress8_V", "role": "read" }} , 
 	{ "name": "regIpAddress9_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress9_V", "role": "dout" }} , 
 	{ "name": "regIpAddress9_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress9_V", "role": "empty_n" }} , 
 	{ "name": "regIpAddress9_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress9_V", "role": "read" }} , 
 	{ "name": "txStatusBuffer_V_ses_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "txStatusBuffer_V_ses", "role": "dout" }} , 
 	{ "name": "txStatusBuffer_V_ses_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txStatusBuffer_V_ses", "role": "empty_n" }} , 
 	{ "name": "txStatusBuffer_V_ses_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txStatusBuffer_V_ses", "role": "read" }} , 
 	{ "name": "txStatusBuffer_V_err_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "txStatusBuffer_V_err", "role": "dout" }} , 
 	{ "name": "txStatusBuffer_V_err_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txStatusBuffer_V_err", "role": "empty_n" }} , 
 	{ "name": "txStatusBuffer_V_err_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txStatusBuffer_V_err", "role": "read" }} , 
 	{ "name": "openConStatus_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "openConStatus_V", "role": "TVALID" }} , 
 	{ "name": "startSignalFifo_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "startSignalFifo_V", "role": "din" }} , 
 	{ "name": "startSignalFifo_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "startSignalFifo_V", "role": "full_n" }} , 
 	{ "name": "startSignalFifo_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "startSignalFifo_V", "role": "write" }} , 
 	{ "name": "stopSignalFifo_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "stopSignalFifo_V", "role": "dout" }} , 
 	{ "name": "stopSignalFifo_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "stopSignalFifo_V", "role": "empty_n" }} , 
 	{ "name": "stopSignalFifo_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "stopSignalFifo_V", "role": "read" }} , 
 	{ "name": "txMetaData_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "txMetaData_V", "role": "TREADY" }} , 
 	{ "name": "closeConnection_V_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "closeConnection_V_V", "role": "TREADY" }} , 
 	{ "name": "m_axis_tx_data_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "txData_V_last_V", "role": "READY" }} , 
 	{ "name": "openConnection_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "openConnection_V", "role": "TREADY" }} , 
 	{ "name": "openConnection_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":48, "type": "signal", "bundle":{"name": "openConnection_V", "role": "TDATA" }} , 
 	{ "name": "openConnection_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "openConnection_V", "role": "TVALID" }} , 
 	{ "name": "openConStatus_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":24, "type": "signal", "bundle":{"name": "openConStatus_V", "role": "TDATA" }} , 
 	{ "name": "openConStatus_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "openConStatus_V", "role": "TREADY" }} , 
 	{ "name": "closeConnection_V_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "closeConnection_V_V", "role": "TDATA" }} , 
 	{ "name": "closeConnection_V_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "closeConnection_V_V", "role": "TVALID" }} , 
 	{ "name": "txMetaData_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "txMetaData_V", "role": "TDATA" }} , 
 	{ "name": "txMetaData_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "txMetaData_V", "role": "TVALID" }} , 
 	{ "name": "m_axis_tx_data_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "txData_V_data_V", "role": "DATA" }} , 
 	{ "name": "m_axis_tx_data_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "txData_V_last_V", "role": "VALID" }} , 
 	{ "name": "m_axis_tx_data_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "txData_V_keep_V", "role": "KEEP" }} , 
 	{ "name": "m_axis_tx_data_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "txData_V_last_V", "role": "LAST" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2", "3", "4", "5", "6"],
		"CDFG" : "client_64_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "openConnection_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "openConnection_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "openConStatus_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "openConStatus_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "closeConnection_V_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "closeConnection_V_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txMetaData_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "txMetaData_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txData_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_tx_data_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txData_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "txData_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "runExperiment_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "runExperiment_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dualModeEn_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "dualModeEn_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "useConn_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "useConn_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "pkgWordCount_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "pkgWordCount_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "packetGap_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "packetGap_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "timeInSeconds_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "timeInSeconds_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress0_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress0_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress1_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress1_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress2_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress2_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress3_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress3_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress4_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress4_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress5_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress5_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress6_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress6_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress7_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress7_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress8_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress8_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress9_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress9_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "iperfFsmState", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "sessionIt_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "closeIt_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "numConnections_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "timeOver", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_idx", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "currentSessionID_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "wordCount_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ipAddressIdx_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "startSignalFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "startSignalFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "header_header_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "txStatusBuffer_V_ses", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txStatusBuffer_V_ses_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txStatusBuffer_V_err", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txStatusBuffer_V_err_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "packetGapCounter_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "stopSignalFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "stopSignalFifo_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_openConnection_V_U", "Parent" : "0"},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_closeConnection_V_V_U", "Parent" : "0"},
	{"ID" : "3", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_txMetaData_V_U", "Parent" : "0"},
	{"ID" : "4", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_txData_V_data_V_U", "Parent" : "0"},
	{"ID" : "5", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_txData_V_keep_V_U", "Parent" : "0"},
	{"ID" : "6", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_txData_V_last_V_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	client_64_s {
		openConnection_V {Type O LastRead -1 FirstWrite 1}
		openConStatus_V {Type I LastRead 0 FirstWrite -1}
		closeConnection_V_V {Type O LastRead -1 FirstWrite 1}
		txMetaData_V {Type O LastRead -1 FirstWrite 1}
		txData_V_data_V {Type O LastRead -1 FirstWrite 1}
		txData_V_keep_V {Type O LastRead -1 FirstWrite 1}
		txData_V_last_V {Type O LastRead -1 FirstWrite 1}
		runExperiment_V {Type I LastRead 0 FirstWrite -1}
		dualModeEn_V {Type I LastRead 0 FirstWrite -1}
		useConn_V {Type I LastRead 0 FirstWrite -1}
		pkgWordCount_V {Type I LastRead 0 FirstWrite -1}
		packetGap_V {Type I LastRead 0 FirstWrite -1}
		timeInSeconds_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress0_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress1_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress2_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress3_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress4_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress5_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress6_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress7_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress8_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress9_V {Type I LastRead 0 FirstWrite -1}
		iperfFsmState {Type IO LastRead -1 FirstWrite -1}
		sessionIt_V {Type IO LastRead -1 FirstWrite -1}
		closeIt_V {Type IO LastRead -1 FirstWrite -1}
		numConnections_V {Type IO LastRead -1 FirstWrite -1}
		timeOver {Type IO LastRead -1 FirstWrite -1}
		header_idx {Type IO LastRead -1 FirstWrite -1}
		currentSessionID_V {Type IO LastRead -1 FirstWrite -1}
		wordCount_V {Type IO LastRead -1 FirstWrite -1}
		ipAddressIdx_V {Type IO LastRead -1 FirstWrite -1}
		startSignalFifo_V {Type O LastRead -1 FirstWrite 1}
		header_header_V {Type IO LastRead -1 FirstWrite -1}
		txStatusBuffer_V_ses {Type I LastRead 0 FirstWrite -1}
		txStatusBuffer_V_err {Type I LastRead 0 FirstWrite -1}
		packetGapCounter_V {Type IO LastRead -1 FirstWrite -1}
		stopSignalFifo_V {Type I LastRead 1 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "2", "Max" : "2"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	openConnection_V { axis {  { openConnection_V_TREADY out_acc 0 1 }  { openConnection_V_TDATA out_data 1 48 }  { openConnection_V_TVALID out_vld 1 1 } } }
	openConStatus_V { axis {  { openConStatus_V_TVALID in_vld 0 1 }  { openConStatus_V_TDATA in_data 0 24 }  { openConStatus_V_TREADY in_acc 1 1 } } }
	closeConnection_V_V { axis {  { closeConnection_V_V_TREADY out_acc 0 1 }  { closeConnection_V_V_TDATA out_data 1 16 }  { closeConnection_V_V_TVALID out_vld 1 1 } } }
	txMetaData_V { axis {  { txMetaData_V_TREADY out_acc 0 1 }  { txMetaData_V_TDATA out_data 1 32 }  { txMetaData_V_TVALID out_vld 1 1 } } }
	txData_V_data_V { axis {  { m_axis_tx_data_TDATA out_data 1 64 } } }
	txData_V_keep_V { axis {  { m_axis_tx_data_TKEEP out_data 1 8 } } }
	txData_V_last_V { axis {  { m_axis_tx_data_TREADY out_acc 0 1 }  { m_axis_tx_data_TVALID out_vld 1 1 }  { m_axis_tx_data_TLAST out_data 1 1 } } }
	runExperiment_V { ap_fifo {  { runExperiment_V_dout fifo_data 0 1 }  { runExperiment_V_empty_n fifo_status 0 1 }  { runExperiment_V_read fifo_update 1 1 } } }
	dualModeEn_V { ap_fifo {  { dualModeEn_V_dout fifo_data 0 1 }  { dualModeEn_V_empty_n fifo_status 0 1 }  { dualModeEn_V_read fifo_update 1 1 } } }
	useConn_V { ap_fifo {  { useConn_V_dout fifo_data 0 14 }  { useConn_V_empty_n fifo_status 0 1 }  { useConn_V_read fifo_update 1 1 } } }
	pkgWordCount_V { ap_fifo {  { pkgWordCount_V_dout fifo_data 0 8 }  { pkgWordCount_V_empty_n fifo_status 0 1 }  { pkgWordCount_V_read fifo_update 1 1 } } }
	packetGap_V { ap_fifo {  { packetGap_V_dout fifo_data 0 8 }  { packetGap_V_empty_n fifo_status 0 1 }  { packetGap_V_read fifo_update 1 1 } } }
	timeInSeconds_V { ap_fifo {  { timeInSeconds_V_dout fifo_data 0 32 }  { timeInSeconds_V_empty_n fifo_status 0 1 }  { timeInSeconds_V_read fifo_update 1 1 } } }
	regIpAddress0_V { ap_fifo {  { regIpAddress0_V_dout fifo_data 0 32 }  { regIpAddress0_V_empty_n fifo_status 0 1 }  { regIpAddress0_V_read fifo_update 1 1 } } }
	regIpAddress1_V { ap_fifo {  { regIpAddress1_V_dout fifo_data 0 32 }  { regIpAddress1_V_empty_n fifo_status 0 1 }  { regIpAddress1_V_read fifo_update 1 1 } } }
	regIpAddress2_V { ap_fifo {  { regIpAddress2_V_dout fifo_data 0 32 }  { regIpAddress2_V_empty_n fifo_status 0 1 }  { regIpAddress2_V_read fifo_update 1 1 } } }
	regIpAddress3_V { ap_fifo {  { regIpAddress3_V_dout fifo_data 0 32 }  { regIpAddress3_V_empty_n fifo_status 0 1 }  { regIpAddress3_V_read fifo_update 1 1 } } }
	regIpAddress4_V { ap_fifo {  { regIpAddress4_V_dout fifo_data 0 32 }  { regIpAddress4_V_empty_n fifo_status 0 1 }  { regIpAddress4_V_read fifo_update 1 1 } } }
	regIpAddress5_V { ap_fifo {  { regIpAddress5_V_dout fifo_data 0 32 }  { regIpAddress5_V_empty_n fifo_status 0 1 }  { regIpAddress5_V_read fifo_update 1 1 } } }
	regIpAddress6_V { ap_fifo {  { regIpAddress6_V_dout fifo_data 0 32 }  { regIpAddress6_V_empty_n fifo_status 0 1 }  { regIpAddress6_V_read fifo_update 1 1 } } }
	regIpAddress7_V { ap_fifo {  { regIpAddress7_V_dout fifo_data 0 32 }  { regIpAddress7_V_empty_n fifo_status 0 1 }  { regIpAddress7_V_read fifo_update 1 1 } } }
	regIpAddress8_V { ap_fifo {  { regIpAddress8_V_dout fifo_data 0 32 }  { regIpAddress8_V_empty_n fifo_status 0 1 }  { regIpAddress8_V_read fifo_update 1 1 } } }
	regIpAddress9_V { ap_fifo {  { regIpAddress9_V_dout fifo_data 0 32 }  { regIpAddress9_V_empty_n fifo_status 0 1 }  { regIpAddress9_V_read fifo_update 1 1 } } }
	startSignalFifo_V { ap_fifo {  { startSignalFifo_V_din fifo_data 1 1 }  { startSignalFifo_V_full_n fifo_status 0 1 }  { startSignalFifo_V_write fifo_update 1 1 } } }
	txStatusBuffer_V_ses { ap_fifo {  { txStatusBuffer_V_ses_dout fifo_data 0 16 }  { txStatusBuffer_V_ses_empty_n fifo_status 0 1 }  { txStatusBuffer_V_ses_read fifo_update 1 1 } } }
	txStatusBuffer_V_err { ap_fifo {  { txStatusBuffer_V_err_dout fifo_data 0 2 }  { txStatusBuffer_V_err_empty_n fifo_status 0 1 }  { txStatusBuffer_V_err_read fifo_update 1 1 } } }
	stopSignalFifo_V { ap_fifo {  { stopSignalFifo_V_dout fifo_data 0 1 }  { stopSignalFifo_V_empty_n fifo_status 0 1 }  { stopSignalFifo_V_read fifo_update 1 1 } } }
}
