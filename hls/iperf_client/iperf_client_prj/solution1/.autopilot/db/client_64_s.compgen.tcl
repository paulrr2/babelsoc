# This script segment is generated automatically by AutoPilot

# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# Native AXIS:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::native_axis_add] == "::AESL_LIB_XILADAPTER::native_axis_add"} {
eval "::AESL_LIB_XILADAPTER::native_axis_add { \
    id 72 \
    name openConnection_V \
    reset_level 1 \
    sync_rst true \
    corename {} \
    metadata {  } \
    op interface \
    ports { openConnection_V_TREADY { I 1 bit } openConnection_V_TDATA { O 48 vector } openConnection_V_TVALID { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'openConnection_V'"
}
}


# Native AXIS:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::native_axis_add] == "::AESL_LIB_XILADAPTER::native_axis_add"} {
eval "::AESL_LIB_XILADAPTER::native_axis_add { \
    id 73 \
    name openConStatus_V \
    reset_level 1 \
    sync_rst true \
    corename {} \
    metadata {  } \
    op interface \
    ports { openConStatus_V_TVALID { I 1 bit } openConStatus_V_TDATA { I 24 vector } openConStatus_V_TREADY { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'openConStatus_V'"
}
}


# Native AXIS:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::native_axis_add] == "::AESL_LIB_XILADAPTER::native_axis_add"} {
eval "::AESL_LIB_XILADAPTER::native_axis_add { \
    id 74 \
    name closeConnection_V_V \
    reset_level 1 \
    sync_rst true \
    corename {} \
    metadata {  } \
    op interface \
    ports { closeConnection_V_V_TREADY { I 1 bit } closeConnection_V_V_TDATA { O 16 vector } closeConnection_V_V_TVALID { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'closeConnection_V_V'"
}
}


# Native AXIS:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::native_axis_add] == "::AESL_LIB_XILADAPTER::native_axis_add"} {
eval "::AESL_LIB_XILADAPTER::native_axis_add { \
    id 75 \
    name txMetaData_V \
    reset_level 1 \
    sync_rst true \
    corename {} \
    metadata {  } \
    op interface \
    ports { txMetaData_V_TREADY { I 1 bit } txMetaData_V_TDATA { O 32 vector } txMetaData_V_TVALID { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'txMetaData_V'"
}
}


# Native AXIS:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::native_axis_add] == "::AESL_LIB_XILADAPTER::native_axis_add"} {
eval "::AESL_LIB_XILADAPTER::native_axis_add { \
    id 76 \
    name txData_V_data_V \
    reset_level 1 \
    sync_rst true \
    corename {m_axis_tx_data} \
    metadata {  } \
    op interface \
    ports { m_axis_tx_data_TDATA { O 64 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'txData_V_data_V'"
}
}


# Native AXIS:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::native_axis_add] == "::AESL_LIB_XILADAPTER::native_axis_add"} {
eval "::AESL_LIB_XILADAPTER::native_axis_add { \
    id 77 \
    name txData_V_keep_V \
    reset_level 1 \
    sync_rst true \
    corename {m_axis_tx_data} \
    metadata {  } \
    op interface \
    ports { m_axis_tx_data_TKEEP { O 8 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'txData_V_keep_V'"
}
}


# Native AXIS:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::native_axis_add] == "::AESL_LIB_XILADAPTER::native_axis_add"} {
eval "::AESL_LIB_XILADAPTER::native_axis_add { \
    id 78 \
    name txData_V_last_V \
    reset_level 1 \
    sync_rst true \
    corename {m_axis_tx_data} \
    metadata {  } \
    op interface \
    ports { m_axis_tx_data_TREADY { I 1 bit } m_axis_tx_data_TVALID { O 1 bit } m_axis_tx_data_TLAST { O 1 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'txData_V_last_V'"
}
}


# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 79 \
    name runExperiment_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_runExperiment_V \
    op interface \
    ports { runExperiment_V_dout { I 1 vector } runExperiment_V_empty_n { I 1 bit } runExperiment_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 80 \
    name dualModeEn_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dualModeEn_V \
    op interface \
    ports { dualModeEn_V_dout { I 1 vector } dualModeEn_V_empty_n { I 1 bit } dualModeEn_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 81 \
    name useConn_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_useConn_V \
    op interface \
    ports { useConn_V_dout { I 14 vector } useConn_V_empty_n { I 1 bit } useConn_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 82 \
    name pkgWordCount_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_pkgWordCount_V \
    op interface \
    ports { pkgWordCount_V_dout { I 8 vector } pkgWordCount_V_empty_n { I 1 bit } pkgWordCount_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 83 \
    name packetGap_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_packetGap_V \
    op interface \
    ports { packetGap_V_dout { I 8 vector } packetGap_V_empty_n { I 1 bit } packetGap_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 84 \
    name timeInSeconds_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_timeInSeconds_V \
    op interface \
    ports { timeInSeconds_V_dout { I 32 vector } timeInSeconds_V_empty_n { I 1 bit } timeInSeconds_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 85 \
    name regIpAddress0_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress0_V \
    op interface \
    ports { regIpAddress0_V_dout { I 32 vector } regIpAddress0_V_empty_n { I 1 bit } regIpAddress0_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 86 \
    name regIpAddress1_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress1_V \
    op interface \
    ports { regIpAddress1_V_dout { I 32 vector } regIpAddress1_V_empty_n { I 1 bit } regIpAddress1_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 87 \
    name regIpAddress2_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress2_V \
    op interface \
    ports { regIpAddress2_V_dout { I 32 vector } regIpAddress2_V_empty_n { I 1 bit } regIpAddress2_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 88 \
    name regIpAddress3_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress3_V \
    op interface \
    ports { regIpAddress3_V_dout { I 32 vector } regIpAddress3_V_empty_n { I 1 bit } regIpAddress3_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 89 \
    name regIpAddress4_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress4_V \
    op interface \
    ports { regIpAddress4_V_dout { I 32 vector } regIpAddress4_V_empty_n { I 1 bit } regIpAddress4_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 90 \
    name regIpAddress5_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress5_V \
    op interface \
    ports { regIpAddress5_V_dout { I 32 vector } regIpAddress5_V_empty_n { I 1 bit } regIpAddress5_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 91 \
    name regIpAddress6_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress6_V \
    op interface \
    ports { regIpAddress6_V_dout { I 32 vector } regIpAddress6_V_empty_n { I 1 bit } regIpAddress6_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 92 \
    name regIpAddress7_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress7_V \
    op interface \
    ports { regIpAddress7_V_dout { I 32 vector } regIpAddress7_V_empty_n { I 1 bit } regIpAddress7_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 93 \
    name regIpAddress8_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress8_V \
    op interface \
    ports { regIpAddress8_V_dout { I 32 vector } regIpAddress8_V_empty_n { I 1 bit } regIpAddress8_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 94 \
    name regIpAddress9_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_regIpAddress9_V \
    op interface \
    ports { regIpAddress9_V_dout { I 32 vector } regIpAddress9_V_empty_n { I 1 bit } regIpAddress9_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 95 \
    name startSignalFifo_V \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_startSignalFifo_V \
    op interface \
    ports { startSignalFifo_V_din { O 1 vector } startSignalFifo_V_full_n { I 1 bit } startSignalFifo_V_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 96 \
    name txStatusBuffer_V_ses \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_txStatusBuffer_V_ses \
    op interface \
    ports { txStatusBuffer_V_ses_dout { I 16 vector } txStatusBuffer_V_ses_empty_n { I 1 bit } txStatusBuffer_V_ses_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 97 \
    name txStatusBuffer_V_err \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_txStatusBuffer_V_err \
    op interface \
    ports { txStatusBuffer_V_err_dout { I 2 vector } txStatusBuffer_V_err_empty_n { I 1 bit } txStatusBuffer_V_err_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 98 \
    name stopSignalFifo_V \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_stopSignalFifo_V \
    op interface \
    ports { stopSignalFifo_V_dout { I 1 vector } stopSignalFifo_V_empty_n { I 1 bit } stopSignalFifo_V_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_start { I 1 bit } ap_ready { O 1 bit } ap_done { O 1 bit } ap_idle { O 1 bit } ap_continue { I 1 bit } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


# RegSlice definition:
set ID 99
set RegSliceName regslice_core
set RegSliceInstName regslice_core_U
set CoreName ap_simcore_regslice_core
if {${::AESL::PGuard_rtl_comp_handler}} {
	::AP::rtl_comp_handler $RegSliceName
}


if {${::AESL::PGuard_autocg_gen} && ${::AESL::PGuard_autocg_ipmgen}} {
if {[info proc ::AESL_LIB_VIRTEX::xil_gen_regSlice] == "::AESL_LIB_VIRTEX::xil_gen_regSlice"} {
eval "::AESL_LIB_VIRTEX::xil_gen_regSlice { \
    name ${RegSliceName} \
}"
} else {
puts "@W \[IMPL-107\] Cannot find ::AESL_LIB_VIRTEX::xil_gen_regSlice, check your platform lib"
}
}


