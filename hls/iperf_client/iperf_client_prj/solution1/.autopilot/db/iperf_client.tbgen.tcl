set moduleName iperf_client
set isTopModule 1
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type dataflow
set FunctionProtocol ap_ctrl_none
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {iperf_client}
set C_modelType { void 0 }
set C_modelArgList {
	{ m_axis_listen_port_V_V int 16 regular {axi_s 1 volatile  { m_axis_listen_port_V_V Data } }  }
	{ s_axis_listen_port_status_V int 8 regular {axi_s 0 volatile  { s_axis_listen_port_status_V Data } }  }
	{ s_axis_notifications_V int 88 regular {axi_s 0 volatile  { s_axis_notifications_V Data } }  }
	{ m_axis_read_package_V int 32 regular {axi_s 1 volatile  { m_axis_read_package_V Data } }  }
	{ s_axis_rx_metadata_V_V int 16 regular {axi_s 0 volatile  { s_axis_rx_metadata_V_V Data } }  }
	{ s_axis_rx_data_V_data_V int 64 regular {axi_s 0 volatile  { s_axis_rx_data Data } }  }
	{ s_axis_rx_data_V_keep_V int 8 regular {axi_s 0 volatile  { s_axis_rx_data Keep } }  }
	{ s_axis_rx_data_V_last_V int 1 regular {axi_s 0 volatile  { s_axis_rx_data Last } }  }
	{ m_axis_open_connection_V int 48 regular {axi_s 1 volatile  { m_axis_open_connection_V Data } }  }
	{ s_axis_open_status_V int 24 regular {axi_s 0 volatile  { s_axis_open_status_V Data } }  }
	{ m_axis_close_connection_V_V int 16 regular {axi_s 1 volatile  { m_axis_close_connection_V_V Data } }  }
	{ m_axis_tx_metadata_V int 32 regular {axi_s 1 volatile  { m_axis_tx_metadata_V Data } }  }
	{ m_axis_tx_data_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_tx_data Data } }  }
	{ m_axis_tx_data_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_tx_data Keep } }  }
	{ m_axis_tx_data_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_tx_data Last } }  }
	{ s_axis_tx_status_V int 64 regular {axi_s 0 volatile  { s_axis_tx_status_V Data } }  }
	{ runExperiment_V int 1 regular  }
	{ dualModeEn_V int 1 regular  }
	{ useConn_V int 14 regular  }
	{ pkgWordCount_V int 8 regular  }
	{ packetGap_V int 8 regular  }
	{ timeInSeconds_V int 32 regular  }
	{ timeInCycles_V int 64 regular  }
	{ regIpAddress0_V int 32 regular  }
	{ regIpAddress1_V int 32 regular  }
	{ regIpAddress2_V int 32 regular  }
	{ regIpAddress3_V int 32 regular  }
	{ regIpAddress4_V int 32 regular  }
	{ regIpAddress5_V int 32 regular  }
	{ regIpAddress6_V int 32 regular  }
	{ regIpAddress7_V int 32 regular  }
	{ regIpAddress8_V int 32 regular  }
	{ regIpAddress9_V int 32 regular  }
}
set C_modelArgMapList {[ 
	{ "Name" : "m_axis_listen_port_V_V", "interface" : "axis", "bitwidth" : 16, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":15,"cElement": [{"cName": "m_axis_listen_port.V.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_listen_port_status_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "s_axis_listen_port_status.V","cData": "bool","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_notifications_V", "interface" : "axis", "bitwidth" : 88, "direction" : "READONLY", "bitSlice":[{"low":0,"up":15,"cElement": [{"cName": "s_axis_notifications.V.sessionID.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":16,"up":31,"cElement": [{"cName": "s_axis_notifications.V.length.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":32,"up":63,"cElement": [{"cName": "s_axis_notifications.V.ipAddress.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":64,"up":79,"cElement": [{"cName": "s_axis_notifications.V.dstPort.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":80,"up":80,"cElement": [{"cName": "s_axis_notifications.V.closed","cData": "bool","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_read_package_V", "interface" : "axis", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":15,"cElement": [{"cName": "m_axis_read_package.V.sessionID.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":16,"up":31,"cElement": [{"cName": "m_axis_read_package.V.length.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_rx_metadata_V_V", "interface" : "axis", "bitwidth" : 16, "direction" : "READONLY", "bitSlice":[{"low":0,"up":15,"cElement": [{"cName": "s_axis_rx_metadata.V.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_rx_data_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "READONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "s_axis_rx_data.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_rx_data_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "s_axis_rx_data.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_rx_data_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "s_axis_rx_data.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_open_connection_V", "interface" : "axis", "bitwidth" : 48, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "m_axis_open_connection.V.ip_address.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":32,"up":47,"cElement": [{"cName": "m_axis_open_connection.V.ip_port.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_open_status_V", "interface" : "axis", "bitwidth" : 24, "direction" : "READONLY", "bitSlice":[{"low":0,"up":15,"cElement": [{"cName": "s_axis_open_status.V.sessionID.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":16,"up":16,"cElement": [{"cName": "s_axis_open_status.V.success","cData": "bool","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_close_connection_V_V", "interface" : "axis", "bitwidth" : 16, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":15,"cElement": [{"cName": "m_axis_close_connection.V.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_tx_metadata_V", "interface" : "axis", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":15,"cElement": [{"cName": "m_axis_tx_metadata.V.sessionID.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":16,"up":31,"cElement": [{"cName": "m_axis_tx_metadata.V.length.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_tx_data_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "m_axis_tx_data.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_tx_data_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "m_axis_tx_data.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_tx_data_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "m_axis_tx_data.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_tx_status_V", "interface" : "axis", "bitwidth" : 64, "direction" : "READONLY", "bitSlice":[{"low":0,"up":15,"cElement": [{"cName": "s_axis_tx_status.V.sessionID.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":16,"up":31,"cElement": [{"cName": "s_axis_tx_status.V.length.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":32,"up":61,"cElement": [{"cName": "s_axis_tx_status.V.remaining_space.V","cData": "uint30","bit_use": { "low": 0,"up": 29},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]},{"low":62,"up":63,"cElement": [{"cName": "s_axis_tx_status.V.error.V","cData": "uint2","bit_use": { "low": 0,"up": 1},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "runExperiment_V", "interface" : "wire", "bitwidth" : 1, "direction" : "READONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "runExperiment.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}]} , 
 	{ "Name" : "dualModeEn_V", "interface" : "wire", "bitwidth" : 1, "direction" : "READONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "dualModeEn.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}]} , 
 	{ "Name" : "useConn_V", "interface" : "wire", "bitwidth" : 14, "direction" : "READONLY", "bitSlice":[{"low":0,"up":13,"cElement": [{"cName": "useConn.V","cData": "uint14","bit_use": { "low": 0,"up": 13},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}]} , 
 	{ "Name" : "pkgWordCount_V", "interface" : "wire", "bitwidth" : 8, "direction" : "READONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "pkgWordCount.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}]} , 
 	{ "Name" : "packetGap_V", "interface" : "wire", "bitwidth" : 8, "direction" : "READONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "packetGap.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}]} , 
 	{ "Name" : "timeInSeconds_V", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "timeInSeconds.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}]} , 
 	{ "Name" : "timeInCycles_V", "interface" : "wire", "bitwidth" : 64, "direction" : "READONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "timeInCycles.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}]} , 
 	{ "Name" : "regIpAddress0_V", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "regIpAddress0.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}]} , 
 	{ "Name" : "regIpAddress1_V", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "regIpAddress1.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}]} , 
 	{ "Name" : "regIpAddress2_V", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "regIpAddress2.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}]} , 
 	{ "Name" : "regIpAddress3_V", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "regIpAddress3.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}]} , 
 	{ "Name" : "regIpAddress4_V", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "regIpAddress4.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}]} , 
 	{ "Name" : "regIpAddress5_V", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "regIpAddress5.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}]} , 
 	{ "Name" : "regIpAddress6_V", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "regIpAddress6.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}]} , 
 	{ "Name" : "regIpAddress7_V", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "regIpAddress7.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}]} , 
 	{ "Name" : "regIpAddress8_V", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "regIpAddress8.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}]} , 
 	{ "Name" : "regIpAddress9_V", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "regIpAddress9.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}]} ]}
# RTL Port declarations: 
set portNum 59
set portList { 
	{ m_axis_listen_port_V_V_TDATA sc_out sc_lv 16 signal 0 } 
	{ s_axis_listen_port_status_V_TDATA sc_in sc_lv 8 signal 1 } 
	{ s_axis_notifications_V_TDATA sc_in sc_lv 88 signal 2 } 
	{ m_axis_read_package_V_TDATA sc_out sc_lv 32 signal 3 } 
	{ s_axis_rx_metadata_V_V_TDATA sc_in sc_lv 16 signal 4 } 
	{ s_axis_rx_data_TDATA sc_in sc_lv 64 signal 5 } 
	{ s_axis_rx_data_TKEEP sc_in sc_lv 8 signal 6 } 
	{ s_axis_rx_data_TLAST sc_in sc_lv 1 signal 7 } 
	{ m_axis_open_connection_V_TDATA sc_out sc_lv 48 signal 8 } 
	{ s_axis_open_status_V_TDATA sc_in sc_lv 24 signal 9 } 
	{ m_axis_close_connection_V_V_TDATA sc_out sc_lv 16 signal 10 } 
	{ m_axis_tx_metadata_V_TDATA sc_out sc_lv 32 signal 11 } 
	{ m_axis_tx_data_TDATA sc_out sc_lv 64 signal 12 } 
	{ m_axis_tx_data_TKEEP sc_out sc_lv 8 signal 13 } 
	{ m_axis_tx_data_TLAST sc_out sc_lv 1 signal 14 } 
	{ s_axis_tx_status_V_TDATA sc_in sc_lv 64 signal 15 } 
	{ runExperiment_V sc_in sc_lv 1 signal 16 } 
	{ dualModeEn_V sc_in sc_lv 1 signal 17 } 
	{ useConn_V sc_in sc_lv 14 signal 18 } 
	{ pkgWordCount_V sc_in sc_lv 8 signal 19 } 
	{ packetGap_V sc_in sc_lv 8 signal 20 } 
	{ timeInSeconds_V sc_in sc_lv 32 signal 21 } 
	{ timeInCycles_V sc_in sc_lv 64 signal 22 } 
	{ regIpAddress0_V sc_in sc_lv 32 signal 23 } 
	{ regIpAddress1_V sc_in sc_lv 32 signal 24 } 
	{ regIpAddress2_V sc_in sc_lv 32 signal 25 } 
	{ regIpAddress3_V sc_in sc_lv 32 signal 26 } 
	{ regIpAddress4_V sc_in sc_lv 32 signal 27 } 
	{ regIpAddress5_V sc_in sc_lv 32 signal 28 } 
	{ regIpAddress6_V sc_in sc_lv 32 signal 29 } 
	{ regIpAddress7_V sc_in sc_lv 32 signal 30 } 
	{ regIpAddress8_V sc_in sc_lv 32 signal 31 } 
	{ regIpAddress9_V sc_in sc_lv 32 signal 32 } 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst_n sc_in sc_logic 1 reset -1 active_low_sync } 
	{ s_axis_tx_status_V_TVALID sc_in sc_logic 1 invld 15 } 
	{ s_axis_tx_status_V_TREADY sc_out sc_logic 1 inacc 15 } 
	{ m_axis_open_connection_V_TVALID sc_out sc_logic 1 outvld 8 } 
	{ m_axis_open_connection_V_TREADY sc_in sc_logic 1 outacc 8 } 
	{ s_axis_open_status_V_TVALID sc_in sc_logic 1 invld 9 } 
	{ s_axis_open_status_V_TREADY sc_out sc_logic 1 inacc 9 } 
	{ m_axis_close_connection_V_V_TVALID sc_out sc_logic 1 outvld 10 } 
	{ m_axis_close_connection_V_V_TREADY sc_in sc_logic 1 outacc 10 } 
	{ m_axis_tx_metadata_V_TVALID sc_out sc_logic 1 outvld 11 } 
	{ m_axis_tx_metadata_V_TREADY sc_in sc_logic 1 outacc 11 } 
	{ m_axis_tx_data_TVALID sc_out sc_logic 1 outvld 14 } 
	{ m_axis_tx_data_TREADY sc_in sc_logic 1 outacc 14 } 
	{ m_axis_listen_port_V_V_TVALID sc_out sc_logic 1 outvld 0 } 
	{ m_axis_listen_port_V_V_TREADY sc_in sc_logic 1 outacc 0 } 
	{ s_axis_listen_port_status_V_TVALID sc_in sc_logic 1 invld 1 } 
	{ s_axis_listen_port_status_V_TREADY sc_out sc_logic 1 inacc 1 } 
	{ s_axis_notifications_V_TVALID sc_in sc_logic 1 invld 2 } 
	{ s_axis_notifications_V_TREADY sc_out sc_logic 1 inacc 2 } 
	{ m_axis_read_package_V_TVALID sc_out sc_logic 1 outvld 3 } 
	{ m_axis_read_package_V_TREADY sc_in sc_logic 1 outacc 3 } 
	{ s_axis_rx_metadata_V_V_TVALID sc_in sc_logic 1 invld 4 } 
	{ s_axis_rx_metadata_V_V_TREADY sc_out sc_logic 1 inacc 4 } 
	{ s_axis_rx_data_TVALID sc_in sc_logic 1 invld 7 } 
	{ s_axis_rx_data_TREADY sc_out sc_logic 1 inacc 7 } 
}
set NewPortList {[ 
	{ "name": "m_axis_listen_port_V_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "m_axis_listen_port_V_V", "role": "TDATA" }} , 
 	{ "name": "s_axis_listen_port_status_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "s_axis_listen_port_status_V", "role": "TDATA" }} , 
 	{ "name": "s_axis_notifications_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":88, "type": "signal", "bundle":{"name": "s_axis_notifications_V", "role": "TDATA" }} , 
 	{ "name": "m_axis_read_package_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "m_axis_read_package_V", "role": "TDATA" }} , 
 	{ "name": "s_axis_rx_metadata_V_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "s_axis_rx_metadata_V_V", "role": "TDATA" }} , 
 	{ "name": "s_axis_rx_data_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "s_axis_rx_data_V_data_V", "role": "default" }} , 
 	{ "name": "s_axis_rx_data_TKEEP", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "s_axis_rx_data_V_keep_V", "role": "default" }} , 
 	{ "name": "s_axis_rx_data_TLAST", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "s_axis_rx_data_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_open_connection_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":48, "type": "signal", "bundle":{"name": "m_axis_open_connection_V", "role": "TDATA" }} , 
 	{ "name": "s_axis_open_status_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":24, "type": "signal", "bundle":{"name": "s_axis_open_status_V", "role": "TDATA" }} , 
 	{ "name": "m_axis_close_connection_V_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "m_axis_close_connection_V_V", "role": "TDATA" }} , 
 	{ "name": "m_axis_tx_metadata_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "m_axis_tx_metadata_V", "role": "TDATA" }} , 
 	{ "name": "m_axis_tx_data_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "m_axis_tx_data_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_tx_data_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "m_axis_tx_data_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_tx_data_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "m_axis_tx_data_V_last_V", "role": "default" }} , 
 	{ "name": "s_axis_tx_status_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "s_axis_tx_status_V", "role": "TDATA" }} , 
 	{ "name": "runExperiment_V", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "runExperiment_V", "role": "default" }} , 
 	{ "name": "dualModeEn_V", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "dualModeEn_V", "role": "default" }} , 
 	{ "name": "useConn_V", "direction": "in", "datatype": "sc_lv", "bitwidth":14, "type": "signal", "bundle":{"name": "useConn_V", "role": "default" }} , 
 	{ "name": "pkgWordCount_V", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "pkgWordCount_V", "role": "default" }} , 
 	{ "name": "packetGap_V", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "packetGap_V", "role": "default" }} , 
 	{ "name": "timeInSeconds_V", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "timeInSeconds_V", "role": "default" }} , 
 	{ "name": "timeInCycles_V", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "timeInCycles_V", "role": "default" }} , 
 	{ "name": "regIpAddress0_V", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress0_V", "role": "default" }} , 
 	{ "name": "regIpAddress1_V", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress1_V", "role": "default" }} , 
 	{ "name": "regIpAddress2_V", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress2_V", "role": "default" }} , 
 	{ "name": "regIpAddress3_V", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress3_V", "role": "default" }} , 
 	{ "name": "regIpAddress4_V", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress4_V", "role": "default" }} , 
 	{ "name": "regIpAddress5_V", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress5_V", "role": "default" }} , 
 	{ "name": "regIpAddress6_V", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress6_V", "role": "default" }} , 
 	{ "name": "regIpAddress7_V", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress7_V", "role": "default" }} , 
 	{ "name": "regIpAddress8_V", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress8_V", "role": "default" }} , 
 	{ "name": "regIpAddress9_V", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress9_V", "role": "default" }} , 
 	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst_n", "role": "default" }} , 
 	{ "name": "s_axis_tx_status_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "s_axis_tx_status_V", "role": "TVALID" }} , 
 	{ "name": "s_axis_tx_status_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "s_axis_tx_status_V", "role": "TREADY" }} , 
 	{ "name": "m_axis_open_connection_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_open_connection_V", "role": "TVALID" }} , 
 	{ "name": "m_axis_open_connection_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_open_connection_V", "role": "TREADY" }} , 
 	{ "name": "s_axis_open_status_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "s_axis_open_status_V", "role": "TVALID" }} , 
 	{ "name": "s_axis_open_status_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "s_axis_open_status_V", "role": "TREADY" }} , 
 	{ "name": "m_axis_close_connection_V_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_close_connection_V_V", "role": "TVALID" }} , 
 	{ "name": "m_axis_close_connection_V_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_close_connection_V_V", "role": "TREADY" }} , 
 	{ "name": "m_axis_tx_metadata_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_tx_metadata_V", "role": "TVALID" }} , 
 	{ "name": "m_axis_tx_metadata_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_tx_metadata_V", "role": "TREADY" }} , 
 	{ "name": "m_axis_tx_data_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_tx_data_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_tx_data_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_tx_data_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_listen_port_V_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_listen_port_V_V", "role": "TVALID" }} , 
 	{ "name": "m_axis_listen_port_V_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_listen_port_V_V", "role": "TREADY" }} , 
 	{ "name": "s_axis_listen_port_status_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "s_axis_listen_port_status_V", "role": "TVALID" }} , 
 	{ "name": "s_axis_listen_port_status_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "s_axis_listen_port_status_V", "role": "TREADY" }} , 
 	{ "name": "s_axis_notifications_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "s_axis_notifications_V", "role": "TVALID" }} , 
 	{ "name": "s_axis_notifications_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "s_axis_notifications_V", "role": "TREADY" }} , 
 	{ "name": "m_axis_read_package_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_read_package_V", "role": "TVALID" }} , 
 	{ "name": "m_axis_read_package_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_read_package_V", "role": "TREADY" }} , 
 	{ "name": "s_axis_rx_metadata_V_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "s_axis_rx_metadata_V_V", "role": "TVALID" }} , 
 	{ "name": "s_axis_rx_metadata_V_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "s_axis_rx_metadata_V_V", "role": "TREADY" }} , 
 	{ "name": "s_axis_rx_data_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "s_axis_rx_data_V_last_V", "role": "default" }} , 
 	{ "name": "s_axis_rx_data_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "s_axis_rx_data_V_last_V", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2", "3", "4", "11", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52"],
		"CDFG" : "iperf_client",
		"Protocol" : "ap_ctrl_none",
		"ControlExist" : "0", "ap_start" : "0", "ap_ready" : "0", "ap_done" : "0", "ap_continue" : "0", "ap_idle" : "0",
		"Pipeline" : "Dataflow", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "1",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "5", "EstimateLatencyMax" : "5",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "1",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"InputProcess" : [
			{"ID" : "1", "Name" : "iperf_client_entry3_U0"},
			{"ID" : "3", "Name" : "status_handler_U0"},
			{"ID" : "4", "Name" : "client_64_U0"},
			{"ID" : "11", "Name" : "server_64_U0"}],
		"OutputProcess" : [
			{"ID" : "4", "Name" : "client_64_U0"},
			{"ID" : "11", "Name" : "server_64_U0"},
			{"ID" : "14", "Name" : "clock_U0"}],
		"Port" : [
			{"Name" : "m_axis_listen_port_V_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "11", "SubInstance" : "server_64_U0", "Port" : "listenPort_V_V"}]},
			{"Name" : "s_axis_listen_port_status_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "11", "SubInstance" : "server_64_U0", "Port" : "listenPortStatus_V"}]},
			{"Name" : "s_axis_notifications_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "11", "SubInstance" : "server_64_U0", "Port" : "notifications_V"}]},
			{"Name" : "m_axis_read_package_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "11", "SubInstance" : "server_64_U0", "Port" : "readRequest_V"}]},
			{"Name" : "s_axis_rx_metadata_V_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "11", "SubInstance" : "server_64_U0", "Port" : "rxMetaData_V_V"}]},
			{"Name" : "s_axis_rx_data_V_data_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "11", "SubInstance" : "server_64_U0", "Port" : "rxData_V_data_V"}]},
			{"Name" : "s_axis_rx_data_V_keep_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "11", "SubInstance" : "server_64_U0", "Port" : "rxData_V_keep_V"}]},
			{"Name" : "s_axis_rx_data_V_last_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "11", "SubInstance" : "server_64_U0", "Port" : "rxData_V_last_V"}]},
			{"Name" : "m_axis_open_connection_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "client_64_U0", "Port" : "openConnection_V"}]},
			{"Name" : "s_axis_open_status_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "client_64_U0", "Port" : "openConStatus_V"}]},
			{"Name" : "m_axis_close_connection_V_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "client_64_U0", "Port" : "closeConnection_V_V"}]},
			{"Name" : "m_axis_tx_metadata_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "client_64_U0", "Port" : "txMetaData_V"}]},
			{"Name" : "m_axis_tx_data_V_data_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "client_64_U0", "Port" : "txData_V_data_V"}]},
			{"Name" : "m_axis_tx_data_V_keep_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "client_64_U0", "Port" : "txData_V_keep_V"}]},
			{"Name" : "m_axis_tx_data_V_last_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "client_64_U0", "Port" : "txData_V_last_V"}]},
			{"Name" : "s_axis_tx_status_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "status_handler_U0", "Port" : "txStatus_V"}]},
			{"Name" : "runExperiment_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "dualModeEn_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "useConn_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "pkgWordCount_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "packetGap_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "timeInSeconds_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "timeInCycles_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "regIpAddress0_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "regIpAddress1_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "regIpAddress2_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "regIpAddress3_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "regIpAddress4_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "regIpAddress5_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "regIpAddress6_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "regIpAddress7_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "regIpAddress8_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "regIpAddress9_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "txStatusBuffer_V_ses", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "status_handler_U0", "Port" : "txStatusBuffer_V_ses"},
					{"ID" : "4", "SubInstance" : "client_64_U0", "Port" : "txStatusBuffer_V_ses"}]},
			{"Name" : "txStatusBuffer_V_err", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "status_handler_U0", "Port" : "txStatusBuffer_V_err"},
					{"ID" : "4", "SubInstance" : "client_64_U0", "Port" : "txStatusBuffer_V_err"}]},
			{"Name" : "iperfFsmState", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "client_64_U0", "Port" : "iperfFsmState"}]},
			{"Name" : "sessionIt_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "client_64_U0", "Port" : "sessionIt_V"}]},
			{"Name" : "closeIt_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "client_64_U0", "Port" : "closeIt_V"}]},
			{"Name" : "numConnections_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "client_64_U0", "Port" : "numConnections_V"}]},
			{"Name" : "timeOver", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "client_64_U0", "Port" : "timeOver"}]},
			{"Name" : "header_idx", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "client_64_U0", "Port" : "header_idx"}]},
			{"Name" : "currentSessionID_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "client_64_U0", "Port" : "currentSessionID_V"}]},
			{"Name" : "wordCount_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "client_64_U0", "Port" : "wordCount_V"}]},
			{"Name" : "ipAddressIdx_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "client_64_U0", "Port" : "ipAddressIdx_V"}]},
			{"Name" : "startSignalFifo_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "client_64_U0", "Port" : "startSignalFifo_V"},
					{"ID" : "14", "SubInstance" : "clock_U0", "Port" : "startSignalFifo_V"}]},
			{"Name" : "header_header_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "client_64_U0", "Port" : "header_header_V"}]},
			{"Name" : "packetGapCounter_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "client_64_U0", "Port" : "packetGapCounter_V"}]},
			{"Name" : "stopSignalFifo_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "client_64_U0", "Port" : "stopSignalFifo_V"},
					{"ID" : "14", "SubInstance" : "clock_U0", "Port" : "stopSignalFifo_V"}]},
			{"Name" : "listenState", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "11", "SubInstance" : "server_64_U0", "Port" : "listenState"}]},
			{"Name" : "serverFsmState", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "11", "SubInstance" : "server_64_U0", "Port" : "serverFsmState"}]},
			{"Name" : "sw_state", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "14", "SubInstance" : "clock_U0", "Port" : "sw_state"}]},
			{"Name" : "time_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "14", "SubInstance" : "clock_U0", "Port" : "time_V"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.iperf_client_entry3_U0", "Parent" : "0",
		"CDFG" : "iperf_client_entry3",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "0", "EstimateLatencyMin" : "0", "EstimateLatencyMax" : "0",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "0",
		"Port" : [
			{"Name" : "runExperiment_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "dualModeEn_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "useConn_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "pkgWordCount_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "packetGap_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "timeInSeconds_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "timeInCycles_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "regIpAddress0_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "regIpAddress1_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "regIpAddress2_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "regIpAddress3_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "regIpAddress4_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "regIpAddress5_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "regIpAddress6_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "regIpAddress7_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "regIpAddress8_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "regIpAddress9_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "runExperiment_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "2", "DependentChan" : "15",
				"BlockSignal" : [
					{"Name" : "runExperiment_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dualModeEn_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "2", "DependentChan" : "16",
				"BlockSignal" : [
					{"Name" : "dualModeEn_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "useConn_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "2", "DependentChan" : "17",
				"BlockSignal" : [
					{"Name" : "useConn_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "pkgWordCount_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "2", "DependentChan" : "18",
				"BlockSignal" : [
					{"Name" : "pkgWordCount_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "packetGap_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "2", "DependentChan" : "19",
				"BlockSignal" : [
					{"Name" : "packetGap_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "timeInSeconds_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "2", "DependentChan" : "20",
				"BlockSignal" : [
					{"Name" : "timeInSeconds_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "timeInCycles_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "2", "DependentChan" : "21",
				"BlockSignal" : [
					{"Name" : "timeInCycles_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress0_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "2", "DependentChan" : "22",
				"BlockSignal" : [
					{"Name" : "regIpAddress0_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress1_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "2", "DependentChan" : "23",
				"BlockSignal" : [
					{"Name" : "regIpAddress1_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress2_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "2", "DependentChan" : "24",
				"BlockSignal" : [
					{"Name" : "regIpAddress2_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress3_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "2", "DependentChan" : "25",
				"BlockSignal" : [
					{"Name" : "regIpAddress3_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress4_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "2", "DependentChan" : "26",
				"BlockSignal" : [
					{"Name" : "regIpAddress4_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress5_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "2", "DependentChan" : "27",
				"BlockSignal" : [
					{"Name" : "regIpAddress5_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress6_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "2", "DependentChan" : "28",
				"BlockSignal" : [
					{"Name" : "regIpAddress6_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress7_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "2", "DependentChan" : "29",
				"BlockSignal" : [
					{"Name" : "regIpAddress7_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress8_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "2", "DependentChan" : "30",
				"BlockSignal" : [
					{"Name" : "regIpAddress8_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress9_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "2", "DependentChan" : "31",
				"BlockSignal" : [
					{"Name" : "regIpAddress9_V_out_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.iperf_client_entry16_U0", "Parent" : "0",
		"CDFG" : "iperf_client_entry16",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "0", "EstimateLatencyMin" : "0", "EstimateLatencyMax" : "0",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "0",
		"Port" : [
			{"Name" : "runExperiment_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "1", "DependentChan" : "15",
				"BlockSignal" : [
					{"Name" : "runExperiment_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dualModeEn_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "1", "DependentChan" : "16",
				"BlockSignal" : [
					{"Name" : "dualModeEn_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "useConn_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "1", "DependentChan" : "17",
				"BlockSignal" : [
					{"Name" : "useConn_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "pkgWordCount_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "1", "DependentChan" : "18",
				"BlockSignal" : [
					{"Name" : "pkgWordCount_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "packetGap_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "1", "DependentChan" : "19",
				"BlockSignal" : [
					{"Name" : "packetGap_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "timeInSeconds_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "1", "DependentChan" : "20",
				"BlockSignal" : [
					{"Name" : "timeInSeconds_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "timeInCycles_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "1", "DependentChan" : "21",
				"BlockSignal" : [
					{"Name" : "timeInCycles_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress0_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "1", "DependentChan" : "22",
				"BlockSignal" : [
					{"Name" : "regIpAddress0_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress1_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "1", "DependentChan" : "23",
				"BlockSignal" : [
					{"Name" : "regIpAddress1_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress2_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "1", "DependentChan" : "24",
				"BlockSignal" : [
					{"Name" : "regIpAddress2_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress3_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "1", "DependentChan" : "25",
				"BlockSignal" : [
					{"Name" : "regIpAddress3_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress4_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "1", "DependentChan" : "26",
				"BlockSignal" : [
					{"Name" : "regIpAddress4_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress5_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "1", "DependentChan" : "27",
				"BlockSignal" : [
					{"Name" : "regIpAddress5_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress6_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "1", "DependentChan" : "28",
				"BlockSignal" : [
					{"Name" : "regIpAddress6_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress7_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "1", "DependentChan" : "29",
				"BlockSignal" : [
					{"Name" : "regIpAddress7_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress8_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "1", "DependentChan" : "30",
				"BlockSignal" : [
					{"Name" : "regIpAddress8_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress9_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "1", "DependentChan" : "31",
				"BlockSignal" : [
					{"Name" : "regIpAddress9_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "runExperiment_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "4", "DependentChan" : "32",
				"BlockSignal" : [
					{"Name" : "runExperiment_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dualModeEn_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "4", "DependentChan" : "33",
				"BlockSignal" : [
					{"Name" : "dualModeEn_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "useConn_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "4", "DependentChan" : "34",
				"BlockSignal" : [
					{"Name" : "useConn_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "pkgWordCount_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "4", "DependentChan" : "35",
				"BlockSignal" : [
					{"Name" : "pkgWordCount_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "packetGap_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "4", "DependentChan" : "36",
				"BlockSignal" : [
					{"Name" : "packetGap_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "timeInSeconds_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "4", "DependentChan" : "37",
				"BlockSignal" : [
					{"Name" : "timeInSeconds_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "timeInCycles_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "14", "DependentChan" : "38",
				"BlockSignal" : [
					{"Name" : "timeInCycles_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress0_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "4", "DependentChan" : "39",
				"BlockSignal" : [
					{"Name" : "regIpAddress0_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress1_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "4", "DependentChan" : "40",
				"BlockSignal" : [
					{"Name" : "regIpAddress1_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress2_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "4", "DependentChan" : "41",
				"BlockSignal" : [
					{"Name" : "regIpAddress2_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress3_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "4", "DependentChan" : "42",
				"BlockSignal" : [
					{"Name" : "regIpAddress3_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress4_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "4", "DependentChan" : "43",
				"BlockSignal" : [
					{"Name" : "regIpAddress4_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress5_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "4", "DependentChan" : "44",
				"BlockSignal" : [
					{"Name" : "regIpAddress5_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress6_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "4", "DependentChan" : "45",
				"BlockSignal" : [
					{"Name" : "regIpAddress6_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress7_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "4", "DependentChan" : "46",
				"BlockSignal" : [
					{"Name" : "regIpAddress7_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress8_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "4", "DependentChan" : "47",
				"BlockSignal" : [
					{"Name" : "regIpAddress8_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress9_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "4", "DependentChan" : "48",
				"BlockSignal" : [
					{"Name" : "regIpAddress9_V_out_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "3", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.status_handler_U0", "Parent" : "0",
		"CDFG" : "status_handler",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "txStatus_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "txStatus_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txStatusBuffer_V_ses", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "4", "DependentChan" : "49",
				"BlockSignal" : [
					{"Name" : "txStatusBuffer_V_ses_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txStatusBuffer_V_err", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "4", "DependentChan" : "50",
				"BlockSignal" : [
					{"Name" : "txStatusBuffer_V_err_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "4", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.client_64_U0", "Parent" : "0", "Child" : ["5", "6", "7", "8", "9", "10"],
		"CDFG" : "client_64_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "openConnection_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "openConnection_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "openConStatus_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "openConStatus_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "closeConnection_V_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "closeConnection_V_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txMetaData_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "txMetaData_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txData_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_tx_data_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txData_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "txData_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "runExperiment_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "32",
				"BlockSignal" : [
					{"Name" : "runExperiment_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dualModeEn_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "33",
				"BlockSignal" : [
					{"Name" : "dualModeEn_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "useConn_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "34",
				"BlockSignal" : [
					{"Name" : "useConn_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "pkgWordCount_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "35",
				"BlockSignal" : [
					{"Name" : "pkgWordCount_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "packetGap_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "36",
				"BlockSignal" : [
					{"Name" : "packetGap_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "timeInSeconds_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "37",
				"BlockSignal" : [
					{"Name" : "timeInSeconds_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress0_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "39",
				"BlockSignal" : [
					{"Name" : "regIpAddress0_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress1_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "40",
				"BlockSignal" : [
					{"Name" : "regIpAddress1_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress2_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "41",
				"BlockSignal" : [
					{"Name" : "regIpAddress2_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress3_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "42",
				"BlockSignal" : [
					{"Name" : "regIpAddress3_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress4_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "43",
				"BlockSignal" : [
					{"Name" : "regIpAddress4_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress5_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "44",
				"BlockSignal" : [
					{"Name" : "regIpAddress5_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress6_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "45",
				"BlockSignal" : [
					{"Name" : "regIpAddress6_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress7_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "46",
				"BlockSignal" : [
					{"Name" : "regIpAddress7_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress8_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "47",
				"BlockSignal" : [
					{"Name" : "regIpAddress8_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress9_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "48",
				"BlockSignal" : [
					{"Name" : "regIpAddress9_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "iperfFsmState", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "sessionIt_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "closeIt_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "numConnections_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "timeOver", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_idx", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "currentSessionID_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "wordCount_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ipAddressIdx_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "startSignalFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "14", "DependentChan" : "51",
				"BlockSignal" : [
					{"Name" : "startSignalFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "header_header_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "txStatusBuffer_V_ses", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "3", "DependentChan" : "49",
				"BlockSignal" : [
					{"Name" : "txStatusBuffer_V_ses_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txStatusBuffer_V_err", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "3", "DependentChan" : "50",
				"BlockSignal" : [
					{"Name" : "txStatusBuffer_V_err_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "packetGapCounter_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "stopSignalFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "14", "DependentChan" : "52",
				"BlockSignal" : [
					{"Name" : "stopSignalFifo_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "5", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.client_64_U0.regslice_both_openConnection_V_U", "Parent" : "4"},
	{"ID" : "6", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.client_64_U0.regslice_both_closeConnection_V_V_U", "Parent" : "4"},
	{"ID" : "7", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.client_64_U0.regslice_both_txMetaData_V_U", "Parent" : "4"},
	{"ID" : "8", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.client_64_U0.regslice_both_txData_V_data_V_U", "Parent" : "4"},
	{"ID" : "9", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.client_64_U0.regslice_both_txData_V_keep_V_U", "Parent" : "4"},
	{"ID" : "10", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.client_64_U0.regslice_both_txData_V_last_V_U", "Parent" : "4"},
	{"ID" : "11", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.server_64_U0", "Parent" : "0", "Child" : ["12", "13"],
		"CDFG" : "server_64_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "listenPort_V_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "listenPort_V_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "listenPortStatus_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "listenPortStatus_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "notifications_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "notifications_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "readRequest_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "readRequest_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxMetaData_V_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "rxMetaData_V_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxData_V_data_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "s_axis_rx_data_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rxData_V_keep_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "rxData_V_last_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "listenState", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "serverFsmState", "Type" : "OVld", "Direction" : "IO"}]},
	{"ID" : "12", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.server_64_U0.regslice_both_listenPort_V_V_U", "Parent" : "11"},
	{"ID" : "13", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.server_64_U0.regslice_both_readRequest_V_U", "Parent" : "11"},
	{"ID" : "14", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.clock_U0", "Parent" : "0",
		"CDFG" : "clock",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "timeInCycles_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "38",
				"BlockSignal" : [
					{"Name" : "timeInCycles_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "sw_state", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "time_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "startSignalFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "4", "DependentChan" : "51",
				"BlockSignal" : [
					{"Name" : "startSignalFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "stopSignalFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "4", "DependentChan" : "52",
				"BlockSignal" : [
					{"Name" : "stopSignalFifo_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "15", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.runExperiment_V_c1_U", "Parent" : "0"},
	{"ID" : "16", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.dualModeEn_V_c2_U", "Parent" : "0"},
	{"ID" : "17", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.useConn_V_c3_U", "Parent" : "0"},
	{"ID" : "18", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.pkgWordCount_V_c4_U", "Parent" : "0"},
	{"ID" : "19", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.packetGap_V_c5_U", "Parent" : "0"},
	{"ID" : "20", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.timeInSeconds_V_c6_U", "Parent" : "0"},
	{"ID" : "21", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.timeInCycles_V_c7_U", "Parent" : "0"},
	{"ID" : "22", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regIpAddress0_V_c8_U", "Parent" : "0"},
	{"ID" : "23", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regIpAddress1_V_c9_U", "Parent" : "0"},
	{"ID" : "24", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regIpAddress2_V_c10_U", "Parent" : "0"},
	{"ID" : "25", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regIpAddress3_V_c11_U", "Parent" : "0"},
	{"ID" : "26", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regIpAddress4_V_c12_U", "Parent" : "0"},
	{"ID" : "27", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regIpAddress5_V_c13_U", "Parent" : "0"},
	{"ID" : "28", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regIpAddress6_V_c14_U", "Parent" : "0"},
	{"ID" : "29", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regIpAddress7_V_c15_U", "Parent" : "0"},
	{"ID" : "30", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regIpAddress8_V_c16_U", "Parent" : "0"},
	{"ID" : "31", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regIpAddress9_V_c17_U", "Parent" : "0"},
	{"ID" : "32", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.runExperiment_V_c_U", "Parent" : "0"},
	{"ID" : "33", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.dualModeEn_V_c_U", "Parent" : "0"},
	{"ID" : "34", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.useConn_V_c_U", "Parent" : "0"},
	{"ID" : "35", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.pkgWordCount_V_c_U", "Parent" : "0"},
	{"ID" : "36", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.packetGap_V_c_U", "Parent" : "0"},
	{"ID" : "37", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.timeInSeconds_V_c_U", "Parent" : "0"},
	{"ID" : "38", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.timeInCycles_V_c_U", "Parent" : "0"},
	{"ID" : "39", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regIpAddress0_V_c_U", "Parent" : "0"},
	{"ID" : "40", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regIpAddress1_V_c_U", "Parent" : "0"},
	{"ID" : "41", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regIpAddress2_V_c_U", "Parent" : "0"},
	{"ID" : "42", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regIpAddress3_V_c_U", "Parent" : "0"},
	{"ID" : "43", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regIpAddress4_V_c_U", "Parent" : "0"},
	{"ID" : "44", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regIpAddress5_V_c_U", "Parent" : "0"},
	{"ID" : "45", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regIpAddress6_V_c_U", "Parent" : "0"},
	{"ID" : "46", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regIpAddress7_V_c_U", "Parent" : "0"},
	{"ID" : "47", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regIpAddress8_V_c_U", "Parent" : "0"},
	{"ID" : "48", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regIpAddress9_V_c_U", "Parent" : "0"},
	{"ID" : "49", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.txStatusBuffer_V_ses_U", "Parent" : "0"},
	{"ID" : "50", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.txStatusBuffer_V_err_U", "Parent" : "0"},
	{"ID" : "51", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.startSignalFifo_V_U", "Parent" : "0"},
	{"ID" : "52", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.stopSignalFifo_V_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	iperf_client {
		m_axis_listen_port_V_V {Type O LastRead -1 FirstWrite 1}
		s_axis_listen_port_status_V {Type I LastRead 0 FirstWrite -1}
		s_axis_notifications_V {Type I LastRead 0 FirstWrite -1}
		m_axis_read_package_V {Type O LastRead -1 FirstWrite 1}
		s_axis_rx_metadata_V_V {Type I LastRead 0 FirstWrite -1}
		s_axis_rx_data_V_data_V {Type I LastRead 0 FirstWrite -1}
		s_axis_rx_data_V_keep_V {Type I LastRead 0 FirstWrite -1}
		s_axis_rx_data_V_last_V {Type I LastRead 0 FirstWrite -1}
		m_axis_open_connection_V {Type O LastRead -1 FirstWrite 1}
		s_axis_open_status_V {Type I LastRead 0 FirstWrite -1}
		m_axis_close_connection_V_V {Type O LastRead -1 FirstWrite 1}
		m_axis_tx_metadata_V {Type O LastRead -1 FirstWrite 1}
		m_axis_tx_data_V_data_V {Type O LastRead -1 FirstWrite 1}
		m_axis_tx_data_V_keep_V {Type O LastRead -1 FirstWrite 1}
		m_axis_tx_data_V_last_V {Type O LastRead -1 FirstWrite 1}
		s_axis_tx_status_V {Type I LastRead 0 FirstWrite -1}
		runExperiment_V {Type I LastRead 0 FirstWrite -1}
		dualModeEn_V {Type I LastRead 0 FirstWrite -1}
		useConn_V {Type I LastRead 0 FirstWrite -1}
		pkgWordCount_V {Type I LastRead 0 FirstWrite -1}
		packetGap_V {Type I LastRead 0 FirstWrite -1}
		timeInSeconds_V {Type I LastRead 0 FirstWrite -1}
		timeInCycles_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress0_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress1_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress2_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress3_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress4_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress5_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress6_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress7_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress8_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress9_V {Type I LastRead 0 FirstWrite -1}
		txStatusBuffer_V_ses {Type IO LastRead -1 FirstWrite -1}
		txStatusBuffer_V_err {Type IO LastRead -1 FirstWrite -1}
		iperfFsmState {Type IO LastRead -1 FirstWrite -1}
		sessionIt_V {Type IO LastRead -1 FirstWrite -1}
		closeIt_V {Type IO LastRead -1 FirstWrite -1}
		numConnections_V {Type IO LastRead -1 FirstWrite -1}
		timeOver {Type IO LastRead -1 FirstWrite -1}
		header_idx {Type IO LastRead -1 FirstWrite -1}
		currentSessionID_V {Type IO LastRead -1 FirstWrite -1}
		wordCount_V {Type IO LastRead -1 FirstWrite -1}
		ipAddressIdx_V {Type IO LastRead -1 FirstWrite -1}
		startSignalFifo_V {Type IO LastRead -1 FirstWrite -1}
		header_header_V {Type IO LastRead -1 FirstWrite -1}
		packetGapCounter_V {Type IO LastRead -1 FirstWrite -1}
		stopSignalFifo_V {Type IO LastRead -1 FirstWrite -1}
		listenState {Type IO LastRead -1 FirstWrite -1}
		serverFsmState {Type IO LastRead -1 FirstWrite -1}
		sw_state {Type IO LastRead -1 FirstWrite -1}
		time_V {Type IO LastRead -1 FirstWrite -1}}
	iperf_client_entry3 {
		runExperiment_V {Type I LastRead 0 FirstWrite -1}
		dualModeEn_V {Type I LastRead 0 FirstWrite -1}
		useConn_V {Type I LastRead 0 FirstWrite -1}
		pkgWordCount_V {Type I LastRead 0 FirstWrite -1}
		packetGap_V {Type I LastRead 0 FirstWrite -1}
		timeInSeconds_V {Type I LastRead 0 FirstWrite -1}
		timeInCycles_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress0_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress1_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress2_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress3_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress4_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress5_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress6_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress7_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress8_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress9_V {Type I LastRead 0 FirstWrite -1}
		runExperiment_V_out {Type O LastRead -1 FirstWrite 0}
		dualModeEn_V_out {Type O LastRead -1 FirstWrite 0}
		useConn_V_out {Type O LastRead -1 FirstWrite 0}
		pkgWordCount_V_out {Type O LastRead -1 FirstWrite 0}
		packetGap_V_out {Type O LastRead -1 FirstWrite 0}
		timeInSeconds_V_out {Type O LastRead -1 FirstWrite 0}
		timeInCycles_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress0_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress1_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress2_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress3_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress4_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress5_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress6_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress7_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress8_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress9_V_out {Type O LastRead -1 FirstWrite 0}}
	iperf_client_entry16 {
		runExperiment_V {Type I LastRead 0 FirstWrite -1}
		dualModeEn_V {Type I LastRead 0 FirstWrite -1}
		useConn_V {Type I LastRead 0 FirstWrite -1}
		pkgWordCount_V {Type I LastRead 0 FirstWrite -1}
		packetGap_V {Type I LastRead 0 FirstWrite -1}
		timeInSeconds_V {Type I LastRead 0 FirstWrite -1}
		timeInCycles_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress0_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress1_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress2_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress3_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress4_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress5_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress6_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress7_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress8_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress9_V {Type I LastRead 0 FirstWrite -1}
		runExperiment_V_out {Type O LastRead -1 FirstWrite 0}
		dualModeEn_V_out {Type O LastRead -1 FirstWrite 0}
		useConn_V_out {Type O LastRead -1 FirstWrite 0}
		pkgWordCount_V_out {Type O LastRead -1 FirstWrite 0}
		packetGap_V_out {Type O LastRead -1 FirstWrite 0}
		timeInSeconds_V_out {Type O LastRead -1 FirstWrite 0}
		timeInCycles_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress0_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress1_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress2_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress3_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress4_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress5_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress6_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress7_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress8_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress9_V_out {Type O LastRead -1 FirstWrite 0}}
	status_handler {
		txStatus_V {Type I LastRead 0 FirstWrite -1}
		txStatusBuffer_V_ses {Type O LastRead -1 FirstWrite 1}
		txStatusBuffer_V_err {Type O LastRead -1 FirstWrite 1}}
	client_64_s {
		openConnection_V {Type O LastRead -1 FirstWrite 1}
		openConStatus_V {Type I LastRead 0 FirstWrite -1}
		closeConnection_V_V {Type O LastRead -1 FirstWrite 1}
		txMetaData_V {Type O LastRead -1 FirstWrite 1}
		txData_V_data_V {Type O LastRead -1 FirstWrite 1}
		txData_V_keep_V {Type O LastRead -1 FirstWrite 1}
		txData_V_last_V {Type O LastRead -1 FirstWrite 1}
		runExperiment_V {Type I LastRead 0 FirstWrite -1}
		dualModeEn_V {Type I LastRead 0 FirstWrite -1}
		useConn_V {Type I LastRead 0 FirstWrite -1}
		pkgWordCount_V {Type I LastRead 0 FirstWrite -1}
		packetGap_V {Type I LastRead 0 FirstWrite -1}
		timeInSeconds_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress0_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress1_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress2_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress3_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress4_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress5_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress6_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress7_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress8_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress9_V {Type I LastRead 0 FirstWrite -1}
		iperfFsmState {Type IO LastRead -1 FirstWrite -1}
		sessionIt_V {Type IO LastRead -1 FirstWrite -1}
		closeIt_V {Type IO LastRead -1 FirstWrite -1}
		numConnections_V {Type IO LastRead -1 FirstWrite -1}
		timeOver {Type IO LastRead -1 FirstWrite -1}
		header_idx {Type IO LastRead -1 FirstWrite -1}
		currentSessionID_V {Type IO LastRead -1 FirstWrite -1}
		wordCount_V {Type IO LastRead -1 FirstWrite -1}
		ipAddressIdx_V {Type IO LastRead -1 FirstWrite -1}
		startSignalFifo_V {Type O LastRead -1 FirstWrite 1}
		header_header_V {Type IO LastRead -1 FirstWrite -1}
		txStatusBuffer_V_ses {Type I LastRead 0 FirstWrite -1}
		txStatusBuffer_V_err {Type I LastRead 0 FirstWrite -1}
		packetGapCounter_V {Type IO LastRead -1 FirstWrite -1}
		stopSignalFifo_V {Type I LastRead 1 FirstWrite -1}}
	server_64_s {
		listenPort_V_V {Type O LastRead -1 FirstWrite 1}
		listenPortStatus_V {Type I LastRead 0 FirstWrite -1}
		notifications_V {Type I LastRead 0 FirstWrite -1}
		readRequest_V {Type O LastRead -1 FirstWrite 1}
		rxMetaData_V_V {Type I LastRead 0 FirstWrite -1}
		rxData_V_data_V {Type I LastRead 0 FirstWrite -1}
		rxData_V_keep_V {Type I LastRead 0 FirstWrite -1}
		rxData_V_last_V {Type I LastRead 0 FirstWrite -1}
		listenState {Type IO LastRead -1 FirstWrite -1}
		serverFsmState {Type IO LastRead -1 FirstWrite -1}}
	clock {
		timeInCycles_V {Type I LastRead 0 FirstWrite -1}
		sw_state {Type IO LastRead -1 FirstWrite -1}
		time_V {Type IO LastRead -1 FirstWrite -1}
		startSignalFifo_V {Type I LastRead 0 FirstWrite -1}
		stopSignalFifo_V {Type O LastRead -1 FirstWrite 1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "5", "Max" : "5"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
]}

set Spec2ImplPortList { 
	m_axis_listen_port_V_V { axis {  { m_axis_listen_port_V_V_TDATA out_data 1 16 }  { m_axis_listen_port_V_V_TVALID out_vld 1 1 }  { m_axis_listen_port_V_V_TREADY out_acc 0 1 } } }
	s_axis_listen_port_status_V { axis {  { s_axis_listen_port_status_V_TDATA in_data 0 8 }  { s_axis_listen_port_status_V_TVALID in_vld 0 1 }  { s_axis_listen_port_status_V_TREADY in_acc 1 1 } } }
	s_axis_notifications_V { axis {  { s_axis_notifications_V_TDATA in_data 0 88 }  { s_axis_notifications_V_TVALID in_vld 0 1 }  { s_axis_notifications_V_TREADY in_acc 1 1 } } }
	m_axis_read_package_V { axis {  { m_axis_read_package_V_TDATA out_data 1 32 }  { m_axis_read_package_V_TVALID out_vld 1 1 }  { m_axis_read_package_V_TREADY out_acc 0 1 } } }
	s_axis_rx_metadata_V_V { axis {  { s_axis_rx_metadata_V_V_TDATA in_data 0 16 }  { s_axis_rx_metadata_V_V_TVALID in_vld 0 1 }  { s_axis_rx_metadata_V_V_TREADY in_acc 1 1 } } }
	s_axis_rx_data_V_data_V { axis {  { s_axis_rx_data_TDATA in_data 0 64 } } }
	s_axis_rx_data_V_keep_V { axis {  { s_axis_rx_data_TKEEP in_data 0 8 } } }
	s_axis_rx_data_V_last_V { axis {  { s_axis_rx_data_TLAST in_data 0 1 }  { s_axis_rx_data_TVALID in_vld 0 1 }  { s_axis_rx_data_TREADY in_acc 1 1 } } }
	m_axis_open_connection_V { axis {  { m_axis_open_connection_V_TDATA out_data 1 48 }  { m_axis_open_connection_V_TVALID out_vld 1 1 }  { m_axis_open_connection_V_TREADY out_acc 0 1 } } }
	s_axis_open_status_V { axis {  { s_axis_open_status_V_TDATA in_data 0 24 }  { s_axis_open_status_V_TVALID in_vld 0 1 }  { s_axis_open_status_V_TREADY in_acc 1 1 } } }
	m_axis_close_connection_V_V { axis {  { m_axis_close_connection_V_V_TDATA out_data 1 16 }  { m_axis_close_connection_V_V_TVALID out_vld 1 1 }  { m_axis_close_connection_V_V_TREADY out_acc 0 1 } } }
	m_axis_tx_metadata_V { axis {  { m_axis_tx_metadata_V_TDATA out_data 1 32 }  { m_axis_tx_metadata_V_TVALID out_vld 1 1 }  { m_axis_tx_metadata_V_TREADY out_acc 0 1 } } }
	m_axis_tx_data_V_data_V { axis {  { m_axis_tx_data_TDATA out_data 1 64 } } }
	m_axis_tx_data_V_keep_V { axis {  { m_axis_tx_data_TKEEP out_data 1 8 } } }
	m_axis_tx_data_V_last_V { axis {  { m_axis_tx_data_TLAST out_data 1 1 }  { m_axis_tx_data_TVALID out_vld 1 1 }  { m_axis_tx_data_TREADY out_acc 0 1 } } }
	s_axis_tx_status_V { axis {  { s_axis_tx_status_V_TDATA in_data 0 64 }  { s_axis_tx_status_V_TVALID in_vld 0 1 }  { s_axis_tx_status_V_TREADY in_acc 1 1 } } }
	runExperiment_V { ap_none {  { runExperiment_V in_data 0 1 } } }
	dualModeEn_V { ap_none {  { dualModeEn_V in_data 0 1 } } }
	useConn_V { ap_none {  { useConn_V in_data 0 14 } } }
	pkgWordCount_V { ap_none {  { pkgWordCount_V in_data 0 8 } } }
	packetGap_V { ap_none {  { packetGap_V in_data 0 8 } } }
	timeInSeconds_V { ap_none {  { timeInSeconds_V in_data 0 32 } } }
	timeInCycles_V { ap_none {  { timeInCycles_V in_data 0 64 } } }
	regIpAddress0_V { ap_none {  { regIpAddress0_V in_data 0 32 } } }
	regIpAddress1_V { ap_none {  { regIpAddress1_V in_data 0 32 } } }
	regIpAddress2_V { ap_none {  { regIpAddress2_V in_data 0 32 } } }
	regIpAddress3_V { ap_none {  { regIpAddress3_V in_data 0 32 } } }
	regIpAddress4_V { ap_none {  { regIpAddress4_V in_data 0 32 } } }
	regIpAddress5_V { ap_none {  { regIpAddress5_V in_data 0 32 } } }
	regIpAddress6_V { ap_none {  { regIpAddress6_V in_data 0 32 } } }
	regIpAddress7_V { ap_none {  { regIpAddress7_V in_data 0 32 } } }
	regIpAddress8_V { ap_none {  { regIpAddress8_V in_data 0 32 } } }
	regIpAddress9_V { ap_none {  { regIpAddress9_V in_data 0 32 } } }
}

set busDeadlockParameterList { 
}

# RTL port scheduling information:
set fifoSchedulingInfoList { 
}

# RTL bus port read request latency information:
set busReadReqLatencyList { 
}

# RTL bus port write response latency information:
set busWriteResLatencyList { 
}

# RTL array port load latency information:
set memoryLoadLatencyList { 
}
