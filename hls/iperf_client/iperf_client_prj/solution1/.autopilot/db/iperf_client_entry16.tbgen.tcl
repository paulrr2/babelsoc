set moduleName iperf_client_entry16
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 1
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {iperf_client.entry16}
set C_modelType { void 0 }
set C_modelArgList {
	{ runExperiment_V int 1 regular {fifo 0}  }
	{ dualModeEn_V int 1 regular {fifo 0}  }
	{ useConn_V int 14 regular {fifo 0}  }
	{ pkgWordCount_V int 8 regular {fifo 0}  }
	{ packetGap_V int 8 regular {fifo 0}  }
	{ timeInSeconds_V int 32 regular {fifo 0}  }
	{ timeInCycles_V int 64 regular {fifo 0}  }
	{ regIpAddress0_V int 32 regular {fifo 0}  }
	{ regIpAddress1_V int 32 regular {fifo 0}  }
	{ regIpAddress2_V int 32 regular {fifo 0}  }
	{ regIpAddress3_V int 32 regular {fifo 0}  }
	{ regIpAddress4_V int 32 regular {fifo 0}  }
	{ regIpAddress5_V int 32 regular {fifo 0}  }
	{ regIpAddress6_V int 32 regular {fifo 0}  }
	{ regIpAddress7_V int 32 regular {fifo 0}  }
	{ regIpAddress8_V int 32 regular {fifo 0}  }
	{ regIpAddress9_V int 32 regular {fifo 0}  }
	{ runExperiment_V_out int 1 regular {fifo 1}  }
	{ dualModeEn_V_out int 1 regular {fifo 1}  }
	{ useConn_V_out int 14 regular {fifo 1}  }
	{ pkgWordCount_V_out int 8 regular {fifo 1}  }
	{ packetGap_V_out int 8 regular {fifo 1}  }
	{ timeInSeconds_V_out int 32 regular {fifo 1}  }
	{ timeInCycles_V_out int 64 regular {fifo 1}  }
	{ regIpAddress0_V_out int 32 regular {fifo 1}  }
	{ regIpAddress1_V_out int 32 regular {fifo 1}  }
	{ regIpAddress2_V_out int 32 regular {fifo 1}  }
	{ regIpAddress3_V_out int 32 regular {fifo 1}  }
	{ regIpAddress4_V_out int 32 regular {fifo 1}  }
	{ regIpAddress5_V_out int 32 regular {fifo 1}  }
	{ regIpAddress6_V_out int 32 regular {fifo 1}  }
	{ regIpAddress7_V_out int 32 regular {fifo 1}  }
	{ regIpAddress8_V_out int 32 regular {fifo 1}  }
	{ regIpAddress9_V_out int 32 regular {fifo 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "runExperiment_V", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY"} , 
 	{ "Name" : "dualModeEn_V", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY"} , 
 	{ "Name" : "useConn_V", "interface" : "fifo", "bitwidth" : 14, "direction" : "READONLY"} , 
 	{ "Name" : "pkgWordCount_V", "interface" : "fifo", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "packetGap_V", "interface" : "fifo", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "timeInSeconds_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "timeInCycles_V", "interface" : "fifo", "bitwidth" : 64, "direction" : "READONLY"} , 
 	{ "Name" : "regIpAddress0_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "regIpAddress1_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "regIpAddress2_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "regIpAddress3_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "regIpAddress4_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "regIpAddress5_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "regIpAddress6_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "regIpAddress7_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "regIpAddress8_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "regIpAddress9_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "runExperiment_V_out", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY"} , 
 	{ "Name" : "dualModeEn_V_out", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY"} , 
 	{ "Name" : "useConn_V_out", "interface" : "fifo", "bitwidth" : 14, "direction" : "WRITEONLY"} , 
 	{ "Name" : "pkgWordCount_V_out", "interface" : "fifo", "bitwidth" : 8, "direction" : "WRITEONLY"} , 
 	{ "Name" : "packetGap_V_out", "interface" : "fifo", "bitwidth" : 8, "direction" : "WRITEONLY"} , 
 	{ "Name" : "timeInSeconds_V_out", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "timeInCycles_V_out", "interface" : "fifo", "bitwidth" : 64, "direction" : "WRITEONLY"} , 
 	{ "Name" : "regIpAddress0_V_out", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "regIpAddress1_V_out", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "regIpAddress2_V_out", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "regIpAddress3_V_out", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "regIpAddress4_V_out", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "regIpAddress5_V_out", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "regIpAddress6_V_out", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "regIpAddress7_V_out", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "regIpAddress8_V_out", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "regIpAddress9_V_out", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY"} ]}
# RTL Port declarations: 
set portNum 109
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ runExperiment_V_dout sc_in sc_lv 1 signal 0 } 
	{ runExperiment_V_empty_n sc_in sc_logic 1 signal 0 } 
	{ runExperiment_V_read sc_out sc_logic 1 signal 0 } 
	{ dualModeEn_V_dout sc_in sc_lv 1 signal 1 } 
	{ dualModeEn_V_empty_n sc_in sc_logic 1 signal 1 } 
	{ dualModeEn_V_read sc_out sc_logic 1 signal 1 } 
	{ useConn_V_dout sc_in sc_lv 14 signal 2 } 
	{ useConn_V_empty_n sc_in sc_logic 1 signal 2 } 
	{ useConn_V_read sc_out sc_logic 1 signal 2 } 
	{ pkgWordCount_V_dout sc_in sc_lv 8 signal 3 } 
	{ pkgWordCount_V_empty_n sc_in sc_logic 1 signal 3 } 
	{ pkgWordCount_V_read sc_out sc_logic 1 signal 3 } 
	{ packetGap_V_dout sc_in sc_lv 8 signal 4 } 
	{ packetGap_V_empty_n sc_in sc_logic 1 signal 4 } 
	{ packetGap_V_read sc_out sc_logic 1 signal 4 } 
	{ timeInSeconds_V_dout sc_in sc_lv 32 signal 5 } 
	{ timeInSeconds_V_empty_n sc_in sc_logic 1 signal 5 } 
	{ timeInSeconds_V_read sc_out sc_logic 1 signal 5 } 
	{ timeInCycles_V_dout sc_in sc_lv 64 signal 6 } 
	{ timeInCycles_V_empty_n sc_in sc_logic 1 signal 6 } 
	{ timeInCycles_V_read sc_out sc_logic 1 signal 6 } 
	{ regIpAddress0_V_dout sc_in sc_lv 32 signal 7 } 
	{ regIpAddress0_V_empty_n sc_in sc_logic 1 signal 7 } 
	{ regIpAddress0_V_read sc_out sc_logic 1 signal 7 } 
	{ regIpAddress1_V_dout sc_in sc_lv 32 signal 8 } 
	{ regIpAddress1_V_empty_n sc_in sc_logic 1 signal 8 } 
	{ regIpAddress1_V_read sc_out sc_logic 1 signal 8 } 
	{ regIpAddress2_V_dout sc_in sc_lv 32 signal 9 } 
	{ regIpAddress2_V_empty_n sc_in sc_logic 1 signal 9 } 
	{ regIpAddress2_V_read sc_out sc_logic 1 signal 9 } 
	{ regIpAddress3_V_dout sc_in sc_lv 32 signal 10 } 
	{ regIpAddress3_V_empty_n sc_in sc_logic 1 signal 10 } 
	{ regIpAddress3_V_read sc_out sc_logic 1 signal 10 } 
	{ regIpAddress4_V_dout sc_in sc_lv 32 signal 11 } 
	{ regIpAddress4_V_empty_n sc_in sc_logic 1 signal 11 } 
	{ regIpAddress4_V_read sc_out sc_logic 1 signal 11 } 
	{ regIpAddress5_V_dout sc_in sc_lv 32 signal 12 } 
	{ regIpAddress5_V_empty_n sc_in sc_logic 1 signal 12 } 
	{ regIpAddress5_V_read sc_out sc_logic 1 signal 12 } 
	{ regIpAddress6_V_dout sc_in sc_lv 32 signal 13 } 
	{ regIpAddress6_V_empty_n sc_in sc_logic 1 signal 13 } 
	{ regIpAddress6_V_read sc_out sc_logic 1 signal 13 } 
	{ regIpAddress7_V_dout sc_in sc_lv 32 signal 14 } 
	{ regIpAddress7_V_empty_n sc_in sc_logic 1 signal 14 } 
	{ regIpAddress7_V_read sc_out sc_logic 1 signal 14 } 
	{ regIpAddress8_V_dout sc_in sc_lv 32 signal 15 } 
	{ regIpAddress8_V_empty_n sc_in sc_logic 1 signal 15 } 
	{ regIpAddress8_V_read sc_out sc_logic 1 signal 15 } 
	{ regIpAddress9_V_dout sc_in sc_lv 32 signal 16 } 
	{ regIpAddress9_V_empty_n sc_in sc_logic 1 signal 16 } 
	{ regIpAddress9_V_read sc_out sc_logic 1 signal 16 } 
	{ runExperiment_V_out_din sc_out sc_lv 1 signal 17 } 
	{ runExperiment_V_out_full_n sc_in sc_logic 1 signal 17 } 
	{ runExperiment_V_out_write sc_out sc_logic 1 signal 17 } 
	{ dualModeEn_V_out_din sc_out sc_lv 1 signal 18 } 
	{ dualModeEn_V_out_full_n sc_in sc_logic 1 signal 18 } 
	{ dualModeEn_V_out_write sc_out sc_logic 1 signal 18 } 
	{ useConn_V_out_din sc_out sc_lv 14 signal 19 } 
	{ useConn_V_out_full_n sc_in sc_logic 1 signal 19 } 
	{ useConn_V_out_write sc_out sc_logic 1 signal 19 } 
	{ pkgWordCount_V_out_din sc_out sc_lv 8 signal 20 } 
	{ pkgWordCount_V_out_full_n sc_in sc_logic 1 signal 20 } 
	{ pkgWordCount_V_out_write sc_out sc_logic 1 signal 20 } 
	{ packetGap_V_out_din sc_out sc_lv 8 signal 21 } 
	{ packetGap_V_out_full_n sc_in sc_logic 1 signal 21 } 
	{ packetGap_V_out_write sc_out sc_logic 1 signal 21 } 
	{ timeInSeconds_V_out_din sc_out sc_lv 32 signal 22 } 
	{ timeInSeconds_V_out_full_n sc_in sc_logic 1 signal 22 } 
	{ timeInSeconds_V_out_write sc_out sc_logic 1 signal 22 } 
	{ timeInCycles_V_out_din sc_out sc_lv 64 signal 23 } 
	{ timeInCycles_V_out_full_n sc_in sc_logic 1 signal 23 } 
	{ timeInCycles_V_out_write sc_out sc_logic 1 signal 23 } 
	{ regIpAddress0_V_out_din sc_out sc_lv 32 signal 24 } 
	{ regIpAddress0_V_out_full_n sc_in sc_logic 1 signal 24 } 
	{ regIpAddress0_V_out_write sc_out sc_logic 1 signal 24 } 
	{ regIpAddress1_V_out_din sc_out sc_lv 32 signal 25 } 
	{ regIpAddress1_V_out_full_n sc_in sc_logic 1 signal 25 } 
	{ regIpAddress1_V_out_write sc_out sc_logic 1 signal 25 } 
	{ regIpAddress2_V_out_din sc_out sc_lv 32 signal 26 } 
	{ regIpAddress2_V_out_full_n sc_in sc_logic 1 signal 26 } 
	{ regIpAddress2_V_out_write sc_out sc_logic 1 signal 26 } 
	{ regIpAddress3_V_out_din sc_out sc_lv 32 signal 27 } 
	{ regIpAddress3_V_out_full_n sc_in sc_logic 1 signal 27 } 
	{ regIpAddress3_V_out_write sc_out sc_logic 1 signal 27 } 
	{ regIpAddress4_V_out_din sc_out sc_lv 32 signal 28 } 
	{ regIpAddress4_V_out_full_n sc_in sc_logic 1 signal 28 } 
	{ regIpAddress4_V_out_write sc_out sc_logic 1 signal 28 } 
	{ regIpAddress5_V_out_din sc_out sc_lv 32 signal 29 } 
	{ regIpAddress5_V_out_full_n sc_in sc_logic 1 signal 29 } 
	{ regIpAddress5_V_out_write sc_out sc_logic 1 signal 29 } 
	{ regIpAddress6_V_out_din sc_out sc_lv 32 signal 30 } 
	{ regIpAddress6_V_out_full_n sc_in sc_logic 1 signal 30 } 
	{ regIpAddress6_V_out_write sc_out sc_logic 1 signal 30 } 
	{ regIpAddress7_V_out_din sc_out sc_lv 32 signal 31 } 
	{ regIpAddress7_V_out_full_n sc_in sc_logic 1 signal 31 } 
	{ regIpAddress7_V_out_write sc_out sc_logic 1 signal 31 } 
	{ regIpAddress8_V_out_din sc_out sc_lv 32 signal 32 } 
	{ regIpAddress8_V_out_full_n sc_in sc_logic 1 signal 32 } 
	{ regIpAddress8_V_out_write sc_out sc_logic 1 signal 32 } 
	{ regIpAddress9_V_out_din sc_out sc_lv 32 signal 33 } 
	{ regIpAddress9_V_out_full_n sc_in sc_logic 1 signal 33 } 
	{ regIpAddress9_V_out_write sc_out sc_logic 1 signal 33 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "runExperiment_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "runExperiment_V", "role": "dout" }} , 
 	{ "name": "runExperiment_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "runExperiment_V", "role": "empty_n" }} , 
 	{ "name": "runExperiment_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "runExperiment_V", "role": "read" }} , 
 	{ "name": "dualModeEn_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "dualModeEn_V", "role": "dout" }} , 
 	{ "name": "dualModeEn_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dualModeEn_V", "role": "empty_n" }} , 
 	{ "name": "dualModeEn_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dualModeEn_V", "role": "read" }} , 
 	{ "name": "useConn_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":14, "type": "signal", "bundle":{"name": "useConn_V", "role": "dout" }} , 
 	{ "name": "useConn_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "useConn_V", "role": "empty_n" }} , 
 	{ "name": "useConn_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "useConn_V", "role": "read" }} , 
 	{ "name": "pkgWordCount_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "pkgWordCount_V", "role": "dout" }} , 
 	{ "name": "pkgWordCount_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "pkgWordCount_V", "role": "empty_n" }} , 
 	{ "name": "pkgWordCount_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "pkgWordCount_V", "role": "read" }} , 
 	{ "name": "packetGap_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "packetGap_V", "role": "dout" }} , 
 	{ "name": "packetGap_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "packetGap_V", "role": "empty_n" }} , 
 	{ "name": "packetGap_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "packetGap_V", "role": "read" }} , 
 	{ "name": "timeInSeconds_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "timeInSeconds_V", "role": "dout" }} , 
 	{ "name": "timeInSeconds_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "timeInSeconds_V", "role": "empty_n" }} , 
 	{ "name": "timeInSeconds_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "timeInSeconds_V", "role": "read" }} , 
 	{ "name": "timeInCycles_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "timeInCycles_V", "role": "dout" }} , 
 	{ "name": "timeInCycles_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "timeInCycles_V", "role": "empty_n" }} , 
 	{ "name": "timeInCycles_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "timeInCycles_V", "role": "read" }} , 
 	{ "name": "regIpAddress0_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress0_V", "role": "dout" }} , 
 	{ "name": "regIpAddress0_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress0_V", "role": "empty_n" }} , 
 	{ "name": "regIpAddress0_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress0_V", "role": "read" }} , 
 	{ "name": "regIpAddress1_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress1_V", "role": "dout" }} , 
 	{ "name": "regIpAddress1_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress1_V", "role": "empty_n" }} , 
 	{ "name": "regIpAddress1_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress1_V", "role": "read" }} , 
 	{ "name": "regIpAddress2_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress2_V", "role": "dout" }} , 
 	{ "name": "regIpAddress2_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress2_V", "role": "empty_n" }} , 
 	{ "name": "regIpAddress2_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress2_V", "role": "read" }} , 
 	{ "name": "regIpAddress3_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress3_V", "role": "dout" }} , 
 	{ "name": "regIpAddress3_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress3_V", "role": "empty_n" }} , 
 	{ "name": "regIpAddress3_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress3_V", "role": "read" }} , 
 	{ "name": "regIpAddress4_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress4_V", "role": "dout" }} , 
 	{ "name": "regIpAddress4_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress4_V", "role": "empty_n" }} , 
 	{ "name": "regIpAddress4_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress4_V", "role": "read" }} , 
 	{ "name": "regIpAddress5_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress5_V", "role": "dout" }} , 
 	{ "name": "regIpAddress5_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress5_V", "role": "empty_n" }} , 
 	{ "name": "regIpAddress5_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress5_V", "role": "read" }} , 
 	{ "name": "regIpAddress6_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress6_V", "role": "dout" }} , 
 	{ "name": "regIpAddress6_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress6_V", "role": "empty_n" }} , 
 	{ "name": "regIpAddress6_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress6_V", "role": "read" }} , 
 	{ "name": "regIpAddress7_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress7_V", "role": "dout" }} , 
 	{ "name": "regIpAddress7_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress7_V", "role": "empty_n" }} , 
 	{ "name": "regIpAddress7_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress7_V", "role": "read" }} , 
 	{ "name": "regIpAddress8_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress8_V", "role": "dout" }} , 
 	{ "name": "regIpAddress8_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress8_V", "role": "empty_n" }} , 
 	{ "name": "regIpAddress8_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress8_V", "role": "read" }} , 
 	{ "name": "regIpAddress9_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress9_V", "role": "dout" }} , 
 	{ "name": "regIpAddress9_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress9_V", "role": "empty_n" }} , 
 	{ "name": "regIpAddress9_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress9_V", "role": "read" }} , 
 	{ "name": "runExperiment_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "runExperiment_V_out", "role": "din" }} , 
 	{ "name": "runExperiment_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "runExperiment_V_out", "role": "full_n" }} , 
 	{ "name": "runExperiment_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "runExperiment_V_out", "role": "write" }} , 
 	{ "name": "dualModeEn_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "dualModeEn_V_out", "role": "din" }} , 
 	{ "name": "dualModeEn_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dualModeEn_V_out", "role": "full_n" }} , 
 	{ "name": "dualModeEn_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dualModeEn_V_out", "role": "write" }} , 
 	{ "name": "useConn_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":14, "type": "signal", "bundle":{"name": "useConn_V_out", "role": "din" }} , 
 	{ "name": "useConn_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "useConn_V_out", "role": "full_n" }} , 
 	{ "name": "useConn_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "useConn_V_out", "role": "write" }} , 
 	{ "name": "pkgWordCount_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "pkgWordCount_V_out", "role": "din" }} , 
 	{ "name": "pkgWordCount_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "pkgWordCount_V_out", "role": "full_n" }} , 
 	{ "name": "pkgWordCount_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "pkgWordCount_V_out", "role": "write" }} , 
 	{ "name": "packetGap_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "packetGap_V_out", "role": "din" }} , 
 	{ "name": "packetGap_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "packetGap_V_out", "role": "full_n" }} , 
 	{ "name": "packetGap_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "packetGap_V_out", "role": "write" }} , 
 	{ "name": "timeInSeconds_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "timeInSeconds_V_out", "role": "din" }} , 
 	{ "name": "timeInSeconds_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "timeInSeconds_V_out", "role": "full_n" }} , 
 	{ "name": "timeInSeconds_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "timeInSeconds_V_out", "role": "write" }} , 
 	{ "name": "timeInCycles_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "timeInCycles_V_out", "role": "din" }} , 
 	{ "name": "timeInCycles_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "timeInCycles_V_out", "role": "full_n" }} , 
 	{ "name": "timeInCycles_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "timeInCycles_V_out", "role": "write" }} , 
 	{ "name": "regIpAddress0_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress0_V_out", "role": "din" }} , 
 	{ "name": "regIpAddress0_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress0_V_out", "role": "full_n" }} , 
 	{ "name": "regIpAddress0_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress0_V_out", "role": "write" }} , 
 	{ "name": "regIpAddress1_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress1_V_out", "role": "din" }} , 
 	{ "name": "regIpAddress1_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress1_V_out", "role": "full_n" }} , 
 	{ "name": "regIpAddress1_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress1_V_out", "role": "write" }} , 
 	{ "name": "regIpAddress2_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress2_V_out", "role": "din" }} , 
 	{ "name": "regIpAddress2_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress2_V_out", "role": "full_n" }} , 
 	{ "name": "regIpAddress2_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress2_V_out", "role": "write" }} , 
 	{ "name": "regIpAddress3_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress3_V_out", "role": "din" }} , 
 	{ "name": "regIpAddress3_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress3_V_out", "role": "full_n" }} , 
 	{ "name": "regIpAddress3_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress3_V_out", "role": "write" }} , 
 	{ "name": "regIpAddress4_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress4_V_out", "role": "din" }} , 
 	{ "name": "regIpAddress4_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress4_V_out", "role": "full_n" }} , 
 	{ "name": "regIpAddress4_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress4_V_out", "role": "write" }} , 
 	{ "name": "regIpAddress5_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress5_V_out", "role": "din" }} , 
 	{ "name": "regIpAddress5_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress5_V_out", "role": "full_n" }} , 
 	{ "name": "regIpAddress5_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress5_V_out", "role": "write" }} , 
 	{ "name": "regIpAddress6_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress6_V_out", "role": "din" }} , 
 	{ "name": "regIpAddress6_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress6_V_out", "role": "full_n" }} , 
 	{ "name": "regIpAddress6_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress6_V_out", "role": "write" }} , 
 	{ "name": "regIpAddress7_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress7_V_out", "role": "din" }} , 
 	{ "name": "regIpAddress7_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress7_V_out", "role": "full_n" }} , 
 	{ "name": "regIpAddress7_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress7_V_out", "role": "write" }} , 
 	{ "name": "regIpAddress8_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress8_V_out", "role": "din" }} , 
 	{ "name": "regIpAddress8_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress8_V_out", "role": "full_n" }} , 
 	{ "name": "regIpAddress8_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress8_V_out", "role": "write" }} , 
 	{ "name": "regIpAddress9_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "regIpAddress9_V_out", "role": "din" }} , 
 	{ "name": "regIpAddress9_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress9_V_out", "role": "full_n" }} , 
 	{ "name": "regIpAddress9_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "regIpAddress9_V_out", "role": "write" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "iperf_client_entry16",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "0", "EstimateLatencyMin" : "0", "EstimateLatencyMax" : "0",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "0",
		"Port" : [
			{"Name" : "runExperiment_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "runExperiment_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dualModeEn_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "dualModeEn_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "useConn_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "useConn_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "pkgWordCount_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "pkgWordCount_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "packetGap_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "packetGap_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "timeInSeconds_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "timeInSeconds_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "timeInCycles_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "timeInCycles_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress0_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress0_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress1_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress1_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress2_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress2_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress3_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress3_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress4_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress4_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress5_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress5_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress6_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress6_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress7_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress7_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress8_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress8_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress9_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress9_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "runExperiment_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "runExperiment_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dualModeEn_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "dualModeEn_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "useConn_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "useConn_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "pkgWordCount_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "pkgWordCount_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "packetGap_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "packetGap_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "timeInSeconds_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "timeInSeconds_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "timeInCycles_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "timeInCycles_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress0_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress0_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress1_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress1_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress2_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress2_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress3_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress3_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress4_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress4_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress5_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress5_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress6_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress6_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress7_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress7_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress8_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress8_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "regIpAddress9_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "regIpAddress9_V_out_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	iperf_client_entry16 {
		runExperiment_V {Type I LastRead 0 FirstWrite -1}
		dualModeEn_V {Type I LastRead 0 FirstWrite -1}
		useConn_V {Type I LastRead 0 FirstWrite -1}
		pkgWordCount_V {Type I LastRead 0 FirstWrite -1}
		packetGap_V {Type I LastRead 0 FirstWrite -1}
		timeInSeconds_V {Type I LastRead 0 FirstWrite -1}
		timeInCycles_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress0_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress1_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress2_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress3_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress4_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress5_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress6_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress7_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress8_V {Type I LastRead 0 FirstWrite -1}
		regIpAddress9_V {Type I LastRead 0 FirstWrite -1}
		runExperiment_V_out {Type O LastRead -1 FirstWrite 0}
		dualModeEn_V_out {Type O LastRead -1 FirstWrite 0}
		useConn_V_out {Type O LastRead -1 FirstWrite 0}
		pkgWordCount_V_out {Type O LastRead -1 FirstWrite 0}
		packetGap_V_out {Type O LastRead -1 FirstWrite 0}
		timeInSeconds_V_out {Type O LastRead -1 FirstWrite 0}
		timeInCycles_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress0_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress1_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress2_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress3_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress4_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress5_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress6_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress7_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress8_V_out {Type O LastRead -1 FirstWrite 0}
		regIpAddress9_V_out {Type O LastRead -1 FirstWrite 0}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "0", "Max" : "0"}
	, {"Name" : "Interval", "Min" : "0", "Max" : "0"}
]}

set PipelineEnableSignalInfo {[
]}

set Spec2ImplPortList { 
	runExperiment_V { ap_fifo {  { runExperiment_V_dout fifo_data 0 1 }  { runExperiment_V_empty_n fifo_status 0 1 }  { runExperiment_V_read fifo_update 1 1 } } }
	dualModeEn_V { ap_fifo {  { dualModeEn_V_dout fifo_data 0 1 }  { dualModeEn_V_empty_n fifo_status 0 1 }  { dualModeEn_V_read fifo_update 1 1 } } }
	useConn_V { ap_fifo {  { useConn_V_dout fifo_data 0 14 }  { useConn_V_empty_n fifo_status 0 1 }  { useConn_V_read fifo_update 1 1 } } }
	pkgWordCount_V { ap_fifo {  { pkgWordCount_V_dout fifo_data 0 8 }  { pkgWordCount_V_empty_n fifo_status 0 1 }  { pkgWordCount_V_read fifo_update 1 1 } } }
	packetGap_V { ap_fifo {  { packetGap_V_dout fifo_data 0 8 }  { packetGap_V_empty_n fifo_status 0 1 }  { packetGap_V_read fifo_update 1 1 } } }
	timeInSeconds_V { ap_fifo {  { timeInSeconds_V_dout fifo_data 0 32 }  { timeInSeconds_V_empty_n fifo_status 0 1 }  { timeInSeconds_V_read fifo_update 1 1 } } }
	timeInCycles_V { ap_fifo {  { timeInCycles_V_dout fifo_data 0 64 }  { timeInCycles_V_empty_n fifo_status 0 1 }  { timeInCycles_V_read fifo_update 1 1 } } }
	regIpAddress0_V { ap_fifo {  { regIpAddress0_V_dout fifo_data 0 32 }  { regIpAddress0_V_empty_n fifo_status 0 1 }  { regIpAddress0_V_read fifo_update 1 1 } } }
	regIpAddress1_V { ap_fifo {  { regIpAddress1_V_dout fifo_data 0 32 }  { regIpAddress1_V_empty_n fifo_status 0 1 }  { regIpAddress1_V_read fifo_update 1 1 } } }
	regIpAddress2_V { ap_fifo {  { regIpAddress2_V_dout fifo_data 0 32 }  { regIpAddress2_V_empty_n fifo_status 0 1 }  { regIpAddress2_V_read fifo_update 1 1 } } }
	regIpAddress3_V { ap_fifo {  { regIpAddress3_V_dout fifo_data 0 32 }  { regIpAddress3_V_empty_n fifo_status 0 1 }  { regIpAddress3_V_read fifo_update 1 1 } } }
	regIpAddress4_V { ap_fifo {  { regIpAddress4_V_dout fifo_data 0 32 }  { regIpAddress4_V_empty_n fifo_status 0 1 }  { regIpAddress4_V_read fifo_update 1 1 } } }
	regIpAddress5_V { ap_fifo {  { regIpAddress5_V_dout fifo_data 0 32 }  { regIpAddress5_V_empty_n fifo_status 0 1 }  { regIpAddress5_V_read fifo_update 1 1 } } }
	regIpAddress6_V { ap_fifo {  { regIpAddress6_V_dout fifo_data 0 32 }  { regIpAddress6_V_empty_n fifo_status 0 1 }  { regIpAddress6_V_read fifo_update 1 1 } } }
	regIpAddress7_V { ap_fifo {  { regIpAddress7_V_dout fifo_data 0 32 }  { regIpAddress7_V_empty_n fifo_status 0 1 }  { regIpAddress7_V_read fifo_update 1 1 } } }
	regIpAddress8_V { ap_fifo {  { regIpAddress8_V_dout fifo_data 0 32 }  { regIpAddress8_V_empty_n fifo_status 0 1 }  { regIpAddress8_V_read fifo_update 1 1 } } }
	regIpAddress9_V { ap_fifo {  { regIpAddress9_V_dout fifo_data 0 32 }  { regIpAddress9_V_empty_n fifo_status 0 1 }  { regIpAddress9_V_read fifo_update 1 1 } } }
	runExperiment_V_out { ap_fifo {  { runExperiment_V_out_din fifo_data 1 1 }  { runExperiment_V_out_full_n fifo_status 0 1 }  { runExperiment_V_out_write fifo_update 1 1 } } }
	dualModeEn_V_out { ap_fifo {  { dualModeEn_V_out_din fifo_data 1 1 }  { dualModeEn_V_out_full_n fifo_status 0 1 }  { dualModeEn_V_out_write fifo_update 1 1 } } }
	useConn_V_out { ap_fifo {  { useConn_V_out_din fifo_data 1 14 }  { useConn_V_out_full_n fifo_status 0 1 }  { useConn_V_out_write fifo_update 1 1 } } }
	pkgWordCount_V_out { ap_fifo {  { pkgWordCount_V_out_din fifo_data 1 8 }  { pkgWordCount_V_out_full_n fifo_status 0 1 }  { pkgWordCount_V_out_write fifo_update 1 1 } } }
	packetGap_V_out { ap_fifo {  { packetGap_V_out_din fifo_data 1 8 }  { packetGap_V_out_full_n fifo_status 0 1 }  { packetGap_V_out_write fifo_update 1 1 } } }
	timeInSeconds_V_out { ap_fifo {  { timeInSeconds_V_out_din fifo_data 1 32 }  { timeInSeconds_V_out_full_n fifo_status 0 1 }  { timeInSeconds_V_out_write fifo_update 1 1 } } }
	timeInCycles_V_out { ap_fifo {  { timeInCycles_V_out_din fifo_data 1 64 }  { timeInCycles_V_out_full_n fifo_status 0 1 }  { timeInCycles_V_out_write fifo_update 1 1 } } }
	regIpAddress0_V_out { ap_fifo {  { regIpAddress0_V_out_din fifo_data 1 32 }  { regIpAddress0_V_out_full_n fifo_status 0 1 }  { regIpAddress0_V_out_write fifo_update 1 1 } } }
	regIpAddress1_V_out { ap_fifo {  { regIpAddress1_V_out_din fifo_data 1 32 }  { regIpAddress1_V_out_full_n fifo_status 0 1 }  { regIpAddress1_V_out_write fifo_update 1 1 } } }
	regIpAddress2_V_out { ap_fifo {  { regIpAddress2_V_out_din fifo_data 1 32 }  { regIpAddress2_V_out_full_n fifo_status 0 1 }  { regIpAddress2_V_out_write fifo_update 1 1 } } }
	regIpAddress3_V_out { ap_fifo {  { regIpAddress3_V_out_din fifo_data 1 32 }  { regIpAddress3_V_out_full_n fifo_status 0 1 }  { regIpAddress3_V_out_write fifo_update 1 1 } } }
	regIpAddress4_V_out { ap_fifo {  { regIpAddress4_V_out_din fifo_data 1 32 }  { regIpAddress4_V_out_full_n fifo_status 0 1 }  { regIpAddress4_V_out_write fifo_update 1 1 } } }
	regIpAddress5_V_out { ap_fifo {  { regIpAddress5_V_out_din fifo_data 1 32 }  { regIpAddress5_V_out_full_n fifo_status 0 1 }  { regIpAddress5_V_out_write fifo_update 1 1 } } }
	regIpAddress6_V_out { ap_fifo {  { regIpAddress6_V_out_din fifo_data 1 32 }  { regIpAddress6_V_out_full_n fifo_status 0 1 }  { regIpAddress6_V_out_write fifo_update 1 1 } } }
	regIpAddress7_V_out { ap_fifo {  { regIpAddress7_V_out_din fifo_data 1 32 }  { regIpAddress7_V_out_full_n fifo_status 0 1 }  { regIpAddress7_V_out_write fifo_update 1 1 } } }
	regIpAddress8_V_out { ap_fifo {  { regIpAddress8_V_out_din fifo_data 1 32 }  { regIpAddress8_V_out_full_n fifo_status 0 1 }  { regIpAddress8_V_out_write fifo_update 1 1 } } }
	regIpAddress9_V_out { ap_fifo {  { regIpAddress9_V_out_din fifo_data 1 32 }  { regIpAddress9_V_out_full_n fifo_status 0 1 }  { regIpAddress9_V_out_write fifo_update 1 1 } } }
}
