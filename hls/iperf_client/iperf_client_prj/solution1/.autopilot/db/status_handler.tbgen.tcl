set moduleName status_handler
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {status_handler}
set C_modelType { void 0 }
set C_modelArgList {
	{ txStatus_V int 64 regular {axi_s 0 volatile  { txStatus_V Data } }  }
	{ txStatusBuffer_V_ses int 16 regular {fifo 1 volatile } {global 1}  }
	{ txStatusBuffer_V_err int 2 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "txStatus_V", "interface" : "axis", "bitwidth" : 64, "direction" : "READONLY"} , 
 	{ "Name" : "txStatusBuffer_V_ses", "interface" : "fifo", "bitwidth" : 16, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "txStatusBuffer_V_err", "interface" : "fifo", "bitwidth" : 2, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 16
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ txStatus_V_TVALID sc_in sc_logic 1 invld 0 } 
	{ txStatusBuffer_V_ses_din sc_out sc_lv 16 signal 1 } 
	{ txStatusBuffer_V_ses_full_n sc_in sc_logic 1 signal 1 } 
	{ txStatusBuffer_V_ses_write sc_out sc_logic 1 signal 1 } 
	{ txStatusBuffer_V_err_din sc_out sc_lv 2 signal 2 } 
	{ txStatusBuffer_V_err_full_n sc_in sc_logic 1 signal 2 } 
	{ txStatusBuffer_V_err_write sc_out sc_logic 1 signal 2 } 
	{ txStatus_V_TDATA sc_in sc_lv 64 signal 0 } 
	{ txStatus_V_TREADY sc_out sc_logic 1 inacc 0 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "txStatus_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "txStatus_V", "role": "TVALID" }} , 
 	{ "name": "txStatusBuffer_V_ses_din", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "txStatusBuffer_V_ses", "role": "din" }} , 
 	{ "name": "txStatusBuffer_V_ses_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txStatusBuffer_V_ses", "role": "full_n" }} , 
 	{ "name": "txStatusBuffer_V_ses_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txStatusBuffer_V_ses", "role": "write" }} , 
 	{ "name": "txStatusBuffer_V_err_din", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "txStatusBuffer_V_err", "role": "din" }} , 
 	{ "name": "txStatusBuffer_V_err_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txStatusBuffer_V_err", "role": "full_n" }} , 
 	{ "name": "txStatusBuffer_V_err_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "txStatusBuffer_V_err", "role": "write" }} , 
 	{ "name": "txStatus_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "txStatus_V", "role": "TDATA" }} , 
 	{ "name": "txStatus_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "txStatus_V", "role": "TREADY" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "status_handler",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "txStatus_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "txStatus_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txStatusBuffer_V_ses", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txStatusBuffer_V_ses_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "txStatusBuffer_V_err", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "txStatusBuffer_V_err_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	status_handler {
		txStatus_V {Type I LastRead 0 FirstWrite -1}
		txStatusBuffer_V_ses {Type O LastRead -1 FirstWrite 1}
		txStatusBuffer_V_err {Type O LastRead -1 FirstWrite 1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "1", "Max" : "1"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	txStatus_V { axis {  { txStatus_V_TVALID in_vld 0 1 }  { txStatus_V_TDATA in_data 0 64 }  { txStatus_V_TREADY in_acc 1 1 } } }
	txStatusBuffer_V_ses { ap_fifo {  { txStatusBuffer_V_ses_din fifo_data 1 16 }  { txStatusBuffer_V_ses_full_n fifo_status 0 1 }  { txStatusBuffer_V_ses_write fifo_update 1 1 } } }
	txStatusBuffer_V_err { ap_fifo {  { txStatusBuffer_V_err_din fifo_data 1 2 }  { txStatusBuffer_V_err_full_n fifo_status 0 1 }  { txStatusBuffer_V_err_write fifo_update 1 1 } } }
}
