# ==============================================================
# Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.2.1 (64-bit)
# Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
# ==============================================================

/tools/Xilinx/Vivado/2019.2/bin/vivado  -notrace -mode batch -source ipi_example.tcl -tclargs xc7vx690t-ffg1761-2 ../ethz_systems_fpga_hls_iperf_client_1_0.zip
