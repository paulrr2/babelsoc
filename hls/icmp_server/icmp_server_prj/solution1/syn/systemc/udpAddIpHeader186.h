// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
// Version: 2019.2.1
// Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

#ifndef _udpAddIpHeader186_HH_
#define _udpAddIpHeader186_HH_

#include "systemc.h"
#include "AESL_pkg.h"


namespace ap_rtl {

struct udpAddIpHeader186 : public sc_module {
    // Port declarations 28
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_in< sc_logic > ap_start;
    sc_out< sc_logic > ap_done;
    sc_in< sc_logic > ap_continue;
    sc_out< sc_logic > ap_idle;
    sc_out< sc_logic > ap_ready;
    sc_in< sc_lv<64> > udpPort2addIpHeader_7_dout;
    sc_in< sc_logic > udpPort2addIpHeader_7_empty_n;
    sc_out< sc_logic > udpPort2addIpHeader_7_read;
    sc_out< sc_lv<64> > dataStreams_V_data_V_1_din;
    sc_in< sc_logic > dataStreams_V_data_V_1_full_n;
    sc_out< sc_logic > dataStreams_V_data_V_1_write;
    sc_out< sc_lv<8> > dataStreams_V_keep_V_1_din;
    sc_in< sc_logic > dataStreams_V_keep_V_1_full_n;
    sc_out< sc_logic > dataStreams_V_keep_V_1_write;
    sc_out< sc_lv<1> > dataStreams_V_last_V_1_din;
    sc_in< sc_logic > dataStreams_V_last_V_1_full_n;
    sc_out< sc_logic > dataStreams_V_last_V_1_write;
    sc_in< sc_lv<64> > udpPort2addIpHeader_8_dout;
    sc_in< sc_logic > udpPort2addIpHeader_8_empty_n;
    sc_out< sc_logic > udpPort2addIpHeader_8_read;
    sc_in< sc_lv<8> > udpPort2addIpHeader_1_dout;
    sc_in< sc_logic > udpPort2addIpHeader_1_empty_n;
    sc_out< sc_logic > udpPort2addIpHeader_1_read;
    sc_in< sc_lv<1> > udpPort2addIpHeader_6_dout;
    sc_in< sc_logic > udpPort2addIpHeader_6_empty_n;
    sc_out< sc_logic > udpPort2addIpHeader_6_read;


    // Module declarations
    udpAddIpHeader186(sc_module_name name);
    SC_HAS_PROCESS(udpAddIpHeader186);

    ~udpAddIpHeader186();

    sc_trace_file* mVcdFile;

    sc_signal< sc_logic > ap_done_reg;
    sc_signal< sc_lv<1> > ap_CS_fsm;
    sc_signal< sc_logic > ap_CS_fsm_state1;
    sc_signal< sc_lv<3> > addIpState;
    sc_signal< sc_lv<64> > tempWord_data_V;
    sc_signal< sc_lv<8> > tempWord_keep_V;
    sc_signal< sc_lv<32> > sourceIP_V;
    sc_signal< sc_logic > udpPort2addIpHeader_7_blk_n;
    sc_signal< sc_lv<1> > grp_nbreadreq_fu_164_p3;
    sc_signal< sc_lv<1> > grp_nbwritereq_fu_116_p5;
    sc_signal< sc_lv<1> > grp_nbreadreq_fu_142_p5;
    sc_signal< sc_logic > dataStreams_V_data_V_1_blk_n;
    sc_signal< sc_logic > dataStreams_V_keep_V_1_blk_n;
    sc_signal< sc_logic > dataStreams_V_last_V_1_blk_n;
    sc_signal< sc_logic > udpPort2addIpHeader_8_blk_n;
    sc_signal< sc_logic > udpPort2addIpHeader_1_blk_n;
    sc_signal< sc_logic > udpPort2addIpHeader_6_blk_n;
    sc_signal< sc_lv<8> > select_ln879_1_fu_330_p3;
    sc_signal< sc_lv<8> > ap_phi_mux_tmp_keep_V_8_phi_fu_183_p4;
    sc_signal< sc_logic > io_acc_block_signal_op20;
    sc_signal< bool > ap_predicate_op20_write_state1;
    sc_signal< sc_logic > io_acc_block_signal_op29;
    sc_signal< bool > ap_predicate_op29_read_state1;
    sc_signal< sc_logic > io_acc_block_signal_op48;
    sc_signal< bool > ap_predicate_op48_write_state1;
    sc_signal< bool > ap_predicate_op57_read_state1;
    sc_signal< sc_logic > io_acc_block_signal_op60;
    sc_signal< bool > ap_predicate_op60_read_state1;
    sc_signal< sc_logic > io_acc_block_signal_op67;
    sc_signal< bool > ap_predicate_op67_write_state1;
    sc_signal< bool > ap_predicate_op75_read_state1;
    sc_signal< sc_logic > io_acc_block_signal_op83;
    sc_signal< bool > ap_predicate_op83_write_state1;
    sc_signal< bool > ap_predicate_op91_read_state1;
    sc_signal< sc_logic > io_acc_block_signal_op102;
    sc_signal< bool > ap_predicate_op102_write_state1;
    sc_signal< bool > ap_block_state1;
    sc_signal< sc_lv<1> > tmp_last_V_fu_276_p1;
    sc_signal< sc_lv<1> > icmp_ln879_fu_303_p2;
    sc_signal< sc_lv<1> > ap_phi_mux_tmp_last_V_6_phi_fu_194_p4;
    sc_signal< sc_lv<3> > select_ln879_fu_322_p3;
    sc_signal< sc_lv<64> > p_Result_3_fu_416_p5;
    sc_signal< sc_lv<64> > p_Result_s_fu_497_p5;
    sc_signal< sc_lv<64> > p_Result_7_fu_246_p1;
    sc_signal< sc_lv<64> > p_Result_5_fu_284_p3;
    sc_signal< sc_lv<64> > p_Result_4_fu_357_p5;
    sc_signal< sc_lv<8> > p_Result_8_fu_265_p1;
    sc_signal< sc_lv<32> > grp_fu_202_p4;
    sc_signal< sc_lv<4> > tmp_17_i_fu_255_p4;
    sc_signal< sc_lv<32> > trunc_ln647_2_fu_280_p1;
    sc_signal< sc_lv<4> > p_Result_20_i_fu_293_p4;
    sc_signal< sc_lv<4> > trunc_ln647_4_fu_310_p1;
    sc_signal< sc_lv<8> > p_Result_6_fu_314_p3;
    sc_signal< sc_lv<64> > tempData_V_fu_349_p1;
    sc_signal< sc_lv<32> > trunc_ln647_3_fu_353_p1;
    sc_signal< sc_lv<64> > p_Result_1_fu_376_p5;
    sc_signal< sc_lv<64> > p_Result_2_fu_388_p5;
    sc_signal< sc_lv<8> > p_Result_i_i_32_fu_451_p4;
    sc_signal< sc_lv<8> > p_Result_i_i_fu_441_p4;
    sc_signal< sc_lv<16> > tempLength_V_fu_461_p3;
    sc_signal< sc_lv<16> > p_Val2_9_fu_469_p2;
    sc_signal< sc_lv<8> > trunc_ln647_fu_485_p1;
    sc_signal< sc_lv<8> > p_Result_i1_i_fu_475_p4;
    sc_signal< sc_lv<16> > tmp_i3_i_fu_489_p3;
    sc_signal< sc_lv<1> > ap_NS_fsm;
    sc_signal< bool > ap_condition_388;
    sc_signal< bool > ap_condition_85;
    sc_signal< bool > ap_condition_387;
    sc_signal< bool > ap_condition_95;
    sc_signal< bool > ap_condition_172;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<1> ap_ST_fsm_state1;
    static const sc_lv<32> ap_const_lv32_0;
    static const bool ap_const_boolean_1;
    static const sc_lv<3> ap_const_lv3_0;
    static const sc_lv<1> ap_const_lv1_1;
    static const sc_lv<3> ap_const_lv3_1;
    static const sc_lv<3> ap_const_lv3_2;
    static const sc_lv<3> ap_const_lv3_3;
    static const sc_lv<3> ap_const_lv3_4;
    static const sc_lv<8> ap_const_lv8_FF;
    static const sc_lv<1> ap_const_lv1_0;
    static const sc_lv<32> ap_const_lv32_20;
    static const sc_lv<32> ap_const_lv32_3F;
    static const sc_lv<32> ap_const_lv32_4;
    static const sc_lv<32> ap_const_lv32_7;
    static const sc_lv<4> ap_const_lv4_0;
    static const sc_lv<4> ap_const_lv4_F;
    static const sc_lv<8> ap_const_lv8_80;
    static const sc_lv<8> ap_const_lv8_1;
    static const sc_lv<32> ap_const_lv32_8;
    static const sc_lv<32> ap_const_lv32_F;
    static const sc_lv<32> ap_const_lv32_1010101;
    static const sc_lv<32> ap_const_lv32_18;
    static const sc_lv<32> ap_const_lv32_1F;
    static const sc_lv<32> ap_const_lv32_10;
    static const sc_lv<32> ap_const_lv32_17;
    static const sc_lv<16> ap_const_lv16_1C;
    // Thread declarations
    void thread_ap_clk_no_reset_();
    void thread_ap_CS_fsm_state1();
    void thread_ap_block_state1();
    void thread_ap_condition_172();
    void thread_ap_condition_387();
    void thread_ap_condition_388();
    void thread_ap_condition_85();
    void thread_ap_condition_95();
    void thread_ap_done();
    void thread_ap_idle();
    void thread_ap_phi_mux_tmp_keep_V_8_phi_fu_183_p4();
    void thread_ap_phi_mux_tmp_last_V_6_phi_fu_194_p4();
    void thread_ap_predicate_op102_write_state1();
    void thread_ap_predicate_op20_write_state1();
    void thread_ap_predicate_op29_read_state1();
    void thread_ap_predicate_op48_write_state1();
    void thread_ap_predicate_op57_read_state1();
    void thread_ap_predicate_op60_read_state1();
    void thread_ap_predicate_op67_write_state1();
    void thread_ap_predicate_op75_read_state1();
    void thread_ap_predicate_op83_write_state1();
    void thread_ap_predicate_op91_read_state1();
    void thread_ap_ready();
    void thread_dataStreams_V_data_V_1_blk_n();
    void thread_dataStreams_V_data_V_1_din();
    void thread_dataStreams_V_data_V_1_write();
    void thread_dataStreams_V_keep_V_1_blk_n();
    void thread_dataStreams_V_keep_V_1_din();
    void thread_dataStreams_V_keep_V_1_write();
    void thread_dataStreams_V_last_V_1_blk_n();
    void thread_dataStreams_V_last_V_1_din();
    void thread_dataStreams_V_last_V_1_write();
    void thread_grp_fu_202_p4();
    void thread_grp_nbreadreq_fu_142_p5();
    void thread_grp_nbreadreq_fu_164_p3();
    void thread_grp_nbwritereq_fu_116_p5();
    void thread_icmp_ln879_fu_303_p2();
    void thread_io_acc_block_signal_op102();
    void thread_io_acc_block_signal_op20();
    void thread_io_acc_block_signal_op29();
    void thread_io_acc_block_signal_op48();
    void thread_io_acc_block_signal_op60();
    void thread_io_acc_block_signal_op67();
    void thread_io_acc_block_signal_op83();
    void thread_p_Result_1_fu_376_p5();
    void thread_p_Result_20_i_fu_293_p4();
    void thread_p_Result_2_fu_388_p5();
    void thread_p_Result_3_fu_416_p5();
    void thread_p_Result_4_fu_357_p5();
    void thread_p_Result_5_fu_284_p3();
    void thread_p_Result_6_fu_314_p3();
    void thread_p_Result_7_fu_246_p1();
    void thread_p_Result_8_fu_265_p1();
    void thread_p_Result_i1_i_fu_475_p4();
    void thread_p_Result_i_i_32_fu_451_p4();
    void thread_p_Result_i_i_fu_441_p4();
    void thread_p_Result_s_fu_497_p5();
    void thread_p_Val2_9_fu_469_p2();
    void thread_select_ln879_1_fu_330_p3();
    void thread_select_ln879_fu_322_p3();
    void thread_tempData_V_fu_349_p1();
    void thread_tempLength_V_fu_461_p3();
    void thread_tmp_17_i_fu_255_p4();
    void thread_tmp_i3_i_fu_489_p3();
    void thread_tmp_last_V_fu_276_p1();
    void thread_trunc_ln647_2_fu_280_p1();
    void thread_trunc_ln647_3_fu_353_p1();
    void thread_trunc_ln647_4_fu_310_p1();
    void thread_trunc_ln647_fu_485_p1();
    void thread_udpPort2addIpHeader_1_blk_n();
    void thread_udpPort2addIpHeader_1_read();
    void thread_udpPort2addIpHeader_6_blk_n();
    void thread_udpPort2addIpHeader_6_read();
    void thread_udpPort2addIpHeader_7_blk_n();
    void thread_udpPort2addIpHeader_7_read();
    void thread_udpPort2addIpHeader_8_blk_n();
    void thread_udpPort2addIpHeader_8_read();
    void thread_ap_NS_fsm();
};

}

using namespace ap_rtl;

#endif
