// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
// Version: 2019.2.1
// Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

#ifndef _udpPortUnreachable18_HH_
#define _udpPortUnreachable18_HH_

#include "systemc.h"
#include "AESL_pkg.h"


namespace ap_rtl {

struct udpPortUnreachable18 : public sc_module {
    // Port declarations 32
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_in< sc_logic > ap_start;
    sc_out< sc_logic > ap_done;
    sc_in< sc_logic > ap_continue;
    sc_out< sc_logic > ap_idle;
    sc_out< sc_logic > ap_ready;
    sc_in< sc_lv<64> > udpIn_TDATA;
    sc_in< sc_logic > udpIn_TVALID;
    sc_out< sc_logic > udpIn_TREADY;
    sc_in< sc_lv<8> > udpIn_TKEEP;
    sc_in< sc_lv<1> > udpIn_TLAST;
    sc_in< sc_lv<64> > ttlIn_TDATA;
    sc_in< sc_logic > ttlIn_TVALID;
    sc_out< sc_logic > ttlIn_TREADY;
    sc_in< sc_lv<8> > ttlIn_TKEEP;
    sc_in< sc_lv<1> > ttlIn_TLAST;
    sc_out< sc_lv<64> > udpPort2addIpHeader_8_din;
    sc_in< sc_logic > udpPort2addIpHeader_8_full_n;
    sc_out< sc_logic > udpPort2addIpHeader_8_write;
    sc_out< sc_lv<8> > udpPort2addIpHeader_1_din;
    sc_in< sc_logic > udpPort2addIpHeader_1_full_n;
    sc_out< sc_logic > udpPort2addIpHeader_1_write;
    sc_out< sc_lv<1> > udpPort2addIpHeader_6_din;
    sc_in< sc_logic > udpPort2addIpHeader_6_full_n;
    sc_out< sc_logic > udpPort2addIpHeader_6_write;
    sc_out< sc_lv<64> > udpPort2addIpHeader_7_din;
    sc_in< sc_logic > udpPort2addIpHeader_7_full_n;
    sc_out< sc_logic > udpPort2addIpHeader_7_write;
    sc_out< sc_lv<16> > checksumStreams_V_V_1_din;
    sc_in< sc_logic > checksumStreams_V_V_1_full_n;
    sc_out< sc_logic > checksumStreams_V_V_1_write;


    // Module declarations
    udpPortUnreachable18(sc_module_name name);
    SC_HAS_PROCESS(udpPortUnreachable18);

    ~udpPortUnreachable18();

    sc_trace_file* mVcdFile;

    sc_signal< sc_logic > ap_done_reg;
    sc_signal< sc_lv<1> > ap_CS_fsm;
    sc_signal< sc_logic > ap_CS_fsm_state1;
    sc_signal< sc_lv<2> > udpState;
    sc_signal< sc_lv<3> > ipWordCounter_V;
    sc_signal< sc_lv<1> > streamSource_V_1;
    sc_signal< sc_lv<20> > udpChecksum_V;
    sc_signal< sc_logic > udpIn_TDATA_blk_n;
    sc_signal< sc_lv<1> > or_ln201_fu_571_p2;
    sc_signal< sc_lv<1> > grp_nbwritereq_fu_155_p5;
    sc_signal< sc_lv<1> > tmp_6_nbwritereq_fu_200_p3;
    sc_signal< sc_lv<1> > or_ln217_fu_449_p2;
    sc_signal< sc_logic > ttlIn_TDATA_blk_n;
    sc_signal< sc_logic > udpPort2addIpHeader_8_blk_n;
    sc_signal< sc_lv<1> > or_ln184_fu_693_p2;
    sc_signal< sc_logic > udpPort2addIpHeader_1_blk_n;
    sc_signal< sc_logic > udpPort2addIpHeader_6_blk_n;
    sc_signal< sc_logic > udpPort2addIpHeader_7_blk_n;
    sc_signal< sc_logic > checksumStreams_V_V_1_blk_n;
    sc_signal< sc_lv<1> > tmp_2_nbwritereq_fu_140_p3;
    sc_signal< sc_lv<1> > ap_phi_mux_tmp_last_V_4_phi_fu_220_p4;
    sc_signal< bool > ap_predicate_op34_write_state1;
    sc_signal< bool > ap_predicate_op46_read_state1;
    sc_signal< bool > ap_predicate_op51_read_state1;
    sc_signal< sc_logic > io_acc_block_signal_op75;
    sc_signal< bool > ap_predicate_op75_write_state1;
    sc_signal< bool > ap_predicate_op91_read_state1;
    sc_signal< bool > ap_predicate_op96_read_state1;
    sc_signal< sc_logic > io_acc_block_signal_op120;
    sc_signal< bool > ap_predicate_op120_write_state1;
    sc_signal< bool > ap_predicate_op121_write_state1;
    sc_signal< sc_logic > io_acc_block_signal_op147;
    sc_signal< bool > ap_predicate_op147_write_state1;
    sc_signal< bool > ap_block_state1;
    sc_signal< sc_lv<8> > ap_phi_mux_tmp_keep_V_4_phi_fu_230_p4;
    sc_signal< sc_lv<64> > ap_phi_mux_p_Val2_6_phi_fu_240_p4;
    sc_signal< sc_lv<1> > ap_phi_mux_tmp_last_V_5_phi_fu_250_p4;
    sc_signal< sc_lv<8> > ap_phi_mux_tmp_keep_V_5_phi_fu_260_p4;
    sc_signal< sc_lv<64> > ap_phi_mux_p_Val2_5_phi_fu_270_p4;
    sc_signal< sc_lv<9> > ap_phi_mux_p_Val2_4_phi_fu_281_p6;
    sc_signal< sc_lv<1> > tmp_nbreadreq_fu_116_p5;
    sc_signal< sc_lv<1> > tmp_1_nbreadreq_fu_128_p5;
    sc_signal< sc_lv<1> > icmp_ln879_fu_669_p2;
    sc_signal< sc_lv<3> > add_ln700_fu_675_p2;
    sc_signal< sc_lv<20> > r_V_2_fu_408_p2;
    sc_signal< sc_lv<20> > add_ln209_3_fu_535_p2;
    sc_signal< sc_lv<20> > add_ln209_7_fu_657_p2;
    sc_signal< sc_lv<20> > zext_ln209_fu_726_p1;
    sc_signal< sc_lv<64> > tmp_data_V_3_fu_721_p1;
    sc_signal< sc_lv<16> > trunc_ln182_fu_344_p1;
    sc_signal< sc_lv<4> > tmp_7_fu_352_p4;
    sc_signal< sc_lv<17> > zext_ln214_fu_348_p1;
    sc_signal< sc_lv<17> > zext_ln1503_fu_362_p1;
    sc_signal< sc_lv<16> > zext_ln214_3_fu_372_p1;
    sc_signal< sc_lv<16> > add_ln214_1_fu_376_p2;
    sc_signal< sc_lv<17> > add_ln214_fu_366_p2;
    sc_signal< sc_lv<1> > tmp_8_fu_386_p3;
    sc_signal< sc_lv<17> > zext_ln214_1_fu_382_p1;
    sc_signal< sc_lv<17> > r_V_fu_394_p1;
    sc_signal< sc_lv<17> > add_ln214_2_fu_398_p2;
    sc_signal< sc_lv<20> > zext_ln214_2_fu_404_p1;
    sc_signal< sc_lv<1> > xor_ln217_fu_431_p2;
    sc_signal< sc_lv<1> > and_ln217_fu_437_p2;
    sc_signal< sc_lv<1> > and_ln217_1_fu_443_p2;
    sc_signal< sc_lv<16> > p_Result_3_i_fu_455_p4;
    sc_signal< sc_lv<16> > p_Result_4_i_fu_465_p4;
    sc_signal< sc_lv<16> > p_Result_5_i_fu_483_p4;
    sc_signal< sc_lv<16> > trunc_ln647_fu_497_p1;
    sc_signal< sc_lv<17> > zext_ln647_fu_479_p1;
    sc_signal< sc_lv<17> > zext_ln209_1_fu_475_p1;
    sc_signal< sc_lv<17> > add_ln209_fu_505_p2;
    sc_signal< sc_lv<17> > zext_ln209_2_fu_501_p1;
    sc_signal< sc_lv<17> > zext_ln647_1_fu_493_p1;
    sc_signal< sc_lv<17> > add_ln209_1_fu_515_p2;
    sc_signal< sc_lv<18> > zext_ln209_3_fu_511_p1;
    sc_signal< sc_lv<18> > zext_ln209_4_fu_521_p1;
    sc_signal< sc_lv<18> > add_ln209_2_fu_525_p2;
    sc_signal< sc_lv<20> > zext_ln209_5_fu_531_p1;
    sc_signal< sc_lv<1> > xor_ln201_fu_553_p2;
    sc_signal< sc_lv<1> > and_ln201_fu_559_p2;
    sc_signal< sc_lv<1> > and_ln201_1_fu_565_p2;
    sc_signal< sc_lv<16> > p_Result_7_i_fu_577_p4;
    sc_signal< sc_lv<16> > p_Result_8_i_fu_587_p4;
    sc_signal< sc_lv<16> > p_Result_9_i_fu_605_p4;
    sc_signal< sc_lv<16> > trunc_ln647_1_fu_619_p1;
    sc_signal< sc_lv<17> > zext_ln647_2_fu_601_p1;
    sc_signal< sc_lv<17> > zext_ln209_6_fu_597_p1;
    sc_signal< sc_lv<17> > add_ln209_4_fu_627_p2;
    sc_signal< sc_lv<17> > zext_ln209_7_fu_623_p1;
    sc_signal< sc_lv<17> > zext_ln647_3_fu_615_p1;
    sc_signal< sc_lv<17> > add_ln209_5_fu_637_p2;
    sc_signal< sc_lv<18> > zext_ln209_8_fu_633_p1;
    sc_signal< sc_lv<18> > zext_ln209_9_fu_643_p1;
    sc_signal< sc_lv<18> > add_ln209_6_fu_647_p2;
    sc_signal< sc_lv<20> > zext_ln209_10_fu_653_p1;
    sc_signal< sc_lv<10> > sext_ln357_fu_717_p1;
    sc_signal< sc_lv<1> > ap_NS_fsm;
    sc_signal< bool > ap_condition_111;
    sc_signal< bool > ap_condition_116;
    sc_signal< bool > ap_condition_119;
    sc_signal< bool > ap_condition_540;
    sc_signal< bool > ap_condition_538;
    sc_signal< bool > ap_condition_234;
    sc_signal< bool > ap_condition_88;
    sc_signal< bool > ap_condition_115;
    sc_signal< bool > ap_condition_110;
    sc_signal< bool > ap_condition_185;
    sc_signal< bool > ap_condition_228;
    sc_signal< bool > ap_condition_232;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<1> ap_ST_fsm_state1;
    static const sc_lv<32> ap_const_lv32_0;
    static const bool ap_const_boolean_1;
    static const sc_lv<2> ap_const_lv2_0;
    static const sc_lv<3> ap_const_lv3_0;
    static const sc_lv<1> ap_const_lv1_0;
    static const sc_lv<2> ap_const_lv2_1;
    static const sc_lv<1> ap_const_lv1_1;
    static const sc_lv<2> ap_const_lv2_2;
    static const sc_lv<2> ap_const_lv2_3;
    static const sc_lv<9> ap_const_lv9_103;
    static const sc_lv<9> ap_const_lv9_B;
    static const sc_lv<9> ap_const_lv9_0;
    static const sc_lv<8> ap_const_lv8_FF;
    static const sc_lv<32> ap_const_lv32_10;
    static const sc_lv<32> ap_const_lv32_13;
    static const sc_lv<20> ap_const_lv20_FFFFF;
    static const sc_lv<32> ap_const_lv32_30;
    static const sc_lv<32> ap_const_lv32_3F;
    static const sc_lv<32> ap_const_lv32_20;
    static const sc_lv<32> ap_const_lv32_2F;
    static const sc_lv<32> ap_const_lv32_1F;
    static const sc_lv<3> ap_const_lv3_2;
    static const sc_lv<3> ap_const_lv3_1;
    // Thread declarations
    void thread_ap_clk_no_reset_();
    void thread_add_ln209_1_fu_515_p2();
    void thread_add_ln209_2_fu_525_p2();
    void thread_add_ln209_3_fu_535_p2();
    void thread_add_ln209_4_fu_627_p2();
    void thread_add_ln209_5_fu_637_p2();
    void thread_add_ln209_6_fu_647_p2();
    void thread_add_ln209_7_fu_657_p2();
    void thread_add_ln209_fu_505_p2();
    void thread_add_ln214_1_fu_376_p2();
    void thread_add_ln214_2_fu_398_p2();
    void thread_add_ln214_fu_366_p2();
    void thread_add_ln700_fu_675_p2();
    void thread_and_ln201_1_fu_565_p2();
    void thread_and_ln201_fu_559_p2();
    void thread_and_ln217_1_fu_443_p2();
    void thread_and_ln217_fu_437_p2();
    void thread_ap_CS_fsm_state1();
    void thread_ap_block_state1();
    void thread_ap_condition_110();
    void thread_ap_condition_111();
    void thread_ap_condition_115();
    void thread_ap_condition_116();
    void thread_ap_condition_119();
    void thread_ap_condition_185();
    void thread_ap_condition_228();
    void thread_ap_condition_232();
    void thread_ap_condition_234();
    void thread_ap_condition_538();
    void thread_ap_condition_540();
    void thread_ap_condition_88();
    void thread_ap_done();
    void thread_ap_idle();
    void thread_ap_phi_mux_p_Val2_4_phi_fu_281_p6();
    void thread_ap_phi_mux_p_Val2_5_phi_fu_270_p4();
    void thread_ap_phi_mux_p_Val2_6_phi_fu_240_p4();
    void thread_ap_phi_mux_tmp_keep_V_4_phi_fu_230_p4();
    void thread_ap_phi_mux_tmp_keep_V_5_phi_fu_260_p4();
    void thread_ap_phi_mux_tmp_last_V_4_phi_fu_220_p4();
    void thread_ap_phi_mux_tmp_last_V_5_phi_fu_250_p4();
    void thread_ap_predicate_op120_write_state1();
    void thread_ap_predicate_op121_write_state1();
    void thread_ap_predicate_op147_write_state1();
    void thread_ap_predicate_op34_write_state1();
    void thread_ap_predicate_op46_read_state1();
    void thread_ap_predicate_op51_read_state1();
    void thread_ap_predicate_op75_write_state1();
    void thread_ap_predicate_op91_read_state1();
    void thread_ap_predicate_op96_read_state1();
    void thread_ap_ready();
    void thread_checksumStreams_V_V_1_blk_n();
    void thread_checksumStreams_V_V_1_din();
    void thread_checksumStreams_V_V_1_write();
    void thread_grp_nbwritereq_fu_155_p5();
    void thread_icmp_ln879_fu_669_p2();
    void thread_io_acc_block_signal_op120();
    void thread_io_acc_block_signal_op147();
    void thread_io_acc_block_signal_op75();
    void thread_or_ln184_fu_693_p2();
    void thread_or_ln201_fu_571_p2();
    void thread_or_ln217_fu_449_p2();
    void thread_p_Result_3_i_fu_455_p4();
    void thread_p_Result_4_i_fu_465_p4();
    void thread_p_Result_5_i_fu_483_p4();
    void thread_p_Result_7_i_fu_577_p4();
    void thread_p_Result_8_i_fu_587_p4();
    void thread_p_Result_9_i_fu_605_p4();
    void thread_r_V_2_fu_408_p2();
    void thread_r_V_fu_394_p1();
    void thread_sext_ln357_fu_717_p1();
    void thread_tmp_1_nbreadreq_fu_128_p5();
    void thread_tmp_2_nbwritereq_fu_140_p3();
    void thread_tmp_6_nbwritereq_fu_200_p3();
    void thread_tmp_7_fu_352_p4();
    void thread_tmp_8_fu_386_p3();
    void thread_tmp_data_V_3_fu_721_p1();
    void thread_tmp_nbreadreq_fu_116_p5();
    void thread_trunc_ln182_fu_344_p1();
    void thread_trunc_ln647_1_fu_619_p1();
    void thread_trunc_ln647_fu_497_p1();
    void thread_ttlIn_TDATA_blk_n();
    void thread_ttlIn_TREADY();
    void thread_udpIn_TDATA_blk_n();
    void thread_udpIn_TREADY();
    void thread_udpPort2addIpHeader_1_blk_n();
    void thread_udpPort2addIpHeader_1_din();
    void thread_udpPort2addIpHeader_1_write();
    void thread_udpPort2addIpHeader_6_blk_n();
    void thread_udpPort2addIpHeader_6_din();
    void thread_udpPort2addIpHeader_6_write();
    void thread_udpPort2addIpHeader_7_blk_n();
    void thread_udpPort2addIpHeader_7_din();
    void thread_udpPort2addIpHeader_7_write();
    void thread_udpPort2addIpHeader_8_blk_n();
    void thread_udpPort2addIpHeader_8_din();
    void thread_udpPort2addIpHeader_8_write();
    void thread_xor_ln201_fu_553_p2();
    void thread_xor_ln217_fu_431_p2();
    void thread_zext_ln1503_fu_362_p1();
    void thread_zext_ln209_10_fu_653_p1();
    void thread_zext_ln209_1_fu_475_p1();
    void thread_zext_ln209_2_fu_501_p1();
    void thread_zext_ln209_3_fu_511_p1();
    void thread_zext_ln209_4_fu_521_p1();
    void thread_zext_ln209_5_fu_531_p1();
    void thread_zext_ln209_6_fu_597_p1();
    void thread_zext_ln209_7_fu_623_p1();
    void thread_zext_ln209_8_fu_633_p1();
    void thread_zext_ln209_9_fu_643_p1();
    void thread_zext_ln209_fu_726_p1();
    void thread_zext_ln214_1_fu_382_p1();
    void thread_zext_ln214_2_fu_404_p1();
    void thread_zext_ln214_3_fu_372_p1();
    void thread_zext_ln214_fu_348_p1();
    void thread_zext_ln647_1_fu_493_p1();
    void thread_zext_ln647_2_fu_601_p1();
    void thread_zext_ln647_3_fu_615_p1();
    void thread_zext_ln647_fu_479_p1();
    void thread_ap_NS_fsm();
};

}

using namespace ap_rtl;

#endif
