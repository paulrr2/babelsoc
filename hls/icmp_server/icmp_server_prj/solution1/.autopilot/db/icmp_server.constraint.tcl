set clock_constraint { \
    name clk \
    module icmp_server \
    port ap_clk \
    period 6.4 \
    uncertainty 0.8 \
}

set all_path {}

set false_path {}

