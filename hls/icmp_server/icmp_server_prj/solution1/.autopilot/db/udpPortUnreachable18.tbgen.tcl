set moduleName udpPortUnreachable18
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 0
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 1
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {udpPortUnreachable18}
set C_modelType { void 0 }
set C_modelArgList {
	{ udpIn_V_data_V int 64 regular {axi_s 0 volatile  { udpIn Data } }  }
	{ udpIn_V_keep_V int 8 regular {axi_s 0 volatile  { udpIn Keep } }  }
	{ udpIn_V_last_V int 1 regular {axi_s 0 volatile  { udpIn Last } }  }
	{ ttlIn_V_data_V int 64 regular {axi_s 0 volatile  { ttlIn Data } }  }
	{ ttlIn_V_keep_V int 8 regular {axi_s 0 volatile  { ttlIn Keep } }  }
	{ ttlIn_V_last_V int 1 regular {axi_s 0 volatile  { ttlIn Last } }  }
	{ udpPort2addIpHeader_8 int 64 regular {fifo 1 volatile } {global 1}  }
	{ udpPort2addIpHeader_1 int 8 regular {fifo 1 volatile } {global 1}  }
	{ udpPort2addIpHeader_6 int 1 regular {fifo 1 volatile } {global 1}  }
	{ udpPort2addIpHeader_7 int 64 regular {fifo 1 volatile } {global 1}  }
	{ checksumStreams_V_V_1 int 16 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "udpIn_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "READONLY"} , 
 	{ "Name" : "udpIn_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "udpIn_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY"} , 
 	{ "Name" : "ttlIn_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "READONLY"} , 
 	{ "Name" : "ttlIn_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "ttlIn_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY"} , 
 	{ "Name" : "udpPort2addIpHeader_8", "interface" : "fifo", "bitwidth" : 64, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "udpPort2addIpHeader_1", "interface" : "fifo", "bitwidth" : 8, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "udpPort2addIpHeader_6", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "udpPort2addIpHeader_7", "interface" : "fifo", "bitwidth" : 64, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "checksumStreams_V_V_1", "interface" : "fifo", "bitwidth" : 16, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 32
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ udpIn_TDATA sc_in sc_lv 64 signal 0 } 
	{ udpIn_TVALID sc_in sc_logic 1 invld 0 } 
	{ udpIn_TREADY sc_out sc_logic 1 inacc 2 } 
	{ udpIn_TKEEP sc_in sc_lv 8 signal 1 } 
	{ udpIn_TLAST sc_in sc_lv 1 signal 2 } 
	{ ttlIn_TDATA sc_in sc_lv 64 signal 3 } 
	{ ttlIn_TVALID sc_in sc_logic 1 invld 3 } 
	{ ttlIn_TREADY sc_out sc_logic 1 inacc 5 } 
	{ ttlIn_TKEEP sc_in sc_lv 8 signal 4 } 
	{ ttlIn_TLAST sc_in sc_lv 1 signal 5 } 
	{ udpPort2addIpHeader_8_din sc_out sc_lv 64 signal 6 } 
	{ udpPort2addIpHeader_8_full_n sc_in sc_logic 1 signal 6 } 
	{ udpPort2addIpHeader_8_write sc_out sc_logic 1 signal 6 } 
	{ udpPort2addIpHeader_1_din sc_out sc_lv 8 signal 7 } 
	{ udpPort2addIpHeader_1_full_n sc_in sc_logic 1 signal 7 } 
	{ udpPort2addIpHeader_1_write sc_out sc_logic 1 signal 7 } 
	{ udpPort2addIpHeader_6_din sc_out sc_lv 1 signal 8 } 
	{ udpPort2addIpHeader_6_full_n sc_in sc_logic 1 signal 8 } 
	{ udpPort2addIpHeader_6_write sc_out sc_logic 1 signal 8 } 
	{ udpPort2addIpHeader_7_din sc_out sc_lv 64 signal 9 } 
	{ udpPort2addIpHeader_7_full_n sc_in sc_logic 1 signal 9 } 
	{ udpPort2addIpHeader_7_write sc_out sc_logic 1 signal 9 } 
	{ checksumStreams_V_V_1_din sc_out sc_lv 16 signal 10 } 
	{ checksumStreams_V_V_1_full_n sc_in sc_logic 1 signal 10 } 
	{ checksumStreams_V_V_1_write sc_out sc_logic 1 signal 10 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "udpIn_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "udpIn_V_data_V", "role": "default" }} , 
 	{ "name": "udpIn_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "udpIn_V_data_V", "role": "default" }} , 
 	{ "name": "udpIn_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "udpIn_V_last_V", "role": "default" }} , 
 	{ "name": "udpIn_TKEEP", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "udpIn_V_keep_V", "role": "default" }} , 
 	{ "name": "udpIn_TLAST", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "udpIn_V_last_V", "role": "default" }} , 
 	{ "name": "ttlIn_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "ttlIn_V_data_V", "role": "default" }} , 
 	{ "name": "ttlIn_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "ttlIn_V_data_V", "role": "default" }} , 
 	{ "name": "ttlIn_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "ttlIn_V_last_V", "role": "default" }} , 
 	{ "name": "ttlIn_TKEEP", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "ttlIn_V_keep_V", "role": "default" }} , 
 	{ "name": "ttlIn_TLAST", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "ttlIn_V_last_V", "role": "default" }} , 
 	{ "name": "udpPort2addIpHeader_8_din", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "udpPort2addIpHeader_8", "role": "din" }} , 
 	{ "name": "udpPort2addIpHeader_8_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "udpPort2addIpHeader_8", "role": "full_n" }} , 
 	{ "name": "udpPort2addIpHeader_8_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "udpPort2addIpHeader_8", "role": "write" }} , 
 	{ "name": "udpPort2addIpHeader_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "udpPort2addIpHeader_1", "role": "din" }} , 
 	{ "name": "udpPort2addIpHeader_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "udpPort2addIpHeader_1", "role": "full_n" }} , 
 	{ "name": "udpPort2addIpHeader_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "udpPort2addIpHeader_1", "role": "write" }} , 
 	{ "name": "udpPort2addIpHeader_6_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "udpPort2addIpHeader_6", "role": "din" }} , 
 	{ "name": "udpPort2addIpHeader_6_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "udpPort2addIpHeader_6", "role": "full_n" }} , 
 	{ "name": "udpPort2addIpHeader_6_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "udpPort2addIpHeader_6", "role": "write" }} , 
 	{ "name": "udpPort2addIpHeader_7_din", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "udpPort2addIpHeader_7", "role": "din" }} , 
 	{ "name": "udpPort2addIpHeader_7_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "udpPort2addIpHeader_7", "role": "full_n" }} , 
 	{ "name": "udpPort2addIpHeader_7_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "udpPort2addIpHeader_7", "role": "write" }} , 
 	{ "name": "checksumStreams_V_V_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "checksumStreams_V_V_1", "role": "din" }} , 
 	{ "name": "checksumStreams_V_V_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "checksumStreams_V_V_1", "role": "full_n" }} , 
 	{ "name": "checksumStreams_V_V_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "checksumStreams_V_V_1", "role": "write" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "udpPortUnreachable18",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "0", "EstimateLatencyMin" : "0", "EstimateLatencyMax" : "0",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "udpIn_V_data_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "udpIn_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "udpIn_V_keep_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "udpIn_V_last_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "ttlIn_V_data_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "ttlIn_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ttlIn_V_keep_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "ttlIn_V_last_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "udpState", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ipWordCounter_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "streamSource_V_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "udpChecksum_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "udpPort2addIpHeader_8", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "udpPort2addIpHeader_8_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "udpPort2addIpHeader_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "udpPort2addIpHeader_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "udpPort2addIpHeader_6", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "udpPort2addIpHeader_6_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "udpPort2addIpHeader_7", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "udpPort2addIpHeader_7_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "checksumStreams_V_V_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "checksumStreams_V_V_1_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	udpPortUnreachable18 {
		udpIn_V_data_V {Type I LastRead 0 FirstWrite -1}
		udpIn_V_keep_V {Type I LastRead 0 FirstWrite -1}
		udpIn_V_last_V {Type I LastRead 0 FirstWrite -1}
		ttlIn_V_data_V {Type I LastRead 0 FirstWrite -1}
		ttlIn_V_keep_V {Type I LastRead 0 FirstWrite -1}
		ttlIn_V_last_V {Type I LastRead 0 FirstWrite -1}
		udpState {Type IO LastRead -1 FirstWrite -1}
		ipWordCounter_V {Type IO LastRead -1 FirstWrite -1}
		streamSource_V_1 {Type IO LastRead -1 FirstWrite -1}
		udpChecksum_V {Type IO LastRead -1 FirstWrite -1}
		udpPort2addIpHeader_8 {Type O LastRead 0 FirstWrite 0}
		udpPort2addIpHeader_1 {Type O LastRead 0 FirstWrite 0}
		udpPort2addIpHeader_6 {Type O LastRead 0 FirstWrite 0}
		udpPort2addIpHeader_7 {Type O LastRead 0 FirstWrite 0}
		checksumStreams_V_V_1 {Type O LastRead 0 FirstWrite 0}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "0", "Max" : "0"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
]}

set Spec2ImplPortList { 
	udpIn_V_data_V { axis {  { udpIn_TDATA in_data 0 64 }  { udpIn_TVALID in_vld 0 1 } } }
	udpIn_V_keep_V { axis {  { udpIn_TKEEP in_data 0 8 } } }
	udpIn_V_last_V { axis {  { udpIn_TREADY in_acc 1 1 }  { udpIn_TLAST in_data 0 1 } } }
	ttlIn_V_data_V { axis {  { ttlIn_TDATA in_data 0 64 }  { ttlIn_TVALID in_vld 0 1 } } }
	ttlIn_V_keep_V { axis {  { ttlIn_TKEEP in_data 0 8 } } }
	ttlIn_V_last_V { axis {  { ttlIn_TREADY in_acc 1 1 }  { ttlIn_TLAST in_data 0 1 } } }
	udpPort2addIpHeader_8 { ap_fifo {  { udpPort2addIpHeader_8_din fifo_data 1 64 }  { udpPort2addIpHeader_8_full_n fifo_status 0 1 }  { udpPort2addIpHeader_8_write fifo_update 1 1 } } }
	udpPort2addIpHeader_1 { ap_fifo {  { udpPort2addIpHeader_1_din fifo_data 1 8 }  { udpPort2addIpHeader_1_full_n fifo_status 0 1 }  { udpPort2addIpHeader_1_write fifo_update 1 1 } } }
	udpPort2addIpHeader_6 { ap_fifo {  { udpPort2addIpHeader_6_din fifo_data 1 1 }  { udpPort2addIpHeader_6_full_n fifo_status 0 1 }  { udpPort2addIpHeader_6_write fifo_update 1 1 } } }
	udpPort2addIpHeader_7 { ap_fifo {  { udpPort2addIpHeader_7_din fifo_data 1 64 }  { udpPort2addIpHeader_7_full_n fifo_status 0 1 }  { udpPort2addIpHeader_7_write fifo_update 1 1 } } }
	checksumStreams_V_V_1 { ap_fifo {  { checksumStreams_V_V_1_din fifo_data 1 16 }  { checksumStreams_V_V_1_full_n fifo_status 0 1 }  { checksumStreams_V_V_1_write fifo_update 1 1 } } }
}
