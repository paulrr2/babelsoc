set moduleName check_icmp_checksum
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {check_icmp_checksum}
set C_modelType { void 0 }
set C_modelArgList {
	{ dataIn_V_data_V int 64 regular {axi_s 0 volatile  { s_axis Data } }  }
	{ dataIn_V_keep_V int 8 regular {axi_s 0 volatile  { s_axis Keep } }  }
	{ dataIn_V_last_V int 1 regular {axi_s 0 volatile  { s_axis Last } }  }
	{ packageBuffer1_V int 73 regular {fifo 1 volatile } {global 1}  }
	{ validFifo_V int 1 regular {fifo 1 volatile } {global 1}  }
	{ checksumStreams_V_V_s int 16 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "dataIn_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "READONLY"} , 
 	{ "Name" : "dataIn_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "dataIn_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY"} , 
 	{ "Name" : "packageBuffer1_V", "interface" : "fifo", "bitwidth" : 73, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "validFifo_V", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "checksumStreams_V_V_s", "interface" : "fifo", "bitwidth" : 16, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 21
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ s_axis_TVALID sc_in sc_logic 1 invld 0 } 
	{ packageBuffer1_V_din sc_out sc_lv 73 signal 3 } 
	{ packageBuffer1_V_full_n sc_in sc_logic 1 signal 3 } 
	{ packageBuffer1_V_write sc_out sc_logic 1 signal 3 } 
	{ validFifo_V_din sc_out sc_lv 1 signal 4 } 
	{ validFifo_V_full_n sc_in sc_logic 1 signal 4 } 
	{ validFifo_V_write sc_out sc_logic 1 signal 4 } 
	{ checksumStreams_V_V_s_din sc_out sc_lv 16 signal 5 } 
	{ checksumStreams_V_V_s_full_n sc_in sc_logic 1 signal 5 } 
	{ checksumStreams_V_V_s_write sc_out sc_logic 1 signal 5 } 
	{ s_axis_TDATA sc_in sc_lv 64 signal 0 } 
	{ s_axis_TREADY sc_out sc_logic 1 inacc 2 } 
	{ s_axis_TKEEP sc_in sc_lv 8 signal 1 } 
	{ s_axis_TLAST sc_in sc_lv 1 signal 2 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "s_axis_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "dataIn_V_data_V", "role": "default" }} , 
 	{ "name": "packageBuffer1_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":73, "type": "signal", "bundle":{"name": "packageBuffer1_V", "role": "din" }} , 
 	{ "name": "packageBuffer1_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "packageBuffer1_V", "role": "full_n" }} , 
 	{ "name": "packageBuffer1_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "packageBuffer1_V", "role": "write" }} , 
 	{ "name": "validFifo_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "validFifo_V", "role": "din" }} , 
 	{ "name": "validFifo_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "validFifo_V", "role": "full_n" }} , 
 	{ "name": "validFifo_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "validFifo_V", "role": "write" }} , 
 	{ "name": "checksumStreams_V_V_s_din", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "checksumStreams_V_V_s", "role": "din" }} , 
 	{ "name": "checksumStreams_V_V_s_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "checksumStreams_V_V_s", "role": "full_n" }} , 
 	{ "name": "checksumStreams_V_V_s_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "checksumStreams_V_V_s", "role": "write" }} , 
 	{ "name": "s_axis_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "dataIn_V_data_V", "role": "default" }} , 
 	{ "name": "s_axis_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "dataIn_V_last_V", "role": "default" }} , 
 	{ "name": "s_axis_TKEEP", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "dataIn_V_keep_V", "role": "default" }} , 
 	{ "name": "s_axis_TLAST", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "dataIn_V_last_V", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "check_icmp_checksum",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "dataIn_V_data_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "s_axis_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataIn_V_keep_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "dataIn_V_last_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "cics_writeLastOne", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_prevWord_data_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_prevWord_keep_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_prevWord_last_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "packageBuffer1_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "packageBuffer1_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "cics_computeCs", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_sums_V_2", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_sums_V_0", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_sums_V_3", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_sums_V_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "icmpChecksum_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "icmpType_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "icmpCode_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_state_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "validFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "validFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "checksumStreams_V_V_s", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "checksumStreams_V_V_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "cics_wordCount_V", "Type" : "OVld", "Direction" : "IO"}]}]}


set ArgLastReadFirstWriteLatency {
	check_icmp_checksum {
		dataIn_V_data_V {Type I LastRead 0 FirstWrite -1}
		dataIn_V_keep_V {Type I LastRead 0 FirstWrite -1}
		dataIn_V_last_V {Type I LastRead 0 FirstWrite -1}
		cics_writeLastOne {Type IO LastRead -1 FirstWrite -1}
		cics_prevWord_data_V {Type IO LastRead -1 FirstWrite -1}
		cics_prevWord_keep_V {Type IO LastRead -1 FirstWrite -1}
		cics_prevWord_last_V {Type IO LastRead -1 FirstWrite -1}
		packageBuffer1_V {Type O LastRead -1 FirstWrite 1}
		cics_computeCs {Type IO LastRead -1 FirstWrite -1}
		cics_sums_V_2 {Type IO LastRead -1 FirstWrite -1}
		cics_sums_V_0 {Type IO LastRead -1 FirstWrite -1}
		cics_sums_V_3 {Type IO LastRead -1 FirstWrite -1}
		cics_sums_V_1 {Type IO LastRead -1 FirstWrite -1}
		icmpChecksum_V {Type IO LastRead -1 FirstWrite -1}
		icmpType_V {Type IO LastRead -1 FirstWrite -1}
		icmpCode_V {Type IO LastRead -1 FirstWrite -1}
		cics_state_V {Type IO LastRead -1 FirstWrite -1}
		validFifo_V {Type O LastRead -1 FirstWrite 1}
		checksumStreams_V_V_s {Type O LastRead -1 FirstWrite 1}
		cics_wordCount_V {Type IO LastRead -1 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "1", "Max" : "1"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	dataIn_V_data_V { axis {  { s_axis_TVALID in_vld 0 1 }  { s_axis_TDATA in_data 0 64 } } }
	dataIn_V_keep_V { axis {  { s_axis_TKEEP in_data 0 8 } } }
	dataIn_V_last_V { axis {  { s_axis_TREADY in_acc 1 1 }  { s_axis_TLAST in_data 0 1 } } }
	packageBuffer1_V { ap_fifo {  { packageBuffer1_V_din fifo_data 1 73 }  { packageBuffer1_V_full_n fifo_status 0 1 }  { packageBuffer1_V_write fifo_update 1 1 } } }
	validFifo_V { ap_fifo {  { validFifo_V_din fifo_data 1 1 }  { validFifo_V_full_n fifo_status 0 1 }  { validFifo_V_write fifo_update 1 1 } } }
	checksumStreams_V_V_s { ap_fifo {  { checksumStreams_V_V_s_din fifo_data 1 16 }  { checksumStreams_V_V_s_full_n fifo_status 0 1 }  { checksumStreams_V_V_s_write fifo_update 1 1 } } }
}
