set moduleName insertChecksum
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {insertChecksum}
set C_modelType { void 0 }
set C_modelArgList {
	{ outputStream_V_data_V int 64 regular {axi_s 1 volatile  { m_axis Data } }  }
	{ outputStream_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis Keep } }  }
	{ outputStream_V_last_V int 1 regular {axi_s 1 volatile  { m_axis Last } }  }
	{ dataStreams_V_data_V int 64 regular {fifo 0 volatile } {global 0}  }
	{ dataStreams_V_keep_V int 8 regular {fifo 0 volatile } {global 0}  }
	{ dataStreams_V_last_V int 1 regular {fifo 0 volatile } {global 0}  }
	{ dataStreams_V_data_V_1 int 64 regular {fifo 0 volatile } {global 0}  }
	{ dataStreams_V_keep_V_1 int 8 regular {fifo 0 volatile } {global 0}  }
	{ dataStreams_V_last_V_1 int 1 regular {fifo 0 volatile } {global 0}  }
	{ checksumStreams_V_V_s int 16 regular {fifo 0 volatile } {global 0}  }
	{ checksumStreams_V_V_1 int 16 regular {fifo 0 volatile } {global 0}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "outputStream_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY"} , 
 	{ "Name" : "outputStream_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY"} , 
 	{ "Name" : "outputStream_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY"} , 
 	{ "Name" : "dataStreams_V_data_V", "interface" : "fifo", "bitwidth" : 64, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "dataStreams_V_keep_V", "interface" : "fifo", "bitwidth" : 8, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "dataStreams_V_last_V", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "dataStreams_V_data_V_1", "interface" : "fifo", "bitwidth" : 64, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "dataStreams_V_keep_V_1", "interface" : "fifo", "bitwidth" : 8, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "dataStreams_V_last_V_1", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "checksumStreams_V_V_s", "interface" : "fifo", "bitwidth" : 16, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "checksumStreams_V_V_1", "interface" : "fifo", "bitwidth" : 16, "direction" : "READONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 36
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ dataStreams_V_data_V_dout sc_in sc_lv 64 signal 3 } 
	{ dataStreams_V_data_V_empty_n sc_in sc_logic 1 signal 3 } 
	{ dataStreams_V_data_V_read sc_out sc_logic 1 signal 3 } 
	{ dataStreams_V_keep_V_dout sc_in sc_lv 8 signal 4 } 
	{ dataStreams_V_keep_V_empty_n sc_in sc_logic 1 signal 4 } 
	{ dataStreams_V_keep_V_read sc_out sc_logic 1 signal 4 } 
	{ dataStreams_V_last_V_dout sc_in sc_lv 1 signal 5 } 
	{ dataStreams_V_last_V_empty_n sc_in sc_logic 1 signal 5 } 
	{ dataStreams_V_last_V_read sc_out sc_logic 1 signal 5 } 
	{ dataStreams_V_data_V_1_dout sc_in sc_lv 64 signal 6 } 
	{ dataStreams_V_data_V_1_empty_n sc_in sc_logic 1 signal 6 } 
	{ dataStreams_V_data_V_1_read sc_out sc_logic 1 signal 6 } 
	{ dataStreams_V_keep_V_1_dout sc_in sc_lv 8 signal 7 } 
	{ dataStreams_V_keep_V_1_empty_n sc_in sc_logic 1 signal 7 } 
	{ dataStreams_V_keep_V_1_read sc_out sc_logic 1 signal 7 } 
	{ dataStreams_V_last_V_1_dout sc_in sc_lv 1 signal 8 } 
	{ dataStreams_V_last_V_1_empty_n sc_in sc_logic 1 signal 8 } 
	{ dataStreams_V_last_V_1_read sc_out sc_logic 1 signal 8 } 
	{ checksumStreams_V_V_s_dout sc_in sc_lv 16 signal 9 } 
	{ checksumStreams_V_V_s_empty_n sc_in sc_logic 1 signal 9 } 
	{ checksumStreams_V_V_s_read sc_out sc_logic 1 signal 9 } 
	{ checksumStreams_V_V_1_dout sc_in sc_lv 16 signal 10 } 
	{ checksumStreams_V_V_1_empty_n sc_in sc_logic 1 signal 10 } 
	{ checksumStreams_V_V_1_read sc_out sc_logic 1 signal 10 } 
	{ m_axis_TREADY sc_in sc_logic 1 outacc 2 } 
	{ m_axis_TDATA sc_out sc_lv 64 signal 0 } 
	{ m_axis_TVALID sc_out sc_logic 1 outvld 2 } 
	{ m_axis_TKEEP sc_out sc_lv 8 signal 1 } 
	{ m_axis_TLAST sc_out sc_lv 1 signal 2 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "dataStreams_V_data_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "dataStreams_V_data_V", "role": "dout" }} , 
 	{ "name": "dataStreams_V_data_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataStreams_V_data_V", "role": "empty_n" }} , 
 	{ "name": "dataStreams_V_data_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataStreams_V_data_V", "role": "read" }} , 
 	{ "name": "dataStreams_V_keep_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "dataStreams_V_keep_V", "role": "dout" }} , 
 	{ "name": "dataStreams_V_keep_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataStreams_V_keep_V", "role": "empty_n" }} , 
 	{ "name": "dataStreams_V_keep_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataStreams_V_keep_V", "role": "read" }} , 
 	{ "name": "dataStreams_V_last_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "dataStreams_V_last_V", "role": "dout" }} , 
 	{ "name": "dataStreams_V_last_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataStreams_V_last_V", "role": "empty_n" }} , 
 	{ "name": "dataStreams_V_last_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataStreams_V_last_V", "role": "read" }} , 
 	{ "name": "dataStreams_V_data_V_1_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "dataStreams_V_data_V_1", "role": "dout" }} , 
 	{ "name": "dataStreams_V_data_V_1_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataStreams_V_data_V_1", "role": "empty_n" }} , 
 	{ "name": "dataStreams_V_data_V_1_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataStreams_V_data_V_1", "role": "read" }} , 
 	{ "name": "dataStreams_V_keep_V_1_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "dataStreams_V_keep_V_1", "role": "dout" }} , 
 	{ "name": "dataStreams_V_keep_V_1_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataStreams_V_keep_V_1", "role": "empty_n" }} , 
 	{ "name": "dataStreams_V_keep_V_1_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataStreams_V_keep_V_1", "role": "read" }} , 
 	{ "name": "dataStreams_V_last_V_1_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "dataStreams_V_last_V_1", "role": "dout" }} , 
 	{ "name": "dataStreams_V_last_V_1_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataStreams_V_last_V_1", "role": "empty_n" }} , 
 	{ "name": "dataStreams_V_last_V_1_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataStreams_V_last_V_1", "role": "read" }} , 
 	{ "name": "checksumStreams_V_V_s_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "checksumStreams_V_V_s", "role": "dout" }} , 
 	{ "name": "checksumStreams_V_V_s_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "checksumStreams_V_V_s", "role": "empty_n" }} , 
 	{ "name": "checksumStreams_V_V_s_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "checksumStreams_V_V_s", "role": "read" }} , 
 	{ "name": "checksumStreams_V_V_1_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "checksumStreams_V_V_1", "role": "dout" }} , 
 	{ "name": "checksumStreams_V_V_1_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "checksumStreams_V_V_1", "role": "empty_n" }} , 
 	{ "name": "checksumStreams_V_V_1_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "checksumStreams_V_V_1", "role": "read" }} , 
 	{ "name": "m_axis_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "outputStream_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "outputStream_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "outputStream_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "outputStream_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "outputStream_V_last_V", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2", "3"],
		"CDFG" : "insertChecksum",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "3", "EstimateLatencyMin" : "3", "EstimateLatencyMax" : "3",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "outputStream_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "outputStream_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "outputStream_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "ic_wordCount_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "streamSource_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "dataStreams_V_data_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "dataStreams_V_data_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataStreams_V_keep_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "dataStreams_V_keep_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataStreams_V_last_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "dataStreams_V_last_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataStreams_V_data_V_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "dataStreams_V_data_V_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataStreams_V_keep_V_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "dataStreams_V_keep_V_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataStreams_V_last_V_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "dataStreams_V_last_V_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "checksumStreams_V_V_s", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "checksumStreams_V_V_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "checksumStreams_V_V_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "checksumStreams_V_V_1_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_outputStream_V_data_V_U", "Parent" : "0"},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_outputStream_V_keep_V_U", "Parent" : "0"},
	{"ID" : "3", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_outputStream_V_last_V_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	insertChecksum {
		outputStream_V_data_V {Type O LastRead -1 FirstWrite 2}
		outputStream_V_keep_V {Type O LastRead -1 FirstWrite 2}
		outputStream_V_last_V {Type O LastRead -1 FirstWrite 2}
		ic_wordCount_V {Type IO LastRead -1 FirstWrite -1}
		streamSource_V {Type IO LastRead -1 FirstWrite -1}
		dataStreams_V_data_V {Type I LastRead 1 FirstWrite -1}
		dataStreams_V_keep_V {Type I LastRead 1 FirstWrite -1}
		dataStreams_V_last_V {Type I LastRead 1 FirstWrite -1}
		dataStreams_V_data_V_1 {Type I LastRead 1 FirstWrite -1}
		dataStreams_V_keep_V_1 {Type I LastRead 1 FirstWrite -1}
		dataStreams_V_last_V_1 {Type I LastRead 1 FirstWrite -1}
		checksumStreams_V_V_s {Type I LastRead 1 FirstWrite -1}
		checksumStreams_V_V_1 {Type I LastRead 1 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "3", "Max" : "3"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	outputStream_V_data_V { axis {  { m_axis_TDATA out_data 1 64 } } }
	outputStream_V_keep_V { axis {  { m_axis_TKEEP out_data 1 8 } } }
	outputStream_V_last_V { axis {  { m_axis_TREADY out_acc 0 1 }  { m_axis_TVALID out_vld 1 1 }  { m_axis_TLAST out_data 1 1 } } }
	dataStreams_V_data_V { ap_fifo {  { dataStreams_V_data_V_dout fifo_data 0 64 }  { dataStreams_V_data_V_empty_n fifo_status 0 1 }  { dataStreams_V_data_V_read fifo_update 1 1 } } }
	dataStreams_V_keep_V { ap_fifo {  { dataStreams_V_keep_V_dout fifo_data 0 8 }  { dataStreams_V_keep_V_empty_n fifo_status 0 1 }  { dataStreams_V_keep_V_read fifo_update 1 1 } } }
	dataStreams_V_last_V { ap_fifo {  { dataStreams_V_last_V_dout fifo_data 0 1 }  { dataStreams_V_last_V_empty_n fifo_status 0 1 }  { dataStreams_V_last_V_read fifo_update 1 1 } } }
	dataStreams_V_data_V_1 { ap_fifo {  { dataStreams_V_data_V_1_dout fifo_data 0 64 }  { dataStreams_V_data_V_1_empty_n fifo_status 0 1 }  { dataStreams_V_data_V_1_read fifo_update 1 1 } } }
	dataStreams_V_keep_V_1 { ap_fifo {  { dataStreams_V_keep_V_1_dout fifo_data 0 8 }  { dataStreams_V_keep_V_1_empty_n fifo_status 0 1 }  { dataStreams_V_keep_V_1_read fifo_update 1 1 } } }
	dataStreams_V_last_V_1 { ap_fifo {  { dataStreams_V_last_V_1_dout fifo_data 0 1 }  { dataStreams_V_last_V_1_empty_n fifo_status 0 1 }  { dataStreams_V_last_V_1_read fifo_update 1 1 } } }
	checksumStreams_V_V_s { ap_fifo {  { checksumStreams_V_V_s_dout fifo_data 0 16 }  { checksumStreams_V_V_s_empty_n fifo_status 0 1 }  { checksumStreams_V_V_s_read fifo_update 1 1 } } }
	checksumStreams_V_V_1 { ap_fifo {  { checksumStreams_V_V_1_dout fifo_data 0 16 }  { checksumStreams_V_V_1_empty_n fifo_status 0 1 }  { checksumStreams_V_V_1_read fifo_update 1 1 } } }
}
