set moduleName icmp_server
set isTopModule 1
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type dataflow
set FunctionProtocol ap_ctrl_none
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {icmp_server}
set C_modelType { void 0 }
set C_modelArgList {
	{ s_axis_V_data_V int 64 regular {axi_s 0 volatile  { s_axis Data } }  }
	{ s_axis_V_keep_V int 8 regular {axi_s 0 volatile  { s_axis Keep } }  }
	{ s_axis_V_last_V int 1 regular {axi_s 0 volatile  { s_axis Last } }  }
	{ udpIn_V_data_V int 64 regular {axi_s 0 volatile  { udpIn Data } }  }
	{ udpIn_V_keep_V int 8 regular {axi_s 0 volatile  { udpIn Keep } }  }
	{ udpIn_V_last_V int 1 regular {axi_s 0 volatile  { udpIn Last } }  }
	{ ttlIn_V_data_V int 64 regular {axi_s 0 volatile  { ttlIn Data } }  }
	{ ttlIn_V_keep_V int 8 regular {axi_s 0 volatile  { ttlIn Keep } }  }
	{ ttlIn_V_last_V int 1 regular {axi_s 0 volatile  { ttlIn Last } }  }
	{ m_axis_V_data_V int 64 regular {axi_s 1 volatile  { m_axis Data } }  }
	{ m_axis_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis Keep } }  }
	{ m_axis_V_last_V int 1 regular {axi_s 1 volatile  { m_axis Last } }  }
}
set C_modelArgMapList {[ 
	{ "Name" : "s_axis_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "READONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "s_axis.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "s_axis.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "s_axis_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "s_axis.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "udpIn_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "READONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "udpIn.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "udpIn_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "udpIn.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "udpIn_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "udpIn.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "ttlIn_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "READONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "ttlIn.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "ttlIn_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "ttlIn.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "ttlIn_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "ttlIn.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":63,"cElement": [{"cName": "m_axis.V.data.V","cData": "uint64","bit_use": { "low": 0,"up": 63},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":7,"cElement": [{"cName": "m_axis.V.keep.V","cData": "uint8","bit_use": { "low": 0,"up": 7},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "m_axis_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "m_axis.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} ]}
# RTL Port declarations: 
set portNum 22
set portList { 
	{ s_axis_TDATA sc_in sc_lv 64 signal 0 } 
	{ s_axis_TKEEP sc_in sc_lv 8 signal 1 } 
	{ s_axis_TLAST sc_in sc_lv 1 signal 2 } 
	{ udpIn_TDATA sc_in sc_lv 64 signal 3 } 
	{ udpIn_TKEEP sc_in sc_lv 8 signal 4 } 
	{ udpIn_TLAST sc_in sc_lv 1 signal 5 } 
	{ ttlIn_TDATA sc_in sc_lv 64 signal 6 } 
	{ ttlIn_TKEEP sc_in sc_lv 8 signal 7 } 
	{ ttlIn_TLAST sc_in sc_lv 1 signal 8 } 
	{ m_axis_TDATA sc_out sc_lv 64 signal 9 } 
	{ m_axis_TKEEP sc_out sc_lv 8 signal 10 } 
	{ m_axis_TLAST sc_out sc_lv 1 signal 11 } 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst_n sc_in sc_logic 1 reset -1 active_low_sync } 
	{ s_axis_TVALID sc_in sc_logic 1 invld 2 } 
	{ s_axis_TREADY sc_out sc_logic 1 inacc 2 } 
	{ udpIn_TVALID sc_in sc_logic 1 invld 5 } 
	{ udpIn_TREADY sc_out sc_logic 1 inacc 5 } 
	{ ttlIn_TVALID sc_in sc_logic 1 invld 8 } 
	{ ttlIn_TREADY sc_out sc_logic 1 inacc 8 } 
	{ m_axis_TVALID sc_out sc_logic 1 outvld 11 } 
	{ m_axis_TREADY sc_in sc_logic 1 outacc 11 } 
}
set NewPortList {[ 
	{ "name": "s_axis_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "s_axis_V_data_V", "role": "default" }} , 
 	{ "name": "s_axis_TKEEP", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "s_axis_V_keep_V", "role": "default" }} , 
 	{ "name": "s_axis_TLAST", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "s_axis_V_last_V", "role": "default" }} , 
 	{ "name": "udpIn_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "udpIn_V_data_V", "role": "default" }} , 
 	{ "name": "udpIn_TKEEP", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "udpIn_V_keep_V", "role": "default" }} , 
 	{ "name": "udpIn_TLAST", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "udpIn_V_last_V", "role": "default" }} , 
 	{ "name": "ttlIn_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "ttlIn_V_data_V", "role": "default" }} , 
 	{ "name": "ttlIn_TKEEP", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "ttlIn_V_keep_V", "role": "default" }} , 
 	{ "name": "ttlIn_TLAST", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "ttlIn_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "m_axis_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "m_axis_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "m_axis_V_last_V", "role": "default" }} , 
 	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst_n", "role": "default" }} , 
 	{ "name": "s_axis_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "s_axis_V_last_V", "role": "default" }} , 
 	{ "name": "s_axis_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "s_axis_V_last_V", "role": "default" }} , 
 	{ "name": "udpIn_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "udpIn_V_last_V", "role": "default" }} , 
 	{ "name": "udpIn_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "udpIn_V_last_V", "role": "default" }} , 
 	{ "name": "ttlIn_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "ttlIn_V_last_V", "role": "default" }} , 
 	{ "name": "ttlIn_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "ttlIn_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_V_last_V", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2", "3", "4", "5", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22"],
		"CDFG" : "icmp_server",
		"Protocol" : "ap_ctrl_none",
		"ControlExist" : "0", "ap_start" : "0", "ap_ready" : "0", "ap_done" : "0", "ap_continue" : "0", "ap_idle" : "0",
		"Pipeline" : "Dataflow", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "1",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "5", "EstimateLatencyMax" : "5",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "1",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"InputProcess" : [
			{"ID" : "1", "Name" : "check_icmp_checksum_U0"},
			{"ID" : "2", "Name" : "udpPortUnreachable18_U0"}],
		"OutputProcess" : [
			{"ID" : "5", "Name" : "insertChecksum_U0"}],
		"Port" : [
			{"Name" : "s_axis_V_data_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "check_icmp_checksum_U0", "Port" : "dataIn_V_data_V"}]},
			{"Name" : "s_axis_V_keep_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "check_icmp_checksum_U0", "Port" : "dataIn_V_keep_V"}]},
			{"Name" : "s_axis_V_last_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "check_icmp_checksum_U0", "Port" : "dataIn_V_last_V"}]},
			{"Name" : "udpIn_V_data_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "udpPortUnreachable18_U0", "Port" : "udpIn_V_data_V"}]},
			{"Name" : "udpIn_V_keep_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "udpPortUnreachable18_U0", "Port" : "udpIn_V_keep_V"}]},
			{"Name" : "udpIn_V_last_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "udpPortUnreachable18_U0", "Port" : "udpIn_V_last_V"}]},
			{"Name" : "ttlIn_V_data_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "udpPortUnreachable18_U0", "Port" : "ttlIn_V_data_V"}]},
			{"Name" : "ttlIn_V_keep_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "udpPortUnreachable18_U0", "Port" : "ttlIn_V_keep_V"}]},
			{"Name" : "ttlIn_V_last_V", "Type" : "Axis", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "udpPortUnreachable18_U0", "Port" : "ttlIn_V_last_V"}]},
			{"Name" : "m_axis_V_data_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "5", "SubInstance" : "insertChecksum_U0", "Port" : "outputStream_V_data_V"}]},
			{"Name" : "m_axis_V_keep_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "5", "SubInstance" : "insertChecksum_U0", "Port" : "outputStream_V_keep_V"}]},
			{"Name" : "m_axis_V_last_V", "Type" : "Axis", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "5", "SubInstance" : "insertChecksum_U0", "Port" : "outputStream_V_last_V"}]},
			{"Name" : "cics_writeLastOne", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "check_icmp_checksum_U0", "Port" : "cics_writeLastOne"}]},
			{"Name" : "cics_prevWord_data_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "check_icmp_checksum_U0", "Port" : "cics_prevWord_data_V"}]},
			{"Name" : "cics_prevWord_keep_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "check_icmp_checksum_U0", "Port" : "cics_prevWord_keep_V"}]},
			{"Name" : "cics_prevWord_last_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "check_icmp_checksum_U0", "Port" : "cics_prevWord_last_V"}]},
			{"Name" : "packageBuffer1_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "check_icmp_checksum_U0", "Port" : "packageBuffer1_V"},
					{"ID" : "4", "SubInstance" : "dropper_U0", "Port" : "packageBuffer1_V"}]},
			{"Name" : "cics_computeCs", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "check_icmp_checksum_U0", "Port" : "cics_computeCs"}]},
			{"Name" : "cics_sums_V_2", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "check_icmp_checksum_U0", "Port" : "cics_sums_V_2"}]},
			{"Name" : "cics_sums_V_0", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "check_icmp_checksum_U0", "Port" : "cics_sums_V_0"}]},
			{"Name" : "cics_sums_V_3", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "check_icmp_checksum_U0", "Port" : "cics_sums_V_3"}]},
			{"Name" : "cics_sums_V_1", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "check_icmp_checksum_U0", "Port" : "cics_sums_V_1"}]},
			{"Name" : "icmpChecksum_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "check_icmp_checksum_U0", "Port" : "icmpChecksum_V"}]},
			{"Name" : "icmpType_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "check_icmp_checksum_U0", "Port" : "icmpType_V"}]},
			{"Name" : "icmpCode_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "check_icmp_checksum_U0", "Port" : "icmpCode_V"}]},
			{"Name" : "cics_state_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "check_icmp_checksum_U0", "Port" : "cics_state_V"}]},
			{"Name" : "validFifo_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "check_icmp_checksum_U0", "Port" : "validFifo_V"},
					{"ID" : "4", "SubInstance" : "dropper_U0", "Port" : "validFifo_V"}]},
			{"Name" : "checksumStreams_V_V_s", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "check_icmp_checksum_U0", "Port" : "checksumStreams_V_V_s"},
					{"ID" : "5", "SubInstance" : "insertChecksum_U0", "Port" : "checksumStreams_V_V_s"}]},
			{"Name" : "cics_wordCount_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "check_icmp_checksum_U0", "Port" : "cics_wordCount_V"}]},
			{"Name" : "udpState", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "udpPortUnreachable18_U0", "Port" : "udpState"}]},
			{"Name" : "ipWordCounter_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "udpPortUnreachable18_U0", "Port" : "ipWordCounter_V"}]},
			{"Name" : "streamSource_V_1", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "udpPortUnreachable18_U0", "Port" : "streamSource_V_1"}]},
			{"Name" : "udpChecksum_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "udpPortUnreachable18_U0", "Port" : "udpChecksum_V"}]},
			{"Name" : "udpPort2addIpHeader_8", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "udpAddIpHeader186_U0", "Port" : "udpPort2addIpHeader_8"},
					{"ID" : "2", "SubInstance" : "udpPortUnreachable18_U0", "Port" : "udpPort2addIpHeader_8"}]},
			{"Name" : "udpPort2addIpHeader_1", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "udpAddIpHeader186_U0", "Port" : "udpPort2addIpHeader_1"},
					{"ID" : "2", "SubInstance" : "udpPortUnreachable18_U0", "Port" : "udpPort2addIpHeader_1"}]},
			{"Name" : "udpPort2addIpHeader_6", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "udpAddIpHeader186_U0", "Port" : "udpPort2addIpHeader_6"},
					{"ID" : "2", "SubInstance" : "udpPortUnreachable18_U0", "Port" : "udpPort2addIpHeader_6"}]},
			{"Name" : "udpPort2addIpHeader_7", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "udpAddIpHeader186_U0", "Port" : "udpPort2addIpHeader_7"},
					{"ID" : "2", "SubInstance" : "udpPortUnreachable18_U0", "Port" : "udpPort2addIpHeader_7"}]},
			{"Name" : "checksumStreams_V_V_1", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "2", "SubInstance" : "udpPortUnreachable18_U0", "Port" : "checksumStreams_V_V_1"},
					{"ID" : "5", "SubInstance" : "insertChecksum_U0", "Port" : "checksumStreams_V_V_1"}]},
			{"Name" : "addIpState", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "udpAddIpHeader186_U0", "Port" : "addIpState"}]},
			{"Name" : "tempWord_data_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "udpAddIpHeader186_U0", "Port" : "tempWord_data_V"}]},
			{"Name" : "dataStreams_V_data_V_1", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "udpAddIpHeader186_U0", "Port" : "dataStreams_V_data_V_1"},
					{"ID" : "5", "SubInstance" : "insertChecksum_U0", "Port" : "dataStreams_V_data_V_1"}]},
			{"Name" : "dataStreams_V_keep_V_1", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "udpAddIpHeader186_U0", "Port" : "dataStreams_V_keep_V_1"},
					{"ID" : "5", "SubInstance" : "insertChecksum_U0", "Port" : "dataStreams_V_keep_V_1"}]},
			{"Name" : "dataStreams_V_last_V_1", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "udpAddIpHeader186_U0", "Port" : "dataStreams_V_last_V_1"},
					{"ID" : "5", "SubInstance" : "insertChecksum_U0", "Port" : "dataStreams_V_last_V_1"}]},
			{"Name" : "tempWord_keep_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "udpAddIpHeader186_U0", "Port" : "tempWord_keep_V"}]},
			{"Name" : "sourceIP_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "udpAddIpHeader186_U0", "Port" : "sourceIP_V"}]},
			{"Name" : "d_isFirstWord", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "dropper_U0", "Port" : "d_isFirstWord"}]},
			{"Name" : "d_drop", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "dropper_U0", "Port" : "d_drop"}]},
			{"Name" : "dataStreams_V_data_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "dropper_U0", "Port" : "dataStreams_V_data_V"},
					{"ID" : "5", "SubInstance" : "insertChecksum_U0", "Port" : "dataStreams_V_data_V"}]},
			{"Name" : "dataStreams_V_keep_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "dropper_U0", "Port" : "dataStreams_V_keep_V"},
					{"ID" : "5", "SubInstance" : "insertChecksum_U0", "Port" : "dataStreams_V_keep_V"}]},
			{"Name" : "dataStreams_V_last_V", "Type" : "Fifo", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "dropper_U0", "Port" : "dataStreams_V_last_V"},
					{"ID" : "5", "SubInstance" : "insertChecksum_U0", "Port" : "dataStreams_V_last_V"}]},
			{"Name" : "ic_wordCount_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "5", "SubInstance" : "insertChecksum_U0", "Port" : "ic_wordCount_V"}]},
			{"Name" : "streamSource_V", "Type" : "OVld", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "5", "SubInstance" : "insertChecksum_U0", "Port" : "streamSource_V"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.check_icmp_checksum_U0", "Parent" : "0",
		"CDFG" : "check_icmp_checksum",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "1", "EstimateLatencyMin" : "1", "EstimateLatencyMax" : "1",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "dataIn_V_data_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "s_axis_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataIn_V_keep_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "dataIn_V_last_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "cics_writeLastOne", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_prevWord_data_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_prevWord_keep_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_prevWord_last_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "packageBuffer1_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "4", "DependentChan" : "9",
				"BlockSignal" : [
					{"Name" : "packageBuffer1_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "cics_computeCs", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_sums_V_2", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_sums_V_0", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_sums_V_3", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_sums_V_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "icmpChecksum_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "icmpType_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "icmpCode_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "cics_state_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "validFifo_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "4", "DependentChan" : "10",
				"BlockSignal" : [
					{"Name" : "validFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "checksumStreams_V_V_s", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "5", "DependentChan" : "11",
				"BlockSignal" : [
					{"Name" : "checksumStreams_V_V_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "cics_wordCount_V", "Type" : "OVld", "Direction" : "IO"}]},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.udpPortUnreachable18_U0", "Parent" : "0",
		"CDFG" : "udpPortUnreachable18",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "0", "EstimateLatencyMin" : "0", "EstimateLatencyMax" : "0",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "udpIn_V_data_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "udpIn_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "udpIn_V_keep_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "udpIn_V_last_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "ttlIn_V_data_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "ttlIn_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "ttlIn_V_keep_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "ttlIn_V_last_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "udpState", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "ipWordCounter_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "streamSource_V_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "udpChecksum_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "udpPort2addIpHeader_8", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "3", "DependentChan" : "12",
				"BlockSignal" : [
					{"Name" : "udpPort2addIpHeader_8_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "udpPort2addIpHeader_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "3", "DependentChan" : "13",
				"BlockSignal" : [
					{"Name" : "udpPort2addIpHeader_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "udpPort2addIpHeader_6", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "3", "DependentChan" : "14",
				"BlockSignal" : [
					{"Name" : "udpPort2addIpHeader_6_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "udpPort2addIpHeader_7", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "3", "DependentChan" : "15",
				"BlockSignal" : [
					{"Name" : "udpPort2addIpHeader_7_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "checksumStreams_V_V_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "5", "DependentChan" : "16",
				"BlockSignal" : [
					{"Name" : "checksumStreams_V_V_1_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "3", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.udpAddIpHeader186_U0", "Parent" : "0",
		"CDFG" : "udpAddIpHeader186",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "0", "EstimateLatencyMin" : "0", "EstimateLatencyMax" : "0",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "addIpState", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tempWord_data_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "udpPort2addIpHeader_7", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "15",
				"BlockSignal" : [
					{"Name" : "udpPort2addIpHeader_7_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataStreams_V_data_V_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "5", "DependentChan" : "17",
				"BlockSignal" : [
					{"Name" : "dataStreams_V_data_V_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataStreams_V_keep_V_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "5", "DependentChan" : "18",
				"BlockSignal" : [
					{"Name" : "dataStreams_V_keep_V_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataStreams_V_last_V_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "5", "DependentChan" : "19",
				"BlockSignal" : [
					{"Name" : "dataStreams_V_last_V_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tempWord_keep_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "sourceIP_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "udpPort2addIpHeader_8", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "12",
				"BlockSignal" : [
					{"Name" : "udpPort2addIpHeader_8_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "udpPort2addIpHeader_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "13",
				"BlockSignal" : [
					{"Name" : "udpPort2addIpHeader_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "udpPort2addIpHeader_6", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "14",
				"BlockSignal" : [
					{"Name" : "udpPort2addIpHeader_6_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "4", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.dropper_U0", "Parent" : "0",
		"CDFG" : "dropper",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "0", "EstimateLatencyMin" : "0", "EstimateLatencyMax" : "0",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "packageBuffer1_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "1", "DependentChan" : "9",
				"BlockSignal" : [
					{"Name" : "packageBuffer1_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "d_isFirstWord", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "d_drop", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "validFifo_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "1", "DependentChan" : "10",
				"BlockSignal" : [
					{"Name" : "validFifo_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataStreams_V_data_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "5", "DependentChan" : "20",
				"BlockSignal" : [
					{"Name" : "dataStreams_V_data_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataStreams_V_keep_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "5", "DependentChan" : "21",
				"BlockSignal" : [
					{"Name" : "dataStreams_V_keep_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataStreams_V_last_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "5", "DependentChan" : "22",
				"BlockSignal" : [
					{"Name" : "dataStreams_V_last_V_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "5", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.insertChecksum_U0", "Parent" : "0", "Child" : ["6", "7", "8"],
		"CDFG" : "insertChecksum",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "3", "EstimateLatencyMin" : "3", "EstimateLatencyMax" : "3",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "outputStream_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "outputStream_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "outputStream_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "ic_wordCount_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "streamSource_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "dataStreams_V_data_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "4", "DependentChan" : "20",
				"BlockSignal" : [
					{"Name" : "dataStreams_V_data_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataStreams_V_keep_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "4", "DependentChan" : "21",
				"BlockSignal" : [
					{"Name" : "dataStreams_V_keep_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataStreams_V_last_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "4", "DependentChan" : "22",
				"BlockSignal" : [
					{"Name" : "dataStreams_V_last_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataStreams_V_data_V_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "3", "DependentChan" : "17",
				"BlockSignal" : [
					{"Name" : "dataStreams_V_data_V_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataStreams_V_keep_V_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "3", "DependentChan" : "18",
				"BlockSignal" : [
					{"Name" : "dataStreams_V_keep_V_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataStreams_V_last_V_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "3", "DependentChan" : "19",
				"BlockSignal" : [
					{"Name" : "dataStreams_V_last_V_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "checksumStreams_V_V_s", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "1", "DependentChan" : "11",
				"BlockSignal" : [
					{"Name" : "checksumStreams_V_V_s_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "checksumStreams_V_V_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "2", "DependentChan" : "16",
				"BlockSignal" : [
					{"Name" : "checksumStreams_V_V_1_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "6", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.insertChecksum_U0.regslice_both_outputStream_V_data_V_U", "Parent" : "5"},
	{"ID" : "7", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.insertChecksum_U0.regslice_both_outputStream_V_keep_V_U", "Parent" : "5"},
	{"ID" : "8", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.insertChecksum_U0.regslice_both_outputStream_V_last_V_U", "Parent" : "5"},
	{"ID" : "9", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.packageBuffer1_V_U", "Parent" : "0"},
	{"ID" : "10", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.validFifo_V_U", "Parent" : "0"},
	{"ID" : "11", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.checksumStreams_V_V_s_U", "Parent" : "0"},
	{"ID" : "12", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.udpPort2addIpHeader_8_U", "Parent" : "0"},
	{"ID" : "13", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.udpPort2addIpHeader_1_U", "Parent" : "0"},
	{"ID" : "14", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.udpPort2addIpHeader_6_U", "Parent" : "0"},
	{"ID" : "15", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.udpPort2addIpHeader_7_U", "Parent" : "0"},
	{"ID" : "16", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.checksumStreams_V_V_1_U", "Parent" : "0"},
	{"ID" : "17", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.dataStreams_V_data_V_1_U", "Parent" : "0"},
	{"ID" : "18", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.dataStreams_V_keep_V_1_U", "Parent" : "0"},
	{"ID" : "19", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.dataStreams_V_last_V_1_U", "Parent" : "0"},
	{"ID" : "20", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.dataStreams_V_data_V_U", "Parent" : "0"},
	{"ID" : "21", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.dataStreams_V_keep_V_U", "Parent" : "0"},
	{"ID" : "22", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.dataStreams_V_last_V_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	icmp_server {
		s_axis_V_data_V {Type I LastRead 0 FirstWrite -1}
		s_axis_V_keep_V {Type I LastRead 0 FirstWrite -1}
		s_axis_V_last_V {Type I LastRead 0 FirstWrite -1}
		udpIn_V_data_V {Type I LastRead 0 FirstWrite -1}
		udpIn_V_keep_V {Type I LastRead 0 FirstWrite -1}
		udpIn_V_last_V {Type I LastRead 0 FirstWrite -1}
		ttlIn_V_data_V {Type I LastRead 0 FirstWrite -1}
		ttlIn_V_keep_V {Type I LastRead 0 FirstWrite -1}
		ttlIn_V_last_V {Type I LastRead 0 FirstWrite -1}
		m_axis_V_data_V {Type O LastRead -1 FirstWrite 2}
		m_axis_V_keep_V {Type O LastRead -1 FirstWrite 2}
		m_axis_V_last_V {Type O LastRead -1 FirstWrite 2}
		cics_writeLastOne {Type IO LastRead -1 FirstWrite -1}
		cics_prevWord_data_V {Type IO LastRead -1 FirstWrite -1}
		cics_prevWord_keep_V {Type IO LastRead -1 FirstWrite -1}
		cics_prevWord_last_V {Type IO LastRead -1 FirstWrite -1}
		packageBuffer1_V {Type IO LastRead -1 FirstWrite -1}
		cics_computeCs {Type IO LastRead -1 FirstWrite -1}
		cics_sums_V_2 {Type IO LastRead -1 FirstWrite -1}
		cics_sums_V_0 {Type IO LastRead -1 FirstWrite -1}
		cics_sums_V_3 {Type IO LastRead -1 FirstWrite -1}
		cics_sums_V_1 {Type IO LastRead -1 FirstWrite -1}
		icmpChecksum_V {Type IO LastRead -1 FirstWrite -1}
		icmpType_V {Type IO LastRead -1 FirstWrite -1}
		icmpCode_V {Type IO LastRead -1 FirstWrite -1}
		cics_state_V {Type IO LastRead -1 FirstWrite -1}
		validFifo_V {Type IO LastRead -1 FirstWrite -1}
		checksumStreams_V_V_s {Type IO LastRead -1 FirstWrite -1}
		cics_wordCount_V {Type IO LastRead -1 FirstWrite -1}
		udpState {Type IO LastRead -1 FirstWrite -1}
		ipWordCounter_V {Type IO LastRead -1 FirstWrite -1}
		streamSource_V_1 {Type IO LastRead -1 FirstWrite -1}
		udpChecksum_V {Type IO LastRead -1 FirstWrite -1}
		udpPort2addIpHeader_8 {Type IO LastRead -1 FirstWrite -1}
		udpPort2addIpHeader_1 {Type IO LastRead -1 FirstWrite -1}
		udpPort2addIpHeader_6 {Type IO LastRead -1 FirstWrite -1}
		udpPort2addIpHeader_7 {Type IO LastRead -1 FirstWrite -1}
		checksumStreams_V_V_1 {Type IO LastRead -1 FirstWrite -1}
		addIpState {Type IO LastRead -1 FirstWrite -1}
		tempWord_data_V {Type IO LastRead -1 FirstWrite -1}
		dataStreams_V_data_V_1 {Type IO LastRead -1 FirstWrite -1}
		dataStreams_V_keep_V_1 {Type IO LastRead -1 FirstWrite -1}
		dataStreams_V_last_V_1 {Type IO LastRead -1 FirstWrite -1}
		tempWord_keep_V {Type IO LastRead -1 FirstWrite -1}
		sourceIP_V {Type IO LastRead -1 FirstWrite -1}
		d_isFirstWord {Type IO LastRead -1 FirstWrite -1}
		d_drop {Type IO LastRead -1 FirstWrite -1}
		dataStreams_V_data_V {Type IO LastRead -1 FirstWrite -1}
		dataStreams_V_keep_V {Type IO LastRead -1 FirstWrite -1}
		dataStreams_V_last_V {Type IO LastRead -1 FirstWrite -1}
		ic_wordCount_V {Type IO LastRead -1 FirstWrite -1}
		streamSource_V {Type IO LastRead -1 FirstWrite -1}}
	check_icmp_checksum {
		dataIn_V_data_V {Type I LastRead 0 FirstWrite -1}
		dataIn_V_keep_V {Type I LastRead 0 FirstWrite -1}
		dataIn_V_last_V {Type I LastRead 0 FirstWrite -1}
		cics_writeLastOne {Type IO LastRead -1 FirstWrite -1}
		cics_prevWord_data_V {Type IO LastRead -1 FirstWrite -1}
		cics_prevWord_keep_V {Type IO LastRead -1 FirstWrite -1}
		cics_prevWord_last_V {Type IO LastRead -1 FirstWrite -1}
		packageBuffer1_V {Type O LastRead -1 FirstWrite 1}
		cics_computeCs {Type IO LastRead -1 FirstWrite -1}
		cics_sums_V_2 {Type IO LastRead -1 FirstWrite -1}
		cics_sums_V_0 {Type IO LastRead -1 FirstWrite -1}
		cics_sums_V_3 {Type IO LastRead -1 FirstWrite -1}
		cics_sums_V_1 {Type IO LastRead -1 FirstWrite -1}
		icmpChecksum_V {Type IO LastRead -1 FirstWrite -1}
		icmpType_V {Type IO LastRead -1 FirstWrite -1}
		icmpCode_V {Type IO LastRead -1 FirstWrite -1}
		cics_state_V {Type IO LastRead -1 FirstWrite -1}
		validFifo_V {Type O LastRead -1 FirstWrite 1}
		checksumStreams_V_V_s {Type O LastRead -1 FirstWrite 1}
		cics_wordCount_V {Type IO LastRead -1 FirstWrite -1}}
	udpPortUnreachable18 {
		udpIn_V_data_V {Type I LastRead 0 FirstWrite -1}
		udpIn_V_keep_V {Type I LastRead 0 FirstWrite -1}
		udpIn_V_last_V {Type I LastRead 0 FirstWrite -1}
		ttlIn_V_data_V {Type I LastRead 0 FirstWrite -1}
		ttlIn_V_keep_V {Type I LastRead 0 FirstWrite -1}
		ttlIn_V_last_V {Type I LastRead 0 FirstWrite -1}
		udpState {Type IO LastRead -1 FirstWrite -1}
		ipWordCounter_V {Type IO LastRead -1 FirstWrite -1}
		streamSource_V_1 {Type IO LastRead -1 FirstWrite -1}
		udpChecksum_V {Type IO LastRead -1 FirstWrite -1}
		udpPort2addIpHeader_8 {Type O LastRead 0 FirstWrite 0}
		udpPort2addIpHeader_1 {Type O LastRead 0 FirstWrite 0}
		udpPort2addIpHeader_6 {Type O LastRead 0 FirstWrite 0}
		udpPort2addIpHeader_7 {Type O LastRead 0 FirstWrite 0}
		checksumStreams_V_V_1 {Type O LastRead 0 FirstWrite 0}}
	udpAddIpHeader186 {
		addIpState {Type IO LastRead -1 FirstWrite -1}
		tempWord_data_V {Type IO LastRead -1 FirstWrite -1}
		udpPort2addIpHeader_7 {Type I LastRead 0 FirstWrite -1}
		dataStreams_V_data_V_1 {Type O LastRead 0 FirstWrite 0}
		dataStreams_V_keep_V_1 {Type O LastRead 0 FirstWrite 0}
		dataStreams_V_last_V_1 {Type O LastRead 0 FirstWrite 0}
		tempWord_keep_V {Type IO LastRead -1 FirstWrite -1}
		sourceIP_V {Type IO LastRead -1 FirstWrite -1}
		udpPort2addIpHeader_8 {Type I LastRead 0 FirstWrite -1}
		udpPort2addIpHeader_1 {Type I LastRead 0 FirstWrite -1}
		udpPort2addIpHeader_6 {Type I LastRead 0 FirstWrite -1}}
	dropper {
		packageBuffer1_V {Type I LastRead 0 FirstWrite -1}
		d_isFirstWord {Type IO LastRead -1 FirstWrite -1}
		d_drop {Type IO LastRead -1 FirstWrite -1}
		validFifo_V {Type I LastRead 0 FirstWrite -1}
		dataStreams_V_data_V {Type O LastRead -1 FirstWrite 0}
		dataStreams_V_keep_V {Type O LastRead -1 FirstWrite 0}
		dataStreams_V_last_V {Type O LastRead -1 FirstWrite 0}}
	insertChecksum {
		outputStream_V_data_V {Type O LastRead -1 FirstWrite 2}
		outputStream_V_keep_V {Type O LastRead -1 FirstWrite 2}
		outputStream_V_last_V {Type O LastRead -1 FirstWrite 2}
		ic_wordCount_V {Type IO LastRead -1 FirstWrite -1}
		streamSource_V {Type IO LastRead -1 FirstWrite -1}
		dataStreams_V_data_V {Type I LastRead 1 FirstWrite -1}
		dataStreams_V_keep_V {Type I LastRead 1 FirstWrite -1}
		dataStreams_V_last_V {Type I LastRead 1 FirstWrite -1}
		dataStreams_V_data_V_1 {Type I LastRead 1 FirstWrite -1}
		dataStreams_V_keep_V_1 {Type I LastRead 1 FirstWrite -1}
		dataStreams_V_last_V_1 {Type I LastRead 1 FirstWrite -1}
		checksumStreams_V_V_s {Type I LastRead 1 FirstWrite -1}
		checksumStreams_V_V_1 {Type I LastRead 1 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "5", "Max" : "5"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
]}

set Spec2ImplPortList { 
	s_axis_V_data_V { axis {  { s_axis_TDATA in_data 0 64 } } }
	s_axis_V_keep_V { axis {  { s_axis_TKEEP in_data 0 8 } } }
	s_axis_V_last_V { axis {  { s_axis_TLAST in_data 0 1 }  { s_axis_TVALID in_vld 0 1 }  { s_axis_TREADY in_acc 1 1 } } }
	udpIn_V_data_V { axis {  { udpIn_TDATA in_data 0 64 } } }
	udpIn_V_keep_V { axis {  { udpIn_TKEEP in_data 0 8 } } }
	udpIn_V_last_V { axis {  { udpIn_TLAST in_data 0 1 }  { udpIn_TVALID in_vld 0 1 }  { udpIn_TREADY in_acc 1 1 } } }
	ttlIn_V_data_V { axis {  { ttlIn_TDATA in_data 0 64 } } }
	ttlIn_V_keep_V { axis {  { ttlIn_TKEEP in_data 0 8 } } }
	ttlIn_V_last_V { axis {  { ttlIn_TLAST in_data 0 1 }  { ttlIn_TVALID in_vld 0 1 }  { ttlIn_TREADY in_acc 1 1 } } }
	m_axis_V_data_V { axis {  { m_axis_TDATA out_data 1 64 } } }
	m_axis_V_keep_V { axis {  { m_axis_TKEEP out_data 1 8 } } }
	m_axis_V_last_V { axis {  { m_axis_TLAST out_data 1 1 }  { m_axis_TVALID out_vld 1 1 }  { m_axis_TREADY out_acc 0 1 } } }
}

set busDeadlockParameterList { 
}

# RTL port scheduling information:
set fifoSchedulingInfoList { 
}

# RTL bus port read request latency information:
set busReadReqLatencyList { 
}

# RTL bus port write response latency information:
set busWriteResLatencyList { 
}

# RTL array port load latency information:
set memoryLoadLatencyList { 
}
