// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef XIPV4_TOP_H
#define XIPV4_TOP_H

#ifdef __cplusplus
extern "C" {
#endif

/***************************** Include Files *********************************/
#ifndef __linux__
#include "xil_types.h"
#include "xil_assert.h"
#include "xstatus.h"
#include "xil_io.h"
#else
#include <stdint.h>
#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stddef.h>
#endif
#include "xipv4_top_hw.h"

/**************************** Type Definitions ******************************/
#ifdef __linux__
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
#else
typedef struct {
    u16 DeviceId;
    u32 Axilites_BaseAddress;
} XIpv4_top_Config;
#endif

typedef struct {
    u32 Axilites_BaseAddress;
    u32 IsReady;
} XIpv4_top;

/***************** Macros (Inline Functions) Definitions *********************/
#ifndef __linux__
#define XIpv4_top_WriteReg(BaseAddress, RegOffset, Data) \
    Xil_Out32((BaseAddress) + (RegOffset), (u32)(Data))
#define XIpv4_top_ReadReg(BaseAddress, RegOffset) \
    Xil_In32((BaseAddress) + (RegOffset))
#else
#define XIpv4_top_WriteReg(BaseAddress, RegOffset, Data) \
    *(volatile u32*)((BaseAddress) + (RegOffset)) = (u32)(Data)
#define XIpv4_top_ReadReg(BaseAddress, RegOffset) \
    *(volatile u32*)((BaseAddress) + (RegOffset))

#define Xil_AssertVoid(expr)    assert(expr)
#define Xil_AssertNonvoid(expr) assert(expr)

#define XST_SUCCESS             0
#define XST_DEVICE_NOT_FOUND    2
#define XST_OPEN_DEVICE_FAILED  3
#define XIL_COMPONENT_IS_READY  1
#endif

/************************** Function Prototypes *****************************/
#ifndef __linux__
int XIpv4_top_Initialize(XIpv4_top *InstancePtr, u16 DeviceId);
XIpv4_top_Config* XIpv4_top_LookupConfig(u16 DeviceId);
int XIpv4_top_CfgInitialize(XIpv4_top *InstancePtr, XIpv4_top_Config *ConfigPtr);
#else
int XIpv4_top_Initialize(XIpv4_top *InstancePtr, const char* InstanceName);
int XIpv4_top_Release(XIpv4_top *InstancePtr);
#endif


void XIpv4_top_Set_local_ipv4_address_V(XIpv4_top *InstancePtr, u32 Data);
u32 XIpv4_top_Get_local_ipv4_address_V(XIpv4_top *InstancePtr);
void XIpv4_top_Set_protocol_V(XIpv4_top *InstancePtr, u32 Data);
u32 XIpv4_top_Get_protocol_V(XIpv4_top *InstancePtr);

#ifdef __cplusplus
}
#endif

#endif
