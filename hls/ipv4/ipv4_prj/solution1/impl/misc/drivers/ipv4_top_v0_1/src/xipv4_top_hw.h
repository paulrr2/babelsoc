// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================
// AXILiteS
// 0x00 : reserved
// 0x04 : reserved
// 0x08 : reserved
// 0x0c : reserved
// 0x10 : Data signal of local_ipv4_address_V
//        bit 31~0 - local_ipv4_address_V[31:0] (Read/Write)
// 0x14 : reserved
// 0x18 : Data signal of protocol_V
//        bit 7~0 - protocol_V[7:0] (Read/Write)
//        others  - reserved
// 0x1c : reserved
// (SC = Self Clear, COR = Clear on Read, TOW = Toggle on Write, COH = Clear on Handshake)

#define XIPV4_TOP_AXILITES_ADDR_LOCAL_IPV4_ADDRESS_V_DATA 0x10
#define XIPV4_TOP_AXILITES_BITS_LOCAL_IPV4_ADDRESS_V_DATA 32
#define XIPV4_TOP_AXILITES_ADDR_PROTOCOL_V_DATA           0x18
#define XIPV4_TOP_AXILITES_BITS_PROTOCOL_V_DATA           8

