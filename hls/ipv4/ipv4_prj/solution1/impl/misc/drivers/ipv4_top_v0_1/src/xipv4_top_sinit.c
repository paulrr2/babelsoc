// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef __linux__

#include "xstatus.h"
#include "xparameters.h"
#include "xipv4_top.h"

extern XIpv4_top_Config XIpv4_top_ConfigTable[];

XIpv4_top_Config *XIpv4_top_LookupConfig(u16 DeviceId) {
	XIpv4_top_Config *ConfigPtr = NULL;

	int Index;

	for (Index = 0; Index < XPAR_XIPV4_TOP_NUM_INSTANCES; Index++) {
		if (XIpv4_top_ConfigTable[Index].DeviceId == DeviceId) {
			ConfigPtr = &XIpv4_top_ConfigTable[Index];
			break;
		}
	}

	return ConfigPtr;
}

int XIpv4_top_Initialize(XIpv4_top *InstancePtr, u16 DeviceId) {
	XIpv4_top_Config *ConfigPtr;

	Xil_AssertNonvoid(InstancePtr != NULL);

	ConfigPtr = XIpv4_top_LookupConfig(DeviceId);
	if (ConfigPtr == NULL) {
		InstancePtr->IsReady = 0;
		return (XST_DEVICE_NOT_FOUND);
	}

	return XIpv4_top_CfgInitialize(InstancePtr, ConfigPtr);
}

#endif

