set moduleName ipv4_top_entry180
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 1
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {ipv4_top.entry180}
set C_modelType { void 0 }
set C_modelArgList {
	{ local_ipv4_address_V int 32 regular {fifo 0}  }
	{ protocol_V int 8 regular {fifo 0}  }
	{ local_ipv4_address_V_out int 32 regular {fifo 1}  }
	{ protocol_V_out int 8 regular {fifo 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "local_ipv4_address_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "protocol_V", "interface" : "fifo", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "local_ipv4_address_V_out", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "protocol_V_out", "interface" : "fifo", "bitwidth" : 8, "direction" : "WRITEONLY"} ]}
# RTL Port declarations: 
set portNum 19
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ local_ipv4_address_V_dout sc_in sc_lv 32 signal 0 } 
	{ local_ipv4_address_V_empty_n sc_in sc_logic 1 signal 0 } 
	{ local_ipv4_address_V_read sc_out sc_logic 1 signal 0 } 
	{ protocol_V_dout sc_in sc_lv 8 signal 1 } 
	{ protocol_V_empty_n sc_in sc_logic 1 signal 1 } 
	{ protocol_V_read sc_out sc_logic 1 signal 1 } 
	{ local_ipv4_address_V_out_din sc_out sc_lv 32 signal 2 } 
	{ local_ipv4_address_V_out_full_n sc_in sc_logic 1 signal 2 } 
	{ local_ipv4_address_V_out_write sc_out sc_logic 1 signal 2 } 
	{ protocol_V_out_din sc_out sc_lv 8 signal 3 } 
	{ protocol_V_out_full_n sc_in sc_logic 1 signal 3 } 
	{ protocol_V_out_write sc_out sc_logic 1 signal 3 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "local_ipv4_address_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "local_ipv4_address_V", "role": "dout" }} , 
 	{ "name": "local_ipv4_address_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "local_ipv4_address_V", "role": "empty_n" }} , 
 	{ "name": "local_ipv4_address_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "local_ipv4_address_V", "role": "read" }} , 
 	{ "name": "protocol_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "protocol_V", "role": "dout" }} , 
 	{ "name": "protocol_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "protocol_V", "role": "empty_n" }} , 
 	{ "name": "protocol_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "protocol_V", "role": "read" }} , 
 	{ "name": "local_ipv4_address_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "local_ipv4_address_V_out", "role": "din" }} , 
 	{ "name": "local_ipv4_address_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "local_ipv4_address_V_out", "role": "full_n" }} , 
 	{ "name": "local_ipv4_address_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "local_ipv4_address_V_out", "role": "write" }} , 
 	{ "name": "protocol_V_out_din", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "protocol_V_out", "role": "din" }} , 
 	{ "name": "protocol_V_out_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "protocol_V_out", "role": "full_n" }} , 
 	{ "name": "protocol_V_out_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "protocol_V_out", "role": "write" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "ipv4_top_entry180",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "0", "EstimateLatencyMin" : "0", "EstimateLatencyMax" : "0",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "0",
		"Port" : [
			{"Name" : "local_ipv4_address_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "local_ipv4_address_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "protocol_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "protocol_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "local_ipv4_address_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "local_ipv4_address_V_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "protocol_V_out", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "protocol_V_out_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	ipv4_top_entry180 {
		local_ipv4_address_V {Type I LastRead 0 FirstWrite -1}
		protocol_V {Type I LastRead 0 FirstWrite -1}
		local_ipv4_address_V_out {Type O LastRead -1 FirstWrite 0}
		protocol_V_out {Type O LastRead -1 FirstWrite 0}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "0", "Max" : "0"}
	, {"Name" : "Interval", "Min" : "0", "Max" : "0"}
]}

set PipelineEnableSignalInfo {[
]}

set Spec2ImplPortList { 
	local_ipv4_address_V { ap_fifo {  { local_ipv4_address_V_dout fifo_data 0 32 }  { local_ipv4_address_V_empty_n fifo_status 0 1 }  { local_ipv4_address_V_read fifo_update 1 1 } } }
	protocol_V { ap_fifo {  { protocol_V_dout fifo_data 0 8 }  { protocol_V_empty_n fifo_status 0 1 }  { protocol_V_read fifo_update 1 1 } } }
	local_ipv4_address_V_out { ap_fifo {  { local_ipv4_address_V_out_din fifo_data 1 32 }  { local_ipv4_address_V_out_full_n fifo_status 0 1 }  { local_ipv4_address_V_out_write fifo_update 1 1 } } }
	protocol_V_out { ap_fifo {  { protocol_V_out_din fifo_data 1 8 }  { protocol_V_out_full_n fifo_status 0 1 }  { protocol_V_out_write fifo_update 1 1 } } }
}
