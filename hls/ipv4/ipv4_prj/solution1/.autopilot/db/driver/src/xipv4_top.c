// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================
/***************************** Include Files *********************************/
#include "xipv4_top.h"

/************************** Function Implementation *************************/
#ifndef __linux__
int XIpv4_top_CfgInitialize(XIpv4_top *InstancePtr, XIpv4_top_Config *ConfigPtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(ConfigPtr != NULL);

    InstancePtr->Axilites_BaseAddress = ConfigPtr->Axilites_BaseAddress;
    InstancePtr->IsReady = XIL_COMPONENT_IS_READY;

    return XST_SUCCESS;
}
#endif

void XIpv4_top_Set_local_ipv4_address_V(XIpv4_top *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XIpv4_top_WriteReg(InstancePtr->Axilites_BaseAddress, XIPV4_TOP_AXILITES_ADDR_LOCAL_IPV4_ADDRESS_V_DATA, Data);
}

u32 XIpv4_top_Get_local_ipv4_address_V(XIpv4_top *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XIpv4_top_ReadReg(InstancePtr->Axilites_BaseAddress, XIPV4_TOP_AXILITES_ADDR_LOCAL_IPV4_ADDRESS_V_DATA);
    return Data;
}

void XIpv4_top_Set_protocol_V(XIpv4_top *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XIpv4_top_WriteReg(InstancePtr->Axilites_BaseAddress, XIPV4_TOP_AXILITES_ADDR_PROTOCOL_V_DATA, Data);
}

u32 XIpv4_top_Get_protocol_V(XIpv4_top *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XIpv4_top_ReadReg(InstancePtr->Axilites_BaseAddress, XIPV4_TOP_AXILITES_ADDR_PROTOCOL_V_DATA);
    return Data;
}

