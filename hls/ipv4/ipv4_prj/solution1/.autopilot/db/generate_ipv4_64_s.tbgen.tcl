set moduleName generate_ipv4_64_s
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {generate_ipv4<64>}
set C_modelType { void 0 }
set C_modelArgList {
	{ txEng_ipMetaDataFifoIn_V int 48 regular {axi_s 0 volatile  { txEng_ipMetaDataFifoIn_V Data } }  }
	{ m_axis_tx_data_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_tx_data Data } }  }
	{ m_axis_tx_data_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_tx_data Keep } }  }
	{ m_axis_tx_data_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_tx_data Last } }  }
	{ local_ipv4_address_V int 32 regular {ap_stable 0} }
	{ protocol_V int 8 regular {ap_stable 0} }
	{ tx_shift2ipv4Fifo_V_1 int 64 regular {fifo 0 volatile } {global 0}  }
	{ tx_shift2ipv4Fifo_V_2 int 8 regular {fifo 0 volatile } {global 0}  }
	{ tx_shift2ipv4Fifo_V_s int 1 regular {fifo 0 volatile } {global 0}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "txEng_ipMetaDataFifoIn_V", "interface" : "axis", "bitwidth" : 48, "direction" : "READONLY"} , 
 	{ "Name" : "m_axis_tx_data_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY"} , 
 	{ "Name" : "m_axis_tx_data_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY"} , 
 	{ "Name" : "m_axis_tx_data_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY"} , 
 	{ "Name" : "local_ipv4_address_V", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "protocol_V", "interface" : "wire", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "tx_shift2ipv4Fifo_V_1", "interface" : "fifo", "bitwidth" : 64, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "tx_shift2ipv4Fifo_V_2", "interface" : "fifo", "bitwidth" : 8, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "tx_shift2ipv4Fifo_V_s", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 26
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ tx_shift2ipv4Fifo_V_1_dout sc_in sc_lv 64 signal 6 } 
	{ tx_shift2ipv4Fifo_V_1_empty_n sc_in sc_logic 1 signal 6 } 
	{ tx_shift2ipv4Fifo_V_1_read sc_out sc_logic 1 signal 6 } 
	{ tx_shift2ipv4Fifo_V_2_dout sc_in sc_lv 8 signal 7 } 
	{ tx_shift2ipv4Fifo_V_2_empty_n sc_in sc_logic 1 signal 7 } 
	{ tx_shift2ipv4Fifo_V_2_read sc_out sc_logic 1 signal 7 } 
	{ tx_shift2ipv4Fifo_V_s_dout sc_in sc_lv 1 signal 8 } 
	{ tx_shift2ipv4Fifo_V_s_empty_n sc_in sc_logic 1 signal 8 } 
	{ tx_shift2ipv4Fifo_V_s_read sc_out sc_logic 1 signal 8 } 
	{ txEng_ipMetaDataFifoIn_V_TVALID sc_in sc_logic 1 invld 0 } 
	{ m_axis_tx_data_TREADY sc_in sc_logic 1 outacc 3 } 
	{ txEng_ipMetaDataFifoIn_V_TDATA sc_in sc_lv 48 signal 0 } 
	{ txEng_ipMetaDataFifoIn_V_TREADY sc_out sc_logic 1 inacc 0 } 
	{ m_axis_tx_data_TDATA sc_out sc_lv 64 signal 1 } 
	{ m_axis_tx_data_TVALID sc_out sc_logic 1 outvld 3 } 
	{ m_axis_tx_data_TKEEP sc_out sc_lv 8 signal 2 } 
	{ m_axis_tx_data_TLAST sc_out sc_lv 1 signal 3 } 
	{ local_ipv4_address_V sc_in sc_lv 32 signal 4 } 
	{ protocol_V sc_in sc_lv 8 signal 5 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "tx_shift2ipv4Fifo_V_1_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "tx_shift2ipv4Fifo_V_1", "role": "dout" }} , 
 	{ "name": "tx_shift2ipv4Fifo_V_1_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tx_shift2ipv4Fifo_V_1", "role": "empty_n" }} , 
 	{ "name": "tx_shift2ipv4Fifo_V_1_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tx_shift2ipv4Fifo_V_1", "role": "read" }} , 
 	{ "name": "tx_shift2ipv4Fifo_V_2_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "tx_shift2ipv4Fifo_V_2", "role": "dout" }} , 
 	{ "name": "tx_shift2ipv4Fifo_V_2_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tx_shift2ipv4Fifo_V_2", "role": "empty_n" }} , 
 	{ "name": "tx_shift2ipv4Fifo_V_2_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tx_shift2ipv4Fifo_V_2", "role": "read" }} , 
 	{ "name": "tx_shift2ipv4Fifo_V_s_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "tx_shift2ipv4Fifo_V_s", "role": "dout" }} , 
 	{ "name": "tx_shift2ipv4Fifo_V_s_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tx_shift2ipv4Fifo_V_s", "role": "empty_n" }} , 
 	{ "name": "tx_shift2ipv4Fifo_V_s_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tx_shift2ipv4Fifo_V_s", "role": "read" }} , 
 	{ "name": "txEng_ipMetaDataFifoIn_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "txEng_ipMetaDataFifoIn_V", "role": "TVALID" }} , 
 	{ "name": "m_axis_tx_data_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_tx_data_V_last_V", "role": "default" }} , 
 	{ "name": "txEng_ipMetaDataFifoIn_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":48, "type": "signal", "bundle":{"name": "txEng_ipMetaDataFifoIn_V", "role": "TDATA" }} , 
 	{ "name": "txEng_ipMetaDataFifoIn_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "txEng_ipMetaDataFifoIn_V", "role": "TREADY" }} , 
 	{ "name": "m_axis_tx_data_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "m_axis_tx_data_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_tx_data_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_tx_data_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_tx_data_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "m_axis_tx_data_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_tx_data_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "m_axis_tx_data_V_last_V", "role": "default" }} , 
 	{ "name": "local_ipv4_address_V", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "local_ipv4_address_V", "role": "default" }} , 
 	{ "name": "protocol_V", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "protocol_V", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2", "3"],
		"CDFG" : "generate_ipv4_64_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "txEng_ipMetaDataFifoIn_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "txEng_ipMetaDataFifoIn_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "m_axis_tx_data_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_tx_data_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "m_axis_tx_data_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "m_axis_tx_data_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "local_ipv4_address_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "protocol_V", "Type" : "Stable", "Direction" : "I"},
			{"Name" : "gi_state", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_idx_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_header_V_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tx_shift2ipv4Fifo_V_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "tx_shift2ipv4Fifo_V_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tx_shift2ipv4Fifo_V_2", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "tx_shift2ipv4Fifo_V_2_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tx_shift2ipv4Fifo_V_s", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "tx_shift2ipv4Fifo_V_s_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_m_axis_tx_data_V_data_V_U", "Parent" : "0"},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_m_axis_tx_data_V_keep_V_U", "Parent" : "0"},
	{"ID" : "3", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_m_axis_tx_data_V_last_V_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	generate_ipv4_64_s {
		txEng_ipMetaDataFifoIn_V {Type I LastRead 0 FirstWrite -1}
		m_axis_tx_data_V_data_V {Type O LastRead -1 FirstWrite 1}
		m_axis_tx_data_V_keep_V {Type O LastRead -1 FirstWrite 1}
		m_axis_tx_data_V_last_V {Type O LastRead -1 FirstWrite 1}
		local_ipv4_address_V {Type I LastRead 0 FirstWrite -1}
		protocol_V {Type I LastRead 0 FirstWrite -1}
		gi_state {Type IO LastRead -1 FirstWrite -1}
		header_idx_1 {Type IO LastRead -1 FirstWrite -1}
		header_header_V_1 {Type IO LastRead -1 FirstWrite -1}
		tx_shift2ipv4Fifo_V_1 {Type I LastRead 0 FirstWrite -1}
		tx_shift2ipv4Fifo_V_2 {Type I LastRead 0 FirstWrite -1}
		tx_shift2ipv4Fifo_V_s {Type I LastRead 0 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "2", "Max" : "2"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	txEng_ipMetaDataFifoIn_V { axis {  { txEng_ipMetaDataFifoIn_V_TVALID in_vld 0 1 }  { txEng_ipMetaDataFifoIn_V_TDATA in_data 0 48 }  { txEng_ipMetaDataFifoIn_V_TREADY in_acc 1 1 } } }
	m_axis_tx_data_V_data_V { axis {  { m_axis_tx_data_TDATA out_data 1 64 } } }
	m_axis_tx_data_V_keep_V { axis {  { m_axis_tx_data_TKEEP out_data 1 8 } } }
	m_axis_tx_data_V_last_V { axis {  { m_axis_tx_data_TREADY out_acc 0 1 }  { m_axis_tx_data_TVALID out_vld 1 1 }  { m_axis_tx_data_TLAST out_data 1 1 } } }
	local_ipv4_address_V { ap_stable {  { local_ipv4_address_V in_data 0 32 } } }
	protocol_V { ap_stable {  { protocol_V in_data 0 8 } } }
	tx_shift2ipv4Fifo_V_1 { ap_fifo {  { tx_shift2ipv4Fifo_V_1_dout fifo_data 0 64 }  { tx_shift2ipv4Fifo_V_1_empty_n fifo_status 0 1 }  { tx_shift2ipv4Fifo_V_1_read fifo_update 1 1 } } }
	tx_shift2ipv4Fifo_V_2 { ap_fifo {  { tx_shift2ipv4Fifo_V_2_dout fifo_data 0 8 }  { tx_shift2ipv4Fifo_V_2_empty_n fifo_status 0 1 }  { tx_shift2ipv4Fifo_V_2_read fifo_update 1 1 } } }
	tx_shift2ipv4Fifo_V_s { ap_fifo {  { tx_shift2ipv4Fifo_V_s_dout fifo_data 0 1 }  { tx_shift2ipv4Fifo_V_s_empty_n fifo_status 0 1 }  { tx_shift2ipv4Fifo_V_s_read fifo_update 1 1 } } }
}
set moduleName generate_ipv4_64_s
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {generate_ipv4<64>}
set C_modelType { void 0 }
set C_modelArgList {
	{ txEng_ipMetaDataFifoIn_V int 48 regular {axi_s 0 volatile  { txEng_ipMetaDataFifoIn_V Data } }  }
	{ m_axis_tx_data_V_data_V int 64 regular {axi_s 1 volatile  { m_axis_tx_data Data } }  }
	{ m_axis_tx_data_V_keep_V int 8 regular {axi_s 1 volatile  { m_axis_tx_data Keep } }  }
	{ m_axis_tx_data_V_last_V int 1 regular {axi_s 1 volatile  { m_axis_tx_data Last } }  }
	{ local_ipv4_address_V int 32 regular {fifo 0}  }
	{ protocol_V int 8 regular {fifo 0}  }
	{ tx_shift2ipv4Fifo_V_1 int 64 regular {fifo 0 volatile } {global 0}  }
	{ tx_shift2ipv4Fifo_V_2 int 8 regular {fifo 0 volatile } {global 0}  }
	{ tx_shift2ipv4Fifo_V_s int 1 regular {fifo 0 volatile } {global 0}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "txEng_ipMetaDataFifoIn_V", "interface" : "axis", "bitwidth" : 48, "direction" : "READONLY"} , 
 	{ "Name" : "m_axis_tx_data_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "WRITEONLY"} , 
 	{ "Name" : "m_axis_tx_data_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY"} , 
 	{ "Name" : "m_axis_tx_data_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY"} , 
 	{ "Name" : "local_ipv4_address_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "protocol_V", "interface" : "fifo", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "tx_shift2ipv4Fifo_V_1", "interface" : "fifo", "bitwidth" : 64, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "tx_shift2ipv4Fifo_V_2", "interface" : "fifo", "bitwidth" : 8, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "tx_shift2ipv4Fifo_V_s", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 30
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ local_ipv4_address_V_dout sc_in sc_lv 32 signal 4 } 
	{ local_ipv4_address_V_empty_n sc_in sc_logic 1 signal 4 } 
	{ local_ipv4_address_V_read sc_out sc_logic 1 signal 4 } 
	{ protocol_V_dout sc_in sc_lv 8 signal 5 } 
	{ protocol_V_empty_n sc_in sc_logic 1 signal 5 } 
	{ protocol_V_read sc_out sc_logic 1 signal 5 } 
	{ tx_shift2ipv4Fifo_V_1_dout sc_in sc_lv 64 signal 6 } 
	{ tx_shift2ipv4Fifo_V_1_empty_n sc_in sc_logic 1 signal 6 } 
	{ tx_shift2ipv4Fifo_V_1_read sc_out sc_logic 1 signal 6 } 
	{ tx_shift2ipv4Fifo_V_2_dout sc_in sc_lv 8 signal 7 } 
	{ tx_shift2ipv4Fifo_V_2_empty_n sc_in sc_logic 1 signal 7 } 
	{ tx_shift2ipv4Fifo_V_2_read sc_out sc_logic 1 signal 7 } 
	{ tx_shift2ipv4Fifo_V_s_dout sc_in sc_lv 1 signal 8 } 
	{ tx_shift2ipv4Fifo_V_s_empty_n sc_in sc_logic 1 signal 8 } 
	{ tx_shift2ipv4Fifo_V_s_read sc_out sc_logic 1 signal 8 } 
	{ txEng_ipMetaDataFifoIn_V_TVALID sc_in sc_logic 1 invld 0 } 
	{ m_axis_tx_data_TREADY sc_in sc_logic 1 outacc 3 } 
	{ txEng_ipMetaDataFifoIn_V_TDATA sc_in sc_lv 48 signal 0 } 
	{ txEng_ipMetaDataFifoIn_V_TREADY sc_out sc_logic 1 inacc 0 } 
	{ m_axis_tx_data_TDATA sc_out sc_lv 64 signal 1 } 
	{ m_axis_tx_data_TVALID sc_out sc_logic 1 outvld 3 } 
	{ m_axis_tx_data_TKEEP sc_out sc_lv 8 signal 2 } 
	{ m_axis_tx_data_TLAST sc_out sc_lv 1 signal 3 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "local_ipv4_address_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "local_ipv4_address_V", "role": "dout" }} , 
 	{ "name": "local_ipv4_address_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "local_ipv4_address_V", "role": "empty_n" }} , 
 	{ "name": "local_ipv4_address_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "local_ipv4_address_V", "role": "read" }} , 
 	{ "name": "protocol_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "protocol_V", "role": "dout" }} , 
 	{ "name": "protocol_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "protocol_V", "role": "empty_n" }} , 
 	{ "name": "protocol_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "protocol_V", "role": "read" }} , 
 	{ "name": "tx_shift2ipv4Fifo_V_1_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "tx_shift2ipv4Fifo_V_1", "role": "dout" }} , 
 	{ "name": "tx_shift2ipv4Fifo_V_1_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tx_shift2ipv4Fifo_V_1", "role": "empty_n" }} , 
 	{ "name": "tx_shift2ipv4Fifo_V_1_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tx_shift2ipv4Fifo_V_1", "role": "read" }} , 
 	{ "name": "tx_shift2ipv4Fifo_V_2_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "tx_shift2ipv4Fifo_V_2", "role": "dout" }} , 
 	{ "name": "tx_shift2ipv4Fifo_V_2_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tx_shift2ipv4Fifo_V_2", "role": "empty_n" }} , 
 	{ "name": "tx_shift2ipv4Fifo_V_2_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tx_shift2ipv4Fifo_V_2", "role": "read" }} , 
 	{ "name": "tx_shift2ipv4Fifo_V_s_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "tx_shift2ipv4Fifo_V_s", "role": "dout" }} , 
 	{ "name": "tx_shift2ipv4Fifo_V_s_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tx_shift2ipv4Fifo_V_s", "role": "empty_n" }} , 
 	{ "name": "tx_shift2ipv4Fifo_V_s_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "tx_shift2ipv4Fifo_V_s", "role": "read" }} , 
 	{ "name": "txEng_ipMetaDataFifoIn_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "txEng_ipMetaDataFifoIn_V", "role": "TVALID" }} , 
 	{ "name": "m_axis_tx_data_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "m_axis_tx_data_V_last_V", "role": "default" }} , 
 	{ "name": "txEng_ipMetaDataFifoIn_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":48, "type": "signal", "bundle":{"name": "txEng_ipMetaDataFifoIn_V", "role": "TDATA" }} , 
 	{ "name": "txEng_ipMetaDataFifoIn_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "txEng_ipMetaDataFifoIn_V", "role": "TREADY" }} , 
 	{ "name": "m_axis_tx_data_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "m_axis_tx_data_V_data_V", "role": "default" }} , 
 	{ "name": "m_axis_tx_data_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "m_axis_tx_data_V_last_V", "role": "default" }} , 
 	{ "name": "m_axis_tx_data_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "m_axis_tx_data_V_keep_V", "role": "default" }} , 
 	{ "name": "m_axis_tx_data_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "m_axis_tx_data_V_last_V", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "generate_ipv4_64_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "2", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "txEng_ipMetaDataFifoIn_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "txEng_ipMetaDataFifoIn_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "m_axis_tx_data_V_data_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "m_axis_tx_data_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "m_axis_tx_data_V_keep_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "m_axis_tx_data_V_last_V", "Type" : "Axis", "Direction" : "O"},
			{"Name" : "local_ipv4_address_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "local_ipv4_address_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "protocol_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "protocol_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "gi_state", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_idx_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_header_V_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tx_shift2ipv4Fifo_V_1", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "tx_shift2ipv4Fifo_V_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tx_shift2ipv4Fifo_V_2", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "tx_shift2ipv4Fifo_V_2_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tx_shift2ipv4Fifo_V_s", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "tx_shift2ipv4Fifo_V_s_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	generate_ipv4_64_s {
		txEng_ipMetaDataFifoIn_V {Type I LastRead 0 FirstWrite -1}
		m_axis_tx_data_V_data_V {Type O LastRead -1 FirstWrite 1}
		m_axis_tx_data_V_keep_V {Type O LastRead -1 FirstWrite 1}
		m_axis_tx_data_V_last_V {Type O LastRead -1 FirstWrite 1}
		local_ipv4_address_V {Type I LastRead 0 FirstWrite -1}
		protocol_V {Type I LastRead 0 FirstWrite -1}
		gi_state {Type IO LastRead -1 FirstWrite -1}
		header_idx_1 {Type IO LastRead -1 FirstWrite -1}
		header_header_V_1 {Type IO LastRead -1 FirstWrite -1}
		tx_shift2ipv4Fifo_V_1 {Type I LastRead 0 FirstWrite -1}
		tx_shift2ipv4Fifo_V_2 {Type I LastRead 0 FirstWrite -1}
		tx_shift2ipv4Fifo_V_s {Type I LastRead 0 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "2", "Max" : "2"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	txEng_ipMetaDataFifoIn_V { axis {  { txEng_ipMetaDataFifoIn_V_TVALID in_vld 0 1 }  { txEng_ipMetaDataFifoIn_V_TDATA in_data 0 48 }  { txEng_ipMetaDataFifoIn_V_TREADY in_acc 1 1 } } }
	m_axis_tx_data_V_data_V { axis {  { m_axis_tx_data_TDATA out_data 1 64 } } }
	m_axis_tx_data_V_keep_V { axis {  { m_axis_tx_data_TKEEP out_data 1 8 } } }
	m_axis_tx_data_V_last_V { axis {  { m_axis_tx_data_TREADY out_acc 0 1 }  { m_axis_tx_data_TVALID out_vld 1 1 }  { m_axis_tx_data_TLAST out_data 1 1 } } }
	local_ipv4_address_V { ap_fifo {  { local_ipv4_address_V_dout fifo_data 0 32 }  { local_ipv4_address_V_empty_n fifo_status 0 1 }  { local_ipv4_address_V_read fifo_update 1 1 } } }
	protocol_V { ap_fifo {  { protocol_V_dout fifo_data 0 8 }  { protocol_V_empty_n fifo_status 0 1 }  { protocol_V_read fifo_update 1 1 } } }
	tx_shift2ipv4Fifo_V_1 { ap_fifo {  { tx_shift2ipv4Fifo_V_1_dout fifo_data 0 64 }  { tx_shift2ipv4Fifo_V_1_empty_n fifo_status 0 1 }  { tx_shift2ipv4Fifo_V_1_read fifo_update 1 1 } } }
	tx_shift2ipv4Fifo_V_2 { ap_fifo {  { tx_shift2ipv4Fifo_V_2_dout fifo_data 0 8 }  { tx_shift2ipv4Fifo_V_2_empty_n fifo_status 0 1 }  { tx_shift2ipv4Fifo_V_2_read fifo_update 1 1 } } }
	tx_shift2ipv4Fifo_V_s { ap_fifo {  { tx_shift2ipv4Fifo_V_s_dout fifo_data 0 1 }  { tx_shift2ipv4Fifo_V_s_empty_n fifo_status 0 1 }  { tx_shift2ipv4Fifo_V_s_read fifo_update 1 1 } } }
}
