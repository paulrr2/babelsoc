set moduleName process_ipv4_64_s
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {process_ipv4<64>}
set C_modelType { void 0 }
set C_modelArgList {
	{ dataIn_V_data_V int 64 regular {axi_s 0 volatile  { s_axis_rx_data Data } }  }
	{ dataIn_V_keep_V int 8 regular {axi_s 0 volatile  { s_axis_rx_data Keep } }  }
	{ dataIn_V_last_V int 1 regular {axi_s 0 volatile  { s_axis_rx_data Last } }  }
	{ MetaOut_V int 48 regular {axi_s 1 volatile  { MetaOut_V Data } }  }
	{ rx_process2dropFifo_1_1 int 64 regular {fifo 1 volatile } {global 1}  }
	{ rx_process2dropFifo_2_0 int 8 regular {fifo 1 volatile } {global 1}  }
	{ rx_process2dropFifo_s_2 int 1 regular {fifo 1 volatile } {global 1}  }
	{ rx_process2dropLengt_1 int 4 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "dataIn_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "READONLY"} , 
 	{ "Name" : "dataIn_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "dataIn_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY"} , 
 	{ "Name" : "MetaOut_V", "interface" : "axis", "bitwidth" : 48, "direction" : "WRITEONLY"} , 
 	{ "Name" : "rx_process2dropFifo_1_1", "interface" : "fifo", "bitwidth" : 64, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rx_process2dropFifo_2_0", "interface" : "fifo", "bitwidth" : 8, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rx_process2dropFifo_s_2", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rx_process2dropLengt_1", "interface" : "fifo", "bitwidth" : 4, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 27
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ s_axis_rx_data_TVALID sc_in sc_logic 1 invld 0 } 
	{ rx_process2dropFifo_1_1_din sc_out sc_lv 64 signal 4 } 
	{ rx_process2dropFifo_1_1_full_n sc_in sc_logic 1 signal 4 } 
	{ rx_process2dropFifo_1_1_write sc_out sc_logic 1 signal 4 } 
	{ rx_process2dropFifo_2_0_din sc_out sc_lv 8 signal 5 } 
	{ rx_process2dropFifo_2_0_full_n sc_in sc_logic 1 signal 5 } 
	{ rx_process2dropFifo_2_0_write sc_out sc_logic 1 signal 5 } 
	{ rx_process2dropFifo_s_2_din sc_out sc_lv 1 signal 6 } 
	{ rx_process2dropFifo_s_2_full_n sc_in sc_logic 1 signal 6 } 
	{ rx_process2dropFifo_s_2_write sc_out sc_logic 1 signal 6 } 
	{ rx_process2dropLengt_1_din sc_out sc_lv 4 signal 7 } 
	{ rx_process2dropLengt_1_full_n sc_in sc_logic 1 signal 7 } 
	{ rx_process2dropLengt_1_write sc_out sc_logic 1 signal 7 } 
	{ MetaOut_V_TREADY sc_in sc_logic 1 outacc 3 } 
	{ s_axis_rx_data_TDATA sc_in sc_lv 64 signal 0 } 
	{ s_axis_rx_data_TREADY sc_out sc_logic 1 inacc 2 } 
	{ s_axis_rx_data_TKEEP sc_in sc_lv 8 signal 1 } 
	{ s_axis_rx_data_TLAST sc_in sc_lv 1 signal 2 } 
	{ MetaOut_V_TDATA sc_out sc_lv 48 signal 3 } 
	{ MetaOut_V_TVALID sc_out sc_logic 1 outvld 3 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "s_axis_rx_data_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "dataIn_V_data_V", "role": "VALID" }} , 
 	{ "name": "rx_process2dropFifo_1_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "rx_process2dropFifo_1_1", "role": "din" }} , 
 	{ "name": "rx_process2dropFifo_1_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_process2dropFifo_1_1", "role": "full_n" }} , 
 	{ "name": "rx_process2dropFifo_1_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_process2dropFifo_1_1", "role": "write" }} , 
 	{ "name": "rx_process2dropFifo_2_0_din", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "rx_process2dropFifo_2_0", "role": "din" }} , 
 	{ "name": "rx_process2dropFifo_2_0_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_process2dropFifo_2_0", "role": "full_n" }} , 
 	{ "name": "rx_process2dropFifo_2_0_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_process2dropFifo_2_0", "role": "write" }} , 
 	{ "name": "rx_process2dropFifo_s_2_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_process2dropFifo_s_2", "role": "din" }} , 
 	{ "name": "rx_process2dropFifo_s_2_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_process2dropFifo_s_2", "role": "full_n" }} , 
 	{ "name": "rx_process2dropFifo_s_2_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_process2dropFifo_s_2", "role": "write" }} , 
 	{ "name": "rx_process2dropLengt_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "rx_process2dropLengt_1", "role": "din" }} , 
 	{ "name": "rx_process2dropLengt_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_process2dropLengt_1", "role": "full_n" }} , 
 	{ "name": "rx_process2dropLengt_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_process2dropLengt_1", "role": "write" }} , 
 	{ "name": "MetaOut_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "MetaOut_V", "role": "TREADY" }} , 
 	{ "name": "s_axis_rx_data_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "dataIn_V_data_V", "role": "DATA" }} , 
 	{ "name": "s_axis_rx_data_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "dataIn_V_last_V", "role": "READY" }} , 
 	{ "name": "s_axis_rx_data_TKEEP", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "dataIn_V_keep_V", "role": "KEEP" }} , 
 	{ "name": "s_axis_rx_data_TLAST", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "dataIn_V_last_V", "role": "LAST" }} , 
 	{ "name": "MetaOut_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":48, "type": "signal", "bundle":{"name": "MetaOut_V", "role": "TDATA" }} , 
 	{ "name": "MetaOut_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "MetaOut_V", "role": "TVALID" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1"],
		"CDFG" : "process_ipv4_64_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "5", "EstimateLatencyMin" : "5", "EstimateLatencyMax" : "5",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "dataIn_V_data_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "s_axis_rx_data_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataIn_V_keep_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "dataIn_V_last_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "MetaOut_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "MetaOut_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "header_ready", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_idx", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_header_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "metaWritten", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "headerWordsDropped_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "rx_process2dropFifo_1_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rx_process2dropFifo_1_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rx_process2dropFifo_2_0", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rx_process2dropFifo_2_0_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rx_process2dropFifo_s_2", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rx_process2dropFifo_s_2_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rx_process2dropLengt_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rx_process2dropLengt_1_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.regslice_both_MetaOut_V_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	process_ipv4_64_s {
		dataIn_V_data_V {Type I LastRead 0 FirstWrite -1}
		dataIn_V_keep_V {Type I LastRead 0 FirstWrite -1}
		dataIn_V_last_V {Type I LastRead 0 FirstWrite -1}
		MetaOut_V {Type O LastRead -1 FirstWrite 4}
		header_ready {Type IO LastRead -1 FirstWrite -1}
		header_idx {Type IO LastRead -1 FirstWrite -1}
		header_header_V {Type IO LastRead -1 FirstWrite -1}
		metaWritten {Type IO LastRead -1 FirstWrite -1}
		headerWordsDropped_V {Type IO LastRead -1 FirstWrite -1}
		rx_process2dropFifo_1_1 {Type O LastRead -1 FirstWrite 2}
		rx_process2dropFifo_2_0 {Type O LastRead -1 FirstWrite 2}
		rx_process2dropFifo_s_2 {Type O LastRead -1 FirstWrite 2}
		rx_process2dropLengt_1 {Type O LastRead -1 FirstWrite 4}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "5", "Max" : "5"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	dataIn_V_data_V { axis {  { s_axis_rx_data_TVALID in_vld 0 1 }  { s_axis_rx_data_TDATA in_data 0 64 } } }
	dataIn_V_keep_V { axis {  { s_axis_rx_data_TKEEP in_data 0 8 } } }
	dataIn_V_last_V { axis {  { s_axis_rx_data_TREADY in_acc 1 1 }  { s_axis_rx_data_TLAST in_data 0 1 } } }
	MetaOut_V { axis {  { MetaOut_V_TREADY out_acc 0 1 }  { MetaOut_V_TDATA out_data 1 48 }  { MetaOut_V_TVALID out_vld 1 1 } } }
	rx_process2dropFifo_1_1 { ap_fifo {  { rx_process2dropFifo_1_1_din fifo_data 1 64 }  { rx_process2dropFifo_1_1_full_n fifo_status 0 1 }  { rx_process2dropFifo_1_1_write fifo_update 1 1 } } }
	rx_process2dropFifo_2_0 { ap_fifo {  { rx_process2dropFifo_2_0_din fifo_data 1 8 }  { rx_process2dropFifo_2_0_full_n fifo_status 0 1 }  { rx_process2dropFifo_2_0_write fifo_update 1 1 } } }
	rx_process2dropFifo_s_2 { ap_fifo {  { rx_process2dropFifo_s_2_din fifo_data 1 1 }  { rx_process2dropFifo_s_2_full_n fifo_status 0 1 }  { rx_process2dropFifo_s_2_write fifo_update 1 1 } } }
	rx_process2dropLengt_1 { ap_fifo {  { rx_process2dropLengt_1_din fifo_data 1 4 }  { rx_process2dropLengt_1_full_n fifo_status 0 1 }  { rx_process2dropLengt_1_write fifo_update 1 1 } } }
}
set moduleName process_ipv4_64_s
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {process_ipv4<64>}
set C_modelType { void 0 }
set C_modelArgList {
	{ dataIn_V_data_V int 64 regular {axi_s 0 volatile  { s_axis_rx_data Data } }  }
	{ dataIn_V_keep_V int 8 regular {axi_s 0 volatile  { s_axis_rx_data Keep } }  }
	{ dataIn_V_last_V int 1 regular {axi_s 0 volatile  { s_axis_rx_data Last } }  }
	{ MetaOut_V int 48 regular {axi_s 1 volatile  { MetaOut_V Data } }  }
	{ rx_process2dropFifo_1_1 int 64 regular {fifo 1 volatile } {global 1}  }
	{ rx_process2dropFifo_2_0 int 8 regular {fifo 1 volatile } {global 1}  }
	{ rx_process2dropFifo_s_2 int 1 regular {fifo 1 volatile } {global 1}  }
	{ rx_process2dropLengt_1 int 4 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "dataIn_V_data_V", "interface" : "axis", "bitwidth" : 64, "direction" : "READONLY"} , 
 	{ "Name" : "dataIn_V_keep_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "dataIn_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY"} , 
 	{ "Name" : "MetaOut_V", "interface" : "axis", "bitwidth" : 48, "direction" : "WRITEONLY"} , 
 	{ "Name" : "rx_process2dropFifo_1_1", "interface" : "fifo", "bitwidth" : 64, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rx_process2dropFifo_2_0", "interface" : "fifo", "bitwidth" : 8, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rx_process2dropFifo_s_2", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "rx_process2dropLengt_1", "interface" : "fifo", "bitwidth" : 4, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 27
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ s_axis_rx_data_TVALID sc_in sc_logic 1 invld 0 } 
	{ rx_process2dropFifo_1_1_din sc_out sc_lv 64 signal 4 } 
	{ rx_process2dropFifo_1_1_full_n sc_in sc_logic 1 signal 4 } 
	{ rx_process2dropFifo_1_1_write sc_out sc_logic 1 signal 4 } 
	{ rx_process2dropFifo_2_0_din sc_out sc_lv 8 signal 5 } 
	{ rx_process2dropFifo_2_0_full_n sc_in sc_logic 1 signal 5 } 
	{ rx_process2dropFifo_2_0_write sc_out sc_logic 1 signal 5 } 
	{ rx_process2dropFifo_s_2_din sc_out sc_lv 1 signal 6 } 
	{ rx_process2dropFifo_s_2_full_n sc_in sc_logic 1 signal 6 } 
	{ rx_process2dropFifo_s_2_write sc_out sc_logic 1 signal 6 } 
	{ rx_process2dropLengt_1_din sc_out sc_lv 4 signal 7 } 
	{ rx_process2dropLengt_1_full_n sc_in sc_logic 1 signal 7 } 
	{ rx_process2dropLengt_1_write sc_out sc_logic 1 signal 7 } 
	{ MetaOut_V_TREADY sc_in sc_logic 1 outacc 3 } 
	{ s_axis_rx_data_TDATA sc_in sc_lv 64 signal 0 } 
	{ s_axis_rx_data_TREADY sc_out sc_logic 1 inacc 2 } 
	{ s_axis_rx_data_TKEEP sc_in sc_lv 8 signal 1 } 
	{ s_axis_rx_data_TLAST sc_in sc_lv 1 signal 2 } 
	{ MetaOut_V_TDATA sc_out sc_lv 48 signal 3 } 
	{ MetaOut_V_TVALID sc_out sc_logic 1 outvld 3 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "s_axis_rx_data_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "dataIn_V_data_V", "role": "VALID" }} , 
 	{ "name": "rx_process2dropFifo_1_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "rx_process2dropFifo_1_1", "role": "din" }} , 
 	{ "name": "rx_process2dropFifo_1_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_process2dropFifo_1_1", "role": "full_n" }} , 
 	{ "name": "rx_process2dropFifo_1_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_process2dropFifo_1_1", "role": "write" }} , 
 	{ "name": "rx_process2dropFifo_2_0_din", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "rx_process2dropFifo_2_0", "role": "din" }} , 
 	{ "name": "rx_process2dropFifo_2_0_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_process2dropFifo_2_0", "role": "full_n" }} , 
 	{ "name": "rx_process2dropFifo_2_0_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_process2dropFifo_2_0", "role": "write" }} , 
 	{ "name": "rx_process2dropFifo_s_2_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_process2dropFifo_s_2", "role": "din" }} , 
 	{ "name": "rx_process2dropFifo_s_2_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_process2dropFifo_s_2", "role": "full_n" }} , 
 	{ "name": "rx_process2dropFifo_s_2_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_process2dropFifo_s_2", "role": "write" }} , 
 	{ "name": "rx_process2dropLengt_1_din", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "rx_process2dropLengt_1", "role": "din" }} , 
 	{ "name": "rx_process2dropLengt_1_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_process2dropLengt_1", "role": "full_n" }} , 
 	{ "name": "rx_process2dropLengt_1_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "rx_process2dropLengt_1", "role": "write" }} , 
 	{ "name": "MetaOut_V_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "MetaOut_V", "role": "TREADY" }} , 
 	{ "name": "s_axis_rx_data_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "dataIn_V_data_V", "role": "DATA" }} , 
 	{ "name": "s_axis_rx_data_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "dataIn_V_last_V", "role": "READY" }} , 
 	{ "name": "s_axis_rx_data_TKEEP", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "dataIn_V_keep_V", "role": "KEEP" }} , 
 	{ "name": "s_axis_rx_data_TLAST", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "dataIn_V_last_V", "role": "LAST" }} , 
 	{ "name": "MetaOut_V_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":48, "type": "signal", "bundle":{"name": "MetaOut_V", "role": "TDATA" }} , 
 	{ "name": "MetaOut_V_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "MetaOut_V", "role": "TVALID" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "process_ipv4_64_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "4", "EstimateLatencyMin" : "4", "EstimateLatencyMax" : "4",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "dataIn_V_data_V", "Type" : "Axis", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "s_axis_rx_data_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataIn_V_keep_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "dataIn_V_last_V", "Type" : "Axis", "Direction" : "I"},
			{"Name" : "MetaOut_V", "Type" : "Axis", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "MetaOut_V_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "header_ready", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_idx", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "header_header_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "metaWritten", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "headerWordsDropped_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "rx_process2dropFifo_1_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rx_process2dropFifo_1_1_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rx_process2dropFifo_2_0", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rx_process2dropFifo_2_0_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rx_process2dropFifo_s_2", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rx_process2dropFifo_s_2_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "rx_process2dropLengt_1", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "rx_process2dropLengt_1_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	process_ipv4_64_s {
		dataIn_V_data_V {Type I LastRead 0 FirstWrite -1}
		dataIn_V_keep_V {Type I LastRead 0 FirstWrite -1}
		dataIn_V_last_V {Type I LastRead 0 FirstWrite -1}
		MetaOut_V {Type O LastRead -1 FirstWrite 3}
		header_ready {Type IO LastRead -1 FirstWrite -1}
		header_idx {Type IO LastRead -1 FirstWrite -1}
		header_header_V {Type IO LastRead -1 FirstWrite -1}
		metaWritten {Type IO LastRead -1 FirstWrite -1}
		headerWordsDropped_V {Type IO LastRead -1 FirstWrite -1}
		rx_process2dropFifo_1_1 {Type O LastRead -1 FirstWrite 1}
		rx_process2dropFifo_2_0 {Type O LastRead -1 FirstWrite 1}
		rx_process2dropFifo_s_2 {Type O LastRead -1 FirstWrite 1}
		rx_process2dropLengt_1 {Type O LastRead -1 FirstWrite 3}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "4", "Max" : "4"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	dataIn_V_data_V { axis {  { s_axis_rx_data_TVALID in_vld 0 1 }  { s_axis_rx_data_TDATA in_data 0 64 } } }
	dataIn_V_keep_V { axis {  { s_axis_rx_data_TKEEP in_data 0 8 } } }
	dataIn_V_last_V { axis {  { s_axis_rx_data_TREADY in_acc 1 1 }  { s_axis_rx_data_TLAST in_data 0 1 } } }
	MetaOut_V { axis {  { MetaOut_V_TREADY out_acc 0 1 }  { MetaOut_V_TDATA out_data 1 48 }  { MetaOut_V_TVALID out_vld 1 1 } } }
	rx_process2dropFifo_1_1 { ap_fifo {  { rx_process2dropFifo_1_1_din fifo_data 1 64 }  { rx_process2dropFifo_1_1_full_n fifo_status 0 1 }  { rx_process2dropFifo_1_1_write fifo_update 1 1 } } }
	rx_process2dropFifo_2_0 { ap_fifo {  { rx_process2dropFifo_2_0_din fifo_data 1 8 }  { rx_process2dropFifo_2_0_full_n fifo_status 0 1 }  { rx_process2dropFifo_2_0_write fifo_update 1 1 } } }
	rx_process2dropFifo_s_2 { ap_fifo {  { rx_process2dropFifo_s_2_din fifo_data 1 1 }  { rx_process2dropFifo_s_2_full_n fifo_status 0 1 }  { rx_process2dropFifo_s_2_write fifo_update 1 1 } } }
	rx_process2dropLengt_1 { ap_fifo {  { rx_process2dropLengt_1_din fifo_data 1 4 }  { rx_process2dropLengt_1_full_n fifo_status 0 1 }  { rx_process2dropLengt_1_write fifo_update 1 1 } } }
}
